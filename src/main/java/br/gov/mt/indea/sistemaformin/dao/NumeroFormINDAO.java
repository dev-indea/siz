package br.gov.mt.indea.sistemaformin.dao;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.inject.Inject;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import br.gov.mt.indea.sistemaformin.entity.Municipio;
import br.gov.mt.indea.sistemaformin.entity.NumeroFormIN;

@Stateless
public class NumeroFormINDAO extends DAO<NumeroFormIN>{

	@Inject
	private EntityManager em;
	
	public NumeroFormINDAO() {
		super(NumeroFormIN.class);
	}

	@Override
	public EntityManager getEntityManager() {
		return this.em;
	}
	
	public NumeroFormIN getNumeroFormINByMunicipio(Municipio municipio){
		NumeroFormIN numeroFormIN;
		
		CriteriaBuilder cb  = em.getCriteriaBuilder();
		CriteriaQuery<NumeroFormIN> criteria = cb.createQuery(NumeroFormIN.class);
		Root<NumeroFormIN> rootNumeroFormIN = criteria.from(NumeroFormIN.class);
		criteria.where(cb.equal(rootNumeroFormIN.get("municipio"), municipio));
		
		try {
			numeroFormIN = em.createQuery(criteria).getSingleResult();
		} catch (NoResultException e) {
			numeroFormIN = null;
		}

		return numeroFormIN;
	}

}
