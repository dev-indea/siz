package br.gov.mt.indea.sistemaformin.managedbeans;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLBeanName;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;

import br.gov.mt.indea.sistemaformin.entity.ParametrosSIZ;
import br.gov.mt.indea.sistemaformin.service.ParametrosSIZService;

@Named
@ViewScoped
@URLBeanName("parametrosSIZManagedBean")
@URLMappings(mappings = {
		@URLMapping(id = "visualizarParametrosSIZ", pattern = "/parametrosSIZ/visualizar", viewId = "/pages/parametrosSIZ/editar.jsf"),
		@URLMapping(id = "editarParametrosSIZ", pattern = "/parametrosSIZ/editar", viewId = "/pages/parametrosSIZ/editar.jsf")})
public class ParametrosSIZManagedBean implements Serializable {

	private static final long serialVersionUID = 5401332308697941357L;
	
	@Inject
	private ParametrosSIZ parametrosSIZ;
	
	@Inject
	private ParametrosSIZService parametrosSIZService;
	
	private boolean visualizando = false;
	
	private boolean editando = false;
	
	@PostConstruct
	private void init(){
		this.parametrosSIZ = this.carregarParametrosSIZ();
	}
	
	private ParametrosSIZ carregarParametrosSIZ(){
		return parametrosSIZService.getInstance();
	}
	
	@URLAction(mappingId = "visualizarParametrosSIZ", onPostback = false)
	public void visualizar(){
		this.visualizando = true;
		this.editando = false;
	}
	
	@URLAction(mappingId = "editarParametrosSIZ", onPostback = false)
	public void editar(){
		this.visualizando = false;
		this.editando = true;
	}
	
	public String salvar() {
		parametrosSIZService.saveOrUpdate(this.parametrosSIZ);
		
		return "pretty:visualizarParametrosSIZ";
	}

	public boolean isVisualizando() {
		return visualizando;
	}

	public void setVisualizando(boolean visualizando) {
		this.visualizando = visualizando;
	}

	public boolean isEditando() {
		return editando;
	}

	public void setEditando(boolean editando) {
		this.editando = editando;
	}

	public ParametrosSIZ getParametrosSIZ() {
		return parametrosSIZ;
	}

	public void setParametrosSIZ(ParametrosSIZ parametrosSIZ) {
		this.parametrosSIZ = parametrosSIZ;
	}

	
}
