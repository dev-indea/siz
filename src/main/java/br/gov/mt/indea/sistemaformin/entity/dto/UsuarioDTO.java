package br.gov.mt.indea.sistemaformin.entity.dto;

import java.io.Serializable;

import br.gov.mt.indea.sistemaformin.enums.Dominio.AtivoInativo;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoUsuario;

public class UsuarioDTO implements AbstractDTO, Serializable {

	private static final long serialVersionUID = 958534186952468759L;

	private String nome;

	private String cpf;
	
	private String username;
	
	private TipoUsuario tipoUsuario;
	
	private AtivoInativo status;
	
	public boolean isNull(){
		if (nome == null && cpf == null && username == null && tipoUsuario == null && status == null)
			return true;
		return false;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public TipoUsuario getTipoUsuario() {
		return tipoUsuario;
	}

	public void setTipoUsuario(TipoUsuario tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}

	public AtivoInativo getStatus() {
		return status;
	}

	public void setStatus(AtivoInativo status) {
		this.status = status;
	}

}
