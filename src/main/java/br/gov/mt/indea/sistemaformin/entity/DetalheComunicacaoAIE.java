package br.gov.mt.indea.sistemaformin.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import br.gov.mt.indea.sistemaformin.webservice.entity.Propriedade;

@Audited
@Entity
@Table(name="detalhe_comunicacao_aie")
public class DetalheComunicacaoAIE extends BaseEntity<Long>{

	private static final long serialVersionUID = 6459515682267284361L;

	@Id
	@SequenceGenerator(name="detalhe_comunicacao_aie_seq", sequenceName="detalhe_comunicacao_aie_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="detalhe_comunicacao_aie_seq")
	private Long id;
	
	@ManyToOne
	@JoinColumn(name="id_comunicacao_aie")
	private ComunicacaoAIE comunicacaoAIE;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_municipio")
	private Municipio municipio;
	
	@Column
	private String nomePropriedade;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_propriedade")
	private Propriedade propriedade;
	
	@Column(name="quantidade_equideos_propriedade")
	private Long quantidadeEquideosNaPropriedade;
	
	@Column(name="quantidade_equinos_positivos")
	private Long quantidadeEquinosPositivos;
	
	@Column(name="quantidade_Muares_positivos")
	private Long quantidadeMuaresPositivos;
	
	@Column(name="quantidade_asininos_positivos")
	private Long quantidadeAsininosPositivos;
	
	@Column(name="quantidade_equinos_negativos")
	private Long quantidadeEquinosNegativos;
	
	@Column(name="quantidade_Muares_negativos")
	private Long quantidadeMuaresNegativos;
	
	@Column(name="quantidade_asininos_negativos")
	private Long quantidadeAsininosNegativos;
	
	@Column(name="quantidade_equinos_examinados")
	private Long quantidadeEquinosExaminados;
	
	@Column(name="quantidade_Muares_examinados")
	private Long quantidadeMuaresExaminados;
	
	@Column(name="quantidade_asininos_examinados")
	private Long quantidadeAsininosExaminados;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_formin")
	private FormIN formIN;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_visita_propriedade_rural")
	private VisitaPropriedadeRural visitaPropriedadeRural;

	public boolean isInvestigacaoFinalizada(){
		return this.visitaPropriedadeRural != null || this.formIN.isInvestigacaoEncerrada();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Municipio getMunicipio() {
		return municipio;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}

	public String getNomePropriedade() {
		return nomePropriedade;
	}

	public void setNomePropriedade(String nomePropriedade) {
		this.nomePropriedade = nomePropriedade;
	}

	public Propriedade getPropriedade() {
		return propriedade;
	}

	public void setPropriedade(Propriedade propriedade) {
		this.propriedade = propriedade;
	}

	public Long getQuantidadeEquideosNaPropriedade() {
		return quantidadeEquideosNaPropriedade;
	}

	public void setQuantidadeEquideosNaPropriedade(Long quantidadeEquideosNaPropriedade) {
		this.quantidadeEquideosNaPropriedade = quantidadeEquideosNaPropriedade;
	}

	public Long getQuantidadeEquinosPositivos() {
		return quantidadeEquinosPositivos;
	}

	public void setQuantidadeEquinosPositivos(Long quantidadeEquinosPositivos) {
		this.quantidadeEquinosPositivos = quantidadeEquinosPositivos;
	}

	public Long getQuantidadeMuaresPositivos() {
		return quantidadeMuaresPositivos;
	}

	public void setQuantidadeMuaresPositivos(Long quantidadeMuaresPositivos) {
		this.quantidadeMuaresPositivos = quantidadeMuaresPositivos;
	}

	public Long getQuantidadeAsininosPositivos() {
		return quantidadeAsininosPositivos;
	}

	public void setQuantidadeAsininosPositivos(Long quantidadeAsininosPositivos) {
		this.quantidadeAsininosPositivos = quantidadeAsininosPositivos;
	}

	public Long getQuantidadeEquinosNegativos() {
		return quantidadeEquinosNegativos;
	}

	public void setQuantidadeEquinosNegativos(Long quantidadeEquinosNegativos) {
		this.quantidadeEquinosNegativos = quantidadeEquinosNegativos;
	}

	public Long getQuantidadeMuaresNegativos() {
		return quantidadeMuaresNegativos;
	}

	public void setQuantidadeMuaresNegativos(Long quantidadeMuaresNegativos) {
		this.quantidadeMuaresNegativos = quantidadeMuaresNegativos;
	}

	public Long getQuantidadeAsininosNegativos() {
		return quantidadeAsininosNegativos;
	}

	public void setQuantidadeAsininosNegativos(Long quantidadeAsininosNegativos) {
		this.quantidadeAsininosNegativos = quantidadeAsininosNegativos;
	}

	public Long getQuantidadeEquinosExaminados() {
		return quantidadeEquinosExaminados;
	}

	public void setQuantidadeEquinosExaminados(Long quantidadeEquinosExaminados) {
		this.quantidadeEquinosExaminados = quantidadeEquinosExaminados;
	}

	public Long getQuantidadeMuaresExaminados() {
		return quantidadeMuaresExaminados;
	}

	public void setQuantidadeMuaresExaminados(Long quantidadeMuaresExaminados) {
		this.quantidadeMuaresExaminados = quantidadeMuaresExaminados;
	}

	public Long getQuantidadeAsininosExaminados() {
		return quantidadeAsininosExaminados;
	}

	public void setQuantidadeAsininosExaminados(Long quantidadeAsininosExaminados) {
		this.quantidadeAsininosExaminados = quantidadeAsininosExaminados;
	}

	public ComunicacaoAIE getComunicacaoAIE() {
		return comunicacaoAIE;
	}

	public void setComunicacaoAIE(ComunicacaoAIE comunicacaoAIE) {
		this.comunicacaoAIE = comunicacaoAIE;
	}

	public FormIN getFormIN() {
		return formIN;
	}

	public void setFormIN(FormIN formIN) {
		this.formIN = formIN;
	}

	public VisitaPropriedadeRural getVisitaPropriedadeRural() {
		return visitaPropriedadeRural;
	}

	public void setVisitaPropriedadeRural(VisitaPropriedadeRural visitaPropriedadeRural) {
		this.visitaPropriedadeRural = visitaPropriedadeRural;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DetalheComunicacaoAIE other = (DetalheComunicacaoAIE) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
