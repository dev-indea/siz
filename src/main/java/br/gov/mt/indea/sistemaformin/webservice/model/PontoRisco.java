package br.gov.mt.indea.sistemaformin.webservice.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class PontoRisco implements Serializable {

	private static final long serialVersionUID = 5686234513709189372L;

	private Long id;
   
	@XmlElement(name="municipio")
	private Municipio municipio;
   
	@XmlElement(name = "tipoPontoRisco")
	private TipoPontoRisco tipoPontoRisco;
   
	private String orientacaoLongitude;
    
	private String orientacaoLatitude;

	private String nome;

	private Integer grauLatitude;
	    
	private Integer minLatitude;
	
	private Float segLatitude;
	
	private Integer grauLongitude;
	
	private Integer minLongitude;
	
	private Float segLongitude;
	
	private String observacao;

    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Municipio getMunicipio() {
		return municipio;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}

	public TipoPontoRisco getTipoPontoRisco() {
		return tipoPontoRisco;
	}

	public void setTipoPontoRisco(TipoPontoRisco tipoPontoRisco) {
		this.tipoPontoRisco = tipoPontoRisco;
	}

	public String getOrientacaoLongitude() {
		return orientacaoLongitude;
	}

	public void setOrientacaoLongitude(String orientacaoLongitude) {
		this.orientacaoLongitude = orientacaoLongitude;
	}

	public String getOrientacaoLatitude() {
		return orientacaoLatitude;
	}

	public void setOrientacaoLatitude(String orientacaoLatitude) {
		this.orientacaoLatitude = orientacaoLatitude;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getGrauLatitude() {
		return grauLatitude;
	}

	public void setGrauLatitude(Integer grauLatitude) {
		this.grauLatitude = grauLatitude;
	}

	public Integer getMinLatitude() {
		return minLatitude;
	}

	public void setMinLatitude(Integer minLatitude) {
		this.minLatitude = minLatitude;
	}

	public Float getSegLatitude() {
		return segLatitude;
	}

	public void setSegLatitude(Float segLatitude) {
		this.segLatitude = segLatitude;
	}

	public Integer getGrauLongitude() {
		return grauLongitude;
	}

	public void setGrauLongitude(Integer grauLongitude) {
		this.grauLongitude = grauLongitude;
	}

	public Integer getMinLongitude() {
		return minLongitude;
	}

	public void setMinLongitude(Integer minLongitude) {
		this.minLongitude = minLongitude;
	}

	public Float getSegLongitude() {
		return segLongitude;
	}

	public void setSegLongitude(Float segLongitude) {
		this.segLongitude = segLongitude;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

}