package br.gov.mt.indea.sistemaformin.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.mt.indea.sistemaformin.exception.ApplicationException;
import br.gov.mt.indea.sistemaformin.exception.ApplicationRuntimeException;
import br.gov.mt.indea.sistemaformin.util.StringUtil;
import br.gov.mt.indea.sistemaformin.webservice.client.ClientWebServicePropriedade;
import br.gov.mt.indea.sistemaformin.webservice.entity.Exploracao;
import br.gov.mt.indea.sistemaformin.webservice.entity.Produtor;
import br.gov.mt.indea.sistemaformin.webservice.entity.Propriedade;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class PropriedadeService extends PaginableService<Propriedade, Long> {
	
	private static final Logger log = LoggerFactory.getLogger(PropriedadeService.class);
	
	@Inject
	private ClientWebServicePropriedade clientWebServicePropriedade;

	protected PropriedadeService() {
		super(Propriedade.class);
	}
	
	public Propriedade findByIdFetchAll(Long id){
		Propriedade propriedade;
		
		StringBuilder sql = new StringBuilder();
		sql.append("  from Propriedade propriedade ")
		   .append(" where propriedade.id = :id ");
		
		Query query = getSession().createQuery(sql.toString()).setLong("id", id);
		propriedade = (Propriedade) query.uniqueResult();
		
		if (propriedade != null){
			
			
		}
		
		return propriedade;
	}
	
	private Propriedade findByCodigoFetchAll(Long codigoPropriedade) throws ApplicationException{
		Propriedade propriedade;
		
		StringBuilder sql = new StringBuilder();
		sql.append("  from Propriedade propriedade ")
		   .append("  left join fetch propriedade.endereco endereco ")
		   .append("  left join fetch propriedade.ule ule ")
		   .append("  left join fetch propriedade.coordenadaGeografica ")
		   .append("  left join fetch propriedade.representanteEstabelecimento ")
		   .append("  left join fetch propriedade.proprietarios proprietario")
		   .append("  left join fetch proprietario.endereco")
		   .append(" where propriedade.codigoPropriedade = :codigoPropriedade ")
		   .append(" order by propriedade.id asc ");
		
		Query query = getSession().createQuery(sql.toString()).setLong("codigoPropriedade", codigoPropriedade);
		query.setMaxResults(1);
		propriedade = (Propriedade) query.uniqueResult();
		
		if (propriedade != null){
			Hibernate.initialize(propriedade.getExploracaos());
			
			for (Exploracao exploracao : propriedade.getExploracaos()){
				Hibernate.initialize(exploracao.getProdutores());
				
				for (Produtor produtor : exploracao.getProdutores()) {
					Hibernate.initialize(produtor.getMunicipioNascimento());
					Hibernate.initialize(produtor.getEndereco());
					Hibernate.initialize(produtor.getEndereco().getMunicipio());
					Hibernate.initialize(produtor.getEndereco().getMunicipio().getUf());
				}
			}
			
		} 
		
		return propriedade;
	}
	
	public Propriedade findByCodigoFetchAll(Long codigoPropriedade, boolean forceUpdatePropriedade) throws ApplicationException{
		Propriedade propriedade;
		
		if (!forceUpdatePropriedade) {
			StringBuilder sql = new StringBuilder();
			sql.append("  from Propriedade propriedade ")
			   .append("  left join fetch propriedade.endereco endereco ")
			   .append("  left join fetch propriedade.ule ule ")
			   .append("  left join fetch propriedade.coordenadaGeografica ")
			   .append("  left join fetch propriedade.representanteEstabelecimento ")
			   .append("  left join fetch propriedade.proprietarios proprietario")
			   .append("  left join fetch proprietario.endereco")
			   .append(" where propriedade.codigoPropriedade = :codigoPropriedade ")
			   .append(" order by propriedade.id asc ");
			
			Query query = getSession().createQuery(sql.toString()).setLong("codigoPropriedade", codigoPropriedade);
			query.setMaxResults(1);
			propriedade = (Propriedade) query.uniqueResult();
			
			if (propriedade != null){
				Hibernate.initialize(propriedade.getExploracaos());
				
				for (Exploracao exploracao : propriedade.getExploracaos()){
					Hibernate.initialize(exploracao.getProdutores());
					
					for (Produtor produtor : exploracao.getProdutores()) {
						Hibernate.initialize(produtor.getMunicipioNascimento());
						Hibernate.initialize(produtor.getEndereco());
						if (produtor.getEndereco() != null) {
							Hibernate.initialize(produtor.getEndereco().getMunicipio());
							if (produtor.getEndereco().getMunicipio() != null)
								Hibernate.initialize(produtor.getEndereco().getMunicipio().getUf());
						}
					}
				}
				
				for (Produtor produtor : propriedade.getProprietarios()) {
					Hibernate.initialize(produtor.getMunicipioNascimento());
					Hibernate.initialize(produtor.getEndereco());
					if (produtor.getEndereco() != null) {
						Hibernate.initialize(produtor.getEndereco().getMunicipio());
						if (produtor.getEndereco().getMunicipio() != null)
							Hibernate.initialize(produtor.getEndereco().getMunicipio().getUf());
					}
				}
				
			} else {
				propriedade = clientWebServicePropriedade.getPropriedadeById(codigoPropriedade);
				
				if (propriedade != null)			{
					this.saveOrUpdateNewTransaction(propriedade);
					propriedade = this.getEntityManager().merge(propriedade);
				}
			}
		} else {
			propriedade = clientWebServicePropriedade.getPropriedadeById(codigoPropriedade);
			
			Propriedade propriedadeLocal = this.findByCodigoFetchAll(codigoPropriedade);
			
			if (propriedadeLocal != null) {
				Long id = propriedadeLocal.getId();
				List<Exploracao> listaExploracao = propriedadeLocal.getExploracaos();
				
				try {
					propriedadeLocal = (Propriedade) propriedade.clone();
					propriedadeLocal.setId(id);
					
					for (Exploracao exploracaoNew : propriedadeLocal.getExploracaos()) {
						for (Exploracao exploracaoOld : listaExploracao) {
							if (exploracaoNew.getCodigo().equals(exploracaoOld.getCodigo()))
								exploracaoNew.setId(exploracaoOld.getId());
						}
					}
				} catch (CloneNotSupportedException e) {
					throw new ApplicationRuntimeException("N�o foi poss�vel atualizar a propriedade");
				}
				
				this.saveOrUpdateNewTransaction(propriedadeLocal);
				
				propriedade = propriedadeLocal;
			} else if (propriedade != null){
				propriedade = this.getEntityManager().merge(propriedade);
			}
				
		}
		
		return propriedade;
	}
	
	public Propriedade findByCodigo(String codigo) throws ApplicationException{
		Propriedade propriedade;
		
		StringBuilder sql = new StringBuilder();
		sql.append("  from Propriedade propriedade ")
		   .append(" where propriedade.codigo = :codigo ");
		
		Query query = getSession().createQuery(sql.toString()).setString("codigo", codigo);
		query.setMaxResults(1);
		propriedade = (Propriedade) query.uniqueResult();
		
		if (propriedade != null){
			
		} else {
			propriedade = clientWebServicePropriedade.getPropriedadeById(Long.parseLong(codigo));
			
			if (propriedade != null) {
				this.saveOrUpdateNewTransaction(propriedade);
				propriedade = (Propriedade) this.getSession().merge(propriedade);
			}
		}
		
		return propriedade;
	}
	
	@SuppressWarnings("unchecked")
	public List<Propriedade> findByNomeFetchAll(String nomePropriedade) throws ApplicationException{
		List<Propriedade> lista;
		
		StringBuilder sql = new StringBuilder();
		sql.append("  from Propriedade propriedade ")
		   .append("  left join fetch propriedade.endereco endereco ")
		   .append("  left join fetch propriedade.ule ule ")
		   .append("  left join fetch propriedade.coordenadaGeografica ")
		   .append("  left join fetch propriedade.representanteEstabelecimento ")
		   .append("  left join fetch propriedade.proprietarios proprietario")
		   .append("  left join fetch proprietario.endereco")
		   .append(" where lower(remove_acento(propriedade.nome)) like :nomePropriedade ");
		
		Query query = getSession().createQuery(sql.toString()).setString("nomePropriedade", "%" + StringUtil.removeAcentos(nomePropriedade.toLowerCase()) + "%");
		lista = query.list();
		
		if (lista != null && !lista.isEmpty()){
			for (Propriedade propriedade : lista) {
				Hibernate.initialize(propriedade.getExploracaos());
				
				for (Exploracao exploracao : propriedade.getExploracaos()){
					Hibernate.initialize(exploracao.getProdutores());
					
					for (Produtor produtor : exploracao.getProdutores()) {
						Hibernate.initialize(produtor.getMunicipioNascimento());
						Hibernate.initialize(produtor.getEndereco());
						Hibernate.initialize(produtor.getEndereco().getMunicipio());
						Hibernate.initialize(produtor.getEndereco().getMunicipio().getUf());
					}
				}
			}
			
		} else {
			lista = clientWebServicePropriedade.getPropriedadeByNome(nomePropriedade);
			
			if (lista != null && !lista.isEmpty()) {
				if (!this.exists(lista.get(0))) {
					this.saveOrUpdateNewTransaction(lista.get(0));
				}
			}
				
		}
		
		return lista;
	}
	
	@Override
	public void saveOrUpdate(Propriedade propriedade) {
		super.saveOrUpdate(propriedade);
		
		log.info("Salvando Propriedade {}", propriedade.getId());
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void saveOrUpdateNewTransaction(Propriedade propriedade) {
		propriedade = (Propriedade) this.getSession().merge(propriedade);
		super.saveOrUpdate(propriedade);
		
		log.info("Salvando Propriedade {}", propriedade.getId());
	}
	
	public boolean exists(Propriedade propriedade) {
		Query query = getSession().             
		createQuery("select 1 from Propriedade p where p.codigoPropriedade = :codigoPropriedade");
		query.setLong("codigoPropriedade", propriedade.getCodigoPropriedade() );
		return (query.uniqueResult() != null);
	}
	
	@Override
	public void delete(Propriedade propriedade) {
		super.delete(propriedade);
		
		log.info("Removendo Propriedade {}", propriedade.getId());
	}
	
	@Override
	public void validar(Propriedade Propriedade) {

	}

	@Override
	public void validarPersist(Propriedade Propriedade) {

	}

	@Override
	public void validarMerge(Propriedade Propriedade) {

	}

	@Override
	public void validarDelete(Propriedade Propriedade) {

	}

}
