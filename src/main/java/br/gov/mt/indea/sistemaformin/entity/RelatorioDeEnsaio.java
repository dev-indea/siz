package br.gov.mt.indea.sistemaformin.entity;

import java.util.Calendar;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

import br.gov.mt.indea.sistemaformin.enums.Dominio.ResultadoRelatorioDeEnsaio;

@Audited
@Entity(name="relatorio_de_ensaio")
public class RelatorioDeEnsaio extends BaseEntity<Long>{
	
	private static final long serialVersionUID = 8142179044208040547L;

	@Id
	@SequenceGenerator(name="relatorio_de_ensaio_seq", sequenceName="relatorio_de_ensaio_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="relatorio_de_ensaio_seq")
	private Long id;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_cadastro")
	private Calendar dataCadastro;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_relatorio")
	private Calendar dataRelatorio;
	
	@Column(name="numero", length=255, unique=true)
	private String numero;
	
	@Enumerated(EnumType.STRING)
	@Column(name="tipo_propriedade")
    private ResultadoRelatorioDeEnsaio resultadoRelatorioDeEnsaio;
	
	@OneToOne(cascade={CascadeType.REFRESH})
	@JoinColumn(name="id_pais")
	private FormularioDeColheitaTroncoEncefalico formularioDeColheitaTroncoEncefalico;
	
	public RelatorioDeEnsaio() {
	}
	
	public RelatorioDeEnsaio(FormularioDeColheitaTroncoEncefalico formularioDeColheitaTroncoEncefalico) {
		this.formularioDeColheitaTroncoEncefalico = formularioDeColheitaTroncoEncefalico;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Calendar getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Calendar dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Calendar getDataRelatorio() {
		return dataRelatorio;
	}

	public void setDataRelatorio(Calendar dataRelatorio) {
		this.dataRelatorio = dataRelatorio;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public ResultadoRelatorioDeEnsaio getResultadoRelatorioDeEnsaio() {
		return resultadoRelatorioDeEnsaio;
	}

	public void setResultadoRelatorioDeEnsaio(ResultadoRelatorioDeEnsaio resultadoRelatorioDeEnsaio) {
		this.resultadoRelatorioDeEnsaio = resultadoRelatorioDeEnsaio;
	}

	public FormularioDeColheitaTroncoEncefalico getFormularioDeColheitaTroncoEncefalico() {
		return formularioDeColheitaTroncoEncefalico;
	}

	public void setFormularioDeColheitaTroncoEncefalico(
			FormularioDeColheitaTroncoEncefalico formularioDeColheitaTroncoEncefalico) {
		this.formularioDeColheitaTroncoEncefalico = formularioDeColheitaTroncoEncefalico;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RelatorioDeEnsaio other = (RelatorioDeEnsaio) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}