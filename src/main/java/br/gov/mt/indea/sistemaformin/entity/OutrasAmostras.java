package br.gov.mt.indea.sistemaformin.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.envers.Audited;

@Audited
@Entity
@DiscriminatorValue("outros")
public class OutrasAmostras extends AbstractAmostra implements Serializable{

	private static final long serialVersionUID = -4754619476252704492L;
	
	@ManyToOne
	@JoinColumn(name="id_formLAB2")
	private FormLAB formLAB2;
	
	@Column(name="tipo_amostra")
	private String tipoAmostra;
	
	private Long total;
	
	@Column(name="meio_conservacao")
	private String meioConservacao;

	public String getTipoAmostra() {
		return tipoAmostra;
	}

	public FormLAB getFormLAB2() {
		return formLAB2;
	}

	public void setFormLAB2(FormLAB formLAB) {
		this.formLAB2 = formLAB;
	}

	public void setTipoAmostra(String tipoAmostra) {
		this.tipoAmostra = tipoAmostra;
	}

	public Long getTotal() {
		return total;
	}

	public void setTotal(Long total) {
		this.total = total;
	}

	public String getMeioConservacao() {
		return meioConservacao;
	}

	public void setMeioConservacao(String meioConservacao) {
		this.meioConservacao = meioConservacao;
	}

}