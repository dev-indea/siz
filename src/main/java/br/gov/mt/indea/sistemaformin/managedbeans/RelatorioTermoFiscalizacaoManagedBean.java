package br.gov.mt.indea.sistemaformin.managedbeans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang.time.DateUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;

import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLBeanName;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;

import br.gov.mt.indea.sistemaformin.entity.view.RelatorioTermoFiscalizacaoGeral;
import br.gov.mt.indea.sistemaformin.service.TermoFiscalizacaoService;
import br.gov.mt.indea.sistemaformin.util.FacesMessageUtil;

@Named
@ViewScoped
@URLBeanName("relatorioTermoFiscalizacaoManagedBean")
@URLMappings(mappings = {
		@URLMapping(id = "relatorioTermoFiscalizacao", pattern = "/relatorio/termoFiscalizacao", viewId = "/pages/relatorios/termofiscalizacao.jsf")})
public class RelatorioTermoFiscalizacaoManagedBean implements Serializable {

	private static final long serialVersionUID = 1760023349204758559L;

	@Inject
	private TermoFiscalizacaoService termoFiscalizacaoService;
	
	private List<RelatorioTermoFiscalizacaoGeral> listaTermoFiscalizacao;
	
	private List<RelatorioTermoFiscalizacaoGeral> listaTermoFiscalizacaoFiltered;
	
	private Date dataInicial;
	
	private Date dataFinal;
	
	@PostConstruct
	private void init(){
		dataInicial = new Date();
		dataFinal = new Date();
		
		Calendar c = Calendar.getInstance();
		c.set(Calendar.DAY_OF_MONTH, 1);
		
		dataInicial.setTime(c.getTimeInMillis());
		
		c.set(Calendar.DAY_OF_MONTH, 30);
		
		dataFinal.setTime(c.getTimeInMillis());
	}
	
	@URLAction(mappingId = "relatorioTermoFiscalizacao", onPostback = false)
	public void novo(){
		
	}
	
	public void buscar(){
		long diff = dataFinal.getTime() - dataInicial.getTime();
		
	    if (TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS) > 366){
	    	FacesMessageUtil.addWarnContextFacesMessage("O per�odo de busca n�o deve ser superior a 1 ano", "");
	    	return;
	    }
		
		listaTermoFiscalizacao = (ArrayList<RelatorioTermoFiscalizacaoGeral>) termoFiscalizacaoService.relatorioTeste(this.dataInicial, this.dataFinal);
	}
	
	public void postProcessor(Object doc){
		HSSFWorkbook book = (HSSFWorkbook)doc;
	    HSSFSheet sheet = book.getSheetAt(0); 

	    HSSFRow header = sheet.getRow(0);
	    
	    int colCount = header.getPhysicalNumberOfCells();
	    int rowCount = sheet.getPhysicalNumberOfRows();

	    HSSFCellStyle intStyle = book.createCellStyle();
		intStyle.setDataFormat((short)1);

		HSSFCellStyle decStyle = book.createCellStyle();
		decStyle.setDataFormat((short)2);
		
		HSSFCellStyle headerStyle = book.createCellStyle();
		headerStyle.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
		headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		HSSFFont font = book.createFont();
        font.setBold(true);
        headerStyle.setFont(font);

        //Transforma o t�tulo em negrito
        for(int colNum = 0; colNum < header.getLastCellNum(); colNum++){   
	    	HSSFCell cell = header.getCell(colNum);
	    	cell.setCellStyle(headerStyle);
	    	book.getSheetAt(0).autoSizeColumn(colNum);
	    	
	    	if (colNum == 4)
	    		cell.setCellValue("Data da Visita");
	    	if (colNum == 5)
	    		cell.setCellValue("Data Cadastro");
        }
	    
		for(int rowInd = 1; rowInd < rowCount; rowInd++) {
		    HSSFRow row = sheet.getRow(rowInd);
		    for(int cellInd = 1; cellInd < colCount; cellInd++) {
		        HSSFCell cell = row.getCell(cellInd);
		        
		        //Apreens�es
		        if (cell.getColumnIndex() == 10){
		        	this.setCellIntegerStyle(cell, intStyle);
		        }
		        
		        //Autua��es
		        if (cell.getColumnIndex() == 11){
		        	this.setCellIntegerStyle(cell, intStyle);
		        }

		        //Comercializa��o
		        if (cell.getColumnIndex() == 12){
		        	this.setCellIntegerStyle(cell, intStyle);
		        }
		        
		        //Com�rcio Ambulante
		        if (cell.getColumnIndex() == 13){
		        	this.setCellIntegerStyle(cell, intStyle);
		        }
		        
		        //Estocagem e armazenagem
		        if (cell.getColumnIndex() == 14){
		        	this.setCellIntegerStyle(cell, intStyle);
		        }
		        
		        //Licenciamento
		        if (cell.getColumnIndex() == 15){
		        	this.setCellIntegerStyle(cell, intStyle);
		        }
		        
		        //Notifica��es
		        if (cell.getColumnIndex() == 16){
		        	this.setCellIntegerStyle(cell, intStyle);
		        }

		        //Prazo de validade
		        if (cell.getColumnIndex() == 17){
		        	this.setCellIntegerStyle(cell, intStyle);
		        }

		        //Recebimento de biol�gicos
		        if (cell.getColumnIndex() == 18){
		        	this.setCellIntegerStyle(cell, intStyle);
		        }

		        //Receitu�rios espec�ficos
		        if (cell.getColumnIndex() == 19){
		        	this.setCellIntegerStyle(cell, intStyle);
		        }

		        //Registro do produto
		        if (cell.getColumnIndex() == 20){
		        	this.setCellIntegerStyle(cell, intStyle);
		        }

		        //Renova��o anual da licen�a
		        if (cell.getColumnIndex() == 21){
		        	this.setCellIntegerStyle(cell, intStyle);
		        }

		        //Temperatura
		        if (cell.getColumnIndex() == 22){
		        	this.setCellIntegerStyle(cell, intStyle);
		        }
		        
		        //Troca de gelo em pontos espec�ficos
		        if (cell.getColumnIndex() == 23){
		        	this.setCellIntegerStyle(cell, intStyle);
		        }
		    }
		}
	}
	
	private void setCellIntegerStyle(HSSFCell cell, HSSFCellStyle style){
		String strVal = cell.getStringCellValue();
		cell.setCellStyle(style);
        
		if (strVal != null && !strVal.equals("")){
	    	int intVal = Integer.valueOf(strVal);
	    	cell.setCellValue(intVal);
		}
	}
	
	public List<String> getListaURSFilter(){
		if (this.listaTermoFiscalizacao == null)
			return null;
		else{
			List<String> lista = new ArrayList<>();
			
			for (RelatorioTermoFiscalizacaoGeral relatorio : listaTermoFiscalizacao) {
				if (!lista.contains(relatorio.getUrs()))
					lista.add((String) relatorio.getUrs());
			}
			
			List<String> sortedLista = new ArrayList<>();
			sortedLista = lista;
			
			try{
				Collections.sort(lista);
				sortedLista = lista;
			} catch (Exception e) {
			}
			
			return sortedLista;
		}
	}
	
	public List<String> getListaMunicipioFilter(){
		if (this.listaTermoFiscalizacao == null)
			return null;
		else{
			List<String> lista = new ArrayList<>();
			
			for (RelatorioTermoFiscalizacaoGeral relatorio : listaTermoFiscalizacao) {
				if (!lista.contains(relatorio.getUle()))
					lista.add((String) relatorio.getUle());
			}
			
			List<String> sortedLista = new ArrayList<>();
			sortedLista = lista;
			
			try{
				Collections.sort(lista);
				sortedLista = lista;
			} catch (Exception e) {
			}
			
			return sortedLista;
		}
	}
	
	public List<String> getListaMotivoDaVisitaFilter(){
		if (this.listaTermoFiscalizacao == null)
			return null;
		else{
			List<String> lista = new ArrayList<>();
			
			for (RelatorioTermoFiscalizacaoGeral relatorio : listaTermoFiscalizacao) {
				if (!lista.contains(relatorio.getMotivo_da_visita()))
					lista.add((String) relatorio.getMotivo_da_visita());
			}
			
			List<String> sortedLista = new ArrayList<>();
			sortedLista = lista;
			
			try{
				Collections.sort(lista);
				sortedLista = lista;
			} catch (Exception e) {
			}
			
			return sortedLista;
		}
	}
	
	public boolean filterByDate(Object value, Object filter, Locale locale){
		if( filter == null ) {
            return true;
        }

        if( value == null ) {
            return false;
        }
        
        if (value instanceof String){
        	String[] date = ((String) value).split("/");

        	Calendar c = Calendar.getInstance();
        	c.set(Calendar.DAY_OF_MONTH, Integer.parseInt(date[0]));
        	c.set(Calendar.MONTH, Integer.parseInt(date[1]) - 1);
        	c.set(Calendar.YEAR, Integer.parseInt(date[2]));
        	
        	return DateUtils.truncatedEquals((Date) filter, c.getTime(), Calendar.DATE);
        } else {
        	return DateUtils.truncatedEquals((Date) filter, (Date) value, Calendar.DATE);
        }
	}
	
	public List<RelatorioTermoFiscalizacaoGeral> getListaTermoFiscalizacao() {
		return listaTermoFiscalizacao;
	}

	public void setListaTermoFiscalizacao(List<RelatorioTermoFiscalizacaoGeral> listaTermoFiscalizacao) {
		this.listaTermoFiscalizacao = listaTermoFiscalizacao;
	}

	public Date getDataInicial() {
		return dataInicial;
	}

	public void setDataInicial(Date dataInicial) {
		this.dataInicial = dataInicial;
	}

	public Date getDataFinal() {
		return dataFinal;
	}

	public void setDataFinal(Date dataFinal) {
		this.dataFinal = dataFinal;
	}

	public List<RelatorioTermoFiscalizacaoGeral> getListaTermoFiscalizacaoFiltered() {
		return listaTermoFiscalizacaoFiltered;
	}

	public void setListaTermoFiscalizacaoFiltered(List<RelatorioTermoFiscalizacaoGeral> listaTermoFiscalizacaoFiltered) {
		this.listaTermoFiscalizacaoFiltered = listaTermoFiscalizacaoFiltered;
	}

}
