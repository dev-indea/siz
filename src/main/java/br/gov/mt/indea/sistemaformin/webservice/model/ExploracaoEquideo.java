package br.gov.mt.indea.sistemaformin.webservice.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ExploracaoEquideo implements Serializable {

	private static final long serialVersionUID = -9198506412636549333L;

	private ExploracaoEquideoPK exploracaoEquideoPK;
    
    @XmlElement
    private TipoExploracaoEquideo tipoExploracaoEquideo;
   
    @XmlElement
    private FinalidadeExploracao finalidadeExploracao;

    public ExploracaoEquideo() {
    }

    public TipoExploracaoEquideo getTipoExploracaoEquideo() {
        return tipoExploracaoEquideo;
    }

    public void setTipoExploracaoEquideo(TipoExploracaoEquideo tipoExploracaoEquideo) {
        this.tipoExploracaoEquideo = tipoExploracaoEquideo;
    }

    public FinalidadeExploracao getFinalidadeExploracao() {
        return finalidadeExploracao;
    }

    public void setFinalidadeExploracao(FinalidadeExploracao finalidadeExploracao) {
        this.finalidadeExploracao = finalidadeExploracao;
    }

    public ExploracaoEquideoPK getExploracaoEquideoPK() {
        return exploracaoEquideoPK;
    }

    public void setExploracaoEquideoPK(ExploracaoEquideoPK exploracaoEquideoPK) {
        this.exploracaoEquideoPK = exploracaoEquideoPK;
    }
}