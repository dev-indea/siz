package br.gov.mt.indea.sistemaformin.entity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

@Audited
@Entity
@Table(name="comunicacao_aie")
public class ComunicacaoAIE extends BaseEntity<Long>{

	private static final long serialVersionUID = 5538966309386584188L;

	@Id
	@SequenceGenerator(name="comunicacao_aie_seq", sequenceName="comunicacao_aie_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="comunicacao_aie_seq")
	private Long id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_cadastro")
	private Calendar dataCadastro = Calendar.getInstance();
	
	@Temporal(TemporalType.DATE)
	@Column(name="data_comunicacao")
	private Calendar dataComunicacao;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_laboratorio")
	private Laboratorio laboratorio;
	
	@OneToMany(mappedBy="comunicacaoAIE", orphanRemoval=true, cascade={CascadeType.ALL}, fetch=FetchType.LAZY)
	private List<DetalheComunicacaoAIE> listaDetalheComunicacaoAIE = new ArrayList<DetalheComunicacaoAIE>();
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Calendar getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Calendar dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Calendar getDataComunicacao() {
		return dataComunicacao;
	}

	public void setDataComunicacao(Calendar dataComunicacao) {
		this.dataComunicacao = dataComunicacao;
	}

	public Laboratorio getLaboratorio() {
		return laboratorio;
	}

	public void setLaboratorio(Laboratorio laboratorio) {
		this.laboratorio = laboratorio;
	}

	public List<DetalheComunicacaoAIE> getListaDetalheComunicacaoAIE() {
		return listaDetalheComunicacaoAIE;
	}

	public void setListaDetalheComunicacaoAIE(List<DetalheComunicacaoAIE> listaDetalheComunicacaoAIE) {
		this.listaDetalheComunicacaoAIE = listaDetalheComunicacaoAIE;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ComunicacaoAIE other = (ComunicacaoAIE) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
