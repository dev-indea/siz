package br.gov.mt.indea.sistemaformin.managedbeans;

import java.io.IOException;
import java.io.Serializable;
import java.util.Calendar;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLBeanName;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;

import br.gov.mt.indea.sistemaformin.entity.TipoChavePrincipalVisitaPropriedadeRural;
import br.gov.mt.indea.sistemaformin.enums.Dominio.AtivoInativo;
import br.gov.mt.indea.sistemaformin.service.LazyObjectDataModel;
import br.gov.mt.indea.sistemaformin.service.TipoChavePrincipalVisitaPropriedadeRuralService;
import br.gov.mt.indea.sistemaformin.util.FacesMessageUtil;

@Named
@ViewScoped
@URLBeanName("tipoChavePrincipalVisitaPropriedadeRuralManagedBean")
@URLMappings(mappings = {
		@URLMapping(id = "pesquisarTipoChavePrincipalVisitaPropriedadeRural", pattern = "/tipoChavePrincipalVisitaPropriedadeRural/pesquisar", viewId = "/pages/tipoChavePrincipalVisitaPropriedadeRural/lista.jsf"),
		@URLMapping(id = "incluirTipoChavePrincipalVisitaPropriedadeRural", pattern = "/tipoChavePrincipalVisitaPropriedadeRural/incluir", viewId = "/pages/tipoChavePrincipalVisitaPropriedadeRural/novo.jsf"),
		@URLMapping(id = "alterarTipoChavePrincipalVisitaPropriedadeRural", pattern = "/tipoChavePrincipalVisitaPropriedadeRural/alterar/#{tipoChavePrincipalVisitaPropriedadeRuralManagedBean.id}", viewId = "/pages/tipoChavePrincipalVisitaPropriedadeRural/novo.jsf")})
public class TipoChavePrincipalVisitaPropriedadeRuralManagedBean implements Serializable {
	
	private static final long serialVersionUID = -7409333626528791621L;
	
	private Long id;

	@Inject
	private TipoChavePrincipalVisitaPropriedadeRuralService tipoChavePrincipalVisitaPropriedadeRuralService;
	
	@Inject
	private TipoChavePrincipalVisitaPropriedadeRural tipoChavePrincipalVisitaPropriedadeRural;
	
	private LazyObjectDataModel<TipoChavePrincipalVisitaPropriedadeRural> listaTipoChavePrincipalVisitaPropriedadeRural;
	
	@PostConstruct
	public void init(){
		this.listaTipoChavePrincipalVisitaPropriedadeRural = new LazyObjectDataModel<TipoChavePrincipalVisitaPropriedadeRural>(this.tipoChavePrincipalVisitaPropriedadeRuralService);
	}
	
	public void limpar(){
		this.tipoChavePrincipalVisitaPropriedadeRural = new TipoChavePrincipalVisitaPropriedadeRural();
	}
	
	@URLAction(mappingId = "incluirTipoChavePrincipalVisitaPropriedadeRural", onPostback = false)
	public void novo(){
		limpar();
	}
	
	@URLAction(mappingId = "alterarTipoChavePrincipalVisitaPropriedadeRural", onPostback = false)
	public void editar(){
		this.tipoChavePrincipalVisitaPropriedadeRural = tipoChavePrincipalVisitaPropriedadeRuralService.findByIdFetchAll(this.getId());
	}
	
	public String adicionar() throws IOException{
		
		if (tipoChavePrincipalVisitaPropriedadeRural.getId() != null){
			this.tipoChavePrincipalVisitaPropriedadeRuralService.saveOrUpdate(tipoChavePrincipalVisitaPropriedadeRural);
			FacesMessageUtil.addInfoContextFacesMessage("Tipo chave principal atualizado", "");
		}else{
			this.tipoChavePrincipalVisitaPropriedadeRural.setDataCadastro(Calendar.getInstance());
			this.tipoChavePrincipalVisitaPropriedadeRural.setStatus(AtivoInativo.ATIVO);
			this.tipoChavePrincipalVisitaPropriedadeRuralService.saveOrUpdate(tipoChavePrincipalVisitaPropriedadeRural);
			FacesMessageUtil.addInfoContextFacesMessage("Tipo chave principal adicionado", "");
		}
		
		this.tipoChavePrincipalVisitaPropriedadeRural = new TipoChavePrincipalVisitaPropriedadeRural();
		
		return "pretty:pesquisarTipoChavePrincipalVisitaPropriedadeRural";
	}
	
	public void remover(TipoChavePrincipalVisitaPropriedadeRural tipoChavePrincipalVisitaPropriedadeRural){
		this.tipoChavePrincipalVisitaPropriedadeRuralService.delete(tipoChavePrincipalVisitaPropriedadeRural);
		FacesMessageUtil.addInfoContextFacesMessage("Tipo chave principal exclu�do com sucesso", "");
	}
	
	public void ativar(TipoChavePrincipalVisitaPropriedadeRural tipoChavePrincipalVisitaPropriedadeRural){
		tipoChavePrincipalVisitaPropriedadeRural.setStatus(AtivoInativo.ATIVO);
		
		this.tipoChavePrincipalVisitaPropriedadeRuralService.saveOrUpdate(tipoChavePrincipalVisitaPropriedadeRural);
		FacesMessageUtil.addInfoContextFacesMessage("Tipo chave principal ativado", "");
	}
	
	public void desativar(TipoChavePrincipalVisitaPropriedadeRural tipoChavePrincipalVisitaPropriedadeRural){
		tipoChavePrincipalVisitaPropriedadeRural.setStatus(AtivoInativo.INATIVO);
		
		this.tipoChavePrincipalVisitaPropriedadeRuralService.saveOrUpdate(tipoChavePrincipalVisitaPropriedadeRural);
		FacesMessageUtil.addInfoContextFacesMessage("Tipo chave principal desativado", "");
	}
	
	public boolean isAtivado(TipoChavePrincipalVisitaPropriedadeRural tipoChavePrincipalVisitaPropriedadeRural){
		return tipoChavePrincipalVisitaPropriedadeRural.getStatus().equals(AtivoInativo.ATIVO);
	}
	
	public TipoChavePrincipalVisitaPropriedadeRural getTipoChavePrincipalVisitaPropriedadeRural() {
		return tipoChavePrincipalVisitaPropriedadeRural;
	}

	public void setTipoChavePrincipalVisitaPropriedadeRural(TipoChavePrincipalVisitaPropriedadeRural tipoChavePrincipalVisitaPropriedadeRural) {
		this.tipoChavePrincipalVisitaPropriedadeRural = tipoChavePrincipalVisitaPropriedadeRural;
	}

	public LazyObjectDataModel<TipoChavePrincipalVisitaPropriedadeRural> getListaTipoChavePrincipalVisitaPropriedadeRural() {
		return listaTipoChavePrincipalVisitaPropriedadeRural;
	}

	public void setListaTipoChavePrincipalVisitaPropriedadeRural(
			LazyObjectDataModel<TipoChavePrincipalVisitaPropriedadeRural> listaTipoChavePrincipalVisitaPropriedadeRural) {
		this.listaTipoChavePrincipalVisitaPropriedadeRural = listaTipoChavePrincipalVisitaPropriedadeRural;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
