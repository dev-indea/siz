package br.gov.mt.indea.sistemaformin.service;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.mt.indea.sistemaformin.entity.FormSV;
import br.gov.mt.indea.sistemaformin.entity.NovoEstabelecimentoParaInvestigacao;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class FormSVService extends PaginableService<FormSV, Long> {

	private static final Logger log = LoggerFactory.getLogger(FormSVService.class);
	
	protected FormSVService() {
		super(FormSV.class);
	}
	
	public FormSV findByIdFetchAll(Long id){
		FormSV formSV;
		
		StringBuilder sql = new StringBuilder();
		sql.append("select formSV ")
		   .append("  from FormSV formSV ")
		   .append("  join fetch formSV.formIN formin")
		   .append("  left join fetch formSV.formCOM")
		   .append("  join fetch formin.propriedade propriedade")
		   .append("  join fetch formin.proprietario proprietario")
		   .append("  left join fetch proprietario.endereco")
		   .append("  join fetch propriedade.municipio")
		   .append("  join fetch propriedade.ule")
		   .append("  left join fetch formSV.veterinarioAssistenteDoEstabelecimento vetA")
		   .append("  left join fetch vetA.endereco ")
		   .append("  join fetch formSV.veterinarioResponsavelPeloAtendimento vet")
		   .append("  left join fetch vet.endereco")
		   .append("  join fetch vet.conselho")
		   .append("  join fetch vet.tipoEmitente")
		   .append("  left join fetch vet.ule uu")
		   .append("  left join fetch uu.urs")
		   .append("  left join fetch vet.endereco")
		   .append(" where formSV.id = :id ");
		
		Query query = getSession().createQuery(sql.toString()).setLong("id", id);
		formSV = (FormSV) query.uniqueResult();
		
		if (formSV != null){
		
			Hibernate.initialize(formSV.getAvaliacoesClinicas());
				
			for (NovoEstabelecimentoParaInvestigacao estabelecimento : formSV.getNovoEstabelecimentoParaInvestigacaos()) {
				Hibernate.initialize(estabelecimento.getPropriedade());
				Hibernate.initialize(estabelecimento.getPropriedade().getEndereco());
				Hibernate.initialize(estabelecimento.getPropriedade().getMunicipio());
				Hibernate.initialize(estabelecimento.getPropriedade().getUle());
				Hibernate.initialize(estabelecimento.getListaVinculoEpidemiologico());
			}
			
		}
		
		return formSV;
	}
	
	@Override
	public void saveOrUpdate(FormSV formSV) {
		formSV = (FormSV) this.getSession().merge(formSV);
		super.saveOrUpdate(formSV);
		
		log.info("Salvando Form SV {}", formSV.getId());
	}
	
	@Override
	public void delete(FormSV formSV) {
		super.delete(formSV);
		
		log.info("Removendo Form SV {}", formSV.getId());
	}
	
	@Override
	public void validar(FormSV FormSV) {

	}

	@Override
	public void validarPersist(FormSV FormSV) {

	}

	@Override
	public void validarMerge(FormSV FormSV) {

	}

	@Override
	public void validarDelete(FormSV FormSV) {

	}

}
