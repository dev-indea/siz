package br.gov.mt.indea.sistemaformin.entity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

import br.gov.mt.indea.sistemaformin.enums.Dominio.Especie;
import br.gov.mt.indea.sistemaformin.enums.Dominio.Sexo;
import br.gov.mt.indea.sistemaformin.enums.Dominio.SimNao;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoPeriodo;

@Audited
@Entity(name="amostras")
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="tipo", discriminatorType=DiscriminatorType.STRING)
public abstract class AbstractAmostra extends BaseEntity<Long>{

	private static final long serialVersionUID = 4638974002543901039L;

	@Id
	@SequenceGenerator(name="amostras_seq", sequenceName="amostras_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="amostras_seq")
	private Long id;
	
	@Column(insertable=false, updatable=false)
    private String tipo;
	
	@Column(name="id_amostra")
	private Long idAmostra;
	
	@Column(name="id_animal_ou_lote")
	private String idAnimalOuLote;
	
	@Column(name="numero_colheita")
	private Long numeroColheita;
	
	@Enumerated(EnumType.STRING)
	private Especie especie;
	
	@Enumerated(EnumType.STRING)
	private Sexo sexo;
	
	private Long idade;
	
	@Enumerated(EnumType.STRING)
	@Column(name="tipo_periodo_idade")
	private TipoPeriodo tipoPeriodoIdade;
	
	@Enumerated(EnumType.STRING)
	@Column(name="sinais_clinicos")
	private SimNao sinaisClinicos;
	
	@Column(name="duracao_sinais")
	private Long duracaoSinais;
	
	@Enumerated(EnumType.STRING)
	@Column(name="tipo_periodo_duracao_sinais")
	private TipoPeriodo tipoPeriodoDaDuracaoDosSinais;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_ultima_vacina")
	private Calendar dataUltimaVacina;
	
	@OneToMany(mappedBy="amostra", cascade=CascadeType.ALL, orphanRemoval=true)
	private List<InformacoesComplementaresEspeciesFormLAB> informacoesComplementaresEspeciesFormLAB = new ArrayList<InformacoesComplementaresEspeciesFormLAB>();
	
	@OneToMany(mappedBy="amostra", cascade=CascadeType.ALL, orphanRemoval=true)
	private List<InformacoesComplementaresMedicamentosFormLAB> informacoesComplementaresMedicamentosFormLAB = new ArrayList<InformacoesComplementaresMedicamentosFormLAB>();
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getIdAnimalOuLote() {
		return idAnimalOuLote;
	}

	public void setIdAnimalOuLote(String idAnimalOuLote) {
		this.idAnimalOuLote = idAnimalOuLote;
	}

	public Long getNumeroColheita() {
		return numeroColheita;
	}

	public void setNumeroColheita(Long numeroColheita) {
		this.numeroColheita = numeroColheita;
	}

	public Especie getEspecie() {
		return especie;
	}

	public void setEspecie(Especie especie) {
		this.especie = especie;
	}

	public Sexo getSexo() {
		return sexo;
	}

	public void setSexo(Sexo sexo) {
		this.sexo = sexo;
	}

	public Long getIdade() {
		return idade;
	}

	public void setIdade(Long idade) {
		this.idade = idade;
	}

	public SimNao getSinaisClinicos() {
		return sinaisClinicos;
	}

	public void setSinaisClinicos(SimNao sinaisClinicos) {
		this.sinaisClinicos = sinaisClinicos;
	}

	public Long getDuracaoSinais() {
		return duracaoSinais;
	}

	public void setDuracaoSinais(Long duracaoSinais) {
		this.duracaoSinais = duracaoSinais;
	}

	public TipoPeriodo getTipoPeriodoDaDuracaoDosSinais() {
		return tipoPeriodoDaDuracaoDosSinais;
	}

	public void setTipoPeriodoDaDuracaoDosSinais(
			TipoPeriodo tipoPeriodoDaDuracaoDosSinais) {
		this.tipoPeriodoDaDuracaoDosSinais = tipoPeriodoDaDuracaoDosSinais;
	}

	public Calendar getDataUltimaVacina() {
		return dataUltimaVacina;
	}

	public void setDataUltimaVacina(Calendar dataUltimaVacina) {
		this.dataUltimaVacina = dataUltimaVacina;
	}

	public Long getIdAmostra() {
		return idAmostra;
	}

	public void setIdAmostra(Long idAmostra) {
		this.idAmostra = idAmostra;
	}

	public List<InformacoesComplementaresEspeciesFormLAB> getInformacoesComplementaresEspeciesFormLAB() {
		return informacoesComplementaresEspeciesFormLAB;
	}

	public void setInformacoesComplementaresEspeciesFormLAB(
			List<InformacoesComplementaresEspeciesFormLAB> informacoesComplementaresEspeciesFormLAB) {
		this.informacoesComplementaresEspeciesFormLAB = informacoesComplementaresEspeciesFormLAB;
	}

	public List<InformacoesComplementaresMedicamentosFormLAB> getInformacoesComplementaresMedicamentosFormLAB() {
		return informacoesComplementaresMedicamentosFormLAB;
	}

	public void setInformacoesComplementaresMedicamentosFormLAB(
			List<InformacoesComplementaresMedicamentosFormLAB> informacoesComplementaresMedicamentosFormLAB) {
		this.informacoesComplementaresMedicamentosFormLAB = informacoesComplementaresMedicamentosFormLAB;
	}

	public TipoPeriodo getTipoPeriodoIdade() {
		return tipoPeriodoIdade;
	}

	public void setTipoPeriodoIdade(TipoPeriodo tipoPeriodoIdade) {
		this.tipoPeriodoIdade = tipoPeriodoIdade;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbstractAmostra other = (AbstractAmostra) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
