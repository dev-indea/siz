package br.gov.mt.indea.sistemaformin.webservice.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class SituacaoFundiaria implements Serializable {

	private static final long serialVersionUID = 964348667461100706L;

	private Long idTable;
	
    private String id;
    
    private String validade;
    
    private String nome;

    public SituacaoFundiaria() {
    }

    public SituacaoFundiaria(String id, String nome, String validade) {
        super();
        this.id = id;
        this.nome = nome;
        this.validade = validade;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getValidade() {
        return validade;
    }

    public void setValidade(String validade) {
        this.validade = validade;
    }

	public Long getIdTable() {
		return idTable;
	}

	public void setIdTable(Long idTable) {
		this.idTable = idTable;
	}
}