package br.gov.mt.indea.sistemaformin.webservice.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ULE implements Serializable {

	private static final long serialVersionUID = -2928539765437467481L;

	private Long id;
    
    private String nome;
    
    @XmlElement(name = "urs")
    private ULE urs;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
    	if (nome != null)
    		return nome.replace("ULE ", "").replace("REGIONAL ", "");
    	else
    		return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public ULE getUrs() {
        return urs;
    }

    public void setUrs(ULE urs) {
        this.urs = urs;
    }
    
}