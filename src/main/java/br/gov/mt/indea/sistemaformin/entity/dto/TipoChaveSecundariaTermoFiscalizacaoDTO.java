package br.gov.mt.indea.sistemaformin.entity.dto;

import java.io.Serializable;

import br.gov.mt.indea.sistemaformin.entity.TipoChavePrincipalTermoFiscalizacao;

public class TipoChaveSecundariaTermoFiscalizacaoDTO implements AbstractDTO, Serializable {

	private static final long serialVersionUID = 958534186952468759L;

	private TipoChavePrincipalTermoFiscalizacao tipoChavePrincipalTermoFiscalizacao;

	public boolean isNull(){
		if (tipoChavePrincipalTermoFiscalizacao == null)
			return true;
		return false;
	}

	public TipoChavePrincipalTermoFiscalizacao getTipoChavePrincipalTermoFiscalizacao() {
		return tipoChavePrincipalTermoFiscalizacao;
	}

	public void setTipoChavePrincipalTermoFiscalizacao(
			TipoChavePrincipalTermoFiscalizacao tipoChavePrincipalTermoFiscalizacao) {
		this.tipoChavePrincipalTermoFiscalizacao = tipoChavePrincipalTermoFiscalizacao;
	}

}
