package br.gov.mt.indea.sistemaformin.managedbeans;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLBeanName;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;

import br.gov.mt.indea.sistemaformin.entity.FormCOM;
import br.gov.mt.indea.sistemaformin.entity.FormIN;
import br.gov.mt.indea.sistemaformin.entity.Municipio;
import br.gov.mt.indea.sistemaformin.entity.Resenho;
import br.gov.mt.indea.sistemaformin.entity.UF;
import br.gov.mt.indea.sistemaformin.enums.Dominio.SimNao;
import br.gov.mt.indea.sistemaformin.exception.ApplicationErrorException;
import br.gov.mt.indea.sistemaformin.exception.ApplicationException;
import br.gov.mt.indea.sistemaformin.service.FormCOMService;
import br.gov.mt.indea.sistemaformin.service.FormINService;
import br.gov.mt.indea.sistemaformin.service.MunicipioService;
import br.gov.mt.indea.sistemaformin.service.ResenhoService;
import br.gov.mt.indea.sistemaformin.util.FacesMessageUtil;
import br.gov.mt.indea.sistemaformin.webservice.client.ClientWebServiceVeterinario;
import br.gov.mt.indea.sistemaformin.webservice.entity.Endereco;
import br.gov.mt.indea.sistemaformin.webservice.entity.Propriedade;
import br.gov.mt.indea.sistemaformin.webservice.entity.Veterinario;

@Named
@ViewScoped
@URLBeanName("resenhoManagedBean")
@URLMappings(mappings = {
		@URLMapping(id = "pesquisarResenho", pattern = "/formIN/#{resenhoManagedBean.idFormIN}/resenho/pesquisar", viewId = "/pages/resenho/lista.jsf"),
		@URLMapping(id = "incluirResenho", pattern = "/formIN/#{resenhoManagedBean.idFormIN}/resenho/incluir", viewId = "/pages/resenho/novo.jsf"),
		@URLMapping(id = "alterarResenho", pattern = "/formIN/#{resenhoManagedBean.idFormIN}/resenho/alterar/#{resenhoManagedBean.id}", viewId = "/pages/resenho/novo.jsf"),
		@URLMapping(id = "impressaoResenho", pattern = "/formIN/#{resenhoManagedBean.idFormIN}/resenho/impressao/#{resenhoManagedBean.id}", viewId = "/pages/resenho/novo.jsf")})
public class ResenhoManagedBean implements Serializable {

	private static final long serialVersionUID = 1760023349204758559L;

	private Long id;
	
	private Long idFormIN;
	
	private FormIN formIN;
	
	@Inject
	private FormINService formINService;
	
	@Inject
	private ResenhoService resenhoService;
	
	@Inject
	private FormCOMService formCOMService;
	
	@Inject
	private MunicipioService municipioService;
	
	@Inject
	private Resenho resenho;
	
	private String cpfVeterinario;

	private String crmvVeterinario;

	private List<Veterinario> listaVeterinarios;
	
	@Inject
	private ClientWebServiceVeterinario clientWebServiceVeterinario;
	
	private SimNao propriedadeOndeOAnimalSeEncontraEAMesmaDoFormIN = SimNao.SIM;
	
	private UF uf;
	
	public List<Municipio> getListaMunicipios(){
		if (this.uf == null)
			return null;
		
		return municipioService.findAllByUF(this.uf);
	}
	
	@PostConstruct
	private void init(){
		
	}
	
	private void carregarFormIN(){
		this.formIN = formINService.findByIdFetchPropriedade(this.idFormIN);
	}
	
	private void limpar() {
		this.resenho = new Resenho();
	}
	
	@URLAction(mappingId = "incluirResenho", onPostback = false)
	public void novo(){
		this.limpar();
		this.carregarFormIN();
		
		this.resenho.setFormIN(formIN);	
		this.resenho.setPropriedade(this.resenho.getFormIN().getPropriedade());
	}
	
	@URLAction(mappingId = "alterarResenho", onPostback = false)
	public void editar(){
		this.resenho = resenhoService.findByIdFetchAll(this.getId());
		
//		if (this.resenho.getPropriedade().getCodigoPropriedade().equals(this.resenho.getFormIN().getPropriedade().getCodigoPropriedade())){
//			this.propriedadeOndeOAnimalSeEncontraEAMesmaDoFormIN = SimNao.SIM;
//		}else{
//			this.propriedadeOndeOAnimalSeEncontraEAMesmaDoFormIN = SimNao.NAO;
//			this.uf = this.resenho.getPropriedade().getMunicipio().getUf();
//		}
	}
	
	public String adicionar() throws IOException{
		
		if (this.resenho.getVeterinario() == null){
			FacesContext context = FacesContext.getCurrentInstance();
			
			context.addMessage("cadastro:fieldVeterinario:campoVeterinario", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Veterin�rio: valor � obrigat�rio.", "Veterin�rio: valor � obrigat�rio."));
			return "";
		}
		
		if (resenho.getId() != null){
			this.resenhoService.saveOrUpdate(resenho);
			FacesMessageUtil.addInfoContextFacesMessage("Resenho atualizado", "");
		}else{
			this.resenho.setDataCadastro(Calendar.getInstance());
			this.resenhoService.saveOrUpdate(resenho);
			FacesMessageUtil.addInfoContextFacesMessage("Resenho adicionado", "");
		}
		
		limpar();
	    return "pretty:visaoGeralFormIN";
	}
	
	@Inject
	private VisualizadorImpressaoManagedBean visualizadorImpressaoManagedBean;
	
	@URLAction(mappingId = "impressaoResenho", onPostback = false)
	public void imprimir() throws ApplicationErrorException{
		this.resenho = resenhoService.findByIdFetchAll(this.getId());
		
		visualizadorImpressaoManagedBean.exibirResenho(resenho);
	}
	
	public void remover(Resenho resenho){
		this.resenhoService.delete(resenho);
		FacesMessageUtil.addInfoContextFacesMessage("Resenho exclu�do com sucesso", "");
	}
	
	public List<FormCOM> getListaFormCOM(){
		return formCOMService.findAllBy(this.resenho.getFormIN());
	}
	
	public void abrirTelaDeBuscaDeVeterinario(){
		this.cpfVeterinario = null;
		this.crmvVeterinario = null;
		this.listaVeterinarios = null;
	}
	
	public void buscarVeterinarioPorCpf(){
		this.listaVeterinarios = null;
		
		Veterinario v;
		try {
			v = clientWebServiceVeterinario.getVeterinarioByCpf(cpfVeterinario);
			
			if (v != null){
				this.listaVeterinarios = new ArrayList<Veterinario>();
				this.listaVeterinarios.add(v);
			}
		} catch (ApplicationException e) {
			FacesMessageUtil.addErrorContextFacesMessage("Erro", e.getLocalizedMessage());
			return;
		}
		
		if (this.listaVeterinarios == null || this.listaVeterinarios.isEmpty())
			FacesMessageUtil.addWarnContextFacesMessage("Nenhum veterin�rio foi encontrado", "");
	}
	
	public void buscarVeterinarioPorCrmv(){
		this.listaVeterinarios = null;
		
		Veterinario v;
		try {
			v = clientWebServiceVeterinario.getVeterinarioByCRMV(crmvVeterinario);
			
			if (v != null){
				this.listaVeterinarios = new ArrayList<Veterinario>();
				this.listaVeterinarios.add(v);
			}
		} catch (ApplicationException e) {
			FacesMessageUtil.addErrorContextFacesMessage("Erro", e.getLocalizedMessage());
			return;
		}
		
		if (this.listaVeterinarios == null || this.listaVeterinarios.isEmpty())
			FacesMessageUtil.addWarnContextFacesMessage("Nenhum veterin�rio foi encontrado", "");
	}
	
	public void selecionarVeterinario(Veterinario veterinario){
		this.resenho.setVeterinario(veterinario);
		FacesMessageUtil.addInfoContextFacesMessage("Veterinario " + this.resenho.getVeterinario().getNome() + " selecionado", "");
	}
	
	public void alterarPropriedade(){
		if (this.propriedadeOndeOAnimalSeEncontraEAMesmaDoFormIN.equals(SimNao.SIM))
			this.resenho.setPropriedade(this.resenho.getFormIN().getPropriedade());
		else{
			this.resenho.setPropriedade(new Propriedade());
			this.resenho.getPropriedade().setEndereco(new Endereco());
		}
		
	}
	
	public Resenho getResenho() {
		return resenho;
	}

	public void setResenho(Resenho resenho) {
		this.resenho = resenho;
	}

	public String getCpfVeterinario() {
		return cpfVeterinario;
	}

	public void setCpfVeterinario(String cpfVeterinario) {
		this.cpfVeterinario = cpfVeterinario;
	}

	public String getCrmvVeterinario() {
		return crmvVeterinario;
	}

	public void setCrmvVeterinario(String crmvVeterinario) {
		this.crmvVeterinario = crmvVeterinario;
	}

	public List<Veterinario> getListaVeterinarios() {
		return listaVeterinarios;
	}

	public void setListaVeterinarios(List<Veterinario> listaVeterinarios) {
		this.listaVeterinarios = listaVeterinarios;
	}

	public SimNao getPropriedadeOndeOAnimalSeEncontraEAMesmaDoFormIN() {
		return propriedadeOndeOAnimalSeEncontraEAMesmaDoFormIN;
	}

	public void setPropriedadeOndeOAnimalSeEncontraEAMesmaDoFormIN(
			SimNao propriedadeOndeOAnimalSeEncontraEAMesmaDoFormIN) {
		this.propriedadeOndeOAnimalSeEncontraEAMesmaDoFormIN = propriedadeOndeOAnimalSeEncontraEAMesmaDoFormIN;
	}
	
	public boolean isPropriedadeOndeOAnimalSeEncontraEAMesmaDoFormIN_SIM(){
		return propriedadeOndeOAnimalSeEncontraEAMesmaDoFormIN.equals(SimNao.SIM);
	}

	public UF getUf() {
		return uf;
	}

	public void setUf(UF uf) {
		this.uf = uf;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdFormIN() {
		return idFormIN;
	}

	public void setIdFormIN(Long idFormIN) {
		this.idFormIN = idFormIN;
	}

	public FormIN getFormIN() {
		return formIN;
	}

	public void setFormIN(FormIN formIN) {
		this.formIN = formIN;
	}

}
