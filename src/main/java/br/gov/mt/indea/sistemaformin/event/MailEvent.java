package br.gov.mt.indea.sistemaformin.event;

import java.util.Map;

import javax.activation.FileDataSource;

public class MailEvent {
	
	private String destinatario;
	
	private String assunto;
	
	private String mensagem;
	
	private String template;
	
	private Map<String, FileDataSource> listaAnexo;

	public String getDestinatario() {
		return destinatario;
	}

	public void setDestinatario(String destinatario) {
		this.destinatario = destinatario;
	}

	public String getAssunto() {
		return assunto;
	}

	public void setAssunto(String assunto) {
		this.assunto = assunto;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public String getTemplate() {
		return template;
	}

	public void setTemplate(String template) {
		this.template = template;
	}

	public Map<String, FileDataSource> getListaAnexo() {
		return listaAnexo;
	}

	public void setListaAnexo(Map<String, FileDataSource> listaAnexo) {
		this.listaAnexo = listaAnexo;
	}
}
