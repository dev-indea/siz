package br.gov.mt.indea.sistemaformin.service;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.mt.indea.sistemaformin.entity.FolhaAdicional;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class FolhaAdicionalService extends PaginableService<FolhaAdicional, Long> {

	private static final Logger log = LoggerFactory.getLogger(FolhaAdicionalService.class);
	
	protected FolhaAdicionalService() {
		super(FolhaAdicional.class);
	}
	
	public FolhaAdicional findByIdFetchAll(Long id){
		FolhaAdicional folhaAdicional;
		
		StringBuilder sql = new StringBuilder();
		sql.append("select folhaAdicional ")
		   .append("  from FolhaAdicional folhaAdicional ")
		   .append("  join fetch folhaAdicional.formIN formIN ")
		   .append("  join fetch formIN.propriedade propriedade")
		   .append("  join fetch propriedade.municipio municipio ")
		   .append("  join fetch municipio.uf ")
		   .append(" where folhaAdicional.id = :id ");
		
		Query query = getSession().createQuery(sql.toString()).setLong("id", id);
		folhaAdicional = (FolhaAdicional) query.uniqueResult();
		
		return folhaAdicional;
	}
	
	@Override
	public void saveOrUpdate(FolhaAdicional folhaAdicional) {
		super.saveOrUpdate(folhaAdicional);
		
		log.info("Salvando Folha Adicional {}", folhaAdicional.getId());
	}
	
	@Override
	public void delete(FolhaAdicional folhaAdicional) {
		super.delete(folhaAdicional);
		
		log.info("Removendo Folha Adicional {}", folhaAdicional.getId());
	}
	
	@Override
	public void validar(FolhaAdicional FolhaAdicional) {

	}

	@Override
	public void validarPersist(FolhaAdicional FolhaAdicional) {

	}

	@Override
	public void validarMerge(FolhaAdicional FolhaAdicional) {

	}

	@Override
	public void validarDelete(FolhaAdicional FolhaAdicional) {

	}

}
