package br.gov.mt.indea.sistemaformin.webservice.model;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Propriedade implements Serializable {
   
	private static final long serialVersionUID = 7723487750664560020L;

	private Long id;
    
    private String tipoPessoa;
    
    private String cpfCnpj;
    
    private String nome;
    
    private String apelido;
    
    private String email;
    
    @XmlElement(name = "endereco")
    private Endereco endereco;
    
    private PessoaFisica pessoaFisica;

    private Integer codigoPropriedade;
    
    private String codigo;
    
    @XmlElement(name = "ule")
    private ULE ule;

    @XmlElement(name = "tipoRegiao")
    private TipoRegiao tipoRegiao;
    
    @XmlElement(name = "setor")
    private Setor setor;
    
    private Municipio municipio = new Municipio();
  
    @XmlElement(name = "assentamento")
    private Assentamento assentamento;
   
    @XmlElement(name = "coordenadaGeograficas")
    private List<CoordenadaGeografica> coordenadaGeograficas;
  
    private String viaAcesso;
  
    private String enderecoPropriedade;
  
    private String complemnto;
    
    @XmlElement(name = "proprietarios")
    private List<ProprietarioRural> proprietarios;
    
    @XmlElement(name = "exploracaos")
    private List<Exploracao> exploracaos;
    
    public Propriedade() {
    }
    
    public String getProprietariosAsString(){
    	StringBuilder sb = null;
		for (ProprietarioRural proprietario : proprietarios) {
			if (sb == null){
				sb = new StringBuilder();
				sb.append(proprietario.getProprietario().getNome());
			}else
				sb.append(", ").append(proprietario.getProprietario().getNome());
		}
		
		return sb.toString();
    }

    public Integer getCodigoPropriedade() {
		return codigoPropriedade;
	}

	public void setCodigoPropriedade(Integer codigoPropriedade) {
		this.codigoPropriedade = codigoPropriedade;
	}

    public String getEnderecoPropriedade() {
        return enderecoPropriedade;
    }

	public List<CoordenadaGeografica> getCoordenadaGeograficas() {
		return coordenadaGeograficas;
	}

	public void setCoordenadaGeograficas(
			List<CoordenadaGeografica> coordenadaGeograficas) {
		this.coordenadaGeograficas = coordenadaGeograficas;
	}

	public List<ProprietarioRural> getProprietarios() {
        return proprietarios;
    }

    public void setProprietarios(List<ProprietarioRural> proprietarios) {
        this.proprietarios = proprietarios;
    }

    public void setEnderecoPropriedade(String enderecoPropriedade) {
        this.enderecoPropriedade = enderecoPropriedade;
    }
    
    public ULE getUle() {
        return ule;
    }

    public void setUle(ULE ule) {
        this.ule = ule;
    }

    public TipoRegiao getTipoRegiao() {
        return tipoRegiao;
    }

    public void setTipoRegiao(TipoRegiao tipoRegiao) {
        this.tipoRegiao = tipoRegiao;
    }

    public Setor getSetor() {
        return setor;
    }

    public void setSetor(Setor setor) {
        this.setor = setor;
    }

    public Municipio getMunicipio() {
        return municipio;
    }

    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }

    public Assentamento getAssentamento() {
        return assentamento;
    }

    public void setAssentamento(Assentamento assentamento) {
        this.assentamento = assentamento;
    }

    public String getViaAcesso() {
        return viaAcesso;
    }

    public void setViaAcesso(String viaAcesso) {
        this.viaAcesso = viaAcesso;
    }

    public String getComplemnto() {
        return complemnto;
    }

    public void setComplemnto(String complemnto) {
        this.complemnto = complemnto;
    }

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public List<Exploracao> getExploracaos() {
		return exploracaos;
	}

	public void setExploracaos(List<Exploracao> exploracaos) {
		this.exploracaos = exploracaos;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTipoPessoa() {
		return tipoPessoa;
	}

	public void setTipoPessoa(String tipoPessoa) {
		this.tipoPessoa = tipoPessoa;
	}

	public String getCpfCnpj() {
		return cpfCnpj;
	}

	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getApelido() {
		return apelido;
	}

	public void setApelido(String apelido) {
		this.apelido = apelido;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public PessoaFisica getPessoaFisica() {
		return pessoaFisica;
	}

	public void setPessoaFisica(PessoaFisica pessoaFisica) {
		this.pessoaFisica = pessoaFisica;
	}

}