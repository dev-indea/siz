package br.gov.mt.indea.sistemaformin.service;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.hibernate.Query;

import br.gov.mt.indea.sistemaformin.entity.Pais;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class PaisService extends PaginableService<Pais, Long> {

	protected PaisService() {
		super(Pais.class);
	}
	
	public Pais findByIdFetchAll(Long id){
		Pais pais;
		
		StringBuilder sql = new StringBuilder();
		sql.append("select pais ")
		   .append("  from Pais pais ")
		   .append(" where pais.id = :id ");
		
		Query query = getSession().createQuery(sql.toString()).setLong("id", id);
		pais = (Pais) query.uniqueResult();
		
		return pais;
	}
	
	public Pais findByNomeFetchAll(String nome){
		Pais pais;
		
		StringBuilder sql = new StringBuilder();
		sql.append("select pais ")
		   .append("  from Pais pais ")
		   .append(" where pais.nome = :nome ");
		
		Query query = getSession().createQuery(sql.toString()).setString("nome", nome);
		pais = (Pais) query.uniqueResult();
		
		return pais;
	}
	
	@Override
	public void validar(Pais Pais) {

	}

	@Override
	public void validarPersist(Pais Pais) {

	}

	@Override
	public void validarMerge(Pais Pais) {

	}

	@Override
	public void validarDelete(Pais Pais) {

	}

}
