package br.gov.mt.indea.sistemaformin.managedbeans;

import java.io.IOException;
import java.io.Serializable;
import java.util.Calendar;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLBeanName;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;

import br.gov.mt.indea.sistemaformin.entity.Universidade;
import br.gov.mt.indea.sistemaformin.entity.Pessoa;
import br.gov.mt.indea.sistemaformin.entity.dto.UniversidadeDTO;
import br.gov.mt.indea.sistemaformin.service.UniversidadeService;
import br.gov.mt.indea.sistemaformin.service.LazyObjectDataModel;
import br.gov.mt.indea.sistemaformin.util.FacesMessageUtil;

@Named
@ViewScoped
@URLBeanName("universidadeManagedBean")
@URLMappings(mappings = {
		@URLMapping(id = "pesquisarUniversidade", pattern = "/universidade/pesquisar", viewId = "/pages/universidade/lista.jsf"),
		@URLMapping(id = "incluirUniversidade", pattern = "/universidade/incluir", viewId = "/pages/universidade/novo.jsf"),
		@URLMapping(id = "alterarUniversidade", pattern = "/universidade/alterar/#{universidadeManagedBean.id}", viewId = "/pages/universidade/novo.jsf")})
public class UniversidadeManagedBean extends PessoaManagedBean implements Serializable {

	private static final long serialVersionUID = 5401332308697941357L;
	
	private Long id;
	
	@Inject
	private Universidade universidade;
	
	@Inject
	private UniversidadeDTO universidadeDTO;
	
	private LazyObjectDataModel<Universidade> listaUniversidade;
	
	@Inject
	private UniversidadeService universidadeService;
	
	@PostConstruct
	public void init(){
		super.init();
	}
	
	@Override
	public Pessoa getPessoa() {
		return this.universidade;
	}

	@Override
	public void setPessoa(Pessoa pessoa) {
		this.universidade = (Universidade) pessoa;
	}
	
	public void limpar(){
		this.universidade = new Universidade();
	}
	
	public void buscarUniversidades(){
		this.listaUniversidade = new LazyObjectDataModel<Universidade>(this.universidadeService, this.universidadeDTO);
	}
	
	@URLAction(mappingId = "incluirUniversidade", onPostback = false)
	public void novo(){
		this.limpar();
	}
	
	@URLAction(mappingId = "pesquisarUniversidade", onPostback = false)
	public void pesquisar(){
		limpar();
	}

	@URLAction(mappingId = "alterarUniversidade", onPostback = false)
	public void editar(){
		this.universidade = universidadeService.findByIdFetchAll(this.getId());

		setUf(this.universidade.getEndereco().getMunicipio().getUf());
		setCep(this.universidade.getEndereco().getCep());
	}
	
	public String adicionar() throws IOException{
		if (universidade.getDataCadastro() != null){
			this.universidadeService.saveOrUpdate(universidade);
			FacesMessageUtil.addInfoContextFacesMessage("Universidade " + universidade.getNome() + " atualizada", "");
		}else{
			this.universidade.setDataCadastro(Calendar.getInstance());
			this.universidadeService.saveOrUpdate(universidade);
			FacesMessageUtil.addInfoContextFacesMessage("Universidade " + universidade.getNome() + " adicionada!", "");
		}
		
		return "pretty:pesquisarUniversidade";
	}
	
	public void remover(Universidade universidade){
		this.universidadeService.delete(universidade);
		FacesMessageUtil.addInfoContextFacesMessage("Universidade exclu�da com sucesso", "");
	}
	
	public Universidade getUniversidade() {
		return universidade;
	}

	public void setUniversidade(Universidade universidade) {
		this.universidade = universidade;
	}

	public LazyObjectDataModel<Universidade> getListaUniversidade() {
		return listaUniversidade;
	}

	public void setListaUniversidade(LazyObjectDataModel<Universidade> listaUniversidade) {
		this.listaUniversidade = listaUniversidade;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public UniversidadeDTO getUniversidadeDTO() {
		return universidadeDTO;
	}

	public void setUniversidadeDTO(UniversidadeDTO universidadeDTO) {
		this.universidadeDTO = universidadeDTO;
	}

}
