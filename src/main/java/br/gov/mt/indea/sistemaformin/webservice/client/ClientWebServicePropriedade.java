package br.gov.mt.indea.sistemaformin.webservice.client;

import java.io.Serializable;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.resource.spi.IllegalStateException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;

import br.gov.mt.indea.sistemaformin.exception.ApplicationException;
import br.gov.mt.indea.sistemaformin.util.StringUtil;
import br.gov.mt.indea.sistemaformin.webservice.convert.PropriedadeConverter;
import br.gov.mt.indea.sistemaformin.webservice.model.Propriedade;


public class ClientWebServicePropriedade implements Serializable{
	
	private static final long serialVersionUID = 1849076463695868949L;

	protected String URL_WS = "https://sistemas.indea.mt.gov.br/FronteiraWeb/ws/propriedade/";
	
//	@Inject
//	@Category("WEBSERVICE::PROPRIEDADE")
//	private CustomLogger log;
	
	@Inject
	private PropriedadeConverter propriedadeConverter;
	
	public br.gov.mt.indea.sistemaformin.webservice.entity.Propriedade getPropriedadeById(Long id) throws ApplicationException {
		
		try {
			id = Long.parseLong(this.format(id + ""));
		} catch (Exception e) {
			throw new ApplicationException(e);
		}
		
		if (id > Integer.MAX_VALUE)
			return null;

		String[] resposta = null;
		try {
			System.out.println(URL_WS + "id/" + id);
			resposta = new WebServiceCliente().get(URL_WS + "id/" + id);
		} catch (URISyntaxException e) {
			//log.excecao(e.getMessage(), e);
			throw new ApplicationException("Erro ao buscar a propriedade");
		}
		
		if (resposta == null || resposta[0] == null){
			return null;
		}
			
		if (resposta[0].equals("200")) {
			GsonBuilder gsonBuilder = new GsonBuilder().setDateFormat("dd/MM/yyyy");
			Gson gson = gsonBuilder.create();
			Propriedade propriedade = gson.fromJson(resposta[1], Propriedade.class);
			
			return propriedadeConverter.fromModelToEntity(propriedade);
		} else if (resposta[0].equals("404")) {
			//log.excecao(resposta[0] + ". " + resposta[1], null);
			throw new ApplicationException("Webservice com problemas. Contate o administrador do sistema");
		} else if (resposta[0].equals("500")) {
			//log.excecao(resposta[0] + ". " + resposta[1], null);
			throw new ApplicationException("Webservice com problemas. Contate o administrador do sistema");
		} else if (resposta[0].equals("400")) {
			//log.excecao(resposta[0] + ". " + resposta[1], null);
			throw new ApplicationException("Webservice com problemas. Contate o administrador do sistema");
		} else{
			//log.excecao(resposta[0] + ". " + resposta[1], null);
			throw new ApplicationException("Webservice inativo. Tente novamente em alguns minutos. Erro: " + resposta[0]);
		} 
	}
	
	public List<br.gov.mt.indea.sistemaformin.webservice.entity.Propriedade> getPropriedadeByNome(String nome) throws ApplicationException {
		
		nome = nome.replace(" ", "%20");
		nome = StringUtil.removeAcentos(nome);
		
		String[] resposta = null;
		try {
			System.out.println(URL_WS + "nome/" + nome);
			resposta = new WebServiceCliente().get(URL_WS + "nome/" + nome);
		} catch (URISyntaxException e) {
			//log.excecao(e.getMessage(), e);
			throw new ApplicationException("Erro ao buscar a propriedade");
		}
		
		if (resposta == null || resposta[0] == null){
			return null;
		}
			
		if (resposta[0].equals("200")) {
			GsonBuilder gsonBuilder = new GsonBuilder().setDateFormat("dd/MM/yyyy");
			Gson gson = gsonBuilder.create();
			
			List<br.gov.mt.indea.sistemaformin.webservice.entity.Propriedade> lista = new ArrayList<>();
			JsonParser parser = new JsonParser();
			JsonArray array = parser.parse(resposta[1]).getAsJsonArray();
			
			for (int i = 0; i < array.size(); i++) {
				lista.add(propriedadeConverter.fromModelToEntity(gson.fromJson(array.get(i), Propriedade.class)));
			}
			
			return lista;
		} else if (resposta[0].equals("404")) {
			//log.excecao(resposta[0] + ". " + resposta[1], null);
			throw new ApplicationException("Webservice com problemas. Contate o administrador do sistema");
		} else if (resposta[0].equals("500")) {
			//log.excecao(resposta[0] + ". " + resposta[1], null);
			throw new ApplicationException("Webservice com problemas. Contate o administrador do sistema");
		} else if (resposta[0].equals("400")) {
			//log.excecao(resposta[0] + ". " + resposta[1], null);
			throw new ApplicationException("Webservice com problemas. Contate o administrador do sistema");
		} else if (resposta[0].equals("0")) {
			//log.excecao(resposta[0] + ". " + resposta[1], null);
			throw new ApplicationException("A sua rede/internet parece estar com problemas. Contate o administrador do sistema");
		} else{
			//log.excecao(resposta[0] + ". " + resposta[1], null);
			throw new ApplicationException("Webservice inativo. Tente novamente em alguns minutos. Erro: " + resposta[0]);
		} 
	}
	
	private String format(String codigo) throws IllegalStateException{
		if (codigo == null)
			return null;
		
		if (codigo.length() == 11){
			if (codigo.startsWith("51")){
				codigo = codigo.replaceFirst("51", "");
				
				return format(codigo);
			}
		} else if (codigo.length() <= 9){
			if (codigo.startsWith("0")){
				codigo = codigo.replaceFirst("0", "");
				
				return format(codigo);
			} else
				return codigo;
		}
		
		throw new IllegalStateException("O c�digo da propriedade n�o est� formatado corretamente");
	}
	
}
