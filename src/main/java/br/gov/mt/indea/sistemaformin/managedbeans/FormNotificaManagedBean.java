package br.gov.mt.indea.sistemaformin.managedbeans;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLBeanName;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;

import br.gov.mt.indea.sistemaformin.entity.DetalheFormNotifica;
import br.gov.mt.indea.sistemaformin.entity.FormNotifica;
import br.gov.mt.indea.sistemaformin.entity.Municipio;
import br.gov.mt.indea.sistemaformin.entity.Notificante;
import br.gov.mt.indea.sistemaformin.entity.Pessoa;
import br.gov.mt.indea.sistemaformin.entity.SuspeitaEspecie;
import br.gov.mt.indea.sistemaformin.entity.TesteLaboratorial;
import br.gov.mt.indea.sistemaformin.entity.UF;
import br.gov.mt.indea.sistemaformin.entity.dto.FormNotificaDTO;
import br.gov.mt.indea.sistemaformin.enums.Dominio.AreaAtuacaoMedicoVeterinario;
import br.gov.mt.indea.sistemaformin.enums.Dominio.SimNao;
import br.gov.mt.indea.sistemaformin.exception.ApplicationException;
import br.gov.mt.indea.sistemaformin.service.FormNotificaService;
import br.gov.mt.indea.sistemaformin.service.LazyObjectDataModel;
import br.gov.mt.indea.sistemaformin.service.MunicipioService;
import br.gov.mt.indea.sistemaformin.service.PropriedadeService;
import br.gov.mt.indea.sistemaformin.service.UFService;
import br.gov.mt.indea.sistemaformin.util.FacesMessageUtil;
import br.gov.mt.indea.sistemaformin.util.StringUtil;
import br.gov.mt.indea.sistemaformin.webservice.entity.Endereco;
import br.gov.mt.indea.sistemaformin.webservice.entity.Propriedade;

@Named("formNotificaManagedBean")
@ViewScoped
@URLBeanName("formNotificaManagedBean")
@URLMappings(mappings = {
		@URLMapping(id = "pesquisarFormNotifica", pattern = "/formNotifica/pesquisar", viewId = "/pages/formNotifica/lista.jsf"),
		@URLMapping(id = "incluirFormNotifica", pattern = "/formNotifica/incluir", viewId = "/pages/formNotifica/novo.jsf"),
		@URLMapping(id = "alterarFormNotifica", pattern = "/formNotifica/alterar/#{formNotificaManagedBean.id}", viewId = "/pages/formNotifica/novo.jsf"),
		@URLMapping(id = "visualizarFormNotifica", pattern = "/formNotifica/visualizar/#{formNotificaManagedBean.id}", viewId = "/pages/formNotifica/novo.jsf")})
public class FormNotificaManagedBean implements Serializable {

	private static final long serialVersionUID = 1760023349204758559L;
	
	private Long id;
	
	private UploadedFile file;

	@Inject
	private FormNotificaService formNotificaService;
	
	@Inject
	private UFService ufService;
	
	@Inject
	private MunicipioService municipioService;
	
	@Inject
	private FormNotifica formNotifica;
	
	private LazyObjectDataModel<FormNotifica> listaFormNotifica;
	
	@Inject
	private FormNotificaDTO formNotificaDTO;
	
	private UF uf;
	
	@Inject
	private Municipio municipio;
	
	private boolean visualizando;
	
	private boolean editandoPropriedade = false;
	
	private boolean visualizandoPropriedade = false;
	
	private boolean editandoSuspeitaEspecie = false;
	
	private boolean visualizandoSuspeitaEspecie = false;
	
	private boolean editandoTesteLaboratorial = false;
	
	private boolean visualizandoTesteLaboratorial = false;
	
	private String idPropriedade;
	
	private String nomePropriedade;
	
	private DetalheFormNotifica detalheFormNotifica;

	private List<Propriedade> listaPropriedades;
	
	@Inject
	private Propriedade propriedade;
	
	private boolean forceUpdatePropriedade = false;
	
	@Inject
	private SuspeitaEspecie suspeitaEspecie;
	
	@Inject
	private TesteLaboratorial testeLaboratorial;
	
	private Date dataInicioDosSinaisClinicos;
	
	private Date dataResultado;
	
	@Inject
	private PropriedadeService propriedadeService;
	
	private SimNao anonimo;
	
	public void buscarFormNotifica(){
		this.listaFormNotifica = new LazyObjectDataModel<FormNotifica>(this.formNotificaService, this.formNotificaDTO);
	}
	
	public void carregarFormNotifica(){
		this.formNotifica = formNotificaService.findByIdFetchAll(this.id);
	}
	
	@URLAction(mappingId = "incluirFormNotifica", onPostback = false)
	public void novo(){
		limpar();
		this.visualizando = false;
		this.uf = ufService.findByIdFetchAll(UF.MATO_GROSSO.getId());
	}
	
	@URLAction(mappingId = "visualizarFormNotifica", onPostback = false)
	public void visualizar(){
		limpar();
		this.visualizando = true;
		
		this.carregarFormNotifica();
		
		if (this.formNotifica.getNotificante() != null)
			if (this.formNotifica.getNotificante().getInstituicao() != null)
				if (this.formNotifica.getNotificante().getInstituicao().getEndereco() != null)
					if (this.formNotifica.getNotificante().getInstituicao().getEndereco().getMunicipio() != null)
						this.uf = this.formNotifica.getNotificante().getInstituicao().getEndereco().getMunicipio().getUf();
	}
	
	@URLAction(mappingId = "pesquisarFormNotifica", onPostback = false)
	public void pesquisar(){
		limpar();
		
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		Long idFormNotifica = (Long) request.getSession().getAttribute("idFormNotifica");
		
		if (idFormNotifica != null){
			this.formNotificaDTO = new FormNotificaDTO();
			this.formNotificaDTO.setId(idFormNotifica);
			this.buscarFormNotifica();
			
			request.getSession().setAttribute("idFormNotifica", null);
		}
	}
	
	private void limpar() {
		this.formNotifica = new FormNotifica();
		this.dataInicioDosSinaisClinicos = null;
		this.dataResultado = null;
	}
	
	@URLAction(mappingId = "alterarFormNotifica", onPostback = false)
	public void editar(){
		this.carregarFormNotifica();
		
		if (this.formNotifica.getNotificante() != null){
			if (this.formNotifica.getNotificante().getInstituicao() != null)
				if (this.formNotifica.getNotificante().getInstituicao().getEndereco() != null)
					if (this.formNotifica.getNotificante().getInstituicao().getEndereco().getMunicipio() != null)
						this.uf = this.formNotifica.getNotificante().getInstituicao().getEndereco().getMunicipio().getUf();
			this.anonimo = SimNao.NAO;
		} else
			this.anonimo = SimNao.SIM;
	}
	
	public String adicionar() throws IOException{
		
		boolean isFormNotificaOK = true;
		
		if (this.formNotifica.getListaDetalheFormNotifica() == null || this.formNotifica.getListaDetalheFormNotifica().isEmpty()){
			FacesContext context = FacesContext.getCurrentInstance();
			
			context.addMessage("cadastro:tablePropriedades", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Estabelecimentos: deve ser adicionado ao menos um.", "Estabelecimentos: deve ser adicionado ao menos um."));
			isFormNotificaOK = false;;
		}
		
		if (!isFormNotificaOK)
			return "";
		
		if (this.formNotifica.getNotificante() != null && this.formNotifica.getNotificante().getFormNotifica() == null)
			this.formNotifica.getNotificante().setFormNotifica(this.formNotifica);
		
		if (this.formNotifica.getSuspeitaDoencaAnimal().getFormNotifica() == null)
			this.formNotifica.getSuspeitaDoencaAnimal().setFormNotifica(this.formNotifica);
		
		if (formNotifica.getId() != null){
			this.formNotificaService.saveOrUpdate(formNotifica);

			FacesMessageUtil.addInfoContextFacesMessage("Form Notifica atualizado", "");
		}else{
			this.formNotifica.setDataCadastro(Calendar.getInstance());
			this.formNotificaService.saveOrUpdate(formNotifica);

			FacesMessageUtil.addInfoContextFacesMessage("Form Notifica adicionado", "");
		}
		
		this.id = this.formNotifica.getId();
		
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		request.getSession().setAttribute("idFormNotifica", this.formNotifica.getId());
		
		limpar();
		
		return "pretty:pesquisarFormNotifica";
	}
	
	public void remover(FormNotifica formNotifica){
		this.formNotificaService.delete(formNotifica);
		FacesMessageUtil.addInfoContextFacesMessage("Form Notifica exclu�do com sucesso", "");
	}
	
	public List<Municipio> getListaMunicipiosBusca(){
		return municipioService.findAllByUF(this.formNotificaDTO.getUf());
	}
	
	public boolean isNotificanteAnonimo(){
		if (this.anonimo == null)
			return true;
		else if (this.anonimo.equals(SimNao.SIM)){
			this.formNotifica.setNotificante(null);
			return true;
		} else{
			this.formNotifica.setNotificante(new Notificante());
			this.formNotifica.getNotificante().setPessoa(new Pessoa());
			return false;
		}
	}
	
	public boolean isNotificanteMedicoVeterinario(){
		if (this.formNotifica == null)
			return false;
		else if (this.formNotifica.getNotificante() == null)
			return false;
		else if (this.formNotifica.getNotificante().getMedicoVeterinario() == null)
			return false;
		else if (this.formNotifica.getNotificante().getMedicoVeterinario().equals(SimNao.SIM)){
			return true;
		} else {
			return false;
		}
	}
	
	public boolean isAreaAtuacaoIgualAOutra(){
		if (this.formNotifica == null)
			return false;
		else if (this.formNotifica.getNotificante() == null)
			return false;
		else if (this.formNotifica.getNotificante().getAreaAtuacao() == null)
			return false;
		else if (this.formNotifica.getNotificante().getAreaAtuacao().equals(AreaAtuacaoMedicoVeterinario.OUTRA)){
			return true;
		} else {
			this.formNotifica.getNotificante().setOutraAreaAtuacao(null);
			return false;
		}
	}
	
	public boolean haTestesLaboratoriais(){
		if (this.formNotifica == null)
			return false;
		else if (this.formNotifica.getSuspeitaDoencaAnimal().getTesteLaboratorial() == null)
			return false;
		else if (this.formNotifica.getSuspeitaDoencaAnimal().getTesteLaboratorial().equals(SimNao.SIM))
			return true;
		else {
			this.formNotifica.getSuspeitaDoencaAnimal().setTestesLaboratoriais(new ArrayList<>());
			return false;
		}
	}
	
	public List<Municipio> getListaMunicipios(){
		if (this.uf == null)
			return null;
		
		return municipioService.findAllByUF(this.uf);
	}
	
	public void adicionarPropriedade(){
		boolean isPropriedadeOk = true;
		
		FacesContext context = FacesContext.getCurrentInstance();
		
		if (this.propriedade == null){
			context.addMessage("formModalPropriedade:fieldPropriedade:campoPropriedade", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Propriedade: valor � obrigat�rio.", "Propriedade: valor � obrigat�rio."));
			isPropriedadeOk = false;
		}
		
		if (!isPropriedadeOk){
			return;
		}
		
		if (!editandoPropriedade){
			this.detalheFormNotifica.setPropriedade(propriedade);
			this.formNotifica.getListaDetalheFormNotifica().add(detalheFormNotifica);
		} else
			editandoPropriedade = false;
		
		this.propriedade = new Propriedade();
		this.propriedade.setEndereco(new Endereco());
		FacesMessageUtil.addInfoContextFacesMessage("Estabelecimento inclu�do com sucesso", "");
	}
	
	public void novaPropriedade(){
		this.detalheFormNotifica = new DetalheFormNotifica();
		this.detalheFormNotifica.setFormNotifica(this.formNotifica);
		
		this.propriedade = new Propriedade();
		this.propriedade.setEndereco(new Endereco());
		this.editandoPropriedade = false;
		this.visualizandoPropriedade = false;
	}
	
	public void removerPropriedade(DetalheFormNotifica detalheFormNotifica){
		this.formNotifica.getListaDetalheFormNotifica().remove(detalheFormNotifica);
	}
	
	public void editarPropriedade(Propriedade propriedade){
		this.editandoPropriedade = true;
		this.visualizandoPropriedade = false;
		this.propriedade = propriedade;
		this.municipio = this.propriedade.getMunicipio();
		this.uf = this.municipio.getUf();
	}
	
	public void visualizarPropriedade(Propriedade propriedade){
		this.visualizandoPropriedade = true;
		this.editandoPropriedade = false;
		this.propriedade = propriedade;
	}
	
	public void abrirTelaDeBuscaDePropriedade(){
		this.idPropriedade = null;
		this.nomePropriedade = null;
		this.listaPropriedades = null;
	}
	
	public void buscarPropriedadeById(){
		this.listaPropriedades = null;
		
		Long idPropriedade;
		
		if (this.idPropriedade.length() == 11 && this.idPropriedade.startsWith("51")){
			idPropriedade = Long.parseLong(this.idPropriedade.replaceFirst("51", ""));
		} else
			idPropriedade = Long.parseLong(this.idPropriedade);
		
		Propriedade p;
		try {
			p = propriedadeService.findByCodigoFetchAll(idPropriedade, forceUpdatePropriedade);
			if (p != null){
				this.listaPropriedades = new ArrayList<Propriedade>();
				listaPropriedades.add(p);
			}
		} catch (ApplicationException e) {
			FacesMessageUtil.addErrorContextFacesMessage("Erro", e.getLocalizedMessage());
			return;
		}
		
		if (this.listaPropriedades == null || this.listaPropriedades.isEmpty())
			FacesMessageUtil.addWarnContextFacesMessage("Nenhuma propriedade foi encontrada", "");
	}
	
	public void buscarPropriedadeByNome(){
		this.listaPropriedades = null;
		
		try {
			this.listaPropriedades = propriedadeService.findByNomeFetchAll(StringUtil.removeAcentos(nomePropriedade).replace(" ", "%20"));
		} catch (ApplicationException e) {
			FacesMessageUtil.addErrorContextFacesMessage("Erro", e.getLocalizedMessage());
			return;
		}
		
		if (this.listaPropriedades == null || this.listaPropriedades.isEmpty())
			FacesMessageUtil.addWarnContextFacesMessage("Nenhuma propriedade foi encontrada", "");
	}
	
	public void selecionarPropriedade_NovoEstab(Propriedade propriedade){
		this.propriedade = propriedade;
		FacesMessageUtil.addInfoContextFacesMessage("Propriedade " + this.propriedade.getNome() + " selecionada", "");
	}
	
	public void abrirTelaDeCadastroDePropriedade(){
		this.propriedade = new Propriedade();
		this.propriedade.setEndereco(new Endereco());
	}
	
	public void cadastrarPropriedade_NovoEstab(){
		this.propriedade.setMunicipio(this.municipio);
		this.municipio = null;
		this.uf = null;
		
		FacesMessageUtil.addInfoContextFacesMessage("Propriedade inclu�da com sucesso", "");
	}
	
	public void adicionarSuspeita(){
		if (this.formNotifica.getSuspeitaDoencaAnimal().getSuspeitaEspecies() == null)
			this.formNotifica.getSuspeitaDoencaAnimal().setSuspeitaEspecies(new ArrayList<>());
		
		if (this.dataInicioDosSinaisClinicos != null){
			if (this.suspeitaEspecie.getDataInicioDosSinaisClinicos() == null)
				this.suspeitaEspecie.setDataInicioDosSinaisClinicos(Calendar.getInstance());
			this.suspeitaEspecie.getDataInicioDosSinaisClinicos().setTime(dataInicioDosSinaisClinicos);
		}else
			this.suspeitaEspecie.setDataInicioDosSinaisClinicos(null);
		
		if (this.suspeitaEspecie.getSuspeitaDoencaAnimal() == null)
			this.suspeitaEspecie.setSuspeitaDoencaAnimal(this.formNotifica.getSuspeitaDoencaAnimal());
		
		if (!editandoSuspeitaEspecie)
			this.formNotifica.getSuspeitaDoencaAnimal().getSuspeitaEspecies().add(suspeitaEspecie);
		else
			editandoSuspeitaEspecie = false;
		
		this.suspeitaEspecie = new SuspeitaEspecie();
		
		FacesMessageUtil.addInfoContextFacesMessage("Investiga��o inclu�da com sucesso", "");
	}
	
	public void novaSuspeita(){
		this.suspeitaEspecie= new SuspeitaEspecie();
		this.dataInicioDosSinaisClinicos = null;
		this.editandoSuspeitaEspecie = false;
		this.visualizandoSuspeitaEspecie = false;
	}
	
	public void removerSuspeita(SuspeitaEspecie suspeita){
		this.formNotifica.getSuspeitaDoencaAnimal().getSuspeitaEspecies().remove(suspeita);
	}
	
	public void editarSuspeita(SuspeitaEspecie suspeita){
		this.suspeitaEspecie = suspeita;
		
		if (this.suspeitaEspecie.getDataInicioDosSinaisClinicos() != null)
			this.dataInicioDosSinaisClinicos = this.suspeitaEspecie.getDataInicioDosSinaisClinicos().getTime();
		
		this.editandoSuspeitaEspecie = true;
		this.visualizandoSuspeitaEspecie = false;
		this.suspeitaEspecie = suspeita;
	}
	
	public void visualizarSuspeita(SuspeitaEspecie suspeita){
		this.suspeitaEspecie = suspeita;
		
		if (this.suspeitaEspecie.getDataInicioDosSinaisClinicos() != null)
			this.dataInicioDosSinaisClinicos = this.suspeitaEspecie.getDataInicioDosSinaisClinicos().getTime();
		
		this.visualizandoSuspeitaEspecie = true;
		this.editandoSuspeitaEspecie = false;
		this.suspeitaEspecie = suspeita;
	}
	
	public void adicionarTesteLaboratorial(){
		if (this.formNotifica.getSuspeitaDoencaAnimal().getTestesLaboratoriais() == null)
			this.formNotifica.getSuspeitaDoencaAnimal().setTestesLaboratoriais(new ArrayList<>());

		if (this.dataResultado != null){
			if (this.testeLaboratorial.getDataResultado() == null)
				this.testeLaboratorial.setDataResultado(Calendar.getInstance());
			this.testeLaboratorial.getDataResultado().setTime(dataResultado);
		}else
			this.testeLaboratorial.setDataResultado(null);
		
		if (this.testeLaboratorial.getSuspeitaDoencaAnimal() == null)
			this.testeLaboratorial.setSuspeitaDoencaAnimal(this.formNotifica.getSuspeitaDoencaAnimal());
		
		if (!editandoTesteLaboratorial)
			this.formNotifica.getSuspeitaDoencaAnimal().getTestesLaboratoriais().add(testeLaboratorial);
		else
			editandoTesteLaboratorial = false;
		
		this.testeLaboratorial = new TesteLaboratorial();
		
		FacesMessageUtil.addInfoContextFacesMessage("Investiga��o inclu�da com sucesso", "");
	}
	
	public void novaTesteLaboratorial(){
		this.testeLaboratorial= new TesteLaboratorial();
		this.dataResultado = null;
		this.editandoTesteLaboratorial = false;
		this.visualizandoTesteLaboratorial = false;
	}
	
	public void removerTesteLaboratorial(TesteLaboratorial testeLaboratorial){
		this.formNotifica.getSuspeitaDoencaAnimal().getTestesLaboratoriais().remove(testeLaboratorial);
	}
	
	public void selecionarTesteLaboratorial(TesteLaboratorial testeLaboratorial){
		this.testeLaboratorial = testeLaboratorial;
	}
	
	public void editarTesteLaboratorial(TesteLaboratorial testeLaboratorial){
		this.testeLaboratorial = testeLaboratorial;
		
		if (this.testeLaboratorial.getDataResultado() != null)
			this.dataResultado = this.testeLaboratorial.getDataResultado().getTime();
		
		this.editandoTesteLaboratorial = true;
		this.visualizandoTesteLaboratorial = false;
		this.testeLaboratorial = testeLaboratorial;
	}
	
	public void visualizarTesteLaboratorial(TesteLaboratorial testeLaboratorial){
		this.testeLaboratorial = testeLaboratorial;
		
		if (this.testeLaboratorial.getDataResultado() != null)
			this.dataResultado = this.testeLaboratorial.getDataResultado().getTime();
		
		this.visualizandoTesteLaboratorial = true;
		this.editandoTesteLaboratorial = false;
		this.testeLaboratorial = testeLaboratorial;
	}
	
	public void handleFileUpload(FileUploadEvent event) {
        FacesMessage message = new FacesMessage("Arquivo " + StringUtil.removeAcentos(event.getFile().getFileName()) + " salvo com sucesso", "");
        FacesContext.getCurrentInstance().addMessage(null, message);
        
        this.testeLaboratorial.setLaudo(event.getFile().getContents());
        this.testeLaboratorial.setLaudoName(event.getFile().getFileName());
        
        this.testeLaboratorial = null;
    }
	
	public FormNotifica getFormNotifica() {
		return formNotifica;
	}

	public void setFormNotifica(FormNotifica formNotifica) {
		this.formNotifica = formNotifica;
	}

	public LazyObjectDataModel<FormNotifica> getListaFormNotifica() {
		return listaFormNotifica;
	}

	public void setListaFormNotifica(LazyObjectDataModel<FormNotifica> listaFormNotifica) {
		this.listaFormNotifica = listaFormNotifica;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public UF getUf() {
		return uf;
	}

	public void setUf(UF uf) {
		this.uf = uf;
	}

	public Municipio getMunicipio() {
		return municipio;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}

	public boolean isEditandoPropriedade() {
		return editandoPropriedade;
	}

	public void setEditandoPropriedade(boolean editandoPropriedade) {
		this.editandoPropriedade = editandoPropriedade;
	}

	public boolean isVisualizandoPropriedade() {
		return visualizandoPropriedade;
	}

	public void setVisualizandoPropriedade(boolean visualizandoPropriedade) {
		this.visualizandoPropriedade = visualizandoPropriedade;
	}

	public String getIdPropriedade() {
		return idPropriedade;
	}

	public void setIdPropriedade(String idPropriedade) {
		this.idPropriedade = idPropriedade;
	}

	public List<Propriedade> getListaPropriedades() {
		return listaPropriedades;
	}

	public void setListaPropriedades(List<Propriedade> listaPropriedades) {
		this.listaPropriedades = listaPropriedades;
	}

	public Propriedade getPropriedade() {
		return propriedade;
	}

	public void setPropriedade(Propriedade propriedade) {
		this.propriedade = propriedade;
	}

	public String getNomePropriedade() {
		return nomePropriedade;
	}

	public void setNomePropriedade(String nomePropriedade) {
		this.nomePropriedade = nomePropriedade;
	}

	public SuspeitaEspecie getSuspeitaEspecie() {
		return suspeitaEspecie;
	}

	public void setSuspeitaEspecie(SuspeitaEspecie suspeitaEspecie) {
		this.suspeitaEspecie = suspeitaEspecie;
	}

	public boolean isEditandoSuspeitaEspecie() {
		return editandoSuspeitaEspecie;
	}

	public void setEditandoSuspeitaEspecie(boolean editandoSuspeitaEspecie) {
		this.editandoSuspeitaEspecie = editandoSuspeitaEspecie;
	}

	public boolean isVisualizandoSuspeitaEspecie() {
		return visualizandoSuspeitaEspecie;
	}

	public void setVisualizandoSuspeitaEspecie(boolean visualizandoSuspeitaEspecie) {
		this.visualizandoSuspeitaEspecie = visualizandoSuspeitaEspecie;
	}

	public Date getDataInicioDosSinaisClinicos() {
		return dataInicioDosSinaisClinicos;
	}

	public void setDataInicioDosSinaisClinicos(Date dataInicioDosSinaisClinicos) {
		this.dataInicioDosSinaisClinicos = dataInicioDosSinaisClinicos;
	}

	public TesteLaboratorial getTesteLaboratorial() {
		return testeLaboratorial;
	}

	public void setTesteLaboratorial(TesteLaboratorial testeLaboratorial) {
		this.testeLaboratorial = testeLaboratorial;
	}

	public boolean isEditandoTesteLaboratorial() {
		return editandoTesteLaboratorial;
	}

	public void setEditandoTesteLaboratorial(boolean editandoTesteLaboratorial) {
		this.editandoTesteLaboratorial = editandoTesteLaboratorial;
	}

	public boolean isVisualizandoTesteLaboratorial() {
		return visualizandoTesteLaboratorial;
	}

	public void setVisualizandoTesteLaboratorial(boolean visualizandoTesteLaboratorial) {
		this.visualizandoTesteLaboratorial = visualizandoTesteLaboratorial;
	}

	public Date getDataResultado() {
		return dataResultado;
	}

	public void setDataResultado(Date dataResultado) {
		this.dataResultado = dataResultado;
	}

	public boolean isVisualizando() {
		return visualizando;
	}

	public void setVisualizando(boolean visualizando) {
		this.visualizando = visualizando;
	}

	public UploadedFile getFile() {
		return file;
	}

	public void setFile(UploadedFile file) {
		this.file = file;
	}

	public DetalheFormNotifica getDetalheFormNotifica() {
		return detalheFormNotifica;
	}

	public void setDetalheFormNotifica(DetalheFormNotifica detalheFormNotifica) {
		this.detalheFormNotifica = detalheFormNotifica;
	}

	public FormNotificaDTO getFormNotificaDTO() {
		return formNotificaDTO;
	}

	public void setFormNotificaDTO(FormNotificaDTO formNotificaDTO) {
		this.formNotificaDTO = formNotificaDTO;
	}

	public SimNao getAnonimo() {
		return anonimo;
	}

	public void setAnonimo(SimNao anonimo) {
		this.anonimo = anonimo;
	}

	public boolean isForceUpdatePropriedade() {
		return forceUpdatePropriedade;
	}

	public void setForceUpdatePropriedade(boolean forceUpdatePropriedade) {
		this.forceUpdatePropriedade = forceUpdatePropriedade;
	}

}