package br.gov.mt.indea.sistemaformin.webservice.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Produtor extends Pessoa implements Serializable {

	private static final long serialVersionUID = -1122657944862190781L;

	private Long codigoProdutor;
    
    private String assSindicatoRural;
    
    private String coopRural;
    
    private String tvPreferencia;
    
    private String jornalPreferencia;
    
    private String radioPreferencia;

    public Produtor() {
    }

    public String getAssSindicatoRural() {
        return assSindicatoRural;
    }

    public void setAssSindicatoRural(String assSindicatoRural) {
        this.assSindicatoRural = assSindicatoRural;
    }

    public String getCoopRural() {
        return coopRural;
    }

    public void setCoopRural(String coopRural) {
        this.coopRural = coopRural;
    }

    public String getTvPreferencia() {
        return tvPreferencia;
    }

    public void setTvPreferencia(String tvPreferencia) {
        this.tvPreferencia = tvPreferencia;
    }

    public String getJornalPreferencia() {
        return jornalPreferencia;
    }

    public void setJornalPreferencia(String jornalPreferencia) {
        this.jornalPreferencia = jornalPreferencia;
    }

    public String getRadioPreferencia() {
        return radioPreferencia;
    }

    public void setRadioPreferencia(String radioPreferencia) {
        this.radioPreferencia = radioPreferencia;
    }

	public Long getCodigoProdutor() {
		return codigoProdutor;
	}

	public void setCodigoProdutor(Long codigoProdutor) {
		this.codigoProdutor = codigoProdutor;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((codigoProdutor == null) ? 0 : codigoProdutor.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Produtor other = (Produtor) obj;
		if (codigoProdutor == null) {
			if (other.codigoProdutor != null)
				return false;
		} else if (!codigoProdutor.equals(other.codigoProdutor))
			return false;
		return true;
	}

}