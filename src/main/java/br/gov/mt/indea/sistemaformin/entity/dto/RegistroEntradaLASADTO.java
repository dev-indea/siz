package br.gov.mt.indea.sistemaformin.entity.dto;

import java.io.Serializable;

import br.gov.mt.indea.sistemaformin.entity.Municipio;
import br.gov.mt.indea.sistemaformin.entity.UF;

public class RegistroEntradaLASADTO implements AbstractDTO, Serializable{
	
	private static final long serialVersionUID = 6205763755360605521L;

	private UF uf = UF.MATO_GROSSO;
	
	private Municipio municipio;
	
    private Long numero;
	
	public boolean isNull(){
		if (uf == null && municipio == null && numero == null )
			return true;
		return false;
	}
	
	public UF getUf() {
		return uf;
	}

	public void setUf(UF uf) {
		this.uf = uf;
	}

	public Municipio getMunicipio() {
		return municipio;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}

	public Long getNumero() {
		return numero;
	}

	public void setNumero(Long numero) {
		this.numero = numero;
	}

}