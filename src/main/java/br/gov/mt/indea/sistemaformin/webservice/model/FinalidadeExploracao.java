package br.gov.mt.indea.sistemaformin.webservice.model;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class FinalidadeExploracao implements Serializable {

	private static final long serialVersionUID = 5788846130554134237L;

	private Long id;
    
    private String nome;
   
    private List<GrupoEspecie> grupoEspecies;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

        public List<GrupoEspecie> getGrupoEspecies() {
        return grupoEspecies;
    }

    public void setGrupoEspecies(List<GrupoEspecie> grupoEspecies) {
        this.grupoEspecies = grupoEspecies;
    }
}