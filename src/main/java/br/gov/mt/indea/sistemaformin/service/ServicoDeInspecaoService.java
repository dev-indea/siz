package br.gov.mt.indea.sistemaformin.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.mt.indea.sistemaformin.entity.ServicoDeInspecao;
import br.gov.mt.indea.sistemaformin.util.StringUtil;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class ServicoDeInspecaoService extends PaginableService<ServicoDeInspecao, Long> {
	
	private static final Logger log = LoggerFactory.getLogger(ServicoDeInspecaoService.class);

	protected ServicoDeInspecaoService() {
		super(ServicoDeInspecao.class);
	}
	
	public ServicoDeInspecao findByIdFetchAll(Long id){
		ServicoDeInspecao servicoDeInspecao;
		
		StringBuilder sql = new StringBuilder();
		sql.append("  from ServicoDeInspecao servicoDeInspecao ")
		   .append("  join fetch servicoDeInspecao.abatedouro abatedouro")
		   .append("  join fetch abatedouro.municipio")
		   .append("  left join fetch abatedouro.coordenadaGeografica")
		   .append(" where servicoDeInspecao.id = :id ");
		
		Query query = getSession().createQuery(sql.toString()).setLong("id", id);
		servicoDeInspecao = (ServicoDeInspecao) query.uniqueResult();
		
		return servicoDeInspecao;
	}
	
	@SuppressWarnings("unchecked")
	public List<ServicoDeInspecao> findAll(String numero){
		StringBuilder sql = new StringBuilder();
		sql.append("  from ServicoDeInspecao servicoDeInspecao ")
		   .append("  join fetch servicoDeInspecao.abatedouro abatedouro")
		   .append("  join fetch abatedouro.municipio")
		   .append(" where lower(remove_acento(servicoDeInspecao.numero)) like :numero ")
		   .append(" order by servicoDeInspecao.numero");
		
		Query query = getSession().createQuery(sql.toString()).setString("numero", '%' + StringUtil.removeAcentos(numero.toLowerCase() + '%'));
		
		return query.list();
	}
	
	@Override
	public void saveOrUpdate(ServicoDeInspecao servicoDeInspecao) {
		servicoDeInspecao = (ServicoDeInspecao) this.getSession().merge(servicoDeInspecao);
		
		super.saveOrUpdate(servicoDeInspecao);
		
		log.info("Salvando ServicoDeInspecao {}", servicoDeInspecao.getId());
	}
	
	@Override
	public void delete(ServicoDeInspecao servicoDeInspecao) {
		super.delete(servicoDeInspecao);
		
		log.info("Removendo ServicoDeInspecao {}", servicoDeInspecao.getId());
	}
	
	@Override
	public void validar(ServicoDeInspecao ServicoDeInspecao) {

	}

	@Override
	public void validarPersist(ServicoDeInspecao ServicoDeInspecao) {

	}

	@Override
	public void validarMerge(ServicoDeInspecao ServicoDeInspecao) {

	}

	@Override
	public void validarDelete(ServicoDeInspecao ServicoDeInspecao) {

	}

}
