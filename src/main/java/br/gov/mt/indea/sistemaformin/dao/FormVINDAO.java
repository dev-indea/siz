package br.gov.mt.indea.sistemaformin.dao;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.inject.Inject;


import br.gov.mt.indea.sistemaformin.entity.FormVIN;
import br.gov.mt.indea.sistemaformin.exception.ApplicationException;

@Stateless
public class FormVINDAO extends DAO<FormVIN> {
	
	@Inject
	private EntityManager em;
	
	public FormVINDAO() {
		super(FormVIN.class);
	}

	@Override
	public EntityManager getEntityManager() {
		return this.em;
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void add(FormVIN formVIN) throws ApplicationException{
		formVIN = this.getEntityManager().merge(formVIN);
		super.add(formVIN);
	}

}
