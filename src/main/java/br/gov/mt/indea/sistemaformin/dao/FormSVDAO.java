package br.gov.mt.indea.sistemaformin.dao;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.inject.Inject;


import br.gov.mt.indea.sistemaformin.entity.FormSV;
import br.gov.mt.indea.sistemaformin.exception.ApplicationException;

@Stateless
public class FormSVDAO extends DAO<FormSV> {
	
	@Inject
	private EntityManager em;
	
	public FormSVDAO() {
		super(FormSV.class);
	}

	@Override
	public EntityManager getEntityManager() {
		return this.em;
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void add(FormSV formSV) throws ApplicationException{
		formSV = this.getEntityManager().merge(formSV);
		if (formSV.getFormCOM() != null)
			formSV.setFormCOM(this.getEntityManager().merge(formSV.getFormCOM()));
		super.add(formSV);
	}

}
