package br.gov.mt.indea.sistemaformin.security;

import java.util.ArrayList;
import java.util.Date;

import javax.enterprise.inject.Produces;
import javax.inject.Named;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import br.gov.mt.indea.sistemaformin.entity.Usuario;

@Named
public class SecurityBean {
	
	@Produces
	public UserSecurity getUserSecurity() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		
		Object principal = null;
		if (authentication != null)	
			principal = authentication.getPrincipal();
		else
			principal = new String(UserSecurity.ANONYMOUS_USER);
		
		UserSecurity userSecurity;
		if (principal != null && principal instanceof UserSecurity)
			userSecurity = (UserSecurity) principal;
		else{
			Usuario usuario = new Usuario();
			usuario.setId((String) principal);
			usuario.setNome((String) principal);
			usuario.setPassword((String) principal);
			usuario.setUltimoLogin(new Date());
			
			userSecurity = new UserSecurity(usuario, new ArrayList<GrantedAuthority>());
		}
		
		return userSecurity;
	}
	
}
