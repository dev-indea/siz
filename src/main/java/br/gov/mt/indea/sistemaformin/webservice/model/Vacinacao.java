package br.gov.mt.indea.sistemaformin.webservice.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Vacinacao implements Serializable {

	private static final long serialVersionUID = 9044460941441616984L;

	private Long id;
  
    @XmlElement(name = "doenca")
    private Doenca doenca;
  
    @XmlElement(name = "etapa")
    private Etapa etapa;
   
    @XmlElement(name = "tipoVacinacao")
    private TipoVacinacao tipoVacinacao;
   
    private String tipoComunicacao;
   
    private Date dataComunicacao;
  
    private Date dataVacina;
   
    @XmlElement(name = "notaFiscalVacinacao")
    private List<NotaFiscalVacinacao> notaFiscalVacinacao;

    public Vacinacao() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Doenca getDoenca() {
        return doenca;
    }

    public void setDoenca(Doenca doenca) {
        this.doenca = doenca;
    }

    public String getTipoComunicacao() {
        return tipoComunicacao;
    }

    public void setTipoComunicacao(String tipoComunicacao) {
        this.tipoComunicacao = tipoComunicacao;
    }

    public Date getDataComunicacao() {
        return dataComunicacao;
    }

    public void setDataComunicacao(Date dataComunicacao) {
        this.dataComunicacao = dataComunicacao;
    }

    public Date getDataVacina() {
        return dataVacina;
    }

    public void setDataVacina(Date dataVacina) {
        this.dataVacina = dataVacina;
    }

    public List<NotaFiscalVacinacao> getNotaFiscalVacinacao() {
        return notaFiscalVacinacao;
    }

    public void setNotaFiscalVacinacao(List<NotaFiscalVacinacao> notaFiscalVacinacao) {
        this.notaFiscalVacinacao = notaFiscalVacinacao;
    }

    public TipoVacinacao getTipoVacinacao() {
        return tipoVacinacao;
    }

    public void setTipoVacinacao(TipoVacinacao tipoVacinacao) {
        this.tipoVacinacao = tipoVacinacao;
    }

    public Etapa getEtapa() {
        return etapa;
    }

    public void setEtapa(Etapa etapa) {
        this.etapa = etapa;
    }
    
}