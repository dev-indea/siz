package br.gov.mt.indea.sistemaformin.xlsximport;

import java.io.Serializable;

public class ErroImportacao implements Serializable {

	private static final long serialVersionUID = -8318930970473574250L;

	int linha;

	String coluna;

	Object valor;

	TipoErro tipoErro;

	public ErroImportacao(int linha, String coluna, Object valor, TipoErro tipoErro) {
		this.linha = linha;
		this.coluna = coluna;
		this.valor = valor;
		this.tipoErro = tipoErro;
	}

	public int getLinha() {
		return linha;
	}

	public String getColuna() {
		return coluna;
	}

	public Object getValor() {
		return valor;
	}

	public TipoErro getTipoErro() {
		return tipoErro;
	}

	public void setLinha(int linha) {
		this.linha = linha;
	}

	public void setColuna(String coluna) {
		this.coluna = coluna;
	}

	public void setValor(Object valor) {
		this.valor = valor;
	}

	public void setTipoErro(TipoErro tipoErro) {
		this.tipoErro = tipoErro;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((coluna == null) ? 0 : coluna.hashCode());
		result = prime * result + linha;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ErroImportacao other = (ErroImportacao) obj;
		if (coluna == null) {
			if (other.coluna != null)
				return false;
		} else if (!coluna.equals(other.coluna))
			return false;
		if (linha != other.linha)
			return false;
		return true;
	}

}
