package br.gov.mt.indea.sistemaformin.entity.dto;

import java.io.Serializable;

import org.springframework.util.ObjectUtils;

import br.gov.mt.indea.sistemaformin.entity.Municipio;
import br.gov.mt.indea.sistemaformin.entity.ServicoDeInspecao;
import br.gov.mt.indea.sistemaformin.entity.UF;

public class ServicoDeInspecaoDTO implements AbstractDTO, Serializable{
	
	private static final long serialVersionUID = 6205763755360605521L;

	private UF uf = UF.MATO_GROSSO;
	
	private Municipio municipio;
	
    private String numero;
	
	private String cnpj;
	
	private ServicoDeInspecao servicoDeInspecao;
	
	public boolean isNull(){
		return ObjectUtils.isEmpty(this);
	}
	
	public UF getUf() {
		return uf;
	}

	public void setUf(UF uf) {
		this.uf = uf;
	}

	public Municipio getMunicipio() {
		return municipio;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public ServicoDeInspecao getServicoDeInspecao() {
		return servicoDeInspecao;
	}

	public void setServicoDeInspecao(ServicoDeInspecao servicoDeInspecao) {
		this.servicoDeInspecao = servicoDeInspecao;
	}

}