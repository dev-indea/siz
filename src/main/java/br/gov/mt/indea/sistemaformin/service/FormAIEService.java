package br.gov.mt.indea.sistemaformin.service;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.mt.indea.sistemaformin.entity.FormAIE;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class FormAIEService extends PaginableService<FormAIE, Long> {

	private static final Logger log = LoggerFactory.getLogger(FormAIEService.class);
	
	protected FormAIEService() {
		super(FormAIE.class);
	}
	
	public FormAIE findByIdFetchAll(Long id){
		FormAIE formAIE;
		
		StringBuilder sql = new StringBuilder();
		sql.append("  from FormAIE formAIE ")
		   .append("  join fetch formAIE.formIN formin ")
		   .append("  left join fetch formAIE.formCOM ")
		   .append("  join fetch formin.propriedade propriedade ")
		   .append("  join fetch propriedade.municipio ")
		   .append("  join fetch propriedade.ule ")
		   .append("  left join fetch propriedade.endereco ")
		   .append("  left join fetch formin.proprietario proprietario ")
		   .append("  left join fetch proprietario.endereco endereco ")
		   .append("  left join fetch endereco.municipio municipio ")
		   .append("  left join fetch municipio.uf")
		   .append("  join fetch formAIE.veterinario vet")
		   .append("  join fetch vet.conselho")
		   .append("  join fetch vet.tipoEmitente")
		   .append("  left join fetch vet.ule uu")
		   .append("  left join fetch uu.urs")
		   .append("  left join fetch vet.endereco")
		   .append(" where formAIE.id = :id ");
		
		Query query = getSession().createQuery(sql.toString()).setLong("id", id);
		formAIE = (FormAIE) query.uniqueResult();
		
		return formAIE;
	}
	
	@Override
	public void saveOrUpdate(FormAIE formAIE) {
		super.saveOrUpdate(formAIE);
		
		log.info("Salvando Form AIE {}", formAIE.getId());
	}
	
	@Override
	public void delete(FormAIE formAIE) {
		super.delete(formAIE);
		
		log.info("Removendo Form AIE {}", formAIE.getId());
	}
	
	@Override
	public void validar(FormAIE FormAIE) {

	}

	@Override
	public void validarPersist(FormAIE FormAIE) {

	}

	@Override
	public void validarMerge(FormAIE FormAIE) {

	}

	@Override
	public void validarDelete(FormAIE FormAIE) {

	}

}
