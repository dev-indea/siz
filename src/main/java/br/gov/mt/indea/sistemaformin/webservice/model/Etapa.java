package br.gov.mt.indea.sistemaformin.webservice.model;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Etapa implements Serializable {

	private static final long serialVersionUID = -1120290641479983805L;

	private Long id;
    
	private String nome;
    
    @XmlElement(name = "doenca")
	private Doenca doenca;
    
    @XmlElement(name = "especies")
	private List<EtapaEspecie> especies;

	public List<EtapaEspecie> getEspecies() {
		return especies;
	}

	public void setEspecies(List<EtapaEspecie> especies) {
		this.especies = especies;
	}

	public Doenca getDoenca() {
		return doenca;
	}

	public void setDoenca(Doenca doenca) {
		this.doenca = doenca;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

}
