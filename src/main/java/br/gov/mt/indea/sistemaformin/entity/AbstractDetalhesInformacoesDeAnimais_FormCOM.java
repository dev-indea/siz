package br.gov.mt.indea.sistemaformin.entity;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.SequenceGenerator;

import org.hibernate.envers.Audited;

import br.gov.mt.indea.sistemaformin.enums.Dominio.FaixaEtariaOuEspecie;

@Audited
@Entity(name="detalhe_info_animais_form_com")
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="tipo", discriminatorType=DiscriminatorType.STRING)
public class AbstractDetalhesInformacoesDeAnimais_FormCOM extends BaseEntity<Long>{

	private static final long serialVersionUID = 5114547401630550405L;
	
	@Id
	@SequenceGenerator(name="detalhe_info_animais_form_com_seq", sequenceName="detalhe_info_animais_form_com_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="detalhe_info_animais_form_com_seq")
	private Long id;
	
	@Column(insertable=false, updatable=false)
    private String tipo;
	
	@Enumerated(EnumType.STRING)
	@Column(name="faixa_etaria")
	private FaixaEtariaOuEspecie faixaEtaria;
	
	@Column(name="qtde_no_dia_da_inspecao")
	private Long quantidadeNoDiaDaInspecao;
	
	@Column(name="qtde_novos_confirmados")
	private Long quantidadeNovosCasosConfirmados;
	
	@Column(name="qtde_novos_provaveis")
	private Long quantidadeNovosCasosProvaveis;
	
	@Column(name="qtde_novos_mortos")
	private Long quantidadeNovosMortos;
	
	@Column(name="qtde_novos_abatidos")
	private Long quantidadeNovosAbatidos;
	
	@Column(name="qtde_novos_destruidos")
	private Long quantidadeNovosDestruidos;
	
	@Column(name="qtde_examinados")
	private Long quantidadeExaminados;
	
	@Column(name="qtde_animaisIngressos")
	private Long quantidadeAnimaisIngressos;
	
	@Column(name="qtde_AnimaisEgressos")
	private Long quantidadeAnimaisEgressos;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public FaixaEtariaOuEspecie getFaixaEtaria() {
		return faixaEtaria;
	}

	public void setFaixaEtaria(FaixaEtariaOuEspecie faixaEtaria) {
		this.faixaEtaria = faixaEtaria;
	}

	public Long getQuantidadeNoDiaDaInspecao() {
		return quantidadeNoDiaDaInspecao;
	}

	public void setQuantidadeNoDiaDaInspecao(Long quantidadeNoDiaDaInspecao) {
		this.quantidadeNoDiaDaInspecao = quantidadeNoDiaDaInspecao;
	}

	public Long getQuantidadeNovosCasosConfirmados() {
		return quantidadeNovosCasosConfirmados;
	}

	public void setQuantidadeNovosCasosConfirmados(
			Long quantidadeNovosCasosConfirmados) {
		this.quantidadeNovosCasosConfirmados = quantidadeNovosCasosConfirmados;
	}

	public Long getQuantidadeNovosCasosProvaveis() {
		return quantidadeNovosCasosProvaveis;
	}

	public void setQuantidadeNovosCasosProvaveis(Long quantidadeNovosCasosProvaveis) {
		this.quantidadeNovosCasosProvaveis = quantidadeNovosCasosProvaveis;
	}

	public Long getQuantidadeNovosMortos() {
		return quantidadeNovosMortos;
	}

	public void setQuantidadeNovosMortos(Long quantidadeNovosMortos) {
		this.quantidadeNovosMortos = quantidadeNovosMortos;
	}

	public Long getQuantidadeNovosAbatidos() {
		return quantidadeNovosAbatidos;
	}

	public void setQuantidadeNovosAbatidos(Long quantidadeNovosAbatidos) {
		this.quantidadeNovosAbatidos = quantidadeNovosAbatidos;
	}

	public Long getQuantidadeNovosDestruidos() {
		return quantidadeNovosDestruidos;
	}

	public void setQuantidadeNovosDestruidos(Long quantidadeNovosDestruidos) {
		this.quantidadeNovosDestruidos = quantidadeNovosDestruidos;
	}

	public Long getQuantidadeExaminados() {
		return quantidadeExaminados;
	}

	public void setQuantidadeExaminados(Long quantidadeExaminados) {
		this.quantidadeExaminados = quantidadeExaminados;
	}

	public Long getQuantidadeAnimaisIngressos() {
		return quantidadeAnimaisIngressos;
	}

	public void setQuantidadeAnimaisIngressos(Long quantidadeAnimaisIngressos) {
		this.quantidadeAnimaisIngressos = quantidadeAnimaisIngressos;
	}

	public Long getQuantidadeAnimaisEgressos() {
		return quantidadeAnimaisEgressos;
	}

	public void setQuantidadeAnimaisEgressos(Long quantidadeAnimaisEgressos) {
		this.quantidadeAnimaisEgressos = quantidadeAnimaisEgressos;
	}

	public Long getAcumuladoQuantidadeNovosCasosConfirmados(){
		return null;
	}
	
	public Long getAcumuladoQuantidadeNovosCasosProvaveis(){
		return null;
	}
	
	public Long getAcumuladoQuantidadeNovosMortos(){
		return null;
	}
	
	public Long getAcumuladoQuantidadeNovosAbatidos(){
		return null;
	}
	
	public Long getAcumuladoQuantidadeNovosDestruidos(){
		return null;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbstractDetalhesInformacoesDeAnimais_FormCOM other = (AbstractDetalhesInformacoesDeAnimais_FormCOM) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
		
}
