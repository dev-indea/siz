package br.gov.mt.indea.sistemaformin.webservice.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ExploracaoRuminante implements Serializable {
    
	private static final long serialVersionUID = 6446135633158401095L;

	private ExploracaoRuminantePK exploracaoRuminantePK;
    
    @XmlElement
    private FinalidadeExploracao finalidadeExploracao;
   
    @XmlElement
    private SistemaCriacao sistemaCriacao;
   
    @XmlElement
    private FaseExploracaoPecuaria faseExploracaoPecuaria;
   
    @XmlElement
    private SistemaProducao sistemaProducao;
   
    private Integer tempoPrenderRebanho;
    
    private Integer idPropriedadeRealizaVacina;

    public ExploracaoRuminante() {
    }

    public SistemaCriacao getSistemaCriacao() {
        return sistemaCriacao;
    }

    public void setSistemaCriacao(SistemaCriacao sistemaCriacao) {
        this.sistemaCriacao = sistemaCriacao;
    }

    public FaseExploracaoPecuaria getFaseExploracaoPecuaria() {
        return faseExploracaoPecuaria;
    }

    public void setFaseExploracaoPecuaria(FaseExploracaoPecuaria faseExploracaoPecuaria) {
        this.faseExploracaoPecuaria = faseExploracaoPecuaria;
    }

    public SistemaProducao getSistemaProducao() {
        return sistemaProducao;
    }

    public void setSistemaProducao(SistemaProducao sistemaProducao) {
        this.sistemaProducao = sistemaProducao;
    }

    public Integer getTempoPrenderRebanho() {
        return tempoPrenderRebanho;
    }

    public void setTempoPrenderRebanho(Integer tempoPrenderRebanho) {
        this.tempoPrenderRebanho = tempoPrenderRebanho;
    }

    public Integer getPropriedadeRealizaVacina() {
        return idPropriedadeRealizaVacina;
    }

    public void setPropriedadeRealizaVacina(Integer idPropriedadeRealizaVacina) {
        this.idPropriedadeRealizaVacina = idPropriedadeRealizaVacina;
    }

    public ExploracaoRuminantePK getExploracaoRuminantePK() {
        return exploracaoRuminantePK;
    }

    public void setExploracaoRuminantePK(ExploracaoRuminantePK exploracaoRuminantePK) {
        this.exploracaoRuminantePK = exploracaoRuminantePK;
    }

    public FinalidadeExploracao getFinalidadeExploracao() {
        return finalidadeExploracao;
    }

    public void setFinalidadeExploracao(FinalidadeExploracao finalidadeExploracao) {
        this.finalidadeExploracao = finalidadeExploracao;
    }
}
