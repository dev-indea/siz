package br.gov.mt.indea.sistemaformin.service;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.mt.indea.sistemaformin.entity.NumeroNotificacao;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class NumeroNotificacaoService extends PaginableService<NumeroNotificacao, Long> {
	
	private static final Logger log = LoggerFactory.getLogger(NumeroNotificacaoService.class);

	protected NumeroNotificacaoService() {
		super(NumeroNotificacao.class);
	}
	
	public NumeroNotificacao findByAno(Long ano){
		NumeroNotificacao numeroNotificacao;
		
		StringBuilder sql = new StringBuilder();
		sql.append("  from NumeroNotificacao numeroNotificacao ")
		   .append(" where numeroNotificacao.ano = :ano ");
		
		Query query = getSession().createQuery(sql.toString()).setLong("ano", ano);
		numeroNotificacao = (NumeroNotificacao) query.uniqueResult();
		
		return numeroNotificacao;
	}
	
	@Override
	public void saveOrUpdate(NumeroNotificacao numeroNotificacao) {
		super.saveOrUpdate(numeroNotificacao);
		
		log.info("Salvando N�mero Notifica��o {}", numeroNotificacao.getId());
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void saveNew(NumeroNotificacao numeroNotificacao) {
		this.saveOrUpdate(numeroNotificacao);
	}
	
	@Override
	public void delete(NumeroNotificacao numeroNotificacao) {
		super.delete(numeroNotificacao);
		
		log.info("Removendo N�mero Notifica��o {}", numeroNotificacao.getId());
	}
	
	@Override
	public void validar(NumeroNotificacao NumeroNotificacao) {

	}

	@Override
	public void validarPersist(NumeroNotificacao NumeroNotificacao) {

	}

	@Override
	public void validarMerge(NumeroNotificacao NumeroNotificacao) {

	}

	@Override
	public void validarDelete(NumeroNotificacao NumeroNotificacao) {

	}

}
