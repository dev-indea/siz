package br.gov.mt.indea.sistemaformin.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import br.gov.mt.indea.sistemaformin.exception.ApplicationException;

public abstract class DAO<T> implements BaseDAO{
	
	private final Class<T> classe;
	
	public DAO(Class<T> classe1){
		this.classe = classe1;
	}
	
	public void add(T object) throws ApplicationException{
		try{
			this.getEntityManager().persist(object);
		}catch(Exception e){
			throw new ApplicationException("N�o foi poss�vel adicionar o registro", e);
		}
	}
	
	public void update(T object) throws ApplicationException{
		try{
			this.getEntityManager().merge(object);
		}catch(Exception e){
			throw new ApplicationException("N�o foi poss�vel atualizar o registro", e);
		}
	}
	
	public void remove(T object) throws ApplicationException{
		try{
			this.getEntityManager().remove(object);
		}catch(Exception e){
			throw new ApplicationException("N�o foi poss�vel excluir o registro", e);
		}
	}
	
	public T merge(T object) throws ApplicationException{
		try{
			return this.getEntityManager().merge(object);
		}catch(Exception e){
			throw new ApplicationException("N�o foi poss�vel atualizar o registro", e);
		}
	}
	
	public List<T> findAll() throws ApplicationException{
		try{
			CriteriaQuery<T> query = this.getEntityManager().getCriteriaBuilder().createQuery(classe);
			query.select(query.from(classe));
			
			return this.getEntityManager().createQuery(query).getResultList();
		}catch(Exception e){
			throw new ApplicationException("N�o foi poss�vel listar os registros", e);
		}
	}
	
	public List<T> findAllOrderByAsc(String nomeCampo) throws ApplicationException{
		try	{
			CriteriaBuilder cb = this.getEntityManager().getCriteriaBuilder();
			CriteriaQuery<T> query = cb.createQuery(classe);
			
			Root<T> t = query.from(classe);
			
			query.orderBy(cb.asc(t.get(nomeCampo)));
			
			return this.getEntityManager().createQuery(query).getResultList();
		}catch(Exception e){
			throw new ApplicationException("N�o foi poss�vel listar os registros", e);
		}
	}
	
	public List<T> findAllOrderByDesc(String nomeCampo) throws ApplicationException{
		try{
			CriteriaBuilder cb = this.getEntityManager().getCriteriaBuilder();
			CriteriaQuery<T> query = cb.createQuery(classe);
			
			Root<T> t = query.from(classe);
			
			query.orderBy(cb.desc(t.get(nomeCampo)));
			
			return this.getEntityManager().createQuery(query).getResultList();
		}catch(Exception e){
			throw new ApplicationException("N�o foi poss�vel listar os registros", e);
		}
	}
	
	public T findById(Long id) throws ApplicationException{
		try{
			CriteriaBuilder cb = this.getEntityManager().getCriteriaBuilder();
			CriteriaQuery<T> query = cb.createQuery(classe);
			
			Root<T> t = query.from(classe);
			
			query.where(cb.equal(t.get("id"), id));
			
			return this.getEntityManager().createQuery(query).getSingleResult();
		}catch(Exception e){
			throw new ApplicationException("N�o foi poss�vel buscar o registro", e);
		}
	}	

}
