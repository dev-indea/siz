package br.gov.mt.indea.sistemaformin.service;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.mt.indea.sistemaformin.entity.HistoricoFormIN;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class HistoricoFormINService extends PaginableService<HistoricoFormIN, Long> {
	
	private static final Logger log = LoggerFactory.getLogger(HistoricoFormINService.class);

	protected HistoricoFormINService() {
		super(HistoricoFormIN.class);
	}
	
	public HistoricoFormIN findByIdFetchAll(Long id){
		HistoricoFormIN historicoFormIN;
		
		StringBuilder sql = new StringBuilder();
		sql.append("select historicoFormIN ")
		   .append("  from HistoricoFormIN historicoFormIN ")
		   .append(" where historicoFormIN.id = :id ");
		
		Query query = getSession().createQuery(sql.toString()).setLong("id", id);
		historicoFormIN = (HistoricoFormIN) query.uniqueResult();
		
		return historicoFormIN;
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	public void saveOrUpdate(HistoricoFormIN historicoFormIN) {
		historicoFormIN = (HistoricoFormIN) this.getSession().merge(historicoFormIN);
		super.saveOrUpdate(historicoFormIN);
		
		log.info("Salvando Historico Form IN {}", historicoFormIN.getId());
	}
	
	@Override
	public void validar(HistoricoFormIN HistoricoFormIN) {

	}

	@Override
	public void validarPersist(HistoricoFormIN HistoricoFormIN) {

	}

	@Override
	public void validarMerge(HistoricoFormIN HistoricoFormIN) {

	}

	@Override
	public void validarDelete(HistoricoFormIN HistoricoFormIN) {

	}

}
