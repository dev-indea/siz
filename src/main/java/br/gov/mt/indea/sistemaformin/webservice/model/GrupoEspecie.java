package br.gov.mt.indea.sistemaformin.webservice.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class GrupoEspecie implements Serializable {

	private static final long serialVersionUID = -1852035611975703831L;

	private String id;
    
    private String nome;

    public GrupoEspecie() {
    }

    public GrupoEspecie(String id, String nome) {
        super();
        this.id = id;
        this.nome = nome;
    }

    public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

}