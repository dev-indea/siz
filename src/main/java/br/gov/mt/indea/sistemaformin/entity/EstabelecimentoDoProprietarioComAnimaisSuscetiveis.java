package br.gov.mt.indea.sistemaformin.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import org.hibernate.envers.Audited;

import br.gov.mt.indea.sistemaformin.webservice.entity.Propriedade;

@Audited
@Entity(name = "estab_animais_suscet_eq")
public class EstabelecimentoDoProprietarioComAnimaisSuscetiveis extends BaseEntity<Long>{

	private static final long serialVersionUID = 9221282771093438602L;

	@Id
	@SequenceGenerator(name = "estab_animais_suscet_eq_seq", sequenceName="estab_animais_suscet_eq_seq", allocationSize=1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "estab_animais_suscet_eq_seq")
	private Long id;

	@ManyToOne
	@JoinColumn(name = "id_formeq")
	private FormEQ formEQ;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "id_propriedade")
	private Propriedade propriedade;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public FormEQ getFormEQ() {
		return formEQ;
	}

	public void setFormEQ(FormEQ formEQ) {
		this.formEQ = formEQ;
	}

	public Propriedade getPropriedade() {
		return propriedade;
	}

	public void setPropriedade(Propriedade propriedade) {
		this.propriedade = propriedade;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EstabelecimentoDoProprietarioComAnimaisSuscetiveis other = (EstabelecimentoDoProprietarioComAnimaisSuscetiveis) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;

	}

}