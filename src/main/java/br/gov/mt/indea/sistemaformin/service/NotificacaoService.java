package br.gov.mt.indea.sistemaformin.service;

import java.util.Calendar;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;

import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.mt.indea.sistemaformin.entity.Municipio;
import br.gov.mt.indea.sistemaformin.entity.Notificacao;
import br.gov.mt.indea.sistemaformin.entity.NumeroNotificacao;
import br.gov.mt.indea.sistemaformin.util.RandomUtil;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class NotificacaoService extends PaginableService<Notificacao, Long> {
	
	private static final Logger log = LoggerFactory.getLogger(NotificacaoService.class);
	
	@Inject
	NumeroNotificacaoService numeroNotificacaoService;

	protected NotificacaoService() {
		super(Notificacao.class);
	}
	
	@SuppressWarnings("unchecked")
	public List<Notificacao> findAll(Municipio municipio){
		List<Notificacao> listNotificacoes;
		
		StringBuilder sql = new StringBuilder();
		sql.append("  from Notificacao notificacao ")
		   .append("  join fetch notificacao.endereco endereco ")
		   .append("  join fetch endereco.municipio ")
		   .append(" where endereco.municipio = :municipio")
		   .append(" order by notificacao.dataCadastro DESC ");
		
		Query query = getSession().createQuery(sql.toString()).setParameter("municipio", municipio);
		listNotificacoes = query.list();
		
		if (listNotificacoes != null){
		
		}
		
		return listNotificacoes;
	}
	
	public Notificacao find(String numero, String codigoSeguranca) {
		Notificacao notificacao;
		
		StringBuilder sql = new StringBuilder();
		sql.append("  from Notificacao notificacao ")
		   .append("  join fetch notificacao.endereco endereco ")
		   .append("  join fetch endereco.municipio ")
		   .append(" where notificacao.numero = :numero")
		   .append("   and notificacao.codigoSeguranca = :codigoSeguranca");
		
		Query query = getSession().createQuery(sql.toString()).setString("numero", numero).setString("codigoSeguranca", codigoSeguranca);
		notificacao = (Notificacao) query.uniqueResult();
		
		if (notificacao != null){
		
		}
		
		return notificacao;
	}
	
	
	public Notificacao findByIdFetchAll(Long id){
		Notificacao notificacao;
		
		StringBuilder sql = new StringBuilder();
		sql.append("  from Notificacao notificacao ")
		   .append(" where notificacao.id = :id ");
		
		Query query = getSession().createQuery(sql.toString()).setLong("id", id);
		notificacao = (Notificacao) query.uniqueResult();
		
		if (notificacao != null){
		
		}
		
		return notificacao;
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void saveOrUpdate(Notificacao notificacao) {
		long ano = notificacao.getDataCadastro().get(Calendar.YEAR) + 0L;
		String numero = String.format("%04d", this.getProximoNumero(ano)) + "/" + ano;
		notificacao.setNumero(numero);
		notificacao.setCodigoSeguranca(RandomUtil.generate(20));
		
		super.saveOrUpdate(notificacao);
		
		log.info("Salvando Notificação {}", notificacao.getId());
	}

	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	private Long getProximoNumero(Long ano) {
		NumeroNotificacao numeroNotificacao = numeroNotificacaoService.findByAno(ano);
		
		if (numeroNotificacao == null) {
			numeroNotificacao = new NumeroNotificacao();
			numeroNotificacao.setAno(ano);
			numeroNotificacao.setUltimoNumero(0L);
			
			numeroNotificacaoService.saveNew(numeroNotificacao);
		}
		
		numeroNotificacao.incrementNumeroNotificacao();
		numeroNotificacaoService.saveOrUpdate(numeroNotificacao);
		
		long numero = numeroNotificacao.getUltimoNumero();
		
		return numero;
	}
	
	@Override
	public void delete(Notificacao notificacao) {
		super.delete(notificacao);
		
		log.info("Removendo Notificação {}", notificacao.getId());
	}
	
	@Override
	public void validar(Notificacao notificacao) {

	}

	@Override
	public void validarPersist(Notificacao notificacao) {

	}

	@Override
	public void validarMerge(Notificacao notificacao) {

	}

	@Override
	public void validarDelete(Notificacao notificacao) {

	}

}
