package br.gov.mt.indea.sistemaformin.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.envers.Audited;

@Audited
@Entity
@DiscriminatorValue("asininos")
public class DetalhesInformacoesAsininos extends AbstractDetalhesInformacoesDeAnimais implements Cloneable{
	
	private static final long serialVersionUID = 1355044082455412390L;

	@ManyToOne
	@JoinColumn(name="id_informacoes_asininos")
	private InformacoesAsininos informacoesAsininos;

	public InformacoesAsininos getInformacoesAsininos() {
		return informacoesAsininos;
	}

	public void setInformacoesAsininos(InformacoesAsininos informacoesAsininos) {
		this.informacoesAsininos = informacoesAsininos;
	}
	
	protected Object clone(InformacoesAsininos informacoesAsininos) throws CloneNotSupportedException{
		DetalhesInformacoesAsininos cloned = (DetalhesInformacoesAsininos) super.clone();
		
		cloned.setId(null);
		cloned.setInformacoesAsininos(informacoesAsininos);
		
		return cloned;
	}

}