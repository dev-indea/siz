package br.gov.mt.indea.sistemaformin.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.inject.Inject;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import br.gov.mt.indea.sistemaformin.entity.TipoChaveSecundariaVisitaPropriedadeRural;
import br.gov.mt.indea.sistemaformin.enums.Dominio.AtivoInativo;
import br.gov.mt.indea.sistemaformin.exception.ApplicationException;

@Stateless
public class TipoChaveSecundariaVisitaPropriedadeRuralDAO extends DAO<TipoChaveSecundariaVisitaPropriedadeRural> {
	
	@Inject
	private EntityManager em;
	
	public TipoChaveSecundariaVisitaPropriedadeRuralDAO() {
		super(TipoChaveSecundariaVisitaPropriedadeRural.class);
	}

	@Override
	public EntityManager getEntityManager() {
		return this.em;
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void add(TipoChaveSecundariaVisitaPropriedadeRural tipoChaveSecundariaVisitaPropriedadeRural) throws ApplicationException{
		tipoChaveSecundariaVisitaPropriedadeRural = this.getEntityManager().merge(tipoChaveSecundariaVisitaPropriedadeRural);
		super.add(tipoChaveSecundariaVisitaPropriedadeRural);
	}
	
	public List<TipoChaveSecundariaVisitaPropriedadeRural> findAllByStatus(AtivoInativo status) throws ApplicationException{
		try{
			CriteriaBuilder cb = this.getEntityManager().getCriteriaBuilder();
			CriteriaQuery<TipoChaveSecundariaVisitaPropriedadeRural> query = cb.createQuery(TipoChaveSecundariaVisitaPropriedadeRural.class);
			
			Root<TipoChaveSecundariaVisitaPropriedadeRural> t = query.from(TipoChaveSecundariaVisitaPropriedadeRural.class);
			
			query.where(cb.equal(t.get("status"), status));
			query.orderBy(cb.asc(t.get("nome")));
			
			return this.getEntityManager().createQuery(query).getResultList();
		}catch(Exception e){
			throw new ApplicationException("N�o foi poss�vel buscar o registro", e);
		}
	}

}
