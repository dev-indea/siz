package br.gov.mt.indea.sistemaformin.managedbeans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.Past;

import org.primefaces.context.RequestContext;
import org.primefaces.event.FlowEvent;

import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLBeanName;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;

import br.gov.mt.indea.sistemaformin.entity.DetalheComunicacaoAIE;
import br.gov.mt.indea.sistemaformin.entity.DetalheFormNotifica;
import br.gov.mt.indea.sistemaformin.entity.DetalhesInformacoesAbelhas;
import br.gov.mt.indea.sistemaformin.entity.DetalhesInformacoesAsininos;
import br.gov.mt.indea.sistemaformin.entity.DetalhesInformacoesAves;
import br.gov.mt.indea.sistemaformin.entity.DetalhesInformacoesBovinos;
import br.gov.mt.indea.sistemaformin.entity.DetalhesInformacoesBubalinos;
import br.gov.mt.indea.sistemaformin.entity.DetalhesInformacoesCaprinos;
import br.gov.mt.indea.sistemaformin.entity.DetalhesInformacoesEquinos;
import br.gov.mt.indea.sistemaformin.entity.DetalhesInformacoesLagomorfos;
import br.gov.mt.indea.sistemaformin.entity.DetalhesInformacoesMuares;
import br.gov.mt.indea.sistemaformin.entity.DetalhesInformacoesOutrosAnimais;
import br.gov.mt.indea.sistemaformin.entity.DetalhesInformacoesOvinos;
import br.gov.mt.indea.sistemaformin.entity.DetalhesInformacoesSuinos;
import br.gov.mt.indea.sistemaformin.entity.FormIN;
import br.gov.mt.indea.sistemaformin.entity.FormVIN;
import br.gov.mt.indea.sistemaformin.entity.HistoricoFormIN;
import br.gov.mt.indea.sistemaformin.entity.InformacoesAbelhas;
import br.gov.mt.indea.sistemaformin.entity.InformacoesAsininos;
import br.gov.mt.indea.sistemaformin.entity.InformacoesAves;
import br.gov.mt.indea.sistemaformin.entity.InformacoesBovinos;
import br.gov.mt.indea.sistemaformin.entity.InformacoesBubalinos;
import br.gov.mt.indea.sistemaformin.entity.InformacoesCaprinos;
import br.gov.mt.indea.sistemaformin.entity.InformacoesEquinos;
import br.gov.mt.indea.sistemaformin.entity.InformacoesLagomorfos;
import br.gov.mt.indea.sistemaformin.entity.InformacoesMuares;
import br.gov.mt.indea.sistemaformin.entity.InformacoesOutrosAnimais;
import br.gov.mt.indea.sistemaformin.entity.InformacoesOvinos;
import br.gov.mt.indea.sistemaformin.entity.InformacoesSuinos;
import br.gov.mt.indea.sistemaformin.entity.LoteEnfermidadeAbatedouroFrigorifico;
import br.gov.mt.indea.sistemaformin.entity.Medicacao;
import br.gov.mt.indea.sistemaformin.entity.Municipio;
import br.gov.mt.indea.sistemaformin.entity.RegistroEntradaLASA;
import br.gov.mt.indea.sistemaformin.entity.TransitoDeAnimais;
import br.gov.mt.indea.sistemaformin.entity.UF;
import br.gov.mt.indea.sistemaformin.entity.VacinacaoFormIN;
import br.gov.mt.indea.sistemaformin.entity.VisitaPropriedadeRural;
import br.gov.mt.indea.sistemaformin.entity.dto.FormINDTO;
import br.gov.mt.indea.sistemaformin.enums.Dominio.FaixaEtariaOuEspecie;
import br.gov.mt.indea.sistemaformin.enums.Dominio.FonteDaNotificacao;
import br.gov.mt.indea.sistemaformin.enums.Dominio.MotivoInicialDaInvestigacao;
import br.gov.mt.indea.sistemaformin.enums.Dominio.SimNao;
import br.gov.mt.indea.sistemaformin.enums.Dominio.StatusFormIN;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoCoordenadaGeografica;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoDiagnostico;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoTransitoDeAnimais;
import br.gov.mt.indea.sistemaformin.enums.Dominio.VigilanciaSindromica;
import br.gov.mt.indea.sistemaformin.exception.ApplicationErrorException;
import br.gov.mt.indea.sistemaformin.exception.ApplicationException;
import br.gov.mt.indea.sistemaformin.service.EnfermidadeAbatedouroFrigorificoService;
import br.gov.mt.indea.sistemaformin.service.FormINService;
import br.gov.mt.indea.sistemaformin.service.FormNotificaService;
import br.gov.mt.indea.sistemaformin.service.FormVINService;
import br.gov.mt.indea.sistemaformin.service.HistoricoFormINService;
import br.gov.mt.indea.sistemaformin.service.LazyObjectDataModel;
import br.gov.mt.indea.sistemaformin.service.MunicipioService;
import br.gov.mt.indea.sistemaformin.service.PropriedadeService;
import br.gov.mt.indea.sistemaformin.service.UFService;
import br.gov.mt.indea.sistemaformin.service.VisitaPropriedadeRuralService;
import br.gov.mt.indea.sistemaformin.util.CoordenadaGeograficaUtil;
import br.gov.mt.indea.sistemaformin.util.FacesMessageUtil;
import br.gov.mt.indea.sistemaformin.webservice.client.ClientWebServiceGTA;
import br.gov.mt.indea.sistemaformin.webservice.client.ClientWebServiceVeterinario;
import br.gov.mt.indea.sistemaformin.webservice.entity.CoordenadaGeografica;
import br.gov.mt.indea.sistemaformin.webservice.entity.Gta;
import br.gov.mt.indea.sistemaformin.webservice.entity.Produtor;
import br.gov.mt.indea.sistemaformin.webservice.entity.Propriedade;
import br.gov.mt.indea.sistemaformin.webservice.entity.Veterinario;

@Named
@ViewScoped
@URLBeanName("formINManagedBean")
@URLMappings(mappings = {
		@URLMapping(id = "pesquisarFormIN", pattern = "/formIN/pesquisar", viewId = "/pages/formIN/lista.jsf"),
		@URLMapping(id = "incluirFormIN", pattern = "/formIN/incluir", viewId = "/pages/formIN/novo.jsf"),
		@URLMapping(id = "alterarFormIN", pattern = "/formIN/alterar/#{formINManagedBean.id}", viewId = "/pages/formIN/novo.jsf"),
		@URLMapping(id = "historicoFormIN", pattern = "/formIN/historico/#{formINManagedBean.id}", viewId = "/pages/formIN/historico.jsf"),
		@URLMapping(id = "impressaoFormIN", pattern = "/formIN/#{formINManagedBean.id}/impressao/#{formINManagedBean.idHistorico}", viewId = "/pages/impressao.jsf")})
public class FormINManagedBean implements Serializable {
	
	private static final long serialVersionUID = -8951571157874536719L;
	
	private Long id;
	
	private Long idHistorico;
	
	private DetalheComunicacaoAIE detalheComunicacaoAIE;
	
	private RegistroEntradaLASA registroEntradaLASA;
	
	private DetalheFormNotifica detalheFormNotifica;
	
	private LoteEnfermidadeAbatedouroFrigorifico loteEnfermidadeAbatedouroFrigorifico;
	
	private int activeIndexTelaBuscaPropriedade = 0;
	
	@Inject
	private FormIN formIN;
	
	@Inject
	private HistoricoFormIN historicoFormIN;
	
	@Inject
	private FormINDTO formINDTO;
	
	@Inject
	private HistoricoFormINService historicoFormINService; 
	
	@Inject
	private MunicipioService municipioService;
	
	private String numeroFormINRelacionado;
	
	private String numeroVisitaPropriedadeRural;
	
	private List<VisitaPropriedadeRural> listaVisitaPropriedadeRural;
	
	@Inject
	private FormINService formINService;
	
	@Inject
	private FormVINService formVINService;
	
	@Inject
	private UFService ufService;
	
	@Inject
	private FormNotificaService formNotificaService;
	
	@Inject
	private EnfermidadeAbatedouroFrigorificoService enfermidadeAbatedouroFrigorificoService;
	
	@Inject
	private VisitaPropriedadeRuralService visitaPropriedadeRuralService;
	
	@Past(message="Data precisa ser menor ou igual a data atual")
	private Date dataDaNotificacao;
	
	@Past(message="Data precisa ser menor ou igual a data atual")
	private Date dataResultadoDiagnosticoLaboratorial;
	
	@Past(message="Data precisa ser menor ou igual a data atual")
	private Date dataAbertura;
	
	@Past(message="Data precisa ser menor ou igual a data atual")
	private Date dataInicioEvento;
	
	@Past(message="Data precisa ser menor ou igual a data atual")
	private Date dataVacinacao;
	
	@Past(message="Data precisa ser menor ou igual a data atual")
	private Date dataInicialMedicacao;
	
	@Past(message="Data precisa ser menor ou igual a data atual")
	private Date dataFinalMedicacao;
	
	@Past(message="Data precisa ser menor ou igual a data atual")
	private Date dataRetificacao;
	
	private Date dataSISBRAVET;

	private Long idPropriedade;
	
	private String nomePropriedade;

	private List<Propriedade> listaPropriedades;
	
	private TipoCoordenadaGeografica tipoCoordenadaGeografica;
	
	private TipoDiagnostico tipoDiagnostico;
	
	private SimNao diagnosticoLaboratorial;

	private String cpfVeterinario;

	private String crmvVeterinario;

	private List<Veterinario> listaVeterinarios;
	
	private Integer numeroGta;

	private String serieGta;
	
	private Date dataInicialBuscaGta;
	
	private Date dataFinalBuscaGta;
	
	private List<TipoTransitoDeAnimais> listaTipoTransitoDeAnimais;

	private List<Gta> listaGtas;
	
	private List<FormIN> listaFormINsRelacionados;
	
	private boolean forceUpdatePropriedade = false;
	
	@Inject
	private PropriedadeService propriedadeService;
	
	@Inject
	private ClientWebServiceVeterinario clientWebServiceVeterinario;
	
	@Inject
	private ClientWebServiceGTA clientWebServiceGTA;
	
	private LazyObjectDataModel<FormIN> listaFormIN;
	
	public String redirectToGoogleMaps(){
		if (this.getFormIN().getPropriedade() == null)
			return null;
		
		CoordenadaGeografica coordenadaGeografica = this.getFormIN().getPropriedade().getCoordenadaGeografica();
		
		if (coordenadaGeografica == null || coordenadaGeografica.isNull())
			return null;
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("https://www.google.com/maps/place/")
		  .append(CoordenadaGeograficaUtil.convertToDecimal(coordenadaGeografica.getLatGrau(), coordenadaGeografica.getLatMin(), coordenadaGeografica.getLatSeg(), coordenadaGeografica.getOrientacaoLatitude()))
		  .append(",")
		  .append(CoordenadaGeograficaUtil.convertToDecimal(coordenadaGeografica.getLongGrau(), coordenadaGeografica.getLongMin(), coordenadaGeografica.getLongSeg(), coordenadaGeografica.getOrientacaoLatitude()));
		
		return sb.toString();
	}
	
	public void carregaFormIN(){
		this.formIN = formINService.findByIdFetchAll(this.id);
	}
	
	@URLAction(mappingId = "historicoFormIN", onPostback = false)
	public void visualizarHistorico(){
		this.formIN = formINService.carregarHistorico(this.getId());
	}
	
	private void limpar() {
		this.dataAbertura = null;
		this.dataDaNotificacao = null;
		this.dataInicioEvento = null;
		this.dataResultadoDiagnosticoLaboratorial = null;
		this.tipoCoordenadaGeografica = null;
		this.dataRetificacao = null;
		this.tipoDiagnostico = null;
		this.dataSISBRAVET = null;
		
		this.formIN = new FormIN();
	}
	
	@URLAction(mappingId = "incluirFormIN", onPostback = false)
	public void novo(){
		limpar();
		
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		Long idFormVIN = (Long) request.getSession().getAttribute("idFormVIN");
		
		if (idFormVIN != null){
			Propriedade propriedade = null;
			FormVIN formVIN = formVINService.findByIdFetchAll(idFormVIN);
			
			try {
				propriedade = propriedadeService.findByCodigoFetchAll(formVIN.getPropriedade().getCodigoPropriedade(), false);
			} catch (ApplicationException e) {
				FacesMessageUtil.addWarnContextFacesMessage("N�o foi poss�vel criar iniciar um Form IN com a propriedade do Form VIN", null);
				return;
			}
			
			this.selecionarPropriedade(propriedade);

			formIN.setMotivoInicialDaInvestigacao(MotivoInicialDaInvestigacao.VINCULO_EPIDEMIOLOGICO);
			formIN.setFonteDaNotificacao(FonteDaNotificacao.VIGILANCIA_SVO);
			formIN.setFormINRelacionado(formVIN.getFormIN());
			
			request.getSession().setAttribute("idFormVIN", null);
		}
		
		UF uf = ufService.findByIdFetchAll(UF.MATO_GROSSO.getId());
		formIN.setUf(uf);
		
		
		detalheComunicacaoAIE = (DetalheComunicacaoAIE) request.getSession().getAttribute("detalheComunicacaoAIE");
		
		if (detalheComunicacaoAIE != null){
			request.getSession().setAttribute("detalheComunicacaoAIE", null);
			this.formIN.setDetalheComunicacaoAIE(detalheComunicacaoAIE);
		}
		
		registroEntradaLASA = (RegistroEntradaLASA) request.getSession().getAttribute("registroEntradaLASA");
		
		if (registroEntradaLASA != null){
			request.getSession().setAttribute("registroEntradaLASA", null);
			this.formIN.setRegistroEntradaLASA(registroEntradaLASA);
		}
		
		detalheFormNotifica = (DetalheFormNotifica) request.getSession().getAttribute("detalheFormNotifica");
		
		if (detalheFormNotifica != null){
			request.getSession().setAttribute("detalheFormNotifica", null);
			
			detalheFormNotifica = formNotificaService.findDetalheFormNotificaByIdFetchAll(detalheFormNotifica.getId());
			
			this.formIN.setDetalheFormNotifica(detalheFormNotifica);
			
			if (detalheFormNotifica.getPropriedade().getCodigo() != null)
				this.formIN.setPropriedade(detalheFormNotifica.getPropriedade());
		}
		
		loteEnfermidadeAbatedouroFrigorifico = (LoteEnfermidadeAbatedouroFrigorifico) request.getSession().getAttribute("loteEnfermidadeAbatedouroFrigorifico");
		
		if (loteEnfermidadeAbatedouroFrigorifico != null){
			request.getSession().setAttribute("loteEnfermidadeAbatedouroFrigorifico", null);
			
			loteEnfermidadeAbatedouroFrigorifico = enfermidadeAbatedouroFrigorificoService.findLoteEnfermidadeAbatedouroFrigorificoByIdFetchAll(loteEnfermidadeAbatedouroFrigorifico.getId());
			
			this.formIN.setLoteEnfermidadeAbatedouroFrigorifico(loteEnfermidadeAbatedouroFrigorifico);
			
			if (loteEnfermidadeAbatedouroFrigorifico.getProprietario().getPropriedade().getCodigo() != null)
				this.formIN.setPropriedade(loteEnfermidadeAbatedouroFrigorifico.getProprietario().getPropriedade());
		}
	}
	
	public void inicializarInformacoesDeAnimais(){
		this.showFormBovinos();
		this.showFormBubalinos();
		this.showFormCaprinos();
		this.showFormOvinos();
		this.showFormSuinos();
		this.showFormEquinos();
		this.showFormAsininos();
		this.showFormMuares();
		this.showFormAves();
		this.showFormAbelhas();
		this.showFormLagomorfos();
		this.showFormOutrosAnimais();
	}
	
	public boolean isPropriedadeNull(){
		
		if (this.formIN.getPropriedade() == null){
			FacesContext context = FacesContext.getCurrentInstance();
			
			context.addMessage("cadastro:fieldPropriedade:campoPropriedade", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Propriedade: valor � obrigat�rio.", "Propriedade: valor � obrigat�rio."));
			context.addMessage("cadastro:fieldProprietario:campoProprietario", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Propriet�rio: valor � obrigat�rio.", "Propriet�rio: valor � obrigat�rio."));
			return false;
		}else {
			if (this.formIN.getPropriedade() != null){
				if (this.formIN.getProprietario() == null || this.formIN.getProprietario().equals("")){
					FacesContext context = FacesContext.getCurrentInstance();
					
					context.addMessage("cadastro:fieldProprietario:campoProprietario", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Propriet�rio: valor � obrigat�rio.", "Propriet�rio: valor � obrigat�rio."));
					return false;
				}
			}
			
			if (this.formIN.getPropriedade() != null){
				if (this.formIN.getPropriedade().getCoordenadaGeografica() == null || 
						this.formIN.getPropriedade().getCoordenadaGeografica().getLatGrau() == null ||
						this.formIN.getPropriedade().getCoordenadaGeografica().getLatMin() == null ||
						this.formIN.getPropriedade().getCoordenadaGeografica().getLatSeg() == null){
					FacesContext context = FacesContext.getCurrentInstance();
					
					context.addMessage("cadastro:fieldPropriedadeLatitude:campoPropriedadeLatitude", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Coordenadas - Latitude: valor � obrigat�rio.", "O valor deve estar corretamente preenchido no SINDESA."));
					return false;
				}
			}
			
			if (this.formIN.getPropriedade() != null){
				if (this.formIN.getPropriedade().getCoordenadaGeografica() == null || 
						this.formIN.getPropriedade().getCoordenadaGeografica().getLongGrau() == null ||
						this.formIN.getPropriedade().getCoordenadaGeografica().getLongMin() == null ||
						this.formIN.getPropriedade().getCoordenadaGeografica().getLongSeg() == null){
					FacesContext context = FacesContext.getCurrentInstance();
					
					context.addMessage("cadastro:fieldPropriedadeLongitude:campoPropriedadeLongitude", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Coordenadas - Latitude: valor � obrigat�rio.", "O valor deve estar corretamente preenchido no SINDESA."));
					return false;
				}
			}
			
		}
		
		if (this.formIN.getDoencaDeVigilanciaSindromica().equals(VigilanciaSindromica.NENHUMA_DAS_ANTERIORES)){
			if ((this.formIN.getDianosticoProvavel() == null || this.formIN.getDianosticoProvavel().equals("")) && (this.formIN.getDianosticoConclusivo() == null || this.formIN.getDianosticoConclusivo().equals(""))){
				FacesContext context = FacesContext.getCurrentInstance();
				
				context.addMessage("cadastro:fieldDiagnosticoProvavel:campoDiagnosticoProvavel", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Deve ser preenchido ao menos um dos campos: \"Diagn�stico prov�vel\" ou \"Diagn�stico conclusivo\"", "Deve ser preenchido ao menos um dos campos: \"Diagn�stico prov�vel\" ou \"Diagn�stico conclusivo\""));
				context.addMessage("cadastro:fieldDiagnosticoConclusivo:campoDiagnosticoConclusivo", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Deve ser preenchido ao menos um dos campos: \"Diagn�stico prov�vel\" ou \"Diagn�stico conclusivo\"", "Deve ser preenchido ao menos um dos campos: \"Diagn�stico prov�vel\" ou \"Diagn�stico conclusivo\""));
				return false;
			}
			
		}
		
		this.formIN.getPropriedade().setTipoCoordenadaGeografica(tipoCoordenadaGeografica);
		
		return true;
	}
	
	public String adicionar(){
		if (this.dataDaNotificacao!= null){
			if (this.formIN.getDataDaNotificacao() == null)
				this.formIN.setDataDaNotificacao(Calendar.getInstance());
			this.formIN.getDataDaNotificacao().setTime(dataDaNotificacao);
		}else
			this.formIN.setDataDaNotificacao(null);
			
		if (this.dataAbertura != null){
			if (this.formIN.getDataAbertura() == null)
				this.formIN.setDataAbertura(Calendar.getInstance());
			this.formIN.getDataAbertura().setTime(dataAbertura);
		}else
			this.formIN.setDataAbertura(null);
			
		if (this.dataInicioEvento != null){
			if (this.formIN.getDataInicioEvento() == null)
				this.formIN.setDataInicioEvento(Calendar.getInstance());
			this.formIN.getDataInicioEvento().setTime(dataInicioEvento);
		}else
			this.formIN.setDataInicioEvento(null);
		
		if (this.dataResultadoDiagnosticoLaboratorial != null){
			if (this.formIN.getDataResultadoDiagnosticoLaboratorial() == null)
				this.formIN.setDataResultadoDiagnosticoLaboratorial(Calendar.getInstance());
			this.formIN.getDataResultadoDiagnosticoLaboratorial().setTime(dataResultadoDiagnosticoLaboratorial);
		}else
			this.formIN.setDataResultadoDiagnosticoLaboratorial(null);
		
		if (this.dataRetificacao != null){
			if (this.formIN.getDataRetificacao() == null)
				this.formIN.setDataRetificacao(Calendar.getInstance());
			this.formIN.getDataRetificacao().setTime(dataRetificacao);
		}else
			this.formIN.setDataRetificacao(null);
		
		if (this.dataSISBRAVET != null){
			if (this.formIN.getDataSISBRAVET() == null)
				this.formIN.setDataSISBRAVET(Calendar.getInstance());
			this.formIN.getDataSISBRAVET().setTime(dataSISBRAVET);
		}else
			this.formIN.setDataSISBRAVET(null);
		
		if (this.formIN.getVeterinario() == null){
			FacesContext context = FacesContext.getCurrentInstance();
			
			context.addMessage("cadastro:fieldVeterinario:campoVeterinario", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Veterin�rio: valor � obrigat�rio.", "Veterin�rio: valor � obrigat�rio."));
			return "";
		}
		
		if (this.detalheComunicacaoAIE != null){
			this.formIN.setDetalheComunicacaoAIE(detalheComunicacaoAIE);
			this.detalheComunicacaoAIE.setFormIN(formIN);
		}
		
		if (this.registroEntradaLASA != null){
			this.formIN.setRegistroEntradaLASA(registroEntradaLASA);
			this.registroEntradaLASA.setFormIN(formIN);
		}
		
		if (this.detalheFormNotifica != null){
			this.formIN.setDetalheFormNotifica(detalheFormNotifica);
			this.detalheFormNotifica.setFormIN(formIN);
		}
		
		if (this.loteEnfermidadeAbatedouroFrigorifico != null){
			this.formIN.setLoteEnfermidadeAbatedouroFrigorifico(loteEnfermidadeAbatedouroFrigorifico);
			this.loteEnfermidadeAbatedouroFrigorifico.setFormIN(formIN);
		}
		
		if (formIN.getId() != null){
			if (this.formIN.getStatusFormIN().equals(StatusFormIN.VALIDADO))
				this.formIN.setStatusFormIN(StatusFormIN.ALTERADO);
			
			this.formINService.saveOrUpdate(formIN);

			FacesMessageUtil.addInfoContextFacesMessage("Form IN n� " + this.formIN.getNumero() + " atualizado", "");
		}else{
			this.formIN.setDataCadastro(Calendar.getInstance());
			this.formINService.saveOrUpdate(formIN);

			FacesMessageUtil.addInfoContextFacesMessage("Form IN n� " + this.formIN.getNumero() + " adicionado", "");
		}
		
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		request.getSession().setAttribute("numeroFormIN", formIN.getNumero());
		
		limpar();
		return "pretty:pesquisarFormIN";
	}

	@Inject
	private VisualizadorImpressaoManagedBean visualizadorImpressaoManagedBean;
	
	@URLAction(mappingId = "impressaoFormIN", onPostback = false)
	public void imprimir() throws ApplicationErrorException{
		this.historicoFormIN = historicoFormINService.findByIdFetchAll(idHistorico);
		
		visualizadorImpressaoManagedBean.exibirFormIN(historicoFormIN);
	}
	
	public void remover(FormIN formIN){
		this.formINService.delete(formIN);

		FacesMessageUtil.addInfoContextFacesMessage("Form IN n� " + formIN.getNumero() + " exclu�do com sucesso", "");
	}
	
	public void removerVisitaPropriedadeRural(){
		this.formIN.setVisitaPropriedadeRural(null);
	}
	
	public void validar(FormIN formIN){
		if (formIN.getStatusFormIN().equals(StatusFormIN.VALIDADO)){
			FacesMessageUtil.addWarnContextFacesMessage("O Form IN j� est� validado", "");
			return;
		}
		
		formIN = formINService.findByIdFetchAll(formIN.getId());
		formINService.validarFormIN(formIN);
		
		FacesMessageUtil.addInfoContextFacesMessage("Form IN n� " + formIN.getNumero() + " validado com sucesso", "");
	}
	
	@URLAction(mappingId = "alterarFormIN", onPostback = false)
	public void editar(){
		this.formIN = formINService.findByIdFetchAll(this.getId());
		
		if (this.formIN.getStatusFormIN().equals(StatusFormIN.VALIDADO))
			this.formIN.setRetificado(SimNao.SIM);
		
		if (this.formIN.getDataDaNotificacao() != null)
			this.dataDaNotificacao = this.formIN.getDataDaNotificacao().getTime();
		if (this.formIN.getDataAbertura() != null)
			this.dataAbertura = this.formIN.getDataAbertura().getTime();
		if (this.formIN.getDataInicioEvento() != null)
			this.dataInicioEvento = this.formIN.getDataInicioEvento().getTime();
		if (this.formIN.getDataResultadoDiagnosticoLaboratorial() != null)
			this.dataResultadoDiagnosticoLaboratorial = this.formIN.getDataResultadoDiagnosticoLaboratorial().getTime();
		if (this.formIN.getDataRetificacao() != null)
			this.dataRetificacao = this.formIN.getDataRetificacao().getTime();
		if (this.formIN.getDataSISBRAVET() != null)
			this.dataSISBRAVET = this.formIN.getDataSISBRAVET().getTime();

		if (this.formIN.getPropriedade() != null)
			this.tipoCoordenadaGeografica = this.formIN.getPropriedade().getTipoCoordenadaGeografica();
		
//		if (this.formIN.getDoencaDeVigilanciaSindromica().equals(VigilanciaSindromica.NENHUMA_DAS_ANTERIORES)){
			if (this.formIN.getDianosticoConclusivo() != null)
				this.tipoDiagnostico = TipoDiagnostico.CONCLUSIVO;
			else if (this.formIN.getDianosticoProvavel() != null)
				this.tipoDiagnostico = TipoDiagnostico.PROVAVEL;
//		}else
//			this.tipoDiagnostico = null;
		
		if (this.formIN.getIdentificacaoDiagnosticoLaboratorial() == null || this.formIN.getIdentificacaoDiagnosticoLaboratorial().equals(""))
			this.diagnosticoLaboratorial = SimNao.NAO;
		else
			this.diagnosticoLaboratorial = SimNao.SIM;
	}
	
	@URLAction(mappingId = "pesquisarFormIN", onPostback = false)
	public void cancelar(){
		this.limpar();
		
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		String numeroFormIN = (String) request.getSession().getAttribute("numeroFormIN");
		
		if (numeroFormIN != null){
			this.formINDTO = new FormINDTO();
			this.formINDTO.setNumeroFormIN(numeroFormIN);
			this.buscarFormINs();
			
			request.getSession().setAttribute("numeroFormIN", null);
		}
	}
	
	public void buscarFormINs(){
		this.listaFormIN = new LazyObjectDataModel<FormIN>(this.formINService, formINDTO);
	}
	
	public List<Municipio> getListaMunicipios(){
		if (this.formIN.getUf() == null)
			return null;
		
		return municipioService.findAllByUF(this.formIN.getUf());
	}
	
	public List<Municipio> getListaMunicipiosBusca(){
		return municipioService.findAllByUF(this.formINDTO.getUf());
	}
	
	public boolean isFormINComVinculoEpidemiologico(){
		boolean resultado = false;
		if (this.formIN.getMotivoInicialDaInvestigacao() == null)
			return resultado;
		else
			resultado = this.formIN.getMotivoInicialDaInvestigacao().equals(MotivoInicialDaInvestigacao.VINCULO_EPIDEMIOLOGICO);
		
		if (!resultado){
			this.formIN.setFormINRelacionado(null);
			this.numeroFormINRelacionado = null;
		}
		
		return resultado;
	}
	
	public boolean isAplica_seMedidasAdotadas(){
		boolean resultado = false;
		if (this.formIN.getAplica_seMedidasAdotadas() == null)
			return resultado;
		else
			resultado = this.formIN.getAplica_seMedidasAdotadas().equals(SimNao.SIM);
		
		if (!resultado){
			this.formIN.getMedidas().clear();
		}
		
		return resultado;
	}
	
	public void abrirPesquisaDeFormIN(){
		this.numeroFormINRelacionado = null;
		this.listaFormINsRelacionados = null;
	}
	
	public void buscarFormIN(){
		this.listaFormINsRelacionados = null;
		
		if (this.formIN.getNumero() != null && this.formIN.getNumero().equals(this.numeroFormINRelacionado)){
			FacesMessageUtil.addWarnContextFacesMessage("O Form IN que voc� est� tentando buscar � o Form IN atual", "");
			return;
		}
		
		FormIN formINRelacionado = null;
		
		formINRelacionado = this.formINService.findByNumero(this.numeroFormINRelacionado);
		this.listaFormINsRelacionados = new ArrayList<FormIN>();
		this.listaFormINsRelacionados.add(formINRelacionado);
	}
	
	public void selecionarFormINRelacionado(FormIN formINRelacionado){
		this.formIN.setFormINRelacionado(formINRelacionado);
		FacesMessageUtil.addInfoContextFacesMessage("Form IN n� " + formINRelacionado.getNumero() + " selecionado", "");
		this.listaFormINsRelacionados = null;
	}
	
	public void abrirPesquisaDeVisitaPropriedadeRural(){
		this.numeroVisitaPropriedadeRural = null;
		this.listaVisitaPropriedadeRural = null;
	}
	
	public void buscarVisitaPropriedadeRural(){
		this.listaFormINsRelacionados = null;
		
		VisitaPropriedadeRural visitaPropriedadeRural = null;
		
		visitaPropriedadeRural = this.visitaPropriedadeRuralService.findByNumero(this.numeroVisitaPropriedadeRural);
		if (visitaPropriedadeRural != null){
			this.listaVisitaPropriedadeRural = new ArrayList<VisitaPropriedadeRural>();
			this.listaVisitaPropriedadeRural.add(visitaPropriedadeRural);
		} else {
			FacesMessageUtil.addWarnContextFacesMessage("Nenhuma visita foi encontrada", "");
		}
	}
	
	public void selecionarVisitaPropriedadeRural(VisitaPropriedadeRural visitaPropriedadeRural){
		this.formIN.setVisitaPropriedadeRural(visitaPropriedadeRural);
		this.setDataAbertura(visitaPropriedadeRural.getDataDaVisita().getTime());

		FacesMessageUtil.addInfoContextFacesMessage("Visita propriedade rural n� " + visitaPropriedadeRural.getNumero() + " selecionada", "");
		this.listaVisitaPropriedadeRural = null;
	}	
	
	public boolean isTelaBuscaPropriedadeAbertaOnLoad(){
		boolean result = false;
		
		if (detalheComunicacaoAIE != null){
			result = true;
			this.nomePropriedade = detalheComunicacaoAIE.getNomePropriedade();
			activeIndexTelaBuscaPropriedade = 1;
		} else if (registroEntradaLASA != null){
			
			
		} else if (detalheFormNotifica != null){
			if (detalheFormNotifica.getPropriedade().getCodigo() == null){
				result = true;
				this.nomePropriedade = detalheFormNotifica.getPropriedade().getNome();
				activeIndexTelaBuscaPropriedade = 1;
			}
		} else if (loteEnfermidadeAbatedouroFrigorifico != null){
			if (loteEnfermidadeAbatedouroFrigorifico.getProprietario().getPropriedade().getCodigo() == null){
				result = true;
				this.nomePropriedade = loteEnfermidadeAbatedouroFrigorifico.getProprietario().getPropriedade().getNome();
				activeIndexTelaBuscaPropriedade = 1;
			}
		}
		
		return result;
	}
	
	public void abrirTelaDeBuscaDePropriedade(){
		this.idPropriedade = null;
		this.nomePropriedade = null;
		this.listaPropriedades = null;
	}
	
	public void buscarPropriedadeCodigo(){
		this.listaPropriedades = null;
		
		Propriedade p;
		try {
			p = propriedadeService.findByCodigoFetchAll(idPropriedade, forceUpdatePropriedade);
			
			if (p != null){
				this.listaPropriedades = new ArrayList<Propriedade>();
				this.listaPropriedades.add(p);
			}
		} catch (ApplicationException e) {
			FacesMessageUtil.addErrorContextFacesMessage("Erro", e.getLocalizedMessage());
			return;
		}
		
		if (this.listaPropriedades == null || this.listaPropriedades.isEmpty())
			FacesMessageUtil.addWarnContextFacesMessage("Nenhuma propriedade foi encontrada", "");
	}
	
	public void buscarPropriedadeNome(){
		this.listaPropriedades = null;
		
		try {
			listaPropriedades = propriedadeService.findByNomeFetchAll(nomePropriedade);
		} catch (ApplicationException e) {
			FacesMessageUtil.addErrorContextFacesMessage("Erro", e.getLocalizedMessage());
			return;
		}
		
		if (this.listaPropriedades == null || this.listaPropriedades.isEmpty())
			FacesMessageUtil.addWarnContextFacesMessage("Nenhuma propriedade foi encontrada", "");
	}
	
	public void selecionarPropriedade(Propriedade propriedade){
		this.formIN.setPropriedade(propriedade);
		this.formIN.setMunicipio(this.formIN.getPropriedade().getMunicipio());
		this.formIN.setNomePropriedade(propriedade.getNome());
		this.formIN.setCodigoPropriedade(propriedade.getCodigoPropriedade());
		
		FacesMessageUtil.addInfoContextFacesMessage("Propriedade " + this.formIN.getPropriedade().getNome() + " selecionada", "");
	}
	
	public void abrirTelaDeBuscaDeVeterinario(){
		this.cpfVeterinario = null;
		this.crmvVeterinario = null;
		this.listaVeterinarios = null;
	}
	
	public void buscarVeterinarioPorCpf(){
		this.listaVeterinarios = null;
		
		Veterinario v;
		try {
			v = clientWebServiceVeterinario.getVeterinarioByCpf(cpfVeterinario);
			
			if (v != null){
				this.listaVeterinarios = new ArrayList<Veterinario>();
				this.listaVeterinarios.add(v);
			}
		} catch (ApplicationException e) {
			FacesMessageUtil.addErrorContextFacesMessage("Erro", e.getLocalizedMessage());
			return;
		}
		
		if (this.listaVeterinarios == null || this.listaVeterinarios.isEmpty())
			FacesMessageUtil.addWarnContextFacesMessage("Nenhum Veterin�rio foi encontrado", "");
	}
	
	public void buscarVeterinarioPorCrmv(){
		this.listaVeterinarios = null;
		
		Veterinario v;
		try {
			v = clientWebServiceVeterinario.getVeterinarioByCRMV(crmvVeterinario);
			
			if (v != null){
				this.listaVeterinarios = new ArrayList<Veterinario>();
				this.listaVeterinarios.add(v);
			}
		} catch (ApplicationException e) {
			FacesMessageUtil.addErrorContextFacesMessage("Erro", e.getLocalizedMessage());
			return;
		}
		
		if (this.listaVeterinarios == null || this.listaVeterinarios.isEmpty())
			FacesMessageUtil.addWarnContextFacesMessage("Nenhum Veterin�rio foi encontrado", "");
	}
	
	public void selecionarVeterinario(Veterinario veterinario){
		this.formIN.setVeterinario(veterinario);
		FacesMessageUtil.addInfoContextFacesMessage("Veterinario " + this.formIN.getVeterinario().getNome() + " selecionado", "");
	}
	
	public void abrirTelaDeBuscaDeGta(){
		this.numeroGta = null;
		this.serieGta = null;
		this.listaGtas = null;
	}
	
	public void buscarGtaPorNumeroESerie(){
		this.listaGtas = null;
		
		try {
			Gta gta = clientWebServiceGTA.getGTAByNumeroSerie(numeroGta, serieGta);
			this.listaGtas = new ArrayList<Gta>();
			if (gta != null)
				this.listaGtas.add(gta);
		} catch (ApplicationException e) {
			FacesMessageUtil.addErrorContextFacesMessage("Erro", e.getLocalizedMessage());
			return;
		}
		
		if (this.listaGtas == null || this.listaGtas.isEmpty())
			FacesMessageUtil.addWarnContextFacesMessage("Nenhum gta foi encontrado", "");
	}
	
	public void buscarGtaPorData(){
		this.listaGtas = new ArrayList<Gta>();
		
		try {
			if (this.listaTipoTransitoDeAnimais.contains(TipoTransitoDeAnimais.EGRESSO))
				this.listaGtas.addAll(clientWebServiceGTA.getGTAByPropriedadeDeOrigemEPeriodoDeEntrada(this.formIN.getPropriedade().getCodigo(), dataInicialBuscaGta, dataFinalBuscaGta));
			
			if (this.listaTipoTransitoDeAnimais.contains(TipoTransitoDeAnimais.INGRESSO))
				this.listaGtas.addAll(clientWebServiceGTA.getGTAByPropriedadeDeDestinoEPeriodoDeEntrada(this.formIN.getPropriedade().getCodigo(), dataInicialBuscaGta, dataFinalBuscaGta));
			
			Collections.sort(this.listaGtas);
		} catch (ApplicationException e) {
			FacesMessageUtil.addErrorContextFacesMessage("Erro", e.getLocalizedMessage());
			return;
		}
		
		if (this.listaGtas == null || this.listaGtas.isEmpty())
			FacesMessageUtil.addWarnContextFacesMessage("Nenhum gta foi encontrado", "");
	}
	
	public void selecionarGtas(){
		TransitoDeAnimais transitoDeAnimais = null;
		
		if (listaGtas != null && !listaGtas.isEmpty()){
			for (Gta gta : listaGtas) {
				if (gta.isSelecionadoParaInclusao()){
					transitoDeAnimais = new TransitoDeAnimais(gta);
					transitoDeAnimais.setFormIN(formIN);
					
					if (!this.formIN.getTransitoDeAnimais().contains(transitoDeAnimais))
						this.formIN.getTransitoDeAnimais().add(transitoDeAnimais);
				}
			}
		}
	}
	
	public List<Produtor> getListaProdutoresDaPropriedade(){
		HashSet<Produtor> set = new HashSet<Produtor>();
		
		if (this.formIN.getPropriedade() != null){
			if (this.formIN.getPropriedade().getProprietarios() != null)
				set.addAll(this.formIN.getPropriedade().getProprietarios());
			
			if (this.formIN.getPropriedade().getExploracaos() != null)
				for (int i = 0; i < this.formIN.getPropriedade().getExploracaos().size(); i++) {
					if (this.formIN.getPropriedade().getExploracaos().get(i).getProdutores() != null)
						set.addAll(this.formIN.getPropriedade().getExploracaos().get(i).getProdutores());
				}
		}
		
		List<Produtor> lista = new ArrayList<Produtor>(set);
		return lista;
	}
	
	public void selecionarProdutor(Produtor produtor) throws CloneNotSupportedException{
		this.formIN.setProprietario((Produtor) produtor.clone());
		this.formIN.getRepresentanteEstabelecimento().setNome(this.formIN.getProprietario().getNome());
		
		if (this.formIN.getProprietario().getEndereco() != null)
			this.formIN.getRepresentanteEstabelecimento().setTelefoneFixo(this.formIN.getProprietario().getEndereco().getTelefone());
		
		FacesMessageUtil.addInfoContextFacesMessage("Propriet�rio " + this.formIN.getProprietario().getNome() + " selecionado", "");
	}
	
	public String getNumeroFormINRelacionado() {
		return numeroFormINRelacionado;
	}

	public void setNumeroFormINRelacionado(String numeroFormINRelacionado) {
		this.numeroFormINRelacionado = numeroFormINRelacionado;
	}

	public FormIN getFormIN() {
		return formIN;
	}
	
	public void setFormIN(FormIN formIN) {
		this.formIN = formIN;
	}
	
	public Long getIdPropriedade() {
		return idPropriedade;
	}

	public void setIdPropriedade(Long idPropriedade) {
		this.idPropriedade = idPropriedade;
	}

	public String getNomePropriedade() {
		return nomePropriedade;
	}

	public void setNomePropriedade(String nomePropriedade) {
		this.nomePropriedade = nomePropriedade;
	}

	public List<Propriedade> getListaPropriedades() {
		return listaPropriedades;
	}

	public void setListaPropriedades(List<Propriedade> listaPropriedades) {
		this.listaPropriedades = listaPropriedades;
	}

	/*
	 * Segunda p�gina do cadastro
	 */

	public void showFormBovinos(){
		if (this.formIN.getBovinos() == null)
			this.formIN.setBovinos(new InformacoesBovinos());
		
		this.formIN.getBovinos().setFormIN(formIN);
		
		List<FaixaEtariaOuEspecie> faixas = new ArrayList<FaixaEtariaOuEspecie>();
		for (DetalhesInformacoesBovinos detalhe : this.formIN.getBovinos().getDetalhes()) {
			faixas.add(detalhe.getFaixaEtaria());
		}
		
		DetalhesInformacoesBovinos info;
		for (FaixaEtariaOuEspecie faixaEtaria : FaixaEtariaOuEspecie.getBovinos()) {
			if (!faixas.contains(faixaEtaria)){
				info = new DetalhesInformacoesBovinos();
				info.setInformacoesBovinos(this.formIN.getBovinos());
				info.setFaixaEtaria(faixaEtaria);
				
				this.formIN.getBovinos().getDetalhes().add(info);
			}
		}
	}
	
	public void showFormBubalinos(){
		if (this.formIN.getBubalinos() == null)
			this.formIN.setBubalinos(new InformacoesBubalinos());
		
		this.formIN.getBubalinos().setFormIN(formIN);
		
		List<FaixaEtariaOuEspecie> faixas = new ArrayList<FaixaEtariaOuEspecie>();
		for (DetalhesInformacoesBubalinos detalhe : this.formIN.getBubalinos().getDetalhes()) {
			faixas.add(detalhe.getFaixaEtaria());
		}
		
		DetalhesInformacoesBubalinos info;
		for (FaixaEtariaOuEspecie faixaEtaria : FaixaEtariaOuEspecie.getBubalinos()) {
			if (!faixas.contains(faixaEtaria)){
				info = new DetalhesInformacoesBubalinos();
				info.setInformacoesBubalinos(this.formIN.getBubalinos());
				info.setFaixaEtaria(faixaEtaria);
				
				this.formIN.getBubalinos().getDetalhes().add(info);
			}
		}
	}
	
	public void showFormCaprinos(){
		if (this.formIN.getCaprinos() == null)
			this.formIN.setCaprinos(new InformacoesCaprinos());
		
		this.formIN.getCaprinos().setFormIN(formIN);
		
		List<FaixaEtariaOuEspecie> faixas = new ArrayList<FaixaEtariaOuEspecie>();
		for (DetalhesInformacoesCaprinos detalhe : this.formIN.getCaprinos().getDetalhes()) {
			faixas.add(detalhe.getFaixaEtaria());
		}
		
		DetalhesInformacoesCaprinos info;
		for (FaixaEtariaOuEspecie faixaEtaria : FaixaEtariaOuEspecie.getCaprinos()) {
			if (!faixas.contains(faixaEtaria)){
				info = new DetalhesInformacoesCaprinos();
				info.setInformacoesCaprinos(this.formIN.getCaprinos());
				info.setFaixaEtaria(faixaEtaria);
				
				this.formIN.getCaprinos().getDetalhes().add(info);
			}
		}
	}
	
	public void showFormOvinos(){
		if (this.formIN.getOvinos() == null)
			this.formIN.setOvinos(new InformacoesOvinos());
		
		this.formIN.getOvinos().setFormIN(formIN);
		
		List<FaixaEtariaOuEspecie> faixas = new ArrayList<FaixaEtariaOuEspecie>();
		for (DetalhesInformacoesOvinos detalhe : this.formIN.getOvinos().getDetalhes()) {
			faixas.add(detalhe.getFaixaEtaria());
		}
		
		DetalhesInformacoesOvinos info;
		for (FaixaEtariaOuEspecie faixaEtaria : FaixaEtariaOuEspecie.getOvinos()) {
			if (!faixas.contains(faixaEtaria)){
				info = new DetalhesInformacoesOvinos();
				info.setInformacoesOvinos(this.formIN.getOvinos());
				info.setFaixaEtaria(faixaEtaria);
				
				this.formIN.getOvinos().getDetalhes().add(info);
			}
		}
	}
	
	public void showFormSuinos(){
		if (this.formIN.getSuinos() == null)
			this.formIN.setSuinos(new InformacoesSuinos());
		
		this.formIN.getSuinos().setFormIN(formIN);
		
		List<FaixaEtariaOuEspecie> faixas = new ArrayList<FaixaEtariaOuEspecie>();
		for (DetalhesInformacoesSuinos detalhe : this.formIN.getSuinos().getDetalhes()) {
			faixas.add(detalhe.getFaixaEtaria());
		}
		
		DetalhesInformacoesSuinos info;
		for (FaixaEtariaOuEspecie faixaEtaria : FaixaEtariaOuEspecie.getSuinos()) {
			if (!faixas.contains(faixaEtaria)){
				info = new DetalhesInformacoesSuinos();
				info.setInformacoesSuinos(this.formIN.getSuinos());
				info.setFaixaEtaria(faixaEtaria);
				
				this.formIN.getSuinos().getDetalhes().add(info);
			}
		}
	}
	
	public void showFormEquinos(){
		if (this.formIN.getEquinos() == null)
			this.formIN.setEquinos(new InformacoesEquinos());
		
		this.formIN.getEquinos().setFormIN(formIN);
		
		List<FaixaEtariaOuEspecie> faixas = new ArrayList<FaixaEtariaOuEspecie>();
		for (DetalhesInformacoesEquinos detalhe : this.formIN.getEquinos().getDetalhes()) {
			faixas.add(detalhe.getFaixaEtaria());
		}
		
		DetalhesInformacoesEquinos info;
		for (FaixaEtariaOuEspecie faixaEtaria : FaixaEtariaOuEspecie.getEquinos()) {
			if (!faixas.contains(faixaEtaria)){
				info = new DetalhesInformacoesEquinos();
				info.setInformacoesEquinos(this.formIN.getEquinos());
				info.setFaixaEtaria(faixaEtaria);
				
				this.formIN.getEquinos().getDetalhes().add(info);
			}
		}
	}
	
	public void showFormAsininos(){
		if (this.formIN.getAsininos() == null)
			this.formIN.setAsininos(new InformacoesAsininos());
		
		this.formIN.getAsininos().setFormIN(formIN);
		
		List<FaixaEtariaOuEspecie> faixas = new ArrayList<FaixaEtariaOuEspecie>();
		for (DetalhesInformacoesAsininos detalhe : this.formIN.getAsininos().getDetalhes()) {
			faixas.add(detalhe.getFaixaEtaria());
		}
		
		DetalhesInformacoesAsininos info;
		for (FaixaEtariaOuEspecie faixaEtaria : FaixaEtariaOuEspecie.getAsininos()) {
			if (!faixas.contains(faixaEtaria)){
				info = new DetalhesInformacoesAsininos();
				info.setInformacoesAsininos(this.formIN.getAsininos());
				info.setFaixaEtaria(faixaEtaria);
				
				this.formIN.getAsininos().getDetalhes().add(info);
			}
		}
	}
	
	public void showFormMuares(){
		if (this.formIN.getMuares() == null)
			this.formIN.setMuares(new InformacoesMuares());
		
		this.formIN.getMuares().setFormIN(formIN);
		
		List<FaixaEtariaOuEspecie> faixas = new ArrayList<FaixaEtariaOuEspecie>();
		for (DetalhesInformacoesMuares detalhe : this.formIN.getMuares().getDetalhes()) {
			faixas.add(detalhe.getFaixaEtaria());
		}
		
		DetalhesInformacoesMuares info;
		for (FaixaEtariaOuEspecie faixaEtaria : FaixaEtariaOuEspecie.getMuares()) {
			if (!faixas.contains(faixaEtaria)){
				info = new DetalhesInformacoesMuares();
				info.setInformacoesMuares(this.formIN.getMuares());
				info.setFaixaEtaria(faixaEtaria);
				
				this.formIN.getMuares().getDetalhes().add(info);
			}
		}
	}
	
	public void showFormAves(){
		if (this.formIN.getAves() == null)
			this.formIN.setAves(new InformacoesAves());
		
		this.formIN.getAves().setFormIN(formIN);
		
		List<FaixaEtariaOuEspecie> faixas = new ArrayList<FaixaEtariaOuEspecie>();
		for (DetalhesInformacoesAves detalhe : this.formIN.getAves().getDetalhes()) {
			faixas.add(detalhe.getFaixaEtaria());
		}
		
		DetalhesInformacoesAves info;
		for (FaixaEtariaOuEspecie faixaEtaria : FaixaEtariaOuEspecie.getAves()) {
			if (!faixas.contains(faixaEtaria)){
				info = new DetalhesInformacoesAves();
				info.setInformacoesAves(this.formIN.getAves());
				info.setFaixaEtaria(faixaEtaria);
				
				this.formIN.getAves().getDetalhes().add(info);
			}
		}
	}
	
	public void showFormAbelhas(){
		if (this.formIN.getAbelhas() == null)
			this.formIN.setAbelhas(new InformacoesAbelhas());
		
		this.formIN.getAbelhas().setFormIN(formIN);
		
		List<FaixaEtariaOuEspecie> faixas = new ArrayList<FaixaEtariaOuEspecie>();
		for (DetalhesInformacoesAbelhas detalhe : this.formIN.getAbelhas().getDetalhes()) {
			faixas.add(detalhe.getFaixaEtaria());
		}
		
		DetalhesInformacoesAbelhas info;
		for (FaixaEtariaOuEspecie faixaEtaria : FaixaEtariaOuEspecie.getAbelhas()) {
			if (!faixas.contains(faixaEtaria)){
				info = new DetalhesInformacoesAbelhas();
				info.setInformacoesAbelhas(this.formIN.getAbelhas());
				info.setFaixaEtaria(faixaEtaria);
				
				this.formIN.getAbelhas().getDetalhes().add(info);
			}
		}
	}
	
	public void showFormLagomorfos(){
		if (this.formIN.getLagomorfos() == null)
			this.formIN.setLagomorfos(new InformacoesLagomorfos());
		
		this.formIN.getLagomorfos().setFormIN(formIN);
		
		List<FaixaEtariaOuEspecie> faixas = new ArrayList<FaixaEtariaOuEspecie>();
		for (DetalhesInformacoesLagomorfos detalhe : this.formIN.getLagomorfos().getDetalhes()) {
			faixas.add(detalhe.getFaixaEtaria());
		}
		
		DetalhesInformacoesLagomorfos info;
		for (FaixaEtariaOuEspecie faixaEtaria : FaixaEtariaOuEspecie.getLagomorfos()) {
			if (!faixas.contains(faixaEtaria)){
				info = new DetalhesInformacoesLagomorfos();
				info.setInformacoesLagomorfos(this.formIN.getLagomorfos());
				info.setFaixaEtaria(faixaEtaria);
				
				this.formIN.getLagomorfos().getDetalhes().add(info);
			}
		}
	}
	
	public void showFormOutrosAnimais(){
		if (this.formIN.getOutrosAnimais() == null)
			this.formIN.setOutrosAnimais(new InformacoesOutrosAnimais());
		
		this.formIN.getOutrosAnimais().setFormIN(formIN);
		
		List<FaixaEtariaOuEspecie> faixas = new ArrayList<FaixaEtariaOuEspecie>();
		for (DetalhesInformacoesOutrosAnimais detalhe : this.formIN.getOutrosAnimais().getDetalhes()) {
			faixas.add(detalhe.getFaixaEtaria());
		}
		
		DetalhesInformacoesOutrosAnimais info;
		for (FaixaEtariaOuEspecie faixaEtaria : FaixaEtariaOuEspecie.getOutrosAnimais()) {
			if (!faixas.contains(faixaEtaria)){
				info = new DetalhesInformacoesOutrosAnimais();
				info.setInformacoesOutrosAnimais(this.formIN.getOutrosAnimais());
				info.setFaixaEtaria(faixaEtaria);
				
				this.formIN.getOutrosAnimais().getDetalhes().add(info);
			}
		}
	}
	
	/*
	 * Terceira p�gina do cadastro
	 */
	
	private VacinacaoFormIN vacinacao = new VacinacaoFormIN();
	
	private Medicacao medicacao = new Medicacao();
	
	
	private boolean editandoVacinacao = false;
	
	private boolean visualizandoVacinacao = false;
	
	private boolean editandoMedicacao = false;
	
	private boolean visualizandoMedicacao = false;
	
	public void adicionarVacinacao(){
		this.vacinacao.setFormIN(formIN);
		
		if (this.dataVacinacao != null){
			if (this.vacinacao.getData() == null)
				this.vacinacao.setData(Calendar.getInstance());
			this.vacinacao.getData().setTime(dataVacinacao);
		}else
			this.vacinacao.setData(null);
		this.dataVacinacao = null;
		
		if (!editandoVacinacao)
			this.formIN.getVacinas().add(vacinacao);
		else
			editandoVacinacao = false;
		
		this.vacinacao = new VacinacaoFormIN();
		this.dataVacinacao = null;
		
		FacesMessageUtil.addInfoContextFacesMessage("Vacina��o inclu�do com sucesso", "");
	}
	
	public void novaVacinacao(){
		this.vacinacao= new VacinacaoFormIN();
		this.editandoVacinacao = false;
		this.visualizandoVacinacao = false;

		this.dataVacinacao = null;
	}
	
	public void removerVacinacaoFormIN(VacinacaoFormIN vacinacao){
		this.formIN.getVacinas().remove(vacinacao);
	}
	
	public void editarVacinacao(VacinacaoFormIN vacinacao){
		this.editandoVacinacao = true;
		this.visualizandoVacinacao = false;
		this.vacinacao = vacinacao;
		
		if (this.vacinacao.getData() != null)
			this.dataVacinacao = this.vacinacao.getData().getTime();
	}
	
	public void visualizarVacinacao(VacinacaoFormIN vacinacao){
		this.visualizandoVacinacao = true;
		this.editandoVacinacao = false;
		this.vacinacao = vacinacao;
		
		if (this.vacinacao.getData() != null)
			this.dataVacinacao = this.vacinacao.getData().getTime();
	}
	
	public void adicionarMedicacao(){
		this.medicacao.setFormIN(formIN);

		if (this.dataFinalMedicacao != null){
			if (this.medicacao.getDataFinal() == null)
				this.medicacao.setDataFinal(Calendar.getInstance());
			this.medicacao.getDataFinal().setTime(dataFinalMedicacao);
		}else
			this.medicacao.setDataFinal(null);
		this.dataFinalMedicacao = null;
		
		if (this.dataInicialMedicacao != null){
			if (this.medicacao.getDataInicial() == null)
				this.medicacao.setDataInicial(Calendar.getInstance());
			this.medicacao.getDataInicial().setTime(dataInicialMedicacao);
		}else
			this.medicacao.setDataInicial(null);
		this.dataInicialMedicacao = null;
		
		if (!editandoMedicacao)
			this.formIN.getMedicacoes().add(medicacao);
		else
			editandoMedicacao = false;
		
		this.medicacao = new Medicacao();
		
		FacesMessageUtil.addInfoContextFacesMessage("Medica��o inclu�da com sucesso", "");
	}
	
	public void novaMedicacao(){
		this.medicacao= new Medicacao();
		this.editandoMedicacao = false;
		this.visualizandoMedicacao = false;

		this.dataInicialMedicacao = null;
		this.dataFinalMedicacao = null;
	}
	
	public void removerMedicacao(Medicacao medicacao){
		this.formIN.getMedicacoes().remove(medicacao);
	}
	
	public void editarMedicacao(Medicacao medicacao){
		this.editandoMedicacao = true;
		this.visualizandoMedicacao = false;
		this.medicacao = medicacao;
		
		if (this.medicacao.getDataFinal() != null)
			this.dataFinalMedicacao = this.medicacao.getDataFinal().getTime();
		if (this.medicacao.getDataInicial() != null)
			this.dataInicialMedicacao = this.medicacao.getDataInicial().getTime();
	}
	
	public void visualizarMedicacao(Medicacao medicacao){
		this.visualizandoMedicacao = true;
		this.editandoMedicacao = false;
		this.medicacao = medicacao;
		
		if (this.medicacao.getDataFinal() != null)
			this.dataFinalMedicacao = this.medicacao.getDataFinal().getTime();
		if (this.medicacao.getDataInicial() != null)
			this.dataInicialMedicacao = this.medicacao.getDataInicial().getTime();
	}
	
	public void novoTransitoDeAnimais(){
		this.listaGtas = null;
		this.numeroGta = null;
		this.serieGta = null;
		this.listaTipoTransitoDeAnimais = null;
		
		Calendar dataInicial = Calendar.getInstance();
		dataInicial.set(Calendar.MONTH, dataInicial.get(Calendar.MONTH) - 2);
		this.dataInicialBuscaGta = dataInicial.getTime();
		this.dataFinalBuscaGta = Calendar.getInstance().getTime();
	}
	
	public void removerTransitoDeAnimais(TransitoDeAnimais transitoDeAnimais){
		this.formIN.getTransitoDeAnimais().remove(transitoDeAnimais);
	}
	
	public boolean isFormINSendoRetificado(){
		if (this.formIN.getStatusFormIN().equals(StatusFormIN.VALIDADO) || this.formIN.getStatusFormIN().equals(StatusFormIN.ALTERADO))
			return true;
		else
			return false;
	}
	
	public boolean isDoencaDeVigilanciaSindromicaPrevistaNoFormIN(){
		boolean result = false;
		
		if (this.formIN.getDoencaDeVigilanciaSindromica() == null)
			return true;
		
		if (!this.formIN.getDoencaDeVigilanciaSindromica().equals(VigilanciaSindromica.NENHUMA_DAS_ANTERIORES) &&
			!this.formIN.getDoencaDeVigilanciaSindromica().equals(VigilanciaSindromica.PATOLOGIA_GRANULOMATOSA_INTERNA) &&
			!this.formIN.getDoencaDeVigilanciaSindromica().equals(VigilanciaSindromica.PATOLOGIA_REPRODUTIVA_DOS_BOVINOS) &&
			!this.formIN.getDoencaDeVigilanciaSindromica().equals(VigilanciaSindromica.ANEMIA_INFECCIOSA_EQUINA) &&
			!this.formIN.getDoencaDeVigilanciaSindromica().equals(VigilanciaSindromica.DOENCA_RESPIRATORIA_DOS_EQUINOS)){
			result = true; 
		}
		
		if (result){
			this.tipoDiagnostico = null;
			this.formIN.setDianosticoConclusivo(null);
			this.formIN.setDianosticoProvavel(null);
		}
		
		return result;
	}
	
	public boolean isNenhumaVigilanciaSindromica(){
		boolean result = false;
		
		if (this.formIN.getDoencaDeVigilanciaSindromica() == null)
			return true;
		
		if (!this.formIN.getDoencaDeVigilanciaSindromica().equals(VigilanciaSindromica.NENHUMA_DAS_ANTERIORES)){
			result = true; 
		}
		
		return result;
	}
	
	public boolean isTipoDiagnosticoProvavel(){
		if (this.tipoDiagnostico == null)
			return false;
		if (this.tipoDiagnostico.equals(TipoDiagnostico.PROVAVEL)){
			this.formIN.setDianosticoConclusivo(null);
			return true;
		}
		return false;
	}
	
	public boolean isTipoDiagnosticoConclusivo(){
		if (this.tipoDiagnostico == null)
			return false;
		if (this.tipoDiagnostico.equals(TipoDiagnostico.CONCLUSIVO)){
			this.formIN.setDianosticoProvavel(null);
			return true;
		}
		return false;
	}
	
	public boolean isDiagnosticoLaboratorialIgualSim(){
		if (this.diagnosticoLaboratorial == null)
			return false;
		if (this.diagnosticoLaboratorial.equals(SimNao.SIM)){
			this.formIN.setDianosticoProvavel(null);
			return true;
		}else{
			this.dataResultadoDiagnosticoLaboratorial = null;
			this.formIN.setIdentificacaoDiagnosticoLaboratorial(null);
			this.formIN.setTipoTesteLaboratorial(null);
		}
			
		return false;
	}
	
	public void limparTipoDiagnostico() {
		this.tipoDiagnostico = null;
		this.formIN.setDianosticoConclusivo(null);
		this.formIN.setDianosticoProvavel(null);
		this.diagnosticoLaboratorial = null;
		this.dataResultadoDiagnosticoLaboratorial = null;
		this.formIN.setTipoTesteLaboratorial(null);
		this.formIN.setIdentificacaoDiagnosticoLaboratorial(null);
	}
	
	public boolean isListaFormSVEmpty(){
		if (this.formIN.getListaFormSV() != null && !this.formIN.getListaFormSV().isEmpty())
			return false;
		else
			return true;
	}
	
	public boolean isListaFormSHEmpty(){
		if (this.formIN.getListaFormSH() != null && !this.formIN.getListaFormSH().isEmpty())
			return false;
		else
			return true;
	}
	
	public boolean isListaFormSRNEmpty(){
		if (this.formIN.getListaFormSRN() != null && !this.formIN.getListaFormSRN().isEmpty())
			return false;
		else
			return true;
	}
	
	public boolean isListaFormLABEmpty(){
		if (this.formIN.getListaFormLAB() != null && !this.formIN.getListaFormLAB().isEmpty())
			return false;
		else
			return true;
	}
	
	public boolean isListaFormEQEmpty(){
		if (this.formIN.getListaFormEQ() != null && !this.formIN.getListaFormEQ().isEmpty())
			return false;
		else
			return true;
	}
	
	public boolean isListaFolhasAdicionaisEmpty(){
		if (this.formIN.getListaFolhasAdicionais() != null && !this.formIN.getListaFolhasAdicionais().isEmpty())
			return false;
		else
			return true;
	}
	
	public boolean isListaFormSNEmpty(){
		if (this.formIN.getListaFormSN() != null && !this.formIN.getListaFormSN().isEmpty())
			return false;
		else
			return true;
	}
	
	public boolean isListaFormAIEEmpty(){
		if (this.formIN.getListaFormAIE() != null && !this.formIN.getListaFormAIE().isEmpty())
			return false;
		else
			return true;
	}
	
	public boolean isListaFormMormoEmpty(){
		if (this.formIN.getListaFormMormo() != null && !this.formIN.getListaFormMormo().isEmpty())
			return false;
		else
			return true;
	}
	
	public boolean isListaFormMaleinaEmpty(){
		if (this.formIN.getListaFormMaleina() != null && !this.formIN.getListaFormMaleina().isEmpty())
			return false;
		else
			return true;
	}
	
	public boolean isListaResenhoEmpty(){
		if (this.formIN.getListaResenho() != null && !this.formIN.getListaResenho().isEmpty())
			return false;
		else
			return true;
	}
	
	public VacinacaoFormIN getVacinacao() {
		return vacinacao;
	}

	public void setVacinacao(VacinacaoFormIN vacinacao) {
		this.vacinacao = vacinacao;
	}

	public Medicacao getMedicacao() {
		return medicacao;
	}

	public void setMedicacao(Medicacao medicacao) {
		this.medicacao = medicacao;
	}

	public boolean isVisualizandoVacinacao() {
		return visualizandoVacinacao;
	}

	public boolean isEditandoVacinacao() {
		return editandoVacinacao;
	}

	public void setEditandoVacinacao(boolean editandoVacinacao) {
		this.editandoVacinacao = editandoVacinacao;
	}

	public boolean isEditandoMedicacao() {
		return editandoMedicacao;
	}

	public void setEditandoMedicacao(boolean editandoMedicacao) {
		this.editandoMedicacao = editandoMedicacao;
	}

	public void setVisualizandoVacinacao(boolean visualizandoVacinacao) {
		this.visualizandoVacinacao = visualizandoVacinacao;
	}

	public void setVisualizandoMedicacao(boolean visualizandoMedicacao) {
		this.visualizandoMedicacao = visualizandoMedicacao;
	}

	public boolean isVisualizandoMedicacao() {
		return visualizandoMedicacao;
	}

	public Date getDataDaNotificacao() {
		return dataDaNotificacao;
	}

	public void setDataDaNotificacao(Date dataDaNotificacao) {
		this.dataDaNotificacao = dataDaNotificacao;
	}

	public Date getDataAbertura() {
		return dataAbertura;
	}

	public void setDataAbertura(Date dataAbertura) {
		this.dataAbertura = dataAbertura;
	}

	public Date getDataInicioEvento() {
		return dataInicioEvento;
	}

	public void setDataInicioEvento(Date dataInicioEvento) {
		this.dataInicioEvento = dataInicioEvento;
	}

	public Date getDataVacinacao() {
		return dataVacinacao;
	}

	public void setDataVacinacao(Date dataVacinacao) {
		this.dataVacinacao = dataVacinacao;
	}

	public Date getDataInicialMedicacao() {
		return dataInicialMedicacao;
	}

	public void setDataInicialMedicacao(Date dataInicialMedicacao) {
		this.dataInicialMedicacao = dataInicialMedicacao;
	}

	public Date getDataFinalMedicacao() {
		return dataFinalMedicacao;
	}

	public void setDataFinalMedicacao(Date dataFinalMedicacao) {
		this.dataFinalMedicacao = dataFinalMedicacao;
	}

	public Date getDataRetificacao() {
		return dataRetificacao;
	}

	public void setDataRetificacao(Date dataRetificacao) {
		this.dataRetificacao = dataRetificacao;
	}

	public String getCpfVeterinario() {
		return cpfVeterinario;
	}

	public void setCpfVeterinario(String cpfVeterinario) {
		this.cpfVeterinario = cpfVeterinario;
	}

	public String getCrmvVeterinario() {
		return crmvVeterinario;
	}

	public void setCrmvVeterinario(String crmvVeterinario) {
		this.crmvVeterinario = crmvVeterinario;
	}

	public List<Veterinario> getListaVeterinarios() {
		return listaVeterinarios;
	}

	public void setListaVeterinarios(List<Veterinario> listaVeterinarios) {
		this.listaVeterinarios = listaVeterinarios;
	}

	public Integer getNumeroGta() {
		return numeroGta;
	}

	public void setNumeroGta(Integer numeroGta) {
		this.numeroGta = numeroGta;
	}

	public String getSerieGta() {
		return serieGta;
	}

	public void setSerieGta(String serieGta) {
		this.serieGta = serieGta;
	}

	public List<Gta> getListaGtas() {
		return listaGtas;
	}

	public void setListaGtas(List<Gta> listaGtas) {
		this.listaGtas = listaGtas;
	}

	public List<FormIN> getListaFormINsRelacionados() {
		return listaFormINsRelacionados;
	}

	public void setListaFormINsRelacionados(List<FormIN> listaFormINsRelacionados) {
		this.listaFormINsRelacionados = listaFormINsRelacionados;
	}

	public List<TipoTransitoDeAnimais> getListaTipoTransitoDeAnimais() {
		return listaTipoTransitoDeAnimais;
	}

	public void setListaTipoTransitoDeAnimais(
			List<TipoTransitoDeAnimais> listaTipoTransitoDeAnimais) {
		this.listaTipoTransitoDeAnimais = listaTipoTransitoDeAnimais;
	}

	public Date getDataInicialBuscaGta() {
		return dataInicialBuscaGta;
	}

	public void setDataInicialBuscaGta(Date dataInicialBuscaGta) {
		this.dataInicialBuscaGta = dataInicialBuscaGta;
	}

	public Date getDataFinalBuscaGta() {
		return dataFinalBuscaGta;
	}

	public void setDataFinalBuscaGta(Date dataFinalBuscaGta) {
		this.dataFinalBuscaGta = dataFinalBuscaGta;
	}

	public TipoCoordenadaGeografica getTipoCoordenadaGeografica() {
		return tipoCoordenadaGeografica;
	}

	public void setTipoCoordenadaGeografica(
			TipoCoordenadaGeografica tipoCoordenadaGeografica) {
		this.tipoCoordenadaGeografica = tipoCoordenadaGeografica;
	}

	public String getNumeroVisitaPropriedadeRural() {
		return numeroVisitaPropriedadeRural;
	}

	public void setNumeroVisitaPropriedadeRural(String numeroVisitaPropriedadeRural) {
		this.numeroVisitaPropriedadeRural = numeroVisitaPropriedadeRural;
	}

	public List<VisitaPropriedadeRural> getListaVisitaPropriedadeRural() {
		return listaVisitaPropriedadeRural;
	}

	public void setListaVisitaPropriedadeRural(
			List<VisitaPropriedadeRural> listaVisitaPropriedadeRural) {
		this.listaVisitaPropriedadeRural = listaVisitaPropriedadeRural;
	}

	public TipoDiagnostico getTipoDiagnostico() {
		return tipoDiagnostico;
	}

	public void setTipoDiagnostico(TipoDiagnostico tipoDiagnostico) {
		this.tipoDiagnostico = tipoDiagnostico;
	}

	public SimNao getDiagnosticoLaboratorial() {
		return diagnosticoLaboratorial;
	}

	public void setDiagnosticoLaboratorial(SimNao diagnosticoLaboratorial) {
		this.diagnosticoLaboratorial = diagnosticoLaboratorial;
	}

	public Date getDataResultadoDiagnosticoLaboratorial() {
		return dataResultadoDiagnosticoLaboratorial;
	}

	public void setDataResultadoDiagnosticoLaboratorial(
			Date dataResultadoDiagnosticoLaboratorial) {
		this.dataResultadoDiagnosticoLaboratorial = dataResultadoDiagnosticoLaboratorial;
	}

	public LazyObjectDataModel<FormIN> getListaFormIN() {
		return listaFormIN;
	}

	public void setListaFormIN(LazyObjectDataModel<FormIN> listaFormIN) {
		this.listaFormIN = listaFormIN;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public FormINDTO getFormINDTO() {
		return formINDTO;
	}

	public void setFormINDTO(FormINDTO formINDTO) {
		this.formINDTO = formINDTO;
	}

	public Long getIdHistorico() {
		return idHistorico;
	}

	public void setIdHistorico(Long idHistorico) {
		this.idHistorico = idHistorico;
	}
	
	public boolean isForceUpdatePropriedade() {
		return forceUpdatePropriedade;
	}

	public Date getDataSISBRAVET() {
		return dataSISBRAVET;
	}

	public void setDataSISBRAVET(Date dataSISBRAVET) {
		this.dataSISBRAVET = dataSISBRAVET;
	}

	public void setForceUpdatePropriedade(boolean forceUpdatePropriedade) {
		this.forceUpdatePropriedade = forceUpdatePropriedade;
	}

	/*
	 * 
	 */
	
	public int getActiveIndexTelaBuscaPropriedade() {
		activeIndexTelaBuscaPropriedade = this.isTelaBuscaPropriedadeAbertaOnLoad() ? 1 : 0;
		
		return activeIndexTelaBuscaPropriedade;
	}

	public void setActiveIndexTelaBuscaPropriedade(int activeIndexTelaBuscaPropriedade) {
		this.activeIndexTelaBuscaPropriedade = activeIndexTelaBuscaPropriedade;
	}

	public String onFlowProcess(FlowEvent event) {
		RequestContext context = RequestContext.getCurrentInstance();
		context.update("pgrowl");
		
		String nextStep = event.getNewStep();
			
		if (nextStep.equals("dois")){
			if (!this.isPropriedadeNull()){
				
				return event.getOldStep();
			}
			this.inicializarInformacoesDeAnimais();
			
		} 
		
		return nextStep;
    }

}