package br.gov.mt.indea.sistemaformin.entity;

import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

import br.gov.mt.indea.sistemaformin.enums.Dominio.SimNao;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoDiagnostico;

@Audited
@Entity
@Table(name="suspeita_doenca_animal")
public class SuspeitaDoencaAnimal extends BaseEntity<Long>{

	private static final long serialVersionUID = 1331282233749031338L;

	@Id
	@SequenceGenerator(name="suspeita_doenca_animal_seq", sequenceName="suspeita_doenca_animal_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="suspeita_doenca_animal_seq")
	private Long id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_cadastro")
	private Calendar dataCadastro;
	
	@ManyToOne
	@JoinColumn(name="id_form_notifica")
	private FormNotifica formNotifica;
	
	@OneToMany(mappedBy="suspeitaDoencaAnimal", cascade={CascadeType.ALL}, fetch=FetchType.LAZY, orphanRemoval=true)
	private List<SuspeitaEspecie> suspeitaEspecies;
	
	@OneToMany(mappedBy="suspeitaDoencaAnimal", cascade={CascadeType.ALL}, fetch=FetchType.LAZY, orphanRemoval=true)
	private List<TesteLaboratorial> testesLaboratoriais;
	
	@Enumerated(EnumType.STRING)
	@Column(name="tipo_diagnostico")
	private TipoDiagnostico tipoDiagnostico;
	
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="id_doenca")
	private Doenca doenca = new Doenca();

	@Enumerated(EnumType.STRING)
	@Column(name="teste_laboratorial")
	private SimNao testeLaboratorial;
	
	@Column(name="descricao_sinais_clinicos", length=4000)
	private String descricaoSinaisClinicos; 
	
	@Column(name="historico_e_info_gerais", length=4000)
	private String historicoEInformacoesGerais; 
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Calendar getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Calendar dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public FormNotifica getFormNotifica() {
		return formNotifica;
	}

	public void setFormNotifica(FormNotifica formNotifica) {
		this.formNotifica = formNotifica;
	}

	public List<SuspeitaEspecie> getSuspeitaEspecies() {
		return suspeitaEspecies;
	}

	public void setSuspeitaEspecies(List<SuspeitaEspecie> suspeitaEspecies) {
		this.suspeitaEspecies = suspeitaEspecies;
	}

	public TipoDiagnostico getTipoDiagnostico() {
		return tipoDiagnostico;
	}

	public void setTipoDiagnostico(TipoDiagnostico tipoDiagnostico) {
		this.tipoDiagnostico = tipoDiagnostico;
	}

	public SimNao getTesteLaboratorial() {
		return testeLaboratorial;
	}

	public void setTesteLaboratorial(SimNao testeLaboratorial) {
		this.testeLaboratorial = testeLaboratorial;
	}

	public Doenca getDoenca() {
		return doenca;
	}

	public void setDoenca(Doenca doenca) {
		this.doenca = doenca;
	}

	public String getDescricaoSinaisClinicos() {
		return descricaoSinaisClinicos;
	}

	public void setDescricaoSinaisClinicos(String descricaoSinaisClinicos) {
		this.descricaoSinaisClinicos = descricaoSinaisClinicos;
	}

	public String getHistoricoEInformacoesGerais() {
		return historicoEInformacoesGerais;
	}

	public void setHistoricoEInformacoesGerais(String historicoEInformacoesGerais) {
		this.historicoEInformacoesGerais = historicoEInformacoesGerais;
	}

	public List<TesteLaboratorial> getTestesLaboratoriais() {
		return testesLaboratoriais;
	}

	public void setTestesLaboratoriais(List<TesteLaboratorial> testesLaboratoriais) {
		this.testesLaboratoriais = testesLaboratoriais;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SuspeitaDoencaAnimal other = (SuspeitaDoencaAnimal) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
