package br.gov.mt.indea.sistemaformin.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.inject.Inject;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import br.gov.mt.indea.sistemaformin.entity.TipoChavePrincipalVisitaPropriedadeRural;
import br.gov.mt.indea.sistemaformin.enums.Dominio.AtivoInativo;
import br.gov.mt.indea.sistemaformin.exception.ApplicationException;

@Stateless
public class TipoChavePrincipalVisitaPropriedadeRuralDAO extends DAO<TipoChavePrincipalVisitaPropriedadeRural> {
	
	@Inject
	private EntityManager em;
	
	public TipoChavePrincipalVisitaPropriedadeRuralDAO() {
		super(TipoChavePrincipalVisitaPropriedadeRural.class);
	}

	@Override
	public EntityManager getEntityManager() {
		return this.em;
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void add(TipoChavePrincipalVisitaPropriedadeRural tipoChavePrincipalVisitaPropriedadeRural) throws ApplicationException{
		tipoChavePrincipalVisitaPropriedadeRural = this.getEntityManager().merge(tipoChavePrincipalVisitaPropriedadeRural);
		super.add(tipoChavePrincipalVisitaPropriedadeRural);
	}
	
	public List<TipoChavePrincipalVisitaPropriedadeRural> findAllByStatus(AtivoInativo status) throws ApplicationException{
		try{
			CriteriaBuilder cb = this.getEntityManager().getCriteriaBuilder();
			CriteriaQuery<TipoChavePrincipalVisitaPropriedadeRural> query = cb.createQuery(TipoChavePrincipalVisitaPropriedadeRural.class);
			
			Root<TipoChavePrincipalVisitaPropriedadeRural> t = query.from(TipoChavePrincipalVisitaPropriedadeRural.class);
			
			query.where(cb.equal(t.get("status"), status));
			query.orderBy(cb.asc(t.get("nome")));
			
			return this.getEntityManager().createQuery(query).getResultList();
		}catch(Exception e){
			throw new ApplicationException("N�o foi poss�vel buscar o registro", e);
		}
	}

}
