package br.gov.mt.indea.sistemaformin.entity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

import br.gov.mt.indea.sistemaformin.enums.Dominio.Especie;

@Audited
@Entity(name="investigacao_epidemiologica")
public class InvestigacaoEpidemiologica extends BaseEntity<Long>{
	
	private static final long serialVersionUID = -361785479572430083L;

	@Id
	@SequenceGenerator(name="investigacao_epidemiologica_seq", sequenceName="investigacao_epidemiologica_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="investigacao_epidemiologica_seq")
	private Long id;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data")
	private Calendar data;
	
	@ManyToOne
	@JoinColumn(name="id_vigilancia_veterinaria")
	private VigilanciaVeterinaria vigilanciaVeterinaria;
	
	@Enumerated(EnumType.STRING)
	@Column(name="especie")
	private Especie especie;
	
	@OneToMany(cascade={CascadeType.ALL}, orphanRemoval=true, fetch=FetchType.LAZY)
	@JoinTable(name="investigacao_doencas", joinColumns=@JoinColumn(name="id_investigacao"), inverseJoinColumns=@JoinColumn(name="id_doenca"))
	private List<Doenca> listaDoencas = new ArrayList<Doenca>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Calendar getData() {
		return data;
	}

	public void setData(Calendar data) {
		this.data = data;
	}

	public VigilanciaVeterinaria getVigilanciaVeterinaria() {
		return vigilanciaVeterinaria;
	}

	public void setVigilanciaVeterinaria(VigilanciaVeterinaria vigilanciaVeterinaria) {
		this.vigilanciaVeterinaria = vigilanciaVeterinaria;
	}

	public Especie getEspecie() {
		return especie;
	}

	public void setEspecie(Especie especie) {
		this.especie = especie;
	}

	public List<Doenca> getListaDoencas() {
		return listaDoencas;
	}

	public void setListaDoencas(List<Doenca> listaDoencas) {
		this.listaDoencas = listaDoencas;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InvestigacaoEpidemiologica other = (InvestigacaoEpidemiologica) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}