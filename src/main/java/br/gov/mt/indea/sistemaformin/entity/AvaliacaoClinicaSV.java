package br.gov.mt.indea.sistemaformin.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import org.hibernate.envers.Audited;

import br.gov.mt.indea.sistemaformin.enums.Dominio.Especie;
import br.gov.mt.indea.sistemaformin.enums.Dominio.SimNao;
import br.gov.mt.indea.sistemaformin.enums.Dominio.SinaisClinicosSindromeVesicular;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoPeriodo;

@Audited
@Entity(name="avaliacao_clinica_sv")
public class AvaliacaoClinicaSV extends BaseEntity<Long>{

	private static final long serialVersionUID = -3272649526591198484L;
	
	@Id
	@SequenceGenerator(name="avaliacao_clinica_sv_seq", sequenceName="avaliacao_clinica_sv_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="avaliacao_clinica_sv_seq")
	private Long id;
	
	@ManyToOne
	@JoinColumn(name="id_formSV")
	private FormSV formSV;
	
	@Column(name="identificacao_animal")
	private String identificacaoAnimal;
	
	@Enumerated(EnumType.STRING)
	private Especie especie;
	
	private Long idade;
	
	@Enumerated(EnumType.STRING)
	@Column(name="tipo_periodo_idade")
	private TipoPeriodo tipoPeriodoIdade;
	
	@Column(name="quantidade_vacinas")
	private Long quantidadeVacinas;
	
	@Enumerated(EnumType.STRING)
	@Column(name="nascido_no_estabelecimento")
	private SimNao nascidoNoEstabelecimento;
	
	@Enumerated(EnumType.STRING)
	private SimNao salivacao;
	
	@Enumerated(EnumType.STRING)
	private SimNao claudicacao;
	
	@Enumerated(EnumType.STRING)
	@Column(name="descolamento_de_unha")
	private SimNao descolamentoDeUnha;
	
	@Enumerated(EnumType.STRING)
	@Column(name="caracteristica_lesoes_boca")
	private SinaisClinicosSindromeVesicular caracteristicasLesoesBoca;
	
	@Enumerated(EnumType.STRING)
	@Column(name="caracteristica_lesoes_patas")
	private SinaisClinicosSindromeVesicular caracteristicasLesoesPatas;
	
	@Enumerated(EnumType.STRING)
	@Column(name="caracteristica_lesoes_tetos")
	private SinaisClinicosSindromeVesicular caracteristicasLesoesTetos;
	
	private Long idadeLesoesEmDias;
	
	@Enumerated(EnumType.STRING)
	private SimNao amostras;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public FormSV getFormSV() {
		return formSV;
	}

	public void setFormSV(FormSV formSV) {
		this.formSV = formSV;
	}

	public String getIdentificacaoAnimal() {
		return identificacaoAnimal;
	}

	public void setIdentificacaoAnimal(String identificacaoAnimal) {
		this.identificacaoAnimal = identificacaoAnimal;
	}

	public Especie getEspecie() {
		return especie;
	}

	public void setEspecie(Especie especie) {
		this.especie = especie;
	}

	public Long getIdade() {
		return idade;
	}

	public void setIdade(Long idade) {
		this.idade = idade;
	}

	public TipoPeriodo getTipoPeriodoidade() {
		return tipoPeriodoIdade;
	}

	public void setTipoPeriodoidade(TipoPeriodo tipoPeriodoidade) {
		this.tipoPeriodoIdade = tipoPeriodoidade;
	}

	public Long getQuantidadeVacinas() {
		return quantidadeVacinas;
	}

	public void setQuantidadeVacinas(Long quantidadeVacinas) {
		this.quantidadeVacinas = quantidadeVacinas;
	}

	public SimNao getNascidoNoEstabelecimento() {
		return nascidoNoEstabelecimento;
	}

	public void setNascidoNoEstabelecimento(SimNao nascidoNoEstabelecimento) {
		this.nascidoNoEstabelecimento = nascidoNoEstabelecimento;
	}

	public SimNao getSalivacao() {
		return salivacao;
	}

	public void setSalivacao(SimNao salivacao) {
		this.salivacao = salivacao;
	}

	public SimNao getClaudicacao() {
		return claudicacao;
	}

	public void setClaudicacao(SimNao claudicacao) {
		this.claudicacao = claudicacao;
	}

	public SimNao getDescolamentoDeUnha() {
		return descolamentoDeUnha;
	}

	public void setDescolamentoDeUnha(SimNao descolamentoDeUnha) {
		this.descolamentoDeUnha = descolamentoDeUnha;
	}

	public SinaisClinicosSindromeVesicular getCaracteristicasLesoesBoca() {
		return caracteristicasLesoesBoca;
	}

	public void setCaracteristicasLesoesBoca(
			SinaisClinicosSindromeVesicular caracteristicasLesoesBoca) {
		this.caracteristicasLesoesBoca = caracteristicasLesoesBoca;
	}

	public SinaisClinicosSindromeVesicular getCaracteristicasLesoesPatas() {
		return caracteristicasLesoesPatas;
	}

	public void setCaracteristicasLesoesPatas(
			SinaisClinicosSindromeVesicular caracteristicasLesoesPatas) {
		this.caracteristicasLesoesPatas = caracteristicasLesoesPatas;
	}

	public SinaisClinicosSindromeVesicular getCaracteristicasLesoesTetos() {
		return caracteristicasLesoesTetos;
	}

	public void setCaracteristicasLesoesTetos(
			SinaisClinicosSindromeVesicular caracteristicasLesoesTetos) {
		this.caracteristicasLesoesTetos = caracteristicasLesoesTetos;
	}

	public Long getIdadeLesoesEmDias() {
		return idadeLesoesEmDias;
	}

	public void setIdadeLesoesEmDias(Long idadeLesoesEmDias) {
		this.idadeLesoesEmDias = idadeLesoesEmDias;
	}

	public SimNao getAmostras() {
		return amostras;
	}

	public void setAmostras(SimNao amotras) {
		this.amostras = amotras;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AvaliacaoClinicaSV other = (AvaliacaoClinicaSV) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}