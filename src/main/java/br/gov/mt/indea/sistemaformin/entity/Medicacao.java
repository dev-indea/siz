package br.gov.mt.indea.sistemaformin.entity;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

@Audited
@Entity
public class Medicacao extends BaseEntity<Long> implements Cloneable{

	private static final long serialVersionUID = 3572099774562833247L;

	@Id
	@SequenceGenerator(name="medicacao_seq", sequenceName="medicacao_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="medicacao_seq")
	private Long id;
	
	@ManyToOne
	@JoinColumn(name="id_formIN")
	private FormIN formIN;
	
	@ManyToOne
	@JoinColumn(name="id_formCOM")
	private FormCOM formCOM;
	
	private String doenca;
	
	@Column(name="nome_comercial_produto")
	private String nomeComercialDoProduto;
	
	@Column(name="via_de_adminsitracao")
	private String viaDeAdministracao;
	
	@Column(name="data_inicial_notificacao")
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar dataInicial;
	
	@Column(name="data_final_notificacao")
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar dataFinal;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public FormIN getFormIN() {
		return formIN;
	}

	public void setFormIN(FormIN formIN) {
		this.formIN = formIN;
	}

	public FormCOM getFormCOM() {
		return formCOM;
	}

	public void setFormCOM(FormCOM formCOM) {
		this.formCOM = formCOM;
	}

	public String getDoenca() {
		return doenca;
	}

	public void setDoenca(String doenca) {
		this.doenca = doenca;
	}

	public String getNomeComercialDoProduto() {
		return nomeComercialDoProduto;
	}

	public void setNomeComercialDoProduto(String nomeComercialDoProduto) {
		this.nomeComercialDoProduto = nomeComercialDoProduto;
	}

	public String getViaDeAdministracao() {
		return viaDeAdministracao;
	}

	public void setViaDeAdministracao(String viaDeAdministracao) {
		this.viaDeAdministracao = viaDeAdministracao;
	}	

	public Calendar getDataInicial() {
		return dataInicial;
	}

	public void setDataInicial(Calendar dataInicial) {
		this.dataInicial = dataInicial;
	}

	public Calendar getDataFinal() {
		return dataFinal;
	}

	public void setDataFinal(Calendar dataFinal) {
		this.dataFinal = dataFinal;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Medicacao other = (Medicacao) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	protected Object clone(FormIN formIN) throws CloneNotSupportedException {
		Medicacao cloned = (Medicacao) super.clone();
		
		cloned.setId(null);
		cloned.setFormIN(formIN);
		
		return cloned;
	}
}
