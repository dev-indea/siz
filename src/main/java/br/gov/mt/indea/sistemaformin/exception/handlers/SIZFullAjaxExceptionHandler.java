package br.gov.mt.indea.sistemaformin.exception.handlers;

import java.time.LocalDate;

import javax.faces.context.ExceptionHandler;
import javax.faces.context.FacesContext;

import org.omnifaces.exceptionhandler.FullAjaxExceptionHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.mt.indea.sistemaformin.managedbeans.ErroManagedBean;
import br.gov.mt.indea.sistemaformin.util.CDIServiceLocator;
import br.gov.mt.indea.sistemaformin.util.TokenGenerator;

public class SIZFullAjaxExceptionHandler extends FullAjaxExceptionHandler{
	
	private static final Logger log = LoggerFactory.getLogger(SIZFullAjaxExceptionHandler.class);

	private ErroManagedBean erroManagedBean = CDIServiceLocator.getBean(ErroManagedBean.class);

	public SIZFullAjaxExceptionHandler(ExceptionHandler wrapped) {
		super(wrapped);
	}
	
	@Override
	protected String findErrorPageLocation(FacesContext context, Throwable exception) {
		return "/erro.jsf";
	}
	
	@Override
	protected void logException(FacesContext context, Throwable exception, String location, LogReason reason) {
		LocalDate date = LocalDate.now();
		String token = TokenGenerator.csRandomAlphaNumericString(30);
		token = date.getDayOfMonth() + "" + String.format("%02d", date.getMonthValue()) + "" + date.getYear() + token;
		erroManagedBean.setCodigo(token);

		log.error("Erro desconhecido: {}", token, exception);
	}


}
