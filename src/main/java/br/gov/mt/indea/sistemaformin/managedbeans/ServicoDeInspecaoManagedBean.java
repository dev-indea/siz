package br.gov.mt.indea.sistemaformin.managedbeans;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLBeanName;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;

import br.gov.mt.indea.sistemaformin.entity.ServicoDeInspecao;
import br.gov.mt.indea.sistemaformin.entity.dto.ServicoDeInspecaoDTO;
import br.gov.mt.indea.sistemaformin.exception.ApplicationException;
import br.gov.mt.indea.sistemaformin.service.LazyObjectDataModel;
import br.gov.mt.indea.sistemaformin.service.ServicoDeInspecaoService;
import br.gov.mt.indea.sistemaformin.util.FacesMessageUtil;
import br.gov.mt.indea.sistemaformin.webservice.client.ClientWebServiceAbatedouro;
import br.gov.mt.indea.sistemaformin.webservice.entity.Abatedouro;

@Named("servicoDeInspecaoManagedBean")
@ViewScoped
@URLBeanName("servicoDeInspecaoManagedBean")
@URLMappings(mappings = {
		@URLMapping(id = "pesquisarServicoDeInspecao", pattern = "/servicoDeInspecao/pesquisar", viewId = "/pages/servicoDeInspecao/lista.jsf"),
		@URLMapping(id = "incluirServicoDeInspecao", pattern = "/servicoDeInspecao/incluir", viewId = "/pages/servicoDeInspecao/novo.jsf"),
		@URLMapping(id = "alterarServicoDeInspecao", pattern = "/servicoDeInspecao/alterar/#{servicoDeInspecaoManagedBean.id}", viewId = "/pages/servicoDeInspecao/novo.jsf")})
public class ServicoDeInspecaoManagedBean implements Serializable {

	private static final long serialVersionUID = 5401332308697941357L;
	
	private Long id;
	
	@Inject
	private ServicoDeInspecao servicoDeInspecao;
	
	private String cnpjAbatedouro;
	
	private List<Abatedouro> listaAbatedouros;
	
	@Inject
	private ServicoDeInspecaoDTO servicoDeInspecaoDTO;
	
	private LazyObjectDataModel<ServicoDeInspecao> listaServicoDeInspecao;
	
	@Inject
	private ServicoDeInspecaoService servicoDeInspecaoService;
	
	@Inject
	private ClientWebServiceAbatedouro clientWebServiceAbatedouro;
	
	@PostConstruct
	public void init(){
		
	}
	
	public void limpar(){
		this.servicoDeInspecao = new ServicoDeInspecao();
	}
	
	public void buscarServicoDeInspecaos(){
		this.listaServicoDeInspecao = new LazyObjectDataModel<ServicoDeInspecao>(this.servicoDeInspecaoService, this.servicoDeInspecaoDTO);
	}
	
	@URLAction(mappingId = "incluirServicoDeInspecao", onPostback = false)
	public void novo(){
		this.limpar();
	}
	
	@URLAction(mappingId = "pesquisarServicoDeInspecao", onPostback = false)
	public void pesquisar(){
		limpar();
	}

	@URLAction(mappingId = "alterarServicoDeInspecao", onPostback = false)
	public void editar(){
		this.servicoDeInspecao = servicoDeInspecaoService.findByIdFetchAll(this.getId());
	}
	
	public String adicionar() throws IOException{
		if (servicoDeInspecao.getDataCadastro() != null){
			this.servicoDeInspecaoService.saveOrUpdate(servicoDeInspecao);
			FacesMessageUtil.addInfoContextFacesMessage("Servi�o De Inspe��o " + servicoDeInspecao.getNumero() + " atualizado", "");
		}else{
			this.servicoDeInspecao.setDataCadastro(Calendar.getInstance());
			this.servicoDeInspecaoService.saveOrUpdate(servicoDeInspecao);
			FacesMessageUtil.addInfoContextFacesMessage("Servi�o De Inspe��o " + servicoDeInspecao.getNumero() + " adicionado!", "");
		}
		
		return "pretty:pesquisarServicoDeInspecao";
	}
	
	public void remover(ServicoDeInspecao servicoDeInspecao){
		this.servicoDeInspecaoService.delete(servicoDeInspecao);
		FacesMessageUtil.addInfoContextFacesMessage("ServicoDeInspecao exclu�da com sucesso", "");
	}
	
	public void abrirTelaDeBuscaDeAbatedouro(){
		this.cnpjAbatedouro = null;
		this.listaAbatedouros = null;
	}
	
	public void buscarAbatedouroPorCNPJ(){
		this.listaAbatedouros = null;
		
		try {
			Abatedouro abatedouro = clientWebServiceAbatedouro.getAbatedouroByCNPJ(cnpjAbatedouro);
			if (abatedouro != null){
				this.listaAbatedouros = new ArrayList<Abatedouro>();
				this.listaAbatedouros.add(abatedouro);
			}
		} catch (ApplicationException e) {
			FacesMessageUtil.addErrorContextFacesMessage("Erro", e.getLocalizedMessage());
			return;
		}
		
		if (this.listaAbatedouros == null || this.listaAbatedouros.isEmpty())
			FacesMessageUtil.addInfoContextFacesMessage("Nenhum abatedouro foi encontrada", "");
	}
	
	public void selecionarAbatedouro(Abatedouro abatedouro){
		this.servicoDeInspecao.setAbatedouro(abatedouro);
		
		FacesMessageUtil.addInfoContextFacesMessage("Abatedouro " + this.servicoDeInspecao.getAbatedouro().getNome() + " selecionada", "");
	}
	
	public ServicoDeInspecao getServicoDeInspecao() {
		return servicoDeInspecao;
	}

	public void setServicoDeInspecao(ServicoDeInspecao servicoDeInspecao) {
		this.servicoDeInspecao = servicoDeInspecao;
	}

	public LazyObjectDataModel<ServicoDeInspecao> getListaServicoDeInspecao() {
		return listaServicoDeInspecao;
	}

	public void setListaServicoDeInspecao(LazyObjectDataModel<ServicoDeInspecao> listaServicoDeInspecao) {
		this.listaServicoDeInspecao = listaServicoDeInspecao;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ServicoDeInspecaoDTO getServicoDeInspecaoDTO() {
		return servicoDeInspecaoDTO;
	}

	public void setServicoDeInspecaoDTO(ServicoDeInspecaoDTO servicoDeInspecaoDTO) {
		this.servicoDeInspecaoDTO = servicoDeInspecaoDTO;
	}

	public String getCnpjAbatedouro() {
		return cnpjAbatedouro;
	}

	public void setCnpjAbatedouro(String cnpjAbatedouro) {
		this.cnpjAbatedouro = cnpjAbatedouro;
	}

	public List<Abatedouro> getListaAbatedouros() {
		return listaAbatedouros;
	}

	public void setListaAbatedouros(List<Abatedouro> listaAbatedouros) {
		this.listaAbatedouros = listaAbatedouros;
	}

}
