package br.gov.mt.indea.sistemaformin.webservice.model;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class NotaFiscalVacinacao implements Serializable {

	private static final long serialVersionUID = 8811725897746061792L;

	private Long id;
    
    @XmlElement(name = "animaisVacinados")
    private List<AnimaisVacinados> animaisVacinados;

    public NotaFiscalVacinacao() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<AnimaisVacinados> getAnimaisVacinados() {
        return animaisVacinados;
    }

    public void setAnimaisVacinados(List<AnimaisVacinados> animaisVacinados) {
        this.animaisVacinados = animaisVacinados;
    }
}