package br.gov.mt.indea.sistemaformin.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.envers.Audited;

@Audited
@Entity
@DiscriminatorValue("equinos")
public class DetalhesInformacoesEquinos extends AbstractDetalhesInformacoesDeAnimais implements Cloneable{
	
	private static final long serialVersionUID = 1355044082455412390L;

	@ManyToOne
	@JoinColumn(name="id_informacoes_equinos")
	private InformacoesEquinos informacoesEquinos;

	public InformacoesEquinos getInformacoesEquinos() {
		return informacoesEquinos;
	}

	public void setInformacoesEquinos(InformacoesEquinos informacoesEquinos) {
		this.informacoesEquinos = informacoesEquinos;
	}
	
	protected Object clone(InformacoesEquinos informacoesEquinos) throws CloneNotSupportedException{
		DetalhesInformacoesEquinos cloned = (DetalhesInformacoesEquinos) super.clone();
		
		cloned.setId(null);
		cloned.setInformacoesEquinos(informacoesEquinos);
		
		return cloned;
	}

}