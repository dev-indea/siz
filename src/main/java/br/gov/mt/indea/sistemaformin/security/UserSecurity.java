package br.gov.mt.indea.sistemaformin.security;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import br.gov.mt.indea.sistemaformin.entity.Usuario;
import br.gov.mt.indea.sistemaformin.util.CDIServiceLocator;

public class UserSecurity extends User{

	private static final long serialVersionUID = -5419226623542090801L;
	
	public static final String ANONYMOUS_USER = "anonymousUser"; 
	
	private Usuario usuario;
	
	private String ip;

	public UserSecurity(Usuario usuario, Collection<? extends GrantedAuthority> authorities) {
		super(usuario.getId(), usuario.getPassword(), true, !usuario.isUsuarioExpirado(), true, usuario.isUsuarioAtivo(), authorities);
		this.usuario = usuario;
		this.ip = getIpUsuarioLogado();
	}
	
	private String getIpUsuarioLogado(){
		
		try{
			HttpServletRequest request = CDIServiceLocator.getBean(HttpServletRequest.class);
					
			String ipAddress = request.getHeader("X-FORWARDED-FOR");
			if (ipAddress == null) {
			    ipAddress = request.getRemoteAddr();
			} else{
				ipAddress = ipAddress.replaceFirst(",.*", "");
			}
			
			return ipAddress;
		} catch (Exception e) {
			return "127.0.0.1";
		}
		
	}

	public Usuario getUsuario() {
		return usuario;
	}
	
	public String getIp() {
		return ip;
	}

	public boolean hasAuthorityFormIN(){
		for (GrantedAuthority authority : this.getAuthorities()) {
			if (authority.getAuthority().equals("formIN"))
				return true;
		}
		return false;
	}
	
	public boolean hasAuthorityTermoFiscalizacao(){
		for (GrantedAuthority authority : this.getAuthorities()) {
			if (authority.getAuthority().equals("termoFiscalizacao"))
				return true;
		}
		return false;
	}
	
	public boolean hasAuthorityTipoChavePrincipalTermoFiscalizacao(){
		for (GrantedAuthority authority : this.getAuthorities()) {
			if (authority.getAuthority().equals("tipoChavePrincipalTermoFiscalizacao"))
				return true;
		}
		return false;
	}
	
	public boolean hasAuthorityTipoChavePrincipalVisitaPropriedadeRural(){
		for (GrantedAuthority authority : this.getAuthorities()) {
			if (authority.getAuthority().equals("tipoChavePrincipalVisitaPropriedadeRural"))
				return true;
		}
		return false;
	}
	
	public boolean hasAuthorityTipoChaveSecundariaTermoFiscalizacao(){
		for (GrantedAuthority authority : this.getAuthorities()) {
			if (authority.getAuthority().equals("tipoChaveSecundariaTermoFiscalizacao"))
				return true;
		}
		return false;
	}
	
	public boolean hasAuthorityTipoChaveSecundariaVisitaPropriedadeRural(){
		for (GrantedAuthority authority : this.getAuthorities()) {
			if (authority.getAuthority().equals("tipoChaveSecundariaVisitaPropriedadeRural"))
				return true;
		}
		return false;
	}
	
	public boolean hasAuthorityVigilanciaVeterinaria(){
		for (GrantedAuthority authority : this.getAuthorities()) {
			if (authority.getAuthority().equals("vigilanciaVeterinaria"))
				return true;
		}
		return false;
	}
	
	public boolean hasAuthorityVisitaPropriedadeRural(){
		for (GrantedAuthority authority : this.getAuthorities()) {
			if (authority.getAuthority().equals("visitaPropriedadeRural"))
				return true;
		}
		return false;
	}
	
	public boolean hasAuthorityAdmin(){
		for (GrantedAuthority authority : this.getAuthorities()) {
			if (authority.getAuthority().equals("admin"))
				return true;
		}
		return false;
	}
	
	public boolean hasAuthorityFormNotifica(){
		for (GrantedAuthority authority : this.getAuthorities()) {
			if (authority.getAuthority().equals("formNotifica"))
				return true;
		}
		return false;
	}
	
	public boolean hasAuthorityEnfermidadeAbatedouroFrigorifico(){
		for (GrantedAuthority authority : this.getAuthorities()) {
			if (authority.getAuthority().equals("enfermidadeAbatedouroFrigorifico"))
				return true;
		}
		return false;
	}
	
	public boolean hasAuthorityRegistroDeEntradaLasa(){
		for (GrantedAuthority authority : this.getAuthorities()) {
			if (authority.getAuthority().equals("registroDeEntradaLasa"))
				return true;
		}
		return false;
	}
	
	public boolean hasAuthorityPefa(){
		for (GrantedAuthority authority : this.getAuthorities()) {
			if (authority.getAuthority().equals("pefa"))
				return true;
		}
		return false;
	}
	
}
