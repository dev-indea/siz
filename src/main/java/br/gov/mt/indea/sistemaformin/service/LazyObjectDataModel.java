package br.gov.mt.indea.sistemaformin.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import br.gov.mt.indea.sistemaformin.entity.dto.AbstractDTO;

public class LazyObjectDataModel<T> extends LazyDataModel<T> {

	private static final long serialVersionUID = -5952188210722897111L;

	private List<T> datasource;
	
	@SuppressWarnings("rawtypes")
	private PaginableService paginableService;
	
	private AbstractDTO dto;
	
	public LazyObjectDataModel(PaginableService<?,?> paginableService) {
		this.datasource = new ArrayList<T>();
		this.paginableService = paginableService;
	}
	
	public LazyObjectDataModel(PaginableService<?,?> paginableService, AbstractDTO dto) {
		this.datasource = new ArrayList<T>();
		this.paginableService = paginableService;
		this.dto = dto;
	}

	public T getRowData(String rowKey) {
		for (T t : datasource) {
			if (t.equals(rowKey)) {
				return t;
			}
		}
		return null;
	}
	
	public T getRowKey(T t) {
		return t;
	}
	
	//para resolver Bug do primefaces 3.5 erro: java.lang.ArithmeticException: / by zero with Primefaces LazyDataModel
	@Override
	public void setRowIndex(int rowIndex){
	    if (rowIndex == -1 || getPageSize() == 0) {
	        super.setRowIndex(-1);
	    } else {
	        super.setRowIndex(rowIndex % getPageSize());
	    }
	}
	
	private String getSortOrder(SortOrder sortOrder) {
		switch (sortOrder) {
			case ASCENDING : 
				return "ASC"; 
			case DESCENDING : 
				return "DESC"; 
			default: 
				return StringUtils.EMPTY;
		}		
	}
	
	@SuppressWarnings("unchecked")	
	public List<T> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
		// rowCount
		int dataSize;
		if (dto != null && !dto.isNull())
			dataSize = paginableService.countAll(dto).intValue();
		else	
			dataSize = paginableService.countAll().intValue();
		this.setRowCount(dataSize);
		
		// Checar se existe itens na pagina e o resultado n�o veio vazio.
		if (dataSize <= first && first != 0 && dataSize != 0) {
			first = 0;
		}
		
		String order = getSortOrder(sortOrder);
		// sort
		if (StringUtils.isNotBlank(sortField)) {
			return paginableService.findAllComPaginacao(dto, first, pageSize, sortField, order);
		} else {
			return paginableService.findAllComPaginacao(dto, first, pageSize, null, null);
		}
	}
	
}
