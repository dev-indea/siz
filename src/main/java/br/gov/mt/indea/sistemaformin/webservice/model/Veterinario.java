package br.gov.mt.indea.sistemaformin.webservice.model;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
public class Veterinario implements Serializable{
	
	private static final long serialVersionUID = 2262593184169979097L;

	private Integer codigoVeterinarioSVO;
	
	@XmlElement
	private Pessoa pessoa;
	
	@XmlElement(name = "ule")
    private ULE ule;
	
	private String numeroConselho;
	
	private String instituicaoFormatura;
	
	private String instituicaoTrabalho;
	
	private String proprietarioInstituicao;
	
	private String profissionalOficial;
	
	private String pesquisador;
	
	private Date dataFormatura;
	
    @XmlElement(name = "conselho")
    private Conselho conselho;
	
    @XmlElement(name = "tipoEmitente")
    private TipoEmitente tipoEmitente;

	public String getProfissionalOficialPorExtenso(){
		if (this.profissionalOficial == null)
			return null;
		
		if (this.profissionalOficial.equals("N"))
			return "N�O";
		else
			return "SIM";
	}

	public ULE getUle() {
		return ule;
	}
	
	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	public void setUle(ULE ule) {
		this.ule = ule;
	}

	public String getNumeroConselho() {
		return numeroConselho;
	}

	public void setNumeroConselho(String numeroConselho) {
		this.numeroConselho = numeroConselho;
	}

	public String getInstituicaoFormatura() {
		return instituicaoFormatura;
	}

	public void setInstituicaoFormatura(String instituicaoFormatura) {
		this.instituicaoFormatura = instituicaoFormatura;
	}

	public String getInstituicaoTrabalho() {
		return instituicaoTrabalho;
	}

	public void setInstituicaoTrabalho(String instituicaoTrabalho) {
		this.instituicaoTrabalho = instituicaoTrabalho;
	}

	public String getProprietarioInstituicao() {
		return proprietarioInstituicao;
	}

	public void setProprietarioInstituicao(String proprietarioInstituicao) {
		this.proprietarioInstituicao = proprietarioInstituicao;
	}

	public String getProfissionalOficial() {
		return profissionalOficial;
	}

	public void setProfissionalOficial(String profissionalOficial) {
		this.profissionalOficial = profissionalOficial;
	}

	public String getPesquisador() {
		return pesquisador;
	}

	public void setPesquisador(String pesquisador) {
		this.pesquisador = pesquisador;
	}

	public Date getDataFormatura() {
		return dataFormatura;
	}

	public void setDataFormatura(Date dataFormatura) {
		this.dataFormatura = dataFormatura;
	}

	public Conselho getConselho() {
		return conselho;
	}

	public void setConselho(Conselho conselho) {
		this.conselho = conselho;
	}

	public TipoEmitente getTipoEmitente() {
		return tipoEmitente;
	}

	public void setTipoEmitente(TipoEmitente tipoEmitente) {
		this.tipoEmitente = tipoEmitente;
	}

	public Integer getCodigoVeterinarioSVO() {
		return codigoVeterinarioSVO;
	}

	public void setCodigoVeterinarioSVO(Integer codigoVeterinarioSVO) {
		this.codigoVeterinarioSVO = codigoVeterinarioSVO;
	}
	
}