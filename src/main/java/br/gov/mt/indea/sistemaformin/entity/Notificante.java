package br.gov.mt.indea.sistemaformin.entity;

import java.util.Calendar;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

import br.gov.mt.indea.sistemaformin.enums.Dominio.AreaAtuacaoMedicoVeterinario;
import br.gov.mt.indea.sistemaformin.enums.Dominio.SimNao;

@Audited
@Entity
@Table(name="notificante")
public class Notificante extends BaseEntity<Long>{

	private static final long serialVersionUID = 1331282233749031338L;

	@Id
	@SequenceGenerator(name="notificante_seq", sequenceName="notificante_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="notificante_seq")
	private Long id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_cadastro")
	private Calendar dataCadastro;
	
	@ManyToOne
	@JoinColumn(name="id_form_notifica")
	private FormNotifica formNotifica;
	
	@Enumerated(EnumType.STRING)
	@Column(name="medico_veterinario")
	private SimNao medicoVeterinario;
	
	@Enumerated(EnumType.STRING)
	@Column(name="area_atuacao")
	private AreaAtuacaoMedicoVeterinario areaAtuacao;
	
	@Column(name="outra_area_atuacao")
	private String outraAreaAtuacao;
	
	@OneToOne(cascade={CascadeType.ALL}, orphanRemoval=true, fetch=FetchType.EAGER)
	@JoinColumn(name="id_notificante")
	private Pessoa pessoa = new Pessoa();
	
	@OneToOne(cascade={CascadeType.ALL}, orphanRemoval=true, fetch=FetchType.LAZY)
	@JoinColumn(name="id_instituicao")
	private Pessoa instituicao = new Pessoa();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Calendar getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Calendar dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public FormNotifica getFormNotifica() {
		return formNotifica;
	}

	public void setFormNotifica(FormNotifica formNotifica) {
		this.formNotifica = formNotifica;
	}

	public SimNao getMedicoVeterinario() {
		return medicoVeterinario;
	}

	public void setMedicoVeterinario(SimNao medicoVeterinario) {
		this.medicoVeterinario = medicoVeterinario;
	}

	public AreaAtuacaoMedicoVeterinario getAreaAtuacao() {
		return areaAtuacao;
	}

	public void setAreaAtuacao(AreaAtuacaoMedicoVeterinario areaAtuacao) {
		this.areaAtuacao = areaAtuacao;
	}

	public String getOutraAreaAtuacao() {
		return outraAreaAtuacao;
	}

	public void setOutraAreaAtuacao(String outraAreaAtuacao) {
		this.outraAreaAtuacao = outraAreaAtuacao;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	public Pessoa getInstituicao() {
		return instituicao;
	}

	public void setInstituicao(Pessoa instituicao) {
		this.instituicao = instituicao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Notificante other = (Notificante) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
