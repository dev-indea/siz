package br.gov.mt.indea.sistemaformin.entity;

import java.util.Calendar;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

import br.gov.mt.indea.sistemaformin.enums.Dominio.Especie;
import br.gov.mt.indea.sistemaformin.enums.Dominio.FinalidadeDoTesteDeLaboratorio;
import br.gov.mt.indea.sistemaformin.enums.Dominio.Sexo;
import br.gov.mt.indea.sistemaformin.enums.Dominio.SimNaoSI;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoDeTesteParaMormo;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoLaboratorio;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoPeriodo;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoPropriedade;
import br.gov.mt.indea.sistemaformin.webservice.entity.Propriedade;

@Audited
@Entity
@Table(name="form_mormo")
public class FormMormo extends BaseEntity<Long>{

	private static final long serialVersionUID = 6806037144033181747L;

	@Id
	@SequenceGenerator(name="form_mormo_seq", sequenceName="form_mormo_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="form_mormo_seq")
	private Long id;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_usuario")
	private Usuario usuario;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_cadastro")
	private Calendar dataCadastro;
	
	@ManyToOne
	@JoinColumn(name="id_formin")
	private FormIN formIN;
	
	@ManyToOne
	@JoinColumn(name="id_formcom")
	private FormCOM formCOM;
	
	@Enumerated(EnumType.STRING)
	@Column(name="tipo_teste")
	private TipoDeTesteParaMormo tipoDeTesteParaMormo;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_propriedade")
	private Propriedade propriedade;
	
	@Enumerated(EnumType.STRING)
    private TipoPropriedade tipoPropriedade;
	
	@Enumerated(EnumType.STRING)
	@Column(name="tipo_laboratorio")
	private TipoLaboratorio tipoLaboratorio;
	
	@Enumerated(EnumType.STRING)
	@Column(name="finalidade_teste_lab_credenciado")
	private FinalidadeDoTesteDeLaboratorio finalidadeDoTesteDeLaboratorioCredenciado;
	
	@Enumerated(EnumType.STRING)
	@Column(name="finalidade_teste_lab_cred_publico")
	private FinalidadeDoTesteDeLaboratorio finalidadeDoTesteDeLaboratorioCredenciadoPublico;
	
	@Enumerated(EnumType.STRING)
	@Column(name="finalidade_teste_lab_oficial")
	private FinalidadeDoTesteDeLaboratorio finalidadeDoTesteDeLaboratorioOficial;
	
	private Long quantidadeDeEquideosNoMomentoDaColheita;
	
	@Column(name="nome_animal", length=255)
	private String nomeAnimal;
	
	private Long idade;
	
	@Enumerated(EnumType.STRING)
	@Column(name="tipo_idade")
	private TipoPeriodo tipoPeriodo;
	
	@Column(length=255)
	private String registro;
	
	@Column(name="id_eletronica", length=255)
	private String idEletronica;
	
	@Column(length=255)
	private String raca;
	
	@Enumerated(EnumType.STRING)
	private Especie especie;
	
	@Enumerated(EnumType.STRING)
	private Sexo sexo;
	
	@Enumerated(EnumType.STRING)
	private SimNaoSI prenhe;
	
	@Enumerated(EnumType.STRING)
	@Column(name="terco_gestacao")
	private Sexo tercoGestacao;
	
	@Column(length=255)
	private String pelagem;
	
	@Column(length=2000)
	private String descricao;
	
	@OneToOne(cascade={CascadeType.ALL}, orphanRemoval=true)
	@JoinColumn(name="id_veterinario")
	private br.gov.mt.indea.sistemaformin.webservice.entity.Veterinario veterinario;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Calendar getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Calendar dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public FormIN getFormIN() {
		return formIN;
	}

	public void setFormIN(FormIN formIN) {
		this.formIN = formIN;
	}

	public FormCOM getFormCOM() {
		return formCOM;
	}

	public void setFormCOM(FormCOM formCOM) {
		this.formCOM = formCOM;
	}

	public TipoDeTesteParaMormo getTipoDeTesteParaMormo() {
		return tipoDeTesteParaMormo;
	}

	public void setTipoDeTesteParaMormo(TipoDeTesteParaMormo tipoDeTesteParaMormo) {
		this.tipoDeTesteParaMormo = tipoDeTesteParaMormo;
	}

	public TipoLaboratorio getTipoLaboratorio() {
		return tipoLaboratorio;
	}

	public void setTipoLaboratorio(TipoLaboratorio tipoLaboratorio) {
		this.tipoLaboratorio = tipoLaboratorio;
	}

	public FinalidadeDoTesteDeLaboratorio getFinalidadeDoTesteDeLaboratorioCredenciado() {
		return finalidadeDoTesteDeLaboratorioCredenciado;
	}

	public void setFinalidadeDoTesteDeLaboratorioCredenciado(
			FinalidadeDoTesteDeLaboratorio finalidadeDoTesteDeLaboratorioCredenciado) {
		this.finalidadeDoTesteDeLaboratorioCredenciado = finalidadeDoTesteDeLaboratorioCredenciado;
	}

	public FinalidadeDoTesteDeLaboratorio getFinalidadeDoTesteDeLaboratorioCredenciadoPublico() {
		return finalidadeDoTesteDeLaboratorioCredenciadoPublico;
	}

	public void setFinalidadeDoTesteDeLaboratorioCredenciadoPublico(
			FinalidadeDoTesteDeLaboratorio finalidadeDoTesteDeLaboratorioCredenciadoPublico) {
		this.finalidadeDoTesteDeLaboratorioCredenciadoPublico = finalidadeDoTesteDeLaboratorioCredenciadoPublico;
	}

	public FinalidadeDoTesteDeLaboratorio getFinalidadeDoTesteDeLaboratorioOficial() {
		return finalidadeDoTesteDeLaboratorioOficial;
	}

	public void setFinalidadeDoTesteDeLaboratorioOficial(
			FinalidadeDoTesteDeLaboratorio finalidadeDoTesteDeLaboratorioOficial) {
		this.finalidadeDoTesteDeLaboratorioOficial = finalidadeDoTesteDeLaboratorioOficial;
	}

	public Long getQuantidadeDeEquideosNoMomentoDaColheita() {
		return quantidadeDeEquideosNoMomentoDaColheita;
	}

	public void setQuantidadeDeEquideosNoMomentoDaColheita(
			Long quantidadeDeEquideosNoMomentoDaColheita) {
		this.quantidadeDeEquideosNoMomentoDaColheita = quantidadeDeEquideosNoMomentoDaColheita;
	}

	public String getNomeAnimal() {
		return nomeAnimal;
	}

	public void setNomeAnimal(String nomeAnimal) {
		this.nomeAnimal = nomeAnimal;
	}

	public Long getIdade() {
		return idade;
	}

	public void setIdade(Long idade) {
		this.idade = idade;
	}

	public TipoPeriodo getTipoPeriodo() {
		return tipoPeriodo;
	}

	public void setTipoPeriodo(TipoPeriodo tipoPeriodo) {
		this.tipoPeriodo = tipoPeriodo;
	}

	public String getRegistro() {
		return registro;
	}

	public void setRegistro(String registro) {
		this.registro = registro;
	}

	public String getIdEletronica() {
		return idEletronica;
	}

	public void setIdEletronica(String idEletronica) {
		this.idEletronica = idEletronica;
	}

	public String getRaca() {
		return raca;
	}

	public void setRaca(String raca) {
		this.raca = raca;
	}

	public Especie getEspecie() {
		return especie;
	}

	public void setEspecie(Especie especie) {
		this.especie = especie;
	}

	public Sexo getSexo() {
		return sexo;
	}

	public void setSexo(Sexo sexo) {
		this.sexo = sexo;
	}

	public String getPelagem() {
		return pelagem;
	}

	public void setPelagem(String pelagem) {
		this.pelagem = pelagem;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public SimNaoSI getPrenhe() {
		return prenhe;
	}

	public void setPrenhe(SimNaoSI prenhe) {
		this.prenhe = prenhe;
	}

	public Sexo getTercoGestacao() {
		return tercoGestacao;
	}

	public void setTercoGestacao(Sexo tercoGestacao) {
		this.tercoGestacao = tercoGestacao;
	}

	public br.gov.mt.indea.sistemaformin.webservice.entity.Veterinario getVeterinario() {
		return veterinario;
	}

	public void setVeterinario(br.gov.mt.indea.sistemaformin.webservice.entity.Veterinario veterinario) {
		this.veterinario = veterinario;
	}

	public Propriedade getPropriedade() {
		return propriedade;
	}

	public void setPropriedade(Propriedade propriedade) {
		this.propriedade = propriedade;
	}

	public TipoPropriedade getTipoPropriedade() {
		return tipoPropriedade;
	}

	public void setTipoPropriedade(TipoPropriedade tipoPropriedade) {
		this.tipoPropriedade = tipoPropriedade;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FormMormo other = (FormMormo) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}