package br.gov.mt.indea.sistemaformin.managedbeans;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletResponse;

import br.gov.mt.indea.sistemaformin.entity.FolhaAdicional;
import br.gov.mt.indea.sistemaformin.entity.FormAIE;
import br.gov.mt.indea.sistemaformin.entity.FormCOM;
import br.gov.mt.indea.sistemaformin.entity.FormEQ;
import br.gov.mt.indea.sistemaformin.entity.FormIN;
import br.gov.mt.indea.sistemaformin.entity.FormLAB;
import br.gov.mt.indea.sistemaformin.entity.FormMaleina;
import br.gov.mt.indea.sistemaformin.entity.FormMormo;
import br.gov.mt.indea.sistemaformin.entity.FormSH;
import br.gov.mt.indea.sistemaformin.entity.FormSN;
import br.gov.mt.indea.sistemaformin.entity.FormSRN;
import br.gov.mt.indea.sistemaformin.entity.FormSV;
import br.gov.mt.indea.sistemaformin.entity.FormVIN;
import br.gov.mt.indea.sistemaformin.entity.FormularioDeColheitaTroncoEncefalico;
import br.gov.mt.indea.sistemaformin.entity.HistoricoFormIN;
import br.gov.mt.indea.sistemaformin.entity.Resenho;
import br.gov.mt.indea.sistemaformin.exception.ApplicationErrorException;
import br.gov.mt.indea.sistemaformin.service.FormINService;
import br.gov.mt.indea.sistemaformin.util.GeradorDeRelatorio;
import net.sf.jasperreports.engine.JRException;

@Named
public class VisualizadorImpressaoManagedBean implements Serializable{
	
	private static final long serialVersionUID = -8324106220588889816L;

	@Inject
	private FormINService formINService;
	
	public void exibirFormCOM(FormCOM formCOM) throws ApplicationErrorException{
		try {
			
			FacesContext fc = FacesContext.getCurrentInstance();
			HttpServletResponse response = (HttpServletResponse) fc.getExternalContext().getResponse();
			
			response.reset();
			
			GeradorDeRelatorio.gerarPdfFormCOM(formCOM, response.getOutputStream());
			
			response.getOutputStream().flush();
			fc.responseComplete();
			
		} catch (IOException e) {
			throw new ApplicationErrorException("Erro ao gerar o pdf", e);
		}
	}
	
	public void exibirFormIN(HistoricoFormIN historicoFormIN) throws ApplicationErrorException{
		try {
			FacesContext fc = FacesContext.getCurrentInstance();
			HttpServletResponse response = (HttpServletResponse) fc.getExternalContext().getResponse();
			
			response.reset();
			
			FormIN formIN = formINService.findByIdFetchAll(historicoFormIN.getFormINCopia().getId());
			FormIN formINOriginal = formINService.findByIdFetchAll(historicoFormIN.getFormINOriginal().getId());
			GeradorDeRelatorio.gerarPdfFormIN(formIN, formINOriginal, response.getOutputStream());
			
			response.getOutputStream().flush();
			fc.responseComplete();
			
		} catch (IOException e) {
			throw new ApplicationErrorException("Erro ao gerar o pdf", e);
		}
	}
	
	public void exibirFolhaAdicional(FolhaAdicional folhaAdicional) throws ApplicationErrorException{
		List<FolhaAdicional> lista = new ArrayList<FolhaAdicional>();
		lista.add(folhaAdicional);
		
		try {
			
			byte[] pdf = GeradorDeRelatorio.gerarPdfFolhaAdicional(lista);
			
			FacesContext fc = FacesContext.getCurrentInstance();
			HttpServletResponse response = (HttpServletResponse) fc.getExternalContext().getResponse();
			response.reset();
			response.setContentLength(pdf.length);
			response.setHeader("Content-disposition", "inline; filename=folhaAdicional.pdf");
			 
			response.getOutputStream().write(pdf);
			response.getOutputStream().flush();
			fc.responseComplete();
		} catch (IOException e) {
			throw new ApplicationErrorException("Erro ao gerar o pdf", e);
		} catch (JRException e) {
			throw new ApplicationErrorException("Erro ao gerar o pdf", e);
		}
	}
	
	public void exibirFormVIN(FormVIN formVIN) throws ApplicationErrorException{
		List<FormVIN> lista = new ArrayList<FormVIN>();
		lista.add(formVIN);
		
		try {
			
			byte[] pdf = GeradorDeRelatorio.gerarPdfFormVIN(lista);
			
			FacesContext fc = FacesContext.getCurrentInstance();
			HttpServletResponse response = (HttpServletResponse) fc.getExternalContext().getResponse();
			response.reset();
			response.setContentLength(pdf.length);
			response.setHeader("Content-disposition", "inline; filename=formVIN.pdf");
			 
			response.getOutputStream().write(pdf);
			response.getOutputStream().flush();
			fc.responseComplete();
			
		} catch (JRException e) {
			throw new ApplicationErrorException("Erro ao gerar o pdf", e);
		} catch (IOException e) {
			throw new ApplicationErrorException("Erro ao gerar o pdf", e);
		}
	}
	
	public void exibirFormLAB(FormLAB formLAB) throws ApplicationErrorException{
		try {
			FacesContext fc = FacesContext.getCurrentInstance();
			HttpServletResponse response = (HttpServletResponse) fc.getExternalContext().getResponse();
			
			response.reset();
			
			GeradorDeRelatorio.gerarPdfFormLAB(formLAB, response.getOutputStream());
			
			response.getOutputStream().flush();
			fc.responseComplete();
			
		} catch (IOException e) {
			throw new ApplicationErrorException("Erro ao gerar o pdf", e);
		}
	}
	
	public void exibirFormSV(FormSV formSV) throws ApplicationErrorException{
		List<FormSV> lista = new ArrayList<FormSV>();
		lista.add(formSV);
		
		try {
			
			byte[] pdf = GeradorDeRelatorio.gerarPdfFormSV(lista);
			
			FacesContext fc = FacesContext.getCurrentInstance();
			HttpServletResponse response = (HttpServletResponse) fc.getExternalContext().getResponse();
			response.reset();
			response.setContentLength(pdf.length);
			response.setHeader("Content-disposition", "inline; filename=formSV.pdf");
			 
			response.getOutputStream().write(pdf);
			response.getOutputStream().flush();
			fc.responseComplete();
			
		} catch (JRException e) {
			throw new ApplicationErrorException("Erro ao gerar o pdf", e);
		} catch (IOException e) {
			throw new ApplicationErrorException("Erro ao gerar o pdf", e);
		}
	}
	
	public void exibirFormSH(FormSH formSH) throws ApplicationErrorException{
		List<FormSH> lista = new ArrayList<FormSH>();
		lista.add(formSH);
		
		try {
			
			byte[] pdf = GeradorDeRelatorio.gerarPdfFormSH(lista);
			
			FacesContext fc = FacesContext.getCurrentInstance();
			HttpServletResponse response = (HttpServletResponse) fc.getExternalContext().getResponse();
			response.reset();
			response.setContentLength(pdf.length);
			response.setHeader("Content-disposition", "inline; filename=formSH.pdf");
			 
			response.getOutputStream().write(pdf);
			response.getOutputStream().flush();
			fc.responseComplete();
			
		} catch (JRException e) {
			throw new ApplicationErrorException("Erro ao gerar o pdf", e);
		} catch (IOException e) {
			throw new ApplicationErrorException("Erro ao gerar o pdf", e);
		}
	}
	
	public void exibirFormSN(FormSN formSN) throws ApplicationErrorException{
		List<FormSN> lista = new ArrayList<FormSN>();
		lista.add(formSN);
		
		try {
			
			byte[] pdf = GeradorDeRelatorio.gerarPdfFormSN(lista);
			
			FacesContext fc = FacesContext.getCurrentInstance();
			HttpServletResponse response = (HttpServletResponse) fc.getExternalContext().getResponse();
			response.reset();
			response.setContentLength(pdf.length);
			response.setHeader("Content-disposition", "inline; filename=formSN.pdf");
			 
			response.getOutputStream().write(pdf);
			response.getOutputStream().flush();
			fc.responseComplete();
			
		} catch (JRException e) {
			throw new ApplicationErrorException("Erro ao gerar o pdf", e);
		} catch (IOException e) {
			throw new ApplicationErrorException("Erro ao gerar o pdf", e);
		}
	}
	
	public void exibirFormSRN(FormSRN formSRN) throws ApplicationErrorException{
		List<FormSRN> lista = new ArrayList<FormSRN>();
		lista.add(formSRN);
		
		try {
			
			byte[] pdf = GeradorDeRelatorio.gerarPdfFormSRN(lista);
			
			FacesContext fc = FacesContext.getCurrentInstance();
			HttpServletResponse response = (HttpServletResponse) fc.getExternalContext().getResponse();
			response.reset();
			response.setContentLength(pdf.length);
			response.setHeader("Content-disposition", "inline; filename=formSRN.pdf");
			 
			response.getOutputStream().write(pdf);
			response.getOutputStream().flush();
			fc.responseComplete();
			
		} catch (JRException e) {
			throw new ApplicationErrorException("Erro ao gerar o pdf", e);
		} catch (IOException e) {
			throw new ApplicationErrorException("Erro ao gerar o pdf", e);
		}
	}

	public void exibirFormEQ(FormEQ formEQ) throws ApplicationErrorException{
		try {
			FacesContext fc = FacesContext.getCurrentInstance();
			HttpServletResponse response = (HttpServletResponse) fc.getExternalContext().getResponse();
			
			response.reset();
			response.setHeader("Content-disposition", "inline; filename=formSRN.pdf");
			
			GeradorDeRelatorio.gerarPdfFormEQ(formEQ, response.getOutputStream());
			
			response.getOutputStream().flush();
			fc.responseComplete();
			
		} catch (IOException e) {
			throw new ApplicationErrorException("Erro ao gerar o pdf", e);
		}
	}

	public void exibirFormMaleina(FormMaleina formMaleina) throws ApplicationErrorException{
		List<FormMaleina> lista = new ArrayList<FormMaleina>();
		lista.add(formMaleina);
		
		try {
			
			byte[] pdf = GeradorDeRelatorio.gerarPdfFormMaleina(lista);
			
			FacesContext fc = FacesContext.getCurrentInstance();
			HttpServletResponse response = (HttpServletResponse) fc.getExternalContext().getResponse();
			response.reset();
			response.setContentLength(pdf.length);
			response.setHeader("Content-disposition", "inline; filename=formMaleina.pdf");
			 
			response.getOutputStream().write(pdf);
			response.getOutputStream().flush();
			fc.responseComplete();
			
		} catch (IOException e) {
			throw new ApplicationErrorException("Erro ao gerar o pdf", e);
		} catch (JRException e) {
			throw new ApplicationErrorException("Erro ao gerar o pdf", e);
		}
	}

	public void exibirFormMormo(FormMormo formMormo) throws ApplicationErrorException{
		List<FormMormo> lista = new ArrayList<FormMormo>();
		lista.add(formMormo);
		
		try {
			
			byte[] pdf = GeradorDeRelatorio.gerarPdfFormMormo(lista);
			
			FacesContext fc = FacesContext.getCurrentInstance();
			HttpServletResponse response = (HttpServletResponse) fc.getExternalContext().getResponse();
			response.reset();
			response.setContentLength(pdf.length);
			response.setHeader("Content-disposition", "inline; filename=formMormo.pdf");
			 
			response.getOutputStream().write(pdf);
			response.getOutputStream().flush();
			fc.responseComplete();
			
		} catch (JRException e) {
			throw new ApplicationErrorException("Erro ao gerar o pdf", e);
		} catch (IOException e) {
			throw new ApplicationErrorException("Erro ao gerar o pdf", e);
		}
	}

	public void exibirFormAIE(FormAIE formAIE) throws ApplicationErrorException{
		List<FormAIE> lista = new ArrayList<FormAIE>();
		lista.add(formAIE);
		
		try {
			
			byte[] pdf = GeradorDeRelatorio.gerarPdfFormAIE(lista);
			
			FacesContext fc = FacesContext.getCurrentInstance();
			HttpServletResponse response = (HttpServletResponse) fc.getExternalContext().getResponse();
			response.reset();
			response.setContentLength(pdf.length);
			response.setHeader("Content-disposition", "inline; filename=formAIE.pdf");
			 
			response.getOutputStream().write(pdf);
			response.getOutputStream().flush();
			fc.responseComplete();
			
		} catch (JRException e) {
			throw new ApplicationErrorException("Erro ao gerar o pdf", e);
		} catch (IOException e) {
			throw new ApplicationErrorException("Erro ao gerar o pdf", e);
		}
	}
	
	public void exibirResenho(Resenho resenho) throws ApplicationErrorException{
		List<Resenho> lista = new ArrayList<Resenho>();
		lista.add(resenho);
		
		try {
			
			byte[] pdf = GeradorDeRelatorio.gerarPdfResenho(lista);
			
			FacesContext fc = FacesContext.getCurrentInstance();
			HttpServletResponse response = (HttpServletResponse) fc.getExternalContext().getResponse();
			response.reset();
			response.setContentLength(pdf.length);
			response.setHeader("Content-disposition", "inline; filename=resenho.pdf");
			 
			response.getOutputStream().write(pdf);
			response.getOutputStream().flush();
			fc.responseComplete();
			
		} catch (JRException e) {
			throw new ApplicationErrorException("Erro ao gerar o pdf", e);
		} catch (IOException e) {
			throw new ApplicationErrorException("Erro ao gerar o pdf", e);
		}
	}

	public void exibirFormularioDeColheitaTroncoEncefalico(FormularioDeColheitaTroncoEncefalico formularioDeColheitaTroncoEncefalico) throws ApplicationErrorException {
		List<FormularioDeColheitaTroncoEncefalico> lista = new ArrayList<FormularioDeColheitaTroncoEncefalico>();
		lista.add(formularioDeColheitaTroncoEncefalico);
		
		try {
			
			byte[] pdf = GeradorDeRelatorio.gerarPdfFormularioDeColheitaTroncoEncefalico(lista);
			
			FacesContext fc = FacesContext.getCurrentInstance();
			HttpServletResponse response = (HttpServletResponse) fc.getExternalContext().getResponse();
			response.reset();
			response.setContentLength(pdf.length);
			response.setHeader("Content-disposition", "inline; filename=resenho.pdf");
			 
			response.getOutputStream().write(pdf);
			response.getOutputStream().flush();
			fc.responseComplete();
			
		} catch (JRException e) {
			throw new ApplicationErrorException("Erro ao gerar o pdf", e);
		} catch (IOException e) {
			throw new ApplicationErrorException("Erro ao gerar o pdf", e);
		}
	}

	public void exibirManualEnfermidadeAbatedouroFrigorifico() throws ApplicationErrorException {
		try {
		
			byte[] pdf = GeradorDeRelatorio.gerarPdfManualAbatedouroFrigorifico();
			
			FacesContext fc = FacesContext.getCurrentInstance();
			HttpServletResponse response = (HttpServletResponse) fc.getExternalContext().getResponse();
			response.reset();
			response.setContentLength(pdf.length);
			response.setHeader("Content-disposition", "inline; filename=resenho.pdf");
			 
			response.getOutputStream().write(pdf);
			response.getOutputStream().flush();
			fc.responseComplete();
			
		} catch (IOException e) {
			throw new ApplicationErrorException("Erro ao gerar o pdf", e);
		}
		
	}

}
