package br.gov.mt.indea.sistemaformin.xlsximport;

public enum TipoErro {
	
	NULO("Campo nulo"),
	TAMANHO_INVALIDO("Tamanho inv�lido"),
	FORMATO_INVALIDO("Formato inv�lido"),
	MULTIPLAS_DATAS("M�tiplas datas encontradas"),
	LABORATORIO_INVALIDO("Laborat�rio n�o condiz com o informado no cadastro do usu�rio"),
	TIPO_INVALIDO("Tipo inv�lido"),
	MUNICIPIO_NAO_ENCONTRADO("Munic�pio n�o encontrado");
	
	private String desc;
	
	TipoErro(String desc){
		this.desc = desc;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
	
}
