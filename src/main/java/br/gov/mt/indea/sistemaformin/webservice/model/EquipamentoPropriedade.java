package br.gov.mt.indea.sistemaformin.webservice.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class EquipamentoPropriedade implements Serializable {

    private static final long serialVersionUID = 1047828762335344087L;

	private Long id;
    
    private Integer propriedade_id;
    
    @XmlElement(name = "tipoEquipamento")
    private TipoEquipamento tipoEquipamento;
    
    private Integer qnt;

    public EquipamentoPropriedade() {
    }

    public TipoEquipamento getTipoEquipamento() {
        return tipoEquipamento;
    }

    public void setTipoEquipamento(TipoEquipamento tipoEquipamento) {
        this.tipoEquipamento = tipoEquipamento;
    }

    public Integer getQnt() {
        return qnt;
    }

    public void setQnt(Integer qnt) {
        this.qnt = qnt;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getPropriedade_id() {
		return propriedade_id;
	}

	public void setPropriedade_id(Integer propriedade_id) {
		this.propriedade_id = propriedade_id;
	}

	public EquipamentoPropriedade(
            TipoEquipamento tipoEquipamento, Integer qnt) {
        super();
        this.tipoEquipamento = tipoEquipamento;
        this.qnt = qnt;
    }

}
