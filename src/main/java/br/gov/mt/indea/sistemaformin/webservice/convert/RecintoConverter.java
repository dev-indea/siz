package br.gov.mt.indea.sistemaformin.webservice.convert;

import javax.inject.Inject;

import br.gov.mt.indea.sistemaformin.entity.Municipio;
import br.gov.mt.indea.sistemaformin.entity.Pessoa;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoUnidade;
import br.gov.mt.indea.sistemaformin.service.MunicipioService;
import br.gov.mt.indea.sistemaformin.service.ULEService;
import br.gov.mt.indea.sistemaformin.webservice.entity.Endereco;
import br.gov.mt.indea.sistemaformin.webservice.entity.Recinto;
import br.gov.mt.indea.sistemaformin.webservice.entity.TipoLogradouro;
import br.gov.mt.indea.sistemaformin.webservice.entity.ULE;

public class RecintoConverter {
	
	@Inject
	private MunicipioService municipioService;
	
	@Inject
	private ULEService uleService;
	
	public Recinto fromModelToEntity(br.gov.mt.indea.sistemaformin.webservice.model.Recinto modelo){
		if (modelo == null)
			return null;
		
		Recinto recinto = new Recinto();
		
		if (modelo.getPessoa() != null){
			recinto.setCodigoRecinto(modelo.getPessoa().getId());
			
			recinto.setApelido(modelo.getPessoa().getApelido());
			recinto.setCpfCnpj(modelo.getPessoa().getCpfCnpj());
			recinto.setEmail(modelo.getPessoa().getEmail());
			recinto.setNome(modelo.getPessoa().getNome());
			recinto.setTipoPessoa(modelo.getPessoa().getTipoPessoa());
			
			if (modelo.getPessoa().getEndereco() != null){
				Endereco enderecoPropriedade = new Endereco();
				
				enderecoPropriedade.setBairro(modelo.getPessoa().getEndereco().getBairro());
				enderecoPropriedade.setCep(modelo.getPessoa().getEndereco().getCep());
				enderecoPropriedade.setComplemento(modelo.getPessoa().getEndereco().getComplemento());
				enderecoPropriedade.setLogradouro(modelo.getPessoa().getEndereco().getLogradouro());
				enderecoPropriedade.setNumero(modelo.getPessoa().getEndereco().getNumero());
				enderecoPropriedade.setReferencia(modelo.getPessoa().getEndereco().getReferencia());
				enderecoPropriedade.setTelefone(modelo.getPessoa().getEndereco().getTelefone());
				
				if (modelo.getPessoa().getEndereco().getTipoLogradouro() != null){
					TipoLogradouro tipoLogradouroEnderecoPropriedade = new TipoLogradouro();
					tipoLogradouroEnderecoPropriedade.setNome(modelo.getPessoa().getEndereco().getTipoLogradouro().getNome());
					
					enderecoPropriedade.setTipoLogradouro(tipoLogradouroEnderecoPropriedade);
				}
				
				if (modelo.getPessoa().getEndereco().getMunicipio() != null)
					enderecoPropriedade.setMunicipio(this.findMunicipio(modelo.getPessoa().getEndereco().getMunicipio()));
				
				recinto.setEndereco(enderecoPropriedade);
			}
		}
		
		recinto.setEnderecoRecinto(modelo.getEnderecoRecinto());
		recinto.setMunicipio(this.findMunicipio(modelo.getMunicipio()));
		recinto.setObservacao(recinto.getObservacao());
		
		if (modelo.getResponsavel() != null){
			Pessoa responsavel = new Pessoa();
			
			responsavel.setApelido(modelo.getResponsavel().getApelido());
			responsavel.setCodigo(modelo.getResponsavel().getId() + "");
			responsavel.setCpf(modelo.getResponsavel().getCpfCnpj());
			responsavel.setEmail(modelo.getResponsavel().getEmail());
			responsavel.setNome(modelo.getResponsavel().getNome());
			responsavel.setTipoPessoa(modelo.getResponsavel().getTipoPessoa());
			
			if (modelo.getPessoa().getEndereco() != null){
				Endereco enderecoResponsavelRecinto = new Endereco();
				
				enderecoResponsavelRecinto.setBairro(modelo.getPessoa().getEndereco().getBairro());
				enderecoResponsavelRecinto.setCep(modelo.getPessoa().getEndereco().getCep());
				enderecoResponsavelRecinto.setComplemento(modelo.getPessoa().getEndereco().getComplemento());
				enderecoResponsavelRecinto.setLogradouro(modelo.getPessoa().getEndereco().getLogradouro());
				enderecoResponsavelRecinto.setNumero(modelo.getPessoa().getEndereco().getNumero());
				enderecoResponsavelRecinto.setReferencia(modelo.getPessoa().getEndereco().getReferencia());
				enderecoResponsavelRecinto.setTelefone(modelo.getPessoa().getEndereco().getTelefone());
				
				if (modelo.getPessoa().getEndereco().getTipoLogradouro() != null){
					TipoLogradouro tipoLogradouroEnderecoPropriedade = new TipoLogradouro();
					tipoLogradouroEnderecoPropriedade.setNome(modelo.getPessoa().getEndereco().getTipoLogradouro().getNome());
					
					enderecoResponsavelRecinto.setTipoLogradouro(tipoLogradouroEnderecoPropriedade);
				}
				
				if (modelo.getPessoa().getEndereco().getMunicipio() != null)
					enderecoResponsavelRecinto.setMunicipio(this.findMunicipio(modelo.getPessoa().getEndereco().getMunicipio()));
				
				responsavel.setEndereco(enderecoResponsavelRecinto);
			}
			
			if (modelo.getResponsavel().getPessoaFisica() != null){
				responsavel.setDataNascimento(modelo.getResponsavel().getPessoaFisica().getDataNascimento());
				responsavel.setEmissorRg(modelo.getResponsavel().getPessoaFisica().getEmissorRg());
				responsavel.setMunicipioNascimento(this.findMunicipio(modelo.getResponsavel().getPessoaFisica().getMunicipioNascimento()));
				responsavel.setNomeMae(modelo.getResponsavel().getPessoaFisica().getNomeMae());
				responsavel.setRg(modelo.getResponsavel().getPessoaFisica().getRg());
				responsavel.setSexo(modelo.getResponsavel().getPessoaFisica().getSexo());
			}
			
			recinto.setResponsavel(responsavel);
		}
		
		if (modelo.getResponsavelTecnico() != null){
			VeterinarioConverter converter = new VeterinarioConverter();
			recinto.setResponsavelTecnico(converter.fromModelToEntity(modelo.getResponsavelTecnico()));
		}
		
		if (modelo.getUle() != null){
			recinto.setUle(this.findULE(modelo.getUle()));
		}
		
		recinto.setViaAcesso(modelo.getViaAcesso());
		
		return recinto;
	}

	private Municipio findMunicipio(br.gov.mt.indea.sistemaformin.webservice.model.Municipio municipioModel){
		if (municipioModel == null)
			return null;
		
		String codgIBGE = municipioModel.getId() + "";
		
		Municipio municipio = municipioService.findByCodgIBGE(codgIBGE.substring(0, 2), codgIBGE.substring(2, codgIBGE.length()));
		
		return municipio;
	}
	
	private ULE findULE(br.gov.mt.indea.sistemaformin.webservice.model.ULE uleModel){
		if (uleModel == null)
			return null;
		
		TipoUnidade tipoUnidade = null;
		if (uleModel .getUrs() == null)
			tipoUnidade = TipoUnidade.URS;
		else
			tipoUnidade = TipoUnidade.ULE;
		
		ULE ule = uleService.findByNome(uleModel.getNome(), tipoUnidade);
		
		if (ule == null)
			if (tipoUnidade.equals(TipoUnidade.URS)){
				tipoUnidade = TipoUnidade.ULE;
				ule = uleService.findByNome(uleModel.getNome(), tipoUnidade);
			}
		
		return ule;
	}

}
