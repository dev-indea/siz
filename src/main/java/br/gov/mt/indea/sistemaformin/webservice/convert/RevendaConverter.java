package br.gov.mt.indea.sistemaformin.webservice.convert;

import javax.inject.Inject;

import br.gov.mt.indea.sistemaformin.entity.Municipio;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoUnidade;
import br.gov.mt.indea.sistemaformin.service.MunicipioService;
import br.gov.mt.indea.sistemaformin.service.ULEService;
import br.gov.mt.indea.sistemaformin.webservice.entity.Endereco;
import br.gov.mt.indea.sistemaformin.webservice.entity.Revenda;
import br.gov.mt.indea.sistemaformin.webservice.entity.TipoLogradouro;
import br.gov.mt.indea.sistemaformin.webservice.entity.ULE;

public class RevendaConverter {
	
	@Inject
	private MunicipioService municipioService;
	
	@Inject
	private ULEService uleService;
	
	public Revenda fromModelToEntity(br.gov.mt.indea.sistemaformin.webservice.model.Revenda modelo){
		if (modelo == null)
			return null;
		
		Revenda revenda = new Revenda();
		
		revenda.setCodigoRevendaSVO(modelo.getId());
		
		revenda.setApelido(modelo.getApelido());
		revenda.setCpf(modelo.getCpfCnpj());
		revenda.setDistribuidor(modelo.getDistribuidor());
		
		if (modelo.getEndereco() != null){
			Endereco enderecoRevenda = new Endereco();
			
			enderecoRevenda.setBairro(modelo.getEndereco().getBairro());
			enderecoRevenda.setCep(modelo.getEndereco().getCep());
			enderecoRevenda.setComplemento(modelo.getEndereco().getComplemento());
			enderecoRevenda.setLogradouro(modelo.getEndereco().getLogradouro());
			enderecoRevenda.setNumero(modelo.getEndereco().getNumero());
			enderecoRevenda.setReferencia(modelo.getEndereco().getReferencia());
			enderecoRevenda.setTelefone(modelo.getEndereco().getTelefone());
			
			if (modelo.getEndereco().getTipoLogradouro() != null){
				TipoLogradouro tipoLogradouroEnderecoPropriedade = new TipoLogradouro();
				tipoLogradouroEnderecoPropriedade.setNome(modelo.getEndereco().getTipoLogradouro().getNome());
				
				enderecoRevenda.setTipoLogradouro(tipoLogradouroEnderecoPropriedade);
			}
			
			if (modelo.getEndereco().getMunicipio() != null)
				enderecoRevenda.setMunicipio(this.findMunicipio(modelo.getEndereco().getMunicipio()));
			
			revenda.setEndereco(enderecoRevenda);
		}
		
		revenda.setEmail(modelo.getEmail());
		revenda.setNome(modelo.getNome());
		revenda.setOrigem(modelo.getOrigem());
		
		if (modelo.getPessoaFisica() != null){
			revenda.setDataNascimento(modelo.getPessoaFisica().getDataNascimento());
			revenda.setEmissorRg(modelo.getPessoaFisica().getEmissorRg());
			revenda.setMunicipioNascimento(this.findMunicipio(modelo.getPessoaFisica().getMunicipioNascimento()));
			revenda.setNomeMae(modelo.getPessoaFisica().getNomeMae());
			revenda.setRg(modelo.getPessoaFisica().getRg());
			revenda.setSexo(modelo.getPessoaFisica().getSexo());
		}
		
		revenda.setTipoPessoa(modelo.getTipoPessoa());
		
		if (modelo.getUle() != null){
			revenda.setUnidade(this.findULE(modelo.getUle()));
		}
		
		
		return revenda;
	}
	
	private Municipio findMunicipio(br.gov.mt.indea.sistemaformin.webservice.model.Municipio municipioModel) {
		if (municipioModel == null)
			return null;
		
		String codgIBGE = municipioModel.getId() + "";
		
		Municipio municipio = municipioService.findByCodgIBGE(codgIBGE.substring(0, 2), codgIBGE.substring(2, codgIBGE.length()));
		
		return municipio;
	}
	
	private ULE findULE(br.gov.mt.indea.sistemaformin.webservice.model.ULE uleModel){
		if (uleModel == null)
			return null;
		
		TipoUnidade tipoUnidade = null;
		if (uleModel .getUrs() == null)
			tipoUnidade = TipoUnidade.URS;
		else
			tipoUnidade = TipoUnidade.ULE;
		
		ULE ule = uleService.findByNome(uleModel.getNome(), tipoUnidade);
		
		if (ule == null)
			if (tipoUnidade.equals(TipoUnidade.URS)){
				tipoUnidade = TipoUnidade.ULE;
				ule = uleService.findByNome(uleModel.getNome(), tipoUnidade);
			}
		
		return ule;
	}

}
