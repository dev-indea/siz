package br.gov.mt.indea.sistemaformin.dao;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.inject.Inject;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import br.gov.mt.indea.sistemaformin.entity.UF;
import br.gov.mt.indea.sistemaformin.exception.ApplicationException;

@Stateless
public class UFDAO extends DAO<UF>{

	@Inject
	private EntityManager em;
	
	public UFDAO() {
		super(UF.class);
	}

	@Override
	public EntityManager getEntityManager() {
		return this.em;
	}

	public UF findBySigla(String sigla) throws ApplicationException{
		try{
			CriteriaBuilder cb = this.getEntityManager().getCriteriaBuilder();
			CriteriaQuery<UF> query = cb.createQuery(UF.class);
			
			Root<UF> t = query.from(UF.class);
			
			query.where(cb.equal(t.get("uf"), sigla));
			
			return this.getEntityManager().createQuery(query).getSingleResult();
		}catch(Exception e){
			throw new ApplicationException("N�o foi poss�vel buscar o registro", e);
		}
	}
	
}
