package br.gov.mt.indea.sistemaformin.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.mt.indea.sistemaformin.entity.Universidade;
import br.gov.mt.indea.sistemaformin.util.StringUtil;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class UniversidadeService extends PaginableService<Universidade, Long> {
	
	private static final Logger log = LoggerFactory.getLogger(UniversidadeService.class);

	protected UniversidadeService() {
		super(Universidade.class);
	}
	
	public Universidade findByIdFetchAll(Long id){
		Universidade universidade;
		
		StringBuilder sql = new StringBuilder();
		sql.append("  from Universidade universidade ")
		   .append("  join fetch universidade.endereco endereco")
		   .append(" where universidade.id = :id ");
		
		Query query = getSession().createQuery(sql.toString()).setLong("id", id);
		universidade = (Universidade) query.uniqueResult();
		
		return universidade;
	}
	
	@SuppressWarnings("unchecked")
	public List<Universidade> findAll(String nome){
		StringBuilder sql = new StringBuilder();
		sql.append("  from Universidade universidade ")
		   .append("  join fetch universidade.endereco endereco")
		   .append(" where lower(remove_acento(universidade.nome)) like :nome ")
		   .append(" order by universidade.nome");
		
		Query query = getSession().createQuery(sql.toString()).setString("nome", '%' + StringUtil.removeAcentos(nome.toLowerCase() + '%'));
		
		return query.list();
	}
	
	@Override
	public void saveOrUpdate(Universidade universidade) {
		super.saveOrUpdate(universidade);
		
		log.info("Salvando Universidade {}", universidade.getId());
	}
	
	@Override
	public void delete(Universidade universidade) {
		super.delete(universidade);
		
		log.info("Removendo Universidade {}", universidade.getId());
	}
	
	@Override
	public void validar(Universidade Universidade) {

	}

	@Override
	public void validarPersist(Universidade Universidade) {

	}

	@Override
	public void validarMerge(Universidade Universidade) {

	}

	@Override
	public void validarDelete(Universidade Universidade) {

	}

}
