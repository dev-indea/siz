package br.gov.mt.indea.sistemaformin.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import org.hibernate.envers.Audited;

@Audited
@Entity(name="info_comp_especies_form_lab")
public class InformacoesComplementaresEspeciesFormLAB extends BaseEntity<Long> implements Cloneable{

	private static final long serialVersionUID = 5646110655563046822L;
	
	@Id
	@SequenceGenerator(name="seq_info_comp_esp_form_lab", sequenceName="seq_info_comp_esp_form_lab", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq_info_comp_esp_form_lab")
	private Long id;

	private String especie;
	
	@Column(name="id_amostra")
	private Long idAmostra;
	
	@ManyToOne
	@JoinColumn(name="id_amostra2")
	private AbstractAmostra amostra;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEspecie() {
		return especie;
	}

	public void setEspecie(String especie) {
		this.especie = especie;
	}

	public Long getIdAmostra() {
		return idAmostra;
	}

	public void setIdAmostra(Long idAmostra) {
		this.idAmostra = idAmostra;
	}

	public AbstractAmostra getAmostra() {
		return amostra;
	}

	public void setAmostra(AbstractAmostra amostra) {
		this.amostra = amostra;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InformacoesComplementaresEspeciesFormLAB other = (InformacoesComplementaresEspeciesFormLAB) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}