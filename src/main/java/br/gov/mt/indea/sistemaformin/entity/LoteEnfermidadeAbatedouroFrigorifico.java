package br.gov.mt.indea.sistemaformin.entity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import br.gov.mt.indea.sistemaformin.webservice.entity.EstratificacaoGta;
import br.gov.mt.indea.sistemaformin.webservice.entity.Gta;
import br.gov.mt.indea.sistemaformin.webservice.entity.PessoaOrigemGta;

@Audited
@Entity
@Table(name="lote_enfermidade_abat_frig")
public class LoteEnfermidadeAbatedouroFrigorifico extends BaseEntity<Long>{

	private static final long serialVersionUID = -7525368149110619959L;

	@Id
	@SequenceGenerator(name="lote_enfermidade_abat_frig_seq", sequenceName="lote_enfermidade_abat_frig_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="lote_enfermidade_abat_frig_seq")
	private Long id;
	
	@ManyToOne
	@JoinColumn(name="id_enfermidade_abat_frig")
	private EnfermidadeAbatedouroFrigorifico enfermidadeAbatedouroFrigorifico;
	
	private Long numero;
	
	@OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_pessoa_origem")
    private PessoaOrigemGta proprietario;
	
	@OneToMany(mappedBy="loteEnfermidadeAbatedouroFrigorifico", orphanRemoval=true, cascade={CascadeType.ALL}, fetch=FetchType.LAZY)
	private List<GtaEnfermidadeAbatedouroFrigorifico> listaGtaEnfermidadeAbatedouroFrigorifico = new ArrayList<GtaEnfermidadeAbatedouroFrigorifico>();
	
	@OneToMany(mappedBy="loteEnfermidadeAbatedouroFrigorifico", orphanRemoval=true, cascade={CascadeType.ALL}, fetch=FetchType.LAZY)
	private List<AchadosLoteEnfermidadeAbatedouroFrigorifico> listaAchadosLoteEnfermidadeAbatedouroFrigorifico = new ArrayList<AchadosLoteEnfermidadeAbatedouroFrigorifico>();
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_formin")
	private FormIN formIN;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_visita_propriedade_rural")
	private VisitaPropriedadeRural visitaPropriedadeRural;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_vigilancia_veterinaria")
	private VigilanciaVeterinaria vigilanciaVeterinaria;

	public boolean isInvestigacaoFinalizada(){
		if (this.houveColheita())
			return this.visitaPropriedadeRural != null && this.formIN.isInvestigacaoEncerrada();
		else
			return this.visitaPropriedadeRural != null && this.vigilanciaVeterinaria != null;
		
		
	}
	
	public boolean contem(DoencaEnfermidadeAbatedouroFrigorifico doencaEnfermidadeAbatedouroFrigorifico){
		if (this.listaAchadosLoteEnfermidadeAbatedouroFrigorifico != null){
			for (AchadosLoteEnfermidadeAbatedouroFrigorifico detalheAchados : listaAchadosLoteEnfermidadeAbatedouroFrigorifico) {
				if (detalheAchados.getDoencaEnfermidadeAbatedouroFrigorifico().equals(doencaEnfermidadeAbatedouroFrigorifico))
					return true;
			}
		}
		
		return false;
	}
	
	public boolean contemGta(String serie, Integer numero){
		Gta gta = new Gta();
		gta.setNumero(numero);
		gta.setSerie(serie);
		
		for (GtaEnfermidadeAbatedouroFrigorifico gtaEnfermidadeAbatedouroFrigorifico : listaGtaEnfermidadeAbatedouroFrigorifico) {
			if (gtaEnfermidadeAbatedouroFrigorifico.getGta().equals(gta))
				return true;
		}
		
		return false;
	}

	public String getGTAAsString(){
		if (this.getListaGtaEnfermidadeAbatedouroFrigorifico() == null || this.getListaGtaEnfermidadeAbatedouroFrigorifico().isEmpty())
			return null;
		
		StringBuilder sb = null;
		
		for (GtaEnfermidadeAbatedouroFrigorifico detalheEnfermidadeAbatedouroFrigorifico : getListaGtaEnfermidadeAbatedouroFrigorifico()) {
			if (sb == null){
				sb = new StringBuilder();
				sb.append(detalheEnfermidadeAbatedouroFrigorifico.getGta().getNumero())
				  .append(" - ")
				  .append(detalheEnfermidadeAbatedouroFrigorifico.getGta().getSerie());
			}else
				sb.append(", ")
				  .append(detalheEnfermidadeAbatedouroFrigorifico.getGta().getNumero())
				  .append(" - ")
				  .append(detalheEnfermidadeAbatedouroFrigorifico.getGta().getSerie());
		}
		
		return sb.toString();
	}
	
	public String getAchadosLoteEnfermidadeAbatedouroFrigorificoAsString(){
		if (this.getListaGtaEnfermidadeAbatedouroFrigorifico() == null || this.getListaGtaEnfermidadeAbatedouroFrigorifico().isEmpty())
			return null;
		
		StringBuilder sb = null;
		
		if (listaAchadosLoteEnfermidadeAbatedouroFrigorifico != null && !listaAchadosLoteEnfermidadeAbatedouroFrigorifico.isEmpty())
			for (AchadosLoteEnfermidadeAbatedouroFrigorifico achado : listaAchadosLoteEnfermidadeAbatedouroFrigorifico) {
				
				if (achado.getQuantidadeCasos() != null && achado.getQuantidadeCasos() != 0){
					if (sb == null){
						sb = new StringBuilder();
						sb.append(achado.getDoencaEnfermidadeAbatedouroFrigorifico().getNome())
						  .append(" - ")
						  .append(achado.getQuantidadeCasos());
					}else
						sb.append(", ")
						  .append(achado.getDoencaEnfermidadeAbatedouroFrigorifico().getNome())
						  .append(" - ")
						  .append(achado.getQuantidadeCasos());
					}
			}
		
		return sb == null ? "" : sb.toString();
	}
	
	public GtaEnfermidadeAbatedouroFrigorifico getDetalheEnfermidadeAbatedouroFrigorifico(Gta gta){
		for (GtaEnfermidadeAbatedouroFrigorifico detalheEnfermidadeAbatedouroFrigorifico : listaGtaEnfermidadeAbatedouroFrigorifico) {
			if (detalheEnfermidadeAbatedouroFrigorifico.getGta().equals(gta))
				return detalheEnfermidadeAbatedouroFrigorifico;
		}
		
		return null;
	}
	
	public boolean houveColheita(){
		boolean houveColheita = false;
		
		if (this.listaAchadosLoteEnfermidadeAbatedouroFrigorifico != null)
			for (AchadosLoteEnfermidadeAbatedouroFrigorifico achado : listaAchadosLoteEnfermidadeAbatedouroFrigorifico) {
				if (achado.isHouveFormularioColheita())
					houveColheita = true;
			}
		
		return houveColheita;
	}
	
	public Long getQuantidadeDeAnimaisNoLote(){
		Long quantidade = 0L;
		
		if (this.getListaGtaEnfermidadeAbatedouroFrigorifico() != null)
			for (GtaEnfermidadeAbatedouroFrigorifico gta : listaGtaEnfermidadeAbatedouroFrigorifico) {
				if (gta.getGta().getEstratificacoes() != null)
					for (EstratificacaoGta estratificacaoGta : gta.getGta().getEstratificacoes()) {
						quantidade += estratificacaoGta.getQntEnviada();
					}
			}
			
			
		return quantidade;
	}
	
	public boolean hasFormulario() {
		for (AchadosLoteEnfermidadeAbatedouroFrigorifico achadosLoteEnfermidadeAbatedouroFrigorifico : listaAchadosLoteEnfermidadeAbatedouroFrigorifico) {
			if (achadosLoteEnfermidadeAbatedouroFrigorifico.getListaFormularioDeColheitaTroncoEncefalico() != null && !achadosLoteEnfermidadeAbatedouroFrigorifico.getListaFormularioDeColheitaTroncoEncefalico().isEmpty())
				return true;
		}
		return false;
	}
	
	public List<FormularioDeColheitaTroncoEncefalico> getListaFormularioDeColheitaTroncoEncefalico(){
		List<FormularioDeColheitaTroncoEncefalico> lista = null;
		
		for (AchadosLoteEnfermidadeAbatedouroFrigorifico achadosLoteEnfermidadeAbatedouroFrigorifico : listaAchadosLoteEnfermidadeAbatedouroFrigorifico) {
			if (achadosLoteEnfermidadeAbatedouroFrigorifico.getListaFormularioDeColheitaTroncoEncefalico() != null && !achadosLoteEnfermidadeAbatedouroFrigorifico.getListaFormularioDeColheitaTroncoEncefalico().isEmpty()) {
				if (lista == null)
					lista = new ArrayList<>();
				lista.addAll(achadosLoteEnfermidadeAbatedouroFrigorifico.getListaFormularioDeColheitaTroncoEncefalico());
			}
		}
		
		return lista;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public EnfermidadeAbatedouroFrigorifico getEnfermidadeAbatedouroFrigorifico() {
		return enfermidadeAbatedouroFrigorifico;
	}

	public void setEnfermidadeAbatedouroFrigorifico(EnfermidadeAbatedouroFrigorifico enfermidadeAbatedouroFrigorifico) {
		this.enfermidadeAbatedouroFrigorifico = enfermidadeAbatedouroFrigorifico;
	}

	public Long getNumero() {
		return numero;
	}

	public void setNumero(Long numero) {
		this.numero = numero;
	}

	public List<GtaEnfermidadeAbatedouroFrigorifico> getListaGtaEnfermidadeAbatedouroFrigorifico() {
		if (listaGtaEnfermidadeAbatedouroFrigorifico.size() > 0) {
			  Collections.sort(listaGtaEnfermidadeAbatedouroFrigorifico, new Comparator<GtaEnfermidadeAbatedouroFrigorifico>() {
			      @Override
			      public int compare(final GtaEnfermidadeAbatedouroFrigorifico object1, final GtaEnfermidadeAbatedouroFrigorifico object2) {
			          return object1.getGta().getNumero().compareTo(object2.getGta().getNumero());
			      }
			  });
			}
		
		return listaGtaEnfermidadeAbatedouroFrigorifico;
	}

	public void setListaGtaEnfermidadeAbatedouroFrigorifico(
			List<GtaEnfermidadeAbatedouroFrigorifico> listaGtaEnfermidadeAbatedouroFrigorifico) {
		this.listaGtaEnfermidadeAbatedouroFrigorifico = listaGtaEnfermidadeAbatedouroFrigorifico;
	}

	public List<AchadosLoteEnfermidadeAbatedouroFrigorifico> getListaAchadosLoteEnfermidadeAbatedouroFrigorifico() {
		return listaAchadosLoteEnfermidadeAbatedouroFrigorifico;
	}

	public void setListaAchadosLoteEnfermidadeAbatedouroFrigorifico(
			List<AchadosLoteEnfermidadeAbatedouroFrigorifico> listaAchadosLoteEnfermidadeAbatedouroFrigorifico) {
		this.listaAchadosLoteEnfermidadeAbatedouroFrigorifico = listaAchadosLoteEnfermidadeAbatedouroFrigorifico;
	}

	public PessoaOrigemGta getProprietario() {
		return proprietario;
	}

	public void setProprietario(PessoaOrigemGta proprietario) {
		this.proprietario = proprietario;
	}

	public FormIN getFormIN() {
		return formIN;
	}

	public void setFormIN(FormIN formIN) {
		this.formIN = formIN;
	}

	public VisitaPropriedadeRural getVisitaPropriedadeRural() {
		return visitaPropriedadeRural;
	}

	public void setVisitaPropriedadeRural(VisitaPropriedadeRural visitaPropriedadeRural) {
		this.visitaPropriedadeRural = visitaPropriedadeRural;
	}

	public VigilanciaVeterinaria getVigilanciaVeterinaria() {
		return vigilanciaVeterinaria;
	}

	public void setVigilanciaVeterinaria(VigilanciaVeterinaria vigilanciaVeterinaria) {
		this.vigilanciaVeterinaria = vigilanciaVeterinaria;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((numero == null) ? 0 : numero.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LoteEnfermidadeAbatedouroFrigorifico other = (LoteEnfermidadeAbatedouroFrigorifico) obj;
		if (numero == null) {
			if (other.numero != null)
				return false;
		} else if (!numero.equals(other.numero))
			return false;
		return true;
	}

}
