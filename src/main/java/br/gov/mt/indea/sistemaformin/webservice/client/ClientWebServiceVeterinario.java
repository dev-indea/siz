package br.gov.mt.indea.sistemaformin.webservice.client;

import java.io.Serializable;
import java.net.URISyntaxException;

import javax.inject.Inject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import br.gov.mt.indea.sistemaformin.exception.ApplicationException;
import br.gov.mt.indea.sistemaformin.webservice.convert.VeterinarioConverter;
import br.gov.mt.indea.sistemaformin.webservice.entity.Servidor;
import br.gov.mt.indea.sistemaformin.webservice.model.Veterinario;


public class ClientWebServiceVeterinario implements Serializable{
	
	private static final long serialVersionUID = -5887697984204696148L;

	protected String URL_WS = "https://sistemas.indea.mt.gov.br/FronteiraWeb/ws/profissional/";
	
//	@Inject
//	@Category("WEBSERVICE::VETERINÁRIO")
//	private CustomLogger log;
	
	@Inject
	private VeterinarioConverter veterinarioConverter;
	
	@Inject
	private ClientWebServiceServidor clientWebServiceServidor;
	
	public br.gov.mt.indea.sistemaformin.webservice.entity.Veterinario getVeterinarioByCRMV(String crmv) throws ApplicationException {

		String[] resposta = null;
		try {
			System.out.println(URL_WS + "findVeterinarioByCRMV/" + crmv);
			resposta = new WebServiceCliente().get(URL_WS + "findVeterinarioByCRMV/" + crmv);
		} catch (URISyntaxException e) {
			//log.excecao(e.getMessage(), e);
			throw new ApplicationException("Erro ao buscar o veterinário");
		}
		
		if (resposta == null || resposta[0] == null){
			return null;
		}
		
		if (resposta[0].equals("200")) {
			GsonBuilder gsonBuilder = new GsonBuilder().setDateFormat("dd/MM/yyyy");
			Gson gson = gsonBuilder.create();
			Veterinario veterinario = gson.fromJson(resposta[1], Veterinario.class);
			
			br.gov.mt.indea.sistemaformin.webservice.entity.Veterinario veterinarioConvertido = veterinarioConverter.fromModelToEntity(veterinario);
			if (veterinarioConvertido != null){
					Servidor servidorByCpf = this.getServidorByCpf(veterinarioConvertido.getCpf());
				if (servidorByCpf != null)
					veterinarioConvertido.setMatricula(servidorByCpf.getMatricula());
			}
			
			return veterinarioConvertido;
		} else if (resposta[0].equals("404")) {
			//log.excecao(resposta[0] + ". " + resposta[1], null);
			throw new ApplicationException("Webservice com problemas. Contate o administrador do sistema");
		} else if (resposta[0].equals("500")) {
			//log.excecao(resposta[0] + ". " + resposta[1], null);
			throw new ApplicationException("Webservice com problemas. Contate o administrador do sistema");
		} else if (resposta[0].equals("400")) {
			//log.excecao(resposta[0] + ". " + resposta[1], null);
			throw new ApplicationException("Webservice com problemas. Contate o administrador do sistema");
		} else{
			//log.excecao(resposta[0] + ". " + resposta[1], null);
			throw new ApplicationException("Webservice inativo. Tente novamente em alguns minutos. Erro: " + resposta[0]);
		} 
	}
	
	public br.gov.mt.indea.sistemaformin.webservice.entity.Veterinario getVeterinarioByCpf(String cpf) throws ApplicationException {
		cpf = cpf.replace(".", "").replace("-", "");
		String[] resposta = null;
		try {
			System.out.println(URL_WS + "findVeterinarioByCpf/" + cpf);
			resposta = new WebServiceCliente().get(URL_WS + "findVeterinarioByCpf/" + cpf);
		} catch (URISyntaxException e) {
			//log.excecao(e.getMessage(), e);
			throw new ApplicationException("Erro ao buscar o veterinário");
		}
		
		if (resposta == null || resposta[0] == null){
			return null;
		}
		
		if (resposta[0].equals("200")) {
			GsonBuilder gsonBuilder = new GsonBuilder().setDateFormat("dd/MM/yyyy");
			Gson gson = gsonBuilder.create();
			Veterinario veterinario = gson.fromJson(resposta[1], Veterinario.class);
			
			br.gov.mt.indea.sistemaformin.webservice.entity.Veterinario veterinarioConvertido = veterinarioConverter.fromModelToEntity(veterinario);
			
			if (veterinarioConvertido != null){
				Servidor servidorByCpf = this.getServidorByCpf(veterinarioConvertido.getCpf());
				if (servidorByCpf != null)
					veterinarioConvertido.setMatricula(servidorByCpf.getMatricula());
			}
			
			return veterinarioConvertido;
		} else if (resposta[0].equals("404")) {
			//log.excecao(resposta[0] + ". " + resposta[1], null);
			throw new ApplicationException("Webservice com problemas. Contate o administrador do sistema");
		} else if (resposta[0].equals("500")) {
			//log.excecao(resposta[0] + ". " + resposta[1], null);
			throw new ApplicationException("Webservice com problemas. Contate o administrador do sistema");
		} else if (resposta[0].equals("400")) {
			//log.excecao(resposta[0] + ". " + resposta[1], null);
			throw new ApplicationException("Webservice com problemas. Contate o administrador do sistema");
		} else{
			//log.excecao(resposta[0] + ". " + resposta[1], null);
			throw new ApplicationException("Webservice inativo. Tente novamente em alguns minutos. Erro: " + resposta[0]);
		} 
	}
	
	private Servidor getServidorByCpf(String cpf) throws ApplicationException{
		return clientWebServiceServidor.getServidorByCpf(cpf);
	}
	
}
