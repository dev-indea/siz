package br.gov.mt.indea.sistemaformin.webservice.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class EstratificacaoGta implements Serializable {

	private static final long serialVersionUID = -6589394251804702859L;

	private Long id;
    
	@XmlElement
    private Estratificacao estratificacao;
    
	private Integer qntEnviada;
    
	private Integer qntRecebida;
    
	private String itemValido;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Estratificacao getEstratificacao() {
        return estratificacao;
    }

    public void setEstratificacao(Estratificacao estratificacao) {
        this.estratificacao = estratificacao;
    }

    public Integer getQntEnviada() {
        return qntEnviada;
    }

    public void setQntEnviada(Integer qntEnviada) {
        this.qntEnviada = qntEnviada;
    }

    public Integer getQntRecebida() {
        return qntRecebida;
    }

    public void setQntRecebida(Integer qntRecebida) {
        this.qntRecebida = qntRecebida;
    }

    public String getItemValido() {
        return itemValido;
    }

    public void setItemValido(String itemValido) {
        this.itemValido = itemValido;
    }

}
