package br.gov.mt.indea.sistemaformin.managedbeans;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLBeanName;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;

import br.gov.mt.indea.sistemaformin.entity.FormularioDeColheitaTroncoEncefalico;
import br.gov.mt.indea.sistemaformin.entity.Municipio;
import br.gov.mt.indea.sistemaformin.entity.RelatorioDeEnsaio;
import br.gov.mt.indea.sistemaformin.entity.ServicoDeInspecao;
import br.gov.mt.indea.sistemaformin.entity.dto.FormularioDeColheitaTroncoEncefalicoDTO;
import br.gov.mt.indea.sistemaformin.enums.Dominio.ResultadoRelatorioDeEnsaio;
import br.gov.mt.indea.sistemaformin.security.UserSecurity;
import br.gov.mt.indea.sistemaformin.service.FormularioDeColheitaTroncoEncefalicoService;
import br.gov.mt.indea.sistemaformin.service.LazyObjectDataModel;
import br.gov.mt.indea.sistemaformin.service.MunicipioService;
import br.gov.mt.indea.sistemaformin.service.ServicoDeInspecaoService;
import br.gov.mt.indea.sistemaformin.util.FacesMessageUtil;

@Named("pesquisaFormularioColheitaManagedBean")
@ViewScoped
@URLBeanName("pesquisaFormularioColheitaManagedBean")
@URLMappings(mappings = {
		@URLMapping(id="pesquisarFormularioColheita", pattern="/formularioDeColheita/pesquisar", viewId="/pages/enfermidadeAbatedouroFrigorifico/relatorio-ensaio/lista.jsf")})
public class PesquisaFormularioColheitaManagedBean implements Serializable{
	private static final long serialVersionUID = 1494107859776182081L;

	@Inject
	private UserSecurity userSecurity;
	
	@Inject
	LoginManagedBean loginManagedBean;
	
	@Inject
	private FormularioDeColheitaTroncoEncefalicoDTO formularioDeColheitaTroncoEncefalicoDTO;
	
	private FormularioDeColheitaTroncoEncefalico formularioDeColheitaTroncoEncefalico;
	
	private RelatorioDeEnsaio relatorioDeEnsaio;

	private LazyObjectDataModel<FormularioDeColheitaTroncoEncefalico> listaFormularioDeColheitaTroncoEncefalico;
	
	private List<ServicoDeInspecao> listaServicoDeInspecao;
	
	private List<Municipio> listaMunicipio;
	
	private Date dataRelatorio;
	
	private boolean visualizandoRelatorioDeEnsaio;
	
	@Inject
	private ServicoDeInspecaoService servicoDeInspecaoService;
	
	@Inject
	private MunicipioService municipioService;
	
	@Inject
	private FormularioDeColheitaTroncoEncefalicoService formularioDeColheitaTroncoEncefalicoService;
	
	@PostConstruct
	public void init(){
		listaServicoDeInspecao = servicoDeInspecaoService.findAll();
		listaMunicipio = municipioService.findAllByUF(this.formularioDeColheitaTroncoEncefalicoDTO.getUf());
	}
	
	@URLAction(mappingId="pesquisarFormularioColheita", onPostback = false)
	public void busca(){
		
		this.buscarListaServicoDeInspecao();
		
		if (loginManagedBean.getUserSecurity().hasAuthorityAdmin())
			return;
		else if (loginManagedBean.getUserSecurity().getUsuario().getServicoDeInspecaoEmUso() == null)
			FacesMessageUtil.addWarnContextFacesMessage("O usu�rio n�o possui Servi�o de Inspe��o vinculado. Procure o administrador do sistema", "");
		else
			this.formularioDeColheitaTroncoEncefalicoDTO.setServicoDeInspecao(loginManagedBean.getUserSecurity().getUsuario().getServicoDeInspecaoEmUso());
		
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		Long idFormularioColheita = (Long) request.getSession().getAttribute("idFormularioColheita");
		
		if (idFormularioColheita != null){
//			this.formularioDeColheitaTroncoEncefalicoDTO = new FormularioDeColheitaTroncoEncefalicoDTO();
//			this.formularioDeColheitaTroncoEncefalicoDTO.setId(idFormularioColheita);
//			this.buscarFormularioColheita();
//			
//			if (loginManagedBean.getUserSecurity().hasAuthorityAdmin())
//				return;
//			else
//				this.formularioDeColheitaTroncoEncefalicoDTO.setServicoDeInspecao(loginManagedBean.getUserSecurity().getUsuario().getServicoDeInspecaoEmUso());
			
			request.getSession().setAttribute("idFormularioColheita", null);
		}
		
	}
	
	public void buscarEnfermidadeAbatedouroFrigorifico(){
		listaFormularioDeColheitaTroncoEncefalico = new LazyObjectDataModel<FormularioDeColheitaTroncoEncefalico>(this.formularioDeColheitaTroncoEncefalicoService, formularioDeColheitaTroncoEncefalicoDTO);
	}
	
	private void buscarListaServicoDeInspecao(){
		listaServicoDeInspecao = servicoDeInspecaoService.findAll();
	}
	
	public void salvarRelatorioDeEnsaio() {
		if (this.relatorioDeEnsaio.getDataRelatorio() == null)
			this.relatorioDeEnsaio.setDataRelatorio(Calendar.getInstance());
		this.relatorioDeEnsaio.getDataRelatorio().setTime(this.dataRelatorio);
		
		this.formularioDeColheitaTroncoEncefalico.setRelatorioDeEnsaio(this.relatorioDeEnsaio);
		formularioDeColheitaTroncoEncefalicoService.saveOrUpdate(formularioDeColheitaTroncoEncefalico);
		FacesMessageUtil.addInfoContextFacesMessage("Relat�rio de Ensaio salvo com sucesso", "");
		this.dataRelatorio = null;
	}
	
	public void abrirTelaDeCadastroDeRelatorioDeEnsaio(FormularioDeColheitaTroncoEncefalico formularioDeColheitaTroncoEncefalico) {
		visualizandoRelatorioDeEnsaio = false;
		this.dataRelatorio = null;
		
		this.formularioDeColheitaTroncoEncefalico = formularioDeColheitaTroncoEncefalico;
		this.relatorioDeEnsaio = new RelatorioDeEnsaio(formularioDeColheitaTroncoEncefalico);
	}
	
	public void abrirTelaDeVisualizacaoDeRelatorioDeEnsaio(FormularioDeColheitaTroncoEncefalico formularioDeColheitaTroncoEncefalico) {
		visualizandoRelatorioDeEnsaio = true;
		
		this.formularioDeColheitaTroncoEncefalico = formularioDeColheitaTroncoEncefalico;
		this.relatorioDeEnsaio = this.formularioDeColheitaTroncoEncefalico.getRelatorioDeEnsaio();
		
		this.dataRelatorio = this.relatorioDeEnsaio.getDataRelatorio().getTime();
	}
	
	public void descartar(FormularioDeColheitaTroncoEncefalico formularioDeColheitaTroncoEncefalico) {
		if (this.relatorioDeEnsaio.getDataRelatorio() == null)
			this.relatorioDeEnsaio.setDataRelatorio(Calendar.getInstance());
		this.relatorioDeEnsaio.getDataRelatorio().setTime(this.dataRelatorio);
		this.relatorioDeEnsaio.setResultadoRelatorioDeEnsaio(ResultadoRelatorioDeEnsaio.DESCARTADO);
		
		this.formularioDeColheitaTroncoEncefalico.setRelatorioDeEnsaio(this.relatorioDeEnsaio);
		formularioDeColheitaTroncoEncefalicoService.saveOrUpdate(formularioDeColheitaTroncoEncefalico);
		FacesMessageUtil.addInfoContextFacesMessage("Descarte da amostra salvo com sucesso", "");
		this.dataRelatorio = null;
	}

	public LazyObjectDataModel<FormularioDeColheitaTroncoEncefalico> getListaFormularioDeColheitaTroncoEncefalico() {
		return listaFormularioDeColheitaTroncoEncefalico;
	}

	public void setListaFormularioDeColheitaTroncoEncefalico(
			LazyObjectDataModel<FormularioDeColheitaTroncoEncefalico> listaFormularioDeColheitaTroncoEncefalico) {
		this.listaFormularioDeColheitaTroncoEncefalico = listaFormularioDeColheitaTroncoEncefalico;
	}

	public UserSecurity getUserSecurity() {
		return userSecurity;
	}

	public void setUserSecurity(UserSecurity userSecurity) {
		this.userSecurity = userSecurity;
	}

	public FormularioDeColheitaTroncoEncefalicoDTO getFormularioDeColheitaTroncoEncefalicoDTO() {
		return formularioDeColheitaTroncoEncefalicoDTO;
	}

	public void setFormularioDeColheitaTroncoEncefalicoDTO(FormularioDeColheitaTroncoEncefalicoDTO formularioDeColheitaTroncoEncefalicoDTO) {
		this.formularioDeColheitaTroncoEncefalicoDTO = formularioDeColheitaTroncoEncefalicoDTO;
	}

	public List<ServicoDeInspecao> getListaServicoDeInspecao() {
		return listaServicoDeInspecao;
	}

	public void setListaServicoDeInspecao(List<ServicoDeInspecao> listaServicoDeInspecao) {
		this.listaServicoDeInspecao = listaServicoDeInspecao;
	}

	public ServicoDeInspecaoService getServicoDeInspecaoService() {
		return servicoDeInspecaoService;
	}

	public void setServicoDeInspecaoService(ServicoDeInspecaoService servicoDeInspecaoService) {
		this.servicoDeInspecaoService = servicoDeInspecaoService;
	}

	public List<Municipio> getListaMunicipio() {
		return listaMunicipio;
	}

	public void setListaMunicipio(List<Municipio> listaMunicipio) {
		this.listaMunicipio = listaMunicipio;
	}

	public FormularioDeColheitaTroncoEncefalico getFormularioDeColheitaTroncoEncefalico() {
		return formularioDeColheitaTroncoEncefalico;
	}

	public void setFormularioDeColheitaTroncoEncefalico(
			FormularioDeColheitaTroncoEncefalico formularioDeColheitaTroncoEncefalico) {
		this.formularioDeColheitaTroncoEncefalico = formularioDeColheitaTroncoEncefalico;
	}

	public RelatorioDeEnsaio getRelatorioDeEnsaio() {
		return relatorioDeEnsaio;
	}

	public void setRelatorioDeEnsaio(RelatorioDeEnsaio relatorioDeEnsaio) {
		this.relatorioDeEnsaio = relatorioDeEnsaio;
	}

	public Date getDataRelatorio() {
		return dataRelatorio;
	}

	public void setDataRelatorio(Date dataRelatorio) {
		this.dataRelatorio = dataRelatorio;
	}

	public boolean isVisualizandoRelatorioDeEnsaio() {
		return visualizandoRelatorioDeEnsaio;
	}

	public void setVisualizandoRelatorioDeEnsaio(boolean visualizandoRelatorioDeEnsaio) {
		this.visualizandoRelatorioDeEnsaio = visualizandoRelatorioDeEnsaio;
	}

}
