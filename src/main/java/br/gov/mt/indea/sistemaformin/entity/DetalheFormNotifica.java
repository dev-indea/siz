package br.gov.mt.indea.sistemaformin.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import br.gov.mt.indea.sistemaformin.webservice.entity.Propriedade;

@Audited
@Entity
@Table(name="detalhe_form_notifica")
public class DetalheFormNotifica extends BaseEntity<Long>{

	private static final long serialVersionUID = 6459515682267284361L;

	@Id
	@SequenceGenerator(name="detalhe_form_notifica_seq", sequenceName="detalhe_form_notifica_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="detalhe_form_notifica_seq")
	private Long id;
	
	@ManyToOne
	@JoinColumn(name="id_form_notifica")
	private FormNotifica formNotifica;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_propriedade")
	private Propriedade propriedade;
	
	@ManyToOne
	@JoinColumn(name="id_formin")
	private FormIN formIN;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_visita_propriedade_rural")
	private VisitaPropriedadeRural visitaPropriedadeRural;

	public boolean isInvestigacaoFinalizada(){
		return this.visitaPropriedadeRural != null || this.formIN.isInvestigacaoEncerrada();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Propriedade getPropriedade() {
		return propriedade;
	}

	public void setPropriedade(Propriedade propriedade) {
		this.propriedade = propriedade;
	}

	public FormNotifica getFormNotifica() {
		return formNotifica;
	}

	public void setFormNotifica(FormNotifica formNotifica) {
		this.formNotifica = formNotifica;
	}

	public FormIN getFormIN() {
		return formIN;
	}

	public void setFormIN(FormIN formIN) {
		this.formIN = formIN;
	}

	public VisitaPropriedadeRural getVisitaPropriedadeRural() {
		return visitaPropriedadeRural;
	}

	public void setVisitaPropriedadeRural(VisitaPropriedadeRural visitaPropriedadeRural) {
		this.visitaPropriedadeRural = visitaPropriedadeRural;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DetalheFormNotifica other = (DetalheFormNotifica) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
