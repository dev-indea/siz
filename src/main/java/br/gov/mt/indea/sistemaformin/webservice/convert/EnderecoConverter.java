package br.gov.mt.indea.sistemaformin.webservice.convert;

import javax.inject.Inject;

import br.gov.mt.indea.sistemaformin.entity.Municipio;
import br.gov.mt.indea.sistemaformin.service.MunicipioService;
import br.gov.mt.indea.sistemaformin.webservice.entity.Endereco;
import br.gov.mt.indea.sistemaformin.webservice.entity.ViaCep;

public class EnderecoConverter {
	
	@Inject
	private MunicipioService municipioService;
	
	public Endereco fromModelToEntity(ViaCep modelo){
		Endereco endereco = new Endereco();
		
		if (modelo == null)
			return null;
		
		endereco.setBairro(modelo.getBairro());
		endereco.setCep(modelo.getCep());
		endereco.setComplemento(modelo.getComplemento());
		endereco.setLogradouro(modelo.getLogradouro());
		endereco.setMunicipio(this.findMunicipio(modelo.getIbge()));
		
		return endereco;
	}

	private Municipio findMunicipio(String codgIBGE) {
		if (codgIBGE == null)
			return null;
		
		Municipio municipio = municipioService.findByCodgIBGE(codgIBGE.substring(0, 2), codgIBGE.substring(2, codgIBGE.length()));
		
		return municipio;
	}
	
}
