package br.gov.mt.indea.sistemaformin.service;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.inject.Singleton;
import javax.persistence.Entity;

import org.omnifaces.cdi.Startup;
import org.reflections.Reflections;

@Singleton
@Startup
public class CacheUniqueFields {

    private static final HashMap<Class<?>, List<Field>> cache = new HashMap<Class<?>, List<Field>>();

    /**
     * Retorna lista de java.lang.reflect.Field anotados com br.gov.mt.cepromat.ceprofw.common.model.UniqueField 
     * @param classe
     * @return
     * @see java.lang.reflect.Field
     * @see br.gov.mt.cepromat.ceprofw.common.model.UniqueValueField
     */
    public List<Field> get(Class<?> classe) {
        return cache.get(classe);
    }
    
    public boolean hasUniqueFields(Class<?> classe){
        return cache.containsKey(classe);
    }

    @PostConstruct
    public void onStartup() {
       
        Reflections reflections = new Reflections("br.gov.mt.cepromat");
        
        Set<Class<?>> annotatedClasses = reflections.getTypesAnnotatedWith(Entity.class);

        for (Class<?> classe : annotatedClasses) {

            Field campos[] = classe.getDeclaredFields();
            for (Field campo : campos) {

                Annotation uniqueFieldAnnotation = campo.getAnnotation(UniqueValueField.class);

                if (uniqueFieldAnnotation != null) {
                    
                    if(!cache.containsKey(classe)){
                        cache.put(classe, new ArrayList<Field>());
                    }
                    
                    List<Field> camposUnicosDaClasse = get(classe);
                    camposUnicosDaClasse.add(campo);
                }
            }
        }
    }
}
