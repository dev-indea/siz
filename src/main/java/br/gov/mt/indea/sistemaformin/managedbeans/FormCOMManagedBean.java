package br.gov.mt.indea.sistemaformin.managedbeans;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.constraints.Past;

import org.primefaces.event.FlowEvent;

import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLBeanName;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;

import br.gov.mt.indea.sistemaformin.entity.DetalhesInformacoesAbelhas_FormCOM;
import br.gov.mt.indea.sistemaformin.entity.DetalhesInformacoesAsininos_FormCOM;
import br.gov.mt.indea.sistemaformin.entity.DetalhesInformacoesAves_FormCOM;
import br.gov.mt.indea.sistemaformin.entity.DetalhesInformacoesBovinos_FormCOM;
import br.gov.mt.indea.sistemaformin.entity.DetalhesInformacoesBubalinos_FormCOM;
import br.gov.mt.indea.sistemaformin.entity.DetalhesInformacoesCaprinos_FormCOM;
import br.gov.mt.indea.sistemaformin.entity.DetalhesInformacoesEquinos_FormCOM;
import br.gov.mt.indea.sistemaformin.entity.DetalhesInformacoesLagomorfos_FormCOM;
import br.gov.mt.indea.sistemaformin.entity.DetalhesInformacoesMuares_FormCOM;
import br.gov.mt.indea.sistemaformin.entity.DetalhesInformacoesOutrosAnimais_FormCOM;
import br.gov.mt.indea.sistemaformin.entity.DetalhesInformacoesOvinos_FormCOM;
import br.gov.mt.indea.sistemaformin.entity.DetalhesInformacoesSuinos_FormCOM;
import br.gov.mt.indea.sistemaformin.entity.FormCOM;
import br.gov.mt.indea.sistemaformin.entity.FormIN;
import br.gov.mt.indea.sistemaformin.entity.InformacoesAbelhas_FormCOM;
import br.gov.mt.indea.sistemaformin.entity.InformacoesAsininos_FormCOM;
import br.gov.mt.indea.sistemaformin.entity.InformacoesAves_FormCOM;
import br.gov.mt.indea.sistemaformin.entity.InformacoesBovinos_FormCOM;
import br.gov.mt.indea.sistemaformin.entity.InformacoesBubalinos_FormCOM;
import br.gov.mt.indea.sistemaformin.entity.InformacoesCaprinos_FormCOM;
import br.gov.mt.indea.sistemaformin.entity.InformacoesEquinos_FormCOM;
import br.gov.mt.indea.sistemaformin.entity.InformacoesLagomorfos_FormCOM;
import br.gov.mt.indea.sistemaformin.entity.InformacoesMuares_FormCOM;
import br.gov.mt.indea.sistemaformin.entity.InformacoesOutrosAnimais_FormCOM;
import br.gov.mt.indea.sistemaformin.entity.InformacoesOvinos_FormCOM;
import br.gov.mt.indea.sistemaformin.entity.InformacoesSuinos_FormCOM;
import br.gov.mt.indea.sistemaformin.entity.Medicacao;
import br.gov.mt.indea.sistemaformin.entity.OcorrenciaObservadaFormCOM;
import br.gov.mt.indea.sistemaformin.entity.ResultadoDeTesteDeDiagnostico;
import br.gov.mt.indea.sistemaformin.entity.TransitoDeAnimais;
import br.gov.mt.indea.sistemaformin.entity.VacinacaoFormIN;
import br.gov.mt.indea.sistemaformin.enums.Dominio.FaixaEtariaOuEspecie;
import br.gov.mt.indea.sistemaformin.enums.Dominio.MedidasAdotadasNoEstabelecimento;
import br.gov.mt.indea.sistemaformin.enums.Dominio.ProvavelOrigemFormCOM;
import br.gov.mt.indea.sistemaformin.enums.Dominio.SimNao;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoTransitoDeAnimais;
import br.gov.mt.indea.sistemaformin.exception.ApplicationErrorException;
import br.gov.mt.indea.sistemaformin.exception.ApplicationException;
import br.gov.mt.indea.sistemaformin.service.FormCOMService;
import br.gov.mt.indea.sistemaformin.service.FormINService;
import br.gov.mt.indea.sistemaformin.util.FacesMessageUtil;
import br.gov.mt.indea.sistemaformin.webservice.client.ClientWebServiceGTA;
import br.gov.mt.indea.sistemaformin.webservice.client.ClientWebServiceVeterinario;
import br.gov.mt.indea.sistemaformin.webservice.entity.Gta;
import br.gov.mt.indea.sistemaformin.webservice.entity.Veterinario;

@Named
@ViewScoped
@URLBeanName("formCOMManagedBean")
@URLMappings(mappings = {
		@URLMapping(id = "pesquisarFormCOM", pattern = "/formIN/#{formCOMManagedBean.idFormIN}/formCOM/pesquisar", viewId = "/pages/formCOM/lista.jsf"),
		@URLMapping(id = "incluirFormCOM", pattern = "/formIN/#{formCOMManagedBean.idFormIN}/formCOM/incluir", viewId = "/pages/formCOM/novo.jsf"),
		@URLMapping(id = "alterarFormCOM", pattern = "/formIN/#{formCOMManagedBean.idFormIN}/formCOM/alterar/#{formCOMManagedBean.id}", viewId = "/pages/formCOM/novo.jsf"),
		@URLMapping(id = "impressaoFormCOM", pattern = "/formIN/#{formCOMManagedBean.idFormIN}/formCOM/impressao/#{formCOMManagedBean.id}", viewId = "/pages/impressao.jsf")})
public class FormCOMManagedBean implements Serializable{

	private static final long serialVersionUID = -6885927564514679798L;
	
	private Long id;

	private Long idFormIN;
	
	@Inject
	private FormCOM formCOM;
	
	private FormIN formIN;
	
	@Inject
	private FormINService formINService;
	
	@Inject
	private FormCOMService formCOMService;
	
	@Inject
	private ResultadoDeTesteDeDiagnostico resultadoDeTesteDeDiagnostico;
	
	private VacinacaoFormIN vacinacao = new VacinacaoFormIN();
	
	private Medicacao medicacao = new Medicacao();
	
	private OcorrenciaObservadaFormCOM ocorrenciaObservada = new OcorrenciaObservadaFormCOM();
	
	private boolean editandoVacinacao = false;
	
	private boolean visualizandoVacinacao = false;
	
	private boolean editandoMedicacao = false;
	
	private boolean visualizandoMedicacao = false;
	
	private boolean editandoResultado = false;
	
	private boolean visualizandoResultado = false;

	private boolean editandoOcorrenciaObservada = false;
	
	private boolean visualizandoOcorrenciaObservada = false;
	
	@Past(message="Data precisa ser menor ou igual a data atual")
	private Date dataInvestigacao;
	
	@Past(message="Data precisa ser menor ou igual a data atual")
	private Date dataVacinacao;
	
	@Past(message="Data precisa ser menor ou igual a data atual")
	private Date dataInicialMedicacao;
	
	@Past(message="Data precisa ser menor ou igual a data atual")
	private Date dataFinalMedicacao;
	
	@Past(message="Data precisa ser menor ou igual a data atual")
	private Date dataRecebimento;
	
	private String cpfVeterinario;

	private String crmvVeterinario;

	private List<Veterinario> listaVeterinarios;
	
	private Integer numeroGta;

	private String serieGta;

	private Date dataInicialBuscaGta;
	
	private Date dataFinalBuscaGta;
	
	private List<TipoTransitoDeAnimais> listaTipoTransitoDeAnimais;
	
	private List<Gta> listaGtas;
	
	@Inject
	private ClientWebServiceVeterinario clientWebServiceVeterinario;
	
	@Inject
	private ClientWebServiceGTA clientWebServiceGTA;
	
	@PostConstruct
	private void init(){
		
	}
	
	private void carregarFormIN(){
		this.formIN = formINService.findByIdFetchPropriedade(this.idFormIN);
	}
	
	private void limpar() {
		this.formCOM = new FormCOM();
		this.dataInvestigacao = null;
		this.dataRecebimento = null;
	}

	@URLAction(mappingId = "incluirFormCOM", onPostback = false)
	public void novo(){
		this.limpar();
		this.carregarFormIN();
		
		this.formCOM.setFormIN(formIN);
		
		this.formCOM = formCOMService.getInformacoesDeAnimaisDoFormIN(this.formCOM);
	}
	
	public void inicializarInformacoesDeAnimais(){
		this.showFormBovinos();
		this.showFormBubalinos();
		this.showFormCaprinos();
		this.showFormOvinos();
		this.showFormSuinos();
		this.showFormEquinos();
		this.showFormAsininos();
		this.showFormMuares();
		this.showFormAves();
		this.showFormAbelhas();
		this.showFormLagomorfos();
		this.showFormOutrosAnimais();
	}
	
	@URLAction(mappingId = "alterarFormCOM", onPostback = false)
	public void editar(){
		this.formCOM = formCOMService.findByIdFetchAll(this.getId());
		
		this.dataInvestigacao = this.formCOM.getDataInvestigacao().getTime();
	}
	
	public String adicionar() throws IOException{
		
		if (this.formCOM.getVeterinario() == null){
			FacesContext context = FacesContext.getCurrentInstance();
			
			context.addMessage("cadastro:fieldVeterinario:campoVeterinario", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Veterin�rio: valor � obrigat�rio.", "Veterin�rio: valor � obrigat�rio."));
			return "";
		}
		
		if (this.dataInvestigacao != null){
			if (this.formCOM.getDataInvestigacao() == null)
				this.formCOM.setDataInvestigacao(Calendar.getInstance());
			this.formCOM.getDataInvestigacao().setTime(dataInvestigacao);
		}else
			this.formCOM.setDataInvestigacao(null);
		
		if (formCOM.getId() != null){
			this.formCOMService.saveOrUpdate(formCOM);
			FacesMessageUtil.addInfoContextFacesMessage("Form COM atualizado", "");
		}else{
			this.formCOM.setDataCadastro(Calendar.getInstance());
			Long numeroDaInvestigacaoMaisAlto = 0L;
			
			List<FormCOM> listaFormCOM = this.formCOM.getFormIN().getListaFormCOM();
			for (FormCOM formCOM : listaFormCOM) {
				if (formCOM.getNumeroInvestigacao() > numeroDaInvestigacaoMaisAlto)
					numeroDaInvestigacaoMaisAlto = formCOM.getNumeroInvestigacao();
			}
			
			this.formCOM.setNumeroInvestigacao(++numeroDaInvestigacaoMaisAlto);
			
			this.formCOMService.saveOrUpdate(formCOM);
			FacesMessageUtil.addInfoContextFacesMessage("Form COM adicionado", "");
		}
		
		limpar();
	    return "pretty:visaoGeralFormIN";
	}
	
	@Inject
	private VisualizadorImpressaoManagedBean visualizadorImpressaoManagedBean;
	
	@URLAction(mappingId = "impressaoFormCOM", onPostback = false)
	public void imprimir() throws ApplicationErrorException{
		this.formCOM = formCOMService.findByIdFetchAll(this.getId());
		
		visualizadorImpressaoManagedBean.exibirFormCOM(formCOM);
	}
	
	public void remover(FormCOM formCOM){
		this.formCOMService.delete(formCOM);
		FacesMessageUtil.addInfoContextFacesMessage("Form COM exclu�do com sucesso", "");
	}
	
	public String cancelar(){
		this.limpar();
		
		return "pretty:visaoGeralFormIN";
	}
	
	public void adicionarResultadoDeTesteDeDiagnostico(){
		boolean isResultadoDeTesteDeDiagnosticoOk = true;
		
		FacesContext context = FacesContext.getCurrentInstance();
		
		if (this.getDataRecebimento() == null || this.getDataRecebimento().equals("")){
			context.addMessage("cadastro:fieldResultadoDeTesteDeDiagnosticoDataRecebimento:campoDataRecebimento", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Data da vacina��o: valor � obrigat�rio.", "Data: valor � obrigat�rio."));
			isResultadoDeTesteDeDiagnosticoOk = false;
		}
		
		if (this.resultadoDeTesteDeDiagnostico.getIdentificacaoDoLaudo() == null || this.resultadoDeTesteDeDiagnostico.getIdentificacaoDoLaudo().equals("")){
			context.addMessage("cadastro:fieldResultadoDeTesteDeDiagnosticoIdentificacao:campoIdentificacao", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Identifica��o do laudo: valor � obrigat�rio.", "Identifica��o do laudo: valor � obrigat�rio."));
			isResultadoDeTesteDeDiagnosticoOk = false;
		}
		
		if (this.resultadoDeTesteDeDiagnostico.getLaboratorio() == null || this.resultadoDeTesteDeDiagnostico.getLaboratorio().equals("")){
			context.addMessage("cadastro:fieldResultadoDeTesteDeDiagnosticoLaboratorio:campoLaboratorio", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Doen�a: valor � obrigat�rio.", "Doen�a: valor � obrigat�rio."));
			isResultadoDeTesteDeDiagnosticoOk = false;
		}
		
		if (this.resultadoDeTesteDeDiagnostico.getTesteRealizado() == null || this.resultadoDeTesteDeDiagnostico.getTesteRealizado().equals("")){
			context.addMessage("cadastro:fieldResultadoDeTesteDeDiagnosticoTesteRealizado:campoTesteRealizado", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Teste realizado: valor � obrigat�rio.", "Teste Realizado: valor � obrigat�rio."));
			isResultadoDeTesteDeDiagnosticoOk = false;
		}
		
		if (this.resultadoDeTesteDeDiagnostico.getDoenca() == null || this.resultadoDeTesteDeDiagnostico.getDoenca().equals("")){
			context.addMessage("cadastro:fieldResultadoDeTesteDeDiagnosticoDoenca:campoDoenca", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Doen�a: valor � obrigat�rio.", "Doen�a: valor � obrigat�rio."));
			isResultadoDeTesteDeDiagnosticoOk = false;
		}
		
		if (this.resultadoDeTesteDeDiagnostico.getQuantidadePositivas() == null || this.resultadoDeTesteDeDiagnostico.getQuantidadePositivas().equals("")){
			context.addMessage("cadastro:fieldResultadoDeTesteDeDiagnosticoQuantidadePositivas:campoQuantidadePositivas", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Quantidade positivas: valor � obrigat�rio.", "Quantidade Positivas: valor � obrigat�rio."));
			isResultadoDeTesteDeDiagnosticoOk = false;
		}
		
		if (this.resultadoDeTesteDeDiagnostico.getQuantidadeNegativas() == null || this.resultadoDeTesteDeDiagnostico.getQuantidadeNegativas().equals("")){
			context.addMessage("cadastro:fieldResultadoDeTesteDeDiagnosticoQuantidadeNegativas:campoQuantidadeNegativas", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Quantidade negativas: valor � obrigat�rio.", "Quantidade Negativas: valor � obrigat�rio."));
			isResultadoDeTesteDeDiagnosticoOk = false;
		}
		
		if (this.resultadoDeTesteDeDiagnostico.getQuantidadeInconclusivas() == null || this.resultadoDeTesteDeDiagnostico.getQuantidadeInconclusivas().equals("")){
			context.addMessage("cadastro:fieldResultadoDeTesteDeDiagnosticoQuantidadeInconclusivas:campoQuantidadeInconclusivas", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Quantidade inconclusivas: valor � obrigat�rio.", "Quantidade Inconclusivas: valor � obrigat�rio."));
			isResultadoDeTesteDeDiagnosticoOk = false;
		}
		
		if (this.resultadoDeTesteDeDiagnostico.getQuantidadeInadequadas() == null || this.resultadoDeTesteDeDiagnostico.getQuantidadeInadequadas().equals("")){
			context.addMessage("cadastro:fieldResultadoDeTesteDeDiagnosticoQuantidadeInadequadas:campoQuantidadeInadequadas", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Quantidade inadequadas: valor � obrigat�rio.", "Quantidade Inadequadas: valor � obrigat�rio."));
			isResultadoDeTesteDeDiagnosticoOk = false;
		}
		
		if (!isResultadoDeTesteDeDiagnosticoOk){
			return;
		}
		
		this.resultadoDeTesteDeDiagnostico.setFormCOM(formCOM);
		
		if (this.dataRecebimento != null){
			if (this.resultadoDeTesteDeDiagnostico.getDataRecebimento() == null)
				this.resultadoDeTesteDeDiagnostico.setDataRecebimento(Calendar.getInstance());
			this.resultadoDeTesteDeDiagnostico.getDataRecebimento().setTime(dataRecebimento);
		}else
			this.resultadoDeTesteDeDiagnostico.setDataRecebimento(null);
		this.dataRecebimento = null;
		
		if (!editandoResultado)
			this.formCOM.getListaDeResultadoDeTesteDeDiagnosticos().add(this.resultadoDeTesteDeDiagnostico);
		else
			editandoResultado = false;
		
		this.resultadoDeTesteDeDiagnostico = new ResultadoDeTesteDeDiagnostico();
		
		FacesMessageUtil.addInfoContextFacesMessage("Resultado inclu�do com sucesso", "");
	}
	
	public void novoResultadoDeTesteDeDiagnostico(){
		this.resultadoDeTesteDeDiagnostico= new ResultadoDeTesteDeDiagnostico();
		this.editandoResultado = false;
		this.visualizandoResultado = false;

		this.dataRecebimento = null;
	}
	
	public void removerResultadoDeTesteDeDiagnostico(ResultadoDeTesteDeDiagnostico resultadoDeTesteDeDiagnostico){
		this.formCOM.getListaDeResultadoDeTesteDeDiagnosticos().remove(resultadoDeTesteDeDiagnostico);
	}
	
	public void editarResultadoDeTesteDeDiagnostico(ResultadoDeTesteDeDiagnostico resultadoDeTesteDeDiagnostico){
		this.editandoResultado = true;
		this.visualizandoResultado = false;
		this.resultadoDeTesteDeDiagnostico = resultadoDeTesteDeDiagnostico;
		
		if (this.resultadoDeTesteDeDiagnostico.getDataRecebimento() != null)
			this.dataRecebimento = this.resultadoDeTesteDeDiagnostico.getDataRecebimento().getTime();
	}
	
	public void visualizarResultadoDeTesteDeDiagnostico(ResultadoDeTesteDeDiagnostico resultadoDeTesteDeDiagnostico){
		this.visualizandoResultado = true;
		this.editandoResultado = false;
		this.resultadoDeTesteDeDiagnostico = resultadoDeTesteDeDiagnostico;
		
		if (this.resultadoDeTesteDeDiagnostico.getDataRecebimento() != null)
			this.dataRecebimento = this.resultadoDeTesteDeDiagnostico.getDataRecebimento().getTime();
	}
	
	public void adicionarVacinacao(){
		this.vacinacao.setFormCOM(formCOM);
		
		if (this.dataVacinacao != null){
			if (this.vacinacao.getData() == null)
				this.vacinacao.setData(Calendar.getInstance());
			this.vacinacao.getData().setTime(dataVacinacao);
		}else
			this.vacinacao.setData(null);
		this.dataVacinacao = null;
		
		if (!editandoVacinacao)
			this.formCOM.getVacinas().add(vacinacao);
		else
			editandoVacinacao = false;
		
		this.vacinacao = new VacinacaoFormIN();
		
		FacesMessageUtil.addInfoContextFacesMessage("Vacina��o inclu�da com sucesso", "");
	}
	
	public void novaVacinacao(){
		this.vacinacao= new VacinacaoFormIN();
		this.editandoVacinacao = false;
		this.visualizandoVacinacao = false;

		this.dataVacinacao = null;
	}
	
	public void removerVacinacao(VacinacaoFormIN vacinacao){
		this.formCOM.getVacinas().remove(vacinacao);
	}
	
	public void editarVacinacao(VacinacaoFormIN vacinacao){
		this.editandoVacinacao = true;
		this.visualizandoVacinacao = false;
		this.vacinacao = vacinacao;
		
		if (this.vacinacao.getData() != null)
			this.dataVacinacao = this.vacinacao.getData().getTime();
	}
	
	public void visualizarVacinacao(VacinacaoFormIN vacinacao){
		this.visualizandoVacinacao = true;
		this.editandoVacinacao = false;
		this.vacinacao= vacinacao;
		
		if (this.vacinacao.getData() != null)
			this.dataVacinacao = this.vacinacao.getData().getTime();
	}
	
	public void adicionarMedicacao(){
		this.medicacao.setFormCOM(formCOM);

		if (this.dataFinalMedicacao != null){
			if (this.medicacao.getDataFinal() == null)
				this.medicacao.setDataFinal(Calendar.getInstance());
			this.medicacao.getDataFinal().setTime(dataFinalMedicacao);
		}else
			this.medicacao.setDataFinal(null);
		this.dataFinalMedicacao = null;
		
		if (this.dataInicialMedicacao != null){
			if (this.medicacao.getDataInicial() == null)
				this.medicacao.setDataInicial(Calendar.getInstance());
			this.medicacao.getDataInicial().setTime(dataInicialMedicacao);
		}else
			this.medicacao.setDataInicial(null);
		this.dataInicialMedicacao = null;
		
		if (!editandoMedicacao)
			this.formCOM.getMedicacoes().add(medicacao);
		else
			editandoMedicacao = false;
		
		this.medicacao = new Medicacao();
		
		FacesMessageUtil.addInfoContextFacesMessage("Medica��o inclu�da com sucesso", "");
	}
	
	public void novaMedicacao(){
		this.medicacao= new Medicacao();
		this.editandoMedicacao = false;
		this.visualizandoMedicacao = false;

		this.dataFinalMedicacao = null;
		this.dataInicialMedicacao = null;
	}
	
	public void removerMedicacao(Medicacao medicacao){
		this.formCOM.getMedicacoes().remove(medicacao);
	}
	
	public void editarMedicacao(Medicacao medicacao){
		this.editandoMedicacao = true;
		this.visualizandoMedicacao = false;
		this.medicacao = medicacao;
		
		if (this.medicacao.getDataFinal() != null)
			this.dataFinalMedicacao = this.medicacao.getDataFinal().getTime();
		if (this.medicacao.getDataInicial() != null)
			this.dataInicialMedicacao = this.medicacao.getDataInicial().getTime();
	}
	
	public void visualizarMedicacao(Medicacao medicacao){
		this.visualizandoMedicacao = true;
		this.editandoMedicacao = false;
		this.medicacao = medicacao;
		
		if (this.medicacao.getDataFinal() != null)
			this.dataFinalMedicacao = this.medicacao.getDataFinal().getTime();
		if (this.medicacao.getDataInicial() != null)
			this.dataInicialMedicacao = this.medicacao.getDataInicial().getTime();
	}
	
	public void novoTransitoDeAnimais(){
		this.listaGtas = null;
		this.numeroGta = null;
		this.serieGta = null;
		this.listaTipoTransitoDeAnimais = null;
		
		Calendar dataInicial = Calendar.getInstance();
		dataInicial.set(Calendar.MONTH, dataInicial.get(Calendar.MONTH) - 2);
		this.dataInicialBuscaGta = dataInicial.getTime();
		this.dataFinalBuscaGta = Calendar.getInstance().getTime();
	}
	
	public void removerTransitoDeAnimais(TransitoDeAnimais transitoDeAnimais){
		this.formCOM.getTransitoDeAnimais().remove(transitoDeAnimais);
	}
	
	public void adicionarOcorrenciaObservada(){
		this.ocorrenciaObservada.setFormCOM(this.formCOM);
		
		if (!editandoOcorrenciaObservada)
			this.formCOM.getOcorrenciasObservadas().add(ocorrenciaObservada);
		else
			editandoOcorrenciaObservada = false;
		
		this.ocorrenciaObservada = new OcorrenciaObservadaFormCOM();
		FacesMessageUtil.addInfoContextFacesMessage("Ocorr�ncia inclu�da com sucesso", "");
	}
	
	public void novaOcorrenciaObservada(){
		this.ocorrenciaObservada= new OcorrenciaObservadaFormCOM();
		this.editandoOcorrenciaObservada = false;
		this.visualizandoOcorrenciaObservada = false;
	}
	
	public void removerOcorrenciaObservada(OcorrenciaObservadaFormCOM ocorrenciaObservada){
		this.formCOM.getOcorrenciasObservadas().remove(ocorrenciaObservada);
	}
	
	public void editarOcorrenciaObservada(OcorrenciaObservadaFormCOM ocorrenciaObservada){
		this.editandoOcorrenciaObservada = true;
		this.visualizandoOcorrenciaObservada = false;
		this.ocorrenciaObservada = ocorrenciaObservada;
	}
	
	public void visualizarOcorrenciaObservada(OcorrenciaObservadaFormCOM ocorrenciaObservada){
		this.visualizandoOcorrenciaObservada = true;
		this.editandoOcorrenciaObservada = false;
		this.ocorrenciaObservada = ocorrenciaObservada;
	}
	
	public boolean isHaDiagnosticoConclusivo(){
		boolean resultado = false;
		if (this.formCOM.getHaDiagnosticoConclusivo() == null)
			return resultado;
		else
			resultado = this.formCOM.getHaDiagnosticoConclusivo().equals(SimNao.SIM);
		
		if (!resultado)
			this.formCOM.setDiagnosticoConclusivo(null);
		
		return resultado;
	}
	
	public boolean isAplica_seMedidasAdotadas(){
		boolean resultado = false;
		if (this.formCOM.getAplica_seMedidasAdotadas() == null)
			return resultado;
		else
			resultado = this.formCOM.getAplica_seMedidasAdotadas().equals(SimNao.SIM);
		
		if (!resultado){
			this.formCOM.getMedidas().clear();
			this.formCOM.setMomentoMedidaAnimaisSentinelas(null);
			this.formCOM.setMomentoMedidaVazioSanitario(null);
		}
		
		return resultado;
	}
	
	public boolean isOutraProvavelOrigemIgual(){
		boolean resultado = false;
		if (this.formCOM.getProvavelOrigem() == null)
			return resultado;
		else
			resultado = this.formCOM.getProvavelOrigem().equals(ProvavelOrigemFormCOM.OUTRA);
		
		if (!resultado)
			this.formCOM.setOutraOrigem(null);
		
		return resultado;
	}
	
	public boolean isMedidaVazioSanitario(){
		boolean resultado = false;
		if (this.formCOM.getMedidas() == null)
			return resultado;
		else
			resultado = this.formCOM.getMedidas().contains(MedidasAdotadasNoEstabelecimento.VAZIO_SANITARIO);
		
		return resultado;
	}
	
	public boolean isMedidaIntroducaoDeSentinelas(){
		boolean resultado = false;
		if (this.formCOM.getMedidas() == null)
			return resultado;
		else
			resultado = this.formCOM.getMedidas().contains(MedidasAdotadasNoEstabelecimento.INTRODUCAO_DE_SENTINELAS);
		
		return resultado;
	}
	
	/*
	 * Segunda p�gina do cadastro
	 */
	
	public void showFormBovinos(){
		if (this.formCOM.getBovinos() == null)
			this.formCOM.setBovinos(new InformacoesBovinos_FormCOM());
		
		this.formCOM.getBovinos().setFormCOM(formCOM);
		
		List<FaixaEtariaOuEspecie> faixas = new ArrayList<FaixaEtariaOuEspecie>();
		for (DetalhesInformacoesBovinos_FormCOM detalhe : this.formCOM.getBovinos().getDetalhes()) {
			faixas.add(detalhe.getFaixaEtaria());
		}
		
		DetalhesInformacoesBovinos_FormCOM info;
		for (FaixaEtariaOuEspecie faixaEtaria : FaixaEtariaOuEspecie.getBovinos()) {
			if (!faixas.contains(faixaEtaria)){
				info = new DetalhesInformacoesBovinos_FormCOM();
				info.setInformacoesBovinos(this.formCOM.getBovinos());
				info.setFaixaEtaria(faixaEtaria);
				
				this.formCOM.getBovinos().getDetalhes().add(info);
			}
		}
	}
	
	public void showFormBubalinos(){
		if (this.formCOM.getBubalinos() == null)
			this.formCOM.setBubalinos(new InformacoesBubalinos_FormCOM());
		
		this.formCOM.getBubalinos().setFormCOM(formCOM);
		
		List<FaixaEtariaOuEspecie> faixas = new ArrayList<FaixaEtariaOuEspecie>();
		for (DetalhesInformacoesBubalinos_FormCOM detalhe : this.formCOM.getBubalinos().getDetalhes()) {
			faixas.add(detalhe.getFaixaEtaria());
		}
		
		DetalhesInformacoesBubalinos_FormCOM info;
		for (FaixaEtariaOuEspecie faixaEtaria : FaixaEtariaOuEspecie.getBubalinos()) {
			if (!faixas.contains(faixaEtaria)){
				info = new DetalhesInformacoesBubalinos_FormCOM();
				info.setInformacoesBubalinos(this.formCOM.getBubalinos());
				info.setFaixaEtaria(faixaEtaria);
				
				this.formCOM.getBubalinos().getDetalhes().add(info);
			}
		}
	}
	
	public void showFormCaprinos(){
		if (this.formCOM.getCaprinos() == null)
			this.formCOM.setCaprinos(new InformacoesCaprinos_FormCOM());
		
		this.formCOM.getCaprinos().setFormCOM(formCOM);
		
		List<FaixaEtariaOuEspecie> faixas = new ArrayList<FaixaEtariaOuEspecie>();
		for (DetalhesInformacoesCaprinos_FormCOM detalhe : this.formCOM.getCaprinos().getDetalhes()) {
			faixas.add(detalhe.getFaixaEtaria());
		}
		
		DetalhesInformacoesCaprinos_FormCOM info;
		for (FaixaEtariaOuEspecie faixaEtaria : FaixaEtariaOuEspecie.getCaprinos()) {
			if (!faixas.contains(faixaEtaria)){
				info = new DetalhesInformacoesCaprinos_FormCOM();
				info.setInformacoesCaprinos(this.formCOM.getCaprinos());
				info.setFaixaEtaria(faixaEtaria);
				
				this.formCOM.getCaprinos().getDetalhes().add(info);
			}
		}
	}
	
	public void showFormOvinos(){
		if (this.formCOM.getOvinos() == null)
			this.formCOM.setOvinos(new InformacoesOvinos_FormCOM());
		
		this.formCOM.getOvinos().setFormCOM(formCOM);
		
		List<FaixaEtariaOuEspecie> faixas = new ArrayList<FaixaEtariaOuEspecie>();
		for (DetalhesInformacoesOvinos_FormCOM detalhe : this.formCOM.getOvinos().getDetalhes()) {
			faixas.add(detalhe.getFaixaEtaria());
		}
		
		DetalhesInformacoesOvinos_FormCOM info;
		for (FaixaEtariaOuEspecie faixaEtaria : FaixaEtariaOuEspecie.getOvinos()) {
			if (!faixas.contains(faixaEtaria)){
				info = new DetalhesInformacoesOvinos_FormCOM();
				info.setInformacoesOvinos(this.formCOM.getOvinos());
				info.setFaixaEtaria(faixaEtaria);
				
				this.formCOM.getOvinos().getDetalhes().add(info);
			}
		}
	}
	
	public void showFormSuinos(){
		if (this.formCOM.getSuinos() == null)
			this.formCOM.setSuinos(new InformacoesSuinos_FormCOM());
		
		this.formCOM.getSuinos().setFormCOM(formCOM);
		
		List<FaixaEtariaOuEspecie> faixas = new ArrayList<FaixaEtariaOuEspecie>();
		for (DetalhesInformacoesSuinos_FormCOM detalhe : this.formCOM.getSuinos().getDetalhes()) {
			faixas.add(detalhe.getFaixaEtaria());
		}
		
		DetalhesInformacoesSuinos_FormCOM info;
		for (FaixaEtariaOuEspecie faixaEtaria : FaixaEtariaOuEspecie.getSuinos()) {
			if (!faixas.contains(faixaEtaria)){
				info = new DetalhesInformacoesSuinos_FormCOM();
				info.setInformacoesSuinos(this.formCOM.getSuinos());
				info.setFaixaEtaria(faixaEtaria);
				
				this.formCOM.getSuinos().getDetalhes().add(info);
			}
		}
	}
	
	public void showFormEquinos(){
		if (this.formCOM.getEquinos() == null)
			this.formCOM.setEquinos(new InformacoesEquinos_FormCOM());
		
		this.formCOM.getEquinos().setFormCOM(formCOM);
		
		List<FaixaEtariaOuEspecie> faixas = new ArrayList<FaixaEtariaOuEspecie>();
		for (DetalhesInformacoesEquinos_FormCOM detalhe : this.formCOM.getEquinos().getDetalhes()) {
			faixas.add(detalhe.getFaixaEtaria());
		}
		
		DetalhesInformacoesEquinos_FormCOM info;
		for (FaixaEtariaOuEspecie faixaEtaria : FaixaEtariaOuEspecie.getEquinos()) {
			if (!faixas.contains(faixaEtaria)){
				info = new DetalhesInformacoesEquinos_FormCOM();
				info.setInformacoesEquinos(this.formCOM.getEquinos());
				info.setFaixaEtaria(faixaEtaria);
				
				this.formCOM.getEquinos().getDetalhes().add(info);
			}
		}
	}
	
	public void showFormAsininos(){
		if (this.formCOM.getAsininos() == null)
			this.formCOM.setAsininos(new InformacoesAsininos_FormCOM());
		
		this.formCOM.getAsininos().setFormCOM(formCOM);
		
		List<FaixaEtariaOuEspecie> faixas = new ArrayList<FaixaEtariaOuEspecie>();
		for (DetalhesInformacoesAsininos_FormCOM detalhe : this.formCOM.getAsininos().getDetalhes()) {
			faixas.add(detalhe.getFaixaEtaria());
		}
		
		DetalhesInformacoesAsininos_FormCOM info;
		for (FaixaEtariaOuEspecie faixaEtaria : FaixaEtariaOuEspecie.getAsininos()) {
			if (!faixas.contains(faixaEtaria)){
				info = new DetalhesInformacoesAsininos_FormCOM();
				info.setInformacoesAsininos(this.formCOM.getAsininos());
				info.setFaixaEtaria(faixaEtaria);
				
				this.formCOM.getAsininos().getDetalhes().add(info);
			}
		}
	}
	
	public void showFormMuares(){
		if (this.formCOM.getMuares() == null)
			this.formCOM.setMuares(new InformacoesMuares_FormCOM());
		
		this.formCOM.getMuares().setFormCOM(formCOM);
		
		List<FaixaEtariaOuEspecie> faixas = new ArrayList<FaixaEtariaOuEspecie>();
		for (DetalhesInformacoesMuares_FormCOM detalhe : this.formCOM.getMuares().getDetalhes()) {
			faixas.add(detalhe.getFaixaEtaria());
		}
		
		DetalhesInformacoesMuares_FormCOM info;
		for (FaixaEtariaOuEspecie faixaEtaria : FaixaEtariaOuEspecie.getMuares()) {
			if (!faixas.contains(faixaEtaria)){
				info = new DetalhesInformacoesMuares_FormCOM();
				info.setInformacoesMuares(this.formCOM.getMuares());
				info.setFaixaEtaria(faixaEtaria);
				
				this.formCOM.getMuares().getDetalhes().add(info);
			}
		}
	}
	
	public void showFormAves(){
		if (this.formCOM.getAves() == null)
			this.formCOM.setAves(new InformacoesAves_FormCOM());
		
		this.formCOM.getAves().setFormCOM(formCOM);
		
		List<FaixaEtariaOuEspecie> faixas = new ArrayList<FaixaEtariaOuEspecie>();
		for (DetalhesInformacoesAves_FormCOM detalhe : this.formCOM.getAves().getDetalhes()) {
			faixas.add(detalhe.getFaixaEtaria());
		}
		
		DetalhesInformacoesAves_FormCOM info;
		for (FaixaEtariaOuEspecie faixaEtaria : FaixaEtariaOuEspecie.getAves()) {
			if (!faixas.contains(faixaEtaria)){
				info = new DetalhesInformacoesAves_FormCOM();
				info.setInformacoesAves(this.formCOM.getAves());
				info.setFaixaEtaria(faixaEtaria);
				
				this.formCOM.getAves().getDetalhes().add(info);
			}
		}
	}
	
	public void showFormAbelhas(){
		if (this.formCOM.getAbelhas() == null)
			this.formCOM.setAbelhas(new InformacoesAbelhas_FormCOM());
		
		this.formCOM.getAbelhas().setFormCOM(formCOM);
		
		List<FaixaEtariaOuEspecie> faixas = new ArrayList<FaixaEtariaOuEspecie>();
		for (DetalhesInformacoesAbelhas_FormCOM detalhe : this.formCOM.getAbelhas().getDetalhes()) {
			faixas.add(detalhe.getFaixaEtaria());
		}
		
		DetalhesInformacoesAbelhas_FormCOM info;
		for (FaixaEtariaOuEspecie faixaEtaria : FaixaEtariaOuEspecie.getAbelhas()) {
			if (!faixas.contains(faixaEtaria)){
				info = new DetalhesInformacoesAbelhas_FormCOM();
				info.setInformacoesAbelhas(this.formCOM.getAbelhas());
				info.setFaixaEtaria(faixaEtaria);
				
				this.formCOM.getAbelhas().getDetalhes().add(info);
			}
		}
	}
	
	public void showFormLagomorfos(){
		if (this.formCOM.getLagomorfos() == null)
			this.formCOM.setLagomorfos(new InformacoesLagomorfos_FormCOM());
		
		this.formCOM.getLagomorfos().setFormCOM(formCOM);
		
		List<FaixaEtariaOuEspecie> faixas = new ArrayList<FaixaEtariaOuEspecie>();
		for (DetalhesInformacoesLagomorfos_FormCOM detalhe : this.formCOM.getLagomorfos().getDetalhes()) {
			faixas.add(detalhe.getFaixaEtaria());
		}
		
		DetalhesInformacoesLagomorfos_FormCOM info;
		for (FaixaEtariaOuEspecie faixaEtaria : FaixaEtariaOuEspecie.getLagomorfos()) {
			if (!faixas.contains(faixaEtaria)){
				info = new DetalhesInformacoesLagomorfos_FormCOM();
				info.setInformacoesLagomorfos(this.formCOM.getLagomorfos());
				info.setFaixaEtaria(faixaEtaria);
				
				this.formCOM.getLagomorfos().getDetalhes().add(info);
			}
		}
	}
	
	public void showFormOutrosAnimais(){
		if (this.formCOM.getOutrosAnimais() == null)
			this.formCOM.setOutrosAnimais(new InformacoesOutrosAnimais_FormCOM());
		
		this.formCOM.getOutrosAnimais().setFormCOM(formCOM);
		
		List<FaixaEtariaOuEspecie> faixas = new ArrayList<FaixaEtariaOuEspecie>();
		for (DetalhesInformacoesOutrosAnimais_FormCOM detalhe : this.formCOM.getOutrosAnimais().getDetalhes()) {
			faixas.add(detalhe.getFaixaEtaria());
		}
		
		DetalhesInformacoesOutrosAnimais_FormCOM info;
		for (FaixaEtariaOuEspecie faixaEtaria : FaixaEtariaOuEspecie.getOutrosAnimais()) {
			if (!faixas.contains(faixaEtaria)){
				info = new DetalhesInformacoesOutrosAnimais_FormCOM();
				info.setInformacoesOutrosAnimais(this.formCOM.getOutrosAnimais());
				info.setFaixaEtaria(faixaEtaria);
				
				this.formCOM.getOutrosAnimais().getDetalhes().add(info);
			}
		}
	}
	
	public void abrirTelaDeBuscaDeVeterinario(){
		this.cpfVeterinario = null;
		this.crmvVeterinario = null;
		this.listaVeterinarios = null;
	}
	
	public void buscarVeterinarioPorCpf(){
		this.listaVeterinarios = null;
		
		Veterinario v;
		try {
			v = clientWebServiceVeterinario.getVeterinarioByCpf(cpfVeterinario);
			
			if (v != null){
				this.listaVeterinarios = new ArrayList<Veterinario>();
				this.listaVeterinarios.add(v);
			}
		} catch (ApplicationException e) {
			FacesMessageUtil.addErrorContextFacesMessage("Erro", e.getLocalizedMessage());
			return;
		}
		
		if (this.listaVeterinarios == null || this.listaVeterinarios.isEmpty())
			FacesMessageUtil.addWarnContextFacesMessage("Nenhum veterin�rio foi encontrado", "");
	}
	
	public void buscarVeterinarioPorCrmv(){
		this.listaVeterinarios = null;
		
		Veterinario v;
		try {
			v = clientWebServiceVeterinario.getVeterinarioByCRMV(crmvVeterinario);
			
			if (v != null){
				this.listaVeterinarios = new ArrayList<Veterinario>();
				this.listaVeterinarios.add(v);
			}
		} catch (ApplicationException e) {
			FacesMessageUtil.addErrorContextFacesMessage("Erro", e.getLocalizedMessage());
			return;
		}
		
		if (this.listaVeterinarios == null || this.listaVeterinarios.isEmpty())
			FacesMessageUtil.addWarnContextFacesMessage("Nenhum veterin�rio foi encontrado", "");
	}
	
	public void selecionarVeterinario(Veterinario veterinario){
		this.formCOM.setVeterinario(veterinario);
		FacesMessageUtil.addInfoContextFacesMessage("Veterinario " + this.formCOM.getVeterinario().getNome() + " selecionado", "");
	}
	
	public void abrirTelaDeBuscaDeGta(){
		this.numeroGta = null;
		this.serieGta = null;
		this.listaGtas = null;
	}
	
	public void buscarGtaPorNumeroESerie(){
		this.listaGtas = null;
		
		try {
			Gta gta = clientWebServiceGTA.getGTAByNumeroSerie(numeroGta, serieGta);
			this.listaGtas = new ArrayList<Gta>();
			if (gta != null)
				this.listaGtas.add(gta);
		} catch (ApplicationException e) {
			FacesMessageUtil.addErrorContextFacesMessage("Erro", e.getLocalizedMessage());
			return;
		}
		
		if (this.listaGtas == null || this.listaGtas.isEmpty())
			FacesMessageUtil.addWarnContextFacesMessage("Nenhum gta foi encontrado", "");
	}
	
	public void buscarGtaPorData(){
		this.listaGtas = null;
		
		try {
			if (this.listaTipoTransitoDeAnimais.contains(TipoTransitoDeAnimais.EGRESSO))
				this.listaGtas.addAll(clientWebServiceGTA.getGTAByPropriedadeDeOrigemEPeriodoDeEntrada(this.formCOM.getFormIN().getPropriedade().getCodigo(), dataInicialBuscaGta, dataFinalBuscaGta));
			
			if (this.listaTipoTransitoDeAnimais.contains(TipoTransitoDeAnimais.INGRESSO))
				this.listaGtas.addAll(clientWebServiceGTA.getGTAByPropriedadeDeDestinoEPeriodoDeEntrada(this.formCOM.getFormIN().getPropriedade().getCodigo(), dataInicialBuscaGta, dataFinalBuscaGta));
		} catch (ApplicationException e) {
			FacesMessageUtil.addErrorContextFacesMessage("Erro", e.getLocalizedMessage());
			return;
		}
		
		if (this.listaGtas == null || this.listaGtas.isEmpty())
			FacesMessageUtil.addWarnContextFacesMessage("Nenhum gta foi encontrado", "");
	}
	
	public void selecionarGtas(){
		TransitoDeAnimais transitoDeAnimais = null;
		
		if (listaGtas != null && !listaGtas.isEmpty()){
			for (Gta gta : listaGtas) {
				if (gta.isSelecionadoParaInclusao()){
					transitoDeAnimais = new TransitoDeAnimais(gta);
					transitoDeAnimais.setFormCOM(formCOM);
					
					if (!this.formCOM.getTransitoDeAnimais().contains(transitoDeAnimais))
						this.formCOM.getTransitoDeAnimais().add(transitoDeAnimais);
				}
			}
		}
	}
	
	public boolean isListaFormSVEmpty(){
		if (this.formCOM.getListaFormSV() != null && !this.formCOM.getListaFormSV().isEmpty())
			return false;
		else
			return true;
	}
	
	public boolean isListaFormSHEmpty(){
		if (this.formCOM.getListaFormSH() != null && !this.formCOM.getListaFormSH().isEmpty())
			return false;
		else
			return true;
	}
	
	public boolean isListaFormSRNEmpty(){
		if (this.formCOM.getListaFormSRN() != null && !this.formCOM.getListaFormSRN().isEmpty())
			return false;
		else
			return true;
	}
	
	public boolean isListaFormLABEmpty(){
		if (this.formCOM.getListaFormLAB() != null && !this.formCOM.getListaFormLAB().isEmpty())
			return false;
		else
			return true;
	}
	
	public boolean isListaFormEQEmpty(){
		if (this.formCOM.getListaFormEQ() != null && !this.formCOM.getListaFormEQ().isEmpty())
			return false;
		else
			return true;
	}
	
	public boolean isListaFolhasAdicionaisEmpty(){
		if (this.formCOM.getListaFolhasAdicionais() != null && !this.formCOM.getListaFolhasAdicionais().isEmpty())
			return false;
		else
			return true;
	}
	
	public boolean isListaFormSNEmpty(){
		if (this.formCOM.getListaFormSN() != null && !this.formCOM.getListaFormSN().isEmpty())
			return false;
		else
			return true;
	}
	
	public boolean isListaFormAIEEmpty(){
		if (this.formCOM.getListaFormAIE() != null && !this.formCOM.getListaFormAIE().isEmpty())
			return false;
		else
			return true;
	}
	
	public boolean isListaFormMormoEmpty(){
		if (this.formCOM.getListaFormMormo() != null && !this.formCOM.getListaFormMormo().isEmpty())
			return false;
		else
			return true;
	}
	
	public boolean isListaFormMaleinaEmpty(){
		if (this.formCOM.getListaFormMaleina() != null && !this.formCOM.getListaFormMaleina().isEmpty())
			return false;
		else
			return true;
	}
	
	public FormCOM getFormCOM() {
		return formCOM;
	}

	public void setFormCOM(FormCOM formCOM) {
		this.formCOM = formCOM;
	}

	public ResultadoDeTesteDeDiagnostico getResultadoDeTesteDeDiagnostico() {
		return resultadoDeTesteDeDiagnostico;
	}

	public void setResultadoDeTesteDeDiagnostico(ResultadoDeTesteDeDiagnostico resultadoDeTesteDeDiagnostico) {
		this.resultadoDeTesteDeDiagnostico = resultadoDeTesteDeDiagnostico;
	}

	public VacinacaoFormIN getVacinacao() {
		return vacinacao;
	}

	public void setVacinacao(VacinacaoFormIN vacinacao) {
		this.vacinacao = vacinacao;
	}

	public Medicacao getMedicacao() {
		return medicacao;
	}

	public void setMedicacao(Medicacao medicacao) {
		this.medicacao = medicacao;
	}

	public OcorrenciaObservadaFormCOM getOcorrenciaObservada() {
		return ocorrenciaObservada;
	}

	public void setOcorrenciaObservada(
			OcorrenciaObservadaFormCOM ocorrenciaObservada) {
		this.ocorrenciaObservada = ocorrenciaObservada;
	}

	public boolean isVisualizandoVacinacao() {
		return visualizandoVacinacao;
	}

	public boolean isVisualizandoMedicacao() {
		return visualizandoMedicacao;
	}

	public boolean isVisualizandoResultado() {
		return visualizandoResultado;
	}

	public boolean isVisualizandoOcorrenciaObservada() {
		return visualizandoOcorrenciaObservada;
	}

	public Date getDataInvestigacao() {
		return dataInvestigacao;
	}

	public void setDataInvestigacao(Date dataInvestigacao) {
		this.dataInvestigacao = dataInvestigacao;
	}

	public Date getDataVacinacao() {
		return dataVacinacao;
	}

	public void setDataVacinacao(Date dataVacinacao) {
		this.dataVacinacao = dataVacinacao;
	}

	public Date getDataInicialMedicacao() {
		return dataInicialMedicacao;
	}

	public void setDataInicialMedicacao(Date dataInicialMedicacao) {
		this.dataInicialMedicacao = dataInicialMedicacao;
	}

	public Date getDataFinalMedicacao() {
		return dataFinalMedicacao;
	}

	public void setDataFinalMedicacao(Date dataFinalMedicacao) {
		this.dataFinalMedicacao = dataFinalMedicacao;
	}

	public Date getDataRecebimento() {
		return dataRecebimento;
	}

	public void setDataRecebimento(Date dataRecebimento) {
		this.dataRecebimento = dataRecebimento;
	}

	public String getCpfVeterinario() {
		return cpfVeterinario;
	}

	public void setCpfVeterinario(String cpfVeterinario) {
		this.cpfVeterinario = cpfVeterinario;
	}

	public String getCrmvVeterinario() {
		return crmvVeterinario;
	}

	public void setCrmvVeterinario(String crmvVeterinario) {
		this.crmvVeterinario = crmvVeterinario;
	}

	public List<Veterinario> getListaVeterinarios() {
		return listaVeterinarios;
	}

	public void setListaVeterinarios(List<Veterinario> listaVeterinarios) {
		this.listaVeterinarios = listaVeterinarios;
	}

	public Integer getNumeroGta() {
		return numeroGta;
	}

	public void setNumeroGta(Integer numeroGta) {
		this.numeroGta = numeroGta;
	}

	public String getSerieGta() {
		return serieGta;
	}

	public void setSerieGta(String serieGta) {
		this.serieGta = serieGta;
	}

	public List<Gta> getListaGtas() {
		return listaGtas;
	}

	public void setListaGtas(List<Gta> listaGtas) {
		this.listaGtas = listaGtas;
	}

	public Date getDataInicialBuscaGta() {
		return dataInicialBuscaGta;
	}

	public void setDataInicialBuscaGta(Date dataInicialBuscaGta) {
		this.dataInicialBuscaGta = dataInicialBuscaGta;
	}

	public Date getDataFinalBuscaGta() {
		return dataFinalBuscaGta;
	}

	public void setDataFinalBuscaGta(Date dataFinalBuscaGta) {
		this.dataFinalBuscaGta = dataFinalBuscaGta;
	}

	public List<TipoTransitoDeAnimais> getListaTipoTransitoDeAnimais() {
		return listaTipoTransitoDeAnimais;
	}

	public void setListaTipoTransitoDeAnimais(
			List<TipoTransitoDeAnimais> listaTipoTransitoDeAnimais) {
		this.listaTipoTransitoDeAnimais = listaTipoTransitoDeAnimais;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public FormIN getFormIN() {
		return formIN;
	}

	public void setFormIN(FormIN formIN) {
		this.formIN = formIN;
	}
	
	public Long getIdFormIN() {
		return idFormIN;
	}

	public void setIdFormIN(Long idFormIN) {
		this.idFormIN = idFormIN;
	}
	
	/*
	 * 
	 */
	
	public String onFlowProcess(FlowEvent event) {
		String nextStep = event.getNewStep();
			
		if (nextStep.equals("dois")){
			this.inicializarInformacoesDeAnimais();
		} 
		
		return nextStep;
    }
	
}
