package br.gov.mt.indea.sistemaformin.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.inject.Inject;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import br.gov.mt.indea.sistemaformin.entity.FormCOM;
import br.gov.mt.indea.sistemaformin.entity.FormIN;
import br.gov.mt.indea.sistemaformin.exception.ApplicationException;

@Stateless
public class FormCOMDAO extends DAO<FormCOM> {
	
	@Inject
	private EntityManager em;
	
	public FormCOMDAO() {
		super(FormCOM.class);
	}

	@Override
	public EntityManager getEntityManager() {
		return this.em;
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void add(FormCOM formCOM) throws ApplicationException{
		formCOM = this.getEntityManager().merge(formCOM);
		super.add(formCOM);
	}
	
	public List<FormCOM> getFormCOMByFormIN(FormIN formIN) throws ApplicationException{
		try{
			CriteriaBuilder cb = this.getEntityManager().getCriteriaBuilder();
			CriteriaQuery<FormCOM> query = cb.createQuery(FormCOM.class);
			
			Root<FormCOM> t = query.from(FormCOM.class);
			
			query.where(cb.equal(t.get("formIN"), formIN.getId()));
			
			return this.getEntityManager().createQuery(query).getResultList();
		}catch(Exception e){
			throw new ApplicationException("N�o foi poss�vel buscar o registro", e);
		}
	}

}
