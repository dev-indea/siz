package br.gov.mt.indea.sistemaformin.dao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import br.gov.mt.indea.sistemaformin.entity.Municipio;
import br.gov.mt.indea.sistemaformin.entity.VisitaPropriedadeRural;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoEstabelecimento;
import br.gov.mt.indea.sistemaformin.exception.ApplicationException;
import br.gov.mt.indea.sistemaformin.util.StringUtil;

@Stateless
public class VisitaPropriedadeRuralDAO extends DAO<VisitaPropriedadeRural> {
	
	@Inject
	private EntityManager em;
	
	public VisitaPropriedadeRuralDAO() {
		super(VisitaPropriedadeRural.class);
	}

	@Override
	public EntityManager getEntityManager() {
		return this.em;
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void add(VisitaPropriedadeRural visitaPropriedadeRural) throws ApplicationException{
		visitaPropriedadeRural = this.getEntityManager().merge(visitaPropriedadeRural);
		super.add(visitaPropriedadeRural);
	}
	
	public VisitaPropriedadeRural findByNumero(String numero) throws ApplicationException{
		try{
			CriteriaBuilder cb = this.getEntityManager().getCriteriaBuilder();
			CriteriaQuery<VisitaPropriedadeRural> query = cb.createQuery(VisitaPropriedadeRural.class);
			
			Root<VisitaPropriedadeRural> t = query.from(VisitaPropriedadeRural.class);
			
			query.where(cb.equal(t.get("numero"), numero));
			
			return this.getEntityManager().createQuery(query).getSingleResult();
		}catch(Exception e){
			throw new ApplicationException("N�o foi poss�vel buscar o registro", e);
		}
	}
	
	public List<VisitaPropriedadeRural> findBy(Municipio municipio, String numero, String estabelecimento, TipoEstabelecimento tipoEstabelecimento, Date data) throws ApplicationException{
		try	{
			if (tipoEstabelecimento != null){
				CriteriaBuilder cb = this.getEntityManager().getCriteriaBuilder();
				CriteriaQuery<VisitaPropriedadeRural> query = cb.createQuery(VisitaPropriedadeRural.class);
				
				Root<VisitaPropriedadeRural> t = query.from(VisitaPropriedadeRural.class);
				
				List<Predicate> predicados = new ArrayList<Predicate>();
				
				// criar 2 consultas caso 'tipoEstabelecimento' n�o esteja selecionado
				if (numero != null && !numero.equals(""))
					predicados.add(cb.equal(t.get("numero"), numero));
				
				if (tipoEstabelecimento.equals(TipoEstabelecimento.PROPRIEDADE)){
					predicados.add(cb.isNotNull(t.get("propriedade")));
					
					if (estabelecimento != null && !estabelecimento.equals("")){
						predicados.add(cb.like(
							cb.function("remove_acento", String.class, cb.lower(t.get("propriedade").<String>get("nome"))), 
							"%" + StringUtil.removeAcentos(estabelecimento.toLowerCase() + "%")));
					}
					
					if (municipio != null)
						predicados.add(cb.equal(t.get("propriedade").get("municipio"), municipio));
				} else{
					predicados.add(cb.isNotNull(t.get("recinto")));
					
					if (estabelecimento != null && !estabelecimento.equals("")){
						predicados.add(cb.like(
							cb.function("remove_acento", String.class, cb.lower(t.get("recinto").<String>get("nome"))), 
							"%" + StringUtil.removeAcentos(estabelecimento.toLowerCase() + "%")));
					}
					
					if (municipio != null)
						predicados.add(cb.equal(t.get("recinto").get("municipio"), municipio));
				}
				
				if (data != null){
					if (data != null){
						Calendar i = Calendar.getInstance();
						
						i.setTime(data);
						i.set(Calendar.HOUR_OF_DAY, 0);
						i.set(Calendar.MINUTE, 0);
						i.set(Calendar.SECOND, 0);
						
						Calendar f = Calendar.getInstance();
						
						f.setTime(data);
						f.set(Calendar.HOUR_OF_DAY, 23);
						f.set(Calendar.MINUTE, 59);
						f.set(Calendar.SECOND, 59);
						
						Path<Date> dataDaVisitaPath = t.get("dataDaVisita");
						predicados.add(cb.between(dataDaVisitaPath, i.getTime(), f.getTime()));
					}
				}
				
				query.where(cb.and(predicados.toArray(new Predicate[]{})));
				query.orderBy(cb.desc(t.get("dataDaVisita")));
				
				return this.getEntityManager().createQuery(query).getResultList();
			}else{
				
				//propriedade
				CriteriaBuilder cbPropriedade = this.getEntityManager().getCriteriaBuilder();
				CriteriaQuery<VisitaPropriedadeRural> queryPropriedade = cbPropriedade.createQuery(VisitaPropriedadeRural.class);
				
				Root<VisitaPropriedadeRural> tPropriedade = queryPropriedade.from(VisitaPropriedadeRural.class);
				
				List<Predicate> predicadosPropriedade = new ArrayList<Predicate>();
				
				if (numero != null && !numero.equals(""))
					predicadosPropriedade.add(cbPropriedade.equal(tPropriedade.get("numero"), numero));
				
				predicadosPropriedade.add(cbPropriedade.isNotNull(tPropriedade.get("propriedade")));
					
				if (estabelecimento != null && !estabelecimento.equals("")){
					predicadosPropriedade.add(cbPropriedade.like(
						cbPropriedade.function("remove_acento", String.class, cbPropriedade.lower(tPropriedade.get("propriedade").<String>get("nome"))), 
						"%" + StringUtil.removeAcentos(estabelecimento.toLowerCase() + "%")));
				}
				
				if (municipio != null)
					predicadosPropriedade.add(cbPropriedade.equal(tPropriedade.get("propriedade").get("municipio"), municipio));
			
				if (data != null){
					if (data != null){
						Calendar i = Calendar.getInstance();
						
						i.setTime(data);
						i.set(Calendar.HOUR_OF_DAY, 0);
						i.set(Calendar.MINUTE, 0);
						i.set(Calendar.SECOND, 0);
						
						Calendar f = Calendar.getInstance();
						
						f.setTime(data);
						f.set(Calendar.HOUR_OF_DAY, 23);
						f.set(Calendar.MINUTE, 59);
						f.set(Calendar.SECOND, 59);
						
						Path<Date> dataDaVisitaPath = tPropriedade.get("dataDaVisita");
						predicadosPropriedade.add(cbPropriedade.between(dataDaVisitaPath, i.getTime(), f.getTime()));
					}
				}
				
				queryPropriedade.where(cbPropriedade.and(predicadosPropriedade.toArray(new Predicate[]{})));
				
				List<VisitaPropriedadeRural> resultListPropriedade = this.getEntityManager().createQuery(queryPropriedade).getResultList();
				
				//recinto
				CriteriaBuilder cbRecinto = this.getEntityManager().getCriteriaBuilder();
				CriteriaQuery<VisitaPropriedadeRural> queryRecinto = cbRecinto.createQuery(VisitaPropriedadeRural.class);
				
				Root<VisitaPropriedadeRural> tRecinto = queryRecinto.from(VisitaPropriedadeRural.class);
				
				List<Predicate> predicadosRecinto = new ArrayList<Predicate>();
				
				if (numero != null && !numero.equals(""))
					predicadosRecinto.add(cbRecinto.equal(tRecinto.get("numero"), numero));
				
				predicadosRecinto.add(cbRecinto.isNotNull(tRecinto.get("recinto")));
					
				if (estabelecimento != null && !estabelecimento.equals("")){
					predicadosRecinto.add(cbRecinto.like(
						cbRecinto.function("remove_acento", String.class, cbRecinto.lower(tRecinto.get("recinto").<String>get("nome"))), 
						"%" + StringUtil.removeAcentos(estabelecimento.toLowerCase() + "%")));
				}
				
				if (municipio != null)
					predicadosRecinto.add(cbRecinto.equal(tRecinto.get("recinto").get("municipio"), municipio));
			
				

				if (data != null){
					if (data != null){
						Calendar i = Calendar.getInstance();
						
						i.setTime(data);
						i.set(Calendar.HOUR_OF_DAY, 0);
						i.set(Calendar.MINUTE, 0);
						i.set(Calendar.SECOND, 0);
						
						Calendar f = Calendar.getInstance();
						
						f.setTime(data);
						f.set(Calendar.HOUR_OF_DAY, 23);
						f.set(Calendar.MINUTE, 59);
						f.set(Calendar.SECOND, 59);
						
						Path<Date> dataDaVisitaPath = tRecinto.get("dataDaVisita");
						predicadosRecinto.add(cbRecinto.between(dataDaVisitaPath, i.getTime(), f.getTime()));
					}
				}
				
				queryRecinto.where(cbRecinto.and(predicadosRecinto.toArray(new Predicate[]{})));
				
				List<VisitaPropriedadeRural> resultListRecinto = this.getEntityManager().createQuery(queryRecinto).getResultList();
				
				List<VisitaPropriedadeRural> resultFinalList = new ArrayList<VisitaPropriedadeRural>();
				
				resultFinalList.addAll(resultListPropriedade);
				resultFinalList.addAll(resultListRecinto);
				
				Collections.sort(resultFinalList);
				
				return resultFinalList;
				
			}
		}catch(Exception e){
			throw new ApplicationException("N�o foi poss�vel listar os registros", e);
		}
		
	}

}
