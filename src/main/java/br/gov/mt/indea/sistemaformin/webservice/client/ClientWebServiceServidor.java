package br.gov.mt.indea.sistemaformin.webservice.client;

import java.io.Serializable;
import java.net.URISyntaxException;

import javax.inject.Inject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import br.gov.mt.indea.sistemaformin.exception.ApplicationException;
import br.gov.mt.indea.sistemaformin.webservice.convert.ServidorConverter;
import br.gov.mt.indea.sistemaformin.webservice.model.Servidor;


public class ClientWebServiceServidor implements Serializable{
	
	private static final long serialVersionUID = -5887697984204696148L;

	protected String URL_WS = "https://sistemas.indea.mt.gov.br/FronteiraWeb/ws/servidor/";
	
//	@Inject
//	@Category("WEBSERVICE::SERVIDOR")
//	private CustomLogger log;
	
	@Inject
	private ServidorConverter servidorConverter;
	
	public br.gov.mt.indea.sistemaformin.webservice.entity.Servidor getServidorById(String id) throws ApplicationException {

		String[] resposta = null;
		try {
			System.out.println(URL_WS + "id/" + id);
			resposta = new WebServiceCliente().get(URL_WS + "id/" + id);
		} catch (URISyntaxException e) {
			//log.excecao(e.getMessage(), e);
			throw new ApplicationException("Erro ao buscar o servidor");
		}
		
		if (resposta == null || resposta[0] == null){
			return null;
		}
		
		if (resposta[0].equals("200")) {
			GsonBuilder gsonBuilder = new GsonBuilder().setDateFormat("dd/MM/yyyy");
			Gson gson = gsonBuilder.create();
			Servidor servidor = gson.fromJson(resposta[1], Servidor.class);
			
			return servidorConverter.fromModelToEntity(servidor);
		} else if (resposta[0].equals("404")) {
			//log.excecao(resposta[0] + ". " + resposta[1], null);
			throw new ApplicationException("Webservice com problemas. Contate o administrador do sistema");
		} else if (resposta[0].equals("500")) {
			//log.excecao(resposta[0] + ". " + resposta[1], null);
			throw new ApplicationException("Webservice com problemas. Contate o administrador do sistema");
		} else if (resposta[0].equals("400")) {
			//log.excecao(resposta[0] + ". " + resposta[1], null);
			throw new ApplicationException("Webservice com problemas. Contate o administrador do sistema");
		} else{
			//log.excecao(resposta[0] + ". " + resposta[1], null);
			throw new ApplicationException("Webservice inativo. Tente novamente em alguns minutos. Erro: " + resposta[0]);
		} 
	}
	
	public br.gov.mt.indea.sistemaformin.webservice.entity.Servidor getServidorByCpf(String cpf) throws ApplicationException {
		cpf = cpf.replace(".", "").replace("-", "");
		String[] resposta = null;
		try {
			System.out.println(URL_WS + "cpf/" + cpf);
			resposta = new WebServiceCliente().get(URL_WS + "cpf/" + cpf);
		} catch (URISyntaxException e) {
			//log.excecao(e.getMessage(), e);
			throw new ApplicationException("Erro ao buscar o servidor");
		}
		
		if (resposta == null || resposta[0] == null){
			return null;
		}
		
		if (resposta[0].equals("200")) {
			GsonBuilder gsonBuilder = new GsonBuilder().setDateFormat("dd/MM/yyyy");
			Gson gson = gsonBuilder.create();
			Servidor servidor = gson.fromJson(resposta[1], Servidor.class);
			
			return servidorConverter.fromModelToEntity(servidor);
		} else if (resposta[0].equals("404")) {
			//log.excecao(resposta[0] + ". " + resposta[1], null);
			throw new ApplicationException("Webservice com problemas. Contate o administrador do sistema");
		} else if (resposta[0].equals("500")) {
			//log.excecao(resposta[0] + ". " + resposta[1], null);
			throw new ApplicationException("Webservice com problemas. Contate o administrador do sistema");
		} else if (resposta[0].equals("400")) {
			//log.excecao(resposta[0] + ". " + resposta[1], null);
			throw new ApplicationException("Webservice com problemas. Contate o administrador do sistema");
		} else{
			//log.excecao(resposta[0] + ". " + resposta[1], null);
			throw new ApplicationException("Webservice inativo. Tente novamente em alguns minutos. Erro: " + resposta[0]);
		} 
	}
	
	public br.gov.mt.indea.sistemaformin.webservice.entity.Servidor getServidorByMatricula(String matricula) throws ApplicationException {
		matricula = matricula.replace(".", "").replace("-", "");
		String[] resposta = null;
		try {
			System.out.println(URL_WS + "matricula/" + matricula);
			resposta = new WebServiceCliente().get(URL_WS + "matricula/" + matricula);
		} catch (URISyntaxException e) {
			//log.excecao(e.getMessage(), e);
			throw new ApplicationException("Erro ao buscar o servidor");
		}
		
		if (resposta == null || resposta[0] == null){
			return null;
		}
		
		if (resposta[0].equals("200")) {
			GsonBuilder gsonBuilder = new GsonBuilder().setDateFormat("dd/MM/yyyy");
			Gson gson = gsonBuilder.create();
			Servidor servidor = gson.fromJson(resposta[1], Servidor.class);
			
			return servidorConverter.fromModelToEntity(servidor);
		} else if (resposta[0].equals("404")) {
			//log.excecao(resposta[0] + ". " + resposta[1], null);
			throw new ApplicationException("Webservice com problemas. Contate o administrador do sistema");
		} else if (resposta[0].equals("500")) {
			//log.excecao(resposta[0] + ". " + resposta[1], null);
			throw new ApplicationException("Webservice com problemas. Contate o administrador do sistema");
		} else if (resposta[0].equals("400")) {
			//log.excecao(resposta[0] + ". " + resposta[1], null);
			throw new ApplicationException("Webservice com problemas. Contate o administrador do sistema");
		} else{
			//log.excecao(resposta[0] + ". " + resposta[1], null);
			throw new ApplicationException("Webservice inativo. Tente novamente em alguns minutos. Erro: " + resposta[0]);
		} 
	}
	
}
