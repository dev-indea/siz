package br.gov.mt.indea.sistemaformin.managedbeans;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.constraints.Past;

import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLBeanName;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;

import br.gov.mt.indea.sistemaformin.entity.AvaliacaoClinicaSV;
import br.gov.mt.indea.sistemaformin.entity.FormCOM;
import br.gov.mt.indea.sistemaformin.entity.FormIN;
import br.gov.mt.indea.sistemaformin.entity.FormSV;
import br.gov.mt.indea.sistemaformin.entity.Municipio;
import br.gov.mt.indea.sistemaformin.entity.NovoEstabelecimentoParaInvestigacao;
import br.gov.mt.indea.sistemaformin.entity.UF;
import br.gov.mt.indea.sistemaformin.enums.Dominio.SimNao;
import br.gov.mt.indea.sistemaformin.exception.ApplicationErrorException;
import br.gov.mt.indea.sistemaformin.exception.ApplicationException;
import br.gov.mt.indea.sistemaformin.service.FormCOMService;
import br.gov.mt.indea.sistemaformin.service.FormINService;
import br.gov.mt.indea.sistemaformin.service.FormSVService;
import br.gov.mt.indea.sistemaformin.service.MunicipioService;
import br.gov.mt.indea.sistemaformin.service.PropriedadeService;
import br.gov.mt.indea.sistemaformin.util.FacesMessageUtil;
import br.gov.mt.indea.sistemaformin.webservice.client.ClientWebServiceVeterinario;
import br.gov.mt.indea.sistemaformin.webservice.entity.Endereco;
import br.gov.mt.indea.sistemaformin.webservice.entity.Propriedade;
import br.gov.mt.indea.sistemaformin.webservice.entity.Veterinario;

@Named
@ViewScoped
@URLBeanName("formSVManagedBean")
@URLMappings(mappings = {
		@URLMapping(id = "pesquisarFormSV", pattern = "/formIN/#{formSVManagedBean.idFormIN}/formSV/pesquisar", viewId = "/pages/formSV/lista.jsf"),
		@URLMapping(id = "incluirFormSV", pattern = "/formIN/#{formSVManagedBean.idFormIN}/formSV/incluir", viewId = "/pages/formSV/novo.jsf"),
		@URLMapping(id = "alterarFormSV", pattern = "/formIN/#{formSVManagedBean.idFormIN}/formSV/alterar/#{formSVManagedBean.id}", viewId = "/pages/formSV/novo.jsf"),
		@URLMapping(id = "impressaoFormSV", pattern = "/formIN/#{formSVManagedBean.idFormIN}/formSV/impressao/#{formSVManagedBean.id}", viewId = "/pages/impressao.jsf")})
public class FormSVManagedBean implements Serializable {

	private static final long serialVersionUID = 1760023349204758559L;

	private Long id;
	
	private Long idFormIN;
	
	private FormIN formIN;
	
	@Inject
	private FormINService formINService;
	
	@Inject
	private FormSVService formSVService;
	
	@Inject
	private FormCOMService formCOMService;
	
	@Inject
	private MunicipioService municipioService;
	
	@Inject
	private FormSV formSV;
	
	private AvaliacaoClinicaSV avaliacaoClinicaSV = new AvaliacaoClinicaSV();
	
	private boolean editandoAvaliacao = false;
	
	private boolean visualizandoAvaliacao = false;
	
	@Past(message="Data precisa ser menor ou igual a data atual")
	private Date dataInvestigacao;
	
	private String cpfVeterinario;

	private String crmvVeterinario;

	private List<Veterinario> listaVeterinarios;
	
	private boolean editandoNovoEstabelecimento = false;
	
	private boolean visualizandoNovoEstabelecimento = false;
	
	private Long idPropriedade;

	private List<Propriedade> listaPropriedades;
	
	private NovoEstabelecimentoParaInvestigacao novoEstabelecimentoParaInvestigacao = new NovoEstabelecimentoParaInvestigacao();
	
	@Inject
	private Propriedade propriedade;
	
	private boolean forceUpdatePropriedade = false;
	
	@Inject
	private Municipio municipio;
	
	private UF uf;
	
	@Inject
	private PropriedadeService propriedadeService;
	
	@Inject
	private ClientWebServiceVeterinario clientWebServiceVeterinario;
	
	public List<Municipio> getListaMunicipios(){
		if (this.uf == null)
			return null;
		
		return municipioService.findAllByUF(this.uf);
	}
	
	@PostConstruct
	private void init(){
		
	}
	
	private void carregarFormIN(){
		this.formIN = formINService.findByIdFetchPropriedade(this.idFormIN);
	}
	
	private void limpar() {
		this.formSV = new FormSV();
		this.dataInvestigacao = null;
	}
	
	@URLAction(mappingId = "incluirFormSV", onPostback = false)
	public void novo(){
		this.limpar();
		this.carregarFormIN();
		this.formSV.setFormIN(formIN);
	}
	
//	public String novo(FormCOM formCOM){
//		this.iniciarConversacao();
//		this.formSV.setFormCOM(formCOM);
//		this.formSV.setFormIN(formCOM.getFormIN());
//
//		return "/pages/formSV/novo.xhtml";
//	}
	
	@URLAction(mappingId = "alterarFormSV", onPostback = false)
	public void editar(){
		this.formSV = formSVService.findByIdFetchAll(this.getId());
		
		if (this.formSV.getDataInvestigacao() != null)
			this.dataInvestigacao = this.formSV.getDataInvestigacao().getTime();
	}
	
	public String adicionar() throws IOException{
		
		if (this.dataInvestigacao != null){
			if (this.formSV.getDataInvestigacao() == null)
				this.formSV.setDataInvestigacao(Calendar.getInstance());
			this.formSV.getDataInvestigacao().setTime(dataInvestigacao);
		}else
			this.formSV.setDataInvestigacao(null);
		
		if (this.formSV.getVeterinarioResponsavelPeloAtendimento() == null){
			FacesContext context = FacesContext.getCurrentInstance();
			
			context.addMessage("cadastro:fieldVeterinario:campoVeterinario", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Veterin�rio: valor � obrigat�rio.", "Veterin�rio: valor � obrigat�rio."));
			return "";
		}
		
		if (formSV.getId() != null){
			this.formSVService.saveOrUpdate(formSV);
			FacesMessageUtil.addInfoContextFacesMessage("Form SV atualizado", "");
		}else{
			this.formSV.setDataCadastro(Calendar.getInstance());
			this.formSVService.saveOrUpdate(formSV);
			FacesMessageUtil.addInfoContextFacesMessage("Form SV adicionado", "");
		}
		
		limpar();
	    return "pretty:visaoGeralFormIN";
	}
	
	@Inject
	private VisualizadorImpressaoManagedBean visualizadorImpressaoManagedBean;
	
	@URLAction(mappingId = "impressaoFormSV", onPostback = false)
	public void imprimir() throws ApplicationErrorException{
		this.formSV = formSVService.findByIdFetchAll(this.getId());
		
		visualizadorImpressaoManagedBean.exibirFormSV(formSV);
	}
	
	public void remover(FormSV formSV){
		this.formSVService.delete(formSV);
		FacesMessageUtil.addInfoContextFacesMessage("Form SV exclu�do com sucesso", "");
	}
	
	public List<FormCOM> getListaFormCOM(){
		return formCOMService.findAllBy(this.formSV.getFormIN());
	}
	
	public void adicionarAvaliacaoClinicaSV(){
		this.avaliacaoClinicaSV.setFormSV(this.formSV);
		
		if (!editandoAvaliacao)
			this.formSV.getAvaliacoesClinicas().add(avaliacaoClinicaSV);
		else
			editandoAvaliacao = false;
		
		this.avaliacaoClinicaSV = new AvaliacaoClinicaSV();
		FacesMessageUtil.addInfoContextFacesMessage("Avalia��o cl�nica inclu�da com sucesso", "");
	}
	
	public void novaAvaliacao(){
		this.avaliacaoClinicaSV = new AvaliacaoClinicaSV();
		this.editandoAvaliacao = false;
		this.visualizandoAvaliacao = false;
	}
	
	public void removerAvaliacaoClinicaSV(AvaliacaoClinicaSV avaliacaoClinicaSV){
		this.formSV.getAvaliacoesClinicas().remove(avaliacaoClinicaSV);
	}
	
	public void editarAvaliacao(AvaliacaoClinicaSV avaliacaoClinicaSV){
		this.editandoAvaliacao = true;
		this.visualizandoAvaliacao = false;
		this.avaliacaoClinicaSV = avaliacaoClinicaSV;
	}
	
	public void visualizarAvaliacao(AvaliacaoClinicaSV avaliacaoClinicaSV){
		this.visualizandoAvaliacao = true;
		this.editandoAvaliacao = false;
		this.avaliacaoClinicaSV = avaliacaoClinicaSV;
	}
	
	public void adicionarNovoEstabelecimentoParaInvestigacao(){
		boolean isNovoEstabelecimentoOk = true;
		
		FacesContext context = FacesContext.getCurrentInstance();
		
		if (this.novoEstabelecimentoParaInvestigacao.getPropriedade() == null){
			context.addMessage("formModalNovoEstabelecimento:fieldPropriedade:campoPropriedade", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Propriedade: valor � obrigat�rio.", "Propriedade: valor � obrigat�rio."));
			isNovoEstabelecimentoOk = false;
		}
		
		if (!isNovoEstabelecimentoOk){
			return;
		}
		
		this.novoEstabelecimentoParaInvestigacao.setFormSV(this.formSV);
		
		if (!editandoNovoEstabelecimento)
			this.formSV.getNovoEstabelecimentoParaInvestigacaos().add(novoEstabelecimentoParaInvestigacao);
		else
			editandoNovoEstabelecimento = false;
		
		this.novoEstabelecimentoParaInvestigacao = new NovoEstabelecimentoParaInvestigacao();
		FacesMessageUtil.addInfoContextFacesMessage("Estabelecimento inclu�do com sucesso", "");
	}
	
	public void novaNovoEstabelecimentoParaInvestigacao(){
		this.novoEstabelecimentoParaInvestigacao = new NovoEstabelecimentoParaInvestigacao();
		this.editandoNovoEstabelecimento = false;
		this.visualizandoNovoEstabelecimento = false;
	}
	
	public void removerNovoEstabelecimentoParaInvestigacao(NovoEstabelecimentoParaInvestigacao novoEstabelecimento){
		this.formSV.getNovoEstabelecimentoParaInvestigacaos().remove(novoEstabelecimento);
	}
	
	public void editarNovoEstabelecimentoParaInvestigacao(NovoEstabelecimentoParaInvestigacao novoEstabelecimento){
		this.editandoNovoEstabelecimento = true;
		this.visualizandoNovoEstabelecimento = false;
		this.novoEstabelecimentoParaInvestigacao = novoEstabelecimento;
	}
	
	public void visualizarNovoEstabelecimentoParaInvestigacao(NovoEstabelecimentoParaInvestigacao novoEstabelecimento){
		this.visualizandoNovoEstabelecimento = true;
		this.editandoNovoEstabelecimento = false;
		this.novoEstabelecimentoParaInvestigacao = novoEstabelecimento;
	}
	
	public void abrirTelaDeBuscaDePropriedade(){
		this.idPropriedade = null;
		this.listaPropriedades = null;
	}
	
	public void buscarPropriedade(){
		this.listaPropriedades = null;
		
		Propriedade p;
		try {
			p = propriedadeService.findByCodigoFetchAll(idPropriedade, forceUpdatePropriedade);
			if (p != null){
				this.listaPropriedades = new ArrayList<Propriedade>();
				listaPropriedades.add(p);
			}
		} catch (ApplicationException e) {
			FacesMessageUtil.addErrorContextFacesMessage("Erro", e.getLocalizedMessage());
			return;
		}
		
		if (this.listaPropriedades == null || this.listaPropriedades.isEmpty())
			FacesMessageUtil.addWarnContextFacesMessage("Nenhuma propriedade foi encontrada", "");
	}
	
	public void selecionarPropriedade_NovoEstab(Propriedade propriedade){
		this.novoEstabelecimentoParaInvestigacao.setPropriedade(propriedade);
		FacesMessageUtil.addInfoContextFacesMessage("Propriedade " + this.novoEstabelecimentoParaInvestigacao.getPropriedade().getNome() + " selecionada", "");
	}
	
	public void abrirTelaDeCadastroDePropriedade(){
		this.propriedade = new Propriedade();
	}
	
	public void cadastrarPropriedade_NovoEstab(){
		this.propriedade.setMunicipio(this.municipio);
		this.novoEstabelecimentoParaInvestigacao.setPropriedade(this.propriedade);
		this.propriedade = null;
		this.municipio = null;
		this.uf = null;
		
		FacesMessageUtil.addInfoContextFacesMessage("Propriedade inclu�da com sucesso", "");
	}
	
	public void abrirTelaDeBuscaDeVeterinario(){
		this.cpfVeterinario = null;
		this.crmvVeterinario = null;
		this.listaVeterinarios = null;
	}
	
	public void buscarVeterinarioPorCpf(){
		this.listaVeterinarios = null;
		
		Veterinario v;
		try {
			v = clientWebServiceVeterinario.getVeterinarioByCpf(cpfVeterinario);
			
			if (v != null){
				this.listaVeterinarios = new ArrayList<Veterinario>();
				this.listaVeterinarios.add(v);
			}
		} catch (ApplicationException e) {
			FacesMessageUtil.addErrorContextFacesMessage("Erro", e.getLocalizedMessage());
			return;
		}
		
		if (this.listaVeterinarios == null || this.listaVeterinarios.isEmpty())
			FacesMessageUtil.addWarnContextFacesMessage("Nenhum veterin�rio foi encontrado", "");
	}
	
	public void buscarVeterinarioPorCrmv(){
		this.listaVeterinarios = null;
		
		Veterinario v;
		try {
			v = clientWebServiceVeterinario.getVeterinarioByCRMV(crmvVeterinario);
			
			if (v != null){
				this.listaVeterinarios = new ArrayList<Veterinario>();
				this.listaVeterinarios.add(v);
			}
		} catch (ApplicationException e) {
			FacesMessageUtil.addErrorContextFacesMessage("Erro", e.getLocalizedMessage());
			return;
		}
		
		if (this.listaVeterinarios == null || this.listaVeterinarios.isEmpty())
			FacesMessageUtil.addWarnContextFacesMessage("Nenhum veterin�rio foi encontrado", "");
	}
	
	public void selecionarVeterinario(Veterinario veterinario){
		this.formSV.setVeterinarioResponsavelPeloAtendimento(veterinario);
		FacesMessageUtil.addInfoContextFacesMessage("Veterinario " + this.formSV.getVeterinarioResponsavelPeloAtendimento().getNome() + " selecionado", "");
	}
	
	public boolean isEstabelecimentoPossuiAssistenciaVeterinariaPermanente(){
		boolean resultado = false;
		if (this.formSV.getEstabelecimentoPossuiAssistenciaVeterinariaPermanente() == null)
			return resultado;
		else
			resultado = this.formSV.getEstabelecimentoPossuiAssistenciaVeterinariaPermanente().equals(SimNao.SIM);
		
		if (resultado){
			this.formSV.setVeterinarioAssistenteDoEstabelecimento(new Veterinario());
			this.formSV.getVeterinarioAssistenteDoEstabelecimento().setEndereco(new Endereco());
		}
		
		return resultado;
	}

	public FormSV getFormSV() {
		return formSV;
	}

	public void setFormSV(FormSV formSV) {
		this.formSV = formSV;
	}

	public AvaliacaoClinicaSV getAvaliacaoClinicaSV() {
		return avaliacaoClinicaSV;
	}

	public void setAvaliacaoClinicaSV(AvaliacaoClinicaSV avaliacaoClinicaSV) {
		this.avaliacaoClinicaSV = avaliacaoClinicaSV;
	}

	public boolean isVisualizandoAvaliacao() {
		return visualizandoAvaliacao;
	}

	public Date getDataInvestigacao() {
		return dataInvestigacao;
	}

	public void setDataInvestigacao(Date dataInvestigacao) {
		this.dataInvestigacao = dataInvestigacao;
	}

	public List<Veterinario> getListaVeterinarios() {
		return listaVeterinarios;
	}

	public void setListaVeterinarios(List<Veterinario> listaVeterinarios) {
		this.listaVeterinarios = listaVeterinarios;
	}

	public String getCpfVeterinario() {
		return cpfVeterinario;
	}

	public void setCpfVeterinario(String cpfVeterinario) {
		this.cpfVeterinario = cpfVeterinario;
	}

	public String getCrmvVeterinario() {
		return crmvVeterinario;
	}

	public void setCrmvVeterinario(String crmvVeterinario) {
		this.crmvVeterinario = crmvVeterinario;
	}

	public NovoEstabelecimentoParaInvestigacao getNovoEstabelecimentoParaInvestigacao() {
		return novoEstabelecimentoParaInvestigacao;
	}

	public void setNovoEstabelecimentoParaInvestigacao(
			NovoEstabelecimentoParaInvestigacao novoEstabelecimentoParaInvestigacao) {
		this.novoEstabelecimentoParaInvestigacao = novoEstabelecimentoParaInvestigacao;
	}

	public Long getIdPropriedade() {
		return idPropriedade;
	}

	public void setIdPropriedade(Long idPropriedade) {
		this.idPropriedade = idPropriedade;
	}

	public List<Propriedade> getListaPropriedades() {
		return listaPropriedades;
	}

	public void setListaPropriedades(List<Propriedade> listaPropriedades) {
		this.listaPropriedades = listaPropriedades;
	}

	public boolean isEditandoNovoEstabelecimento() {
		return editandoNovoEstabelecimento;
	}

	public void setEditandoNovoEstabelecimento(boolean editandoNovoEstabelecimento) {
		this.editandoNovoEstabelecimento = editandoNovoEstabelecimento;
	}

	public boolean isVisualizandoNovoEstabelecimento() {
		return visualizandoNovoEstabelecimento;
	}

	public void setVisualizandoNovoEstabelecimento(
			boolean visualizandoNovoEstabelecimento) {
		this.visualizandoNovoEstabelecimento = visualizandoNovoEstabelecimento;
	}

	public Propriedade getPropriedade() {
		return propriedade;
	}

	public void setPropriedade(Propriedade propriedade) {
		this.propriedade = propriedade;
	}

	public Municipio getMunicipio() {
		return municipio;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}

	public UF getUf() {
		return uf;
	}

	public void setUf(UF uf) {
		this.uf = uf;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdFormIN() {
		return idFormIN;
	}

	public void setIdFormIN(Long idFormIN) {
		this.idFormIN = idFormIN;
	}

	public FormIN getFormIN() {
		return formIN;
	}

	public void setFormIN(FormIN formIN) {
		this.formIN = formIN;
	}

	public boolean isForceUpdatePropriedade() {
		return forceUpdatePropriedade;
	}

	public void setForceUpdatePropriedade(boolean forceUpdatePropriedade) {
		this.forceUpdatePropriedade = forceUpdatePropriedade;
	}

}
