package br.gov.mt.indea.sistemaformin.exception;

public class XlsxFormatException extends Exception {

	private static final long serialVersionUID = -5849615990643923188L;
	
	private String mensagem;
	
	public XlsxFormatException(){
		super();
	}
	
	public XlsxFormatException(String mensagem){
		super(mensagem);
		this.mensagem = mensagem;
	}
	
	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

}
