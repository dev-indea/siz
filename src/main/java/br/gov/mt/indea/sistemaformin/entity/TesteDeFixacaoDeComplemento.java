package br.gov.mt.indea.sistemaformin.entity;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

import br.gov.mt.indea.sistemaformin.enums.Dominio.PositivoNegativo;

@Audited
@Entity(name="teste_fixacao_complemento")
public class TesteDeFixacaoDeComplemento extends BaseEntity<Long>{

	private static final long serialVersionUID = -2774366385105998755L;
	
	@Id
	@SequenceGenerator(name="teste_fixacao_complemento_seq", sequenceName="teste_fixacao_complemento_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="teste_fixacao_complemento_seq")
	private Long id;
	
	@ManyToOne
	@JoinColumn(name="id_formmaleina")
	private FormMaleina formMaleina;
	
	@Column(name="id_laudo")
	private Long idLaudo;
	
	@Enumerated(EnumType.STRING)
	@Column(name="resultado_teste")
	private PositivoNegativo resultadoTeste;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_resultado")
	private Calendar dataResultado;

	@Column(length=255)
	private String laboratorio;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public FormMaleina getFormMaleina() {
		return formMaleina;
	}

	public void setFormMaleina(FormMaleina formMaleina) {
		this.formMaleina = formMaleina;
	}

	public Long getIdLaudo() {
		return idLaudo;
	}

	public void setIdLaudo(Long idLaudo) {
		this.idLaudo = idLaudo;
	}

	public PositivoNegativo getResultadoTeste() {
		return resultadoTeste;
	}

	public void setResultadoTeste(PositivoNegativo resultadoTeste) {
		this.resultadoTeste = resultadoTeste;
	}

	public Calendar getDataResultado() {
		return dataResultado;
	}

	public void setDataResultado(Calendar dataResultado) {
		this.dataResultado = dataResultado;
	}

	public String getLaboratorio() {
		return laboratorio;
	}

	public void setLaboratorio(String laboratorio) {
		this.laboratorio = laboratorio;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TesteDeFixacaoDeComplemento other = (TesteDeFixacaoDeComplemento) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}