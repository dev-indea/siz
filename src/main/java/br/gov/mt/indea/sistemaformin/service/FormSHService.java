package br.gov.mt.indea.sistemaformin.service;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.mt.indea.sistemaformin.entity.FormSH;
import br.gov.mt.indea.sistemaformin.entity.NovoEstabelecimentoParaInvestigacao;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class FormSHService extends PaginableService<FormSH, Long> {

	private static final Logger log = LoggerFactory.getLogger(FormSHService.class);
	
	protected FormSHService() {
		super(FormSH.class);
	}
	
	public FormSH findByIdFetchAll(Long id){
		FormSH formSH;
		
		StringBuilder sql = new StringBuilder();
		sql.append("  from FormSH formSH ")
		   .append("  join fetch formSH.formIN formin")
		   .append("  left join fetch formSH.formCOM")
		   .append("  join fetch formin.propriedade propriedade")
		   .append("  join fetch formin.proprietario proprietario")
		   .append("  left join fetch proprietario.endereco")
		   .append("  join fetch propriedade.municipio")
		   .append("  join fetch propriedade.ule")
		   .append("  left join fetch formSH.veterinarioAssistenteDoEstabelecimento veterinarioAssistenteDoEstabelecimento")
		   .append("  left join fetch veterinarioAssistenteDoEstabelecimento.endereco ")
		   .append("  join fetch formSH.veterinarioResponsavelPeloAtendimento vet")
		   .append("  join fetch vet.conselho")
		   .append("  join fetch vet.tipoEmitente")
		   .append("  left join fetch vet.ule uu")
		   .append("  left join fetch uu.urs")
		   .append("  left join fetch vet.endereco")
		   .append(" where formSH.id = :id ");
		
		Query query = getSession().createQuery(sql.toString()).setLong("id", id);
		formSH = (FormSH) query.uniqueResult();
		
		if (formSH != null){
		
			Hibernate.initialize(formSH.getTipoCriacaoAnimais());
			Hibernate.initialize(formSH.getTipoReposicaoAnimais());
			Hibernate.initialize(formSH.getTipoOrigemRacaoAnimais());
			Hibernate.initialize(formSH.getTipoOrigemRestosDeComida());
			Hibernate.initialize(formSH.getTipoOrigemSoroOuRestosDeLavoura());
			Hibernate.initialize(formSH.getSinaisClinicosEstadoGeral());
			Hibernate.initialize(formSH.getSinaisClinicosSistemaRespiratorio());
			Hibernate.initialize(formSH.getSinaisClinicosSistemaNervoso());
			Hibernate.initialize(formSH.getSinaisClinicosSistemaDigestorio());
			Hibernate.initialize(formSH.getSinaisClinicosSistemaReprodutivo());
			Hibernate.initialize(formSH.getSinaisClinicosSistemaTegumentar());
			Hibernate.initialize(formSH.getSinaisClinicosSistemaLinfatico());
			Hibernate.initialize(formSH.getNovoEstabelecimentoParaInvestigacaos());
			
			for (NovoEstabelecimentoParaInvestigacao estabelecimento : formSH.getNovoEstabelecimentoParaInvestigacaos()) {
				Hibernate.initialize(estabelecimento.getPropriedade());
				Hibernate.initialize(estabelecimento.getPropriedade().getEndereco());
				Hibernate.initialize(estabelecimento.getPropriedade().getMunicipio());
				Hibernate.initialize(estabelecimento.getPropriedade().getUle());
				Hibernate.initialize(estabelecimento.getListaVinculoEpidemiologico());
			}
			
		}
		
		return formSH;
	}
	
	@Override
	public void saveOrUpdate(FormSH formSH) {
		super.saveOrUpdate(formSH);
		
		log.info("Salvando Form SH {}", formSH.getId());
	}
	
	@Override
	public void delete(FormSH formSH) {
		super.delete(formSH);
		
		log.info("Removendo Form SH {}", formSH.getId());
	}
	
	@Override
	public void validar(FormSH FormSH) {

	}

	@Override
	public void validarPersist(FormSH FormSH) {

	}

	@Override
	public void validarMerge(FormSH FormSH) {

	}

	@Override
	public void validarDelete(FormSH FormSH) {

	}

}
