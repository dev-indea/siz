package br.gov.mt.indea.sistemaformin.managedbeans;

import java.io.IOException;
import java.io.Serializable;
import java.util.Calendar;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLBeanName;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;

import br.gov.mt.indea.sistemaformin.entity.TipoExame;
import br.gov.mt.indea.sistemaformin.service.LazyObjectDataModel;
import br.gov.mt.indea.sistemaformin.service.TipoExameService;
import br.gov.mt.indea.sistemaformin.util.FacesMessageUtil;

@Named
@ViewScoped
@URLBeanName("tipoExameManagedBean")
@URLMappings(mappings = {
		@URLMapping(id = "pesquisarTipoExame", pattern = "/tipoExame/pesquisar", viewId = "/pages/tipoExame/lista.jsf"),
		@URLMapping(id = "incluirTipoExame", pattern = "/tipoExame/incluir", viewId = "/pages/tipoExame/novo.jsf"),
		@URLMapping(id = "alterarTipoExame", pattern = "/tipoExame/alterar/#{tipoExameManagedBean.id}", viewId = "/pages/tipoExame/novo.jsf")})
public class TipoExameManagedBean implements Serializable {
	
	private static final long serialVersionUID = -7409333626528791621L;

	private Long id;

	@Inject
	private TipoExameService tipoExameService;
	
	@Inject
	private TipoExame tipoExame;
	
	private LazyObjectDataModel<TipoExame> listaTipoExame;
	
	@PostConstruct
	public void init(){
		this.listaTipoExame = new LazyObjectDataModel<TipoExame>(this.tipoExameService);
	}
	
	public void limpar(){
		this.tipoExame = new TipoExame();
	}
	
	@URLAction(mappingId = "incluirTipoExame", onPostback = false)
	public void novo(){
		limpar();
	}
	
	@URLAction(mappingId = "alterarTipoExame", onPostback = false)
	public void editar(){
		this.tipoExame = tipoExameService.findByIdFetchAll(this.getId());
	}
	
	public String adicionar() throws IOException{
		
		if (tipoExame.getId() != null){
			this.tipoExameService.saveOrUpdate(tipoExame);
			FacesMessageUtil.addInfoContextFacesMessage("Tipo chave principal atualizado", "");
		}else{
			this.tipoExame.setDataCadastro(Calendar.getInstance());
			this.tipoExameService.saveOrUpdate(tipoExame);
			FacesMessageUtil.addInfoContextFacesMessage("Tipo chave principal adicionado", "");
		}
		
		this.tipoExame = new TipoExame();
		
		return "pretty:pesquisarTipoExame";
	}
	
	public void remover(TipoExame tipoExame){
		this.tipoExameService.delete(tipoExame);
		FacesMessageUtil.addInfoContextFacesMessage("Tipo chave principal exclu�do com sucesso", "");
	}
	
	public TipoExame getTipoExame() {
		return tipoExame;
	}

	public void setTipoExame(TipoExame tipoExame) {
		this.tipoExame = tipoExame;
	}

	public LazyObjectDataModel<TipoExame> getListaTipoExame() {
		return listaTipoExame;
	}

	public void setListaTipoExame(
			LazyObjectDataModel<TipoExame> listaTipoExame) {
		this.listaTipoExame = listaTipoExame;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
