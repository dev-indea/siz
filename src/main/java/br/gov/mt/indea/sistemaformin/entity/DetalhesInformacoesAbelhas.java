package br.gov.mt.indea.sistemaformin.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.envers.Audited;

@Audited
@Entity
@DiscriminatorValue("abelhas")
public class DetalhesInformacoesAbelhas extends AbstractDetalhesInformacoesDeAnimais implements Cloneable{
	
	private static final long serialVersionUID = 1355044082455412390L;

	@ManyToOne
	@JoinColumn(name="id_informacoes_abelhas")
	private InformacoesAbelhas informacoesAbelhas;

	public InformacoesAbelhas getInformacoesAbelhas() {
		return informacoesAbelhas;
	}

	public void setInformacoesAbelhas(InformacoesAbelhas informacoesAbelhas) {
		this.informacoesAbelhas = informacoesAbelhas;
	}

	protected Object clone(InformacoesAbelhas informacoesAbelhas) throws CloneNotSupportedException{
		DetalhesInformacoesAbelhas cloned = (DetalhesInformacoesAbelhas) super.clone();
		
		cloned.setId(null);
		cloned.setInformacoesAbelhas(informacoesAbelhas);
		
		return cloned;
	}

}