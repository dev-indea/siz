package br.gov.mt.indea.sistemaformin.webservice.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Abatedouro implements Serializable {

	private static final long serialVersionUID = 2243046885433336462L;

	private Long id;
    
    private String nome;
    
    private String apelido;
    
    private String cnpj;
    
    private String codigo;
    
    private String municipio;
    
    private Long numeroInspecao;
    
    private String tipoInspecao;
    
    private String codIbgeEmpresa;
    
    @XmlElement(name = "endereco")
    private Endereco endereco;
    
    @XmlElement(name = "coordenada")
    private CoordenadaGeografica coordenada;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getApelido() {
		return apelido;
	}

	public void setApelido(String apelido) {
		this.apelido = apelido;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	public String getCodIbgeEmpresa() {
		return codIbgeEmpresa;
	}

	public void setCodIbgeEmpresa(String codIbgeEmpresa) {
		this.codIbgeEmpresa = codIbgeEmpresa;
	}

	public Long getNumeroInspecao() {
		return numeroInspecao;
	}

	public void setNumeroInspecao(Long numeroInspecao) {
		this.numeroInspecao = numeroInspecao;
	}

	public String getTipoInspecao() {
		return tipoInspecao;
	}

	public void setTipoInspecao(String tipoInspecao) {
		this.tipoInspecao = tipoInspecao;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public CoordenadaGeografica getCoordenada() {
		return coordenada;
	}

	public void setCoordenada(CoordenadaGeografica coordenada) {
		this.coordenada = coordenada;
	}

}