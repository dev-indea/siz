package br.gov.mt.indea.sistemaformin.entity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

import br.gov.mt.indea.sistemaformin.webservice.entity.Propriedade;

@Audited
@Entity
@Table(name="form_notifica")
public class FormNotifica extends BaseEntity<Long>{

	private static final long serialVersionUID = 3657868508326962579L;

	@Id
	@SequenceGenerator(name="form_notifica_seq", sequenceName="form_notifica_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="form_notifica_seq")
	private Long id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_cadastro")
	private Calendar dataCadastro;
	
	@OneToOne(mappedBy="formNotifica", cascade=CascadeType.ALL)
	@JoinColumn(name="id_notificante")
	private Notificante notificante;
	
	@OneToMany(mappedBy="formNotifica", orphanRemoval=true, cascade={CascadeType.ALL}, fetch=FetchType.LAZY)
	private List<DetalheFormNotifica> listaDetalheFormNotifica = new ArrayList<DetalheFormNotifica>(); 
	
	@OneToOne(mappedBy="formNotifica", cascade=CascadeType.ALL)
	@JoinColumn(name="id_suspeita_doenca_animal")
	private SuspeitaDoencaAnimal suspeitaDoencaAnimal = new SuspeitaDoencaAnimal();
	
	public String getPropriedadesAsString(){
    	StringBuilder sb = null;
    	
    	if (listaDetalheFormNotifica != null && !listaDetalheFormNotifica.isEmpty()){
    		Propriedade propriedade;
    		
			for (DetalheFormNotifica detalhe : listaDetalheFormNotifica) {
				propriedade = detalhe.getPropriedade();
				
				if (sb == null){
					sb = new StringBuilder();
					sb.append(propriedade.getCodigoPropriedade())
					  .append(" - ")
					  .append(propriedade.getNome())
					  .append(" - ")
					  .append(propriedade.getMunicipio().getNome());
				}else
					sb.append("<br/>")
					  .append(propriedade.getCodigoPropriedade())
					  .append(" - ")
					  .append(propriedade.getNome())
					  .append(" - ")
					  .append(propriedade.getMunicipio().getNome());
			}
    	}
		
    	if (sb != null)
    		return sb.toString();
    	else
    		return null;
    }
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Calendar getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Calendar dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Notificante getNotificante() {
		return notificante;
	}

	public void setNotificante(Notificante notificante) {
		this.notificante = notificante;
	}

	public SuspeitaDoencaAnimal getSuspeitaDoencaAnimal() {
		return suspeitaDoencaAnimal;
	}

	public void setSuspeitaDoencaAnimal(SuspeitaDoencaAnimal suspeitaDoencaAnimal) {
		this.suspeitaDoencaAnimal = suspeitaDoencaAnimal;
	}

	public List<DetalheFormNotifica> getListaDetalheFormNotifica() {
		return listaDetalheFormNotifica;
	}

	public void setListaDetalheFormNotifica(List<DetalheFormNotifica> listaDetalheFormNotifica) {
		this.listaDetalheFormNotifica = listaDetalheFormNotifica;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FormNotifica other = (FormNotifica) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}