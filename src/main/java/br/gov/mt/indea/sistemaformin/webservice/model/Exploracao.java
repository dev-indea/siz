package br.gov.mt.indea.sistemaformin.webservice.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Exploracao implements Serializable {

	private static final long serialVersionUID = -2420311196263240812L;

	private Long id;
    
    private Integer estabelecimentoGta;
    
    private Integer propriedade_id;
    
    private Propriedade propriedade;
    
    private String orientacaoLongitude;
    
    private String orientacaoLatitude;
   
    private Integer grauLatitude;
    
    private Integer minLatitude;
    
    private Float segLatitude;
    
    private Integer grauLongitude;
    
    private Integer minLongitude;
    
    private Float segLongitude;
   
    private String codigo;
   
    private String idAntigo;
    
    @XmlElement
    private SituacaoFundiaria situacaoFundiaria;
    
    private Integer capacidadeExploracao;
    
    private String espolio;
    
    private String confinamento;
    
    private String animaisImportados;
    
    private String areaCompartilhada;
    
    private Integer areaUtilizada;
   
    private Float areaExploracao;
    
    private String qntPessoaResidente;
    
    private Integer qntPessoaTrabalha;
    
    private Date dataValidade;
    
    @XmlElement(name = "produtores")
    private List<ProdutorRural> produtores;
   
    public Float getSegLatitude() {
        return segLatitude;
    }

    public void setSegLatitude(Float segLatitude) {
        this.segLatitude = segLatitude;
    }

    public Float getSegLongitude() {
        return segLongitude;
    }

    public void setSegLongitude(Float segLongitude) {
        this.segLongitude = segLongitude;
    }

    public Integer getCapacidadeExploracao() {
        return capacidadeExploracao;
    }

    public void setCapacidadeExploracao(Integer capacidadeExploracao) {
        this.capacidadeExploracao = capacidadeExploracao;
    }

    public String getAreaCompartilhada() {
        return areaCompartilhada;
    }

    public void setAreaCompartilhada(String areaCompartilhada) {
        this.areaCompartilhada = areaCompartilhada;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrientacaoLongitude() {
        return orientacaoLongitude;
    }

    public void setOrientacaoLongitude(String orientacaoLongitude) {
        this.orientacaoLongitude = orientacaoLongitude;
    }

    public String getOrientacaoLatitude() {
        return orientacaoLatitude;
    }

    public void setOrientacaoLatitude(String orientacaoLatitude) {
        this.orientacaoLatitude = orientacaoLatitude;
    }

    public Integer getGrauLatitude() {
        return grauLatitude;
    }

    public void setGrauLatitude(Integer grauLatitude) {
        this.grauLatitude = grauLatitude;
    }

    public Integer getMinLatitude() {
        return minLatitude;
    }

    public void setMinLatitude(Integer minLatitude) {
        this.minLatitude = minLatitude;
    }

    public Integer getGrauLongitude() {
        return grauLongitude;
    }

    public void setGrauLongitude(Integer grauLongitude) {
        this.grauLongitude = grauLongitude;
    }

    public Integer getMinLongitude() {
        return minLongitude;
    }

    public void setMinLongitude(Integer minLongitude) {
        this.minLongitude = minLongitude;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getIdAntigo() {
        return idAntigo;
    }

    public void setIdAntigo(String idAntigo) {
        this.idAntigo = idAntigo;
    }

    public SituacaoFundiaria getSituacaoFundiaria() {
        return situacaoFundiaria;
    }

    public void setSituacaoFundiaria(SituacaoFundiaria situacaoFundiaria) {
        this.situacaoFundiaria = situacaoFundiaria;
    }

    public Integer getAreaUtilizada() {
        return areaUtilizada;
    }

    public void setAreaUtilizada(Integer areaUtilizada) {
        this.areaUtilizada = areaUtilizada;
    }

    public Float getAreaExploracao() {
        return areaExploracao;
    }

    public void setAreaExploracao(Float areaExploracao) {
        this.areaExploracao = areaExploracao;
    }

    
    public String getQntPessoaResidente() {
        return qntPessoaResidente;
    }

    public void setQntPessoaResidente(String qntPessoaResidente) {
        this.qntPessoaResidente = qntPessoaResidente;
    }

    public Integer getQntPessoaTrabalha() {
        return qntPessoaTrabalha;
    }

    public void setQntPessoaTrabalha(Integer qntPessoaTrabalha) {
        this.qntPessoaTrabalha = qntPessoaTrabalha;
    }

    public String getEspolio() {
        return espolio;
    }

    public void setEspolio(String espolio) {
        this.espolio = espolio;
    }

    public String getConfinamento() {
        return confinamento;
    }

    public void setConfinamento(String confinamento) {
        this.confinamento = confinamento;
    }

    public String getAnimaisImportados() {
        return animaisImportados;
    }

    public void setAnimaisImportados(String animaisImportados) {
        this.animaisImportados = animaisImportados;
    }

    public Date getDataValidade() {
        return dataValidade;
    }

    public void setDataValidade(Date dataValidade) {
        this.dataValidade = dataValidade;
    }

    public Integer getEstabelecimentoGta() {
        return estabelecimentoGta;
    }

    public void setEstabelecimentoGta(Integer estabelecimentoGta) {
        this.estabelecimentoGta = estabelecimentoGta;
    }

    public Integer getPropriedade_id() {
        return propriedade_id;
    }

    public void setPropriedade_id(Integer propriedade_id) {
        this.propriedade_id = propriedade_id;
    }

    public List<ProdutorRural> getProdutores() {
        return produtores;
    }

    public void setProdutores(List<ProdutorRural> produtores) {
        this.produtores = produtores;
    }

	public Propriedade getPropriedade() {
		return propriedade;
	}

	public void setPropriedade(Propriedade propriedade) {
		this.propriedade = propriedade;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Exploracao other = (Exploracao) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
