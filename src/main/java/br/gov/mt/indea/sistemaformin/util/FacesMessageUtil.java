package br.gov.mt.indea.sistemaformin.util;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

public class FacesMessageUtil {

	public static void addContextFacesMessage(FacesMessage.Severity severity, String summary, String detail) {
    	addFacesMessage(null, severity, summary, detail);
    }
    
    public static void addFacesMessage(UIComponent component, FacesMessage.Severity severity, String summary, String detail) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(component!=null ? component.getClientId(context) : null, newFacesMessage(severity, summary, detail));
    }

    private static FacesMessage newFacesMessage(Severity severity, String summary, String detail) {
    	return new FacesMessage(severity, summary, detail);
    }
    
    public static void addInfoFacesMessage(UIComponent component, String summary, String detail){
    	addFacesMessage(component, FacesMessage.SEVERITY_INFO, summary, detail);
    }
    
    public static void addWarnFacesMessage(UIComponent component, String summary, String detail){
    	addFacesMessage(component, FacesMessage.SEVERITY_WARN, summary, detail);
    }
    
    public static void addFatalFacesMessage(UIComponent component, String summary, String detail){
    	addFacesMessage(component, FacesMessage.SEVERITY_FATAL, summary, detail);
    }
    
    public static void addErrorFacesMessage(UIComponent component, String summary, String detail){
    	addFacesMessage(component, FacesMessage.SEVERITY_ERROR, summary, detail);
    }
    
    public static void addInfoContextFacesMessage(String summary, String detail) {
    	addFacesMessage(null, FacesMessage.SEVERITY_INFO, summary, detail);
    }
    
    public static void addWarnContextFacesMessage(String summary, String detail) {
    	addFacesMessage(null, FacesMessage.SEVERITY_WARN, summary, detail);
    }
    
    public static void addFatalContextFacesMessage(String summary, String detail) {
    	addFacesMessage(null, FacesMessage.SEVERITY_FATAL, summary, detail);
    }
    
    public static void addErrorContextFacesMessage(String summary, String detail) {
    	addFacesMessage(null, FacesMessage.SEVERITY_ERROR, summary, detail);
    }
}