package br.gov.mt.indea.sistemaformin.managedbeans;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.constraints.Past;

import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLBeanName;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;

import br.gov.mt.indea.sistemaformin.entity.FormIN;
import br.gov.mt.indea.sistemaformin.entity.FormVIN;
import br.gov.mt.indea.sistemaformin.entity.Municipio;
import br.gov.mt.indea.sistemaformin.entity.NovoEstabelecimentoParaInvestigacao;
import br.gov.mt.indea.sistemaformin.entity.UF;
import br.gov.mt.indea.sistemaformin.entity.VistoriaFormVIN;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoCoordenadaGeografica;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoEstabelecimento;
import br.gov.mt.indea.sistemaformin.exception.ApplicationErrorException;
import br.gov.mt.indea.sistemaformin.exception.ApplicationException;
import br.gov.mt.indea.sistemaformin.service.FormINService;
import br.gov.mt.indea.sistemaformin.service.FormVINService;
import br.gov.mt.indea.sistemaformin.service.MunicipioService;
import br.gov.mt.indea.sistemaformin.service.PropriedadeService;
import br.gov.mt.indea.sistemaformin.util.FacesMessageUtil;
import br.gov.mt.indea.sistemaformin.webservice.client.ClientWebServiceAbatedouro;
import br.gov.mt.indea.sistemaformin.webservice.client.ClientWebServiceVeterinario;
import br.gov.mt.indea.sistemaformin.webservice.entity.Abatedouro;
import br.gov.mt.indea.sistemaformin.webservice.entity.Produtor;
import br.gov.mt.indea.sistemaformin.webservice.entity.Propriedade;
import br.gov.mt.indea.sistemaformin.webservice.entity.Veterinario;

@Named
@ViewScoped
@URLBeanName("formVINManagedBean")
@URLMappings(mappings = {
		@URLMapping(id = "pesquisarFormVIN", pattern = "/formIN/#{formVINManagedBean.idFormIN}/formVIN/pesquisar", viewId = "/pages/formVIN/lista.jsf"),
		@URLMapping(id = "incluirFormVIN", pattern = "/formIN/#{formVINManagedBean.idFormIN}/formVIN/incluir", viewId = "/pages/formVIN/novo.jsf"),
		@URLMapping(id = "alterarFormVIN", pattern = "/formIN/#{formVINManagedBean.idFormIN}/formVIN/alterar/#{formVINManagedBean.id}", viewId = "/pages/formVIN/novo.jsf"),
		@URLMapping(id = "impressaoFormVIN", pattern = "/formIN/#{formVINManagedBean.idFormIN}/formVIN/impressao/#{formVINManagedBean.id}", viewId = "/pages/impressao.jsf")})
public class FormVINManagedBean implements Serializable {

	private static final long serialVersionUID = 1760023349204758559L;

	private Long id;
	
	private Long idFormIN;

	private FormIN formIN;
	
	@Inject
	private FormINService formINService;
	
	@Inject
	private FormVINService formVINService;
	
	@Inject
	private MunicipioService municipioService;
	
	@Inject
	private FormVIN formVIN;
	
	private VistoriaFormVIN vistoriaFormVIN = new VistoriaFormVIN();
	
	private boolean editandoVistoriaFormVIN = false;
	
	private boolean visualizandoVistoriaFormVIN = false;
	
	private boolean editandoNovoEstabelecimento = false;
	
	private boolean visualizandoNovoEstabelecimento = false;
	
	@Past(message="Data precisa ser menor ou igual a data atual")
	private Date dataInspecao;
	
	private Long idPropriedade;

	private List<Propriedade> listaPropriedades;
	
	private TipoCoordenadaGeografica tipoCoordenadaGeografica;
	
	private String cpfVeterinario;

	private String crmvVeterinario;

	private List<Veterinario> listaVeterinarios;
	
	private NovoEstabelecimentoParaInvestigacao novoEstabelecimentoParaInvestigacao = new NovoEstabelecimentoParaInvestigacao();

	@Inject
	private Propriedade propriedade;
	
	private boolean forceUpdatePropriedade = false;
	
	@Inject
	private Municipio municipio;
	
	private UF uf;
	
	private TipoEstabelecimento tipoEstabelecimento = TipoEstabelecimento.PROPRIEDADE;
	
	private String cnpjAbatedouro;
	
	private List<Abatedouro> listaAbatedouros;
	
	@Inject
	private PropriedadeService propriedadeService;
	
	@Inject
	private ClientWebServiceVeterinario clientWebServiceVeterinario;
	
	@Inject
	private ClientWebServiceAbatedouro clientWebServiceAbatedouro;
	
	public List<Municipio> getListaMunicipios(){
		if (this.uf == null)
			return null;
		
		return municipioService.findAllByUF(this.uf);
	}
	
	@PostConstruct
	private void init(){
		
	}
	
	private void carregarFormIN(){
		this.formIN = formINService.findByIdFetchPropriedade(this.idFormIN);
	}

	private void limpar() {
		this.formVIN = new FormVIN();
		
		this.dataInspecao = null;
		this.idPropriedade = null;
		this.listaPropriedades = null;
		this.listaVeterinarios = null;
		this.tipoCoordenadaGeografica = null;
	}
	
	@URLAction(mappingId = "incluirFormVIN", onPostback = false)
	public void novo(){
		this.limpar();
		this.carregarFormIN();
		
		this.formVIN.setFormIN(formIN);
	}
	
	@URLAction(mappingId = "alterarFormVIN", onPostback = false)
	public void editar(){
		this.formVIN = formVINService.findByIdFetchAll(this.getId());
		
		if (this.formVIN.getDataInspecao() != null)
			this.dataInspecao = this.formVIN.getDataInspecao().getTime();
		
		if (this.formVIN.getPropriedade() != null)
			this.tipoCoordenadaGeografica = this.formVIN.getPropriedade().getTipoCoordenadaGeografica();
		
		if (this.formVIN.getPropriedade() == null)
			this.tipoEstabelecimento = TipoEstabelecimento.ABATEDOURO;
		else
			this.tipoEstabelecimento = TipoEstabelecimento.PROPRIEDADE;
	}
	
	public String adicionar() throws IOException{
		
		if (this.tipoEstabelecimento.equals(TipoEstabelecimento.PROPRIEDADE)){
			if (this.formVIN.getPropriedade() == null){
				FacesContext context = FacesContext.getCurrentInstance();
				
				context.addMessage("cadastro:fieldPropriedade:campoPropriedade", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Propriedade: valor � obrigat�rio.", "Propriedade: valor � obrigat�rio."));
				return "";
			}
		} else {
			if (this.formVIN.getAbatedouro() == null){
				FacesContext context = FacesContext.getCurrentInstance();
				
				context.addMessage("cadastro:fieldAbatedouro:campoAbatedouro", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Abatedouro: valor � obrigat�rio.", "Abatedouro: valor � obrigat�rio."));
				return "";
			}
		}
		
		boolean isFormINok = true;
		
		if (this.formVIN.getPropriedade() != null){
			if (this.formVIN.getProprietario() == null || this.formVIN.getProprietario().equals("")){
				FacesContext context = FacesContext.getCurrentInstance();
				
				context.addMessage("cadastro:fieldProprietario:campoProprietario", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Propriet�rio: valor � obrigat�rio.", "Propriet�rio: valor � obrigat�rio."));
				isFormINok = false;
			}
		}
		
		if (this.formVIN.getPropriedade() != null){
			if (this.formVIN.getPropriedade().getCoordenadaGeografica() == null || 
					this.formVIN.getPropriedade().getCoordenadaGeografica().getLatGrau() == null ||
					this.formVIN.getPropriedade().getCoordenadaGeografica().getLatMin() == null ||
					this.formVIN.getPropriedade().getCoordenadaGeografica().getLatSeg() == null){
				FacesContext context = FacesContext.getCurrentInstance();
				
				context.addMessage("cadastro:fieldPropriedadeLatitude:campoPropriedadeLatitude", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Coordenadas - Latitude: valor � obrigat�rio.", "O valor deve estar corretamente preenchido no SINDESA."));
				isFormINok = false;
			}
		}
		
		if (this.formVIN.getPropriedade() != null){
			if (this.formVIN.getPropriedade().getCoordenadaGeografica() == null || 
					this.formVIN.getPropriedade().getCoordenadaGeografica().getLongGrau() == null ||
					this.formVIN.getPropriedade().getCoordenadaGeografica().getLongMin() == null ||
					this.formVIN.getPropriedade().getCoordenadaGeografica().getLongSeg() == null){
				FacesContext context = FacesContext.getCurrentInstance();
				
				context.addMessage("cadastro:fieldPropriedadeLongitude:campoPropriedadeLongitude", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Coordenadas - Longitude: valor � obrigat�rio.", "O valor deve estar corretamente preenchido no SINDESA."));
				isFormINok = false;
			}
		}
		
		if (this.formVIN.getAbatedouro() != null){
			if (this.formVIN.getAbatedouro().getCoordenadaGeografica() == null || 
					this.formVIN.getAbatedouro().getCoordenadaGeografica().getLatGrau() == null ||
					this.formVIN.getAbatedouro().getCoordenadaGeografica().getLatMin() == null ||
					this.formVIN.getAbatedouro().getCoordenadaGeografica().getLatSeg() == null){
				FacesContext context = FacesContext.getCurrentInstance();
				
				context.addMessage("cadastro:fieldAbatedouroLatitude:campoAbatedouroLatitude", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Coordenadas - Latitude: valor � obrigat�rio.", "O valor deve estar corretamente preenchido no SINDESA."));
				isFormINok = false;
			}
		}
		
		if (this.formVIN.getAbatedouro() != null){
			if (this.formVIN.getAbatedouro().getCoordenadaGeografica() == null || 
					this.formVIN.getAbatedouro().getCoordenadaGeografica().getLongGrau() == null ||
					this.formVIN.getAbatedouro().getCoordenadaGeografica().getLongMin() == null ||
					this.formVIN.getAbatedouro().getCoordenadaGeografica().getLongSeg() == null){
				FacesContext context = FacesContext.getCurrentInstance();
				
				context.addMessage("cadastro:fieldAbatedouroLongitude:campoAbatedouroLongitude", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Coordenadas - Longitude: valor � obrigat�rio.", "O valor deve estar corretamente preenchido no SINDESA."));
				isFormINok = false;
			}
		}
		
		if (this.formVIN.getVeterinario() == null){
			FacesContext context = FacesContext.getCurrentInstance();
			
			context.addMessage("cadastro:fieldVeterinario:campoVeterinario", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Veterin�rio: valor � obrigat�rio.", "Veterin�rio: valor � obrigat�rio."));
			isFormINok = false;
		}
		
		if (!isFormINok)
			return "";
		
		if (this.tipoEstabelecimento.equals(TipoEstabelecimento.PROPRIEDADE))
			this.formVIN.getPropriedade().setTipoCoordenadaGeografica(tipoCoordenadaGeografica);
		
		if (this.dataInspecao != null){
			if (this.formVIN.getDataInspecao() == null)
				this.formVIN.setDataInspecao(Calendar.getInstance());
			this.formVIN.getDataInspecao().setTime(dataInspecao);
		}else
			this.formVIN.setDataInspecao(null);
		
		if (formVIN.getId() != null){
			this.formVINService.saveOrUpdate(formVIN);
			FacesMessageUtil.addInfoContextFacesMessage("Form VIN atualizado", "");
		}else{
			this.formVIN.setDataCadastro(Calendar.getInstance());
			this.formVINService.saveOrUpdate(formVIN);
			FacesMessageUtil.addInfoContextFacesMessage("Form VIN adicionado", "");
		}
		
		limpar();
	    return "pretty:visaoGeralFormIN";
	}
	
	@Inject
	private VisualizadorImpressaoManagedBean visualizadorImpressaoManagedBean;
	
	@URLAction(mappingId = "impressaoFormVIN", onPostback = false)
	public void imprimir() throws ApplicationErrorException{
		this.formVIN = formVINService.findByIdFetchAll(this.getId());
		
		visualizadorImpressaoManagedBean.exibirFormVIN(formVIN);
	}
	
	public void remover(FormVIN formVIN){
		this.formVINService.delete(formVIN);
		FacesMessageUtil.addInfoContextFacesMessage("Form VIN exclu�do com sucesso", "");
	}
	
	public void adicionarVistoriaFormVIN(){
		this.vistoriaFormVIN.setFormVIN(this.formVIN);
		
		if (!editandoVistoriaFormVIN)
			this.formVIN.getVistoriasFormVIN().add(vistoriaFormVIN);
		else
			editandoVistoriaFormVIN = false;
		
		this.vistoriaFormVIN = new VistoriaFormVIN();
		FacesMessageUtil.addInfoContextFacesMessage("Vistoria inclu�da com sucesso", "");
	}
	
	public void novaVistoriaFormVIN(){
		this.vistoriaFormVIN = new VistoriaFormVIN();
		this.editandoVistoriaFormVIN = false;
		this.visualizandoVistoriaFormVIN = false;
	}
	
	public void removerVistoriaFormVIN(VistoriaFormVIN vistoriaFormVIN){
		this.formVIN.getVistoriasFormVIN().remove(vistoriaFormVIN);
	}
	
	public void editarVistoriaFormVIN(VistoriaFormVIN vistoriaFormVIN){
		this.editandoVistoriaFormVIN = true;
		this.visualizandoVistoriaFormVIN = false;
		this.vistoriaFormVIN = vistoriaFormVIN;
	}
	
	public void visualizarVistoriaFormVIN(VistoriaFormVIN vistoriaFormVIN){
		this.visualizandoVistoriaFormVIN = true;
		this.editandoVistoriaFormVIN = false;
		this.vistoriaFormVIN = vistoriaFormVIN;
	}
	
	public void adicionarNovoEstabelecimentoParaInvestigacao(){
		boolean isNovoEstabelecimentoOk = true;
		
		FacesContext context = FacesContext.getCurrentInstance();
		
		if (this.novoEstabelecimentoParaInvestigacao.getPropriedade() == null){
			context.addMessage("formModalNovoEstabelecimento:fieldPropriedade:campoPropriedade", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Propriedade: valor � obrigat�rio.", "Propriedade: valor � obrigat�rio."));
			isNovoEstabelecimentoOk = false;
		}
		
		if (!isNovoEstabelecimentoOk){
			return;
		}
		
		this.novoEstabelecimentoParaInvestigacao.setFormVIN(this.formVIN);
		
		if (!editandoNovoEstabelecimento)
			this.formVIN.getNovoEstabelecimentoParaInvestigacaos().add(novoEstabelecimentoParaInvestigacao);
		else
			editandoNovoEstabelecimento = false;
		
		this.novoEstabelecimentoParaInvestigacao = new NovoEstabelecimentoParaInvestigacao();
		FacesMessageUtil.addInfoContextFacesMessage("Estabelecimento inclu�do com sucesso", "");
	}
	
	public void novaNovoEstabelecimentoParaInvestigacao(){
		this.novoEstabelecimentoParaInvestigacao = new NovoEstabelecimentoParaInvestigacao();
		this.editandoNovoEstabelecimento = false;
		this.visualizandoNovoEstabelecimento = false;
	}
	
	public void removerNovoEstabelecimentoParaInvestigacao(NovoEstabelecimentoParaInvestigacao novoEstabelecimento){
		this.formVIN.getNovoEstabelecimentoParaInvestigacaos().remove(novoEstabelecimento);
	}
	
	public void editarNovoEstabelecimentoParaInvestigacao(NovoEstabelecimentoParaInvestigacao novoEstabelecimento){
		this.editandoNovoEstabelecimento = true;
		this.visualizandoNovoEstabelecimento = false;
		this.novoEstabelecimentoParaInvestigacao = novoEstabelecimento;
	}
	
	public void visualizarNovoEstabelecimentoParaInvestigacao(NovoEstabelecimentoParaInvestigacao novoEstabelecimento){
		this.visualizandoNovoEstabelecimento = true;
		this.editandoNovoEstabelecimento = false;
		this.novoEstabelecimentoParaInvestigacao = novoEstabelecimento;
	}
	
	public void abrirTelaDeBuscaDePropriedade(){
		this.idPropriedade = null;
		this.listaPropriedades = null;
	}
	
	public void buscarPropriedade(){
		this.listaPropriedades = null;
		
		Propriedade p;
		try {
			p = propriedadeService.findByCodigoFetchAll(idPropriedade, forceUpdatePropriedade);
			if (p != null){
				this.listaPropriedades = new ArrayList<Propriedade>();
				listaPropriedades.add(p);
			}
		} catch (ApplicationException e) {
			FacesMessageUtil.addErrorContextFacesMessage("Erro", e.getLocalizedMessage());
			return;
		}
		
		if (this.listaPropriedades == null || this.listaPropriedades.isEmpty())
			FacesMessageUtil.addWarnContextFacesMessage("Nenhuma propriedade foi encontrada", "");
	}
	
	public void selecionarPropriedade(Propriedade propriedade){
		this.formVIN.setPropriedade(propriedade);
		this.formVIN.setNomePropriedade(propriedade.getNome());
		this.formVIN.setCodigoPropriedade(propriedade.getCodigoPropriedade());
		
		FacesMessageUtil.addInfoContextFacesMessage("Propriedade " + this.formVIN.getPropriedade().getNome() + " selecionada", "");
	}
	
	public void selecionarPropriedade_NovoEstab(Propriedade propriedade){
		this.novoEstabelecimentoParaInvestigacao.setPropriedade(propriedade);
		FacesMessageUtil.addInfoContextFacesMessage("Propriedade " + this.novoEstabelecimentoParaInvestigacao.getPropriedade().getNome() + " selecionada", "");
	}
	
	public void abrirTelaDeCadastroDePropriedade(){
		this.propriedade = new Propriedade();
	}
	
	public void cadastrarPropriedade_NovoEstab(){
		this.propriedade.setMunicipio(this.municipio);
		this.novoEstabelecimentoParaInvestigacao.setPropriedade(this.propriedade);
		this.propriedade = null;
		this.municipio = null;
		this.uf = null;
		
		FacesMessageUtil.addInfoContextFacesMessage("Propriedade inclu�da com sucesso", "");
	}
	
	public void abrirTelaDeBuscaDeVeterinario(){
		this.cpfVeterinario = null;
		this.crmvVeterinario = null;
		this.listaVeterinarios = null;
	}
	
	public void buscarVeterinarioPorCpf(){
		this.listaVeterinarios = null;
		
		Veterinario v;
		try {
			v = clientWebServiceVeterinario.getVeterinarioByCpf(cpfVeterinario);
			
			if (v != null){
				this.listaVeterinarios = new ArrayList<Veterinario>();
				this.listaVeterinarios.add(v);
			}
		} catch (ApplicationException e) {
			FacesMessageUtil.addErrorContextFacesMessage("Erro", e.getLocalizedMessage());
			return;
		}
		
		if (this.listaVeterinarios == null || this.listaVeterinarios.isEmpty())
			FacesMessageUtil.addWarnContextFacesMessage("Nenhum veterin�rio foi encontrado", "");
	}
	
	public void buscarVeterinarioPorCrmv(){
		this.listaVeterinarios = null;
		
		Veterinario v;
		try {
			v = clientWebServiceVeterinario.getVeterinarioByCRMV(crmvVeterinario);
			
			if (v != null){
				this.listaVeterinarios = new ArrayList<Veterinario>();
				this.listaVeterinarios.add(v);
			}
		} catch (ApplicationException e) {
			FacesMessageUtil.addErrorContextFacesMessage("Erro", e.getLocalizedMessage());
			return;
		}
		
		if (this.listaVeterinarios == null || this.listaVeterinarios.isEmpty())
			FacesMessageUtil.addWarnContextFacesMessage("Nenhum veterin�rio foi encontrado", "");
	}
	
	public void selecionarVeterinario(Veterinario veterinario){
		this.formVIN.setVeterinario(veterinario);
		FacesMessageUtil.addInfoContextFacesMessage("Veterinario " + this.formVIN.getVeterinario().getNome() + " selecionado", "");
	}
	
	public List<Produtor> getListaProdutoresDaPropriedade(){
		HashSet<Produtor> set = new HashSet<Produtor>();
		
		if (this.formVIN.getPropriedade() != null){
			if (this.formVIN.getPropriedade().getProprietarios() != null)
				set.addAll(this.formVIN.getPropriedade().getProprietarios());
			
			if (this.formVIN.getPropriedade().getExploracaos() != null)
				for (int i = 0; i < this.formVIN.getPropriedade().getExploracaos().size(); i++) {
					if (this.formVIN.getPropriedade().getExploracaos().get(i).getProdutores() != null)
						set.addAll(this.formVIN.getPropriedade().getExploracaos().get(i).getProdutores());
				}
		}
		
		List<Produtor> lista = new ArrayList<Produtor>(set);
		return lista;
	}
	
	public void selecionarProdutor(Produtor produtor) throws CloneNotSupportedException{
		this.formVIN.setProprietario((Produtor) produtor.clone());
		this.formVIN.getRepresentanteEstabelecimento().setNome(this.formVIN.getProprietario().getNome());
		
		if (this.formVIN.getProprietario().getEndereco() != null)
			this.formVIN.getRepresentanteEstabelecimento().setTelefoneFixo(this.formVIN.getProprietario().getEndereco().getTelefone());
		
		FacesMessageUtil.addInfoContextFacesMessage("Propriet�rio " + this.formVIN.getProprietario().getNome() + " selecionado", "");
	}

	public void abrirTelaDeBuscaDeAbatedouro(){
		this.cnpjAbatedouro = null;
		this.listaAbatedouros = null;
	}
	
	public void buscarAbatedouroPorCNPJ(){
		this.listaAbatedouros = null;
		
		try {
			Abatedouro abatedouro = clientWebServiceAbatedouro.getAbatedouroByCNPJ(cnpjAbatedouro);
			if (abatedouro != null){
				this.listaAbatedouros = new ArrayList<Abatedouro>();
				this.listaAbatedouros.add(abatedouro);
			}
		} catch (ApplicationException e) {
			FacesMessageUtil.addErrorContextFacesMessage("Erro", e.getLocalizedMessage());
			return;
		}
		
		if (this.listaAbatedouros == null || this.listaAbatedouros.isEmpty())
			FacesMessageUtil.addInfoContextFacesMessage("Nenhum abatedouro foi encontrada", "");
	}
	
	public void selecionarAbatedouro(Abatedouro abatedouro){
		this.formVIN.setAbatedouro(abatedouro);
		
		FacesMessageUtil.addInfoContextFacesMessage("Abatedouro " + this.formVIN.getAbatedouro().getNome() + " selecionada", "");
	}
	
	public boolean isTipoEstabelecimentoIgualPropriedade(){
		boolean result = false;
		
		if (this.tipoEstabelecimento != null){
			result = this.tipoEstabelecimento.equals(TipoEstabelecimento.PROPRIEDADE);
			
			if (result)
				this.formVIN.setAbatedouro(null);
			else
				this.formVIN.setPropriedade(null);
		}
		
		return result;
	}
	
	public FormVIN getFormVIN() {
		return formVIN;
	}

	public void setFormVIN(FormVIN formVIN) {
		this.formVIN = formVIN;
	}

	public VistoriaFormVIN getVistoriaFormVIN() {
		return vistoriaFormVIN;
	}

	public void setVistoriaFormVIN(VistoriaFormVIN vistoria) {
		this.vistoriaFormVIN = vistoria;
	}

	public boolean isEditandoVistoriaFormVIN() {
		return editandoVistoriaFormVIN;
	}

	public void setEditandoVistoriaFormVIN(boolean editandoVistoriaFormVIN) {
		this.editandoVistoriaFormVIN = editandoVistoriaFormVIN;
	}

	public boolean isVisualizandoVistoriaFormVIN() {
		return visualizandoVistoriaFormVIN;
	}

	public void setVisualizandoVistoriaFormVIN(boolean visualizandoVistoriaFormVIN) {
		this.visualizandoVistoriaFormVIN = visualizandoVistoriaFormVIN;
	}

	public boolean isEditandoNovoEstabelecimento() {
		return editandoNovoEstabelecimento;
	}

	public void setEditandoNovoEstabelecimento(boolean editandoNovoEstabelecimento) {
		this.editandoNovoEstabelecimento = editandoNovoEstabelecimento;
	}

	public boolean isVisualizandoNovoEstabelecimento() {
		return visualizandoNovoEstabelecimento;
	}

	public void setVisualizandoNovoEstabelecimento(
			boolean visualizandoNovoEstabelecimento) {
		this.visualizandoNovoEstabelecimento = visualizandoNovoEstabelecimento;
	}

	public Date getDataInspecao() {
		return dataInspecao;
	}

	public void setDataInspecao(Date dataInspecao) {
		this.dataInspecao = dataInspecao;
	}

	public Long getIdPropriedade() {
		return idPropriedade;
	}

	public void setIdPropriedade(Long idPropriedade) {
		this.idPropriedade = idPropriedade;
	}

	public List<Propriedade> getListaPropriedades() {
		return listaPropriedades;
	}

	public void setListaPropriedades(List<Propriedade> listaPropriedades) {
		this.listaPropriedades = listaPropriedades;
	}

	public String getCpfVeterinario() {
		return cpfVeterinario;
	}

	public void setCpfVeterinario(String cpfVeterinario) {
		this.cpfVeterinario = cpfVeterinario;
	}

	public String getCrmvVeterinario() {
		return crmvVeterinario;
	}

	public void setCrmvVeterinario(String crmvVeterinario) {
		this.crmvVeterinario = crmvVeterinario;
	}

	public List<Veterinario> getListaVeterinarios() {
		return listaVeterinarios;
	}

	public void setListaVeterinarios(List<Veterinario> listaVeterinarios) {
		this.listaVeterinarios = listaVeterinarios;
	}

	public NovoEstabelecimentoParaInvestigacao getNovoEstabelecimentosParaInvestigacao() {
		return novoEstabelecimentoParaInvestigacao;
	}

	public void setNovoEstabelecimentosParaInvestigacao(
			NovoEstabelecimentoParaInvestigacao novoEstabelecimentosParaInvestigacao) {
		this.novoEstabelecimentoParaInvestigacao = novoEstabelecimentosParaInvestigacao;
	}

	public Propriedade getPropriedade() {
		return propriedade;
	}

	public void setPropriedade(Propriedade propriedade) {
		this.propriedade = propriedade;
	}

	public TipoCoordenadaGeografica getTipoCoordenadaGeografica() {
		return tipoCoordenadaGeografica;
	}

	public void setTipoCoordenadaGeografica(
			TipoCoordenadaGeografica tipoCoordenadaGeografica) {
		this.tipoCoordenadaGeografica = tipoCoordenadaGeografica;
	}

	public Municipio getMunicipio() {
		return municipio;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}

	public UF getUf() {
		return uf;
	}

	public void setUf(UF uf) {
		this.uf = uf;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public FormIN getFormIN() {
		return formIN;
	}

	public void setFormIN(FormIN formIN) {
		this.formIN = formIN;
	}

	public Long getIdFormIN() {
		return idFormIN;
	}

	public void setIdFormIN(Long idFormIN) {
		this.idFormIN = idFormIN;
	}

	public TipoEstabelecimento getTipoEstabelecimento() {
		return tipoEstabelecimento;
	}

	public void setTipoEstabelecimento(TipoEstabelecimento tipoEstabelecimento) {
		this.tipoEstabelecimento = tipoEstabelecimento;
	}

	public String getCnpjAbatedouro() {
		return cnpjAbatedouro;
	}

	public void setCnpjAbatedouro(String cnpjAbatedouro) {
		this.cnpjAbatedouro = cnpjAbatedouro;
	}

	public List<Abatedouro> getListaAbatedouros() {
		return listaAbatedouros;
	}

	public void setListaAbatedouros(List<Abatedouro> listaAbatedouros) {
		this.listaAbatedouros = listaAbatedouros;
	}

	public boolean isForceUpdatePropriedade() {
		return forceUpdatePropriedade;
	}

	public void setForceUpdatePropriedade(boolean forceUpdatePropriedade) {
		this.forceUpdatePropriedade = forceUpdatePropriedade;
	}
	
	

}
