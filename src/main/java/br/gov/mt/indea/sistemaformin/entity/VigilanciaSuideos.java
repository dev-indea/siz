package br.gov.mt.indea.sistemaformin.entity;

import java.util.Calendar;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

import br.gov.mt.indea.sistemaformin.enums.Dominio.FinalidadeExploracaoSuinos;
import br.gov.mt.indea.sistemaformin.enums.Dominio.SimNao;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoAlimentoSuideos;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoDestinoCadaveres;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoDestinoSuideos;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoOrigemSuideos;

@Audited
@Entity(name="vigilancia_suideos")
public class VigilanciaSuideos extends BaseEntity<Long>{
	
	private static final long serialVersionUID = -361785479572430083L;

	@Id
	@SequenceGenerator(name="vigilancia_suideos_seq", sequenceName="vigilancia_suideos_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="vigilancia_suideos_seq")
	private Long id;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_cadastro")
	private Calendar dataCadastro;
	
	
	@Column(name="quantidade_inspec_Ate_30_f")
	private Integer quantidadeInspecionadosAte_30_F;
	
	@Column(name="quantidade_inspec_Ate_30_m")
	private Integer quantidadeInspecionadosAte_30_M;
	
	@Column(name="quantidade_inspec_31_60_f")
	private Integer quantidadeInspecionados31_60_F;
	
	@Column(name="quantidade_inspec_31_60_m")
	private Integer quantidadeInspecionados31_60_M;
	
	@Column(name="quantidade_inspec_61_180_f")
	private Integer quantidadeInspecionados61_180_F;
	
	@Column(name="quantidade_inspec_61_180_m")
	private Integer quantidadeInspecionados61_180_M;
	
	@Column(name="quantidade_inspec_acima180_f")
	private Integer quantidadeInspecionadosAcima180_F;
	
	@Column(name="quantidade_inspec_acima180_m")
	private Integer quantidadeInspecionadosAcima180_M;
	
	@Column(name="quantidade_vistoriados")
	private Integer quantidadeVistoriados;
	
	@Enumerated(EnumType.STRING)
	@Column(name="presenca_suinos_asselvajados")
	private SimNao presencaDeSuinosAsselvajados;
	
	@Enumerated(EnumType.STRING)
	@Column(name="contato_suinos_domest_e_assel")
	private SimNao contatoDiretoDeSuinosDomesticosComAsselvajados;
	
	@Enumerated(EnumType.STRING)
	@Column(name="prop_prox_comunidade_carente")
	private SimNao propriedadeProximaComunidadesCarentes;
	
	@Enumerated(EnumType.STRING)
	@Column(name="prop_prox_quarentenario")
	private SimNao propriedadeProximaQuarentenarioDeSuideos;
	
	@Enumerated(EnumType.STRING)
	@Column(name="prop_suideos_criados_extensiv")
	private SimNao propriedadeComSuideosCriadosExtensivamente;
	
	@Enumerated(EnumType.STRING)
	@Column(name="prop_com_lavagem_de_terceiros")
	private SimNao propriedadeForneceLavagemDeTerceirosAosSuideos;
	
	@Enumerated(EnumType.STRING)
	@Column(name="dono_tem_prop_em_area_endem")
	private SimNao proprietarioPossuiPropriedadeEmOutraAreaInfectadaOuEndemica;
	
	@Enumerated(EnumType.STRING)
	@Column(name="finalidade_explocarao_suinos")
	private FinalidadeExploracaoSuinos finalidadeExploracaoSuinos;
	
	@ElementCollection(targetClass = TipoAlimentoSuideos.class, fetch=FetchType.LAZY)
	@JoinTable(name = "vig_suideos_tipo_alimento", joinColumns = @JoinColumn(name = "id_vigilancia_suideos", nullable = false))
	@Column(name = "id_vig_suideos_tipo_alimento")
	@Enumerated(EnumType.STRING)
	private List<TipoAlimentoSuideos> listaTipoAlimentoSuideos;
	
	@Column(name="outra_alimentacao_suideos", length=200)
	private String outraAlimentacaoSuideos;
	
	@ElementCollection(targetClass = TipoOrigemSuideos.class, fetch=FetchType.LAZY)
	@JoinTable(name = "vig_suideos_tipo_origem", joinColumns = @JoinColumn(name = "id_vigilancia_suideos", nullable = false))
	@Column(name = "id_vig_suideos_tipo_origem")
	@Enumerated(EnumType.STRING)
	private List<TipoOrigemSuideos> listaTipoOrigemSuideos;
	
	@Column(name="outra_origem_suideos", length=200)
	private String outraOrigemSuideos;
	
	@Enumerated(EnumType.STRING)
	@Column(name="tipo_destino_suideos")
	private TipoDestinoSuideos tipoDestinoSuideos;
	
	@Column(name="outro_destino_suideos", length=200)
	private String outroDestinoSuideos;
	
	@Enumerated(EnumType.STRING)
	@Column(name="tipo_destino_cadaveres")
	private TipoDestinoCadaveres tipoDestinoCadaveres;
	
	@Enumerated(EnumType.STRING)
	@Column(name="hipertermia")
	private SimNao animaisComHipertermia;
	
	@Column(name="porc_hipertermia")
	private Integer porcAnimaisComHipertermia;
	
	@Enumerated(EnumType.STRING)
	@Column(name="incoordenacao_motora")
	private SimNao animaisComIncoordenacaoMotora;
	
	@Column(name="porc_incoordenacao_motora")
	private Integer porcAnimaisComIncoordenacaoMotora;
	
	@Enumerated(EnumType.STRING)
	@Column(name="extremidades_cianoticas")
	private SimNao animaisComExtremidadesCianoticas;
	
	@Column(name="porc_extremidades_cianoticas")
	private Integer porcAnimaisComExtremidadesCianoticas;
	
	@Enumerated(EnumType.STRING)
	@Column(name="vomitos")
	private SimNao animaisComVomitos;
	
	@Column(name="porc_vomitos")
	private Integer porcAnimaisComVomitos;
	
	@Enumerated(EnumType.STRING)
	@Column(name="diarreia")
	private SimNao animaisComDiarreia;
	
	@Column(name="porc_diarreia")
	private Integer porcAnimaisComDiarreia;
	
	@Enumerated(EnumType.STRING)
	@Column(name="anorexia")
	private SimNao animaisComAnorexia;
	
	@Column(name="porc_anorexia")
	private Integer porcAnimaisComAnorexia;
	
	@Enumerated(EnumType.STRING)
	@Column(name="abortos")
	private SimNao animaisComAbortos;
	
	@Column(name="porc_abortos")
	private Integer porcAnimaisComAbortos;
	
	@Enumerated(EnumType.STRING)
	@Column(name="leitoes_natimortos")
	private SimNao animaisComLeitoesNatimortos;
	
	@Column(name="porc_leitoes_natimortos")
	private Integer porcAnimaisComNatimortos;
	
	@Enumerated(EnumType.STRING)
	@Column(name="agrupamentos_nas_pocilgas")
	private SimNao animaisComAgrupamentoNasPocilgas;
	
	@Column(name="porc_agrupamentos_nas_pocilgas")
	private Integer porcAnimaisComAgrupamentoNasPocilgas;
	
	@Enumerated(EnumType.STRING)
	@Column(name="hemorragias_e_petequias")
	private SimNao animaisComHemorragiasEPetequiasNasMucosas;
	
	@Column(name="porc_hemorragias_e_petequias")
	private Integer porcAnimaisComHemorragiasEPetequiasNasMucosas;
	
	@Enumerated(EnumType.STRING)
	@Column(name="cresc_retardado_de_leitoes")
	private SimNao animaisComCrescimentoRetardadoDeLeitoes;
	
	@Column(name="porc_cresc_retardado_de_leitoes")
	private Integer porcAnimaisComCrescimentoRetardadoDeLeitoes;
	
	@Enumerated(EnumType.STRING)
	@Column(name="mortalidade_mensal")
	private SimNao animaisComMortalidadeMensal;
	
	@Column(name="porc_mortalidade_mensal")
	private Integer porcAnimaisComMortalidadeMensal;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Calendar getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Calendar dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Integer getQuantidadeInspecionadosAte_30_F() {
		return quantidadeInspecionadosAte_30_F;
	}

	public void setQuantidadeInspecionadosAte_30_F(
			Integer quantidadeInspecionadosAte_30_F) {
		this.quantidadeInspecionadosAte_30_F = quantidadeInspecionadosAte_30_F;
	}

	public Integer getQuantidadeInspecionadosAte_30_M() {
		return quantidadeInspecionadosAte_30_M;
	}

	public void setQuantidadeInspecionadosAte_30_M(
			Integer quantidadeInspecionadosAte_30_M) {
		this.quantidadeInspecionadosAte_30_M = quantidadeInspecionadosAte_30_M;
	}

	public Integer getQuantidadeInspecionados31_60_F() {
		return quantidadeInspecionados31_60_F;
	}

	public void setQuantidadeInspecionados31_60_F(
			Integer quantidadeInspecionados31_60_F) {
		this.quantidadeInspecionados31_60_F = quantidadeInspecionados31_60_F;
	}

	public Integer getQuantidadeInspecionados31_60_M() {
		return quantidadeInspecionados31_60_M;
	}

	public void setQuantidadeInspecionados31_60_M(
			Integer quantidadeInspecionados31_60_M) {
		this.quantidadeInspecionados31_60_M = quantidadeInspecionados31_60_M;
	}

	public Integer getQuantidadeInspecionados61_180_F() {
		return quantidadeInspecionados61_180_F;
	}

	public void setQuantidadeInspecionados61_180_F(
			Integer quantidadeInspecionados61_180_F) {
		this.quantidadeInspecionados61_180_F = quantidadeInspecionados61_180_F;
	}

	public Integer getQuantidadeInspecionados61_180_M() {
		return quantidadeInspecionados61_180_M;
	}

	public void setQuantidadeInspecionados61_180_M(
			Integer quantidadeInspecionados61_180_M) {
		this.quantidadeInspecionados61_180_M = quantidadeInspecionados61_180_M;
	}

	public Integer getQuantidadeInspecionadosAcima180_F() {
		return quantidadeInspecionadosAcima180_F;
	}

	public void setQuantidadeInspecionadosAcima180_F(
			Integer quantidadeInspecionadosAcima180_F) {
		this.quantidadeInspecionadosAcima180_F = quantidadeInspecionadosAcima180_F;
	}

	public Integer getQuantidadeInspecionadosAcima180_M() {
		return quantidadeInspecionadosAcima180_M;
	}

	public void setQuantidadeInspecionadosAcima180_M(
			Integer quantidadeInspecionadosAcima180_M) {
		this.quantidadeInspecionadosAcima180_M = quantidadeInspecionadosAcima180_M;
	}

	public SimNao getPresencaDeSuinosAsselvajados() {
		return presencaDeSuinosAsselvajados;
	}

	public void setPresencaDeSuinosAsselvajados(SimNao presencaDeSuinosAsselvajados) {
		this.presencaDeSuinosAsselvajados = presencaDeSuinosAsselvajados;
	}

	public SimNao getContatoDiretoDeSuinosDomesticosComAsselvajados() {
		return contatoDiretoDeSuinosDomesticosComAsselvajados;
	}

	public void setContatoDiretoDeSuinosDomesticosComAsselvajados(
			SimNao contatoDiretoDeSuinosDomesticosComAsselvajados) {
		this.contatoDiretoDeSuinosDomesticosComAsselvajados = contatoDiretoDeSuinosDomesticosComAsselvajados;
	}

	public SimNao getPropriedadeProximaComunidadesCarentes() {
		return propriedadeProximaComunidadesCarentes;
	}

	public void setPropriedadeProximaComunidadesCarentes(
			SimNao propriedadeProximaComunidadesCarentes) {
		this.propriedadeProximaComunidadesCarentes = propriedadeProximaComunidadesCarentes;
	}

	public SimNao getPropriedadeProximaQuarentenarioDeSuideos() {
		return propriedadeProximaQuarentenarioDeSuideos;
	}

	public void setPropriedadeProximaQuarentenarioDeSuideos(
			SimNao propriedadeProximaQuarentenarioDeSuideos) {
		this.propriedadeProximaQuarentenarioDeSuideos = propriedadeProximaQuarentenarioDeSuideos;
	}

	public SimNao getPropriedadeComSuideosCriadosExtensivamente() {
		return propriedadeComSuideosCriadosExtensivamente;
	}

	public void setPropriedadeComSuideosCriadosExtensivamente(
			SimNao propriedadeComSuideosCriadosExtensivamente) {
		this.propriedadeComSuideosCriadosExtensivamente = propriedadeComSuideosCriadosExtensivamente;
	}

	public SimNao getPropriedadeForneceLavagemDeTerceirosAosSuideos() {
		return propriedadeForneceLavagemDeTerceirosAosSuideos;
	}

	public void setPropriedadeForneceLavagemDeTerceirosAosSuideos(
			SimNao propriedadeForneceLavagemDeTerceirosAosSuideos) {
		this.propriedadeForneceLavagemDeTerceirosAosSuideos = propriedadeForneceLavagemDeTerceirosAosSuideos;
	}

	public SimNao getProprietarioPossuiPropriedadeEmOutraAreaInfectadaOuEndemica() {
		return proprietarioPossuiPropriedadeEmOutraAreaInfectadaOuEndemica;
	}

	public void setProprietarioPossuiPropriedadeEmOutraAreaInfectadaOuEndemica(
			SimNao proprietarioPossuiPropriedadeEmOutraAreaInfectadaOuEndemica) {
		this.proprietarioPossuiPropriedadeEmOutraAreaInfectadaOuEndemica = proprietarioPossuiPropriedadeEmOutraAreaInfectadaOuEndemica;
	}

	public FinalidadeExploracaoSuinos getFinalidadeExploracaoSuinos() {
		return finalidadeExploracaoSuinos;
	}

	public void setFinalidadeExploracaoSuinos(
			FinalidadeExploracaoSuinos finalidadeExploracaoSuinos) {
		this.finalidadeExploracaoSuinos = finalidadeExploracaoSuinos;
	}

	public String getOutraOrigemSuideos() {
		return outraOrigemSuideos;
	}

	public void setOutraOrigemSuideos(String outraOrigemSuideos) {
		this.outraOrigemSuideos = outraOrigemSuideos;
	}

	public TipoDestinoSuideos getTipoDestinoSuideos() {
		return tipoDestinoSuideos;
	}

	public void setTipoDestinoSuideos(TipoDestinoSuideos tipoDestinoSuideos) {
		this.tipoDestinoSuideos = tipoDestinoSuideos;
	}

	public String getOutroDestinoSuideos() {
		return outroDestinoSuideos;
	}

	public void setOutroDestinoSuideos(String outroDestinoSuideos) {
		this.outroDestinoSuideos = outroDestinoSuideos;
	}

	public TipoDestinoCadaveres getTipoDestinoCadaveres() {
		return tipoDestinoCadaveres;
	}

	public void setTipoDestinoCadaveres(TipoDestinoCadaveres tipoDestinoCadaveres) {
		this.tipoDestinoCadaveres = tipoDestinoCadaveres;
	}

	public SimNao getAnimaisComHipertermia() {
		return animaisComHipertermia;
	}

	public void setAnimaisComHipertermia(SimNao animaisComHipertermia) {
		this.animaisComHipertermia = animaisComHipertermia;
	}

	public Integer getPorcAnimaisComHipertermia() {
		return porcAnimaisComHipertermia;
	}

	public void setPorcAnimaisComHipertermia(Integer porcAnimaisComHipertermia) {
		this.porcAnimaisComHipertermia = porcAnimaisComHipertermia;
	}

	public SimNao getAnimaisComIncoordenacaoMotora() {
		return animaisComIncoordenacaoMotora;
	}

	public void setAnimaisComIncoordenacaoMotora(
			SimNao animaisComIncoordenacaoMotora) {
		this.animaisComIncoordenacaoMotora = animaisComIncoordenacaoMotora;
	}

	public Integer getPorcAnimaisComIncoordenacaoMotora() {
		return porcAnimaisComIncoordenacaoMotora;
	}

	public void setPorcAnimaisComIncoordenacaoMotora(
			Integer porcAnimaisComIncoordenacaoMotora) {
		this.porcAnimaisComIncoordenacaoMotora = porcAnimaisComIncoordenacaoMotora;
	}

	public SimNao getAnimaisComExtremidadesCianoticas() {
		return animaisComExtremidadesCianoticas;
	}

	public void setAnimaisComExtremidadesCianoticas(
			SimNao animaisComExtremidadesCianoticas) {
		this.animaisComExtremidadesCianoticas = animaisComExtremidadesCianoticas;
	}

	public Integer getPorcAnimaisComExtremidadesCianoticas() {
		return porcAnimaisComExtremidadesCianoticas;
	}

	public void setPorcAnimaisComExtremidadesCianoticas(
			Integer porcAnimaisComExtremidadesCianoticas) {
		this.porcAnimaisComExtremidadesCianoticas = porcAnimaisComExtremidadesCianoticas;
	}

	public SimNao getAnimaisComVomitos() {
		return animaisComVomitos;
	}

	public void setAnimaisComVomitos(SimNao animaisComVomitos) {
		this.animaisComVomitos = animaisComVomitos;
	}

	public Integer getPorcAnimaisComVomitos() {
		return porcAnimaisComVomitos;
	}

	public void setPorcAnimaisComVomitos(Integer porcAnimaisComVomitos) {
		this.porcAnimaisComVomitos = porcAnimaisComVomitos;
	}

	public SimNao getAnimaisComDiarreia() {
		return animaisComDiarreia;
	}

	public void setAnimaisComDiarreia(SimNao animaisComDiarreia) {
		this.animaisComDiarreia = animaisComDiarreia;
	}

	public Integer getPorcAnimaisComDiarreia() {
		return porcAnimaisComDiarreia;
	}

	public void setPorcAnimaisComDiarreia(Integer porcAnimaisComDiarreia) {
		this.porcAnimaisComDiarreia = porcAnimaisComDiarreia;
	}

	public SimNao getAnimaisComAnorexia() {
		return animaisComAnorexia;
	}

	public void setAnimaisComAnorexia(SimNao animaisComAnorexia) {
		this.animaisComAnorexia = animaisComAnorexia;
	}

	public Integer getPorcAnimaisComAnorexia() {
		return porcAnimaisComAnorexia;
	}

	public void setPorcAnimaisComAnorexia(Integer porcAnimaisComAnorexia) {
		this.porcAnimaisComAnorexia = porcAnimaisComAnorexia;
	}

	public SimNao getAnimaisComAbortos() {
		return animaisComAbortos;
	}

	public void setAnimaisComAbortos(SimNao animaisComAbortos) {
		this.animaisComAbortos = animaisComAbortos;
	}

	public Integer getPorcAnimaisComAbortos() {
		return porcAnimaisComAbortos;
	}

	public void setPorcAnimaisComAbortos(Integer porcAnimaisComAbortos) {
		this.porcAnimaisComAbortos = porcAnimaisComAbortos;
	}

	public SimNao getAnimaisComLeitoesNatimortos() {
		return animaisComLeitoesNatimortos;
	}

	public void setAnimaisComLeitoesNatimortos(SimNao animaisComLeitoesNatimortos) {
		this.animaisComLeitoesNatimortos = animaisComLeitoesNatimortos;
	}

	public Integer getPorcAnimaisComNatimortos() {
		return porcAnimaisComNatimortos;
	}

	public void setPorcAnimaisComNatimortos(Integer porcAnimaisComNatimortos) {
		this.porcAnimaisComNatimortos = porcAnimaisComNatimortos;
	}

	public SimNao getAnimaisComAgrupamentoNasPocilgas() {
		return animaisComAgrupamentoNasPocilgas;
	}

	public void setAnimaisComAgrupamentoNasPocilgas(
			SimNao animaisComAgrupamentoNasPocilgas) {
		this.animaisComAgrupamentoNasPocilgas = animaisComAgrupamentoNasPocilgas;
	}

	public Integer getPorcAnimaisComAgrupamentoNasPocilgas() {
		return porcAnimaisComAgrupamentoNasPocilgas;
	}

	public void setPorcAnimaisComAgrupamentoNasPocilgas(
			Integer porcAnimaisComAgrupamentoNasPocilgas) {
		this.porcAnimaisComAgrupamentoNasPocilgas = porcAnimaisComAgrupamentoNasPocilgas;
	}

	public SimNao getAnimaisComHemorragiasEPetequiasNasMucosas() {
		return animaisComHemorragiasEPetequiasNasMucosas;
	}

	public void setAnimaisComHemorragiasEPetequiasNasMucosas(
			SimNao animaisComHemorragiasEPetequiasNasMucosas) {
		this.animaisComHemorragiasEPetequiasNasMucosas = animaisComHemorragiasEPetequiasNasMucosas;
	}

	public Integer getPorcAnimaisComHemorragiasEPetequiasNasMucosas() {
		return porcAnimaisComHemorragiasEPetequiasNasMucosas;
	}

	public void setPorcAnimaisComHemorragiasEPetequiasNasMucosas(
			Integer porcAnimaisComHemorragiasEPetequiasNasMucosas) {
		this.porcAnimaisComHemorragiasEPetequiasNasMucosas = porcAnimaisComHemorragiasEPetequiasNasMucosas;
	}

	public SimNao getAnimaisComCrescimentoRetardadoDeLeitoes() {
		return animaisComCrescimentoRetardadoDeLeitoes;
	}

	public void setAnimaisComCrescimentoRetardadoDeLeitoes(
			SimNao animaisComCrescimentoRetardadoDeLeitoes) {
		this.animaisComCrescimentoRetardadoDeLeitoes = animaisComCrescimentoRetardadoDeLeitoes;
	}

	public Integer getPorcAnimaisComCrescimentoRetardadoDeLeitoes() {
		return porcAnimaisComCrescimentoRetardadoDeLeitoes;
	}

	public void setPorcAnimaisComCrescimentoRetardadoDeLeitoes(
			Integer porcAnimaisComCrescimentoRetardadoDeLeitoes) {
		this.porcAnimaisComCrescimentoRetardadoDeLeitoes = porcAnimaisComCrescimentoRetardadoDeLeitoes;
	}

	public SimNao getAnimaisComMortalidadeMensal() {
		return animaisComMortalidadeMensal;
	}

	public void setAnimaisComMortalidadeMensal(SimNao animaisComMortalidadeMensal) {
		this.animaisComMortalidadeMensal = animaisComMortalidadeMensal;
	}

	public Integer getPorcAnimaisComMortalidadeMensal() {
		return porcAnimaisComMortalidadeMensal;
	}

	public void setPorcAnimaisComMortalidadeMensal(
			Integer porcAnimaisComMortalidadeMensal) {
		this.porcAnimaisComMortalidadeMensal = porcAnimaisComMortalidadeMensal;
	}

	public List<TipoAlimentoSuideos> getListaTipoAlimentoSuideos() {
		return listaTipoAlimentoSuideos;
	}

	public void setListaTipoAlimentoSuideos(
			List<TipoAlimentoSuideos> listaTipoAlimentoSuideos) {
		this.listaTipoAlimentoSuideos = listaTipoAlimentoSuideos;
	}

	public String getOutraAlimentacaoSuideos() {
		return outraAlimentacaoSuideos;
	}

	public void setOutraAlimentacaoSuideos(String outraAlimentacaoSuideos) {
		this.outraAlimentacaoSuideos = outraAlimentacaoSuideos;
	}

	public List<TipoOrigemSuideos> getListaTipoOrigemSuideos() {
		return listaTipoOrigemSuideos;
	}

	public void setListaTipoOrigemSuideos(
			List<TipoOrigemSuideos> listaTipoOrigemSuideos) {
		this.listaTipoOrigemSuideos = listaTipoOrigemSuideos;
	}

	public Integer getQuantidadeVistoriados() {
		return quantidadeVistoriados;
	}

	public void setQuantidadeVistoriados(Integer quantidadeVistoriados) {
		this.quantidadeVistoriados = quantidadeVistoriados;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VigilanciaSuideos other = (VigilanciaSuideos) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}