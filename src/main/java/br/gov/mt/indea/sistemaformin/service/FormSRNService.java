package br.gov.mt.indea.sistemaformin.service;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.mt.indea.sistemaformin.entity.FormSRN;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class FormSRNService extends PaginableService<FormSRN, Long> {

	private static final Logger log = LoggerFactory.getLogger(FormSRNService.class);
	
	protected FormSRNService() {
		super(FormSRN.class);
	}
	
	public FormSRN findByIdFetchAll(Long id){
		FormSRN formSRN;
		
		StringBuilder sql = new StringBuilder();
		sql.append("  from FormSRN formSRN ")
		   .append("  join fetch formSRN.formIN formin")
		   .append("  left join fetch formSRN.formCOM")
		   .append("  join fetch formin.propriedade propriedade")
		   .append("  join fetch formin.proprietario proprietario")
		   .append("  left join fetch proprietario.endereco")
		   .append("  join fetch propriedade.municipio")
		   .append("  join fetch propriedade.ule")
		   .append("  left join fetch propriedade.coordenadaGeografica ")
		   .append("  left join fetch formSRN.ufGranjaOuLocalDeOrigemDosAnimais")
		   .append("  left join fetch formSRN.municipioGranjaOuLocalDeOrigemDosAnimais")
		   .append("  left join fetch formSRN.ufIncubatorioDeOrigem")
		   .append("  left join fetch formSRN.municipioIncubatorioDeOrigem")
		   .append("  join fetch formSRN.veterinario vet")
		   .append("  join fetch vet.conselho")
		   .append("  join fetch vet.tipoEmitente")
		   .append("  left join fetch vet.ule uu")
		   .append("  left join fetch uu.urs")
		   .append("  left join fetch vet.endereco")
		   .append(" where formSRN.id = :id ");
		
		Query query = getSession().createQuery(sql.toString()).setLong("id", id);
		formSRN = (FormSRN) query.uniqueResult();
		
		if (formSRN != null){
		
			Hibernate.initialize(formSRN.getTipoAlimento());
			Hibernate.initialize(formSRN.getSinaisClinicosEstadoGeral());
			Hibernate.initialize(formSRN.getSinaisClinicosSistemaRespiratorio());
			Hibernate.initialize(formSRN.getSinaisClinicosSistemaNervoso());
			Hibernate.initialize(formSRN.getSinaisClinicosSistemaDigestorio());
			Hibernate.initialize(formSRN.getSinaisClinicosSistemaCirculatorio());
			Hibernate.initialize(formSRN.getNecropsiaEstadoGeral());
			Hibernate.initialize(formSRN.getNecropsiaSistemaRespiratorio());
			Hibernate.initialize(formSRN.getNecropsiaSistemaUrinario());
			Hibernate.initialize(formSRN.getNecropsiaSistemaCirculatorio());
			Hibernate.initialize(formSRN.getNecropsiaSistemaNervoso());
			Hibernate.initialize(formSRN.getNecropsiaSistemaDigestivo());
			
		}
		
		return formSRN;
	}
	
	@Override
	public void saveOrUpdate(FormSRN formSRN) {
		super.saveOrUpdate(formSRN);
		
		log.info("Salvando Form SRN {}", formSRN.getId());
	}
	
	@Override
	public void delete(FormSRN formSRN) {
		super.delete(formSRN);
		
		log.info("Removendo Form SRN {}", formSRN.getId());
	}
	
	@Override
	public void validar(FormSRN FormSRN) {

	}

	@Override
	public void validarPersist(FormSRN FormSRN) {

	}

	@Override
	public void validarMerge(FormSRN FormSRN) {

	}

	@Override
	public void validarDelete(FormSRN FormSRN) {

	}

}
