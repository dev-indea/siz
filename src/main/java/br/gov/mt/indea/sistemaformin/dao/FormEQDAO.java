package br.gov.mt.indea.sistemaformin.dao;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.inject.Inject;


import br.gov.mt.indea.sistemaformin.entity.FormEQ;
import br.gov.mt.indea.sistemaformin.exception.ApplicationException;

@Stateless
public class FormEQDAO extends DAO<FormEQ> {
	
	@Inject
	private EntityManager em;
	
	public FormEQDAO() {
		super(FormEQ.class);
	}

	@Override
	public EntityManager getEntityManager() {
		return this.em;
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void add(FormEQ formEQ) throws ApplicationException{
		formEQ = this.getEntityManager().merge(formEQ);
		if (formEQ.getFormCOM() != null)
			formEQ.setFormCOM(this.getEntityManager().merge(formEQ.getFormCOM()));
		super.add(formEQ);
	}

}
