package br.gov.mt.indea.sistemaformin.webservice.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Especie implements Serializable {

	private static final long serialVersionUID = -8466642547334193944L;

	private Long id;
    
    @XmlElement(name = "grupoEspecie")
    private GrupoEspecie grupoEspecie;
    
    private String nome;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public GrupoEspecie getGrupoEspecie() {
        return grupoEspecie;
    }

    public void setGrupoEspecie(GrupoEspecie grupoEspecie) {
        this.grupoEspecie = grupoEspecie;
    }

    public String getNome() {
        return nome;
    }

    public void setEsp_nome(String nome) {
        this.nome = nome;
    }
    
}
