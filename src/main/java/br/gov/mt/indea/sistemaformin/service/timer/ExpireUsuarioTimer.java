package br.gov.mt.indea.sistemaformin.service.timer;

import java.time.Duration;
import java.time.Instant;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Timer;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.mt.indea.sistemaformin.entity.ParametrosSIZ;
import br.gov.mt.indea.sistemaformin.entity.Usuario;
import br.gov.mt.indea.sistemaformin.enums.Dominio.AtivoInativo;
import br.gov.mt.indea.sistemaformin.service.ParametrosSIZService;
import br.gov.mt.indea.sistemaformin.service.UsuarioService;

@Singleton
@Startup
@LocalBean
public class ExpireUsuarioTimer {

	private static final Logger log = LoggerFactory.getLogger(ExpireUsuarioTimer.class);

	@Inject
	private UsuarioService usuarioService;
	
	@Inject
	private ParametrosSIZService parametrosSIZService;

	@Schedule(hour = "8", minute = "0", second = "0")
	public void execute(Timer timer) {
		log.info("Iniciando timer"); 
		ParametrosSIZ parametrosSIZ = parametrosSIZService.getInstance();

		List<Usuario> lista = usuarioService.findAll();

		if (lista != null) {
			Instant now = Instant.now();
			Instant before = null;
			Duration duration = null;

			for (Usuario usuario : lista) {
				before = usuario.getUltimoLogin().toInstant();

				duration = Duration.between(before, now);

				if (duration.toDays() > parametrosSIZ.getDiasParaExpiracaoDeUsuario()) {
					usuario.setStatus(AtivoInativo.EXPIRADO);

					usuarioService.saveOrUpdate(usuario);
				}
			}
		}

		log.info("Finalizando timer");
	}

}