package br.gov.mt.indea.sistemaformin.webservice.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class TipoExploracaoEquideo implements Serializable {

	private static final long serialVersionUID = -3836116915692312580L;

	private Long id;
    
	private String nome;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


	public String getNome() {
		return nome.replaceAll("  ", "");
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

}