package br.gov.mt.indea.sistemaformin.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import org.hibernate.envers.Audited;

@Audited
@Entity
@DiscriminatorValue("UNIVERSIDADE")
public class Universidade extends Pessoa implements Cloneable {

	private static final long serialVersionUID = -7571727456856230547L;
	
}
