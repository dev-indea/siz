package br.gov.mt.indea.sistemaformin.managedbeans;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.gov.mt.indea.sistemaformin.entity.Pais;
import br.gov.mt.indea.sistemaformin.entity.UF;
import br.gov.mt.indea.sistemaformin.enums.Dominio.AcompanhaNaoAcompanha;
import br.gov.mt.indea.sistemaformin.enums.Dominio.AreaAtuacaoMedicoVeterinario;
import br.gov.mt.indea.sistemaformin.enums.Dominio.AtivaPassiva;
import br.gov.mt.indea.sistemaformin.enums.Dominio.AtivoInativo;
import br.gov.mt.indea.sistemaformin.enums.Dominio.AtivoPassivo;
import br.gov.mt.indea.sistemaformin.enums.Dominio.CategoriaAnimal;
import br.gov.mt.indea.sistemaformin.enums.Dominio.CategoriaAves;
import br.gov.mt.indea.sistemaformin.enums.Dominio.CategoriaDoAnimalSubmetidoAVigilancia;
import br.gov.mt.indea.sistemaformin.enums.Dominio.CondicaoMaterial;
import br.gov.mt.indea.sistemaformin.enums.Dominio.DireitaEsquerda;
import br.gov.mt.indea.sistemaformin.enums.Dominio.DireitoEsquerdo;
import br.gov.mt.indea.sistemaformin.enums.Dominio.DoencasRespiratorias;
import br.gov.mt.indea.sistemaformin.enums.Dominio.Especie;
import br.gov.mt.indea.sistemaformin.enums.Dominio.FaixaEtariaOuEspecie;
import br.gov.mt.indea.sistemaformin.enums.Dominio.FaseExploracaoBovinosEBubalinos;
import br.gov.mt.indea.sistemaformin.enums.Dominio.FaseExploracaoCaprinos;
import br.gov.mt.indea.sistemaformin.enums.Dominio.FaseExploracaoOvinos;
import br.gov.mt.indea.sistemaformin.enums.Dominio.FinalidadeDoTesteDeLaboratorio;
import br.gov.mt.indea.sistemaformin.enums.Dominio.FinalidadeExploracaoBovinosEBubalinos;
import br.gov.mt.indea.sistemaformin.enums.Dominio.FinalidadeExploracaoCaprinos;
import br.gov.mt.indea.sistemaformin.enums.Dominio.FinalidadeExploracaoOvinos;
import br.gov.mt.indea.sistemaformin.enums.Dominio.FinalidadeExploracaoSuinos;
import br.gov.mt.indea.sistemaformin.enums.Dominio.FonteDaNotificacao;
import br.gov.mt.indea.sistemaformin.enums.Dominio.FuncaoNoEstabelecimento;
import br.gov.mt.indea.sistemaformin.enums.Dominio.LocalDeColhimentoDeAmostra;
import br.gov.mt.indea.sistemaformin.enums.Dominio.LocalizacaoCorrimentoNasal;
import br.gov.mt.indea.sistemaformin.enums.Dominio.MachoFemea;
import br.gov.mt.indea.sistemaformin.enums.Dominio.ManejoDosAnimais;
import br.gov.mt.indea.sistemaformin.enums.Dominio.MedidasAdotadasNoEstabelecimento;
import br.gov.mt.indea.sistemaformin.enums.Dominio.MeioConservacaoAmostra;
import br.gov.mt.indea.sistemaformin.enums.Dominio.Mes;
import br.gov.mt.indea.sistemaformin.enums.Dominio.MetodoParaEstipularIdade;
import br.gov.mt.indea.sistemaformin.enums.Dominio.MomentoMedidasAdotadasNoEstabelecimento;
import br.gov.mt.indea.sistemaformin.enums.Dominio.MotivacaoParaAbateDeEmergenciaEmFrigorifico;
import br.gov.mt.indea.sistemaformin.enums.Dominio.MotivoInicialDaInvestigacao;
import br.gov.mt.indea.sistemaformin.enums.Dominio.OrigemDoAnimal;
import br.gov.mt.indea.sistemaformin.enums.Dominio.PontosDeRisco;
import br.gov.mt.indea.sistemaformin.enums.Dominio.PositivoNegativo;
import br.gov.mt.indea.sistemaformin.enums.Dominio.ProvavelOrigemFormCOM;
import br.gov.mt.indea.sistemaformin.enums.Dominio.ResultadoNecropsiaDeAves;
import br.gov.mt.indea.sistemaformin.enums.Dominio.ResultadoRelatorioDeEnsaio;
import br.gov.mt.indea.sistemaformin.enums.Dominio.Sexo;
import br.gov.mt.indea.sistemaformin.enums.Dominio.SimNao;
import br.gov.mt.indea.sistemaformin.enums.Dominio.SimNaoNA;
import br.gov.mt.indea.sistemaformin.enums.Dominio.SimNaoSI;
import br.gov.mt.indea.sistemaformin.enums.Dominio.SinaisClinicosCavidadeNasal;
import br.gov.mt.indea.sistemaformin.enums.Dominio.SinaisClinicosExameMaleina;
import br.gov.mt.indea.sistemaformin.enums.Dominio.SinaisClinicosNervososEncefalopatoaEspongiformeTransmissivel;
import br.gov.mt.indea.sistemaformin.enums.Dominio.SinaisClinicosPele;
import br.gov.mt.indea.sistemaformin.enums.Dominio.SinaisClinicosSindromeHemorragica;
import br.gov.mt.indea.sistemaformin.enums.Dominio.SinaisClinicosSindromeNervosaERespiratoriaDasAves;
import br.gov.mt.indea.sistemaformin.enums.Dominio.SinaisClinicosSindromeVesicular;
import br.gov.mt.indea.sistemaformin.enums.Dominio.SistemaDeCriacaoDeRuminantes;
import br.gov.mt.indea.sistemaformin.enums.Dominio.SistemaDeCriacaoPropriedade;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoAlimentoAves;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoAlimentoSuideos;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoAlteracaoSindromeNeurologica;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoAmostra;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoCoordenadaGeografica;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoCorrimentoNasal;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoCriacaoAnimais;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoDeAgrupamento;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoDeMorteEmFrigorifico;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoDeTesteParaMormo;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoDestinoCadaveres;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoDestinoExploracao;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoDestinoSuideos;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoDiagnostico;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoEstabelecimento;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoExploracaoAbelhas;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoExploracaoAves;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoExploracaoAvicolaSistemaIndustrial;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoExploracaoCoelhos;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoExploracaoEquideos;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoExploracaoOutrosRuminantes;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoExploracaoSuinos;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoExploracaoSuinosSistemaIndustrial;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoInvestigacaoFormCOM;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoLaboratorio;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoMotivoNotificacao;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoOcorrenciaObservadaFormCOM;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoOrigemRacaoAnimais;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoOrigemRestosDeComida;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoOrigemSoroOuRestosDeLavoura;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoOrigemSuideos;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoPeriodo;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoPropriedade;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoReposicaoAnimais;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoResponsavelColheitaDeAmostra;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoSaida;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoServicoDeInspecao;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoTelefone;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoTesteLaboratorial;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoTransitoDeAnimais;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoUsuario;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoVinculoEpidemiologico;
import br.gov.mt.indea.sistemaformin.enums.Dominio.UsoDeMedicacaoFormCOM;
import br.gov.mt.indea.sistemaformin.enums.Dominio.VigilanciaSindromica;
import br.gov.mt.indea.sistemaformin.service.PaisService;
import br.gov.mt.indea.sistemaformin.service.UFService;

@Named
@ApplicationScoped
public class ListsManagedBean implements Serializable{

	private static final long serialVersionUID = 1096028368132582154L;

	private List<UF> listaUFs;
	
	private List<Pais> listaPais;
	
	@Inject
	private UFService ufService;
	
	@Inject
	private PaisService paisService;

	public ListsManagedBean(){
	}
	
	@PostConstruct
	public void init(){
		this.listaUFs = ufService.findAll("nome");
		this.listaPais = paisService.findAll("nome");
	}
	
	public List<UF> getListaUFs(){	
		return listaUFs;
	}
	
	public List<Pais> getListaPais() {
		return listaPais;
	}

	public AtivoInativo[] getListaAtivoInativo(){
		return AtivoInativo.values();
	}
	
	public AtivoPassivo[] getListaAtivoPassivo(){
		return AtivoPassivo.values();
	}
	
	public AtivaPassiva[] getListaAtivaPassiva(){
		return AtivaPassiva.values();
	}
	
	public SimNao[] getListaSimNao(){
		return SimNao.values();
	}
	
	public SimNaoSI[] getListaSimNaoSI(){
		return SimNaoSI.values();
	}
	
	public SimNaoNA[] getListaSimNaoNA(){
		return SimNaoNA.values();
	}
	
	public FonteDaNotificacao[] getListaFonteDaNotificacao(){
		return FonteDaNotificacao.values();
	}
	
	public MotivoInicialDaInvestigacao[] getListaMotivoInicialDaInvestigacao(){
		return MotivoInicialDaInvestigacao.values();
	}
	
	public TipoPropriedade[] getListaTipoPropriedadeFormIN(){
		return TipoPropriedade.getTipoPropriedadeFormIN();
	}
	
	public TipoPropriedade[] getListaTipoPropriedadeEquinos(){
		return TipoPropriedade.getTipoPropriedadeEquinos();
	}
	
	public SistemaDeCriacaoPropriedade[] getListaSistemaDeCriacaoPropriedade(){
		return SistemaDeCriacaoPropriedade.values();
	}
	
	public TipoCoordenadaGeografica[] getListaTipoCoordenadaGeografica(){
		return TipoCoordenadaGeografica.values();
	}
	
	public TipoCoordenadaGeografica[] getListaTipoCoordenadaGeograficaFormSN(){
		return TipoCoordenadaGeografica.getListaTipoCoordenadaGeograficaFormSN();
	}
	
	public FuncaoNoEstabelecimento[] getListaFuncaoNoEstabelecimento(){
		return FuncaoNoEstabelecimento.values();
	}
	
	public TipoTelefone[] getListaTipoTelefone(){
		return TipoTelefone.values();
	}
	
	public VigilanciaSindromica[] getListaVigilanciaSindromica(){
		return VigilanciaSindromica.values();
	}
	
	public TipoTransitoDeAnimais[] getListaTipoTransitoDeAnimais(){
		return TipoTransitoDeAnimais.values();
	}
	
	public MedidasAdotadasNoEstabelecimento[] getListaMedidasAdotadasNoEstabelecimentos_FormIN(){
		return MedidasAdotadasNoEstabelecimento.getFormINValues();
	}
	
	public MedidasAdotadasNoEstabelecimento[] getListaMedidasAdotadasNoEstabelecimentos_FormCOM(){
		return MedidasAdotadasNoEstabelecimento.getFormCOMValues();
	}
	
	public TipoDestinoExploracao[] getListaTipoDestinoExploracao(){
		return TipoDestinoExploracao.values();
	}
	
	public FinalidadeExploracaoBovinosEBubalinos[] getListaFinalidadeExploracaoBovinosEBubalinos(){
		return FinalidadeExploracaoBovinosEBubalinos.getFinalidadeExploracaoBovinosEBubalinos();
	}
	
	public FaseExploracaoBovinosEBubalinos[] getListaFaseExploracaoBovinosEBubalinos_FormIN(){
		return FaseExploracaoBovinosEBubalinos.getListaFaseExploracaoBovinosEBubalinos_FormIN();
	}
	
	public FaseExploracaoBovinosEBubalinos[] getListaFaseExploracaoBovinosEBubalinos_Vigilancia(){
		return FaseExploracaoBovinosEBubalinos.getListaFaseExploracaoBovinosEBubalinos_Vigilancia();
	}
	
	public FinalidadeExploracaoCaprinos[] getListaFinalidadeExploracaoCaprinos(){
		return FinalidadeExploracaoCaprinos.values();
	}
	
	public SistemaDeCriacaoDeRuminantes[] getListaSistemaDeCriacaoDeRuminantes(){
		return SistemaDeCriacaoDeRuminantes.values();
	}
	
	public FaseExploracaoCaprinos[] getListaFaseExploracaoCaprinos(){
		return FaseExploracaoCaprinos.values();
	}

	public FinalidadeExploracaoOvinos[] getListaFinalidadeExploracaoOvinos(){
		return FinalidadeExploracaoOvinos.values();
	}
	
	public FaseExploracaoOvinos[] getListaFaseExploracaoOvinos(){
		return FaseExploracaoOvinos.values();
	}
	
	public TipoExploracaoSuinos[] getListaTipoExploracaoSuinos(){
		return TipoExploracaoSuinos.values();
	}
	
	public TipoExploracaoSuinos[] getListaTipoExploracaoSuinos_Subsistencia(){
		return TipoExploracaoSuinos.getTiposSubsistencia();
	}
	
	public TipoExploracaoSuinos[] getListaTipoExploracaoSuinos_Granja(){
		return TipoExploracaoSuinos.getTiposGranja();
	}
	
	public TipoExploracaoEquideos[] getListaTipoExploracaoEquideos(){
		return TipoExploracaoEquideos.values();
	}
	
	public TipoExploracaoAves[] getListaTipoExploracaoAves(){
		return TipoExploracaoAves.values();
	}
	
	public TipoExploracaoAbelhas[] getListaTipoExploracaoAbelhas(){
		return TipoExploracaoAbelhas.values();
	}
	
	public TipoExploracaoCoelhos[] getListaTipoExploracaoCoelhos(){
		return TipoExploracaoCoelhos.values();
	}
	
	public TipoInvestigacaoFormCOM[] getListaTipoInvestigacaoFormCOM(){
		return TipoInvestigacaoFormCOM.values();
	}
	
	public ProvavelOrigemFormCOM[] getListaProvavelOrigemFormCOM(){
		return ProvavelOrigemFormCOM.values();
	}
	
	public TipoOcorrenciaObservadaFormCOM[] getListaTipoOcorrenciaObservadaFormCOM(){
		return TipoOcorrenciaObservadaFormCOM.values();
	}
	
	public MomentoMedidasAdotadasNoEstabelecimento[] getListaMomentoMedidasAdotadasNoEstabelecimento(){
		return MomentoMedidasAdotadasNoEstabelecimento.values();
	}
	
	public TipoVinculoEpidemiologico[] getListaTipoVinculoEpidemiologico(){
		return TipoVinculoEpidemiologico.values();
	}
	
	public Especie[] getListaEspecies(){
		return Especie.values();
	}
	
	public Especie[] getListaEspeciesAfetadasFormIN(){
		return Especie.getEspeciesAfetadasFormIN();
	}
	public Especie[] getListaEspeciesLAB(){
		return Especie.getEspeciesLAB();
	}
	
	public Especie[] getListaEspeciesSV(){
		return Especie.getEspeciesSV();
	}
	
	public Especie[] getListaEspeciesSN(){
		return Especie.getEspeciesSN();
	}
	
	public Especie[] getListaEspeciesDeEquinos(){
		return Especie.getEspeciesDeEquinos();
	}
	
	public Especie[] getListaEspeciesDeAves(){
		return Especie.getEspeciesDeAves();
	}
	
	public Especie[] getListaEspeciesFormularioDeColheitaEET(){
		return Especie.getEspeciesFormularioDeColheitaEET();
	}
	
	public Especie[] getListaEspeciesInvestigacaoEpidemiologica(){
		return Especie.getEspeciesInvestigacaoEpidemiologica();
	}
	
	public Sexo[] getListaSexo_FormLAB(){
		return Sexo.getSexo_FormLAB();
	}
	
	public Sexo[] getListaSexo_FormEQ(){
		return Sexo.getSexo_FormEQ();
	}
	
	public Sexo[] getListaSexoSimples(){
		return Sexo.getSexoSimples();
	}
	
	public MachoFemea[] getListaMachoFemea(){
		return MachoFemea.values();
	}
	
	public Sexo[] getListaPeriodoGestacao(){
		return Sexo.getSexo_PeriodoGestacao();
	}
	
	public TipoPeriodo[] getListaTipoPeriodos(){
		return TipoPeriodo.values();
	}
	
	public TipoPeriodo[] getListaTipoPeriodo_AnoMes(){
		return TipoPeriodo.getListaTipoPeriodos_AnoMes();
	}
	
	public TipoPeriodo[] getListaTipoPeriodo_DiaAno(){
		return TipoPeriodo.getListaTipoPeriodos_DiaAno();
	}
	
	public TipoPeriodo[] getListaTipoPeriodo_DiaMes(){
		return TipoPeriodo.getListaTipoPeriodos_DiaMes();
	}
	
	public TipoCriacaoAnimais[] getListaTipoCriacaoAnimais(){
		return TipoCriacaoAnimais.values();
	}
	
	public TipoReposicaoAnimais[] getListaTipoReposicaoAnimais(){
		return TipoReposicaoAnimais.values();
	}
	
	public TipoOrigemRacaoAnimais[] getListaTipoOrigemRacaoAnimais(){
		return TipoOrigemRacaoAnimais.values();
	}
	
	public TipoOrigemRestosDeComida[] getListaTipoOrigemRestosDeComida(){
		return TipoOrigemRestosDeComida.values();
	}
	
	public TipoOrigemSoroOuRestosDeLavoura[] getListaTipoOrigemSoroOuRestosDeLavouras(){
		return TipoOrigemSoroOuRestosDeLavoura.values();
	}
	
	public SinaisClinicosSindromeVesicular[] getListaSinaisClinicosSindromeVesicular(){
		return SinaisClinicosSindromeVesicular.values();
	}
	
	public SinaisClinicosSindromeHemorragica[] getListaSinaisClinicosSindromeHemorragica_EstadoGeral(){
		return SinaisClinicosSindromeHemorragica.getLesoesEstadoGeral();
	}
	
	public SinaisClinicosSindromeHemorragica[] getListaSinaisClinicosSindromeHemorragica_SistemaRespiratorio(){
		return SinaisClinicosSindromeHemorragica.getLesoesSistemaRespiratorio();
	}
	
	public SinaisClinicosSindromeHemorragica[] getListaSinaisClinicosSindromeHemorragica_SistemaNervoso(){
		return SinaisClinicosSindromeHemorragica.getLesoesSistemaNervoso();
	}
	
	public SinaisClinicosSindromeHemorragica[] getListaSinaisClinicosSindromeHemorragica_SistemaDigestorio(){
		return SinaisClinicosSindromeHemorragica.getLesoesSistemaDigestorio();
	}
	
	public SinaisClinicosSindromeHemorragica[] getListaSinaisClinicosSindromeHemorragica_SistemaReprodutivo(){
		return SinaisClinicosSindromeHemorragica.getLesoesSistemaReprodutivo();
	}
	
	public SinaisClinicosSindromeHemorragica[] getListaSinaisClinicosSindromeHemorragica_SistemaTegumentar(){
		return SinaisClinicosSindromeHemorragica.getLesoesSistemaTegumentar();
	}
	
	public SinaisClinicosSindromeHemorragica[] getListaSinaisClinicosSindromeHemorragica_SistemaLinfatico(){
		return SinaisClinicosSindromeHemorragica.getLesoesSistemaLinfatico();
	}
	
	public SinaisClinicosSindromeNervosaERespiratoriaDasAves[] getListaSinaisClinicosSindromeNervosasSindromeNervosaERespiratoriaDasAves_EstadoGeral(){
		return SinaisClinicosSindromeNervosaERespiratoriaDasAves.getSinaisClinicosSindromeNervosa_EstadoGeral();
	}
	
	public SinaisClinicosSindromeNervosaERespiratoriaDasAves[] getListaSinaisClinicosSindromeNervosasSindromeNervosaERespiratoriaDasAves_SistemaRespiratorio(){
		return SinaisClinicosSindromeNervosaERespiratoriaDasAves.getSinaisClinicosSindromeNervosa_SistemaRespiratorio();
	}
	
	public SinaisClinicosSindromeNervosaERespiratoriaDasAves[] getListaSinaisClinicosSindromeNervosasSindromeNervosaERespiratoriaDasAves_SistemaNervoso(){
		return SinaisClinicosSindromeNervosaERespiratoriaDasAves.getSinaisClinicosSindromeNervosa_SistemaNervoso();
	}
	
	public SinaisClinicosSindromeNervosaERespiratoriaDasAves[] getListaSinaisClinicosSindromeNervosasSindromeNervosaERespiratoriaDasAves_SistemaDigestorio(){
		return SinaisClinicosSindromeNervosaERespiratoriaDasAves.getSinaisClinicosSindromeNervosa_SistemaDigestorio();
	}
	
	public SinaisClinicosSindromeNervosaERespiratoriaDasAves[] getListaSinaisClinicosSindromeNervosasSindromeNervosaERespiratoriaDasAves_SistemaCirculatorio(){
		return SinaisClinicosSindromeNervosaERespiratoriaDasAves.getSinaisClinicosSindromeNervosa_SistemaCirculatorio();
	}
	
	public SinaisClinicosCavidadeNasal[] getListaSinaisClinicosCavidadeNasal(){
		return SinaisClinicosCavidadeNasal.values();
	}
	
	public SinaisClinicosPele[] getListaSinaisClinicosPele(){
		return SinaisClinicosPele.values();
	}
	
	public ResultadoNecropsiaDeAves[] getListaResultadoNecropsiaDeAves_EstadoGeral(){
		return ResultadoNecropsiaDeAves.getResultadoNecropsiaDeAves_EstadoGeral();
	}
	
	public ResultadoNecropsiaDeAves[] getListaResultadoNecropsiaDeAves_SistemaRespiratorio(){
		return ResultadoNecropsiaDeAves.getResultadoNecropsiaDeAves_SistemaRespiratorio();
	}
	
	public ResultadoNecropsiaDeAves[] getListaResultadoNecropsiaDeAves_SistemaUrinarioEReprodutor(){
		return ResultadoNecropsiaDeAves.getResultadoNecropsiaDeAves_SistemaUrinarioEReprodutor();
	}
	
	public ResultadoNecropsiaDeAves[] getListaResultadoNecropsiaDeAves_SistemaCirculatorioHematopoteicoELinfatico(){
		return ResultadoNecropsiaDeAves.getResultadoNecropsiaDeAves_SistemaCirculatorioHematopoteicoELinfatico();
	}
	
	public ResultadoNecropsiaDeAves[] getListaResultadoNecropsiaDeAves_SistemaDigestivo(){
		return ResultadoNecropsiaDeAves.getResultadoNecropsiaDeAves_SistemaDigestivo();
	}
	
	public ResultadoNecropsiaDeAves[] getListaResultadoNecropsiaDeAves_SistemaNervoso(){
		return ResultadoNecropsiaDeAves.getResultadoNecropsiaDeAves_SistemaNervoso();
	}
	
	public LocalDeColhimentoDeAmostra[] getListaLocalDeColhimentoDeAmostras(){
		return LocalDeColhimentoDeAmostra.values();
	}
	
	public MetodoParaEstipularIdade[] getListaMetodoParaEstipularIdade(){
		return MetodoParaEstipularIdade.values();
	}
	
	public CategoriaDoAnimalSubmetidoAVigilancia[] getListaCategoriaDoAnimalSubmetidoAVigilancia(){
		return CategoriaDoAnimalSubmetidoAVigilancia.values();
	}
	
	public TipoAmostra[] getListaTipoAmostras(){
		return TipoAmostra.values();
	}
	
	public MeioConservacaoAmostra[] getListaMeioConservacaoAmostras(){
		return MeioConservacaoAmostra.values();
	}
	
	public TipoAlimentoAves[] getListaTipoAlimentoAves(){
		return TipoAlimentoAves.values();
	}
	
	public TipoLaboratorio[] getListaTipoLaboratorio(){
		return TipoLaboratorio.values();
	}
	
	public FinalidadeDoTesteDeLaboratorio[] getListaFinalidadeDoTesteDeLaboratorio_LaboratorioCredenciado(){
		return FinalidadeDoTesteDeLaboratorio.getFinalidadeDoTesteDeLaboratorio_LaboratorioCredenciado();
	}
	
	public FinalidadeDoTesteDeLaboratorio[] getListaFinalidadeDoTesteDeLaboratorio_LaboratorioCredenciadoPublico(){
		return FinalidadeDoTesteDeLaboratorio.getFinalidadeDoTesteDeLaboratorio_LaboratorioCredenciadoPublico();
	}

	public FinalidadeDoTesteDeLaboratorio[] getListaFinalidadeDoTesteDeLaboratorio_LaboratorioOficial(){
		return FinalidadeDoTesteDeLaboratorio.getFinalidadeDoTesteDeLaboratorio_LaboratorioOficial();
	}
	
	public TipoDeTesteParaMormo[] getListaTipoDeTesteParaMormo(){
		return TipoDeTesteParaMormo.values();
	}
	
	public DireitaEsquerda[] getListaDireitaEsquerda(){
		return DireitaEsquerda.values();
	}
	
	public DireitoEsquerdo[] getListaDireitoEsquerdo(){
		return DireitoEsquerdo.values();
	}
	
	public PositivoNegativo[] getListaPositivoNegativoSimple(){
		return PositivoNegativo.getPositivoNegativoSimples();
	}
	
	public PositivoNegativo[] getListaPositivoNegativo(){
		return PositivoNegativo.values();
	}
	
	public SinaisClinicosExameMaleina[] getListaSinaisClinicosExameMaleina(){
		return SinaisClinicosExameMaleina.values();
	}
	
	public LocalizacaoCorrimentoNasal[] getListaLocalizacaoCorrimentoNasal(){
		return LocalizacaoCorrimentoNasal.values();
	}
	
	public TipoCorrimentoNasal[] getListaTipoCorrimentoNasal(){
		return TipoCorrimentoNasal.values();
	}
	
	public OrigemDoAnimal[] getListaOrigemDoAnimal(){
		return OrigemDoAnimal.values();
	}
	
	public DoencasRespiratorias[] getListaDoencasRespiratorias(){
		return DoencasRespiratorias.values();
	}
	
	public ManejoDosAnimais[] getListaManejoDosAnimais(){
		return ManejoDosAnimais.values();
	}

	public CategoriaAves[] getListaCategoriaAves(){
		return CategoriaAves.values();
	}
	
	public TipoDeAgrupamento[] getListaTipoDeAgrupamento(){
		return TipoDeAgrupamento.values();
	}
	
	public FaixaEtariaOuEspecie[] getListaFaixaEtariaFormSN(){
		return FaixaEtariaOuEspecie.getFaixaEtariaFormSN();
	}
	
	public FaixaEtariaOuEspecie[] getListaFaixaOutrosTiposDeAves(){
		return FaixaEtariaOuEspecie.getOutrasAves();
	}
	
	public UsoDeMedicacaoFormCOM[] getListaUsoDeMedicacaoFormCOMs(){
		return UsoDeMedicacaoFormCOM.values();
	}
	
	public PontosDeRisco[] getListaPontosDeRiscos(){
		return PontosDeRisco.values();
	}
	
	public TipoAlimentoSuideos[] getListaTipoAlimentoSuideos(){
		return TipoAlimentoSuideos.values();
	}
	
	public TipoOrigemSuideos[] getListaTipoOrigemSuideos(){
		return TipoOrigemSuideos.values();
	}
	
	public TipoDestinoSuideos[] getListaTipoDestinoSuideos(){
		return TipoDestinoSuideos.values();
	}
	
	public TipoDestinoCadaveres[] getListaTipoDestinoCadaveres(){
		return TipoDestinoCadaveres.values();
	}
	
	public TipoEstabelecimento[] getListaTipoEstabelecimento(){
		return TipoEstabelecimento.values();
	}
	
	public TipoEstabelecimento[] getListaTipoEstabelecimento_FormIN(){
		return TipoEstabelecimento.getListaTipoEstabelecimento_FormIN();
	}
	
	public TipoResponsavelColheitaDeAmostra[] getListaTipoResponsavelColheitaDeAmostra(){
		return TipoResponsavelColheitaDeAmostra.values();
	}
	
	public TipoAlteracaoSindromeNeurologica[] getListaTipoAlteracaoSindromeNeurologica(){
		return TipoAlteracaoSindromeNeurologica.values();
	}
	
	public TipoExploracaoSuinosSistemaIndustrial[] getListaTipoExploracaoSuinosSistemaIndustrial(){
		return TipoExploracaoSuinosSistemaIndustrial.values();
	}
	
	public TipoExploracaoAvicolaSistemaIndustrial[] getListaTipoExploracaoAvicolaSistemaIndustrial(){
		return TipoExploracaoAvicolaSistemaIndustrial.values();
	}
	
	public FinalidadeExploracaoSuinos[] getListaFinalidadeExploracaoSuinos(){
		return FinalidadeExploracaoSuinos.values();
	}

	public TipoExploracaoOutrosRuminantes[] getListaTipoExploracaoOutrosRuminantes(){
		return TipoExploracaoOutrosRuminantes.values();
	}
	
	public TipoDiagnostico[] getListaTipoDiagnostico_ProvavelConclusivo(){
		return TipoDiagnostico.getListTipoDiagnostico_ProvavelConclusivo();
	}
	
	public TipoDiagnostico[] getListaTipoDiagnostico_ConclusivoLaboratorial(){
		return TipoDiagnostico.getListTipoDiagnostico_ConclusivoLaboratorial();
	}
	
	public TipoTesteLaboratorial[] getListaTipoTesteLaboratorial(){
		return TipoTesteLaboratorial.values();
	}
	
	public AreaAtuacaoMedicoVeterinario[] getListaAreaAtuacaoMedicoVeterinario(){
		return AreaAtuacaoMedicoVeterinario.values();
	}
	
	public TipoUsuario[] getListaTipoUsuario(){
		return TipoUsuario.values();
	}
	
	public CondicaoMaterial[] getListaCondicaoMaterial(){
		return CondicaoMaterial.values();
	}
	
	public TipoSaida[] getListaTipoSaida(){
		return TipoSaida.values();
	}
	
	public AcompanhaNaoAcompanha[] getListaAcompanhaNaoAcompanha(){
		return AcompanhaNaoAcompanha.values();
	}
	
	public Mes[] getListaMes(){
		return Mes.values();
	}

	public TipoServicoDeInspecao[] getListaTipoServicoDeInspecao(){
		return TipoServicoDeInspecao.values();
	}
	
	public TipoDeMorteEmFrigorifico[] getListaTipoDeMorteEmFrigorifico(){
		return TipoDeMorteEmFrigorifico.values();
	}
	
	public MotivacaoParaAbateDeEmergenciaEmFrigorifico[] getListaMotivacaoParaAbateDeEmergenciaEmFrigorifico(){
		return MotivacaoParaAbateDeEmergenciaEmFrigorifico.values();
	}
	
	public SinaisClinicosNervososEncefalopatoaEspongiformeTransmissivel[] getListaSinaisClinicosNervososEncefalopatoaEspongiformeTransmissivel(){
		return SinaisClinicosNervososEncefalopatoaEspongiformeTransmissivel.values();
	}
	
	public CategoriaAnimal[] getListaCategoriaAnimal(){
		return CategoriaAnimal.values();
	}
	
	public ResultadoRelatorioDeEnsaio[] getListaResultadoRelatorioDeEnsaio() {
		return ResultadoRelatorioDeEnsaio.values();
	}
	
	public TipoMotivoNotificacao[] getListaTipoMotivoNotificacaos() {
		return TipoMotivoNotificacao.values();
	}

	public int[] getListaAnosFrom(int inicio){
		int[] anos;
		LocalDate data = LocalDate.now();
		int anoAtual = data.getYear();
		
		anos = new int[anoAtual - inicio + 1];
		int i = 0;
		
		
		do {
			anos[i] = anoAtual;
			i++;
			anoAtual--;
		} while (anoAtual >= inicio);
		
		return anos;
	}
	
}
