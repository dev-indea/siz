package br.gov.mt.indea.sistemaformin.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Audited
@Entity
@Table(name="numero_inspecao_form_vin")
public class NumeroInspecaoFormVIN extends BaseEntity<Long>{
	
	private static final long serialVersionUID = 1065292498852981795L;

	@Id
	@SequenceGenerator(name="numero_inspecao_form_vin_seq", sequenceName="numero_inspecao_form_vin_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="numero_inspecao_form_vin_seq")
	private Long id;
	
	@Column(name="numero_form_in")
	private String numeroFormIN;
	
	@Column(name="id_propriedade")
	private Long idPropriedade;
	
	@Column(name="ultimo_numero")
	private Long ultimoNumero;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNumeroFormIN() {
		return numeroFormIN;
	}

	public void setNumeroFormIN(String numeroFormIN) {
		this.numeroFormIN = numeroFormIN;
	}

	public Long getIdPropriedade() {
		return idPropriedade;
	}

	public void setIdPropriedade(Long idPropriedade) {
		this.idPropriedade = idPropriedade;
	}

	public Long getUltimoNumero() {
		return ultimoNumero;
	}

	public void setUltimoNumero(Long ultimoNumero) {
		this.ultimoNumero = ultimoNumero;
	}
	
	public void incrementNumeroInspecaoFormVIN() {
		this.ultimoNumero++;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NumeroInspecaoFormVIN other = (NumeroInspecaoFormVIN) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}