package br.gov.mt.indea.sistemaformin.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.envers.Audited;

@Audited
@Entity
@DiscriminatorValue("bubalinos")
public class DetalhesInformacoesBubalinos extends AbstractDetalhesInformacoesDeAnimais implements Cloneable{
	
	private static final long serialVersionUID = 3938198344196865288L;

	@ManyToOne
	@JoinColumn(name="id_informacoes_bubalinos")
	private InformacoesBubalinos informacoesBubalinos;

	public InformacoesBubalinos getInformacoesBubalinos() {
		return informacoesBubalinos;
	}

	public void setInformacoesBubalinos(InformacoesBubalinos informacoesBubalinos) {
		this.informacoesBubalinos = informacoesBubalinos;
	}
	
	protected Object clone(InformacoesBubalinos informacoesBubalinos) throws CloneNotSupportedException{
		DetalhesInformacoesBubalinos cloned = (DetalhesInformacoesBubalinos) super.clone();
		
		cloned.setId(null);
		cloned.setInformacoesBubalinos(informacoesBubalinos);
		
		return cloned;
	}

}