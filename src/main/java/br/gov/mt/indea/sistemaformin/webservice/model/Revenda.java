package br.gov.mt.indea.sistemaformin.webservice.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Revenda extends Pessoa implements Serializable {

    private static final long serialVersionUID = 5192589142309125439L;

	private String distribuidor;
    
    private String origem;

    @XmlElement(name = "ule")
    private ULE ule;
    
    public ULE getUle() {
        return ule;
    }

    public void setUle(ULE ule) {
        this.ule = ule;
    }

    public Revenda() {
    }

    public String getDistribuidor() {
        return distribuidor;
    }

    public void setDistribuidor(String distribuidor) {
        this.distribuidor = distribuidor;
    }

    public String getOrigem() {
        return origem;
    }

    public void setOrigem(String origem) {
        this.origem = origem;
    }
    
}