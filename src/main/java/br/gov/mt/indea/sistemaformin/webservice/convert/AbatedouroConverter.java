package br.gov.mt.indea.sistemaformin.webservice.convert;

import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;

import br.gov.mt.indea.sistemaformin.entity.Municipio;
import br.gov.mt.indea.sistemaformin.service.AbatedouroService;
import br.gov.mt.indea.sistemaformin.service.MunicipioService;
import br.gov.mt.indea.sistemaformin.webservice.entity.Abatedouro;
import br.gov.mt.indea.sistemaformin.webservice.entity.CoordenadaGeografica;
import br.gov.mt.indea.sistemaformin.webservice.entity.Endereco;
import br.gov.mt.indea.sistemaformin.webservice.entity.TipoLogradouro;

public class AbatedouroConverter {

	@Inject
	private MunicipioService municipioService;
	
	@Inject
	private AbatedouroService abatedouroService;
	
	public Abatedouro fromModelToEntity(br.gov.mt.indea.sistemaformin.webservice.model.Abatedouro modelo){
		if (modelo == null)
			return null;
		
		Abatedouro abatedouro = null;
		
		abatedouro = this.findAbatedouroByCodigo(modelo.getCnpj());
		
		if (abatedouro != null)
			return abatedouro;
		else
			abatedouro = new Abatedouro();
		

		abatedouro.setApelido(modelo.getApelido());
		abatedouro.setCnpj(modelo.getCnpj());
		abatedouro.setCodigo(modelo.getCodigo());
		abatedouro.setNome(modelo.getNome());
		abatedouro.setNumeroInspecao(modelo.getNumeroInspecao());
		abatedouro.setTipoInspecao(modelo.getTipoInspecao().replace("INPE��O", "INSPE��O"));
		
		abatedouro.setMunicipio(this.findMunicipio(modelo.getCodIbgeEmpresa()));
		
		if (modelo.getEndereco() != null){
			Endereco enderecoPropriedade = new Endereco();
			
			enderecoPropriedade.setBairro(modelo.getEndereco().getBairro());
			enderecoPropriedade.setCep(modelo.getEndereco().getCep());
			enderecoPropriedade.setComplemento(modelo.getEndereco().getComplemento());
			enderecoPropriedade.setLogradouro(modelo.getEndereco().getLogradouro());
			enderecoPropriedade.setNumero(modelo.getEndereco().getNumero());
			enderecoPropriedade.setReferencia(modelo.getEndereco().getReferencia());
			enderecoPropriedade.setTelefone(modelo.getEndereco().getTelefone());
			
			if (modelo.getEndereco().getTipoLogradouro() != null){
				TipoLogradouro tipoLogradouroEnderecoPropriedade = new TipoLogradouro();
				tipoLogradouroEnderecoPropriedade.setNome(modelo.getEndereco().getTipoLogradouro().getNome());
				
				enderecoPropriedade.setTipoLogradouro(tipoLogradouroEnderecoPropriedade);
			}
			
			if (modelo.getEndereco().getMunicipio() != null)
				enderecoPropriedade.setMunicipio(this.findMunicipio(modelo.getEndereco().getMunicipio()));
			
			abatedouro.setEndereco(enderecoPropriedade);
		}
		
		if (modelo.getCoordenada() != null ){
			CoordenadaGeografica coordenadaGeografica = new CoordenadaGeografica();
			
			coordenadaGeografica.setLatGrau(modelo.getCoordenada().getLatGrau());
			coordenadaGeografica.setLatMin(modelo.getCoordenada().getLatMin());
			coordenadaGeografica.setLatSeg(modelo.getCoordenada().getLatSeg());
			coordenadaGeografica.setLongGrau(modelo.getCoordenada().getLongGrau());
			coordenadaGeografica.setLongMin(modelo.getCoordenada().getLongMin());
			coordenadaGeografica.setLongSeg(modelo.getCoordenada().getLongSeg());
			coordenadaGeografica.setOrientacaoLatitude(modelo.getCoordenada().getOrientacaoLatitude());
			coordenadaGeografica.setOrientacaoLongitude(modelo.getCoordenada().getOrientacaoLongitude());
			coordenadaGeografica.setPessoa_id(modelo.getCoordenada().getPessoa_id());
			
			abatedouro.setCoordenadaGeografica(coordenadaGeografica);
		}
		
		return abatedouro;
	}

	private Municipio findMunicipio(String codgIBGE){
		if (StringUtils.isEmpty(codgIBGE))
			return null;
		
		Municipio municipio = municipioService.findByCodgIBGE(codgIBGE.substring(0, 2), codgIBGE.substring(2, codgIBGE.length()));
		
		return municipio;
	}
	
	private Municipio findMunicipio(br.gov.mt.indea.sistemaformin.webservice.model.Municipio municipioModel){
		if (municipioModel == null)
			return null;
		
		String codgIBGE = municipioModel.getId() + "";
		
		Municipio municipio = municipioService.findByCodgIBGE(codgIBGE.substring(0, 2), codgIBGE.substring(2, codgIBGE.length()));
		
		return municipio;
	}
	
	private Abatedouro findAbatedouroByCodigo(String cnpj){
		if (cnpj == null)
			return null;
		
		return this.abatedouroService.findByCnpj(cnpj);
	}
	
}
