package br.gov.mt.indea.sistemaformin.webservice.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class MeioTransporte implements Serializable {

	private static final long serialVersionUID = -3021310514218862832L;

	private Long idTable;
	
	@Column(name = "meio_transporte_id")
	private String id;
    
	@Column(name = "mtr_nome")
    private String nome;
    
	@Column(name = "mtr_codigo_pga")
    private String codigoPga;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCodigoPga() {
        return codigoPga;
    }

    public void setCodigoPga(String codigoPga) {
        this.codigoPga = codigoPga;
    }

	public Long getIdTable() {
		return idTable;
	}

	public void setIdTable(Long idTable) {
		this.idTable = idTable;
	}
}