package br.gov.mt.indea.sistemaformin.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.mt.indea.sistemaformin.entity.TipoChavePrincipalTermoFiscalizacao;
import br.gov.mt.indea.sistemaformin.enums.Dominio.AtivoInativo;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class TipoChavePrincipalTermoFiscalizacaoService extends PaginableService<TipoChavePrincipalTermoFiscalizacao, Long> {

	private static final Logger log = LoggerFactory.getLogger(TipoChavePrincipalTermoFiscalizacaoService.class);
	
	protected TipoChavePrincipalTermoFiscalizacaoService() {
		super(TipoChavePrincipalTermoFiscalizacao.class);
	}
	
	public TipoChavePrincipalTermoFiscalizacao findByIdFetchAll(Long id){
		TipoChavePrincipalTermoFiscalizacao tipoChavePrincipalTermoFiscalizacao;
		
		StringBuilder sql = new StringBuilder();
		sql.append("select v ")
		   .append("  from TipoChavePrincipalTermoFiscalizacao v ")
		   .append(" where v.id = :id ");
		
		Query query = getSession().createQuery(sql.toString()).setLong("id", id);
		tipoChavePrincipalTermoFiscalizacao = (TipoChavePrincipalTermoFiscalizacao) query.uniqueResult();
		
		return tipoChavePrincipalTermoFiscalizacao;
	}
	
	@SuppressWarnings("unchecked")
	public List<TipoChavePrincipalTermoFiscalizacao> findAllByStatus(AtivoInativo status) {
		List<TipoChavePrincipalTermoFiscalizacao> tipoChavePrincipalTermoFiscalizacao;
		
		StringBuilder sql = new StringBuilder();
		sql.append("select v ")
		   .append("  from TipoChavePrincipalTermoFiscalizacao v ")
		   .append(" where v.status = :status ");
		
		Query query = getSession().createQuery(sql.toString()).setParameter("status", status);
		tipoChavePrincipalTermoFiscalizacao = query.list();
		
		return tipoChavePrincipalTermoFiscalizacao;
	}
	
	@Override
	public void saveOrUpdate(TipoChavePrincipalTermoFiscalizacao tipoChavePrincipalTermoFiscalizacao) {
		super.saveOrUpdate(tipoChavePrincipalTermoFiscalizacao);
		
		log.info("Salvando Tipo Chave Principal Termo Fiscalizacao {}", tipoChavePrincipalTermoFiscalizacao.getId());
	}
	
	@Override
	public void delete(TipoChavePrincipalTermoFiscalizacao tipoChavePrincipalTermoFiscalizacao) {
		super.delete(tipoChavePrincipalTermoFiscalizacao);
		
		log.info("Removendo Tipo Chave Principal Termo Fiscalizacao {}", tipoChavePrincipalTermoFiscalizacao.getId());
	}
	
	@Override
	public void validar(TipoChavePrincipalTermoFiscalizacao TipoChavePrincipalTermoFiscalizacao) {

	}

	@Override
	public void validarPersist(TipoChavePrincipalTermoFiscalizacao TipoChavePrincipalTermoFiscalizacao) {

	}

	@Override
	public void validarMerge(TipoChavePrincipalTermoFiscalizacao TipoChavePrincipalTermoFiscalizacao) {

	}

	@Override
	public void validarDelete(TipoChavePrincipalTermoFiscalizacao TipoChavePrincipalTermoFiscalizacao) {

	}

}
