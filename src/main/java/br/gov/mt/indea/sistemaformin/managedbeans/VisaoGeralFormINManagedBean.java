package br.gov.mt.indea.sistemaformin.managedbeans;

import java.io.Serializable;

import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLBeanName;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;

import br.gov.mt.indea.sistemaformin.entity.FolhaAdicional;
import br.gov.mt.indea.sistemaformin.entity.FormAIE;
import br.gov.mt.indea.sistemaformin.entity.FormCOM;
import br.gov.mt.indea.sistemaformin.entity.FormEQ;
import br.gov.mt.indea.sistemaformin.entity.FormIN;
import br.gov.mt.indea.sistemaformin.entity.FormLAB;
import br.gov.mt.indea.sistemaformin.entity.FormMaleina;
import br.gov.mt.indea.sistemaformin.entity.FormMormo;
import br.gov.mt.indea.sistemaformin.entity.FormSH;
import br.gov.mt.indea.sistemaformin.entity.FormSN;
import br.gov.mt.indea.sistemaformin.entity.FormSRN;
import br.gov.mt.indea.sistemaformin.entity.FormSV;
import br.gov.mt.indea.sistemaformin.entity.FormVIN;
import br.gov.mt.indea.sistemaformin.entity.Resenho;
import br.gov.mt.indea.sistemaformin.service.FolhaAdicionalService;
import br.gov.mt.indea.sistemaformin.service.FormAIEService;
import br.gov.mt.indea.sistemaformin.service.FormCOMService;
import br.gov.mt.indea.sistemaformin.service.FormEQService;
import br.gov.mt.indea.sistemaformin.service.FormINService;
import br.gov.mt.indea.sistemaformin.service.FormLABService;
import br.gov.mt.indea.sistemaformin.service.FormMaleinaService;
import br.gov.mt.indea.sistemaformin.service.FormMormoService;
import br.gov.mt.indea.sistemaformin.service.FormSHService;
import br.gov.mt.indea.sistemaformin.service.FormSNService;
import br.gov.mt.indea.sistemaformin.service.FormSRNService;
import br.gov.mt.indea.sistemaformin.service.FormSVService;
import br.gov.mt.indea.sistemaformin.service.FormVINService;
import br.gov.mt.indea.sistemaformin.service.ResenhoService;
import br.gov.mt.indea.sistemaformin.util.FacesMessageUtil;

@Named
@ViewScoped
@URLBeanName("visaoGeralFormINManagedBean")
@URLMappings(mappings = {
		@URLMapping(id = "visaoGeralFormIN", pattern = "/formIN/#{visaoGeralFormINManagedBean.id}/visaoGeral", viewId = "/pages/formIN/formIN.jsf")})
public class VisaoGeralFormINManagedBean implements Serializable{
	
	private static final long serialVersionUID = 9184314208953602729L;

	private Long id;
	
	@Inject
	private FormIN formIN;
	
	@Inject
	private FormINService formINService;
	
	@Inject
	private FolhaAdicionalService folhaAdicionalService;
	
	@Inject
	private FormCOMService formCOMService;
	
	@Inject
	private FormVINService formVINService;
	
	@Inject
	private FormLABService formLABService;
	
	@Inject
	private FormSVService formSVService;
	
	@Inject
	private FormSHService formSHService;
	
	@Inject
	private FormSNService formSNService;
	
	@Inject
	private FormSRNService formSRNService;
	
	@Inject
	private FormAIEService formAIEService;
	
	@Inject
	private FormMormoService formMormoService;
	
	@Inject
	private FormMaleinaService formMaleinaService;
	
	@Inject
	private FormEQService formEQService;
	
	@Inject
	private ResenhoService resenhoService;
	
	@URLAction(mappingId = "visaoGeralFormIN", onPostback = false)
	public void visaoGeral(){
		carregarFormIN();
	}

	private void carregarFormIN() {
		this.formIN = formINService.getFormINComFormulariosAnexos(this.getId());
	}

	public String transformarEmFormIN(FormVIN formVIN){
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		request.getSession().setAttribute("idFormVIN", formVIN.getId());
		
		return "pretty:incluirFormIN";
	}
	
	public void removerFolhaAdicional(FolhaAdicional folhaAdicional){
		this.folhaAdicionalService.delete(folhaAdicional);
		this.formIN.getListaFolhasAdicionais().remove(folhaAdicional);
		FacesMessageUtil.addInfoContextFacesMessage("Folha Adicional exclu�da com sucesso", "");
	}
	
	public void removerFormCOM(FormCOM formCOM){
		this.formCOMService.delete(formCOM);
		this.formIN.getListaFormCOM().remove(formCOM);
		FacesMessageUtil.addInfoContextFacesMessage("Form COM exclu�do com sucesso", "");
	}
	
	public void removerFormLAB(FormLAB formLAB){
		this.formLABService.delete(formLAB);
		this.formIN.getListaFormLAB().remove(formLAB);
		FacesMessageUtil.addInfoContextFacesMessage("Form LAB exclu�do com sucesso", "");
	}
	
	public void removerFormVIN(FormVIN formVIN){
		this.formVINService.delete(formVIN);
		this.formIN.getListaFormVIN().remove(formVIN);
		FacesMessageUtil.addInfoContextFacesMessage("Form VIN exclu�do com sucesso", "");
	}

	public void removerFormSV(FormSV formSV){
		this.formSVService.delete(formSV);
		this.formIN.getListaFormSV().remove(formSV);
		FacesMessageUtil.addInfoContextFacesMessage("Form SV exclu�do com sucesso", "");
	}
	
	public void removerFormSH(FormSH formSH){
		this.formSHService.delete(formSH);
		this.formIN.getListaFormSH().remove(formSH);
		FacesMessageUtil.addInfoContextFacesMessage("Form SH exclu�do com sucesso", "");
	}
	
	public void removerFormSN(FormSN formSN){
		this.formSNService.delete(formSN);
		this.formIN.getListaFormSN().remove(formSN);
		FacesMessageUtil.addInfoContextFacesMessage("Form SN exclu�do com sucesso", "");
	}
	
	public void removerFormSRN(FormSRN formSRN){
		this.formSRNService.delete(formSRN);
		this.formIN.getListaFormSRN().remove(formSRN);
		FacesMessageUtil.addInfoContextFacesMessage("Form SRN exclu�do com sucesso", "");
	}
	
	public void removerFormAIE(FormAIE formAIE){
		this.formAIEService.delete(formAIE);
		this.formIN.getListaFormAIE().remove(formAIE);
		FacesMessageUtil.addInfoContextFacesMessage("Form AIE exclu�do com sucesso", "");
	}
	
	public void removerFormMormo(FormMormo formMormo){
		this.formMormoService.delete(formMormo);
		this.formIN.getListaFormMormo().remove(formMormo);
		FacesMessageUtil.addInfoContextFacesMessage("Form Mormo exclu�do com sucesso", "");
	}
	
	public void removerFormMaleina(FormMaleina formMaleina){
		this.formMaleinaService.delete(formMaleina);
		this.formIN.getListaFormMaleina().remove(formMaleina);
		FacesMessageUtil.addInfoContextFacesMessage("Form Maleina exclu�do com sucesso", "");
	}
	
	public void removerFormEQ(FormEQ formEQ){
		this.formEQService.delete(formEQ);
		this.formIN.getListaFormEQ().remove(formEQ);
		FacesMessageUtil.addInfoContextFacesMessage("Form EQ exclu�do com sucesso", "");
	}
	
	public void removerResenho(Resenho resenho){
		this.resenhoService.delete(resenho);
		this.formIN.getListaResenho().remove(resenho);
		FacesMessageUtil.addInfoContextFacesMessage("Resenho exclu�do com sucesso", "");
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public FormIN getFormIN() {
		return formIN;
	}

	public void setFormIN(FormIN formIN) {
		this.formIN = formIN;
	}

}
