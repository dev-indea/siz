package br.gov.mt.indea.sistemaformin.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.inject.Inject;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import br.gov.mt.indea.sistemaformin.entity.TipoChavePrincipalTermoFiscalizacao;
import br.gov.mt.indea.sistemaformin.enums.Dominio.AtivoInativo;
import br.gov.mt.indea.sistemaformin.exception.ApplicationException;

@Stateless
public class TipoChavePrincipalTermoFiscalizacaoDAO extends DAO<TipoChavePrincipalTermoFiscalizacao> {
	
	@Inject
	private EntityManager em;
	
	public TipoChavePrincipalTermoFiscalizacaoDAO() {
		super(TipoChavePrincipalTermoFiscalizacao.class);
	}

	@Override
	public EntityManager getEntityManager() {
		return this.em;
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void add(TipoChavePrincipalTermoFiscalizacao tipoChavePrincipalTermoFiscalizacao) throws ApplicationException{
		tipoChavePrincipalTermoFiscalizacao = this.getEntityManager().merge(tipoChavePrincipalTermoFiscalizacao);
		super.add(tipoChavePrincipalTermoFiscalizacao);
	}
	
	public List<TipoChavePrincipalTermoFiscalizacao> findAllByStatus(AtivoInativo status) throws ApplicationException{
		try{
			CriteriaBuilder cb = this.getEntityManager().getCriteriaBuilder();
			CriteriaQuery<TipoChavePrincipalTermoFiscalizacao> query = cb.createQuery(TipoChavePrincipalTermoFiscalizacao.class);
			
			Root<TipoChavePrincipalTermoFiscalizacao> t = query.from(TipoChavePrincipalTermoFiscalizacao.class);
			
			query.where(cb.equal(t.get("status"), status));
			query.orderBy(cb.asc(t.get("nome")));
			
			return this.getEntityManager().createQuery(query).getResultList();
		}catch(Exception e){
			throw new ApplicationException("N�o foi poss�vel buscar o registro", e);
		}
	}

}
