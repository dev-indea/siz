package br.gov.mt.indea.sistemaformin.entity;

import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.envers.Audited;

@Audited
@Entity
@DiscriminatorValue("ovinos")
public class DetalhesInformacoesOvinos_FormCOM extends AbstractDetalhesInformacoesDeAnimais_FormCOM implements Cloneable{
	
	private static final long serialVersionUID = 1355044082455412390L;

	@ManyToOne
	@JoinColumn(name="id_informacoes_ovinos")
	private InformacoesOvinos_FormCOM informacoesOvinos;

	public InformacoesOvinos_FormCOM getInformacoesOvinos() {
		return informacoesOvinos;
	}

	public void setInformacoesOvinos(InformacoesOvinos_FormCOM informacoesOvinos) {
		this.informacoesOvinos = informacoesOvinos;
	}
	
	public Long getAcumuladoQuantidadeNovosCasosConfirmados(){
		Long total = null;
		DetalhesInformacoesOvinos detalhesFormIN = null;
		
		List<DetalhesInformacoesOvinos> detalhes = this.getInformacoesOvinos().getFormCOM().getFormIN().getOvinos().getDetalhes();
		for (DetalhesInformacoesOvinos detalhe : detalhes) {
			if (detalhe.getFaixaEtaria().equals(this.getFaixaEtaria()))
				detalhesFormIN = detalhe;
		}
		
		if (detalhesFormIN.getQuantidadeDoentesConfirmados() != null || this.getQuantidadeNovosCasosConfirmados() != null)
			total = new Long(0);
		
		if (detalhesFormIN.getQuantidadeDoentesConfirmados() != null)
			total += detalhesFormIN.getQuantidadeDoentesConfirmados();
		
		if (this.getQuantidadeNovosCasosConfirmados() != null)
			total += this.getQuantidadeNovosCasosConfirmados();
		
		// adicionar todos os valores dos formcoms com numera��o abaixo do formcom atual
		DetalhesInformacoesOvinos_FormCOM detalhesFormCOM = null;
		
		for (FormCOM formCOM : this.getInformacoesOvinos().getFormCOM().getFormIN().getListaFormCOM()) {
			if (this.getInformacoesOvinos().getFormCOM().getNumeroInvestigacao() == null || formCOM.getNumeroInvestigacao() < this.getInformacoesOvinos().getFormCOM().getNumeroInvestigacao()){
				
				List<DetalhesInformacoesOvinos_FormCOM> detalhesOvinosFormCOM = formCOM.getOvinos().getDetalhes();
				for (DetalhesInformacoesOvinos_FormCOM detalhe : detalhesOvinosFormCOM) {
					if (detalhe.getFaixaEtaria().equals(this.getFaixaEtaria()))
						detalhesFormCOM = detalhe;
				}
				
				if (total == null){
					if (detalhesFormCOM.getQuantidadeNovosCasosConfirmados() != null){
						total = new Long(0);
					}
				}
				
				if (detalhesFormCOM.getQuantidadeNovosCasosConfirmados() != null){
					total += detalhesFormCOM.getQuantidadeNovosCasosConfirmados();
				}
				
			}
		}
		
		return total;
	}
	
	public Long getAcumuladoQuantidadeNovosCasosProvaveis(){
		Long total = null;
		DetalhesInformacoesOvinos detalhesFormIN = null;
		
		List<DetalhesInformacoesOvinos> detalhes = this.getInformacoesOvinos().getFormCOM().getFormIN().getOvinos().getDetalhes();
		for (DetalhesInformacoesOvinos detalhe : detalhes) {
			if (detalhe.getFaixaEtaria().equals(this.getFaixaEtaria()))
				detalhesFormIN = detalhe;
		}
		
		if (detalhesFormIN.getQuantidadeDoentesProvaveis() != null || this.getQuantidadeNovosCasosProvaveis() != null)
			total = new Long(0);
		
		if (detalhesFormIN.getQuantidadeDoentesProvaveis() != null)
			total += detalhesFormIN.getQuantidadeDoentesProvaveis();
		
		if (this.getQuantidadeNovosCasosProvaveis() != null)
			total += this.getQuantidadeNovosCasosProvaveis();
		
		// adicionar todos os valores dos formcoms com numera��o abaixo do formcom atual
		DetalhesInformacoesOvinos_FormCOM detalhesFormCOM = null;
		
		for (FormCOM formCOM : this.getInformacoesOvinos().getFormCOM().getFormIN().getListaFormCOM()) {
			if (this.getInformacoesOvinos().getFormCOM().getNumeroInvestigacao() == null || formCOM.getNumeroInvestigacao() < this.getInformacoesOvinos().getFormCOM().getNumeroInvestigacao()){
				
				List<DetalhesInformacoesOvinos_FormCOM> detalhesOvinosFormCOM = formCOM.getOvinos().getDetalhes();
				for (DetalhesInformacoesOvinos_FormCOM detalhe : detalhesOvinosFormCOM) {
					if (detalhe.getFaixaEtaria().equals(this.getFaixaEtaria()))
						detalhesFormCOM = detalhe;
				}
				
				if (total == null){
					if (detalhesFormCOM.getQuantidadeNovosCasosProvaveis() != null){
						total = new Long(0);
					}
				}
				
				if (detalhesFormCOM.getQuantidadeNovosCasosProvaveis() != null){
					total += detalhesFormCOM.getQuantidadeNovosCasosProvaveis();
				}
				
			}
		}
		
		return total;
	}
	
	public Long getAcumuladoQuantidadeNovosMortos(){
		Long total = null;
		DetalhesInformacoesOvinos detalhesFormIN = null;
		
		List<DetalhesInformacoesOvinos> detalhes = this.getInformacoesOvinos().getFormCOM().getFormIN().getOvinos().getDetalhes();
		for (DetalhesInformacoesOvinos detalhe : detalhes) {
			if (detalhe.getFaixaEtaria().equals(this.getFaixaEtaria()))
				detalhesFormIN = detalhe;
		}
		
		if (detalhesFormIN.getQuantidadeMortos() != null || this.getQuantidadeNovosMortos() != null)
			total = new Long(0);
		
		if (detalhesFormIN.getQuantidadeMortos() != null)
			total += detalhesFormIN.getQuantidadeMortos();
		
		if (this.getQuantidadeNovosMortos() != null)
			total += this.getQuantidadeNovosMortos();
		
		// adicionar todos os valores dos formcoms com numera��o abaixo do formcom atual
		DetalhesInformacoesOvinos_FormCOM detalhesFormCOM = null;
		
		for (FormCOM formCOM : this.getInformacoesOvinos().getFormCOM().getFormIN().getListaFormCOM()) {
			if (this.getInformacoesOvinos().getFormCOM().getNumeroInvestigacao() == null || formCOM.getNumeroInvestigacao() < this.getInformacoesOvinos().getFormCOM().getNumeroInvestigacao()){
				
				List<DetalhesInformacoesOvinos_FormCOM> detalhesOvinosFormCOM = formCOM.getOvinos().getDetalhes();
				for (DetalhesInformacoesOvinos_FormCOM detalhe : detalhesOvinosFormCOM) {
					if (detalhe.getFaixaEtaria().equals(this.getFaixaEtaria()))
						detalhesFormCOM = detalhe;
				}
				
				if (total == null){
					if (detalhesFormCOM.getQuantidadeNovosMortos() != null){
						total = new Long(0);
					}
				}
				
				if (detalhesFormCOM.getQuantidadeNovosMortos() != null){
					total += detalhesFormCOM.getQuantidadeNovosMortos();
				}
				
			}
		}
		
		return total;
	}
	
	public Long getAcumuladoQuantidadeNovosAbatidos(){
		Long total = null;
		DetalhesInformacoesOvinos detalhesFormIN = null;
		
		List<DetalhesInformacoesOvinos> detalhes = this.getInformacoesOvinos().getFormCOM().getFormIN().getOvinos().getDetalhes();
		for (DetalhesInformacoesOvinos detalhe : detalhes) {
			if (detalhe.getFaixaEtaria().equals(this.getFaixaEtaria()))
				detalhesFormIN = detalhe;
		}
		
		if (detalhesFormIN.getQuantidadeAbatidos() != null || this.getQuantidadeNovosAbatidos() != null)
			total = new Long(0);
		
		if (detalhesFormIN.getQuantidadeAbatidos() != null)
			total += detalhesFormIN.getQuantidadeAbatidos();
		
		if (this.getQuantidadeNovosAbatidos() != null)
			total += this.getQuantidadeNovosAbatidos();
		
		// adicionar todos os valores dos formcoms com numera��o abaixo do formcom atual
		DetalhesInformacoesOvinos_FormCOM detalhesFormCOM = null;
			
		for (FormCOM formCOM : this.getInformacoesOvinos().getFormCOM().getFormIN().getListaFormCOM()) {
			if (this.getInformacoesOvinos().getFormCOM().getNumeroInvestigacao() == null || formCOM.getNumeroInvestigacao() < this.getInformacoesOvinos().getFormCOM().getNumeroInvestigacao()){
				
				List<DetalhesInformacoesOvinos_FormCOM> detalhesOvinosFormCOM = formCOM.getOvinos().getDetalhes();
				for (DetalhesInformacoesOvinos_FormCOM detalhe : detalhesOvinosFormCOM) {
					if (detalhe.getFaixaEtaria().equals(this.getFaixaEtaria()))
						detalhesFormCOM = detalhe;
				}
				
				if (total == null){
					if (detalhesFormCOM.getQuantidadeNovosAbatidos() != null){
						total = new Long(0);
					}
				}
				
				if (detalhesFormCOM.getQuantidadeNovosAbatidos() != null){
					total += detalhesFormCOM.getQuantidadeNovosAbatidos();
				}
				
			}
		}
		
		return total;
	}
	
	public Long getAcumuladoQuantidadeNovosDestruidos(){
		Long total = null;
		DetalhesInformacoesOvinos detalhesFormIN = null;
		
		List<DetalhesInformacoesOvinos> detalhes = this.getInformacoesOvinos().getFormCOM().getFormIN().getOvinos().getDetalhes();
		for (DetalhesInformacoesOvinos detalhe : detalhes) {
			if (detalhe.getFaixaEtaria().equals(this.getFaixaEtaria()))
				detalhesFormIN = detalhe;
		}
		
		if (detalhesFormIN.getQuantidadeDestruidos() != null || this.getQuantidadeNovosDestruidos() != null)
			total = new Long(0);
		
		if (detalhesFormIN.getQuantidadeDestruidos() != null)
			total += detalhesFormIN.getQuantidadeDestruidos();
		
		if (this.getQuantidadeNovosDestruidos() != null)
			total += this.getQuantidadeNovosDestruidos();
		
		// adicionar todos os valores dos formcoms com numera��o abaixo do formcom atual
		DetalhesInformacoesOvinos_FormCOM detalhesFormCOM = null;
		
		for (FormCOM formCOM : this.getInformacoesOvinos().getFormCOM().getFormIN().getListaFormCOM()) {
			if (this.getInformacoesOvinos().getFormCOM().getNumeroInvestigacao() == null || formCOM.getNumeroInvestigacao() < this.getInformacoesOvinos().getFormCOM().getNumeroInvestigacao()){
				
				List<DetalhesInformacoesOvinos_FormCOM> detalhesOvinosFormCOM = formCOM.getOvinos().getDetalhes();
				for (DetalhesInformacoesOvinos_FormCOM detalhe : detalhesOvinosFormCOM) {
					if (detalhe.getFaixaEtaria().equals(this.getFaixaEtaria()))
						detalhesFormCOM = detalhe;
				}
				
				if (total == null){
					if (detalhesFormCOM.getQuantidadeNovosDestruidos() != null){
						total = new Long(0);
					}
				}
				
				if (detalhesFormCOM.getQuantidadeNovosDestruidos() != null){
					total += detalhesFormCOM.getQuantidadeNovosDestruidos();
				}
				
			}
		}
		
		return total;
	}
}