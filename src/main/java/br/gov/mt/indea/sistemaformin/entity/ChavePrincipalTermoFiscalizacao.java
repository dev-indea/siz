package br.gov.mt.indea.sistemaformin.entity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

import br.gov.mt.indea.sistemaformin.enums.Dominio.AtivoInativo;

@Audited
@Entity(name="chave_principal_termo")
public class ChavePrincipalTermoFiscalizacao extends BaseEntity<Long>{
	
	private static final long serialVersionUID = 4474406491616877949L;

	@Id
	@SequenceGenerator(name="chave_principal_termo_seq", sequenceName="chave_principal_termo_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="chave_principal_termo_seq")
	private Long id;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_cadastro")
	private Calendar dataCadastro;
	
	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="id_tipo_chave_principal_visit")
	private TipoChavePrincipalTermoFiscalizacao tipoChavePrincipalTermoFiscalizacao;
	
	@OneToMany(mappedBy="chavePrincipalTermoFiscalizacao", cascade={CascadeType.ALL}, orphanRemoval=true, fetch=FetchType.LAZY)
	private List<ChaveSecundariaTermoFiscalizacao> listChaveSecundariaTermoFiscalizacao = new ArrayList<ChaveSecundariaTermoFiscalizacao>();
	
	@ManyToOne
	@JoinColumn(name="id_principal_termo")
	private TermoFiscalizacao termoFiscalizacao;
	
	public String getListaChaveSecundariaTermoFiscalizacaoAsString(){
		StringBuilder sb = new StringBuilder();
		
		if (this.listChaveSecundariaTermoFiscalizacao != null && !this.listChaveSecundariaTermoFiscalizacao.isEmpty()){
			for (ChaveSecundariaTermoFiscalizacao chaveSecundariaTermoFiscalizacao : this.listChaveSecundariaTermoFiscalizacao) {
				sb.append(chaveSecundariaTermoFiscalizacao.getTipoChaveSecundariaTermoFiscalizacao().getNome()).append(", ");
			}
			
			sb = new StringBuilder(sb.toString().substring(0, sb.length()-2));
		}
		
		return sb.toString();
	}
	
	public boolean isChavePrincipalAtiva(){
		return this.tipoChavePrincipalTermoFiscalizacao.getStatus().equals(AtivoInativo.ATIVO);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Calendar getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Calendar dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public TipoChavePrincipalTermoFiscalizacao getTipoChavePrincipalTermoFiscalizacao() {
		return tipoChavePrincipalTermoFiscalizacao;
	}

	public void setTipoChavePrincipalTermoFiscalizacao(
			TipoChavePrincipalTermoFiscalizacao tipoChavePrincipalTermoFiscalizacao) {
		this.tipoChavePrincipalTermoFiscalizacao = tipoChavePrincipalTermoFiscalizacao;
	}

	public List<ChaveSecundariaTermoFiscalizacao> getListChaveSecundariaTermoFiscalizacao() {
		return listChaveSecundariaTermoFiscalizacao;
	}

	public void setListChaveSecundariaTermoFiscalizacao(
			List<ChaveSecundariaTermoFiscalizacao> listChaveSecundariaTermoFiscalizacao) {
		this.listChaveSecundariaTermoFiscalizacao = listChaveSecundariaTermoFiscalizacao;
	}

	public TermoFiscalizacao getTermoFiscalizacao() {
		return termoFiscalizacao;
	}

	public void setTermoFiscalizacao(
			TermoFiscalizacao termoFiscalizacao) {
		this.termoFiscalizacao = termoFiscalizacao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ChavePrincipalTermoFiscalizacao other = (ChavePrincipalTermoFiscalizacao) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}