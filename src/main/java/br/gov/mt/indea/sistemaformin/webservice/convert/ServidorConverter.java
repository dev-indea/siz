package br.gov.mt.indea.sistemaformin.webservice.convert;

import javax.inject.Inject;

import br.gov.mt.indea.sistemaformin.entity.Municipio;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoUnidade;
import br.gov.mt.indea.sistemaformin.service.MunicipioService;
import br.gov.mt.indea.sistemaformin.service.ULEService;
import br.gov.mt.indea.sistemaformin.webservice.entity.Endereco;
import br.gov.mt.indea.sistemaformin.webservice.entity.Profissao;
import br.gov.mt.indea.sistemaformin.webservice.entity.Servidor;
import br.gov.mt.indea.sistemaformin.webservice.entity.TipoLogradouro;
import br.gov.mt.indea.sistemaformin.webservice.entity.ULE;

public class ServidorConverter {
	
	@Inject
	private MunicipioService municipioService;
	
	@Inject
	private ULEService uleService;
	
	public Servidor fromModelToEntity(br.gov.mt.indea.sistemaformin.webservice.model.Servidor modelo){
		Servidor servidor = new Servidor();
		
		if (modelo == null)
			return null;
		
		if (modelo.getPessoa() != null){
			servidor.setCodigoServidorSVO(modelo.getPessoa().getId());
			
			servidor.setApelido(modelo.getPessoa().getApelido());
			servidor.setCpf(modelo.getPessoa().getCpfCnpj());
			servidor.setEmail(modelo.getPessoa().getEmail());
			servidor.setNome(modelo.getPessoa().getNome());
			servidor.setTipoPessoa(modelo.getPessoa().getTipoPessoa());
			
			if (modelo.getPessoa().getEndereco() != null){
				Endereco enderecoPropriedade = new Endereco();
				
				enderecoPropriedade.setBairro(modelo.getPessoa().getEndereco().getBairro());
				enderecoPropriedade.setCep(modelo.getPessoa().getEndereco().getCep());
				enderecoPropriedade.setComplemento(modelo.getPessoa().getEndereco().getComplemento());
				enderecoPropriedade.setLogradouro(modelo.getPessoa().getEndereco().getLogradouro());
				enderecoPropriedade.setNumero(modelo.getPessoa().getEndereco().getNumero());
				enderecoPropriedade.setReferencia(modelo.getPessoa().getEndereco().getReferencia());
				enderecoPropriedade.setTelefone(modelo.getPessoa().getEndereco().getTelefone());
				
				if (modelo.getPessoa().getEndereco().getTipoLogradouro() != null){
					TipoLogradouro tipoLogradouroEnderecoPropriedade = new TipoLogradouro();
					tipoLogradouroEnderecoPropriedade.setNome(modelo.getPessoa().getEndereco().getTipoLogradouro().getNome());
					
					enderecoPropriedade.setTipoLogradouro(tipoLogradouroEnderecoPropriedade);
				}
				
				if (modelo.getPessoa().getEndereco().getMunicipio() != null)
					enderecoPropriedade.setMunicipio(this.findMunicipio(modelo.getPessoa().getEndereco().getMunicipio()));
				
				servidor.setEndereco(enderecoPropriedade);
			}
			
			if (modelo.getPessoa().getPessoaFisica() != null){
				servidor.setDataNascimento(modelo.getPessoa().getPessoaFisica().getDataNascimento());
				servidor.setEmissorRg(modelo.getPessoa().getPessoaFisica().getEmissorRg());
				servidor.setMunicipioNascimento(this.findMunicipio(modelo.getPessoa().getPessoaFisica().getMunicipioNascimento()));
				servidor.setNomeMae(modelo.getPessoa().getPessoaFisica().getNomeMae());
				servidor.setRg(modelo.getPessoa().getPessoaFisica().getRg());
				servidor.setSexo(modelo.getPessoa().getPessoaFisica().getSexo());
			}
		}

		if (modelo.getUle() != null){
			servidor.setUle(this.findULE(modelo.getUle()));
		}
		
		if (modelo.getProfissao() != null){
			Profissao profissao = new Profissao();
			
			profissao.setNome(modelo.getProfissao().getNome());
			
			servidor.setProfissao(profissao);
		}
		
		servidor.setMatricula(modelo.getMatricula());
		
		return servidor;
	}

	private Municipio findMunicipio(br.gov.mt.indea.sistemaformin.webservice.model.Municipio municipioModel) {
		if (municipioModel == null)
			return null;
		
		String codgIBGE = municipioModel.getId() + "";
		
		Municipio municipio = municipioService.findByCodgIBGE(codgIBGE.substring(0, 2), codgIBGE.substring(2, codgIBGE.length()));
		
		return municipio;
	}
	
	private ULE findULE(br.gov.mt.indea.sistemaformin.webservice.model.ULE uleModel){
		if (uleModel == null)
			return null;
		
		TipoUnidade tipoUnidade = null;
		if (uleModel .getUrs() == null)
			tipoUnidade = TipoUnidade.URS;
		else
			tipoUnidade = TipoUnidade.ULE;
		
		ULE ule = uleService.findByNome(uleModel.getNome(), tipoUnidade);
		
		if (ule == null)
			if (tipoUnidade.equals(TipoUnidade.URS)){
				tipoUnidade = TipoUnidade.ULE;
				ule = uleService.findByNome(uleModel.getNome(), tipoUnidade);
			}
		
		return ule;
	}

}
