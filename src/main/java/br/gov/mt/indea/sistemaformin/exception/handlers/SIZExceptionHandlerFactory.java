package br.gov.mt.indea.sistemaformin.exception.handlers;

import javax.faces.context.ExceptionHandler;
import javax.faces.context.ExceptionHandlerFactory;

import org.primefaces.application.exceptionhandler.PrimeExceptionHandlerFactory;

public class SIZExceptionHandlerFactory extends PrimeExceptionHandlerFactory {

	private ExceptionHandlerFactory parent;

	public SIZExceptionHandlerFactory(ExceptionHandlerFactory parent) {
		super(parent);
		this.parent = parent;
	}

    @Override
    public ExceptionHandler getExceptionHandler() {
        ExceptionHandler result = parent.getExceptionHandler();
        result = new SIZExceptionHandler(result);

        return result;
    }
	
     
}
