package br.gov.mt.indea.sistemaformin.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.envers.Audited;

@Audited
@Entity
@DiscriminatorValue("caprinos")
public class DetalhesInformacoesCaprinos extends AbstractDetalhesInformacoesDeAnimais implements Cloneable{
	
	private static final long serialVersionUID = 1355044082455412390L;

	@ManyToOne
	@JoinColumn(name="id_informacoes_caprinos")
	private InformacoesCaprinos informacoesCaprinos;

	public InformacoesCaprinos getInformacoesCaprinos() {
		return informacoesCaprinos;
	}

	public void setInformacoesCaprinos(InformacoesCaprinos informacoesCaprinos) {
		this.informacoesCaprinos = informacoesCaprinos;
	}
	
	protected Object clone(InformacoesCaprinos informacoesCaprinos) throws CloneNotSupportedException{
		DetalhesInformacoesCaprinos cloned = (DetalhesInformacoesCaprinos) super.clone();
		
		cloned.setId(null);
		cloned.setInformacoesCaprinos(informacoesCaprinos);
		
		return cloned;
	}

}