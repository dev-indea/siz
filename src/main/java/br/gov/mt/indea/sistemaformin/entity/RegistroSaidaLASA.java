package br.gov.mt.indea.sistemaformin.entity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoSaida;

@Entity
@Table(name = "registro_saida_lasa")
@Audited
public class RegistroSaidaLASA extends BaseEntity<Long>{

	private static final long serialVersionUID = 6945160424281951286L;
	
	@Id
	@GeneratedValue(generator = "registro_saida_lasa_seq", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "registro_saida_lasa_seq", sequenceName = "registro_saida_lasa_seq", allocationSize = 1)
	private Long id;
	
	@Column(name = "data_cadastro")
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar dataCadastro;
	
	@Column(name = "data_saida")
	@Temporal(TemporalType.DATE)
	private Calendar dataSaida;
	
	@ElementCollection(targetClass = TipoSaida.class, fetch=FetchType.LAZY)
	@JoinTable(name = "registro_saida_lasa_tipo_saida", joinColumns = @JoinColumn(name = "id_registro_saida_lasa", nullable = false))
	@Column(name = "id_tipo_saida")
	@Enumerated(EnumType.STRING)	
	private List<TipoSaida> listaTipoSaida = new ArrayList<TipoSaida>();
	
	@Column(length = 400)
	private String observacao;
	
	@Column(name="numero_ci")
	private String numeroCI;
	
	@OneToOne(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
	@JoinColumn(name="id_registro_entrada_lasa")
	private RegistroEntradaLASA registroEntradaLASA;
	
	public RegistroSaidaLASA() {
	}
	
	public RegistroSaidaLASA(RegistroEntradaLASA registroEntradaLASA) {
		this.registroEntradaLASA = registroEntradaLASA;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Calendar getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Calendar dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Calendar getDataSaida() {
		return dataSaida;
	}

	public void setDataSaida(Calendar dataSaida) {
		this.dataSaida = dataSaida;
	}

	public List<TipoSaida> getListaTipoSaida() {
		return listaTipoSaida;
	}

	public void setListaTipoSaida(List<TipoSaida> listaTipoSaida) {
		this.listaTipoSaida = listaTipoSaida;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public RegistroEntradaLASA getRegistroEntradaLASA() {
		return registroEntradaLASA;
	}

	public void setRegistroEntradaLASA(RegistroEntradaLASA registroEntradaLASA) {
		this.registroEntradaLASA = registroEntradaLASA;
	}

	public String getNumeroCI() {
		return numeroCI;
	}

	public void setNumeroCI(String numeroCI) {
		this.numeroCI = numeroCI;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RegistroSaidaLASA other = (RegistroSaidaLASA) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
