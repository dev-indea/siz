package br.gov.mt.indea.sistemaformin.webservice.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Conselho implements Serializable{

	private static final long serialVersionUID = 1812036083210342302L;

	private Long idTable;
	
	private String id;
	
	private String nome;
	
	private Integer codigoPga;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getCodigoPga() {
		return codigoPga;
	}

	public void setCodigoPga(Integer codigoPga) {
		this.codigoPga = codigoPga;
	}

	public Long getIdTable() {
		return idTable;
	}

	public void setIdTable(Long idTable) {
		this.idTable = idTable;
	}
	
}
