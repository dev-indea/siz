package br.gov.mt.indea.sistemaformin.webservice.client;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;

import javax.inject.Inject;

import com.google.gson.Gson;

import br.gov.mt.indea.sistemaformin.exception.ApplicationRuntimeException;
import br.gov.mt.indea.sistemaformin.webservice.convert.EnderecoConverter;
import br.gov.mt.indea.sistemaformin.webservice.entity.Endereco;
import br.gov.mt.indea.sistemaformin.webservice.entity.ViaCep;

public class CepService implements Serializable{
	
	private static final long serialVersionUID = 2385971446574183012L;
	
	@Inject
	private EnderecoConverter enderecoConverter;

    public Endereco buscarCep(String cep) {
    	ViaCep viaCep = null;
    	cep = cep.replace("-", "");
        try {
            URL url = new URL("http://viacep.com.br/ws/"+ cep +"/json");
//            Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("192.168.201.253", 3128));
//            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection(proxy);
            
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            
            httpURLConnection.setRequestMethod("GET");
            
            InputStream is = httpURLConnection.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            
            Gson gson = new Gson();
            viaCep = gson.fromJson(br, ViaCep.class);
            
            if (viaCep.equals(new ViaCep()))
            	viaCep = null;
            
        } catch (Exception e) {
            throw new ApplicationRuntimeException("Formato de CEP inv�lido");
        }

        return enderecoConverter.fromModelToEntity(viaCep);
    }

}