package br.gov.mt.indea.sistemaformin.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.mt.indea.sistemaformin.entity.TipoChaveSecundariaVisitaPropriedadeRural;
import br.gov.mt.indea.sistemaformin.enums.Dominio.AtivoInativo;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class TipoChaveSecundariaVisitaPropriedadeRuralService extends PaginableService<TipoChaveSecundariaVisitaPropriedadeRural, Long> {
	
	private static final Logger log = LoggerFactory.getLogger(TipoChaveSecundariaVisitaPropriedadeRuralService.class);

	protected TipoChaveSecundariaVisitaPropriedadeRuralService() {
		super(TipoChaveSecundariaVisitaPropriedadeRural.class);
	}
	
	public TipoChaveSecundariaVisitaPropriedadeRural findByIdFetchAll(Long id){
		TipoChaveSecundariaVisitaPropriedadeRural tipoChaveSecundariaVisitaPropriedadeRural;
		
		StringBuilder sql = new StringBuilder();
		sql.append("select v ")
		   .append("  from TipoChaveSecundariaVisitaPropriedadeRural v ")
		   .append(" where v.id = :id ");
		
		Query query = getSession().createQuery(sql.toString()).setLong("id", id);
		tipoChaveSecundariaVisitaPropriedadeRural = (TipoChaveSecundariaVisitaPropriedadeRural) query.uniqueResult();
		
		return tipoChaveSecundariaVisitaPropriedadeRural;
	}
	
	@SuppressWarnings("unchecked")
	public List<TipoChaveSecundariaVisitaPropriedadeRural> findAllByStatus(AtivoInativo status) {
		List<TipoChaveSecundariaVisitaPropriedadeRural> tipoChaveSecundariaVisitaPropriedadeRural;
		
		StringBuilder sql = new StringBuilder();
		sql.append("select v ")
		   .append("  from TipoChaveSecundariaVisitaPropriedadeRural v ")
		   .append(" where v.status = :status ");
		
		Query query = getSession().createQuery(sql.toString()).setParameter("status", status);
		tipoChaveSecundariaVisitaPropriedadeRural = query.list();
		
		return tipoChaveSecundariaVisitaPropriedadeRural;
	}
	
	@Override
	public void saveOrUpdate(TipoChaveSecundariaVisitaPropriedadeRural tipoChaveSecundariaVisitaPropriedadeRural) {
		super.saveOrUpdate(tipoChaveSecundariaVisitaPropriedadeRural);
		
		log.info("Salvando Tipo Chave Secundaria Visita Propriedade Rural {}", tipoChaveSecundariaVisitaPropriedadeRural.getId());
	}
	
	@Override
	public void delete(TipoChaveSecundariaVisitaPropriedadeRural tipoChaveSecundariaVisitaPropriedadeRural) {
		super.delete(tipoChaveSecundariaVisitaPropriedadeRural);
		
		log.info("Removendo Tipo Chave Secundaria Visita Propriedade Rural {}", tipoChaveSecundariaVisitaPropriedadeRural.getId());
	}
	
	@Override
	public void validar(TipoChaveSecundariaVisitaPropriedadeRural TipoChaveSecundariaVisitaPropriedadeRural) {

	}

	@Override
	public void validarPersist(TipoChaveSecundariaVisitaPropriedadeRural TipoChaveSecundariaVisitaPropriedadeRural) {

	}

	@Override
	public void validarMerge(TipoChaveSecundariaVisitaPropriedadeRural TipoChaveSecundariaVisitaPropriedadeRural) {

	}

	@Override
	public void validarDelete(TipoChaveSecundariaVisitaPropriedadeRural TipoChaveSecundariaVisitaPropriedadeRural) {

	}

}
