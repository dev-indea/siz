package br.gov.mt.indea.sistemaformin.webservice.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
public class ProprietarioRural implements Serializable{
	
	private static final long serialVersionUID = 460668477984859948L;

	@XmlElement(name = "proprietario")
	private Produtor proprietario;
	
	private Integer id;

	public Produtor getProprietario() {
		return proprietario;
	}

	public void setProprietario(Produtor produtor) {
		this.proprietario = produtor;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
}