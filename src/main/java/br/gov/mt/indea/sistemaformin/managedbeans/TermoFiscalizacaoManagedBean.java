package br.gov.mt.indea.sistemaformin.managedbeans;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.Past;

import org.primefaces.PrimeFaces;

import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLBeanName;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;

import br.gov.mt.indea.sistemaformin.entity.ChavePrincipalTermoFiscalizacao;
import br.gov.mt.indea.sistemaformin.entity.ChaveSecundariaTermoFiscalizacao;
import br.gov.mt.indea.sistemaformin.entity.Municipio;
import br.gov.mt.indea.sistemaformin.entity.TermoFiscalizacao;
import br.gov.mt.indea.sistemaformin.entity.TipoChavePrincipalTermoFiscalizacao;
import br.gov.mt.indea.sistemaformin.entity.TipoChaveSecundariaTermoFiscalizacao;
import br.gov.mt.indea.sistemaformin.entity.dto.TermoFiscalizacaoDTO;
import br.gov.mt.indea.sistemaformin.enums.Dominio.AtivoInativo;
import br.gov.mt.indea.sistemaformin.exception.ApplicationException;
import br.gov.mt.indea.sistemaformin.service.LazyObjectDataModel;
import br.gov.mt.indea.sistemaformin.service.MunicipioService;
import br.gov.mt.indea.sistemaformin.service.TermoFiscalizacaoService;
import br.gov.mt.indea.sistemaformin.service.TipoChavePrincipalTermoFiscalizacaoService;
import br.gov.mt.indea.sistemaformin.service.TipoChaveSecundariaTermoFiscalizacaoService;
import br.gov.mt.indea.sistemaformin.util.FacesMessageUtil;
import br.gov.mt.indea.sistemaformin.webservice.client.ClientWebServiceRevenda;
import br.gov.mt.indea.sistemaformin.webservice.client.ClientWebServiceVeterinario;
import br.gov.mt.indea.sistemaformin.webservice.entity.Revenda;
import br.gov.mt.indea.sistemaformin.webservice.entity.Veterinario;

@Named("termoFiscalizacaoManagedBean")
@ViewScoped
@URLBeanName("termoFiscalizacaoManagedBean")
@URLMappings(mappings = {
		@URLMapping(id = "pesquisarTermoFiscalizacao", pattern = "/termoFiscalizacao/pesquisar", viewId = "/pages/termoFiscalizacao/lista.jsf"),
		@URLMapping(id = "incluirTermoFiscalizacao", pattern = "/termoFiscalizacao/incluir", viewId = "/pages/termoFiscalizacao/novo.jsf"),
		@URLMapping(id = "alterarTermoFiscalizacao", pattern = "/termoFiscalizacao/alterar/#{termoFiscalizacaoManagedBean.id}", viewId = "/pages/termoFiscalizacao/novo.jsf")})
public class TermoFiscalizacaoManagedBean implements Serializable {

	private static final long serialVersionUID = 1760023349204758559L;
	
	private Long id;

	@Inject
	private MunicipioService municipioService;
	
	@Inject
	private TermoFiscalizacaoService termoFiscalizacaoService;
	
	@Inject
	private TipoChavePrincipalTermoFiscalizacaoService tipoChavePrincipalTermoFiscalizacaoService;
	
	@Inject
	private TipoChaveSecundariaTermoFiscalizacaoService tipoChaveSecundariaTermoFiscalizacaoService;
	
	@Inject
	private TermoFiscalizacao termoFiscalizacao;
	
	@Inject
	private TermoFiscalizacaoDTO termoFiscalizacaoDTO;
	
	@Past(message="Data precisa ser menor ou igual a data atual")
	private Date dataVisita;
	
	private String cpfVeterinario;

	private String crmvVeterinario;

	private List<Veterinario> listaVeterinarios;
	
	@Inject
	private ClientWebServiceVeterinario clientWebServiceVeterinario;
	
	private boolean editandoChavePrincipalTermoFiscalizacao = false;
	
	private boolean visualizandoChavePrincipalTermoFiscalizacao = false;

	private ChavePrincipalTermoFiscalizacao chavePrincipalTermoFiscalizacao;

	private List<TipoChaveSecundariaTermoFiscalizacao> tipoChaveSecundariaTermoFiscalizacao = new ArrayList<TipoChaveSecundariaTermoFiscalizacao>();

	private List<TipoChaveSecundariaTermoFiscalizacao> listaTipoChaveSecundariaTermoFiscalizacao;
	
	@Inject
	private ClientWebServiceRevenda clientWebServiceRevenda;
	
	private String cnpjRevenda;
	
	private List<Revenda> listaRevendas;
	
	private LazyObjectDataModel<TermoFiscalizacao> listaTermoFiscalizacao;
	
	@PostConstruct
	private void init(){
		FacesMessageUtil.addInfoContextFacesMessage("Para termos de fiscaliza��o realizados a partir do dia 07/03/2024, deve ser utilizado a nova vers�o do SIZ dispon�vel em: ", "https://siz-v3.indea.mt.gov.br/");
		PrimeFaces current = PrimeFaces.current();
		current.executeScript("PF('modalNotificacaoSizMobile').show();");
	}
	
	public List<Municipio> getListaMunicipiosBusca(){
		return municipioService.findAllByUF(this.termoFiscalizacaoDTO.getUf());
	}
	
	public void buscarTermoFiscalizacao(){
		this.listaTermoFiscalizacao = new LazyObjectDataModel<TermoFiscalizacao>(this.termoFiscalizacaoService, this.termoFiscalizacaoDTO);
	}
	
	@URLAction(mappingId = "incluirTermoFiscalizacao", onPostback = false)
	public void novo(){
		limpar();
	}
	
	@URLAction(mappingId = "pesquisarTermoFiscalizacao", onPostback = false)
	public void pesquisar(){
		limpar();
		
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		String numeroTermoFiscalizacao = (String) request.getSession().getAttribute("numeroTermoFiscalizacao");
		
		if (numeroTermoFiscalizacao != null){
			this.termoFiscalizacaoDTO = new TermoFiscalizacaoDTO();
			this.termoFiscalizacaoDTO.setNumero(numeroTermoFiscalizacao);
			this.buscarTermoFiscalizacao();
			
			request.getSession().setAttribute("numeroFormIN", null);
		}
	}
	
	private void limpar() {
		this.termoFiscalizacao = new TermoFiscalizacao();
		this.dataVisita = null;
	}
	
	@URLAction(mappingId = "alterarTermoFiscalizacao", onPostback = false)
	public void editar(){
		this.termoFiscalizacao = termoFiscalizacaoService.findByIdFetchAll(this.getId());
		
		if (termoFiscalizacao.getDataVisita() != null)
			this.dataVisita = termoFiscalizacao.getDataVisita().getTime();
	}
	
	public String adicionar() throws IOException{
		
		boolean isTermoFiscalizacaoOK = true;
		
		if (this.termoFiscalizacao.getVeterinario() == null){
			FacesContext context = FacesContext.getCurrentInstance();
			
			context.addMessage("cadastro:fieldVeterinario:campoVeterinario", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Veterin�rio: valor � obrigat�rio.", "Veterin�rio: valor � obrigat�rio."));
			isTermoFiscalizacaoOK = false;
		}
		
		if (this.termoFiscalizacao.getRevenda() == null){
			FacesContext context = FacesContext.getCurrentInstance();
			
			context.addMessage("cadastro:fieldRevenda:campoRevenda", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Revenda: valor � obrigat�rio.", "Revenda: valor � obrigat�rio."));
			isTermoFiscalizacaoOK = false;
		}
		
		if (this.termoFiscalizacao.getListChavePrincipalTermoFiscalizacao() == null || this.termoFiscalizacao.getListChavePrincipalTermoFiscalizacao().isEmpty()){
			FacesContext context = FacesContext.getCurrentInstance();
			
			context.addMessage("cadastro:tableProgramas", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Programas sanit�rios e atividades: deve ser adicionado ao menos um.", "Programas sanit�rios e atividades: deve ser adicionado ao menos um."));
			isTermoFiscalizacaoOK = false;;
		}
		
		if (!isTermoFiscalizacaoOK)
			return "";
		
		if (this.dataVisita!= null){
			if (this.termoFiscalizacao.getDataVisita() == null)
				this.termoFiscalizacao.setDataVisita(Calendar.getInstance());
			this.termoFiscalizacao.getDataVisita().setTime(dataVisita);
		}else
			this.termoFiscalizacao.setDataVisita(null);
		
		if (termoFiscalizacao.getId() != null){
			this.termoFiscalizacaoService.saveOrUpdate(termoFiscalizacao);

			FacesMessageUtil.addInfoContextFacesMessage("Termo de Fiscalizacao n� " + this.termoFiscalizacao.getNumero() + " atualizado", "");
		}else{
			this.termoFiscalizacao.setDataCadastro(Calendar.getInstance());
			this.termoFiscalizacaoService.saveOrUpdate(termoFiscalizacao);

			FacesMessageUtil.addInfoContextFacesMessage("Termo de Fiscalizacao n� " + this.termoFiscalizacao.getNumero() + " adicionado", "");
		}
		
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		request.getSession().setAttribute("numeroTermoFiscalizacao", termoFiscalizacao.getNumero());
		
		limpar();
		return "pretty:pesquisarTermoFiscalizacao";
	}
	
	public void remover(TermoFiscalizacao termoFiscalizacao){
		this.termoFiscalizacaoService.delete(termoFiscalizacao);
		FacesMessageUtil.addInfoContextFacesMessage("Termo de Fiscaliza��o Rural n� " + termoFiscalizacao.getNumero() + " exclu�da com sucesso", "");
	}
	
	public void abrirTelaDeBuscaDeVeterinario(){
		this.cpfVeterinario = null;
		this.crmvVeterinario = null;
		this.listaVeterinarios = null;
	}
	
	public void buscarVeterinarioPorCpf(){
		this.listaVeterinarios = null;
		
		Veterinario v;
		try {
			v = clientWebServiceVeterinario.getVeterinarioByCpf(cpfVeterinario);
			
			if (v != null){
				this.listaVeterinarios = new ArrayList<Veterinario>();
				this.listaVeterinarios.add(v);
			}
		} catch (ApplicationException e) {
			FacesMessageUtil.addErrorContextFacesMessage("Erro", e.getLocalizedMessage());
			return;
		}
		
		if (this.listaVeterinarios == null || this.listaVeterinarios.isEmpty())
			FacesMessageUtil.addWarnContextFacesMessage("Nenhum veterin�rio foi encontrado", "");
	}
	
	public void buscarVeterinarioPorCrmv(){
		this.listaVeterinarios = null;
		
		Veterinario v;
		try {
			v = clientWebServiceVeterinario.getVeterinarioByCRMV(crmvVeterinario);
			
			if (v != null){
				this.listaVeterinarios = new ArrayList<Veterinario>();
				this.listaVeterinarios.add(v);
			}
		} catch (ApplicationException e) {
			FacesMessageUtil.addErrorContextFacesMessage("Erro", e.getLocalizedMessage());
			return;
		}
		
		if (this.listaVeterinarios == null || this.listaVeterinarios.isEmpty())
			FacesMessageUtil.addWarnContextFacesMessage("Nenhum veterin�rio foi encontrado", "");
	}
	
	public void selecionarVeterinario(Veterinario veterinario){
		this.termoFiscalizacao.setVeterinario(veterinario);
		FacesMessageUtil.addInfoContextFacesMessage("Veterinario " + this.termoFiscalizacao.getVeterinario().getNome() + " selecionado", "");
	}
	
	public List<TipoChavePrincipalTermoFiscalizacao> getListaTipoChavePrincipalTermoFiscalizacao(){
		return this.tipoChavePrincipalTermoFiscalizacaoService.findAllByStatus(AtivoInativo.ATIVO);
	}
	
	public void buscarListaTipoChaveSecundariaTermoFiscalizacao(){
		if (this.chavePrincipalTermoFiscalizacao != null && this.chavePrincipalTermoFiscalizacao.getTipoChavePrincipalTermoFiscalizacao() != null) {
			this.listaTipoChaveSecundariaTermoFiscalizacao = this.tipoChaveSecundariaTermoFiscalizacaoService.findAllByTipoChavePrincipal(this.chavePrincipalTermoFiscalizacao.getTipoChavePrincipalTermoFiscalizacao(), AtivoInativo.ATIVO);
		} 
	}
	
	public List<ChaveSecundariaTermoFiscalizacao> getListaChaveSecundariaTermoFiscalizacao_Inativos(){
		List<ChaveSecundariaTermoFiscalizacao> listChaveSecundariaTermoFiscalizacao = new ArrayList<ChaveSecundariaTermoFiscalizacao>();
		
		if (chavePrincipalTermoFiscalizacao != null)
			if (chavePrincipalTermoFiscalizacao.getListChaveSecundariaTermoFiscalizacao() != null && !chavePrincipalTermoFiscalizacao.getListChaveSecundariaTermoFiscalizacao().isEmpty()){
				for (ChaveSecundariaTermoFiscalizacao chaveSecundariaTermoFiscalizacao : chavePrincipalTermoFiscalizacao.getListChaveSecundariaTermoFiscalizacao()) {
					if (chaveSecundariaTermoFiscalizacao.getTipoChaveSecundariaTermoFiscalizacao().getStatus().equals(AtivoInativo.INATIVO))
						listChaveSecundariaTermoFiscalizacao.add(chaveSecundariaTermoFiscalizacao);
				}
			}
		
		return listChaveSecundariaTermoFiscalizacao;
	}
	
	public void adicionarChavePrincipalTermoFiscalizacao(){
		this.chavePrincipalTermoFiscalizacao.setTermoFiscalizacao(this.termoFiscalizacao);
		
		// fazer um for e criar as chaves
		if (!editandoChavePrincipalTermoFiscalizacao){
			ChaveSecundariaTermoFiscalizacao chaveSecundariaTermoFiscalizacao;
			
			for (TipoChaveSecundariaTermoFiscalizacao tipo : this.tipoChaveSecundariaTermoFiscalizacao) {
				chaveSecundariaTermoFiscalizacao = new ChaveSecundariaTermoFiscalizacao();
				
				chaveSecundariaTermoFiscalizacao.setDataCadastro(Calendar.getInstance());
				chaveSecundariaTermoFiscalizacao.setChavePrincipalTermoFiscalizacao(this.chavePrincipalTermoFiscalizacao);
				chaveSecundariaTermoFiscalizacao.setTipoChaveSecundariaTermoFiscalizacao(tipo);
				
				this.chavePrincipalTermoFiscalizacao.getListChaveSecundariaTermoFiscalizacao().add(chaveSecundariaTermoFiscalizacao);
			}
			
			this.termoFiscalizacao.getListChavePrincipalTermoFiscalizacao().add(this.chavePrincipalTermoFiscalizacao);
		}else{
			
			for (int i = 0; i < this.chavePrincipalTermoFiscalizacao.getListChaveSecundariaTermoFiscalizacao().size(); i++) {
				if (!this.tipoChaveSecundariaTermoFiscalizacao.contains(this.chavePrincipalTermoFiscalizacao.getListChaveSecundariaTermoFiscalizacao().get(i).getTipoChaveSecundariaTermoFiscalizacao())){
					this.chavePrincipalTermoFiscalizacao.getListChaveSecundariaTermoFiscalizacao().remove(i);
					i--;
				}
			}
			
			boolean contains;
			ChaveSecundariaTermoFiscalizacao chaveSecundariaTermoFiscalizacao;
			
			
			// percorre B
			for (int i = 0; i < this.tipoChaveSecundariaTermoFiscalizacao.size(); i++) {
				contains = false;
				
				// percorre A
				for (int j = 0; j < this.chavePrincipalTermoFiscalizacao.getListChaveSecundariaTermoFiscalizacao().size(); j++) {
					// checa se algum item de B n�o est� em A 
					if (this.chavePrincipalTermoFiscalizacao.getListChaveSecundariaTermoFiscalizacao().get(j).getTipoChaveSecundariaTermoFiscalizacao().equals(this.tipoChaveSecundariaTermoFiscalizacao.get(i)))
						contains = true;
				}
				
				if (!contains){
					chaveSecundariaTermoFiscalizacao = new ChaveSecundariaTermoFiscalizacao();
					
					chaveSecundariaTermoFiscalizacao.setDataCadastro(Calendar.getInstance());
					chaveSecundariaTermoFiscalizacao.setChavePrincipalTermoFiscalizacao(this.chavePrincipalTermoFiscalizacao);
					chaveSecundariaTermoFiscalizacao.setTipoChaveSecundariaTermoFiscalizacao(this.tipoChaveSecundariaTermoFiscalizacao.get(i));
					
					this.chavePrincipalTermoFiscalizacao.getListChaveSecundariaTermoFiscalizacao().add(chaveSecundariaTermoFiscalizacao);
					
				}
			}
			
			editandoChavePrincipalTermoFiscalizacao = false;
		}
		
		this.chavePrincipalTermoFiscalizacao = new ChavePrincipalTermoFiscalizacao();
		this.tipoChaveSecundariaTermoFiscalizacao = new ArrayList<TipoChaveSecundariaTermoFiscalizacao>();
		FacesMessageUtil.addInfoContextFacesMessage("Atividades realizadas na propriedade incluidas com sucesso", "");
	}
	
	public void novaChavePrincipalTermoFiscalizacao(){
		this.chavePrincipalTermoFiscalizacao = new ChavePrincipalTermoFiscalizacao();
		this.editandoChavePrincipalTermoFiscalizacao = false;
		this.visualizandoChavePrincipalTermoFiscalizacao = false;
		this.tipoChaveSecundariaTermoFiscalizacao = new ArrayList<TipoChaveSecundariaTermoFiscalizacao>();
		this.listaTipoChaveSecundariaTermoFiscalizacao = new ArrayList<TipoChaveSecundariaTermoFiscalizacao>();
	}
	
	public void removerChavePrincipalTermoFiscalizacao(ChavePrincipalTermoFiscalizacao chavePrincipalTermoFiscalizacao){
		this.termoFiscalizacao.getListChavePrincipalTermoFiscalizacao().remove(chavePrincipalTermoFiscalizacao);
		
		FacesMessageUtil.addInfoContextFacesMessage("Atividades realizadas na propriedade incluidas com sucesso", null);
	}
	
	public void editarChavePrincipalTermoFiscalizacao(ChavePrincipalTermoFiscalizacao chavePrincipalTermoFiscalizacao){
		this.editandoChavePrincipalTermoFiscalizacao = true;
		this.visualizandoChavePrincipalTermoFiscalizacao = false;
		this.chavePrincipalTermoFiscalizacao = chavePrincipalTermoFiscalizacao;
		
		this.tipoChaveSecundariaTermoFiscalizacao = new ArrayList<TipoChaveSecundariaTermoFiscalizacao>();
		
		this.buscarListaTipoChaveSecundariaTermoFiscalizacao();
		
		for (ChaveSecundariaTermoFiscalizacao chave : this.chavePrincipalTermoFiscalizacao.getListChaveSecundariaTermoFiscalizacao()) {
			this.tipoChaveSecundariaTermoFiscalizacao.add(chave.getTipoChaveSecundariaTermoFiscalizacao());
		}
		
	}
	
	public void visualizarChavePrincipalTermoFiscalizacao(ChavePrincipalTermoFiscalizacao chavePrincipalTermoFiscalizacao){
		this.visualizandoChavePrincipalTermoFiscalizacao = true;
		this.editandoChavePrincipalTermoFiscalizacao = false;
		this.chavePrincipalTermoFiscalizacao = chavePrincipalTermoFiscalizacao;
		
		this.tipoChaveSecundariaTermoFiscalizacao = new ArrayList<TipoChaveSecundariaTermoFiscalizacao>();
		
		this.buscarListaTipoChaveSecundariaTermoFiscalizacao();
		
		for (ChaveSecundariaTermoFiscalizacao chave : this.chavePrincipalTermoFiscalizacao.getListChaveSecundariaTermoFiscalizacao()) {
			this.tipoChaveSecundariaTermoFiscalizacao.add(chave.getTipoChaveSecundariaTermoFiscalizacao());
		}
	}
	
	
	public void abrirTelaDeBuscaDeRevenda(){
		this.cnpjRevenda = null;
		this.listaRevendas = null;
	}
	
	public void buscarRevendaPorCNPJ(){
		this.listaRevendas = null;
		
		try {
			Revenda revenda = clientWebServiceRevenda.getRevendaByCNPJ(cnpjRevenda);
			if (revenda != null){
				this.listaRevendas = new ArrayList<Revenda>();
				this.listaRevendas.add(revenda);
			}
		} catch (ApplicationException e) {
			FacesMessageUtil.addErrorContextFacesMessage("Erro", e.getLocalizedMessage());
			return;
		}
		
		if (this.listaRevendas == null || this.listaRevendas.isEmpty())
			FacesMessageUtil.addInfoContextFacesMessage("Nenhuma revenda foi encontrada", "");
	}
	
	public void selecionarRevenda(Revenda revenda){
		this.termoFiscalizacao.setRevenda(revenda);
		FacesMessageUtil.addInfoContextFacesMessage("Revenda " + this.termoFiscalizacao.getRevenda().getNome() + " selecionada", "");
	}
	
	
	public void removerChaveSecundaria(ChaveSecundariaTermoFiscalizacao chaveSecundariaTermoFiscalizacao){
		this.chavePrincipalTermoFiscalizacao.getListChaveSecundariaTermoFiscalizacao().remove(chaveSecundariaTermoFiscalizacao);
	}
	
	public boolean isTipoChavePrincipalTermoFiscalizacaoEmpty(){
		return this.chavePrincipalTermoFiscalizacao == null ||  this.chavePrincipalTermoFiscalizacao.getTipoChavePrincipalTermoFiscalizacao() == null;
	}
	
	public boolean isListaTipoChaveSecundariaTermoFiscalizacaoEmpty(){
		return this.listaTipoChaveSecundariaTermoFiscalizacao == null || this.listaTipoChaveSecundariaTermoFiscalizacao.isEmpty();
	}
	
	public TermoFiscalizacao getTermoFiscalizacao() {
		return termoFiscalizacao;
	}

	public void setTermoFiscalizacao(TermoFiscalizacao termoFiscalizacao) {
		this.termoFiscalizacao = termoFiscalizacao;
	}

	public String getCpfVeterinario() {
		return cpfVeterinario;
	}

	public void setCpfVeterinario(String cpfVeterinario) {
		this.cpfVeterinario = cpfVeterinario;
	}

	public String getCrmvVeterinario() {
		return crmvVeterinario;
	}

	public void setCrmvVeterinario(String crmvVeterinario) {
		this.crmvVeterinario = crmvVeterinario;
	}

	public List<Veterinario> getListaVeterinarios() {
		return listaVeterinarios;
	}

	public void setListaVeterinarios(List<Veterinario> listaVeterinarios) {
		this.listaVeterinarios = listaVeterinarios;
	}

	public Date getDataVisita() {
		return dataVisita;
	}

	public void setDataVisita(Date dataVisita) {
		this.dataVisita = dataVisita;
	}

	public boolean isEditandoChavePrincipalTermoFiscalizacao() {
		return editandoChavePrincipalTermoFiscalizacao;
	}

	public void setEditandoChavePrincipalTermoFiscalizacao(
			boolean editandoChavePrincipalTermoFiscalizacao) {
		this.editandoChavePrincipalTermoFiscalizacao = editandoChavePrincipalTermoFiscalizacao;
	}

	public boolean isVisualizandoChavePrincipalTermoFiscalizacao() {
		return visualizandoChavePrincipalTermoFiscalizacao;
	}

	public void setVisualizandoChavePrincipalTermoFiscalizacao(
			boolean visualizandoChavePrincipalTermoFiscalizacao) {
		this.visualizandoChavePrincipalTermoFiscalizacao = visualizandoChavePrincipalTermoFiscalizacao;
	}

	public ChavePrincipalTermoFiscalizacao getChavePrincipalTermoFiscalizacao() {
		return chavePrincipalTermoFiscalizacao;
	}

	public void setChavePrincipalTermoFiscalizacao(
			ChavePrincipalTermoFiscalizacao chavePrincipalTermoFiscalizacao) {
		this.chavePrincipalTermoFiscalizacao = chavePrincipalTermoFiscalizacao;
	}

	public List<TipoChaveSecundariaTermoFiscalizacao> getTipoChaveSecundariaTermoFiscalizacao() {
		return tipoChaveSecundariaTermoFiscalizacao;
	}

	public void setTipoChaveSecundariaTermoFiscalizacao(
			List<TipoChaveSecundariaTermoFiscalizacao> tipoChaveSecundariaTermoFiscalizacao) {
		this.tipoChaveSecundariaTermoFiscalizacao = tipoChaveSecundariaTermoFiscalizacao;
	}

	public List<TipoChaveSecundariaTermoFiscalizacao> getListaTipoChaveSecundariaTermoFiscalizacao() {
		return listaTipoChaveSecundariaTermoFiscalizacao;
	}

	public void setListaTipoChaveSecundariaTermoFiscalizacao(
			List<TipoChaveSecundariaTermoFiscalizacao> listaTipoChaveSecundariaTermoFiscalizacao) {
		this.listaTipoChaveSecundariaTermoFiscalizacao = listaTipoChaveSecundariaTermoFiscalizacao;
	}

	public String getCnpjRevenda() {
		return cnpjRevenda;
	}

	public void setCnpjRevenda(String cnpjRevenda) {
		this.cnpjRevenda = cnpjRevenda;
	}

	public List<Revenda> getListaRevendas() {
		return listaRevendas;
	}

	public void setListaRevendas(List<Revenda> listaRevendas) {
		this.listaRevendas = listaRevendas;
	}

	public LazyObjectDataModel<TermoFiscalizacao> getListaTermoFiscalizacao() {
		return listaTermoFiscalizacao;
	}

	public void setListaTermoFiscalizacao(LazyObjectDataModel<TermoFiscalizacao> listaTermoFiscalizacao) {
		this.listaTermoFiscalizacao = listaTermoFiscalizacao;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TermoFiscalizacaoDTO getTermoFiscalizacaoDTO() {
		return termoFiscalizacaoDTO;
	}

	public void setTermoFiscalizacaoDTO(TermoFiscalizacaoDTO termoFiscalizacaoDTO) {
		this.termoFiscalizacaoDTO = termoFiscalizacaoDTO;
	}

}
