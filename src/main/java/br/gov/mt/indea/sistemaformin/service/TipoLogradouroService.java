package br.gov.mt.indea.sistemaformin.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.mt.indea.sistemaformin.webservice.entity.TipoLogradouro;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class TipoLogradouroService extends PaginableService<TipoLogradouro, Long> {
	
	private static final Logger log = LoggerFactory.getLogger(TipoLogradouroService.class);

	protected TipoLogradouroService() {
		super(TipoLogradouro.class);
	}
	
	@SuppressWarnings("unchecked")
	public List<TipoLogradouro> findAll(){
		StringBuilder sql = new StringBuilder();
		sql.append("select tipologradouro ") 
		   .append("  from tipo_logradouro tipologradouro")
		   .append(" where id in (select min(id) ") 
		   .append("                from tipo_logradouro  ")
		   .append("               group by nome)")
		   .append(" order by nome");
		
		Query query = getSession().createQuery(sql.toString());
		
		return query.list();
	}
	
	@Override
	public void saveOrUpdate(TipoLogradouro tipoLogradouro) {
		super.saveOrUpdate(tipoLogradouro);
		
		log.info("Salvando TipoLogradouro {}", tipoLogradouro.getId());
	}
	
	@Override
	public void delete(TipoLogradouro tipoLogradouro) {
		super.delete(tipoLogradouro);
		
		log.info("Removendo TipoLogradouro {}", tipoLogradouro.getId());
	}
	
	@Override
	public void validar(TipoLogradouro TipoLogradouro) {

	}

	@Override
	public void validarPersist(TipoLogradouro TipoLogradouro) {

	}

	@Override
	public void validarMerge(TipoLogradouro TipoLogradouro) {

	}

	@Override
	public void validarDelete(TipoLogradouro TipoLogradouro) {

	}

}
