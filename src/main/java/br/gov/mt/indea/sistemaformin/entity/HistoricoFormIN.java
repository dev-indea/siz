package br.gov.mt.indea.sistemaformin.entity;

import java.util.Calendar;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

@Audited
@Entity
@Table(name="historico_form_in")
public class HistoricoFormIN extends BaseEntity<Long>{

	private static final long serialVersionUID = 2108532075251016547L;

	@Id
	@SequenceGenerator(name="historico_form_in_seq", sequenceName="historico_form_in_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="historico_form_in_seq")
	private Long id;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_usuario")
	private Usuario usuario;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data")
	private Calendar data;
	
	@ManyToOne
	@JoinColumn(name="id_formin_original")
	private FormIN formINOriginal;

	@OneToOne(cascade=CascadeType.ALL, orphanRemoval=true)
	@JoinColumn(name="id_formin_copia")
	private FormIN formINCopia;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Calendar getData() {
		return data;
	}

	public void setData(Calendar data) {
		this.data = data;
	}

	public FormIN getFormINOriginal() {
		return formINOriginal;
	}

	public void setFormINOriginal(FormIN formIN) {
		this.formINOriginal = formIN;
	}

	public FormIN getFormINCopia() {
		return formINCopia;
	}

	public void setFormINCopia(FormIN formINCopia) {
		this.formINCopia = formINCopia;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HistoricoFormIN other = (HistoricoFormIN) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}