package br.gov.mt.indea.sistemaformin.managedbeans;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLBeanName;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;

import br.gov.mt.indea.sistemaformin.entity.FormAIE;
import br.gov.mt.indea.sistemaformin.entity.FormCOM;
import br.gov.mt.indea.sistemaformin.entity.FormIN;
import br.gov.mt.indea.sistemaformin.entity.Municipio;
import br.gov.mt.indea.sistemaformin.entity.UF;
import br.gov.mt.indea.sistemaformin.enums.Dominio.SimNao;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoLaboratorio;
import br.gov.mt.indea.sistemaformin.exception.ApplicationErrorException;
import br.gov.mt.indea.sistemaformin.exception.ApplicationException;
import br.gov.mt.indea.sistemaformin.service.FormAIEService;
import br.gov.mt.indea.sistemaformin.service.FormCOMService;
import br.gov.mt.indea.sistemaformin.service.FormINService;
import br.gov.mt.indea.sistemaformin.service.MunicipioService;
import br.gov.mt.indea.sistemaformin.util.FacesMessageUtil;
import br.gov.mt.indea.sistemaformin.webservice.client.ClientWebServiceVeterinario;
import br.gov.mt.indea.sistemaformin.webservice.entity.Endereco;
import br.gov.mt.indea.sistemaformin.webservice.entity.Propriedade;
import br.gov.mt.indea.sistemaformin.webservice.entity.Veterinario;

@Named
@ViewScoped
@URLBeanName("formAIEManagedBean")
@URLMappings(mappings = {
		@URLMapping(id = "pesquisarFormAIE", pattern = "/formIN/#{formAIEManagedBean.idFormIN}/formAIE/pesquisar", viewId = "/pages/formAIE/lista.jsf"),
		@URLMapping(id = "incluirFormAIE", pattern = "/formIN/#{formAIEManagedBean.idFormIN}/formAIE/incluir", viewId = "/pages/formAIE/novo.jsf"),
		@URLMapping(id = "alterarFormAIE", pattern = "/formIN/#{formAIEManagedBean.idFormIN}/formAIE/alterar/#{formAIEManagedBean.id}", viewId = "/pages/formAIE/novo.jsf"),
		@URLMapping(id = "impressaoFormAIE", pattern = "/formIN/#{formAIEManagedBean.idFormIN}/formAIE/impressao/#{formAIEManagedBean.id}", viewId = "/pages/impressao.jsf")})
public class FormAIEManagedBean implements Serializable {

	private static final long serialVersionUID = 1760023349204758559L;

	private Long id;
	
	private Long idFormIN;
	
	private FormIN formIN;
	
	@Inject
	private FormINService formINService;
	
	@Inject
	private FormAIEService formAIEService;
	
	@Inject
	private FormCOMService formCOMService;
	
	@Inject
	private MunicipioService municipioService;
	
	@Inject
	private FormAIE formAIE;
	
	private String cpfVeterinario;

	private String crmvVeterinario;

	private List<Veterinario> listaVeterinarios;
	
	@Inject
	private ClientWebServiceVeterinario clientWebServiceVeterinario;
	
	private SimNao propriedadeOndeOAnimalSeEncontraEAMesmaDoFormIN = SimNao.SIM;
	
	private UF uf;
	
	public List<Municipio> getListaMunicipios(){
		if (this.uf == null)
			return null;
		
		return municipioService.findAllByUF(this.uf);
	}
	
	@PostConstruct
	private void init(){
		
	}
	
	private void carregarFormIN(){
		this.formIN = formINService.findByIdFetchPropriedade(this.idFormIN);
	}
	
	private void limpar() {
		this.formAIE = new FormAIE();
	}
	
	@URLAction(mappingId = "incluirFormAIE", onPostback = false)
	public void novo(){
		this.limpar();
		this.carregarFormIN();
		
		this.formAIE.setFormIN(formIN);
		
		this.formAIE.setPropriedade(this.formAIE.getFormIN().getPropriedade());
	}
	
	@URLAction(mappingId = "alterarFormAIE", onPostback = false)
	public void editar(){
		this.formAIE = formAIEService.findByIdFetchAll(this.getId());
		
//		if (this.formAIE.getPropriedade().getCodigoPropriedade().equals(this.formAIE.getFormIN().getPropriedade().getCodigoPropriedade())){
//			this.propriedadeOndeOAnimalSeEncontraEAMesmaDoFormIN = SimNao.SIM;
//		}else{
//			this.propriedadeOndeOAnimalSeEncontraEAMesmaDoFormIN = SimNao.NAO;
//			this.uf = this.formAIE.getPropriedade().getMunicipio().getUf();
//		}
	}
	
	public String adicionar() throws IOException{
		if (this.formAIE.getVeterinario() == null){
			FacesContext context = FacesContext.getCurrentInstance();
			
			context.addMessage("cadastro:fieldVeterinario:campoVeterinario", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Veterin�rio: valor � obrigat�rio.", "Veterin�rio: valor � obrigat�rio."));
			return "";
		}
		
		if (formAIE.getId() != null){
			this.formAIEService.saveOrUpdate(formAIE);
			FacesMessageUtil.addInfoContextFacesMessage("Form AIE atualizado", "");
		}else{
			this.formAIE.setDataCadastro(Calendar.getInstance());
			this.formAIEService.saveOrUpdate(formAIE);
			FacesMessageUtil.addInfoContextFacesMessage("Form AIE adicionado", "");
		}

		limpar();
	    return "pretty:visaoGeralFormIN";
	}
	
	public void abrirTelaDeBuscaDeVeterinario(){
		this.cpfVeterinario = null;
		this.crmvVeterinario = null;
		this.listaVeterinarios = null;
	}
	
	public void buscarVeterinarioPorCpf(){
		this.listaVeterinarios = null;
		
		Veterinario v;
		try {
			v = clientWebServiceVeterinario.getVeterinarioByCpf(cpfVeterinario);
			
			if (v != null){
				this.listaVeterinarios = new ArrayList<Veterinario>();
				this.listaVeterinarios.add(v);
			}
		} catch (ApplicationException e) {
			FacesMessageUtil.addErrorContextFacesMessage("Erro", e.getLocalizedMessage());
			return;
		}
		
		if (this.listaVeterinarios == null || this.listaVeterinarios.isEmpty()){
			FacesMessageUtil.addWarnContextFacesMessage("Nenhum veterin�rio foi encontrado", "");
		}
	}
	
	public void buscarVeterinarioPorCrmv(){
		this.listaVeterinarios = null;

		Veterinario v;
		try {
			v = clientWebServiceVeterinario.getVeterinarioByCRMV(crmvVeterinario);
			
			if (v != null){
				this.listaVeterinarios = new ArrayList<Veterinario>();
				this.listaVeterinarios.add(v);
			}
		} catch (ApplicationException e) {
			FacesMessageUtil.addErrorContextFacesMessage("Erro", e.getLocalizedMessage());
			return;
		}
		
		if (this.listaVeterinarios == null || this.listaVeterinarios.isEmpty()){
			FacesMessageUtil.addWarnContextFacesMessage("Nenhum veterin�rio foi encontrado", "");
		}
	}
	
	public void selecionarVeterinario(Veterinario veterinario){
		this.formAIE.setVeterinario(veterinario);
		FacesMessageUtil.addInfoContextFacesMessage("Veterinario " + this.formAIE.getVeterinario().getNome() + " selecionado", "");
	}
	
	public boolean isTipoLaboratorioCredenciado(){
		boolean resultado = false;
		if (this.formAIE.getTipoLaboratorio() == null)
			return resultado;
		else
			resultado = this.formAIE.getTipoLaboratorio().equals(TipoLaboratorio.CREDENCIADO);
		
		if (!resultado)
			this.formAIE.setFinalidadeDoTesteDeLaboratorioCredenciado(null);
		
		return resultado;
	}
	
	public boolean isTipoLaboratorioCredenciadoPublico(){
		boolean resultado = false;
		if (this.formAIE.getTipoLaboratorio() == null)
			return resultado;
		else
			resultado = this.formAIE.getTipoLaboratorio().equals(TipoLaboratorio.CREDENCIADO_PUBLICO);
		
		if (!resultado)
			this.formAIE.setFinalidadeDoTesteDeLaboratorioCredenciadoPublico(null);
		
		return resultado;
	}
	
	public boolean isTipoLaboratorioOficial(){
		boolean resultado = false;
		if (this.formAIE.getTipoLaboratorio() == null)
			return resultado;
		else
			resultado = this.formAIE.getTipoLaboratorio().equals(TipoLaboratorio.OFICIAL);
		
		if (!resultado)
			this.formAIE.setFinalidadeDoTesteDeLaboratorioOficial(null);
		
		return resultado;
	}
	
	@Inject
	private VisualizadorImpressaoManagedBean visualizadorImpressaoManagedBean;
	
	@URLAction(mappingId = "impressaoFormAIE", onPostback = false)
	public void imprimir() throws ApplicationErrorException{
		this.formAIE = formAIEService.findByIdFetchAll(this.getId());
		
		visualizadorImpressaoManagedBean.exibirFormAIE(formAIE);
	}
	
	public void remover(FormAIE formAIE){
		this.formAIEService.delete(formAIE);
		FacesMessageUtil.addInfoContextFacesMessage("Form AIE exclu�do com sucesso", "");
	}
	
	public void alterarPropriedade(){
		if (this.propriedadeOndeOAnimalSeEncontraEAMesmaDoFormIN.equals(SimNao.SIM))
			this.formAIE.setPropriedade(this.formAIE.getFormIN().getPropriedade());
		else{
			this.formAIE.setPropriedade(new Propriedade());
			this.formAIE.getPropriedade().setEndereco(new Endereco());
		}
		
	}
	
	public List<FormCOM> getListaFormCOM(){
		return formCOMService.findAllBy(this.formAIE.getFormIN());
	}
	
	public FormAIE getFormAIE() {
		return formAIE;
	}

	public void setFormAIE(FormAIE formAIE) {
		this.formAIE = formAIE;
	}

	public String getCpfVeterinario() {
		return cpfVeterinario;
	}

	public void setCpfVeterinario(String cpfVeterinario) {
		this.cpfVeterinario = cpfVeterinario;
	}

	public String getCrmvVeterinario() {
		return crmvVeterinario;
	}

	public void setCrmvVeterinario(String crmvVeterinario) {
		this.crmvVeterinario = crmvVeterinario;
	}

	public List<Veterinario> getListaVeterinarios() {
		return listaVeterinarios;
	}

	public void setListaVeterinarios(List<Veterinario> listaVeterinarios) {
		this.listaVeterinarios = listaVeterinarios;
	}

	public SimNao getPropriedadeOndeOAnimalSeEncontraEAMesmaDoFormIN() {
		return propriedadeOndeOAnimalSeEncontraEAMesmaDoFormIN;
	}

	public void setPropriedadeOndeOAnimalSeEncontraEAMesmaDoFormIN(
			SimNao propriedadeOndeOAnimalSeEncontraEAMesmaDoFormIN) {
		this.propriedadeOndeOAnimalSeEncontraEAMesmaDoFormIN = propriedadeOndeOAnimalSeEncontraEAMesmaDoFormIN;
	}
	
	public boolean isPropriedadeOndeOAnimalSeEncontraEAMesmaDoFormIN_SIM(){
		return propriedadeOndeOAnimalSeEncontraEAMesmaDoFormIN.equals(SimNao.SIM);
	}

	public UF getUf() {
		return uf;
	}

	public void setUf(UF uf) {
		this.uf = uf;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdFormIN() {
		return idFormIN;
	}

	public void setIdFormIN(Long idFormIN) {
		this.idFormIN = idFormIN;
	}

	public FormIN getFormIN() {
		return formIN;
	}

	public void setFormIN(FormIN formIN) {
		this.formIN = formIN;
	}

}
