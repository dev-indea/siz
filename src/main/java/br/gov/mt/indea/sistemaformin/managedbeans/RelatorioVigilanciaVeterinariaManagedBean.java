package br.gov.mt.indea.sistemaformin.managedbeans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang.time.DateUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;

import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLBeanName;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;

import br.gov.mt.indea.sistemaformin.entity.view.RelatorioVigilanciaVeterinariaGeral;
import br.gov.mt.indea.sistemaformin.enums.Dominio.SimNao;
import br.gov.mt.indea.sistemaformin.service.VigilanciaVeterinariaService;
import br.gov.mt.indea.sistemaformin.util.FacesMessageUtil;
import br.gov.mt.indea.sistemaformin.webservice.entity.ULE;

@Named
@ViewScoped
@URLBeanName("relatorioVigilanciaVeterinariaManagedBean")
@URLMappings(mappings = {
		@URLMapping(id = "relatorioVigilanciaVeterinaria", pattern = "/relatorio/vigilanciaVeterinaria", viewId = "/pages/relatorios/vigilanciaveterinaria.jsf")})
public class RelatorioVigilanciaVeterinariaManagedBean implements Serializable {

	private static final long serialVersionUID = 1760023349204758559L;

	@Inject
	private VigilanciaVeterinariaService vigilanciaVeterinariaService;
	
	private List<RelatorioVigilanciaVeterinariaGeral> listaVigilanciaVeterinaria;
	
	private List<RelatorioVigilanciaVeterinariaGeral> listaVigilanciaVeterinariaFiltered;
	
	private Date dataInicial;
	
	private Date dataFinal;
	
	private ULE urs;
	
	private ULE ule;
	
	@PostConstruct
	private void init(){
		dataInicial = new Date();
		dataFinal = new Date();
		
		Calendar c = Calendar.getInstance();
		c.set(Calendar.DAY_OF_MONTH, 1);
		
		dataInicial.setTime(c.getTimeInMillis());
		
		c.set(Calendar.DAY_OF_MONTH, 30);
		
		dataFinal.setTime(c.getTimeInMillis());
	}
	
	@URLAction(mappingId = "relatorioVigilanciaVeterinaria", onPostback = false)
	public void novo(){
		
	}
	
	public void buscar(){
		long diff = dataFinal.getTime() - dataInicial.getTime();
		
	    if (TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS) > 366){
	    	FacesMessageUtil.addWarnContextFacesMessage("O per�odo de busca n�o deve ser superior a 1 ano", "");
	    	return;
	    }
	    
		listaVigilanciaVeterinaria = (ArrayList<RelatorioVigilanciaVeterinariaGeral>) vigilanciaVeterinariaService.relatorioTeste(this.dataInicial, this.dataFinal);
	}
	
	public void postProcessor(Object doc){
		HSSFWorkbook book = (HSSFWorkbook)doc;
	    HSSFSheet sheet = book.getSheetAt(0); 

	    HSSFRow header = sheet.getRow(0);
	    
	    int colCount = header.getPhysicalNumberOfCells();
	    int rowCount = sheet.getPhysicalNumberOfRows();

	    HSSFCellStyle intStyle = book.createCellStyle();
		intStyle.setDataFormat((short)1);

		HSSFCellStyle decStyle = book.createCellStyle();
		decStyle.setDataFormat((short)2);
		
		HSSFCellStyle headerStyle = book.createCellStyle();
		headerStyle.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
		headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		HSSFFont font = book.createFont();
        font.setBold(true);
        headerStyle.setFont(font);

        //Transforma o t�tulo em negrito
        for(int colNum = 0; colNum < header.getLastCellNum(); colNum++){   
	    	HSSFCell cell = header.getCell(colNum);
	    	cell.setCellStyle(headerStyle);
	    	book.getSheetAt(0).autoSizeColumn(colNum);
	    	
	    	if (colNum == 4)
	    		cell.setCellValue("Data Vigil�ncia");
	    	if (colNum == 5)
	    		cell.setCellValue("Data Cadastro");
        }
	    
		for(int rowInd = 1; rowInd < rowCount; rowInd++) {
		    HSSFRow row = sheet.getRow(rowInd);
		    for(int cellInd = 1; cellInd < colCount; cellInd++) {
		        HSSFCell cell = row.getCell(cellInd);
		        
		        //Latitude (grau)
		        if (cell.getColumnIndex() == 12){
		        	this.setCellIntegerStyle(cell, intStyle);
		        }
		        
		        if (cell.getColumnIndex() == 13){
		        	this.setCellIntegerStyle(cell, intStyle);
		        }
		        
		        if (cell.getColumnIndex() == 14){
		        	this.setCellDoubleStyle(cell, decStyle);
		        }
		        
		        if (cell.getColumnIndex() == 15){
		        	this.setCellIntegerStyle(cell, intStyle);
		        }
		        
		        if (cell.getColumnIndex() == 16){
		        	this.setCellIntegerStyle(cell, intStyle);
		        }
		        
		        if (cell.getColumnIndex() == 17){
		        	this.setCellDoubleStyle(cell, decStyle);
		        }
		        
		        if (cell.getColumnIndex() == 22){
		        	this.setCellIntegerStyle(cell, intStyle);
		        }
		        
		        if (cell.getColumnIndex() == 23){
		        	this.setCellIntegerStyle(cell, intStyle);
		        }
		        
		        if (cell.getColumnIndex() == 24){
		        	this.setCellIntegerStyle(cell, intStyle);
		        }
		        
		        if (cell.getColumnIndex() == 25){
		        	this.setCellIntegerStyle(cell, intStyle);
		        }

		        if (cell.getColumnIndex() == 27){
		        	this.setCellIntegerStyle(cell, intStyle);
		        }
		        
		        if (cell.getColumnIndex() == 28){
		        	this.setCellIntegerStyle(cell, intStyle);
		        }
		        
		        if (cell.getColumnIndex() == 29){
		        	this.setCellIntegerStyle(cell, intStyle);
		        }
		        
		        if (cell.getColumnIndex() == 47){
		        	this.setCellIntegerStyle(cell, intStyle);
		        }
		        
		        if (cell.getColumnIndex() == 48){
		        	this.setCellIntegerStyle(cell, intStyle);
		        }

		        if (cell.getColumnIndex() == 49){
		        	this.setCellIntegerStyle(cell, intStyle);
		        }
		        
		        if (cell.getColumnIndex() == 50){
		        	this.setCellIntegerStyle(cell, intStyle);
		        }
		        
		        if (cell.getColumnIndex() == 51){
		        	this.setCellIntegerStyle(cell, intStyle);
		        }
		        
		        if (cell.getColumnIndex() == 53){
		        	this.setCellIntegerStyle(cell, intStyle);
		        }
		        
		        if (cell.getColumnIndex() == 54){
		        	this.setCellIntegerStyle(cell, intStyle);
		        }
		        
		        if (cell.getColumnIndex() == 68){
		        	this.setCellDoubleStyle(cell, decStyle);
		        }
		        
		        if (cell.getColumnIndex() == 70){
		        	this.setCellDoubleStyle(cell, decStyle);
		        }
		        
		        if (cell.getColumnIndex() == 72){
		        	this.setCellDoubleStyle(cell, decStyle);
		        }
		        
		        if (cell.getColumnIndex() == 74){
		        	this.setCellDoubleStyle(cell, decStyle);
		        }
		        
		        if (cell.getColumnIndex() == 76){
		        	this.setCellDoubleStyle(cell, decStyle);
		        }
		        
		        if (cell.getColumnIndex() == 78){
		        	this.setCellDoubleStyle(cell, decStyle);
		        }
		        
		        if (cell.getColumnIndex() == 80){
		        	this.setCellDoubleStyle(cell, decStyle);
		        }
		        
		        if (cell.getColumnIndex() == 82){
		        	this.setCellDoubleStyle(cell, decStyle);
		        }
		        
		        if (cell.getColumnIndex() == 84){
		        	this.setCellDoubleStyle(cell, decStyle);
		        }

		        if (cell.getColumnIndex() == 86){
		        	this.setCellDoubleStyle(cell, decStyle);
		        }
		        
		        if (cell.getColumnIndex() == 88){
		        	this.setCellDoubleStyle(cell, decStyle);
		        }
		        
		        if (cell.getColumnIndex() == 90){
		        	this.setCellDoubleStyle(cell, decStyle);
		        }
		        
		        if (cell.getColumnIndex() == 92){
		        	this.setCellIntegerStyle(cell, intStyle);
		        }
		        
		        if (cell.getColumnIndex() == 93){
		        	this.setCellIntegerStyle(cell, intStyle);
		        }
		        
		        if (cell.getColumnIndex() == 95){
		        	this.setCellIntegerStyle(cell, intStyle);
		        }
		        
		        if (cell.getColumnIndex() == 96){
		        	this.setCellIntegerStyle(cell, intStyle);
		        }
		        
		        if (cell.getColumnIndex() == 97){
		        	this.setCellIntegerStyle(cell, intStyle);
		        }
		        
		        if (cell.getColumnIndex() == 98){
		        	this.setCellIntegerStyle(cell, intStyle);
		        }
		        
		        if (cell.getColumnIndex() == 99){
		        	this.setCellIntegerStyle(cell, intStyle);
		        }
		        
		        if (cell.getColumnIndex() == 100){
		        	this.setCellIntegerStyle(cell, intStyle);
		        }
		        
		        if (cell.getColumnIndex() == 101){
		        	this.setCellIntegerStyle(cell, intStyle);
		        }
		        
		        if (cell.getColumnIndex() == 102){
		        	this.setCellIntegerStyle(cell, intStyle);
		        }
		        
		    }
		}
	}
	
	private void setCellIntegerStyle(HSSFCell cell, HSSFCellStyle style){
		String strVal = cell.getStringCellValue();
		cell.setCellStyle(style);
        
		strVal = strVal.replaceAll("[^\\d.]", "");
		if (strVal != null && !strVal.equals("")){
	    	int intVal = Integer.valueOf(strVal);
	    	cell.setCellValue(intVal);
		}
	}
	
	private void setCellDoubleStyle(HSSFCell cell, HSSFCellStyle style){
		String strVal = cell.getStringCellValue();
    	cell.setCellStyle(style);
		
    	if (strVal != null && !strVal.equals("")){
    	    double dblVal = Double.valueOf(strVal);
    	    cell.setCellValue(dblVal);
    	}
	}
	
	public List<String> getListaURSFilter(){
		if (this.listaVigilanciaVeterinaria == null)
			return null;
		else{
			List<String> lista = new ArrayList<>();
			
			for (RelatorioVigilanciaVeterinariaGeral relatorio : listaVigilanciaVeterinaria) {
				if (!lista.contains(relatorio.getUrs()))
					lista.add((String) relatorio.getUrs());
			}
			
			List<String> sortedLista = new ArrayList<>();
			sortedLista = lista;
			
			try{
				Collections.sort(lista);
				sortedLista = lista;
			} catch (Exception e) {
			}
			
			return sortedLista;
		}
	}
	
	public List<String> getListaMunicipioFilter(){
		if (this.listaVigilanciaVeterinaria == null)
			return null;
		else{
			List<String> lista = new ArrayList<>();
			
			for (RelatorioVigilanciaVeterinariaGeral relatorio : listaVigilanciaVeterinaria) {
				if (!lista.contains(relatorio.getUle()))
					lista.add((String) relatorio.getUle());
			}
			
			List<String> sortedLista = new ArrayList<>();
			sortedLista = lista;
			
			try{
				Collections.sort(lista);
				sortedLista = lista;
			} catch (Exception e) {
			}
			
			return sortedLista;
		}
	}
	
	public List<String> getListaPrincipaisMotivosVisitaFilter(){
		if (this.listaVigilanciaVeterinaria == null)
			return null;
		else{
			List<String> lista = new ArrayList<>();
			
			for (RelatorioVigilanciaVeterinariaGeral relatorio : listaVigilanciaVeterinaria) {
				String[] motivos = relatorio.getPrincipais_motivos_visita().split(",");
				
				for (String motivo : motivos) {
					if (!lista.contains(motivo))
						lista.add(motivo);
				}
				
			}
			
			List<String> sortedLista = new ArrayList<>();
			sortedLista = lista;
			
			try{
				Collections.sort(lista);
				sortedLista = lista;
			} catch (Exception e) {
			}
			
			return sortedLista;
		}
	}
	
	public List<String> getListaTipoExploracaoRuminantesFilter(){
		if (this.listaVigilanciaVeterinaria == null)
			return null;
		else{
			List<String> lista = new ArrayList<>();
			
			for (RelatorioVigilanciaVeterinariaGeral relatorio : listaVigilanciaVeterinaria) {
				String tipo_exploracao_outros_ruminantes = relatorio.getTipo_exploracao_outros_ruminantes();
				
				if (tipo_exploracao_outros_ruminantes != null){
				
					String[] tipos = tipo_exploracao_outros_ruminantes.split(",");
					
					for (String tipo : tipos) {
						if (!lista.contains(tipo))
							lista.add(tipo);
					}
				}
				
			}
			
			List<String> sortedLista = new ArrayList<>();
			sortedLista = lista;
			
			try{
				Collections.sort(lista);
				sortedLista = lista;
			} catch (Exception e) {
			}
			
			return sortedLista;
		}
	}
	
	public List<String> getListaSistemaCriacaoRuminantesFilter(){
		if (this.listaVigilanciaVeterinaria == null)
			return null;
		else{
			List<String> lista = new ArrayList<>();
			
			for (RelatorioVigilanciaVeterinariaGeral relatorio : listaVigilanciaVeterinaria) {
				String tipo_exploracao_outros_ruminantes = relatorio.getSistema_criacao_ruminantes();
				
				if (tipo_exploracao_outros_ruminantes != null){
				
					String[] tipos = tipo_exploracao_outros_ruminantes.split(",");
					
					for (String tipo : tipos) {
						if (!lista.contains(tipo))
							lista.add(tipo);
					}
				}
				
			}
			
			List<String> sortedLista = new ArrayList<>();
			sortedLista = lista;
			
			try{
				Collections.sort(lista);
				sortedLista = lista;
			} catch (Exception e) {
			}
			
			return sortedLista;
		}
	}
	
	public List<String> getListaTipoExploracaoAvicolaSistemaIndustrialFilter(){
		if (this.listaVigilanciaVeterinaria == null)
			return null;
		else{
			List<String> lista = new ArrayList<>();
			
			for (RelatorioVigilanciaVeterinariaGeral relatorio : listaVigilanciaVeterinaria) {
				if (relatorio.getTipo_exp_avicola_sist_indust() != null)
					if (!lista.contains(relatorio.getTipo_exp_avicola_sist_indust()))
						lista.add((String) relatorio.getTipo_exp_avicola_sist_indust());
			}
			
			List<String> sortedLista = new ArrayList<>();
			sortedLista = lista;
			
			try{
				Collections.sort(lista);
				sortedLista = lista;
			} catch (Exception e) {
			}
			
			return sortedLista;
		}
	}
	
	public List<String> getListaTipoExploracaoSuinosSistemaIndustrialFilter(){
		if (this.listaVigilanciaVeterinaria == null)
			return null;
		else{
			List<String> lista = new ArrayList<>();
			
			for (RelatorioVigilanciaVeterinariaGeral relatorio : listaVigilanciaVeterinaria) {
				if (relatorio.getTipo_exp_suino_sist_indust() != null)
					if (!lista.contains(relatorio.getTipo_exp_suino_sist_indust()))
						lista.add((String) relatorio.getTipo_exp_suino_sist_indust());
			}
			
			List<String> sortedLista = new ArrayList<>();
			sortedLista = lista;
			
			try{
				Collections.sort(lista);
				sortedLista = lista;
			} catch (Exception e) {
			}
			
			return sortedLista;
		}
	}
	
	public List<String> getListaTipoFiscalizacaoFilter(){
		if (this.listaVigilanciaVeterinaria == null)
			return null;
		else{
			List<String> lista = new ArrayList<>();
			
			for (RelatorioVigilanciaVeterinariaGeral relatorio : listaVigilanciaVeterinaria) {
				if (relatorio.getTipo_fiscalizacao() != null)
					if (!lista.contains(relatorio.getTipo_fiscalizacao()))
						lista.add((String) relatorio.getTipo_fiscalizacao());
			}
			
			List<String> sortedLista = new ArrayList<>();
			sortedLista = lista;
			
			try{
				Collections.sort(lista);
				sortedLista = lista;
			} catch (Exception e) {
			}
			
			return sortedLista;
		}
	}
	
	public List<String> getListaSimNaoFilter(){
		SimNao[] values = SimNao.values();
		List<String> lista = new ArrayList<String>();
		
		for (SimNao simNao : values) {
			lista.add(simNao.toString());
		}
		
		List<String> sortedLista = new ArrayList<>();
		sortedLista = lista;
		
		try{
			Collections.sort(lista);
			sortedLista = lista;
		} catch (Exception e) {
		}
		
		return sortedLista;
	}
	
	public List<String> getListaTipoRiscoFilter(){
		if (this.listaVigilanciaVeterinaria == null)
			return null;
		else{
			List<String> lista = new ArrayList<>();
			
			for (RelatorioVigilanciaVeterinariaGeral relatorio : listaVigilanciaVeterinaria) {
				
				if (relatorio.getTipos_risco() != null){
					String[] motivos = relatorio.getTipos_risco().split(",");
					
					for (String motivo : motivos) {
						if (!lista.contains(motivo))
							lista.add(motivo);
					}
				}
				
			}
			
			List<String> sortedLista = new ArrayList<>();
			sortedLista = lista;
			
			try{
				Collections.sort(lista);
				sortedLista = lista;
			} catch (Exception e) {
			}
			
			return sortedLista;
		}
	}
	
	public List<String> getListaFinalidadeFilter(){
		if (this.listaVigilanciaVeterinaria == null)
			return null;
		else{
			List<String> lista = new ArrayList<>();
			
			for (RelatorioVigilanciaVeterinariaGeral relatorio : listaVigilanciaVeterinaria) {
				if (relatorio.getFinalidade_explocarao_suinos() != null)
					if (!lista.contains(relatorio.getFinalidade_explocarao_suinos()))
						lista.add((String) relatorio.getFinalidade_explocarao_suinos());
			}
			
			List<String> sortedLista = new ArrayList<>();
			sortedLista = lista;
			
			try{
				Collections.sort(lista);
				sortedLista = lista;
			} catch (Exception e) {
			}
			
			return sortedLista;
		}
	}
	
	public List<String> getListaTipoOrigemSuideosFilter(){
		if (this.listaVigilanciaVeterinaria == null)
			return null;
		else{
			List<String> lista = new ArrayList<>();
			
			for (RelatorioVigilanciaVeterinariaGeral relatorio : listaVigilanciaVeterinaria) {

				if (relatorio.getTipo_origem_suideos() != null){
					String[] tipos_origem = relatorio.getTipo_origem_suideos().split(",");
					
					for (String tipo_origem : tipos_origem) {
						if (!lista.contains(tipo_origem))
							lista.add(tipo_origem);
					}
				}
				
			}
			
			List<String> sortedLista = new ArrayList<>();
			sortedLista = lista;
			
			try{
				Collections.sort(lista);
				sortedLista = lista;
			} catch (Exception e) {
			}
			
			return sortedLista;
		}
	}
	
	public List<String> getListaTipoDestinoSuideosFilter(){
		if (this.listaVigilanciaVeterinaria == null)
			return null;
		else{
			List<String> lista = new ArrayList<>();
			
			for (RelatorioVigilanciaVeterinariaGeral relatorio : listaVigilanciaVeterinaria) {
				if (relatorio.getTipo_destino_suideos() != null)
					if (!lista.contains(relatorio.getTipo_destino_suideos()))
						lista.add((String) relatorio.getTipo_destino_suideos());
			}
			
			List<String> sortedLista = new ArrayList<>();
			sortedLista = lista;
			
			try{
				Collections.sort(lista);
				sortedLista = lista;
			} catch (Exception e) {
			}
			
			return sortedLista;
		}
	}
	
	public List<String> getListaTipoDestinoCadaveresFilter(){
		if (this.listaVigilanciaVeterinaria == null)
			return null;
		else{
			List<String> lista = new ArrayList<>();
			
			for (RelatorioVigilanciaVeterinariaGeral relatorio : listaVigilanciaVeterinaria) {
				if (relatorio.getTipo_destino_cadaveres() != null)
					if (!lista.contains(relatorio.getTipo_destino_cadaveres()))
						lista.add((String) relatorio.getTipo_destino_cadaveres());
			}
			
			List<String> sortedLista = new ArrayList<>();
			sortedLista = lista;
			
			try{
				Collections.sort(lista);
				sortedLista = lista;
			} catch (Exception e) {
			}
			
			return sortedLista;
		}
	}
	
	public boolean filterByDate(Object value, Object filter, Locale locale){
		if( filter == null ) {
            return true;
        }

        if( value == null ) {
            return false;
        }
        
        if (value instanceof String){
        	String[] date = ((String) value).split("/");

        	Calendar c = Calendar.getInstance();
        	c.set(Calendar.DAY_OF_MONTH, Integer.parseInt(date[0]));
        	c.set(Calendar.MONTH, Integer.parseInt(date[1]) - 1);
        	c.set(Calendar.YEAR, Integer.parseInt(date[2]));
        	
        	return DateUtils.truncatedEquals((Date) filter, c.getTime(), Calendar.DATE);
        } else {
        	return DateUtils.truncatedEquals((Date) filter, (Date) value, Calendar.DATE);
        }
	}
	
	public boolean filterInsideList(Object value, Object filter, Locale locale){
		String[] list = (String[]) filter;
		
		
		if( list.length < 1 ) {
            return true;
        }
		
		if( value == null ) {
            return false;
        }
		
		String value_ = (String)value;
		
		boolean hasAll = true;
		
		for (String motivo : list) {
			if (!value_.contains(motivo))
				hasAll = false;
		}

        return hasAll;	
	}
	
	public List<RelatorioVigilanciaVeterinariaGeral> getListaVigilanciaVeterinaria() {
		return listaVigilanciaVeterinaria;
	}

	public void setListaVigilanciaVeterinaria(List<RelatorioVigilanciaVeterinariaGeral> listaVigilanciaVeterinaria) {
		this.listaVigilanciaVeterinaria = listaVigilanciaVeterinaria;
	}

	public List<RelatorioVigilanciaVeterinariaGeral> getListaVigilanciaVeterinariaFiltered() {
		return listaVigilanciaVeterinariaFiltered;
	}

	public void setListaVigilanciaVeterinariaFiltered(
			List<RelatorioVigilanciaVeterinariaGeral> listaVigilanciaVeterinariaFiltered) {
		this.listaVigilanciaVeterinariaFiltered = listaVigilanciaVeterinariaFiltered;
	}

	public Date getDataInicial() {
		return dataInicial;
	}

	public void setDataInicial(Date dataInicial) {
		this.dataInicial = dataInicial;
	}

	public Date getDataFinal() {
		return dataFinal;
	}

	public void setDataFinal(Date dataFinal) {
		this.dataFinal = dataFinal;
	}

	public ULE getUrs() {
		return urs;
	}

	public void setUrs(ULE urs) {
		this.urs = urs;
	}

	public ULE getUle() {
		return ule;
	}

	public void setUle(ULE ule) {
		this.ule = ule;
	}
	
}
