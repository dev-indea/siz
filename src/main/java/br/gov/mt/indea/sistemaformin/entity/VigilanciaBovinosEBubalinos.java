package br.gov.mt.indea.sistemaformin.entity;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

@Audited
@Entity(name="vigilancia_bov_bub")
public class VigilanciaBovinosEBubalinos extends BaseEntity<Long>{
	
	private static final long serialVersionUID = -361785479572430083L;

	@Id
	@SequenceGenerator(name="vigilancia_bov_bub_seq", sequenceName="vigilancia_bov_bub_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="vigilancia_bov_bub_seq")
	private Long id;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_cadastro")
	private Calendar dataCadastro;
	
	
	@Column(name="quantidade_inspec_00_04_f")
	private Integer quantidadeInspecionados00_04_F;
	
	@Column(name="quantidade_inspec_00_04_m")
	private Integer quantidadeInspecionados00_04_M;
	
	@Column(name="quantidade_inspec_05_12_f")
	private Integer quantidadeInspecionados05_12_F;
	
	@Column(name="quantidade_inspec_05_12_m")
	private Integer quantidadeInspecionados05_12_M;
	
	@Column(name="quantidade_inspec_13_24_f")
	private Integer quantidadeInspecionados13_24_F;
	
	@Column(name="quantidade_inspec_13_24_m")
	private Integer quantidadeInspecionados13_24_M;
	
	@Column(name="quantidade_inspec_25_36_f")
	private Integer quantidadeInspecionados25_36_F;
	
	@Column(name="quantidade_inspec_25_36_m")
	private Integer quantidadeInspecionados25_36_M;
	
	@Column(name="quantidade_inspec_acima36_f")
	private Integer quantidadeInspecionadosAcima36_F;
	
	@Column(name="quantidade_inspec_acima36_m")
	private Integer quantidadeInspecionadosAcima36_M;
	
	@Column(name="quantidade_vistoriados")
	private Integer quantidadeVistoriados;
	
	@Column(name="quantidade_leiteiras")
	private Integer quantidadeLeiteiras;
	
	@Column(name="quantidade_reprodutoras")
	private Integer quantidadeReprodutoras;
	
	@Column(name="porcentagem_natalidade")
	private Integer porcentagemNatalidade;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Calendar getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Calendar dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Integer getQuantidadeInspecionados00_04_F() {
		return quantidadeInspecionados00_04_F;
	}

	public void setQuantidadeInspecionados00_04_F(
			Integer quantidadeInspecionados00_04_F) {
		this.quantidadeInspecionados00_04_F = quantidadeInspecionados00_04_F;
	}

	public Integer getQuantidadeInspecionados00_04_M() {
		return quantidadeInspecionados00_04_M;
	}

	public void setQuantidadeInspecionados00_04_M(
			Integer quantidadeInspecionados00_04_M) {
		this.quantidadeInspecionados00_04_M = quantidadeInspecionados00_04_M;
	}

	public Integer getQuantidadeInspecionados05_12_F() {
		return quantidadeInspecionados05_12_F;
	}

	public void setQuantidadeInspecionados05_12_F(
			Integer quantidadeInspecionados05_12_F) {
		this.quantidadeInspecionados05_12_F = quantidadeInspecionados05_12_F;
	}

	public Integer getQuantidadeInspecionados05_12_M() {
		return quantidadeInspecionados05_12_M;
	}

	public void setQuantidadeInspecionados05_12_M(
			Integer quantidadeInspecionados05_12_M) {
		this.quantidadeInspecionados05_12_M = quantidadeInspecionados05_12_M;
	}

	public Integer getQuantidadeInspecionados13_24_F() {
		return quantidadeInspecionados13_24_F;
	}

	public void setQuantidadeInspecionados13_24_F(
			Integer quantidadeInspecionados13_24_F) {
		this.quantidadeInspecionados13_24_F = quantidadeInspecionados13_24_F;
	}

	public Integer getQuantidadeInspecionados13_24_M() {
		return quantidadeInspecionados13_24_M;
	}

	public void setQuantidadeInspecionados13_24_M(
			Integer quantidadeInspecionados13_24_M) {
		this.quantidadeInspecionados13_24_M = quantidadeInspecionados13_24_M;
	}

	public Integer getQuantidadeInspecionados25_36_F() {
		return quantidadeInspecionados25_36_F;
	}

	public void setQuantidadeInspecionados25_36_F(
			Integer quantidadeInspecionados25_36_F) {
		this.quantidadeInspecionados25_36_F = quantidadeInspecionados25_36_F;
	}

	public Integer getQuantidadeInspecionados25_36_M() {
		return quantidadeInspecionados25_36_M;
	}

	public void setQuantidadeInspecionados25_36_M(
			Integer quantidadeInspecionados25_36_M) {
		this.quantidadeInspecionados25_36_M = quantidadeInspecionados25_36_M;
	}

	public Integer getQuantidadeInspecionadosAcima36_F() {
		return quantidadeInspecionadosAcima36_F;
	}

	public void setQuantidadeInspecionadosAcima36_F(
			Integer quantidadeInspecionadosAcima36_F) {
		this.quantidadeInspecionadosAcima36_F = quantidadeInspecionadosAcima36_F;
	}

	public Integer getQuantidadeInspecionadosAcima36_M() {
		return quantidadeInspecionadosAcima36_M;
	}

	public void setQuantidadeInspecionadosAcima36_M(
			Integer quantidadeInspecionadosAcima36_M) {
		this.quantidadeInspecionadosAcima36_M = quantidadeInspecionadosAcima36_M;
	}

	public Integer getQuantidadeLeiteiras() {
		return quantidadeLeiteiras;
	}

	public void setQuantidadeLeiteiras(Integer quantidadeLeiteiras) {
		this.quantidadeLeiteiras = quantidadeLeiteiras;
	}

	public Integer getQuantidadeReprodutoras() {
		return quantidadeReprodutoras;
	}

	public void setQuantidadeReprodutoras(Integer quantidadeReprodutoras) {
		this.quantidadeReprodutoras = quantidadeReprodutoras;
	}

	public Integer getPorcentagemNatalidade() {
		return porcentagemNatalidade;
	}

	public void setPorcentagemNatalidade(Integer porcentagemNatalidade) {
		this.porcentagemNatalidade = porcentagemNatalidade;
	}

	public Integer getQuantidadeVistoriados() {
		return quantidadeVistoriados;
	}

	public void setQuantidadeVistoriados(Integer quantidadeVistoriados) {
		this.quantidadeVistoriados = quantidadeVistoriados;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VigilanciaBovinosEBubalinos other = (VigilanciaBovinosEBubalinos) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}