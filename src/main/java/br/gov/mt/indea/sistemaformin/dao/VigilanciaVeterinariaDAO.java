package br.gov.mt.indea.sistemaformin.dao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import br.gov.mt.indea.sistemaformin.entity.Municipio;
import br.gov.mt.indea.sistemaformin.entity.VigilanciaVeterinaria;
import br.gov.mt.indea.sistemaformin.exception.ApplicationException;
import br.gov.mt.indea.sistemaformin.util.StringUtil;

@Stateless
public class VigilanciaVeterinariaDAO extends DAO<VigilanciaVeterinaria> {
	
	@Inject
	private EntityManager em;
	
	public VigilanciaVeterinariaDAO() {
		super(VigilanciaVeterinaria.class);
	}

	@Override
	public EntityManager getEntityManager() {
		return this.em;
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void add(VigilanciaVeterinaria vigilanciaVeterinaria) throws ApplicationException{
		vigilanciaVeterinaria = this.getEntityManager().merge(vigilanciaVeterinaria);
		super.add(vigilanciaVeterinaria);
	}
	
	public List<VigilanciaVeterinaria> findBy(Municipio municipio, String numero, String propriedade, Date data) throws ApplicationException{
		try	{
			CriteriaBuilder cb = this.getEntityManager().getCriteriaBuilder();
			CriteriaQuery<VigilanciaVeterinaria> query = cb.createQuery(VigilanciaVeterinaria.class);
			
			Root<VigilanciaVeterinaria> t = query.from(VigilanciaVeterinaria.class);
			
			List<Predicate> predicados = new ArrayList<Predicate>();
			
			if (municipio != null)
				predicados.add(cb.equal(t.get("propriedade").get("municipio"), municipio));
			
			if (numero != null && !numero.equals(""))
				predicados.add(cb.equal(t.get("numero"), numero));
			
			if (propriedade != null && !propriedade.equals("")){
				predicados.add(cb.like(
					cb.function("remove_acento", String.class, cb.lower(t.get("propriedade").<String>get("nome"))), 
					"%" + StringUtil.removeAcentos(propriedade.toLowerCase() + "%")));
			}
			

			if (data != null){
				if (data != null){
					Calendar i = Calendar.getInstance();
					
					i.setTime(data);
					i.set(Calendar.HOUR_OF_DAY, 0);
					i.set(Calendar.MINUTE, 0);
					i.set(Calendar.SECOND, 0);
					
					Calendar f = Calendar.getInstance();
					
					f.setTime(data);
					f.set(Calendar.HOUR_OF_DAY, 23);
					f.set(Calendar.MINUTE, 59);
					f.set(Calendar.SECOND, 59);
					
					Path<Date> dataVigilanciaPath = t.get("dataVigilancia");
					predicados.add(cb.between(dataVigilanciaPath, i.getTime(), f.getTime()));
				}
			}
			
			query.where(cb.and(predicados.toArray(new Predicate[]{})));
			query.orderBy(cb.desc(t.get("dataVigilancia")));
			
			return this.getEntityManager().createQuery(query).getResultList();
		}catch(Exception e){
			throw new ApplicationException("N�o foi poss�vel listar os registros", e);
		}
	}

}
