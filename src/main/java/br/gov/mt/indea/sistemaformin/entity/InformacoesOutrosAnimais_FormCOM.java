package br.gov.mt.indea.sistemaformin.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;

import org.hibernate.envers.Audited;

@Audited
@Entity
@DiscriminatorValue("outrosAnimais")
public class InformacoesOutrosAnimais_FormCOM extends AbstractInformacoesDeAnimais_FormCOM implements Cloneable{

	private static final long serialVersionUID = -1025406482483570563L;
	
	private String nome;
	
	@OneToMany(mappedBy = "informacoesOutrosAnimais", cascade={CascadeType.ALL}, orphanRemoval=true)
	@OrderBy("id")
	private List<DetalhesInformacoesOutrosAnimais_FormCOM> detalhes = new ArrayList<DetalhesInformacoesOutrosAnimais_FormCOM>();
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<DetalhesInformacoesOutrosAnimais_FormCOM> getDetalhes() {
		return detalhes;
	}

	public void setDetalhes(List<DetalhesInformacoesOutrosAnimais_FormCOM> detalhes) {
		this.detalhes = detalhes;
	}

	@Override
	protected List<AbstractDetalhesInformacoesDeAnimais_FormCOM> getDetalhesInformacoesDeAnimais() {
		List<AbstractDetalhesInformacoesDeAnimais_FormCOM> lista = new ArrayList<AbstractDetalhesInformacoesDeAnimais_FormCOM>();
		lista.addAll(this.detalhes);
		return lista;
	}

}
