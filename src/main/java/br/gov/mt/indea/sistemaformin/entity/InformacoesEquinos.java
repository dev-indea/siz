package br.gov.mt.indea.sistemaformin.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;

import org.hibernate.envers.Audited;

import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoDestinoExploracao;

@Audited
@Entity
@DiscriminatorValue("equinos")
public class InformacoesEquinos extends AbstractInformacoesDeAnimais implements Cloneable{

	private static final long serialVersionUID = 7673446298964333656L;
	
	@OneToMany(mappedBy = "informacoesEquinos", cascade={CascadeType.ALL}, orphanRemoval=true)
	@OrderBy("id")
	private List<DetalhesInformacoesEquinos> detalhes = new ArrayList<DetalhesInformacoesEquinos>();

	public List<DetalhesInformacoesEquinos> getDetalhes() {
		return detalhes;
	}

	public void setDetalhes(List<DetalhesInformacoesEquinos> detalhes) {
		this.detalhes = detalhes;
	}

	@Override
	protected List<AbstractDetalhesInformacoesDeAnimais> getDetalhesInformacoesDeAnimais() {
		List<AbstractDetalhesInformacoesDeAnimais> lista = new ArrayList<AbstractDetalhesInformacoesDeAnimais>();
		lista.addAll(this.detalhes);
		return lista;
	}
	
	protected Object clone(FormIN formIN) throws CloneNotSupportedException{
		InformacoesEquinos cloned = (InformacoesEquinos) super.clone();
		
		cloned.setId(null);
		
		if (this.getDestinos() != null){
			cloned.setDestinos(new ArrayList<TipoDestinoExploracao>());
			for (TipoDestinoExploracao destino : this.getDestinos()) {
				cloned.getDestinos().add(destino);
			}
		}
		
		if (this.detalhes != null){
			cloned.setDetalhes(new ArrayList<DetalhesInformacoesEquinos>());
			for (DetalhesInformacoesEquinos detalhe : this.detalhes) {
				cloned.getDetalhes().add((DetalhesInformacoesEquinos) detalhe.clone(cloned));
			}
		}
		
		cloned.setFormIN(formIN);
		
		return cloned;
	}

}
