package br.gov.mt.indea.sistemaformin.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.mt.indea.sistemaformin.entity.TipoChavePrincipalTermoFiscalizacao;
import br.gov.mt.indea.sistemaformin.entity.TipoChaveSecundariaTermoFiscalizacao;
import br.gov.mt.indea.sistemaformin.entity.dto.AbstractDTO;
import br.gov.mt.indea.sistemaformin.entity.dto.TipoChaveSecundariaTermoFiscalizacaoDTO;
import br.gov.mt.indea.sistemaformin.enums.Dominio.AtivoInativo;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class TipoChaveSecundariaTermoFiscalizacaoService extends PaginableService<TipoChaveSecundariaTermoFiscalizacao, Long> {

	private static final Logger log = LoggerFactory.getLogger(TipoChaveSecundariaTermoFiscalizacaoService.class);
	
	protected TipoChaveSecundariaTermoFiscalizacaoService() {
		super(TipoChaveSecundariaTermoFiscalizacao.class);
	}
	
	public TipoChaveSecundariaTermoFiscalizacao findByIdFetchAll(Long id){
		TipoChaveSecundariaTermoFiscalizacao tipoChaveSecundariaTermoFiscalizacao;
		
		StringBuilder sql = new StringBuilder();
		sql.append("select v ")
		   .append("  from TipoChaveSecundariaTermoFiscalizacao v ")
		   .append(" where v.id = :id ");
		
		Query query = getSession().createQuery(sql.toString()).setLong("id", id);
		tipoChaveSecundariaTermoFiscalizacao = (TipoChaveSecundariaTermoFiscalizacao) query.uniqueResult();
		
		return tipoChaveSecundariaTermoFiscalizacao;
	}
	
	@Override
	public void saveOrUpdate(TipoChaveSecundariaTermoFiscalizacao tipoChaveSecundariaTermoFiscalizacao) {
		super.saveOrUpdate(tipoChaveSecundariaTermoFiscalizacao);
		
		log.info("Salvando Tipo Chave Secundaria Termo Fiscalizacao {}", tipoChaveSecundariaTermoFiscalizacao.getId());
	}
	
	@Override
	public void delete(TipoChaveSecundariaTermoFiscalizacao tipoChaveSecundariaTermoFiscalizacao) {
		super.delete(tipoChaveSecundariaTermoFiscalizacao);
		
		log.info("Removendo Tipo Chave Secundaria Termo Fiscalizacao {}", tipoChaveSecundariaTermoFiscalizacao.getId());
	}
	
	@Override
	public void validar(TipoChaveSecundariaTermoFiscalizacao TipoChaveSecundariaTermoFiscalizacao) {

	}

	@Override
	public void validarPersist(TipoChaveSecundariaTermoFiscalizacao TipoChaveSecundariaTermoFiscalizacao) {

	}

	@Override
	public void validarMerge(TipoChaveSecundariaTermoFiscalizacao TipoChaveSecundariaTermoFiscalizacao) {

	}

	@Override
	public void validarDelete(TipoChaveSecundariaTermoFiscalizacao TipoChaveSecundariaTermoFiscalizacao) {

	}

	@SuppressWarnings("unchecked")
	public List<TipoChaveSecundariaTermoFiscalizacao> findAllByTipoChavePrincipal(TipoChavePrincipalTermoFiscalizacao tipoChavePrincipalTermoFiscalizacao, AtivoInativo status) {
		List<TipoChaveSecundariaTermoFiscalizacao> listaTipoChaveSecundariaTermoFiscalizacao;
		
		StringBuilder sql = new StringBuilder();
		sql.append("select v ")
		   .append("  from TipoChaveSecundariaTermoFiscalizacao v ")
		   .append(" where v.tipoChavePrincipalTermoFiscalizacao = :tipoChavePrincipalTermoFiscalizacao ")
		   .append("   and v.status = :status ");
		
		Query query = getSession().createQuery(sql.toString());
		query.setParameter("tipoChavePrincipalTermoFiscalizacao", tipoChavePrincipalTermoFiscalizacao);
		query.setParameter("status", status);

		listaTipoChaveSecundariaTermoFiscalizacao = query.list();
		
		return listaTipoChaveSecundariaTermoFiscalizacao;
	}
	
	@Override
	public Long countAll(AbstractDTO dto) {
		StringBuilder sql = new StringBuilder();
		sql.append("select count(entity) ")
		   .append("  from " + this.getType().getSimpleName() + " as entity");
		
		if (dto != null && !dto.isNull())
			sql.append(" where 1 = 1");
		else
			sql.append(" where 1 = 2");
		
		if (dto != null && !dto.isNull()){
			TipoChaveSecundariaTermoFiscalizacaoDTO tipoChaveSecundariaTermoFiscalizacaoDTO = (TipoChaveSecundariaTermoFiscalizacaoDTO) dto;
			
			if (tipoChaveSecundariaTermoFiscalizacaoDTO.getTipoChavePrincipalTermoFiscalizacao() != null)
				sql.append("  and tipoChavePrincipalTermoFiscalizacao = :tipoChavePrincipalTermoFiscalizacao");
		}
		
		Query query = getSession().createQuery(sql.toString());
		
		if (dto != null && !dto.isNull()){
			TipoChaveSecundariaTermoFiscalizacaoDTO tipoChaveSecundariaTermoFiscalizacaoDTO = (TipoChaveSecundariaTermoFiscalizacaoDTO) dto;
			
			if (tipoChaveSecundariaTermoFiscalizacaoDTO.getTipoChavePrincipalTermoFiscalizacao() != null)
				query.setParameter("tipoChavePrincipalTermoFiscalizacao", tipoChaveSecundariaTermoFiscalizacaoDTO.getTipoChavePrincipalTermoFiscalizacao());
		}

		return (Long) query.uniqueResult();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<TipoChaveSecundariaTermoFiscalizacao> findAllComPaginacao(AbstractDTO dto, int first, int pageSize,String sortField, String sortOrder) {
		StringBuilder sql = new StringBuilder();
		sql.append("select entity ")
		   .append("  from " + this.getType().getSimpleName() + " as entity");
		
		if (dto != null && !dto.isNull())
			sql.append(" where 1 = 1");
		else
			sql.append(" where 1 = 2");
		
		if (dto != null && !dto.isNull()){
			TipoChaveSecundariaTermoFiscalizacaoDTO tipoChaveSecundariaTermoFiscalizacaoDTO = (TipoChaveSecundariaTermoFiscalizacaoDTO) dto;
			
			if (tipoChaveSecundariaTermoFiscalizacaoDTO.getTipoChavePrincipalTermoFiscalizacao() != null)
				sql.append("  and tipoChavePrincipalTermoFiscalizacao = :tipoChavePrincipalTermoFiscalizacao");
		}
		
		if (StringUtils.isNotBlank(sortField))
			sql.append(" order by entity." + sortField + " " + sortOrder);

		Query query = getSession().createQuery(sql.toString());
		
		if (dto != null && !dto.isNull()){
			TipoChaveSecundariaTermoFiscalizacaoDTO tipoChaveSecundariaTermoFiscalizacaoDTO = (TipoChaveSecundariaTermoFiscalizacaoDTO) dto;
			
			if (tipoChaveSecundariaTermoFiscalizacaoDTO.getTipoChavePrincipalTermoFiscalizacao() != null)
				query.setParameter("tipoChavePrincipalTermoFiscalizacao", tipoChaveSecundariaTermoFiscalizacaoDTO.getTipoChavePrincipalTermoFiscalizacao());
		}

		query.setFirstResult(first);
		query.setMaxResults(pageSize);

		List<TipoChaveSecundariaTermoFiscalizacao> lista = query.list();
		
		return lista;
	}

}
