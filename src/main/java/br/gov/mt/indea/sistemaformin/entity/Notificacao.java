package br.gov.mt.indea.sistemaformin.entity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;
import org.springframework.util.StringUtils;

import br.gov.mt.indea.sistemaformin.enums.Dominio.Especie;
import br.gov.mt.indea.sistemaformin.enums.Dominio.SimNao;
import br.gov.mt.indea.sistemaformin.enums.Dominio.StatusNotificacao;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoMotivoNotificacao;
import br.gov.mt.indea.sistemaformin.webservice.entity.Endereco;

@Audited
@Entity
@Table(name="notificacao")
public class Notificacao extends BaseEntity<Long>{

	private static final long serialVersionUID = 3657868508326962579L;

	@Id
	@SequenceGenerator(name="form_notifica_seq", sequenceName="form_notifica_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="form_notifica_seq")
	private Long id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_cadastro")
	private Calendar dataCadastro;
	
	@Column(unique = true)
	private String numero;
	
	@Column(name = "codigo_seguranca")
	private String codigoSeguranca;
	
	@Enumerated(EnumType.STRING)
	@Column(name="status")
	private StatusNotificacao statusNotificacao = StatusNotificacao.NOVO;
	
	@OneToOne(mappedBy="formNotifica", cascade=CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name="id_notificante")
	private Notificante notificante;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "relacao_notificante_propriedade")
	private SimNao relacaoDoNotificanteComAPropriedade;
	
	@Column(name = "nome_estabelecimento")
	private String nomeEstabelecimento;
	
	@OneToOne(cascade=CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name="id_endereco")
	private Endereco endereco = new Endereco();
	
	@Column(name = "nome_contato_estab")
	private String nomeContatoEstabelecimento;
	
	@Column(name = "telefone_contato_estab")
    private String telefoneContatoEstabelecimento;
	
	@Column(name = "tipo_contato_estab")
    private String tipoContatoEstabelecimento;
	
	@Enumerated(EnumType.STRING)
	@Column(name="tipo_motivo_notificacao")
    private TipoMotivoNotificacao tipoMotivoNotificacao;
	
	@Column(name = "resumo_notificacao", length = 2000)
    private String resumoNotificacao;
	
	@ElementCollection(targetClass = Especie.class, fetch=FetchType.EAGER)
	@JoinTable(name = "notificacao_especies_afetadas", joinColumns = @JoinColumn(name = "id_notificacao", nullable = false))
	@Column(name = "id_especie")
	@Enumerated(EnumType.STRING)
	private List<Especie> especiesAfetadas = new ArrayList<Especie>();
	
	public boolean isNotificanteAnonimo() {
		if (this.notificante == null)
			return true;
		else if (StringUtils.isEmpty(this.notificante.getPessoa().getNome()) && StringUtils.isEmpty(this.notificante.getPessoa().getEndereco().getTelefone())){
			return true;
		}
		return false;
	}
	
	public boolean isContatoInformado() {
		if (StringUtils.isEmpty(this.nomeContatoEstabelecimento) && StringUtils.isEmpty(this.telefoneContatoEstabelecimento) && StringUtils.isEmpty(this.tipoContatoEstabelecimento))
			return false;
		return true;
	}
	
	public String getEspeciesAfetadasAsString() {
		if (this.getEspeciesAfetadas() == null || this.getEspeciesAfetadas().isEmpty())
			return null;
		
		StringBuilder sb = null;
		
		if (getEspeciesAfetadas() != null && !getEspeciesAfetadas().isEmpty())
			for (Especie especie : getEspeciesAfetadas()) {
				if (sb == null){
					sb = new StringBuilder();
					sb.append(especie.getDescricao());
				}else
					sb.append(", ")
					  .append(especie.getDescricao());
				}
		
		return sb == null ? "" : sb.toString();
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Calendar getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Calendar dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Notificante getNotificante() {
		return notificante;
	}

	public void setNotificante(Notificante notificante) {
		this.notificante = notificante;
	}

	public String getNomeEstabelecimento() {
		return nomeEstabelecimento;
	}
	
	public void setNomeEstabelecimento(String nomeEstabelecimento) {
		this.nomeEstabelecimento = nomeEstabelecimento;
	}
	
	public Endereco getEndereco() {
		return endereco;
	}
	
	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
	
	public String getNomeContatoEstabelecimento() {
		return nomeContatoEstabelecimento;
	}
	
	public void setNomeContatoEstabelecimento(String nomeContatoEstabelecimento) {
		this.nomeContatoEstabelecimento = nomeContatoEstabelecimento;
	}
	
	public String getTelefoneContatoEstabelecimento() {
		return telefoneContatoEstabelecimento;
	}
	
	public void setTelefoneContatoEstabelecimento(String telefoneContatoEstabelecimento) {
		this.telefoneContatoEstabelecimento = telefoneContatoEstabelecimento;
	}
	
	public String getTipoContatoEstabelecimento() {
		return tipoContatoEstabelecimento;
	}
	
	public void setTipoContatoEstabelecimento(String tipoContatoEstabelecimento) {
		this.tipoContatoEstabelecimento = tipoContatoEstabelecimento;
	}
	
	public TipoMotivoNotificacao getTipoMotivoNotificacao() {
		return tipoMotivoNotificacao;
	}

	public void setTipoMotivoNotificacao(TipoMotivoNotificacao tipoMotivoNotificacao) {
		this.tipoMotivoNotificacao = tipoMotivoNotificacao;
	}

	public String getResumoNotificacao() {
		return resumoNotificacao;
	}

	public void setResumoNotificacao(String resumoNotificacao) {
		this.resumoNotificacao = resumoNotificacao;
	}

	public List<Especie> getEspeciesAfetadas() {
		return especiesAfetadas;
	}

	public void setEspeciesAfetadas(List<Especie> especiesAfetadas) {
		this.especiesAfetadas = especiesAfetadas;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public SimNao getRelacaoDoNotificanteComAPropriedade() {
		return relacaoDoNotificanteComAPropriedade;
	}

	public void setRelacaoDoNotificanteComAPropriedade(SimNao relacaoDoNotificanteComAPropriedade) {
		this.relacaoDoNotificanteComAPropriedade = relacaoDoNotificanteComAPropriedade;
	}

	public String getCodigoSeguranca() {
		return codigoSeguranca;
	}

	public void setCodigoSeguranca(String codigoSeguranca) {
		this.codigoSeguranca = codigoSeguranca;
	}

	public StatusNotificacao getStatusNotificacao() {
		return statusNotificacao;
	}

	public void setStatusNotificacao(StatusNotificacao statusNotificacao) {
		this.statusNotificacao = statusNotificacao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Notificacao other = (Notificacao) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}