package br.gov.mt.indea.sistemaformin.webservice.client;

import java.io.Serializable;
import java.net.URISyntaxException;

import javax.inject.Inject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import br.gov.mt.indea.sistemaformin.exception.ApplicationException;
import br.gov.mt.indea.sistemaformin.webservice.convert.RecintoConverter;
import br.gov.mt.indea.sistemaformin.webservice.model.Recinto;


public class ClientWebServiceRecinto implements Serializable{
	
	private static final long serialVersionUID = -6524521505872165758L;

	protected String URL_WS = "https://sistemas.indea.mt.gov.br/FronteiraWeb/ws/recinto/";
	
//	@Inject
//	@Category("WEBSERVICE::RECINTO")
//	private CustomLogger log;
	
	@Inject
	private RecintoConverter recintoConverter;
	
	public br.gov.mt.indea.sistemaformin.webservice.entity.Recinto getRecintoById(Long id) throws ApplicationException {

		if (id > Integer.MAX_VALUE)
			return null;

		String[] resposta = null;
		try {
			System.out.println(URL_WS + "id/" + id);
			resposta = new WebServiceCliente().get(URL_WS + "id/" + id);
		} catch (URISyntaxException e) {
			//log.excecao(e.getMessage(), e);
			throw new ApplicationException("Erro ao buscar a recinto");
		}
		
		if (resposta == null || resposta[0] == null){
			return null;
		}
			
		if (resposta[0].equals("200")) {
			GsonBuilder gsonBuilder = new GsonBuilder().setDateFormat("dd/MM/yyyy");
			Gson gson = gsonBuilder.create();
			Recinto recinto = gson.fromJson(resposta[1], Recinto.class);
			
			return recintoConverter.fromModelToEntity(recinto);
		} else if (resposta[0].equals("404")) {
			//log.excecao(resposta[0] + ". " + resposta[1], null);
			throw new ApplicationException("Webservice com problemas. Contate o administrador do sistema");
		} else if (resposta[0].equals("500")) {
			//log.excecao(resposta[0] + ". " + resposta[1], null);
			throw new ApplicationException("Webservice com problemas. Contate o administrador do sistema");
		} else if (resposta[0].equals("400")) {
			//log.excecao(resposta[0] + ". " + resposta[1], null);
			throw new ApplicationException("Webservice com problemas. Contate o administrador do sistema");
		} else{
			//log.excecao(resposta[0] + ". " + resposta[1], null);
			throw new ApplicationException("Webservice inativo. Tente novamente em alguns minutos. Erro: " + resposta[0]);
		} 
	}
	
}
