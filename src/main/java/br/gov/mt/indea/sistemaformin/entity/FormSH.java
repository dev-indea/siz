package br.gov.mt.indea.sistemaformin.entity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

import br.gov.mt.indea.sistemaformin.enums.Dominio.SimNao;
import br.gov.mt.indea.sistemaformin.enums.Dominio.SimNaoNA;
import br.gov.mt.indea.sistemaformin.enums.Dominio.SinaisClinicosSindromeHemorragica;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoCriacaoAnimais;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoOrigemRacaoAnimais;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoOrigemRestosDeComida;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoOrigemSoroOuRestosDeLavoura;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoReposicaoAnimais;

@Audited
@Entity
@Table(name="form_sh")
public class FormSH extends BaseEntity<Long>{

	private static final long serialVersionUID = -2433246362033951832L;

	@Id
	@SequenceGenerator(name="form_sh_seq", sequenceName="form_sh_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="form_sh_seq")
	private Long id;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_usuario")
	private Usuario usuario;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_cadastro")
	private Calendar dataCadastro;
	
	@ManyToOne
	@JoinColumn(name="id_formin")
	private FormIN formIN;
	
	@ManyToOne
	@JoinColumn(name="id_formcom")
	private FormCOM formCOM;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_investigacao")
	private Calendar dataInvestigacao;
	
	@Column(name="integradora_ou_cooperativa", length=255)
    private String empresaIntegradoraOuCooperativa;
    
    @Column(name="granja_ou_local_de_origem", length=255)
    private String granjaOuLocalDeOrigemDosAnimais;
    
    @Column(name="codigo_granja_ou_local_origem")
    private Integer codigoGranjaOuLocalDeOrigem;
	
	@Enumerated(EnumType.STRING)
	@Column(name="info_estb_proximos_com_suideos")
	private SimNao possuiInformacaoSobreEstabelecimentosProximosComSuideos;
	
	@Column(name="quantidade_estab_proximos")
	private Long quantidadeEstabelecimentosComSuideosProximos;
	
	@Column(name="distancia_da_via_mais_proxima")
	private Long distanciaDaViaMaisProxima;

	@Column(name="distancia_estab_mais_proximo")
	private Long distanciaDoEstabelecimentoMaisProximo;
	
	@ElementCollection(targetClass = TipoCriacaoAnimais.class)
	@JoinTable(name = "formsh_tipo_criacao_animais", joinColumns = @JoinColumn(name = "id_formsh", nullable = false))
	@Column(name = "id_formsh_tipo_criacao_animais")
	@Enumerated(EnumType.STRING)
	private List<TipoCriacaoAnimais> tipoCriacaoAnimais = new ArrayList<TipoCriacaoAnimais>();

	@ElementCollection(targetClass = TipoReposicaoAnimais.class)
	@JoinTable(name = "formsh_tipo_reposicao_animais", joinColumns = @JoinColumn(name = "id_formsh", nullable = false))
	@Column(name = "id_formsh_tipo_reposicao_animais")
	@Enumerated(EnumType.STRING)
	private List<TipoReposicaoAnimais> tipoReposicaoAnimais = new ArrayList<TipoReposicaoAnimais>();
	
	@ElementCollection(targetClass = TipoOrigemRacaoAnimais.class)
	@JoinTable(name = "formsh_tipo_racao_animais", joinColumns = @JoinColumn(name = "id_formsh", nullable = false))
	@Column(name = "id_formsh_tipo_racao_animais")
	@Enumerated(EnumType.STRING)
	private List<TipoOrigemRacaoAnimais> tipoOrigemRacaoAnimais = new ArrayList<TipoOrigemRacaoAnimais>();
	
	@ElementCollection(targetClass = TipoOrigemRestosDeComida.class)
	@JoinTable(name = "formsh_tipo_restos_comida", joinColumns = @JoinColumn(name = "id_formsh", nullable = false))
	@Column(name = "id_formsh_tipo_restos_comida")
	@Enumerated(EnumType.STRING)
	private List<TipoOrigemRestosDeComida> tipoOrigemRestosDeComida = new ArrayList<TipoOrigemRestosDeComida>();

	@Column(name="outra_origem_restos_de_comida", length=255)
	private String outraOrigemRestosDeComida;
	
	@ElementCollection(targetClass = TipoOrigemSoroOuRestosDeLavoura.class)
	@JoinTable(name = "formsh_origem_soro_restos", joinColumns = @JoinColumn(name = "id_formsh", nullable = false))
	@Column(name = "id_formsh_origem_soro_restos")
	@Enumerated(EnumType.STRING)
	private List<TipoOrigemSoroOuRestosDeLavoura> tipoOrigemSoroOuRestosDeLavoura = new ArrayList<TipoOrigemSoroOuRestosDeLavoura>();
	
	@Enumerated(EnumType.STRING)
	@Column(name="alimentos_tratados_termicamente")
	private SimNaoNA alimentosTratadosTermicamente;
	
	@Column(name="detalhe_alimentos_trat_termic", length=255)
	private String detalheAlimentosTratadosTermicamente;
	
	@Enumerated(EnumType.STRING)
	@Column(name="animais_com_acesso_a_lixoes")
	private SimNaoNA animaisComAcessoALixoes;

	@Enumerated(EnumType.STRING)
	@Column(name="proteina_animal_como_alimento")
	private SimNaoNA proteinaAnimalComoAlimento;
	
	@Enumerated(EnumType.STRING)
	@Column(name="contato_suinos_asselvajados")
	private SimNaoNA contatoComSuinosAsselvajados;
	
	@Enumerated(EnumType.STRING)
	@Column(name="contato_suinos_Domesticos")
	private SimNaoNA contatoComSuinosDomesticos;
	
	@Enumerated(EnumType.STRING)
	@Column(name="uso_irregular_vacina_psc")
	private SimNaoNA usoIrregularVacinaPSC;
	
	@Enumerated(EnumType.STRING)
	@Column(name="agua_tratada")
	private SimNaoNA aguaPassaPorTratamento;
	
	@Column(name="detalhe_tratamento_agua", length=255)
	private String detalheTratamentoAgua;
	
	@Enumerated(EnumType.STRING)
	@Column(name="existe_vazio_sanit_entre_lote")
	private SimNaoNA existeVazioSanitarioEntreLotes;
	
	@Column(name="qtdade_dias_vazio_sanitario")
	private Long quantidadeDiasVazioSanitario;
	
	@Enumerated(EnumType.STRING)
	@Column(name="alta_mortalidade_animais_jovens")
	private SimNaoNA altaMortalidadeDeAnimaisJovens;
	
	@Enumerated(EnumType.STRING)
	@Column(name="morte_em_5_a_25_dias_apos_doente")
	private SimNaoNA morteEm5a25DiasAposDoente;
	
	@Enumerated(EnumType.STRING)
	@Column(name="natimortalidade")
	private SimNaoNA natimortalidade;
	
	@Enumerated(EnumType.STRING)
	@Column(name="leitegada_com_poucos_leitoes")
	private SimNaoNA leitegadaComPoucosLeitoes;
	
	@Enumerated(EnumType.STRING)
	@Column(name="tremor_congenito_ou_debilidade")
	private SimNaoNA tremorCongenitoOuDebilidade;
	
	@Enumerated(EnumType.STRING)
	@Column(name="alto_numero_de_animais_refugo")
	private SimNaoNA altoNumeroDeAnimaisRefugo;
	
	@Enumerated(EnumType.STRING)
	@Column(name="mumificacao_fetal")
	private SimNaoNA mumificacaoFetal;
	
	@ElementCollection(targetClass = SinaisClinicosSindromeHemorragica.class)
	@JoinTable(name = "formsh_sinais_estado_geral", joinColumns = @JoinColumn(name = "id_formsh", nullable = false))
	@Column(name = "id_formsh_sinais_estado_geral")
	@Enumerated(EnumType.STRING)
	private List<SinaisClinicosSindromeHemorragica> sinaisClinicosEstadoGeral = new ArrayList<SinaisClinicosSindromeHemorragica>();
	
	@ElementCollection(targetClass = SinaisClinicosSindromeHemorragica.class)
	@JoinTable(name = "formsh_sinais_sist_resp", joinColumns = @JoinColumn(name = "id_formsh", nullable = false))
	@Column(name = "id_formsh_sinais_sist_resp")
	@Enumerated(EnumType.STRING)
	private List<SinaisClinicosSindromeHemorragica> sinaisClinicosSistemaRespiratorio = new ArrayList<SinaisClinicosSindromeHemorragica>();
	
	@ElementCollection(targetClass = SinaisClinicosSindromeHemorragica.class)
	@JoinTable(name = "formsh_sinais_sist_nervoso", joinColumns = @JoinColumn(name = "id_formsh", nullable = false))
	@Column(name = "id_formsh_sinais_sist_nervoso")
	@Enumerated(EnumType.STRING)
	private List<SinaisClinicosSindromeHemorragica> sinaisClinicosSistemaNervoso = new ArrayList<SinaisClinicosSindromeHemorragica>();
	
	@ElementCollection(targetClass = SinaisClinicosSindromeHemorragica.class)
	@JoinTable(name = "formsh_sinais_sist_digestorio", joinColumns = @JoinColumn(name = "id_formsh", nullable = false))
	@Column(name = "id_formsh_sinais_sist_digestorio")
	@Enumerated(EnumType.STRING)
	private List<SinaisClinicosSindromeHemorragica> sinaisClinicosSistemaDigestorio = new ArrayList<SinaisClinicosSindromeHemorragica>();
	
	@ElementCollection(targetClass = SinaisClinicosSindromeHemorragica.class)
	@JoinTable(name = "formsh_sinais_sist_reprodutivo", joinColumns = @JoinColumn(name = "id_formsh", nullable = false))
	@Column(name = "id_formsh_sinais_sist_reprodutivo")
	@Enumerated(EnumType.STRING)
	private List<SinaisClinicosSindromeHemorragica> sinaisClinicosSistemaReprodutivo = new ArrayList<SinaisClinicosSindromeHemorragica>();
	
	@ElementCollection(targetClass = SinaisClinicosSindromeHemorragica.class)
	@JoinTable(name = "formsh_sinais_sist_tegumentar", joinColumns = @JoinColumn(name = "id_formsh", nullable = false))
	@Column(name = "id_formsh_sinais_sist_tegumentar")
	@Enumerated(EnumType.STRING)
	private List<SinaisClinicosSindromeHemorragica> sinaisClinicosSistemaTegumentar = new ArrayList<SinaisClinicosSindromeHemorragica>();
	
	@ElementCollection(targetClass = SinaisClinicosSindromeHemorragica.class)
	@JoinTable(name = "formsh_sinais_sist_linfatico", joinColumns = @JoinColumn(name = "id_formsh", nullable = false))
	@Column(name = "id_formsh_sinais_sist_linfatico")
	@Enumerated(EnumType.STRING)
	private List<SinaisClinicosSindromeHemorragica> sinaisClinicosSistemaLinfatico = new ArrayList<SinaisClinicosSindromeHemorragica>();
	
	@Column(name="informacoes_adicionais", length=400)
	private String informacoesAdicionais;
	
	@OneToMany(mappedBy="formSH", cascade={CascadeType.ALL}, orphanRemoval=true)
	private List<NovoEstabelecimentoParaInvestigacao> novoEstabelecimentoParaInvestigacaos = new ArrayList<NovoEstabelecimentoParaInvestigacao>();
	
	@Enumerated(EnumType.STRING)
	@Column(name="estab_possui_vet_permanente")
	private SimNao estabelecimentoPossuiAssistenciaVeterinariaPermanente;
	
	@OneToOne(cascade={CascadeType.ALL}, orphanRemoval=true)
	@JoinColumn(name="id_veterinario_assistente")
	private br.gov.mt.indea.sistemaformin.webservice.entity.Veterinario veterinarioAssistenteDoEstabelecimento;
	
	@OneToOne(cascade={CascadeType.ALL}, orphanRemoval=true)
	@JoinColumn(name="id_veterinario")
	private br.gov.mt.indea.sistemaformin.webservice.entity.Veterinario veterinarioResponsavelPeloAtendimento;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Calendar getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Calendar dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public FormIN getFormIN() {
		return formIN;
	}

	public void setFormIN(FormIN formIN) {
		this.formIN = formIN;
	}

	public FormCOM getFormCOM() {
		return formCOM;
	}

	public void setFormCOM(FormCOM formCOM) {
		this.formCOM = formCOM;
	}

	public Calendar getDataInvestigacao() {
		return dataInvestigacao;
	}

	public void setDataInvestigacao(Calendar dataInvestigacao) {
		this.dataInvestigacao = dataInvestigacao;
	}

	public Long getQuantidadeEstabelecimentosComSuideosProximos() {
		return quantidadeEstabelecimentosComSuideosProximos;
	}

	public void setQuantidadeEstabelecimentosComSuideosProximos(
			Long quantidadeEstabelecimentosComSuideosProximos) {
		this.quantidadeEstabelecimentosComSuideosProximos = quantidadeEstabelecimentosComSuideosProximos;
	}

	public Long getDistanciaDaViaMaisProxima() {
		return distanciaDaViaMaisProxima;
	}

	public void setDistanciaDaViaMaisProxima(Long distanciaDaViaMaisProxima) {
		this.distanciaDaViaMaisProxima = distanciaDaViaMaisProxima;
	}

	public Long getDistanciaDoEstabelecimentoMaisProximo() {
		return distanciaDoEstabelecimentoMaisProximo;
	}

	public void setDistanciaDoEstabelecimentoMaisProximo(
			Long distanciaDoEstabelecimentoMaisProximo) {
		this.distanciaDoEstabelecimentoMaisProximo = distanciaDoEstabelecimentoMaisProximo;
	}

	public List<TipoCriacaoAnimais> getTipoCriacaoAnimais() {
		return tipoCriacaoAnimais;
	}

	public void setTipoCriacaoAnimais(List<TipoCriacaoAnimais> tipoCriacaoAnimais) {
		this.tipoCriacaoAnimais = tipoCriacaoAnimais;
	}

	public List<TipoReposicaoAnimais> getTipoReposicaoAnimais() {
		return tipoReposicaoAnimais;
	}

	public void setTipoReposicaoAnimais(
			List<TipoReposicaoAnimais> tipoReposicaoAnimais) {
		this.tipoReposicaoAnimais = tipoReposicaoAnimais;
	}

	public List<TipoOrigemRacaoAnimais> getTipoOrigemRacaoAnimais() {
		return tipoOrigemRacaoAnimais;
	}

	public void setTipoOrigemRacaoAnimais(
			List<TipoOrigemRacaoAnimais> tipoOrigemRacaoAnimais) {
		this.tipoOrigemRacaoAnimais = tipoOrigemRacaoAnimais;
	}

	public List<TipoOrigemRestosDeComida> getTipoOrigemRestosDeComida() {
		return tipoOrigemRestosDeComida;
	}

	public void setTipoOrigemRestosDeComida(
			List<TipoOrigemRestosDeComida> tipoOrigemRestosDeComida) {
		this.tipoOrigemRestosDeComida = tipoOrigemRestosDeComida;
	}

	public String getOutraOrigemRestosDeComida() {
		return outraOrigemRestosDeComida;
	}

	public void setOutraOrigemRestosDeComida(String outraOrigemRestosDeComida) {
		this.outraOrigemRestosDeComida = outraOrigemRestosDeComida;
	}

	public List<TipoOrigemSoroOuRestosDeLavoura> getTipoOrigemSoroOuRestosDeLavoura() {
		return tipoOrigemSoroOuRestosDeLavoura;
	}

	public void setTipoOrigemSoroOuRestosDeLavoura(
			List<TipoOrigemSoroOuRestosDeLavoura> tipoOrigemSoroOuRestosDeLavoura) {
		this.tipoOrigemSoroOuRestosDeLavoura = tipoOrigemSoroOuRestosDeLavoura;
	}

	public SimNaoNA getAlimentosTratadosTermicamente() {
		return alimentosTratadosTermicamente;
	}

	public void setAlimentosTratadosTermicamente(
			SimNaoNA alimentosTratadosTermicamente) {
		this.alimentosTratadosTermicamente = alimentosTratadosTermicamente;
	}

	public String getDetalheAlimentosTratadosTermicamente() {
		return detalheAlimentosTratadosTermicamente;
	}

	public void setDetalheAlimentosTratadosTermicamente(
			String detalheAlimentosTratadosTermicamente) {
		this.detalheAlimentosTratadosTermicamente = detalheAlimentosTratadosTermicamente;
	}

	public SimNaoNA getAnimaisComAcessoALixoes() {
		return animaisComAcessoALixoes;
	}

	public void setAnimaisComAcessoALixoes(SimNaoNA animaisComAcessoALixoes) {
		this.animaisComAcessoALixoes = animaisComAcessoALixoes;
	}

	public SimNaoNA getProteinaAnimalComoAlimento() {
		return proteinaAnimalComoAlimento;
	}

	public void setProteinaAnimalComoAlimento(SimNaoNA proteinaAnimalComoAlimento) {
		this.proteinaAnimalComoAlimento = proteinaAnimalComoAlimento;
	}

	public SimNaoNA getContatoComSuinosAsselvajados() {
		return contatoComSuinosAsselvajados;
	}

	public void setContatoComSuinosAsselvajados(
			SimNaoNA contatoComSuinosAsselvajados) {
		this.contatoComSuinosAsselvajados = contatoComSuinosAsselvajados;
	}

	public SimNaoNA getContatoComSuinosDomesticos() {
		return contatoComSuinosDomesticos;
	}

	public void setContatoComSuinosDomesticos(SimNaoNA contatoComSuinosDomesticos) {
		this.contatoComSuinosDomesticos = contatoComSuinosDomesticos;
	}

	public SimNaoNA getUsoIrregularVacinaPSC() {
		return usoIrregularVacinaPSC;
	}

	public void setUsoIrregularVacinaPSC(SimNaoNA usoIrregularVacinaPSC) {
		this.usoIrregularVacinaPSC = usoIrregularVacinaPSC;
	}

	public SimNaoNA getAguaPassaPorTratamento() {
		return aguaPassaPorTratamento;
	}

	public void setAguaPassaPorTratamento(SimNaoNA aguaPassaPorTratamento) {
		this.aguaPassaPorTratamento = aguaPassaPorTratamento;
	}

	public String getDetalheTratamentoAgua() {
		return detalheTratamentoAgua;
	}

	public void setDetalheTratamentoAgua(String detalheTratamentoAgua) {
		this.detalheTratamentoAgua = detalheTratamentoAgua;
	}

	public SimNaoNA getExisteVazioSanitarioEntreLotes() {
		return existeVazioSanitarioEntreLotes;
	}

	public void setExisteVazioSanitarioEntreLotes(
			SimNaoNA existeVazioSanitarioEntreLotes) {
		this.existeVazioSanitarioEntreLotes = existeVazioSanitarioEntreLotes;
	}

	public Long getQuantidadeDiasVazioSanitario() {
		return quantidadeDiasVazioSanitario;
	}

	public void setQuantidadeDiasVazioSanitario(Long quantidadeDiasVazioSanitario) {
		this.quantidadeDiasVazioSanitario = quantidadeDiasVazioSanitario;
	}

	public SimNaoNA getAltaMortalidadeDeAnimaisJovens() {
		return altaMortalidadeDeAnimaisJovens;
	}

	public void setAltaMortalidadeDeAnimaisJovens(
			SimNaoNA altaMortalidadeDeAnimaisJovens) {
		this.altaMortalidadeDeAnimaisJovens = altaMortalidadeDeAnimaisJovens;
	}

	public SimNaoNA getMorteEm5a25DiasAposDoente() {
		return morteEm5a25DiasAposDoente;
	}

	public void setMorteEm5a25DiasAposDoente(SimNaoNA morteEm5a25DiasAposDoente) {
		this.morteEm5a25DiasAposDoente = morteEm5a25DiasAposDoente;
	}

	public SimNaoNA getNatimortalidade() {
		return natimortalidade;
	}

	public void setNatimortalidade(SimNaoNA natimortalidade) {
		this.natimortalidade = natimortalidade;
	}

	public SimNaoNA getLeitegadaComPoucosLeitoes() {
		return leitegadaComPoucosLeitoes;
	}

	public void setLeitegadaComPoucosLeitoes(SimNaoNA leitegadaComPoucosLeitoes) {
		this.leitegadaComPoucosLeitoes = leitegadaComPoucosLeitoes;
	}

	public SimNaoNA getTremorCongenitoOuDebilidade() {
		return tremorCongenitoOuDebilidade;
	}

	public void setTremorCongenitoOuDebilidade(SimNaoNA tremorCongenitoOuDebilidade) {
		this.tremorCongenitoOuDebilidade = tremorCongenitoOuDebilidade;
	}

	public SimNaoNA getAltoNumeroDeAnimaisRefugo() {
		return altoNumeroDeAnimaisRefugo;
	}

	public void setAltoNumeroDeAnimaisRefugo(SimNaoNA altoNumeroDeAnimaisRefugo) {
		this.altoNumeroDeAnimaisRefugo = altoNumeroDeAnimaisRefugo;
	}

	public SimNaoNA getMumificacaoFetal() {
		return mumificacaoFetal;
	}

	public void setMumificacaoFetal(SimNaoNA mumificacaoFetal) {
		this.mumificacaoFetal = mumificacaoFetal;
	}

	public List<SinaisClinicosSindromeHemorragica> getSinaisClinicosEstadoGeral() {
		return sinaisClinicosEstadoGeral;
	}

	public void setSinaisClinicosEstadoGeral(List<SinaisClinicosSindromeHemorragica> sinaisClinicosEstadoGeral) {
		this.sinaisClinicosEstadoGeral = sinaisClinicosEstadoGeral;
	}

	public List<SinaisClinicosSindromeHemorragica> getSinaisClinicosSistemaRespiratorio() {
		return sinaisClinicosSistemaRespiratorio;
	}

	public void setSinaisClinicosSistemaRespiratorio(
			List<SinaisClinicosSindromeHemorragica> sinaisClinicosSistemaRespiratorio) {
		this.sinaisClinicosSistemaRespiratorio = sinaisClinicosSistemaRespiratorio;
	}

	public List<SinaisClinicosSindromeHemorragica> getSinaisClinicosSistemaNervoso() {
		return sinaisClinicosSistemaNervoso;
	}

	public void setSinaisClinicosSistemaNervoso(List<SinaisClinicosSindromeHemorragica> sinaisClinicosSistemaNervoso) {
		this.sinaisClinicosSistemaNervoso = sinaisClinicosSistemaNervoso;
	}

	public List<SinaisClinicosSindromeHemorragica> getSinaisClinicosSistemaDigestorio() {
		return sinaisClinicosSistemaDigestorio;
	}

	public void setSinaisClinicosSistemaDigestorio(
			List<SinaisClinicosSindromeHemorragica> sinaisClinicosSistemaDigestorio) {
		this.sinaisClinicosSistemaDigestorio = sinaisClinicosSistemaDigestorio;
	}

	public List<SinaisClinicosSindromeHemorragica> getSinaisClinicosSistemaReprodutivo() {
		return sinaisClinicosSistemaReprodutivo;
	}

	public void setSinaisClinicosSistemaReprodutivo(
			List<SinaisClinicosSindromeHemorragica> sinaisClinicosSistemaReprodutivo) {
		this.sinaisClinicosSistemaReprodutivo = sinaisClinicosSistemaReprodutivo;
	}

	public List<SinaisClinicosSindromeHemorragica> getSinaisClinicosSistemaTegumentar() {
		return sinaisClinicosSistemaTegumentar;
	}

	public void setSinaisClinicosSistemaTegumentar(
			List<SinaisClinicosSindromeHemorragica> sinaisClinicosSistemaTegumentar) {
		this.sinaisClinicosSistemaTegumentar = sinaisClinicosSistemaTegumentar;
	}

	public List<SinaisClinicosSindromeHemorragica> getSinaisClinicosSistemaLinfatico() {
		return sinaisClinicosSistemaLinfatico;
	}

	public void setSinaisClinicosSistemaLinfatico(List<SinaisClinicosSindromeHemorragica> sinaisClinicosSistemaLinfatico) {
		this.sinaisClinicosSistemaLinfatico = sinaisClinicosSistemaLinfatico;
	}

	public String getInformacoesAdicionais() {
		return informacoesAdicionais;
	}

	public void setInformacoesAdicionais(String informacoesAdicionais) {
		this.informacoesAdicionais = informacoesAdicionais;
	}

	public SimNao getPossuiInformacaoSobreEstabelecimentosProximosComSuideos() {
		return possuiInformacaoSobreEstabelecimentosProximosComSuideos;
	}

	public void setPossuiInformacaoSobreEstabelecimentosProximosComSuideos(
			SimNao possuiInformacaoSobreEstabelecimentosProximosComSuideos) {
		this.possuiInformacaoSobreEstabelecimentosProximosComSuideos = possuiInformacaoSobreEstabelecimentosProximosComSuideos;
	}

	public br.gov.mt.indea.sistemaformin.webservice.entity.Veterinario getVeterinarioResponsavelPeloAtendimento() {
		return veterinarioResponsavelPeloAtendimento;
	}

	public void setVeterinarioResponsavelPeloAtendimento(br.gov.mt.indea.sistemaformin.webservice.entity.Veterinario veterinario) {
		this.veterinarioResponsavelPeloAtendimento = veterinario;
	}

	public List<NovoEstabelecimentoParaInvestigacao> getNovoEstabelecimentoParaInvestigacaos() {
		return novoEstabelecimentoParaInvestigacaos;
	}

	public void setNovoEstabelecimentoParaInvestigacaos(
			List<NovoEstabelecimentoParaInvestigacao> novoEstabelecimentoParaInvestigacaos) {
		this.novoEstabelecimentoParaInvestigacaos = novoEstabelecimentoParaInvestigacaos;
	}

	public br.gov.mt.indea.sistemaformin.webservice.entity.Veterinario getVeterinarioAssistenteDoEstabelecimento() {
		return veterinarioAssistenteDoEstabelecimento;
	}

	public void setVeterinarioAssistenteDoEstabelecimento(
			br.gov.mt.indea.sistemaformin.webservice.entity.Veterinario veterinarioAssistenteDoEstabelecimento) {
		this.veterinarioAssistenteDoEstabelecimento = veterinarioAssistenteDoEstabelecimento;
	}

	public SimNao getEstabelecimentoPossuiAssistenciaVeterinariaPermanente() {
		return estabelecimentoPossuiAssistenciaVeterinariaPermanente;
	}

	public void setEstabelecimentoPossuiAssistenciaVeterinariaPermanente(
			SimNao estabelecimentoPossuiAssistenciaVeterinariaPermanente) {
		this.estabelecimentoPossuiAssistenciaVeterinariaPermanente = estabelecimentoPossuiAssistenciaVeterinariaPermanente;
	}

	public String getEmpresaIntegradoraOuCooperativa() {
		return empresaIntegradoraOuCooperativa;
	}

	public void setEmpresaIntegradoraOuCooperativa(
			String empresaIntegradoraOuCooperativa) {
		this.empresaIntegradoraOuCooperativa = empresaIntegradoraOuCooperativa;
	}

	public String getGranjaOuLocalDeOrigemDosAnimais() {
		return granjaOuLocalDeOrigemDosAnimais;
	}

	public void setGranjaOuLocalDeOrigemDosAnimais(
			String granjaOuLocalDeOrigemDosAnimais) {
		this.granjaOuLocalDeOrigemDosAnimais = granjaOuLocalDeOrigemDosAnimais;
	}

	public Integer getCodigoGranjaOuLocalDeOrigem() {
		return codigoGranjaOuLocalDeOrigem;
	}

	public void setCodigoGranjaOuLocalDeOrigem(Integer codigoGranjaOuLocalDeOrigem) {
		this.codigoGranjaOuLocalDeOrigem = codigoGranjaOuLocalDeOrigem;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FormSH other = (FormSH) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}