package br.gov.mt.indea.sistemaformin.managedbeans;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLBeanName;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;

import br.gov.mt.indea.sistemaformin.entity.FormCOM;
import br.gov.mt.indea.sistemaformin.entity.FormIN;
import br.gov.mt.indea.sistemaformin.entity.FormMormo;
import br.gov.mt.indea.sistemaformin.entity.Municipio;
import br.gov.mt.indea.sistemaformin.entity.UF;
import br.gov.mt.indea.sistemaformin.enums.Dominio.Sexo;
import br.gov.mt.indea.sistemaformin.enums.Dominio.SimNao;
import br.gov.mt.indea.sistemaformin.enums.Dominio.SimNaoSI;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoLaboratorio;
import br.gov.mt.indea.sistemaformin.exception.ApplicationErrorException;
import br.gov.mt.indea.sistemaformin.exception.ApplicationException;
import br.gov.mt.indea.sistemaformin.service.FormCOMService;
import br.gov.mt.indea.sistemaformin.service.FormINService;
import br.gov.mt.indea.sistemaformin.service.FormMormoService;
import br.gov.mt.indea.sistemaformin.service.MunicipioService;
import br.gov.mt.indea.sistemaformin.util.FacesMessageUtil;
import br.gov.mt.indea.sistemaformin.webservice.client.ClientWebServiceVeterinario;
import br.gov.mt.indea.sistemaformin.webservice.entity.Endereco;
import br.gov.mt.indea.sistemaformin.webservice.entity.Propriedade;
import br.gov.mt.indea.sistemaformin.webservice.entity.Veterinario;

@Named
@ViewScoped
@URLBeanName("formMormoManagedBean")
@URLMappings(mappings = {
		@URLMapping(id = "pesquisarFormMormo", pattern = "/formIN/#{formMormoManagedBean.idFormIN}/formMormo/pesquisar", viewId = "/pages/formMormo/lista.jsf"),
		@URLMapping(id = "incluirFormMormo", pattern = "/formIN/#{formMormoManagedBean.idFormIN}/formMormo/incluir", viewId = "/pages/formMormo/novo.jsf"),
		@URLMapping(id = "alterarFormMormo", pattern = "/formIN/#{formMormoManagedBean.idFormIN}/formMormo/alterar/#{formMormoManagedBean.id}", viewId = "/pages/formMormo/novo.jsf"),
		@URLMapping(id = "impressaoFormMormo", pattern = "/formIN/#{formMormoManagedBean.idFormIN}/formMormo/impressao/#{formMormoManagedBean.id}", viewId = "/pages/impressao.jsf")})
public class FormMormoManagedBean implements Serializable {

	private static final long serialVersionUID = 1760023349204758559L;

	private Long id;
	
	private Long idFormIN;
	
	private FormIN formIN;
	
	@Inject
	private FormINService formINService;
	
	@Inject
	private FormMormoService formMormoService;
	
	@Inject
	private FormCOMService formCOMService;
	
	@Inject
	private MunicipioService municipioService;
	
	@Inject
	private FormMormo formMormo;
	
	private String cpfVeterinario;

	private String crmvVeterinario;

	private List<Veterinario> listaVeterinarios;
	
	@Inject
	private ClientWebServiceVeterinario clientWebServiceVeterinario;
	
	private SimNao propriedadeOndeOAnimalSeEncontraEAMesmaDoFormIN = SimNao.SIM;
	
	private UF uf;
	
	public List<Municipio> getListaMunicipios(){
		if (this.uf == null)
			return null;
		
		return municipioService.findAllByUF(this.uf);
	}
	
	@PostConstruct
	private void init(){
		
	}
	
	private void carregarFormIN(){
		this.formIN = formINService.findByIdFetchPropriedade(this.idFormIN);
	}
	
	private void limpar() {
		this.formMormo = new FormMormo();
	}
	
	@URLAction(mappingId = "incluirFormMormo", onPostback = false)
	public void novo(){
		this.limpar();
		this.carregarFormIN();
		
		this.formMormo.setFormIN(formIN);	
		this.formMormo.setPropriedade(this.formMormo.getFormIN().getPropriedade());
	}
	
	@URLAction(mappingId = "alterarFormMormo", onPostback = false)
	public void editar(){
		this.formMormo = formMormoService.findByIdFetchAll(this.getId());
		
//		if (this.formMormo.getPropriedade().getCodigoPropriedade() != null
//				&& this.formMormo.getPropriedade().getCodigoPropriedade().equals(this.formMormo.getFormIN().getPropriedade().getCodigoPropriedade())){
//			this.propriedadeOndeOAnimalSeEncontraEAMesmaDoFormIN = SimNao.SIM;
//		}else{
//			this.propriedadeOndeOAnimalSeEncontraEAMesmaDoFormIN = SimNao.NAO;
//			this.uf = this.formMormo.getPropriedade().getMunicipio().getUf();
//		}
	}
	
	public String adicionar() throws IOException{
		if (this.formMormo.getVeterinario() == null){
			FacesContext context = FacesContext.getCurrentInstance();
			
			context.addMessage("cadastro:fieldVeterinario:campoVeterinario", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Veterin�rio: valor � obrigat�rio.", "Veterin�rio: valor � obrigat�rio."));
			return "";
		}
		
		if (formMormo.getId() != null){
			this.formMormoService.saveOrUpdate(formMormo);
			FacesMessageUtil.addInfoContextFacesMessage("Form Mormo atualizado", "");
		}else{
			this.formMormo.setDataCadastro(Calendar.getInstance());
			this.formMormoService.saveOrUpdate(formMormo);
			FacesMessageUtil.addInfoContextFacesMessage("Form Mormo adicionado", "");
		}
		
		limpar();
	    return "pretty:visaoGeralFormIN";
	}
	
	@Inject
	private VisualizadorImpressaoManagedBean visualizadorImpressaoManagedBean;
	
	@URLAction(mappingId = "impressaoFormMormo", onPostback = false)
	public void imprimir() throws ApplicationErrorException{
		this.formMormo = formMormoService.findByIdFetchAll(this.getId());
		
		visualizadorImpressaoManagedBean.exibirFormMormo(formMormo);
	}
	
	public void remover(FormMormo formMormo){
		this.formMormoService.delete(formMormo);
		FacesMessageUtil.addInfoContextFacesMessage("Form Mormo exclu�do com sucesso", "");
	}
	
	public List<FormCOM> getListaFormCOM(){
		return formCOMService.findAllBy(this.formMormo.getFormIN());
	}
	
	public void abrirTelaDeBuscaDeVeterinario(){
		this.cpfVeterinario = null;
		this.crmvVeterinario = null;
		this.listaVeterinarios = null;
	}
	
	public void buscarVeterinarioPorCpf(){
		this.listaVeterinarios = null;
		
		Veterinario v;
		try {
			v = clientWebServiceVeterinario.getVeterinarioByCpf(cpfVeterinario);
			
			if (v != null){
				this.listaVeterinarios = new ArrayList<Veterinario>();
				this.listaVeterinarios.add(v);
			}
		} catch (ApplicationException e) {
			FacesMessageUtil.addErrorContextFacesMessage("Erro", e.getLocalizedMessage());
			return;
		}
		
		if (this.listaVeterinarios == null || this.listaVeterinarios.isEmpty())
			FacesMessageUtil.addWarnContextFacesMessage("Nenhum veterin�rio foi encontrado", "");
	}
	
	public void buscarVeterinarioPorCrmv(){
		this.listaVeterinarios = null;
		
		Veterinario v;
		try {
			v = clientWebServiceVeterinario.getVeterinarioByCRMV(crmvVeterinario);
			
			if (v != null){
				this.listaVeterinarios = new ArrayList<Veterinario>();
				this.listaVeterinarios.add(v);
			}
		} catch (ApplicationException e) {
			FacesMessageUtil.addErrorContextFacesMessage("Erro", e.getLocalizedMessage());
			return;
		}
		
		if (this.listaVeterinarios == null || this.listaVeterinarios.isEmpty())
			FacesMessageUtil.addWarnContextFacesMessage("Nenhum veterin�rio foi encontrado", "");
	}
	
	public void selecionarVeterinario(Veterinario veterinario){
		this.formMormo.setVeterinario(veterinario);
		FacesMessageUtil.addInfoContextFacesMessage("Veterinario " + this.formMormo.getVeterinario().getNome() + " selecionado", "");
	}
	
	
	public boolean isTipoLaboratorioCredenciado(){
		boolean resultado = false;
		if (this.formMormo.getTipoLaboratorio() == null)
			return resultado;
		else
			resultado = this.formMormo.getTipoLaboratorio().equals(TipoLaboratorio.CREDENCIADO);
		
		if (!resultado)
			this.formMormo.setFinalidadeDoTesteDeLaboratorioCredenciado(null);
		
		return resultado;
	}
	
	public boolean isTipoLaboratorioCredenciadoPublico(){
		boolean resultado = false;
		if (this.formMormo.getTipoLaboratorio() == null)
			return resultado;
		else
			resultado = this.formMormo.getTipoLaboratorio().equals(TipoLaboratorio.CREDENCIADO_PUBLICO);
		
		if (!resultado)
			this.formMormo.setFinalidadeDoTesteDeLaboratorioCredenciadoPublico(null);
		
		return resultado;
	}
	
	public boolean isTipoLaboratorioOficial(){
		boolean resultado = false;
		if (this.formMormo.getTipoLaboratorio() == null)
			return resultado;
		else
			resultado = this.formMormo.getTipoLaboratorio().equals(TipoLaboratorio.OFICIAL);
		
		if (!resultado)
			this.formMormo.setFinalidadeDoTesteDeLaboratorioOficial(null);
		
		return resultado;
	}
	
	public boolean isAnimalFemea(){
		boolean resultado = false;
		if (this.formMormo.getSexo() == null)
			return resultado;
		else
			resultado = this.formMormo.getSexo().equals(Sexo.FEMININO);
		
		if (!resultado){
			this.formMormo.setPrenhe(null);
			this.formMormo.setTercoGestacao(null);
		}
		
		return resultado;
	}
	
	public boolean isAnimalFemeaPrenhe(){
		boolean resultado = false;
		if (this.formMormo.getPrenhe() == null)
			return resultado;
		else
			resultado = this.formMormo.getPrenhe().equals(SimNaoSI.SIM);
		
		if (!resultado){
			this.formMormo.setTercoGestacao(null);
		}
		
		return resultado;
	}
	
	public void alterarPropriedade(){
		if (this.propriedadeOndeOAnimalSeEncontraEAMesmaDoFormIN.equals(SimNao.SIM))
			this.formMormo.setPropriedade(this.formMormo.getFormIN().getPropriedade());
		else{
			this.formMormo.setPropriedade(new Propriedade());
			this.formMormo.getPropriedade().setEndereco(new Endereco());
		}
		
	}
	
	public FormMormo getFormMormo() {
		return formMormo;
	}

	public void setFormMormo(FormMormo formMormo) {
		this.formMormo = formMormo;
	}

	public String getCpfVeterinario() {
		return cpfVeterinario;
	}

	public void setCpfVeterinario(String cpfVeterinario) {
		this.cpfVeterinario = cpfVeterinario;
	}

	public String getCrmvVeterinario() {
		return crmvVeterinario;
	}

	public void setCrmvVeterinario(String crmvVeterinario) {
		this.crmvVeterinario = crmvVeterinario;
	}

	public List<Veterinario> getListaVeterinarios() {
		return listaVeterinarios;
	}

	public void setListaVeterinarios(List<Veterinario> listaVeterinarios) {
		this.listaVeterinarios = listaVeterinarios;
	}

	public SimNao getPropriedadeOndeOAnimalSeEncontraEAMesmaDoFormIN() {
		return propriedadeOndeOAnimalSeEncontraEAMesmaDoFormIN;
	}

	public void setPropriedadeOndeOAnimalSeEncontraEAMesmaDoFormIN(
			SimNao propriedadeOndeOAnimalSeEncontraEAMesmaDoFormIN) {
		this.propriedadeOndeOAnimalSeEncontraEAMesmaDoFormIN = propriedadeOndeOAnimalSeEncontraEAMesmaDoFormIN;
	}

	public boolean isPropriedadeOndeOAnimalSeEncontraEAMesmaDoFormIN_SIM(){
		return propriedadeOndeOAnimalSeEncontraEAMesmaDoFormIN.equals(SimNao.SIM);
	}
	
	public UF getUf() {
		return uf;
	}

	public void setUf(UF uf) {
		this.uf = uf;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdFormIN() {
		return idFormIN;
	}

	public void setIdFormIN(Long idFormIN) {
		this.idFormIN = idFormIN;
	}

	public FormIN getFormIN() {
		return formIN;
	}

	public void setFormIN(FormIN formIN) {
		this.formIN = formIN;
	}

}
