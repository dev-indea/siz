package br.gov.mt.indea.sistemaformin.managedbeans;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.constraints.Past;

import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLBeanName;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;

import br.gov.mt.indea.sistemaformin.entity.FormCOM;
import br.gov.mt.indea.sistemaformin.entity.FormIN;
import br.gov.mt.indea.sistemaformin.entity.FormMaleina;
import br.gov.mt.indea.sistemaformin.entity.TesteDeFixacaoDeComplemento;
import br.gov.mt.indea.sistemaformin.exception.ApplicationErrorException;
import br.gov.mt.indea.sistemaformin.exception.ApplicationException;
import br.gov.mt.indea.sistemaformin.service.FormCOMService;
import br.gov.mt.indea.sistemaformin.service.FormINService;
import br.gov.mt.indea.sistemaformin.service.FormMaleinaService;
import br.gov.mt.indea.sistemaformin.util.FacesMessageUtil;
import br.gov.mt.indea.sistemaformin.webservice.client.ClientWebServiceVeterinario;
import br.gov.mt.indea.sistemaformin.webservice.entity.Veterinario;

@Named
@ViewScoped
@URLBeanName("formMaleinaManagedBean")
@URLMappings(mappings = {
		@URLMapping(id = "pesquisarFormMaleina", pattern = "/formIN/#{formMaleinaManagedBean.idFormIN}/formMaleina/pesquisar", viewId = "/pages/formMaleina/lista.jsf"),
		@URLMapping(id = "incluirFormMaleina", pattern = "/formIN/#{formMaleinaManagedBean.idFormIN}/formMaleina/incluir", viewId = "/pages/formMaleina/novo.jsf"),
		@URLMapping(id = "alterarFormMaleina", pattern = "/formIN/#{formMaleinaManagedBean.idFormIN}/formMaleina/alterar/#{formMaleinaManagedBean.id}", viewId = "/pages/formMaleina/novo.jsf"),
		@URLMapping(id = "impressaoFormMaleina", pattern = "/formIN/#{formMaleinaManagedBean.idFormIN}/formMaleina/impressao/#{formMaleinaManagedBean.id}", viewId = "/pages/formMaleina/novo.jsf")})
public class FormMaleinaManagedBean implements Serializable {

	private static final long serialVersionUID = 1760023349204758559L;

	private Long id;
	
	private Long idFormIN;
	
	private FormIN formIN;
	
	@Inject
	private FormINService formINService;
	
	@Inject
	private FormMaleinaService formMaleinaService;
	
	@Inject
	private FormCOMService formCOMService;
	
	@Inject
	private FormMaleina formMaleina;
	
	private TesteDeFixacaoDeComplemento testeDeFixacaoDeComplemento = new TesteDeFixacaoDeComplemento();
	
	private boolean editandoTeste = false;
	
	private boolean visualizandoTeste = false;
	
	@Past(message="Data precisa ser menor ou igual a data atual")
	private Date dataDeAplicacaoDaMaleina;
	
	@Past(message="Data precisa ser menor ou igual a data atual")
	private Date dataDeLeitura;
	
	private Date dataValidadeMaleina;
	
	private Date dataValidadeExame;
	
	private Date dataResultado;
	
	private String cpfVeterinario;

	private String crmvVeterinario;

	private List<Veterinario> listaVeterinarios;
	
	@Inject
	private ClientWebServiceVeterinario clientWebServiceVeterinario;
	
	@PostConstruct
	private void init(){
		
	}
	
	private void carregarFormIN(){
		this.formIN = formINService.findByIdFetchPropriedade(this.idFormIN);
	}
	
	private void limpar() {
		this.formMaleina = new FormMaleina();
		this.dataDeAplicacaoDaMaleina = null;
		this.dataDeLeitura = null;
		this.dataValidadeExame = null;
		this.dataValidadeMaleina = null;
	}
	
	@URLAction(mappingId = "incluirFormMaleina", onPostback = false)
	public void novo(){
		this.limpar();
		this.carregarFormIN();
		
		this.formMaleina.setFormIN(formIN);
	}
	
//	public String novo(FormCOM formCOM){
//		this.iniciarConversacao();
//		this.formMaleina.setFormCOM(formCOM);
//		this.formMaleina.setFormIN(formCOM.getFormIN());
//
//		return "/pages/formMaleina/novo.xhtml";
//	}
	
	@URLAction(mappingId = "alterarFormMaleina", onPostback = false)
	public void editar(){
		this.formMaleina = formMaleinaService.findByIdFetchAll(this.getId());
		
		if (this.formMaleina.getDataDeAplicacaoDaMaleina() != null)
			this.dataDeAplicacaoDaMaleina = this.formMaleina.getDataDeAplicacaoDaMaleina().getTime();
		if (this.formMaleina.getDataDeLeitura() != null)
			this.dataDeLeitura = this.formMaleina.getDataDeLeitura().getTime();
		if (this.formMaleina.getDataValidadeExame() != null)
			this.dataValidadeExame = this.formMaleina.getDataValidadeExame().getTime();
		if (this.formMaleina.getDataValidadeMaleina() != null)
			this.dataValidadeMaleina = this.formMaleina.getDataValidadeMaleina().getTime();
		
	}
	
	public String adicionar() throws IOException{
		
		if (this.dataDeAplicacaoDaMaleina != null){
			if (this.formMaleina.getDataDeAplicacaoDaMaleina() == null)
				this.formMaleina.setDataDeAplicacaoDaMaleina(Calendar.getInstance());
			this.formMaleina.getDataDeAplicacaoDaMaleina().setTime(dataDeAplicacaoDaMaleina);
		}else
			this.formMaleina.setDataDeAplicacaoDaMaleina(null);
			
		if (this.dataDeLeitura != null){
			if (this.formMaleina.getDataDeLeitura() == null)
				this.formMaleina.setDataDeLeitura(Calendar.getInstance());
			this.formMaleina.getDataDeLeitura().setTime(dataDeLeitura);
		}else
			this.formMaleina.setDataDeLeitura(null);
			
		if (this.dataValidadeExame != null){
			if (this.formMaleina.getDataValidadeExame() == null)
				this.formMaleina.setDataValidadeExame(Calendar.getInstance());
			this.formMaleina.getDataValidadeExame().setTime(dataValidadeExame);
		}else
			this.formMaleina.setDataValidadeExame(null);
			
		if (this.dataValidadeMaleina != null){
			if (this.formMaleina.getDataValidadeMaleina() == null)
				this.formMaleina.setDataValidadeMaleina(Calendar.getInstance());
			this.formMaleina.getDataValidadeMaleina().setTime(dataValidadeMaleina);
		}else
			this.formMaleina.setDataValidadeMaleina(null);
		
		if (this.formMaleina.getVeterinario() == null){
			FacesContext context = FacesContext.getCurrentInstance();
			
			context.addMessage("cadastro:fieldVeterinario:campoVeterinario", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Veterin�rio: valor � obrigat�rio.", "Veterin�rio: valor � obrigat�rio."));
			return "";
		}
		
		if (formMaleina.getId() != null){
			this.formMaleinaService.saveOrUpdate(formMaleina);
			FacesMessageUtil.addInfoContextFacesMessage("Form Maleina atualizado", "");
		}else{
			this.formMaleina.setDataCadastro(Calendar.getInstance());
			this.formMaleinaService.saveOrUpdate(formMaleina);
			FacesMessageUtil.addInfoContextFacesMessage("Form Maleina adicionado", "");
		}
		
		limpar();
	    return "pretty:visaoGeralFormIN";
	}
	
	@Inject
	private VisualizadorImpressaoManagedBean visualizadorImpressaoManagedBean;
	
	@URLAction(mappingId = "impressaoFormMaleina", onPostback = false)
	public void imprimir() throws ApplicationErrorException{
		this.formMaleina = formMaleinaService.findByIdFetchAll(this.getId());
		
		visualizadorImpressaoManagedBean.exibirFormMaleina(formMaleina);
	}
	
	public void remover(FormMaleina formMaleina){
		this.formMaleinaService.delete(formMaleina);
		FacesMessageUtil.addInfoContextFacesMessage("Form Maleina exclu�do com sucesso", "");
	}
	
	public List<FormCOM> getListaFormCOM(){
		return formCOMService.findAllBy(this.formMaleina.getFormIN());
	}
	
	public void adicionarTesteDeFixacaoDeComplemento(){
		this.testeDeFixacaoDeComplemento.setFormMaleina(formMaleina);
		
		if (this.dataResultado != null){
			if (this.testeDeFixacaoDeComplemento.getDataResultado() == null)
				this.testeDeFixacaoDeComplemento.setDataResultado(Calendar.getInstance());
			this.testeDeFixacaoDeComplemento.getDataResultado().setTime(dataResultado);
		}else
			this.testeDeFixacaoDeComplemento.setDataResultado(null);
		this.dataResultado = null;
		
		if (!editandoTeste)
			this.formMaleina.getTestesDeFixacaoDeComplementos().add(this.testeDeFixacaoDeComplemento);
		else
			editandoTeste = false;
		
		this.testeDeFixacaoDeComplemento = new TesteDeFixacaoDeComplemento();
		this.dataResultado = null;
		
		FacesMessageUtil.addInfoContextFacesMessage("Resultado inclu�do com sucesso", "");
	}
	
	public void novoTeste(){
		this.testeDeFixacaoDeComplemento = new TesteDeFixacaoDeComplemento();
		this.editandoTeste = false;
		this.visualizandoTeste = false;
	}
	
	public void removerTesteDeFixacaoDeComplemento(TesteDeFixacaoDeComplemento testeDeFixacaoDeComplemento){
		this.formMaleina.getTestesDeFixacaoDeComplementos().remove(testeDeFixacaoDeComplemento);
	}
	
	public void editarTeste(TesteDeFixacaoDeComplemento testeDeFixacaoDeComplemento){
		this.editandoTeste = true;
		this.visualizandoTeste = false;
		this.testeDeFixacaoDeComplemento = testeDeFixacaoDeComplemento;
		
		if (this.testeDeFixacaoDeComplemento.getDataResultado() != null)
			this.dataResultado = this.testeDeFixacaoDeComplemento.getDataResultado().getTime();
	}
	
	public void visualizarTeste(TesteDeFixacaoDeComplemento testeDeFixacaoDeComplemento){
		this.visualizandoTeste = true;
		this.editandoTeste = false;
		this.testeDeFixacaoDeComplemento = testeDeFixacaoDeComplemento;
		
		if (this.testeDeFixacaoDeComplemento.getDataResultado() != null)
			this.dataResultado = this.testeDeFixacaoDeComplemento.getDataResultado().getTime();
	}
	
	public void abrirTelaDeBuscaDeVeterinario(){
		this.cpfVeterinario = null;
		this.crmvVeterinario = null;
		this.listaVeterinarios = null;
	}
	
	public void buscarVeterinarioPorCpf(){
		this.listaVeterinarios = null;
		
		Veterinario v;
		try {
			v = clientWebServiceVeterinario.getVeterinarioByCpf(cpfVeterinario);
			
			if (v != null){
				this.listaVeterinarios = new ArrayList<Veterinario>();
				this.listaVeterinarios.add(v);
			}
		} catch (ApplicationException e) {
			FacesMessageUtil.addErrorContextFacesMessage("Erro", e.getLocalizedMessage());
			return;
		}
		
		if (this.listaVeterinarios == null || this.listaVeterinarios.isEmpty())
			FacesMessageUtil.addWarnContextFacesMessage("Nenhum veterin�rio foi encontrado", "");
	}
	
	public void buscarVeterinarioPorCrmv(){
		this.listaVeterinarios = null;
		
		Veterinario v;
		try {
			v = clientWebServiceVeterinario.getVeterinarioByCRMV(crmvVeterinario);
			
			if (v != null){
				this.listaVeterinarios = new ArrayList<Veterinario>();
				this.listaVeterinarios.add(v);
			}
		} catch (ApplicationException e) {
			FacesMessageUtil.addErrorContextFacesMessage("Erro", e.getLocalizedMessage());
			return;
		}
		
		if (this.listaVeterinarios == null || this.listaVeterinarios.isEmpty())
			FacesMessageUtil.addWarnContextFacesMessage("Nenhum veterin�rio foi encontrado", "");
	}
	
	public void selecionarVeterinario(Veterinario veterinario){
		this.formMaleina.setVeterinario(veterinario);
		FacesMessageUtil.addInfoContextFacesMessage("Veterinario " + this.formMaleina.getVeterinario().getNome() + " selecionado", "");
	}
	
	public FormMaleina getFormMaleina() {
		return formMaleina;
	}

	public void setFormMaleina(FormMaleina formMaleina) {
		this.formMaleina = formMaleina;
	}

	public TesteDeFixacaoDeComplemento getTesteDeFixacaoDeComplemento() {
		return testeDeFixacaoDeComplemento;
	}

	public void setTesteDeFixacaoDeComplemento(
			TesteDeFixacaoDeComplemento testeDeFixacaoDeComplemento) {
		this.testeDeFixacaoDeComplemento = testeDeFixacaoDeComplemento;
	}

	public boolean isVisualizandoTeste() {
		return visualizandoTeste;
	}

	public Date getDataDeAplicacaoDaMaleina() {
		return dataDeAplicacaoDaMaleina;
	}

	public void setDataDeAplicacaoDaMaleina(Date dataDeAplicacaoDaMaleina) {
		this.dataDeAplicacaoDaMaleina = dataDeAplicacaoDaMaleina;
	}

	public Date getDataDeLeitura() {
		return dataDeLeitura;
	}

	public void setDataDeLeitura(Date dataDeLeitura) {
		this.dataDeLeitura = dataDeLeitura;
	}

	public Date getDataValidadeMaleina() {
		return dataValidadeMaleina;
	}

	public void setDataValidadeMaleina(Date dataValidadeMaleina) {
		this.dataValidadeMaleina = dataValidadeMaleina;
	}

	public Date getDataValidadeExame() {
		return dataValidadeExame;
	}

	public void setDataValidadeExame(Date dataValidadeExame) {
		this.dataValidadeExame = dataValidadeExame;
	}

	public Date getDataResultado() {
		return dataResultado;
	}

	public void setDataResultado(Date dataResultado) {
		this.dataResultado = dataResultado;
	}

	public String getCpfVeterinario() {
		return cpfVeterinario;
	}

	public void setCpfVeterinario(String cpfVeterinario) {
		this.cpfVeterinario = cpfVeterinario;
	}

	public String getCrmvVeterinario() {
		return crmvVeterinario;
	}

	public void setCrmvVeterinario(String crmvVeterinario) {
		this.crmvVeterinario = crmvVeterinario;
	}

	public List<Veterinario> getListaVeterinarios() {
		return listaVeterinarios;
	}

	public void setListaVeterinarios(List<Veterinario> listaVeterinarios) {
		this.listaVeterinarios = listaVeterinarios;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdFormIN() {
		return idFormIN;
	}

	public void setIdFormIN(Long idFormIN) {
		this.idFormIN = idFormIN;
	}

	public FormIN getFormIN() {
		return formIN;
	}

	public void setFormIN(FormIN formIN) {
		this.formIN = formIN;
	}

}
