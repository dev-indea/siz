package br.gov.mt.indea.sistemaformin.managedbeans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLBeanName;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;

import br.gov.mt.indea.sistemaformin.entity.Municipio;
import br.gov.mt.indea.sistemaformin.entity.RegistroEntradaLASA;
import br.gov.mt.indea.sistemaformin.entity.RegistroSaidaLASA;
import br.gov.mt.indea.sistemaformin.entity.TipoExame;
import br.gov.mt.indea.sistemaformin.entity.UF;
import br.gov.mt.indea.sistemaformin.entity.dto.RegistroEntradaLASADTO;
import br.gov.mt.indea.sistemaformin.exception.ApplicationException;
import br.gov.mt.indea.sistemaformin.service.LazyObjectDataModel;
import br.gov.mt.indea.sistemaformin.service.MunicipioService;
import br.gov.mt.indea.sistemaformin.service.RegistroEntradaLASAService;
import br.gov.mt.indea.sistemaformin.service.TipoExameService;
import br.gov.mt.indea.sistemaformin.service.UFService;
import br.gov.mt.indea.sistemaformin.util.FacesMessageUtil;
import br.gov.mt.indea.sistemaformin.webservice.client.ClientWebServiceVeterinario;
import br.gov.mt.indea.sistemaformin.webservice.entity.Veterinario;

@Named
@ViewScoped
@URLBeanName("registroEntradaLASAManagedBean")
@URLMappings(mappings = {
		@URLMapping(id="pesquisarRegistroEntradaLASA", pattern="/registroEntradaLASA/pesquisar", viewId="/pages/registroEntradaLASA/lista.jsf"),
		@URLMapping(id="novoRegistroEntradaLASA", pattern="/registroEntradaLASA/novo", viewId="/pages/registroEntradaLASA/entrada.jsf"),
		@URLMapping(id="novoRegistroSaidaLASA", pattern="/registroSaidaLASA/entrada/#{registroEntradaLASAManagedBean.id}/saida", viewId="/pages/registroEntradaLASA/entrada.jsf")})
public class RegistroEntradaLASAManagedBean implements Serializable{

	private static final long serialVersionUID = 2046061781097536140L;

	private LazyObjectDataModel<RegistroEntradaLASA> listaRegistroEntradaLASA;
	
	private Long id;
	
	private boolean incluindoSaida = false;

	private RegistroEntradaLASA registroEntradaLASA;
	
	@Inject
	private RegistroEntradaLASADTO registroEntradaLASADTO;
	
	private Date dataEntrada;
	
	private Date dataSaida;
	
	private UF uf;
	
	private String cpfVeterinario;

	private String crmvVeterinario;

	private List<Veterinario> listaVeterinarios;
	
	@Inject
	private ClientWebServiceVeterinario clientWebServiceVeterinario;
	
	private List<TipoExame> listaTipoExame;
	
	@Inject
	private RegistroEntradaLASAService registroEntradaLASAService;
	
	@Inject
	private UFService ufService;
	
	@Inject
	private MunicipioService municipioService;
	
	@Inject
	private TipoExameService tipoExameService;  
	
	@URLAction(mappingId="pesquisarRegistroEntradaLASA", onPostback = false)
	public void busca(){
		limpar();
	}
	
	public void buscarRegistroEntradaLASA(){
		listaRegistroEntradaLASA = new LazyObjectDataModel<RegistroEntradaLASA>(this.registroEntradaLASAService, registroEntradaLASADTO);
	}
	
	@URLAction(mappingId="novoRegistroEntradaLASA", onPostback = false)
	public void novaEntrada(){
		registroEntradaLASA = new RegistroEntradaLASA();
		this.loadListaTipoExame();
		
		uf = ufService.findByIdFetchAll(UF.MATO_GROSSO.getId());
		
		this.incluindoSaida = false;
	}
	
	@URLAction(mappingId="novoRegistroSaidaLASA", onPostback = false)
	public void novaSaida(){
		registroEntradaLASA = this.registroEntradaLASAService.findByIdFetchAll(this.id);
		this.loadListaTipoExame();
		
		uf = ufService.findByIdFetchAll(UF.MATO_GROSSO.getId());
		this.dataEntrada = this.registroEntradaLASA.getDataEntrada().getTime();
		
		if (this.registroEntradaLASA.getDataEntrada() != null)
			this.dataEntrada = this.registroEntradaLASA.getDataEntrada().getTime();
		
		this.registroEntradaLASA.setRegistroSaidaLASA(new RegistroSaidaLASA(this.registroEntradaLASA));
				
		if (this.registroEntradaLASA.getRegistroSaidaLASA().getDataSaida() != null)
			this.dataSaida = this.registroEntradaLASA.getRegistroSaidaLASA().getDataSaida().getTime();
		
		this.incluindoSaida = true;
	}
	
	private void limpar(){
		this.registroEntradaLASA = new RegistroEntradaLASA();
	}
	
	public String adicionar(){
		
		if (this.dataEntrada != null){
			if (registroEntradaLASA.getDataEntrada() == null)
				this.registroEntradaLASA.setDataEntrada(Calendar.getInstance());
			this.registroEntradaLASA.getDataEntrada().setTime(this.dataEntrada);
		} else
			this.registroEntradaLASA.setDataEntrada(null);
		
		if (this.isIncluindoSaida())
			if (this.dataSaida != null){
				if (registroEntradaLASA.getRegistroSaidaLASA().getDataSaida() == null)
					this.registroEntradaLASA.getRegistroSaidaLASA().setDataSaida(Calendar.getInstance());
				this.registroEntradaLASA.getRegistroSaidaLASA().getDataSaida().setTime(this.dataSaida);
			} else
				this.registroEntradaLASA.getRegistroSaidaLASA().setDataSaida(null);
		
		if (registroEntradaLASA.getId() != null){
			this.registroEntradaLASAService.saveOrUpdate(registroEntradaLASA);
			FacesMessageUtil.addInfoContextFacesMessage("Registro de entrada atualizado", "");
		}else{
			this.registroEntradaLASA.setDataCadastro(Calendar.getInstance());
			this.registroEntradaLASAService.saveOrUpdate(registroEntradaLASA);
			FacesMessageUtil.addInfoContextFacesMessage("Registro de entrada adicionado", "");
		}
		
		limpar();
		
		return "pretty:pesquisarRegistroEntradaLASA";
	}
	
	public List<Municipio> getListaMunicipios(){
		if (this.uf == null)
			return null;
		
		return municipioService.findAllByUF(this.uf);
	}
	
	public boolean isIncluindoSaida(){
		return this.incluindoSaida;
	}
	
	private void loadListaTipoExame(){
		this.listaTipoExame = tipoExameService.findAll();
	}
	
	public void abrirTelaDeBuscaDeVeterinario(){
		this.cpfVeterinario = null;
		this.crmvVeterinario = null;
		this.listaVeterinarios = null;
	}
	
	public void buscarVeterinarioPorCpf(){
		this.listaVeterinarios = null;
		
		Veterinario v;
		try {
			v = clientWebServiceVeterinario.getVeterinarioByCpf(cpfVeterinario);
			
			if (v != null){
				this.listaVeterinarios = new ArrayList<Veterinario>();
				this.listaVeterinarios.add(v);
			}
		} catch (ApplicationException e) {
			FacesMessageUtil.addErrorContextFacesMessage("Erro", e.getLocalizedMessage());
			return;
		}
		
		if (this.listaVeterinarios == null || this.listaVeterinarios.isEmpty())
			FacesMessageUtil.addWarnContextFacesMessage("Nenhum veterinário foi encontrado", "");
	}
	
	public void buscarVeterinarioPorCrmv(){
		this.listaVeterinarios = null;
		
		Veterinario v;
		try {
			v = clientWebServiceVeterinario.getVeterinarioByCRMV(crmvVeterinario);
			
			if (v != null){
				this.listaVeterinarios = new ArrayList<Veterinario>();
				this.listaVeterinarios.add(v);
			}
		} catch (ApplicationException e) {
			FacesMessageUtil.addErrorContextFacesMessage("Erro", e.getLocalizedMessage());
			return;
		}
		
		if (this.listaVeterinarios == null || this.listaVeterinarios.isEmpty())
			FacesMessageUtil.addWarnContextFacesMessage("Nenhum veterinário foi encontrado", "");
	}
	
	public void selecionarVeterinario(Veterinario veterinario){
		this.registroEntradaLASA.setVeterinario(veterinario);
		FacesMessageUtil.addInfoContextFacesMessage("Veterinario " + this.registroEntradaLASA.getVeterinario().getNome() + " selecionado", "");
	}
	
	public LazyObjectDataModel<RegistroEntradaLASA> getListaRegistroEntradaLASA() {
		return listaRegistroEntradaLASA;
	}

	public RegistroEntradaLASA getRegistroEntradaLASA() {
		return registroEntradaLASA;
	}

	public void setRegistroEntradaLASA(RegistroEntradaLASA registroEntradaLASA) {
		this.registroEntradaLASA = registroEntradaLASA;
	}

	public Date getDataEntrada() {
		return dataEntrada;
	}

	public void setDataEntrada(Date dataEntrada) {
		this.dataEntrada = dataEntrada;
	}

	public UF getUf() {
		return uf;
	}

	public void setUf(UF uf) {
		this.uf = uf;
	}

	public List<TipoExame> getListaTipoExame() {
		return listaTipoExame;
	}

	public void setListaTipoExame(List<TipoExame> listaTipoExame) {
		this.listaTipoExame = listaTipoExame;
	}

	public void setListaRegistroEntradaLASA(LazyObjectDataModel<RegistroEntradaLASA> listaRegistroEntradaLASA) {
		this.listaRegistroEntradaLASA = listaRegistroEntradaLASA;
	}

	public String getCpfVeterinario() {
		return cpfVeterinario;
	}

	public void setCpfVeterinario(String cpfVeterinario) {
		this.cpfVeterinario = cpfVeterinario;
	}

	public String getCrmvVeterinario() {
		return crmvVeterinario;
	}

	public void setCrmvVeterinario(String crmvVeterinario) {
		this.crmvVeterinario = crmvVeterinario;
	}

	public List<Veterinario> getListaVeterinarios() {
		return listaVeterinarios;
	}

	public void setListaVeterinarios(List<Veterinario> listaVeterinarios) {
		this.listaVeterinarios = listaVeterinarios;
	}

	public RegistroEntradaLASADTO getRegistroEntradaLASADTO() {
		return registroEntradaLASADTO;
	}

	public void setRegistroEntradaLASADTO(RegistroEntradaLASADTO registroEntradaLASADTO) {
		this.registroEntradaLASADTO = registroEntradaLASADTO;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDataSaida() {
		return dataSaida;
	}

	public void setDataSaida(Date dataSaida) {
		this.dataSaida = dataSaida;
	}

}
