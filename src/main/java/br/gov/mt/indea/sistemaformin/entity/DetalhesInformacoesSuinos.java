package br.gov.mt.indea.sistemaformin.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.envers.Audited;

@Audited
@Entity
@DiscriminatorValue("suinos")
public class DetalhesInformacoesSuinos extends AbstractDetalhesInformacoesDeAnimais implements Cloneable{
	
	private static final long serialVersionUID = 1355044082455412390L;

	@ManyToOne
	@JoinColumn(name="id_informacoes_suinos")
	private InformacoesSuinos informacoesSuinos;

	public InformacoesSuinos getInformacoesSuinos() {
		return informacoesSuinos;
	}

	public void setInformacoesSuinos(InformacoesSuinos informacoesSuinos) {
		this.informacoesSuinos = informacoesSuinos;
	}
	
	protected Object clone(InformacoesSuinos informacoesSuinos) throws CloneNotSupportedException{
		DetalhesInformacoesSuinos cloned = (DetalhesInformacoesSuinos) super.clone();
		
		cloned.setId(null);
		cloned.setInformacoesSuinos(informacoesSuinos);
		
		return cloned;
	}

}