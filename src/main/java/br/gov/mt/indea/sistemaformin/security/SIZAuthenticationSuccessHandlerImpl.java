package br.gov.mt.indea.sistemaformin.security;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;

import br.gov.mt.indea.sistemaformin.entity.Usuario;
import br.gov.mt.indea.sistemaformin.service.UsuarioService;
import br.gov.mt.indea.sistemaformin.util.CDIServiceLocator;

public class SIZAuthenticationSuccessHandlerImpl extends SavedRequestAwareAuthenticationSuccessHandler{
	
	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
			throws IOException, ServletException {
		
		UsuarioService usuarioService = CDIServiceLocator.getBean(UsuarioService.class);
		
		Usuario usuario = ((UserSecurity) authentication.getPrincipal()).getUsuario();
		usuario.setUltimoLogin(new Date());
		
		usuarioService.saveOrUpdate(usuario);
		
		super.onAuthenticationSuccess(request, response, authentication);
	}

}
