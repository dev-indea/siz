package br.gov.mt.indea.sistemaformin.entity;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

import br.gov.mt.indea.sistemaformin.enums.Dominio.SimNao;

@Audited
@Entity
@Table(name="folha_adicional")
public class FolhaAdicional extends BaseEntity<Long>{
	
	private static final long serialVersionUID = -4282187368801464436L;
	
	@Id
	@SequenceGenerator(name="folha_adicional_seq", sequenceName="folha_adicional_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="folha_adicional_seq")
	private Long id;

	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_usuario")
	private Usuario usuario;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_cadastro")
	private Calendar dataCadastro;
	
	@ManyToOne
	@JoinColumn(name="id_formin")
	private FormIN formIN;
	
	@ManyToOne
	@JoinColumn(name="id_formcom")
	private FormCOM formCOM;
	
	@Enumerated(EnumType.STRING)
	private SimNao encerramento;
	
	@Column(name="info_complementares", length=4500)
	private String informacoesComplementares;
	
	public String getResumoInformacoesComplementares(){
		if (this.informacoesComplementares == null)
			return null;
		
		StringBuilder sb = new StringBuilder(this.informacoesComplementares);
		
		if (sb.length() < 50)
			return sb.toString();
		else
			return sb.substring(0, 47) + "...";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public FormIN getFormIN() {
		return formIN;
	}

	public void setFormIN(FormIN formIN) {
		this.formIN = formIN;
	}

	public FormCOM getFormCOM() {
		return formCOM;
	}

	public void setFormCOM(FormCOM formCOM) {
		this.formCOM = formCOM;
	}

	public SimNao getEncerramento() {
		return encerramento;
	}

	public void setEncerramento(SimNao encerramento) {
		this.encerramento = encerramento;
	}

	public String getInformacoesComplementares() {
		return informacoesComplementares;
	}

	public void setInformacoesComplementares(String informacoesComplementares) {
		this.informacoesComplementares = informacoesComplementares;
	}

	public Calendar getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Calendar dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FolhaAdicional other = (FolhaAdicional) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}