package br.gov.mt.indea.sistemaformin.managedbeans;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.constraints.Past;

import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLBeanName;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;

import br.gov.mt.indea.sistemaformin.entity.AbstractAmostra;
import br.gov.mt.indea.sistemaformin.entity.AmostraSoroSanguineo;
import br.gov.mt.indea.sistemaformin.entity.FormCOM;
import br.gov.mt.indea.sistemaformin.entity.FormIN;
import br.gov.mt.indea.sistemaformin.entity.FormLAB;
import br.gov.mt.indea.sistemaformin.entity.InformacoesComplementaresEspeciesFormLAB;
import br.gov.mt.indea.sistemaformin.entity.InformacoesComplementaresMedicamentosFormLAB;
import br.gov.mt.indea.sistemaformin.entity.OutrasAmostras;
import br.gov.mt.indea.sistemaformin.enums.Dominio.SimNao;
import br.gov.mt.indea.sistemaformin.enums.Dominio.VigilanciaSindromica;
import br.gov.mt.indea.sistemaformin.exception.ApplicationErrorException;
import br.gov.mt.indea.sistemaformin.exception.ApplicationException;
import br.gov.mt.indea.sistemaformin.service.FormCOMService;
import br.gov.mt.indea.sistemaformin.service.FormINService;
import br.gov.mt.indea.sistemaformin.service.FormLABService;
import br.gov.mt.indea.sistemaformin.util.FacesMessageUtil;
import br.gov.mt.indea.sistemaformin.webservice.client.ClientWebServiceVeterinario;
import br.gov.mt.indea.sistemaformin.webservice.entity.Veterinario;

@Named
@ViewScoped
@URLBeanName("formLABManagedBean")
@URLMappings(mappings = {
		@URLMapping(id = "pesquisarFormLAB", pattern = "/formIN/#{formLABManagedBean.idFormIN}/formLAB/pesquisar", viewId = "/pages/formLAB/lista.jsf"),
		@URLMapping(id = "incluirFormLAB", pattern = "/formIN/#{formLABManagedBean.idFormIN}/formLAB/incluir", viewId = "/pages/formLAB/novo.jsf"),
		@URLMapping(id = "alterarFormLAB", pattern = "/formIN/#{formLABManagedBean.idFormIN}/formLAB/alterar/#{formLABManagedBean.id}", viewId = "/pages/formLAB/novo.jsf"),
		@URLMapping(id = "impressaoFormLAB", pattern = "/formIN/#{formLABManagedBean.idFormIN}/formLAB/impressao/#{formLABManagedBean.id}", viewId = "/pages/impressao.jsf")})
public class FormLABManagedBean implements Serializable {

	private static final long serialVersionUID = 1760023349204758559L;

	private Long id;
	
	private Long idFormIN;
	
	private FormIN formIN;
	
	@Inject
	private FormINService formINService;
	
	@Inject
	private FormLABService formLABService;
	
	@Inject
	private FormCOMService formCOMService;
	
	@Inject
	private FormLAB formLAB;
	
	private AmostraSoroSanguineo amostraSoroSanguineo;
	
	private OutrasAmostras outrasAmostras;
	
	private InformacoesComplementaresEspeciesFormLAB informacoesComplementaresEspecies = new InformacoesComplementaresEspeciesFormLAB();
	
	private InformacoesComplementaresMedicamentosFormLAB informacoesComplementaresMedicamentos = new InformacoesComplementaresMedicamentosFormLAB();
	
	private boolean editandoAmostraSoroSanguineo = false;
	
	private boolean visualizandoAmostraSoroSanguineo = false;
	
	private boolean editandoOutraAmostrasSoroSanguineo = false;
	
	private boolean visualizandoOutraAmostrasSoroSanguineo = false;
	
	private boolean editandoInformacoesComplementaresEspecies = false;
	
	private boolean visualizandoInformacoesComplementaresEspecies = false;
	
	private boolean editandoInformacoesComplementaresMedicamentos = false;
	
	private boolean visualizandoInformacoesComplementaresMedicamentos = false;
	
	@Past(message="Data precisa ser menor ou igual a data atual")
	private Date dataColheita;
	
	@Past(message="Data precisa ser menor ou igual a data atual")
	private Date dataUltimaVacina;
	
	@Past(message="Data precisa ser menor ou igual a data atual")
	private Date dataDeEnvioDasAmostras;
	
	private String cpfVeterinario;

	private String crmvVeterinario;

	private List<Veterinario> listaVeterinarios;
	
	@Inject
	private ClientWebServiceVeterinario clientWebServiceVeterinario;
	
	@PostConstruct
	private void init(){
		
	}
	
	private void carregarFormIN(){
		this.formIN = formINService.findByIdFetchPropriedade(this.idFormIN);
	}
	
	private void limpar() {
		this.formLAB = new FormLAB();
		this.dataColheita = null;
		this.dataDeEnvioDasAmostras = null;
	}
	
	@URLAction(mappingId = "incluirFormLAB", onPostback = false)
	public void novo(){
		this.limpar();
		this.carregarFormIN();
		
		this.formLAB.setFormIN(formIN);
		
		if (!formIN.getDoencaDeVigilanciaSindromica().equals(VigilanciaSindromica.NENHUMA_DAS_ANTERIORES))
			this.formLAB.setSindromeInvestigada(formIN.getDoencaDeVigilanciaSindromica().getDescricao());
		else if (formIN.getDianosticoProvavel() != null && !formIN.getDianosticoProvavel().equals(""))
			this.formLAB.setSindromeInvestigada(formIN.getDianosticoProvavel());
		else
			this.formLAB.setSindromeInvestigada(formIN.getDianosticoConclusivo());
	}
	
//	public String novo(FormCOM formCOM){
//		this.iniciarConversacao();
//		this.formLAB.setFormCOM(formCOM);
//		this.formLAB.setFormIN(formCOM.getFormIN());
//
//		return "/pages/formLAB/novo.xhtml";
//	}
	
	@URLAction(mappingId = "alterarFormLAB", onPostback = false)
	public void editar(){
		this.formLAB = formLABService.findByIdFetchAll(this.getId());
		
		if (this.formLAB.getDataColheita() != null)
			this.dataColheita = this.formLAB.getDataColheita().getTime();
		
		if (this.formLAB.getDataDeEnvioDasAmostras() != null)
			this.dataDeEnvioDasAmostras = this.formLAB.getDataDeEnvioDasAmostras().getTime();
	}
	
	public String adicionar() throws IOException{
		if (this.dataColheita != null){
			if (this.formLAB.getDataColheita() == null)
				this.formLAB.setDataColheita(Calendar.getInstance());
			this.formLAB.getDataColheita().setTime(dataColheita);
		}else
			this.formLAB.setDataColheita(null);
		
		if (this.dataDeEnvioDasAmostras != null){
			if (this.formLAB.getDataDeEnvioDasAmostras() == null)
				this.formLAB.setDataDeEnvioDasAmostras(Calendar.getInstance());
			this.formLAB.getDataDeEnvioDasAmostras().setTime(dataDeEnvioDasAmostras);
		}else
			this.formLAB.setDataDeEnvioDasAmostras(null);
		
		if (this.formLAB.getVeterinario() == null){
			FacesContext context = FacesContext.getCurrentInstance();
			
			context.addMessage("cadastro:fieldVeterinario:campoVeterinario", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Veterin�rio: valor � obrigat�rio.", "Veterin�rio: valor � obrigat�rio."));
			return "";
		}
		
		if (formLAB.getId() != null){
			this.formLABService.saveOrUpdate(formLAB);
			FacesMessageUtil.addInfoContextFacesMessage("Form LAB atualizado", "");
		}else{
			this.formLAB.setDataCadastro(Calendar.getInstance());
			this.formLABService.saveOrUpdate(formLAB);
			FacesMessageUtil.addInfoContextFacesMessage("Form LAB adicionado", "");
		}
		
		limpar();
	    return "pretty:visaoGeralFormIN";
	}
	
	@Inject
	private VisualizadorImpressaoManagedBean visualizadorImpressaoManagedBean;
	
	@URLAction(mappingId = "impressaoFormLAB", onPostback = false)
	public void imprimir() throws ApplicationErrorException{
		this.formLAB = formLABService.findByIdFetchAll(this.getId());
		
		visualizadorImpressaoManagedBean.exibirFormLAB(formLAB);
	}
	
	public void remover(FormLAB formLAB){
		this.formLABService.delete(formLAB);
		FacesMessageUtil.addInfoContextFacesMessage("Form LAB exclu�do com sucesso", "");
	}
	
	public List<FormCOM> getListaFormCOM(){
		return formCOMService.findAllBy(this.formLAB.getFormIN());
	}
	
	public boolean isTestePadraoSolicitado(){
		boolean resultado = false;
		if (this.formLAB.getTestePadraoSolicitado() == null)
			return resultado;
		else
			resultado = this.formLAB.getTestePadraoSolicitado().equals(SimNao.SIM);
		
		if (resultado)
			this.formLAB.setOutroTesteSolicitado(null);
		
		return resultado;
	}
	
	public void adicionarAmostraSoroSanguineo(){
		this.amostraSoroSanguineo.setFormLAB1(this.formLAB);
		
		if (this.dataUltimaVacina != null){
			if (this.amostraSoroSanguineo.getDataUltimaVacina() == null)
				this.amostraSoroSanguineo.setDataUltimaVacina(Calendar.getInstance());
			this.amostraSoroSanguineo.getDataUltimaVacina().setTime(dataUltimaVacina);
		}else
			this.amostraSoroSanguineo.setDataUltimaVacina(null);
		this.dataUltimaVacina = null;
		
		if (!editandoAmostraSoroSanguineo)
			this.formLAB.getAmostrasSoroSanguineo().add(amostraSoroSanguineo);
		else
			editandoAmostraSoroSanguineo = false;
		
		this.amostraSoroSanguineo = null;
		FacesMessageUtil.addInfoContextFacesMessage("Amostra inclu�da com sucesso", "");
	}
	
	public void novaAmostraSoroSanguineo(){
		this.amostraSoroSanguineo= new AmostraSoroSanguineo();
		this.editandoAmostraSoroSanguineo = false;
		this.visualizandoAmostraSoroSanguineo = false;
		
		this.dataUltimaVacina = null;
	}
	
	public void removerAmostraSoroSanguineo(AmostraSoroSanguineo amostraSoroSanguineo){
		this.formLAB.getAmostrasSoroSanguineo().remove(amostraSoroSanguineo);
	}
	
	public void editarAmostraSoroSanguineo(AmostraSoroSanguineo amostraSoroSanguineo){
		this.editandoAmostraSoroSanguineo = true;
		this.visualizandoAmostraSoroSanguineo = false;
		this.amostraSoroSanguineo = amostraSoroSanguineo;
		
		if (this.amostraSoroSanguineo.getDataUltimaVacina() != null)
			this.dataUltimaVacina = this.amostraSoroSanguineo.getDataUltimaVacina().getTime();
	}
	
	public void visualizarAmostraSoroSanguineo(AmostraSoroSanguineo amostraSoroSanguineo){
		this.visualizandoAmostraSoroSanguineo = true;
		this.editandoAmostraSoroSanguineo = false;
		this.amostraSoroSanguineo = amostraSoroSanguineo;
		
		if (this.amostraSoroSanguineo.getDataUltimaVacina() != null)
			this.dataUltimaVacina = this.amostraSoroSanguineo.getDataUltimaVacina().getTime();
	}
	
	public void adicionarOutrasAmostras(){
		this.outrasAmostras.setFormLAB2(this.formLAB);
		
		if (this.dataUltimaVacina != null){
			if (this.outrasAmostras.getDataUltimaVacina() == null)
				this.outrasAmostras.setDataUltimaVacina(Calendar.getInstance());
			this.outrasAmostras.getDataUltimaVacina().setTime(dataUltimaVacina);
		}else
			this.outrasAmostras.setDataUltimaVacina(null);
		this.dataUltimaVacina = null;
		
		if (!editandoOutraAmostrasSoroSanguineo)
			this.formLAB.getOutrasAmostras().add(outrasAmostras);
		else
			editandoOutraAmostrasSoroSanguineo = false;
		
		this.outrasAmostras = null;
		FacesMessageUtil.addInfoContextFacesMessage("Amostra inclu�da com sucesso", "");
	}
	
	public void novaOutrasAmostras(){
		this.outrasAmostras = new OutrasAmostras();
		this.editandoOutraAmostrasSoroSanguineo = false;
		this.visualizandoOutraAmostrasSoroSanguineo = false;

		this.dataUltimaVacina = null;
	}
	
	public void removerOutrasAmostras(OutrasAmostras outrasAmostras){
		this.formLAB.getOutrasAmostras().remove(outrasAmostras);
	}
	
	public void editarOutrasAmostras(OutrasAmostras outrasAmostras){
		this.editandoOutraAmostrasSoroSanguineo = true;
		this.visualizandoOutraAmostrasSoroSanguineo = false;
		this.outrasAmostras = outrasAmostras;
		
		if (this.outrasAmostras.getDataUltimaVacina() != null)
			this.dataUltimaVacina = this.outrasAmostras.getDataUltimaVacina().getTime();
	}
	
	public void visualizarOutrasAmostras(OutrasAmostras outrasAmostras){
		this.visualizandoOutraAmostrasSoroSanguineo = true;
		this.editandoOutraAmostrasSoroSanguineo = false;
		this.outrasAmostras = outrasAmostras;
		
		if (this.outrasAmostras.getDataUltimaVacina() != null)
			this.dataUltimaVacina = this.outrasAmostras.getDataUltimaVacina().getTime();
	}
	
	public void adicionarInformacoesComplementaresEspecies(){
		AbstractAmostra abs = null;
		if (amostraSoroSanguineo != null)
			abs = amostraSoroSanguineo;
		else
			abs = outrasAmostras;
		
		this.informacoesComplementaresEspecies.setAmostra(abs);
		if (!editandoInformacoesComplementaresEspecies)
			abs.getInformacoesComplementaresEspeciesFormLAB().add(informacoesComplementaresEspecies);
		else
			editandoInformacoesComplementaresEspecies = false;
		
		this.informacoesComplementaresEspecies = new InformacoesComplementaresEspeciesFormLAB();
		FacesMessageUtil.addInfoContextFacesMessage("Informa��o complementar inclu�da com sucesso", "");
	}
	
	public void novaInformacoesComplementaresEspecies(){
		this.informacoesComplementaresEspecies = new InformacoesComplementaresEspeciesFormLAB();
		this.editandoInformacoesComplementaresEspecies = false;
		this.visualizandoInformacoesComplementaresEspecies = false;
	}
	
	public void removerInformacoesComplementaresEspecies(InformacoesComplementaresEspeciesFormLAB informacoesComplementaresEspecies){
		AbstractAmostra abs = null;
		if (amostraSoroSanguineo != null)
			abs = amostraSoroSanguineo;
		else
			abs = outrasAmostras;
		
		abs.getInformacoesComplementaresEspeciesFormLAB().remove(informacoesComplementaresEspecies);
		
		FacesMessageUtil.addInfoContextFacesMessage("Informa��o complementar removida com sucesso", "");
	}
	
	public void editarInformacoesComplementaresEspecies(InformacoesComplementaresEspeciesFormLAB informacoesComplementaresEspecies){
		this.editandoInformacoesComplementaresEspecies = true;
		this.visualizandoInformacoesComplementaresEspecies = false;
		this.informacoesComplementaresEspecies = informacoesComplementaresEspecies;
	}
	
	public void visualizarInformacoesComplementaresEspecies(InformacoesComplementaresEspeciesFormLAB informacoesComplementaresEspecies){
		this.visualizandoInformacoesComplementaresEspecies = true;
		this.editandoInformacoesComplementaresEspecies = false;
		this.informacoesComplementaresEspecies = informacoesComplementaresEspecies;
	}
	
	public void cancelarCadastroInformacoesComplementares(){
		this.amostraSoroSanguineo = null;
		this.outrasAmostras = null;
	}
	
	public void adicionarInformacoesComplementaresMedicamentos(){
		AbstractAmostra abs = null;
		if (amostraSoroSanguineo != null)
			abs = amostraSoroSanguineo;
		else
			abs = outrasAmostras;
		
		this.informacoesComplementaresMedicamentos.setAmostra(abs);
		if (!editandoInformacoesComplementaresMedicamentos)
			abs.getInformacoesComplementaresMedicamentosFormLAB().add(informacoesComplementaresMedicamentos);
		else
			editandoInformacoesComplementaresMedicamentos = false;
		
		this.informacoesComplementaresMedicamentos = new InformacoesComplementaresMedicamentosFormLAB();
		FacesMessageUtil.addInfoContextFacesMessage("Informa��o complementar inclu�da com sucesso", "");
	}
	
	public void novaInformacoesComplementaresMedicamentos(){
		this.informacoesComplementaresMedicamentos = new InformacoesComplementaresMedicamentosFormLAB();
		this.editandoInformacoesComplementaresMedicamentos = false;
		this.visualizandoInformacoesComplementaresMedicamentos = false;
	}
	
	public void removerInformacoesComplementaresMedicamentos(InformacoesComplementaresMedicamentosFormLAB informacoesComplementaresMedicamentos){
		AbstractAmostra abs = null;
		if (amostraSoroSanguineo != null)
			abs = amostraSoroSanguineo;
		else
			abs = outrasAmostras;
		
		abs.getInformacoesComplementaresMedicamentosFormLAB().remove(informacoesComplementaresMedicamentos);
		
		FacesMessageUtil.addInfoContextFacesMessage("Informa��o complementar removida com sucesso", "");
	}
	
	public void editarInformacoesComplementaresMedicamentos(InformacoesComplementaresMedicamentosFormLAB informacoesComplementaresMedicamentos){
		this.editandoInformacoesComplementaresMedicamentos = true;
		this.visualizandoInformacoesComplementaresMedicamentos = false;
		this.informacoesComplementaresMedicamentos = informacoesComplementaresMedicamentos;
	}
	
	public void visualizarInformacoesComplementaresMedicamentos(InformacoesComplementaresMedicamentosFormLAB informacoesComplementaresMedicamentos){
		this.visualizandoInformacoesComplementaresMedicamentos = true;
		this.editandoInformacoesComplementaresMedicamentos = false;
		this.informacoesComplementaresMedicamentos = informacoesComplementaresMedicamentos;
	}
	
	public void abrirTelaDeBuscaDeVeterinario(){
		this.cpfVeterinario = null;
		this.crmvVeterinario = null;
		this.listaVeterinarios = null;
	}
	
	public void buscarVeterinarioPorCpf(){
		this.listaVeterinarios = null;
		
		Veterinario v;
		try {
			v = clientWebServiceVeterinario.getVeterinarioByCpf(cpfVeterinario);
			
			if (v != null){
				this.listaVeterinarios = new ArrayList<Veterinario>();
				this.listaVeterinarios.add(v);
			}
		} catch (ApplicationException e) {
			FacesMessageUtil.addErrorContextFacesMessage("Erro", e.getLocalizedMessage());
			return;
		}
		
		if (this.listaVeterinarios == null || this.listaVeterinarios.isEmpty())
			FacesMessageUtil.addWarnContextFacesMessage("Nenhum veterin�rio foi encontrado", "");
	}
	
	public void buscarVeterinarioPorCrmv(){
		this.listaVeterinarios = null;
		
		Veterinario v;
		try {
			v = clientWebServiceVeterinario.getVeterinarioByCRMV(crmvVeterinario);
			
			if (v != null){
				this.listaVeterinarios = new ArrayList<Veterinario>();
				this.listaVeterinarios.add(v);
			}
		} catch (ApplicationException e) {
			FacesMessageUtil.addErrorContextFacesMessage("Erro", e.getLocalizedMessage());
			return;
		}
		
		if (this.listaVeterinarios == null || this.listaVeterinarios.isEmpty())
			FacesMessageUtil.addWarnContextFacesMessage("Nenhum veterin�rio foi encontrado", "");
	}
	
	public void selecionarVeterinario(Veterinario veterinario){
		this.formLAB.setVeterinario(veterinario);
		FacesMessageUtil.addInfoContextFacesMessage("Veterinario " + this.formLAB.getVeterinario().getNome() + " selecionado", "");
	}

	public FormLAB getFormLAB() {
		return formLAB;
	}

	public void setFormLAB(FormLAB formLAB) {
		this.formLAB = formLAB;
	}

	public AmostraSoroSanguineo getAmostraSoroSanguineo() {
		return amostraSoroSanguineo;
	}

	public void setAmostraSoroSanguineo(AmostraSoroSanguineo amostraSoroSanguineo) {
		this.amostraSoroSanguineo = amostraSoroSanguineo;
	}

	public OutrasAmostras getOutrasAmostras() {
		return outrasAmostras;
	}

	public void setOutrasAmostras(OutrasAmostras outrasAmostras) {
		this.outrasAmostras = outrasAmostras;
	}

	public InformacoesComplementaresEspeciesFormLAB getInformacoesComplementaresEspecies() {
		return informacoesComplementaresEspecies;
	}

	public void setInformacoesComplementaresEspecies(
			InformacoesComplementaresEspeciesFormLAB informacoesComplementaresEspecies) {
		this.informacoesComplementaresEspecies = informacoesComplementaresEspecies;
	}

	public InformacoesComplementaresMedicamentosFormLAB getInformacoesComplementaresMedicamentos() {
		return informacoesComplementaresMedicamentos;
	}

	public void setInformacoesComplementaresMedicamentos(
			InformacoesComplementaresMedicamentosFormLAB informacoesComplementaresMedicamentos) {
		this.informacoesComplementaresMedicamentos = informacoesComplementaresMedicamentos;
	}

	public boolean isVisualizandoAmostraSoroSanguineo() {
		return visualizandoAmostraSoroSanguineo;
	}

	public boolean isVisualizandoOutraAmostrasSoroSanguineo() {
		return visualizandoOutraAmostrasSoroSanguineo;
	}
	
	public boolean isVisualizandoInformacoesComplementaresEspecies() {
		return visualizandoInformacoesComplementaresEspecies;
	}
	
	public boolean isVisualizandoInformacoesComplementaresMedicamentos() {
		return visualizandoInformacoesComplementaresMedicamentos;
	}

	public Date getDataColheita() {
		return dataColheita;
	}

	public void setDataColheita(Date dataColheita) {
		this.dataColheita = dataColheita;
	}

	public Date getDataUltimaVacina() {
		return dataUltimaVacina;
	}

	public void setDataUltimaVacina(Date dataUltimaVacina) {
		this.dataUltimaVacina = dataUltimaVacina;
	}

	public boolean isEditandoInformacoesComplementaresEspecies() {
		return editandoInformacoesComplementaresEspecies;
	}

	public void setEditandoInformacoesComplementaresEspecies(
			boolean editandoInformacoesComplementaresEspecies) {
		this.editandoInformacoesComplementaresEspecies = editandoInformacoesComplementaresEspecies;
	}

	public boolean isEditandoInformacoesComplementaresMedicamentos() {
		return editandoInformacoesComplementaresMedicamentos;
	}

	public void setEditandoInformacoesComplementaresMedicamentos(
			boolean editandoInformacoesComplementaresMedicamentos) {
		this.editandoInformacoesComplementaresMedicamentos = editandoInformacoesComplementaresMedicamentos;
	}

	public String getCpfVeterinario() {
		return cpfVeterinario;
	}

	public void setCpfVeterinario(String cpfVeterinario) {
		this.cpfVeterinario = cpfVeterinario;
	}

	public String getCrmvVeterinario() {
		return crmvVeterinario;
	}

	public void setCrmvVeterinario(String crmvVeterinario) {
		this.crmvVeterinario = crmvVeterinario;
	}

	public List<Veterinario> getListaVeterinarios() {
		return listaVeterinarios;
	}

	public void setListaVeterinarios(List<Veterinario> listaVeterinarios) {
		this.listaVeterinarios = listaVeterinarios;
	}

	public Date getDataDeEnvioDasAmostras() {
		return dataDeEnvioDasAmostras;
	}

	public void setDataDeEnvioDasAmostras(Date dataDeEnvioDasAmostras) {
		this.dataDeEnvioDasAmostras = dataDeEnvioDasAmostras;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public FormIN getFormIN() {
		return formIN;
	}

	public void setFormIN(FormIN formIN) {
		this.formIN = formIN;
	}

	public Long getIdFormIN() {
		return idFormIN;
	}

	public void setIdFormIN(Long idFormIN) {
		this.idFormIN = idFormIN;
	}

}
