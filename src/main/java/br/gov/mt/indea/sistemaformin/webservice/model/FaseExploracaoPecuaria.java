package br.gov.mt.indea.sistemaformin.webservice.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class FaseExploracaoPecuaria implements Serializable {
    
	private static final long serialVersionUID = -8143470007558241L;

	private Long idTable;
	
	private String id;
    
	private String nome;

    public FaseExploracaoPecuaria() {
        
    }

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public FaseExploracaoPecuaria(String id, String nome) {
		super();
		this.id = id;
		this.nome = nome;
	}

	public Long getIdTable() {
		return idTable;
	}

	public void setIdTable(Long idTable) {
		this.idTable = idTable;
	}

}