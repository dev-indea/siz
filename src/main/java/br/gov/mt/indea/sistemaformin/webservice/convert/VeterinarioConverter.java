package br.gov.mt.indea.sistemaformin.webservice.convert;

import javax.inject.Inject;

import br.gov.mt.indea.sistemaformin.entity.Municipio;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoUnidade;
import br.gov.mt.indea.sistemaformin.service.MunicipioService;
import br.gov.mt.indea.sistemaformin.service.ULEService;
import br.gov.mt.indea.sistemaformin.webservice.entity.Conselho;
import br.gov.mt.indea.sistemaformin.webservice.entity.Endereco;
import br.gov.mt.indea.sistemaformin.webservice.entity.TipoEmitente;
import br.gov.mt.indea.sistemaformin.webservice.entity.TipoLogradouro;
import br.gov.mt.indea.sistemaformin.webservice.entity.ULE;
import br.gov.mt.indea.sistemaformin.webservice.entity.Veterinario;

public class VeterinarioConverter {
	
	@Inject
	private MunicipioService municipioService;
	
	@Inject
	private ULEService uleService;
	
	public Veterinario fromModelToEntity(br.gov.mt.indea.sistemaformin.webservice.model.Veterinario modelo){
		Veterinario veterinario = new Veterinario();
		
		if (modelo == null)
			return null;
		
		if (modelo.getPessoa() != null){
		
			veterinario.setCodigoVeterinarioSVO(modelo.getPessoa().getId());
			
			veterinario.setApelido(modelo.getPessoa().getApelido());

			modelo.getPessoa().setCpfCnpj("00000000000" + modelo.getPessoa().getCpfCnpj());
			
			veterinario.setCpf(modelo.getPessoa().getCpfCnpj().substring((modelo.getPessoa().getCpfCnpj().length() - 11), modelo.getPessoa().getCpfCnpj().length()));
			
			veterinario.setDataFormatura(modelo.getDataFormatura());
			veterinario.setEmail(modelo.getPessoa().getEmail());
			veterinario.setInstituicaoFormatura(modelo.getInstituicaoFormatura());
			veterinario.setInstituicaoTrabalho(modelo.getInstituicaoTrabalho());
			veterinario.setNome(modelo.getPessoa().getNome());
			veterinario.setNumeroConselho(modelo.getNumeroConselho());
			veterinario.setPesquisador(modelo.getPesquisador());
			veterinario.setProfissionalOficial(modelo.getProfissionalOficial());
			veterinario.setProprietarioInstituicao(modelo.getProprietarioInstituicao());
			veterinario.setTipoPessoa(modelo.getPessoa().getTipoPessoa());
			
			if (modelo.getPessoa().getEndereco() != null){
				Endereco enderecoPropriedade = new Endereco();
				
				enderecoPropriedade.setBairro(modelo.getPessoa().getEndereco().getBairro());
				enderecoPropriedade.setCep(modelo.getPessoa().getEndereco().getCep());
				enderecoPropriedade.setComplemento(modelo.getPessoa().getEndereco().getComplemento());
				enderecoPropriedade.setLogradouro(modelo.getPessoa().getEndereco().getLogradouro());
				enderecoPropriedade.setNumero(modelo.getPessoa().getEndereco().getNumero());
				enderecoPropriedade.setReferencia(modelo.getPessoa().getEndereco().getReferencia());
				enderecoPropriedade.setTelefone(modelo.getPessoa().getEndereco().getTelefone());
				
				if (modelo.getPessoa().getEndereco().getTipoLogradouro() != null){
					TipoLogradouro tipoLogradouroEnderecoPropriedade = new TipoLogradouro();
					tipoLogradouroEnderecoPropriedade.setNome(modelo.getPessoa().getEndereco().getTipoLogradouro().getNome());
					
					enderecoPropriedade.setTipoLogradouro(tipoLogradouroEnderecoPropriedade);
				}
				
				if (modelo.getPessoa().getEndereco().getMunicipio() != null)
					enderecoPropriedade.setMunicipio(this.findMunicipio(modelo.getPessoa().getEndereco().getMunicipio()));
				
				veterinario.setEndereco(enderecoPropriedade);
				
				if (modelo.getPessoa().getPessoaFisica() != null){
					veterinario.setDataNascimento(modelo.getPessoa().getPessoaFisica().getDataNascimento());
					veterinario.setEmissorRg(modelo.getPessoa().getPessoaFisica().getEmissorRg());
					veterinario.setMunicipioNascimento(this.findMunicipio(modelo.getPessoa().getPessoaFisica().getMunicipioNascimento()));
					veterinario.setNomeMae(modelo.getPessoa().getPessoaFisica().getNomeMae());
					veterinario.setRg(modelo.getPessoa().getPessoaFisica().getRg());
					veterinario.setSexo(modelo.getPessoa().getPessoaFisica().getSexo());
				}
			}
		}
		
		if (modelo.getConselho() != null){
			Conselho conselho = new Conselho();
			
			conselho.setCodigoPga(modelo.getConselho().getCodigoPga());
			conselho.setId(modelo.getConselho().getId());
			conselho.setIdTable(modelo.getConselho().getIdTable());
			conselho.setNome(modelo.getConselho().getNome());
			
			veterinario.setConselho(conselho);
		}
		
		if (modelo.getTipoEmitente() != null){
			TipoEmitente tipoEmitente = new TipoEmitente();
			
			tipoEmitente.setCodigoPga(modelo.getTipoEmitente().getCodigoPga());
			tipoEmitente.setId(modelo.getTipoEmitente().getId());
			tipoEmitente.setIdTable(modelo.getTipoEmitente().getIdTable());
			tipoEmitente.setNome(modelo.getTipoEmitente().getNome());
			
			veterinario.setTipoEmitente(tipoEmitente);
		}

		if (modelo.getUle() != null){
			veterinario.setUle(this.findULE(modelo.getUle()));
		}
		
		return veterinario;
	}

	private Municipio findMunicipio(br.gov.mt.indea.sistemaformin.webservice.model.Municipio municipioModel) {
		if (municipioModel == null)
			return null;
		
		String codgIBGE = municipioModel.getId() + "";
		
		Municipio municipio = municipioService.findByCodgIBGE(codgIBGE.substring(0, 2), codgIBGE.substring(2, codgIBGE.length()));
		
		return municipio;
	}
	
	private ULE findULE(br.gov.mt.indea.sistemaformin.webservice.model.ULE uleModel){
		if (uleModel == null)
			return null;
		
		TipoUnidade tipoUnidade = null;
		if (uleModel .getUrs() == null)
			tipoUnidade = TipoUnidade.URS;
		else
			tipoUnidade = TipoUnidade.ULE;
		
		ULE ule = uleService.findByNome(uleModel.getNome(), tipoUnidade);
		
		if (ule == null)
			if (tipoUnidade.equals(TipoUnidade.URS)){
				tipoUnidade = TipoUnidade.ULE;
				ule = uleService.findByNome(uleModel.getNome(), tipoUnidade);
			}
		
		return ule;
	}

}