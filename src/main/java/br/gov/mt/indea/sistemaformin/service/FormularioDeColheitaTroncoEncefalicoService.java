package br.gov.mt.indea.sistemaformin.service;

import java.util.Calendar;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.mt.indea.sistemaformin.entity.FormularioDeColheitaTroncoEncefalico;
import br.gov.mt.indea.sistemaformin.entity.dto.AbstractDTO;
import br.gov.mt.indea.sistemaformin.entity.dto.FormularioDeColheitaTroncoEncefalicoDTO;

@Stateless
public class FormularioDeColheitaTroncoEncefalicoService extends PaginableService<FormularioDeColheitaTroncoEncefalico, Long>{

	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(FormularioDeColheitaTroncoEncefalicoService.class);
	
	protected FormularioDeColheitaTroncoEncefalicoService() {
		super(FormularioDeColheitaTroncoEncefalico.class);
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public Long countAll(AbstractDTO dto) {
		StringBuilder sql = new StringBuilder();
		sql.append("select count(distinct formularioDeColheitaTroncoEncefalico) ")
		   .append("  from " + this.getType().getSimpleName() + " as formularioDeColheitaTroncoEncefalico")
		   .append("  left join formularioDeColheitaTroncoEncefalico.achadosLoteEnfermidadeAbatedouroFrigorifico achadosLoteEnfermidadeAbatedouroFrigorifico")
		   .append("  left join achadosLoteEnfermidadeAbatedouroFrigorifico.loteEnfermidadeAbatedouroFrigorifico loteEnfermidadeAbatedouroFrigorifico")
		   .append("  left join loteEnfermidadeAbatedouroFrigorifico.proprietario proprietario ")
		   .append("  left join proprietario.propriedade propriedade")
		   .append("  left join loteEnfermidadeAbatedouroFrigorifico.enfermidadeAbatedouroFrigorifico enfermidadeAbatedouroFrigorifico")
		   .append("  left join enfermidadeAbatedouroFrigorifico.servicoDeInspecao servicoDeInspecao")
		   .append("  where 1 = 1 ");
		
		if (dto != null && !dto.isNull()){
			FormularioDeColheitaTroncoEncefalicoDTO formularioDeColheitaTroncoEncefalicoDTO = (FormularioDeColheitaTroncoEncefalicoDTO) dto;
			if (formularioDeColheitaTroncoEncefalicoDTO.getId() != null)
				sql.append("  and formularioDeColheitaTroncoEncefalico.id = :id");
			else {
				if (formularioDeColheitaTroncoEncefalicoDTO.getDataDoAbate() != null)
					sql.append("  and to_char(enfermidadeAbatedouroFrigorifico.dataAbate, 'YYYY-MM-DD HH:MI:SS') between :inicio and :final");
				if (formularioDeColheitaTroncoEncefalicoDTO.getMunicipio() != null)
					sql.append("  and propriedade.municipio = :municipio");
				if (formularioDeColheitaTroncoEncefalicoDTO.getNumero() != null && !formularioDeColheitaTroncoEncefalicoDTO.getNumero().equals(""))
					sql.append("  and formularioDeColheitaTroncoEncefalico.numero = :numero");
				if (formularioDeColheitaTroncoEncefalicoDTO.getServicoDeInspecao() != null)
					sql.append("  and enfermidadeAbatedouroFrigorifico.servicoDeInspecao = :servicoDeInspecao");
			}
		}
		
		Query query = getSession().createQuery(sql.toString());
		
		if (dto != null && !dto.isNull()){
			FormularioDeColheitaTroncoEncefalicoDTO formularioDeColheitaTroncoEncefalicoDTO = (FormularioDeColheitaTroncoEncefalicoDTO) dto;
			
			if (formularioDeColheitaTroncoEncefalicoDTO.getId() != null)
				query.setParameter("id", formularioDeColheitaTroncoEncefalicoDTO.getId());
			else {
				if (formularioDeColheitaTroncoEncefalicoDTO.getMunicipio() != null)
					query.setParameter("municipio", formularioDeColheitaTroncoEncefalicoDTO.getMunicipio());
				if (formularioDeColheitaTroncoEncefalicoDTO.getNumero() != null && !formularioDeColheitaTroncoEncefalicoDTO.getNumero().equals(""))
					query.setString("numero", formularioDeColheitaTroncoEncefalicoDTO.getNumero());
				if (formularioDeColheitaTroncoEncefalicoDTO.getServicoDeInspecao() != null)
					query.setParameter("servicoDeInspecao", formularioDeColheitaTroncoEncefalicoDTO.getServicoDeInspecao());
				
				if (formularioDeColheitaTroncoEncefalicoDTO.getDataDoAbate() != null){
					Calendar i = Calendar.getInstance();
					i.setTime(formularioDeColheitaTroncoEncefalicoDTO.getDataDoAbate());
					
					StringBuilder sbI = new StringBuilder();
					sbI.append(i.get(Calendar.YEAR)).append("-")
					   .append(((i.get(Calendar.MONTH) + 1) + "").length() > 1 ? (i.get(Calendar.MONTH)+1) : "0" + (i.get(Calendar.MONTH)+1)).append("-")
					   .append((i.get(Calendar.DAY_OF_MONTH) + "").length() > 1 ? i.get(Calendar.DAY_OF_MONTH) : "0" + i.get(Calendar.DAY_OF_MONTH))
					   .append(" ")
					   .append("00").append(":")
					   .append("00").append(":")
					   .append("00");
					
					Calendar f = Calendar.getInstance();
					f.setTime(formularioDeColheitaTroncoEncefalicoDTO.getDataDoAbate());
					
					StringBuilder sbF = new StringBuilder();
					sbF.append(f.get(Calendar.YEAR)).append("-")
					   .append(((f.get(Calendar.MONTH) + 1) + "").length() > 1 ? (f.get(Calendar.MONTH)+1) : "0" + (f.get(Calendar.MONTH)+1)).append("-")
					   .append((f.get(Calendar.DAY_OF_MONTH) + "").length() > 1 ? f.get(Calendar.DAY_OF_MONTH) : "0" + f.get(Calendar.DAY_OF_MONTH))
					   .append(" ")
					   .append("23").append(":")
					   .append("23").append(":")
					   .append("59");
					
					query.setString("inicio", sbI.toString());
					query.setString("final", sbF.toString());
				}
			}
		}

		return (Long) query.uniqueResult();
    }
	
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<FormularioDeColheitaTroncoEncefalico> findAllComPaginacao(AbstractDTO dto, int first, int pageSize, String sortField, String sortOrder) {
		StringBuilder sql = new StringBuilder();
		sql.append("select formularioDeColheitaTroncoEncefalico")
		   .append("  from " + this.getType().getSimpleName() + " as formularioDeColheitaTroncoEncefalico")
		   .append("  left join fetch formularioDeColheitaTroncoEncefalico.achadosLoteEnfermidadeAbatedouroFrigorifico achadosLoteEnfermidadeAbatedouroFrigorifico")
		   .append("  left join fetch achadosLoteEnfermidadeAbatedouroFrigorifico.loteEnfermidadeAbatedouroFrigorifico loteEnfermidadeAbatedouroFrigorifico")
		   .append("  left join fetch loteEnfermidadeAbatedouroFrigorifico.proprietario proprietario ")
		   .append("  left join fetch proprietario.propriedade propriedade")
		   .append("  left join fetch loteEnfermidadeAbatedouroFrigorifico.enfermidadeAbatedouroFrigorifico enfermidadeAbatedouroFrigorifico")
		   .append("  left join fetch enfermidadeAbatedouroFrigorifico.servicoDeInspecao servicoDeInspecao")
		   .append("  where 1 = 1 ");
		
		if (dto != null && !dto.isNull()){
			FormularioDeColheitaTroncoEncefalicoDTO formularioDeColheitaTroncoEncefalicoDTO = (FormularioDeColheitaTroncoEncefalicoDTO) dto;
			if (formularioDeColheitaTroncoEncefalicoDTO.getId() != null)
				sql.append("  and formularioDeColheitaTroncoEncefalico.id = :id");
			else {
				if (formularioDeColheitaTroncoEncefalicoDTO.getDataDoAbate() != null)
					sql.append("  and to_char(enfermidadeAbatedouroFrigorifico.dataAbate, 'YYYY-MM-DD HH:MI:SS') between :inicio and :final");
				if (formularioDeColheitaTroncoEncefalicoDTO.getMunicipio() != null)
					sql.append("  and propriedade.municipio = :municipio");
				if (formularioDeColheitaTroncoEncefalicoDTO.getNumero() != null && !formularioDeColheitaTroncoEncefalicoDTO.getNumero().equals(""))
					sql.append("  and formularioDeColheitaTroncoEncefalico.numero = :numero");
				if (formularioDeColheitaTroncoEncefalicoDTO.getServicoDeInspecao() != null)
					sql.append("  and enfermidadeAbatedouroFrigorifico.servicoDeInspecao = :servicoDeInspecao");
			}
		}
		
		if (StringUtils.isNotBlank(sortField))
			sql.append(" order by enfermidadeAbatedouroFrigorifico." + sortField + " " + sortOrder);

		Query query = getSession().createQuery(sql.toString());
		
		if (dto != null && !dto.isNull()){
			FormularioDeColheitaTroncoEncefalicoDTO formularioDeColheitaTroncoEncefalicoDTO = (FormularioDeColheitaTroncoEncefalicoDTO) dto;
			
			if (formularioDeColheitaTroncoEncefalicoDTO.getId() != null)
				query.setParameter("id", formularioDeColheitaTroncoEncefalicoDTO.getId());
			else {
				if (formularioDeColheitaTroncoEncefalicoDTO.getMunicipio() != null)
					query.setParameter("municipio", formularioDeColheitaTroncoEncefalicoDTO.getMunicipio());
				if (formularioDeColheitaTroncoEncefalicoDTO.getNumero() != null && !formularioDeColheitaTroncoEncefalicoDTO.getNumero().equals(""))
					query.setString("numero", formularioDeColheitaTroncoEncefalicoDTO.getNumero());
				if (formularioDeColheitaTroncoEncefalicoDTO.getServicoDeInspecao() != null)
					query.setParameter("servicoDeInspecao", formularioDeColheitaTroncoEncefalicoDTO.getServicoDeInspecao());
				
				if (formularioDeColheitaTroncoEncefalicoDTO.getDataDoAbate() != null){
					Calendar i = Calendar.getInstance();
					i.setTime(formularioDeColheitaTroncoEncefalicoDTO.getDataDoAbate());
					
					StringBuilder sbI = new StringBuilder();
					sbI.append(i.get(Calendar.YEAR)).append("-")
					   .append(((i.get(Calendar.MONTH) + 1) + "").length() > 1 ? (i.get(Calendar.MONTH)+1) : "0" + (i.get(Calendar.MONTH)+1)).append("-")
					   .append((i.get(Calendar.DAY_OF_MONTH) + "").length() > 1 ? i.get(Calendar.DAY_OF_MONTH) : "0" + i.get(Calendar.DAY_OF_MONTH))
					   .append(" ")
					   .append("00").append(":")
					   .append("00").append(":")
					   .append("00");
					
					Calendar f = Calendar.getInstance();
					f.setTime(formularioDeColheitaTroncoEncefalicoDTO.getDataDoAbate());
					
					StringBuilder sbF = new StringBuilder();
					sbF.append(f.get(Calendar.YEAR)).append("-")
					   .append(((f.get(Calendar.MONTH) + 1) + "").length() > 1 ? (f.get(Calendar.MONTH)+1) : "0" + (f.get(Calendar.MONTH)+1)).append("-")
					   .append((f.get(Calendar.DAY_OF_MONTH) + "").length() > 1 ? f.get(Calendar.DAY_OF_MONTH) : "0" + f.get(Calendar.DAY_OF_MONTH))
					   .append(" ")
					   .append("23").append(":")
					   .append("23").append(":")
					   .append("59");
					
					query.setString("inicio", sbI.toString());
					query.setString("final", sbF.toString());
				}
			}
		}

		query.setFirstResult(first);
		query.setMaxResults(pageSize);

		List<FormularioDeColheitaTroncoEncefalico> lista = query.list();
		
		if (dto != null && !dto.isNull()){
			FormularioDeColheitaTroncoEncefalicoDTO formularioDeColheitaTroncoEncefalicoDTO = (FormularioDeColheitaTroncoEncefalicoDTO) dto;
			formularioDeColheitaTroncoEncefalicoDTO.setId(null);
		}
		
		return lista;
    }
	
	@Override
	public void saveOrUpdate(FormularioDeColheitaTroncoEncefalico formularioDeColheitaTroncoEncefalico) {
		formularioDeColheitaTroncoEncefalico = (FormularioDeColheitaTroncoEncefalico) this.getSession().merge(formularioDeColheitaTroncoEncefalico);
		
		super.saveOrUpdate(formularioDeColheitaTroncoEncefalico);
	}
	
	@Override
	public void validar(FormularioDeColheitaTroncoEncefalico model) {
		
	}

	@Override
	public void validarPersist(FormularioDeColheitaTroncoEncefalico model) {
		
	}

	@Override
	public void validarMerge(FormularioDeColheitaTroncoEncefalico model) {
		
	}

	@Override
	public void validarDelete(FormularioDeColheitaTroncoEncefalico model) {
		
	}

	public FormularioDeColheitaTroncoEncefalico findFormularioDeColheitaTroncoEncefalicoFetchAll(Long id){
		FormularioDeColheitaTroncoEncefalico formularioDeColheitaTroncoEncefalico;
		
		StringBuilder sql = new StringBuilder();
		sql.append("select formularioDeColheitaTroncoEncefalico ")
		   .append("  from FormularioDeColheitaTroncoEncefalico formularioDeColheitaTroncoEncefalico ")
		   .append("  join fetch formularioDeColheitaTroncoEncefalico.achadosLoteFormularioDeColheitaTroncoEncefalico achadosLoteFormularioDeColheitaTroncoEncefalico ")
		   .append("  join fetch achadosLoteFormularioDeColheitaTroncoEncefalico.loteFormularioDeColheitaTroncoEncefalico loteFormularioDeColheitaTroncoEncefalico ")
		   .append("  join fetch loteFormularioDeColheitaTroncoEncefalico.formularioDeColheitaTroncoEncefalico formularioDeColheitaTroncoEncefalico ")
		   .append("  join fetch formularioDeColheitaTroncoEncefalico.servicoDeInspecao servicoDeInspecao ")
		   .append(" where formularioDeColheitaTroncoEncefalico.id = :id ");
		
		Query query = getSession().createQuery(sql.toString()).setLong("id", id);
		formularioDeColheitaTroncoEncefalico = (FormularioDeColheitaTroncoEncefalico) query.uniqueResult();
		
		if (formularioDeColheitaTroncoEncefalico != null){
			
		}
		
		return formularioDeColheitaTroncoEncefalico;
	}

}
