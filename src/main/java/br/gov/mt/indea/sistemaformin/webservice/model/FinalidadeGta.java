package br.gov.mt.indea.sistemaformin.webservice.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class FinalidadeGta implements Serializable {

	private static final long serialVersionUID = -8755693590006154020L;

	private Long id;
    
    private String nome;
    
    private String atualizaSaldoDestino;
    
    private String dePropriedade;
    
    private String deAglomeracao;
    
    private String deEstabelecimento;
    
    private String paraPropriedade;
    
    private String paraAglomeracao;
    
    private String paraEstabelecimento;
    
    private String codigoPga;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getAtualizaSaldoDestino() {
        return atualizaSaldoDestino;
    }

    public void setAtualizaSaldoDestino(String atualizaSaldoDestino) {
        this.atualizaSaldoDestino = atualizaSaldoDestino;
    }

    public String getDePropriedade() {
        return dePropriedade;
    }

    public void setDePropriedade(String dePropriedade) {
        this.dePropriedade = dePropriedade;
    }

    public String getDeAglomeracao() {
        return deAglomeracao;
    }

    public void setDeAglomeracao(String deAglomeracao) {
        this.deAglomeracao = deAglomeracao;
    }

    public String getDeEstabelecimento() {
        return deEstabelecimento;
    }

    public void setDeEstabelecimento(String deEstabelecimento) {
        this.deEstabelecimento = deEstabelecimento;
    }

    public String getParaPropriedade() {
        return paraPropriedade;
    }

    public void setParaPropriedade(String paraPropriedade) {
        this.paraPropriedade = paraPropriedade;
    }

    public String getParaAglomeracao() {
        return paraAglomeracao;
    }

    public void setParaAglomeracao(String paraAglomeracao) {
        this.paraAglomeracao = paraAglomeracao;
    }

    public String getParaEstabelecimento() {
        return paraEstabelecimento;
    }

    public void setParaEstabelecimento(String paraEstabelecimento) {
        this.paraEstabelecimento = paraEstabelecimento;
    }

    public String getCodigoPga() {
        return codigoPga;
    }

    public void setCodigoPga(String codigoPga) {
        this.codigoPga = codigoPga;
    }
}