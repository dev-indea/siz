package br.gov.mt.indea.sistemaformin.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.mt.indea.sistemaformin.entity.FormCOM;
import br.gov.mt.indea.sistemaformin.entity.FormIN;
import br.gov.mt.indea.sistemaformin.entity.TransitoDeAnimais;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class FormCOMService extends PaginableService<FormCOM, Long> {

	private static final Logger log = LoggerFactory.getLogger(FormCOMService.class);
	
	protected FormCOMService() {
		super(FormCOM.class);
	}
	
	public FormCOM findByIdFetchAll(Long id){
		FormCOM formCOM;
		
		StringBuilder sql = new StringBuilder();
		sql.append("select formCOM ")
		   .append("  from FormCOM formCOM ")
		   .append("  join fetch formCOM.formIN formin")
		   .append("  left join fetch formin.listaFormCOM")
		   
		   .append("  left join fetch formin.bovinos")
		   .append("  left join fetch formin.bubalinos")
		   .append("  left join fetch formin.caprinos")
		   .append("  left join fetch formin.ovinos")
		   .append("  left join fetch formin.suinos")
		   .append("  left join fetch formin.equinos")
		   .append("  left join fetch formin.asininos")
		   .append("  left join fetch formin.muares")
		   .append("  left join fetch formin.aves")
		   .append("  left join fetch formin.abelhas")
		   .append("  left join fetch formin.lagomorfos")
		   .append("  left join fetch formin.outrosAnimais")
		   
		   .append("  join fetch formin.propriedade propriedade")
		   .append("  join fetch formin.proprietario proprietario")
		   .append("  left join fetch proprietario.endereco")
		   .append("  left join fetch propriedade.coordenadaGeografica")
		   .append("  join fetch propriedade.municipio")
		   .append("  join fetch propriedade.ule")
		   
		   .append("  left join fetch formCOM.bovinos")
		   .append("  left join fetch formCOM.bubalinos")
		   .append("  left join fetch formCOM.caprinos")
		   .append("  left join fetch formCOM.ovinos")
		   .append("  left join fetch formCOM.suinos")
		   .append("  left join fetch formCOM.equinos")
		   .append("  left join fetch formCOM.asininos")
		   .append("  left join fetch formCOM.muares")
		   .append("  left join fetch formCOM.aves")
		   .append("  left join fetch formCOM.abelhas")
		   .append("  left join fetch formCOM.lagomorfos")
		   .append("  left join fetch formCOM.outrosAnimais")
		   
		   .append("  join fetch formCOM.veterinario vet")
		   .append("  join fetch vet.conselho")
		   .append("  join fetch vet.tipoEmitente")
		   .append("  left join fetch vet.ule uu")
		   .append("  left join fetch uu.urs")
		   .append("  left join fetch vet.endereco")
		   .append(" where formCOM.id = :id ");
		
		Query query = getSession().createQuery(sql.toString()).setLong("id", id);
		formCOM = (FormCOM) query.uniqueResult();
		
		if (formCOM != null){
			
			Hibernate.initialize(formCOM.getListaDeResultadoDeTesteDeDiagnosticos());
			Hibernate.initialize(formCOM.getVacinas());
			Hibernate.initialize(formCOM.getMedicacoes());
			Hibernate.initialize(formCOM.getTransitoDeAnimais());
			
			for (TransitoDeAnimais transito : formCOM.getTransitoDeAnimais()) {
				Hibernate.initialize(transito.getGta());
				Hibernate.initialize(transito.getGta().getEmitente());
				Hibernate.initialize(transito.getGta().getFinalidade());
				Hibernate.initialize(transito.getGta().getTipoEmitente());
				Hibernate.initialize(transito.getGta().getGrupoEspecie());
				Hibernate.initialize(transito.getGta().getEspecie());
				Hibernate.initialize(transito.getGta().getUfOrigem());
				Hibernate.initialize(transito.getGta().getUfDestino());
				Hibernate.initialize(transito.getGta().getTipoGta());
				Hibernate.initialize(transito.getGta().getPessoaDestino());
				Hibernate.initialize(transito.getGta().getPessoaDestino().getMunicipio());
				Hibernate.initialize(transito.getGta().getPessoaDestino().getMunicipio().getUf());
				Hibernate.initialize(transito.getGta().getPessoaOrigemGta());
				Hibernate.initialize(transito.getGta().getPessoaOrigemGta().getMunicipio());
				Hibernate.initialize(transito.getGta().getPessoaOrigemGta().getMunicipio().getUf());
				Hibernate.initialize(transito.getGta().getSituacaoGta());
			}
			
			Hibernate.initialize(formCOM.getOcorrenciasObservadas());
			Hibernate.initialize(formCOM.getMedidas());
			Hibernate.initialize(formCOM.getListaFolhasAdicionais());
			Hibernate.initialize(formCOM.getListaFormLAB());
			Hibernate.initialize(formCOM.getListaFormSV());
			Hibernate.initialize(formCOM.getListaFormSH());
			Hibernate.initialize(formCOM.getListaFormSN());
			Hibernate.initialize(formCOM.getListaFormSRN());
			Hibernate.initialize(formCOM.getListaFormAIE());
			Hibernate.initialize(formCOM.getListaFormMormo());
			Hibernate.initialize(formCOM.getListaFormMaleina());
			Hibernate.initialize(formCOM.getListaFormEQ());
	
			if (formCOM.getBovinos() != null){
				Hibernate.initialize(formCOM.getBovinos().getDetalhes());
			}
			if (formCOM.getBubalinos() != null){
				Hibernate.initialize(formCOM.getBubalinos().getDetalhes());
			}
			if (formCOM.getCaprinos() != null){
				Hibernate.initialize(formCOM.getCaprinos().getDetalhes());
			}
			if (formCOM.getOvinos() != null){
				Hibernate.initialize(formCOM.getOvinos().getDetalhes());
			}
			if (formCOM.getSuinos() != null){
				Hibernate.initialize(formCOM.getSuinos().getDetalhes());
			}
			if (formCOM.getEquinos() != null){
				Hibernate.initialize(formCOM.getEquinos().getDetalhes());
			}
			if (formCOM.getAsininos() != null){
				Hibernate.initialize(formCOM.getAsininos().getDetalhes());
			}
			if (formCOM.getMuares() != null){
				Hibernate.initialize(formCOM.getMuares().getDetalhes());
			}
			if (formCOM.getAves() != null){
				Hibernate.initialize(formCOM.getAves().getDetalhes());
			}
			if (formCOM.getAbelhas() != null){
				Hibernate.initialize(formCOM.getAbelhas().getDetalhes());
			}
			if (formCOM.getLagomorfos() != null){
				Hibernate.initialize(formCOM.getLagomorfos().getDetalhes());
			}
			if (formCOM.getOutrosAnimais() != null){
				Hibernate.initialize(formCOM.getOutrosAnimais().getDetalhes());
			}
			
			if (formCOM.getFormIN().getListaFormCOM() != null){
				for (FormCOM formCOM_ : formCOM.getFormIN().getListaFormCOM()) {
					if (!formCOM_.equals(formCOM)){
						if (formCOM_.getBovinos() != null){
							Hibernate.initialize(formCOM_.getBovinos().getDetalhes());
						}
						if (formCOM_.getBubalinos() != null){
							Hibernate.initialize(formCOM_.getBubalinos().getDetalhes());
						}
						if (formCOM_.getCaprinos() != null){
							Hibernate.initialize(formCOM_.getCaprinos().getDetalhes());
						}
						if (formCOM_.getOvinos() != null){
							Hibernate.initialize(formCOM_.getOvinos().getDetalhes());
						}
						if (formCOM_.getSuinos() != null){
							Hibernate.initialize(formCOM_.getSuinos().getDetalhes());
						}
						if (formCOM_.getEquinos() != null){
							Hibernate.initialize(formCOM_.getEquinos().getDetalhes());
						}
						if (formCOM_.getAsininos() != null){
							Hibernate.initialize(formCOM_.getAsininos().getDetalhes());
						}
						if (formCOM_.getMuares() != null){
							Hibernate.initialize(formCOM_.getMuares().getDetalhes());
						}
						if (formCOM_.getAves() != null){
							Hibernate.initialize(formCOM_.getAves().getDetalhes());
						}
						if (formCOM_.getAbelhas() != null){
							Hibernate.initialize(formCOM_.getAbelhas().getDetalhes());
						}
						if (formCOM_.getLagomorfos() != null){
							Hibernate.initialize(formCOM_.getLagomorfos().getDetalhes());
						}
						if (formCOM_.getOutrosAnimais() != null){
							Hibernate.initialize(formCOM_.getOutrosAnimais().getDetalhes());
						}
					}
				}
			}
			
			//Informações de animais do Form IN
			if (formCOM.getFormIN().getBovinos() != null){
				Hibernate.initialize(formCOM.getFormIN().getBovinos().getDetalhes());
			}
			if (formCOM.getFormIN().getBubalinos() != null){
				Hibernate.initialize(formCOM.getFormIN().getBubalinos().getDetalhes());
			}
			if (formCOM.getFormIN().getCaprinos() != null){
				Hibernate.initialize(formCOM.getFormIN().getCaprinos().getDetalhes());
			}
			if (formCOM.getFormIN().getOvinos() != null){
				Hibernate.initialize(formCOM.getFormIN().getOvinos().getDetalhes());
			}
			if (formCOM.getFormIN().getSuinos() != null){
				Hibernate.initialize(formCOM.getFormIN().getSuinos().getDetalhes());
			}
			if (formCOM.getFormIN().getEquinos() != null){
				Hibernate.initialize(formCOM.getFormIN().getEquinos().getDetalhes());
			}
			if (formCOM.getFormIN().getAsininos() != null){
				Hibernate.initialize(formCOM.getFormIN().getAsininos().getDetalhes());
			}
			if (formCOM.getFormIN().getMuares() != null){
				Hibernate.initialize(formCOM.getFormIN().getMuares().getDetalhes());
			}
			if (formCOM.getFormIN().getAves() != null){
				Hibernate.initialize(formCOM.getFormIN().getAves().getDetalhes());
			}
			if (formCOM.getFormIN().getAbelhas() != null){
				Hibernate.initialize(formCOM.getFormIN().getAbelhas().getDetalhes());
			}
			if (formCOM.getFormIN().getLagomorfos() != null){
				Hibernate.initialize(formCOM.getFormIN().getLagomorfos().getDetalhes());
			}
			if (formCOM.getFormIN().getOutrosAnimais() != null){
				Hibernate.initialize(formCOM.getFormIN().getOutrosAnimais().getDetalhes());
			}
			
		}
		
		return formCOM;
	}
	
	public FormCOM getInformacoesDeAnimaisDoFormIN(FormCOM formCOM){
		
		formCOM.setFormIN((FormIN) this.getSession().merge(formCOM.getFormIN()));
		
		Hibernate.initialize(formCOM.getFormIN().getBovinos());
		Hibernate.initialize(formCOM.getFormIN().getBubalinos());
		Hibernate.initialize(formCOM.getFormIN().getCaprinos());
		Hibernate.initialize(formCOM.getFormIN().getOvinos());
		Hibernate.initialize(formCOM.getFormIN().getSuinos());
		Hibernate.initialize(formCOM.getFormIN().getEquinos());
		Hibernate.initialize(formCOM.getFormIN().getAsininos());
		Hibernate.initialize(formCOM.getFormIN().getMuares());
		Hibernate.initialize(formCOM.getFormIN().getAves());
		Hibernate.initialize(formCOM.getFormIN().getAbelhas());
		Hibernate.initialize(formCOM.getFormIN().getLagomorfos());
		Hibernate.initialize(formCOM.getFormIN().getOutrosAnimais());
		
		if (formCOM.getFormIN().getBovinos() != null){
			Hibernate.initialize(formCOM.getFormIN().getBovinos().getDetalhes());
		}
		if (formCOM.getFormIN().getBubalinos() != null){
			Hibernate.initialize(formCOM.getFormIN().getBubalinos().getDetalhes());
		}
		if (formCOM.getFormIN().getCaprinos() != null){
			Hibernate.initialize(formCOM.getFormIN().getCaprinos().getDetalhes());
		}
		if (formCOM.getFormIN().getOvinos() != null){
			Hibernate.initialize(formCOM.getFormIN().getOvinos().getDetalhes());
		}
		if (formCOM.getFormIN().getSuinos() != null){
			Hibernate.initialize(formCOM.getFormIN().getSuinos().getDetalhes());
		}
		if (formCOM.getFormIN().getEquinos() != null){
			Hibernate.initialize(formCOM.getFormIN().getEquinos().getDetalhes());
		}
		if (formCOM.getFormIN().getAsininos() != null){
			Hibernate.initialize(formCOM.getFormIN().getAsininos().getDetalhes());
		}
		if (formCOM.getFormIN().getMuares() != null){
			Hibernate.initialize(formCOM.getFormIN().getMuares().getDetalhes());
		}
		if (formCOM.getFormIN().getAves() != null){
			Hibernate.initialize(formCOM.getFormIN().getAves().getDetalhes());
		}
		if (formCOM.getFormIN().getAbelhas() != null){
			Hibernate.initialize(formCOM.getFormIN().getAbelhas().getDetalhes());
		}
		if (formCOM.getFormIN().getLagomorfos() != null){
			Hibernate.initialize(formCOM.getFormIN().getLagomorfos().getDetalhes());
		}
		if (formCOM.getFormIN().getOutrosAnimais() != null){
			Hibernate.initialize(formCOM.getFormIN().getOutrosAnimais().getDetalhes());
		}
		
		Hibernate.initialize(formCOM.getFormIN().getListaFormCOM());
		
		if (formCOM.getFormIN().getListaFormCOM() != null){
			for (FormCOM formCOM_I : formCOM.getFormIN().getListaFormCOM()) {
				Hibernate.initialize(formCOM_I.getBovinos());
				Hibernate.initialize(formCOM_I.getBubalinos());
				Hibernate.initialize(formCOM_I.getCaprinos());
				Hibernate.initialize(formCOM_I.getOvinos());
				Hibernate.initialize(formCOM_I.getSuinos());
				Hibernate.initialize(formCOM_I.getEquinos());
				Hibernate.initialize(formCOM_I.getAsininos());
				Hibernate.initialize(formCOM_I.getMuares());
				Hibernate.initialize(formCOM_I.getAves());
				Hibernate.initialize(formCOM_I.getAbelhas());
				Hibernate.initialize(formCOM_I.getLagomorfos());
				Hibernate.initialize(formCOM_I.getOutrosAnimais());
				
				
				if (formCOM_I.getBovinos() != null){
					Hibernate.initialize(formCOM_I.getBovinos().getDetalhes());
				}
				if (formCOM_I.getBubalinos() != null){
					Hibernate.initialize(formCOM_I.getBubalinos().getDetalhes());
				}
				if (formCOM_I.getCaprinos() != null){
					Hibernate.initialize(formCOM_I.getCaprinos().getDetalhes());
				}
				if (formCOM_I.getOvinos() != null){
					Hibernate.initialize(formCOM_I.getOvinos().getDetalhes());
				}
				if (formCOM_I.getSuinos() != null){
					Hibernate.initialize(formCOM_I.getSuinos().getDetalhes());
				}
				if (formCOM_I.getEquinos() != null){
					Hibernate.initialize(formCOM_I.getEquinos().getDetalhes());
				}
				if (formCOM_I.getAsininos() != null){
					Hibernate.initialize(formCOM_I.getAsininos().getDetalhes());
				}
				if (formCOM_I.getMuares() != null){
					Hibernate.initialize(formCOM_I.getMuares().getDetalhes());
				}
				if (formCOM_I.getAves() != null){
					Hibernate.initialize(formCOM_I.getAves().getDetalhes());
				}
				if (formCOM_I.getAbelhas() != null){
					Hibernate.initialize(formCOM_I.getAbelhas().getDetalhes());
				}
				if (formCOM_I.getLagomorfos() != null){
					Hibernate.initialize(formCOM_I.getLagomorfos().getDetalhes());
				}
				if (formCOM_I.getOutrosAnimais() != null){
					Hibernate.initialize(formCOM_I.getOutrosAnimais().getDetalhes());
				}
			}
		}
		
		return formCOM;
	}
	
	@SuppressWarnings("unchecked")
	public List<FormCOM> findAllBy(FormIN formIN){
		List<FormCOM> listFormCOM;
		
		StringBuilder sql = new StringBuilder();
		sql.append("select entity ")
		   .append("  from FormCOM entity ")
		   .append(" where entity.formIN = :formIN ");
		
		Query query = getSession().createQuery(sql.toString()).setParameter("formIN", formIN);
		listFormCOM = query.list();
		
		return listFormCOM;
	}
	
	@Override
	public void saveOrUpdate(FormCOM formCOM) {
		formCOM = (FormCOM) this.getSession().merge(formCOM);
		
		super.saveOrUpdate(formCOM);
		
		log.info("Salvando Form COM {}", formCOM.getId());
	}
	
	@Override
	public void delete(FormCOM formCOM) {
		super.delete(formCOM);
		
		log.info("Removendo Form COM {}", formCOM.getId());
	}
	
	@Override
	public void validar(FormCOM FormCOM) {

	}

	@Override
	public void validarPersist(FormCOM FormCOM) {

	}

	@Override
	public void validarMerge(FormCOM FormCOM) {

	}

	@Override
	public void validarDelete(FormCOM FormCOM) {

	}

}
