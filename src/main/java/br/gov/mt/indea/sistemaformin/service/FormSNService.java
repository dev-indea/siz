package br.gov.mt.indea.sistemaformin.service;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.mt.indea.sistemaformin.entity.AlteracaoSindromeNeurologica;
import br.gov.mt.indea.sistemaformin.entity.FormSN;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class FormSNService extends PaginableService<FormSN, Long> {

	private static final Logger log = LoggerFactory.getLogger(FormSNService.class);
	
	protected FormSNService() {
		super(FormSN.class);
	}
	
	public FormSN findByIdFetchAll(Long id){
		FormSN formSN;
		
		StringBuilder sql = new StringBuilder();
		sql.append("  from FormSN formSN ")
		   .append("  join fetch formSN.formIN formin")
		   .append("  left join fetch formSN.formCOM")
		   .append("  join fetch formSN.propriedade propriedade")
		   .append("  join fetch propriedade.municipio municipio")
		   .append("  join fetch municipio.uf")
		   .append("  join fetch propriedade.ule")
		   .append("  left join fetch propriedade.coordenadaGeografica ")
		   
		   .append("  left join fetch formin.proprietario proprietario")
		   .append("  left join fetch proprietario.endereco")
		   
		   .append("  join fetch formSN.veterinarioColheita vet")
		   .append("  join fetch vet.conselho")
		   .append("  join fetch vet.tipoEmitente")
		   .append("  left join fetch vet.ule uu")
		   .append("  left join fetch uu.urs")
		   .append("  left join fetch vet.endereco")
		   .append(" where formSN.id = :id ");
		
		Query query = getSession().createQuery(sql.toString()).setLong("id", id);
		formSN = (FormSN) query.uniqueResult();
		
		if (formSN != null){
		
			Hibernate.initialize(formSN.getMetodosParaEstipularIdade());
			Hibernate.initialize(formSN.getOutrasVacinas());
			Hibernate.initialize(formSN.getListaAlteracoesSindromeNeurologica());
			Hibernate.initialize(formSN.getTipoAmostra());
			Hibernate.initialize(formSN.getMeioConservacaoAmostra());
			
			if (formSN.getListaAlteracoesSindromeNeurologica() != null)
				for (AlteracaoSindromeNeurologica alteracao : formSN.getListaAlteracoesSindromeNeurologica()) {
					Hibernate.initialize(alteracao.getListaTipoDisturbioSindromeNeurologica());
				}
			
		}
		
		return formSN;
	}
	
	@Override
	public void saveOrUpdate(FormSN formSN) {
		formSN = (FormSN) this.getSession().merge(formSN);
		super.saveOrUpdate(formSN);
		
		log.info("Salvando Form SN {}", formSN.getId());
	}
	
	@Override
	public void delete(FormSN formSN) {
		super.delete(formSN);
		
		log.info("Removendo Form SN {}", formSN.getId());
	}
	
	@Override
	public void validar(FormSN FormSN) {

	}

	@Override
	public void validarPersist(FormSN FormSN) {

	}

	@Override
	public void validarMerge(FormSN FormSN) {

	}

	@Override
	public void validarDelete(FormSN FormSN) {

	}

}
