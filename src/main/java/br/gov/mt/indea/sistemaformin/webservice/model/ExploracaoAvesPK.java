package br.gov.mt.indea.sistemaformin.webservice.model;

import java.io.Serializable;

public class ExploracaoAvesPK implements Serializable {

    private static final long serialVersionUID = 3134341215472006981L;

	private Integer exploracao;
    
    private Especie especie;

    public ExploracaoAvesPK() {
    }

    public Integer getExploracao() {
        return exploracao;
    }

    public void setExploracao(Integer exploracao) {
        this.exploracao = exploracao;
    }

    public Especie getEspecie() {
        return especie;
    }

    public void setEspecie(Especie especie) {
        this.especie = especie;
    }

    @Override
    public int hashCode() {
        return exploracao.hashCode() + especie.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof ExploracaoAvesPK) {
            ExploracaoAvesPK exploracaoEquideo = (ExploracaoAvesPK) o;
            if (!exploracaoEquideo.getExploracao().equals(exploracao)) {
                return false;
            }
            if (!exploracaoEquideo.getEspecie().equals(especie)) {
                return false;
            }
            return true;
        }
        return false;
    }
}