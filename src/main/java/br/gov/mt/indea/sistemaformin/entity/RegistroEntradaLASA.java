package br.gov.mt.indea.sistemaformin.entity;

import java.util.Calendar;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

import br.gov.mt.indea.sistemaformin.enums.Dominio.AcompanhaNaoAcompanha;
import br.gov.mt.indea.sistemaformin.enums.Dominio.CondicaoMaterial;
import br.gov.mt.indea.sistemaformin.enums.Dominio.Especie;
import br.gov.mt.indea.sistemaformin.webservice.entity.Veterinario;

@Entity
@Table(name = "registro_entrada_lasa")
@Audited
public class RegistroEntradaLASA extends BaseEntity<Long>{

	private static final long serialVersionUID = 6945160424281951286L;
	
	@Id
	@GeneratedValue(generator = "registro_entrada_lasa_seq", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "registro_entrada_lasa_seq", sequenceName = "registro_entrada_lasa_seq", allocationSize = 1)
	private Long id;
	
	@Column(name = "nomero_registro")
	private Long numeroRegistro;
	
	@Column(name = "data_cadastro")
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar dataCadastro;
	
	@Column(name = "data_entrada")
	@Temporal(TemporalType.DATE)
	private Calendar dataEntrada;
	
	@Column(name = "nome_proprietario", length = 400)
	private String nomeProprietario;
	
	@ManyToOne
	@JoinColumn(name = "id_municipio")
	private Municipio municipio;
	
	@Column(name = "numero_exame")
	private Long numeroExame;
	
	@Enumerated(EnumType.STRING)
	private Especie especie;
	
	@ManyToOne(cascade={CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
	@JoinColumn(name = "id_tipo_exame")
	private TipoExame tipoExame;
	
	@Enumerated(EnumType.STRING)
	@Column(name="condicaoMaterial")
    private CondicaoMaterial condicaoMaterial;
	
	@Enumerated(EnumType.STRING)
	@Column(name="acompanhaNaoAcompanha")
    private AcompanhaNaoAcompanha formularioAcompanhaAmostra;
	
	@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "id_veterinario")
	private Veterinario veterinario;
	
	@OneToOne(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
	@JoinColumn(name="id_registro_saida_lasa")
	private RegistroSaidaLASA registroSaidaLASA;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_formin")
	private FormIN formIN;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_visita_propriedade_rural")
	private VisitaPropriedadeRural visitaPropriedadeRural;
	
	public Long getNumeroRegistro() {
		return numeroRegistro;
	}

	public void setNumeroRegistro(Long numeroRegistro) {
		this.numeroRegistro = numeroRegistro;
	}

	public Calendar getDataEntrada() {
		return dataEntrada;
	}

	public void setDataEntrada(Calendar dataEntrada) {
		this.dataEntrada = dataEntrada;
	}

	public String getNomeProprietario() {
		return nomeProprietario;
	}

	public void setNomeProprietario(String nomeProprietario) {
		this.nomeProprietario = nomeProprietario;
	}

	public Municipio getMunicipio() {
		return municipio;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}

	public Long getNumeroExame() {
		return numeroExame;
	}

	public void setNumeroExame(Long numeroExame) {
		this.numeroExame = numeroExame;
	}

	public Especie getEspecie() {
		return especie;
	}

	public void setEspecie(Especie especie) {
		this.especie = especie;
	}

	public TipoExame getTipoExame() {
		return tipoExame;
	}

	public void setTipoExame(TipoExame tipoExame) {
		this.tipoExame = tipoExame;
	}

	public Veterinario getVeterinario() {
		return veterinario;
	}

	public void setVeterinario(Veterinario veterinario) {
		this.veterinario = veterinario;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public CondicaoMaterial getCondicaoMaterial() {
		return condicaoMaterial;
	}

	public void setCondicaoMaterial(CondicaoMaterial condicaoMaterial) {
		this.condicaoMaterial = condicaoMaterial;
	}

	public AcompanhaNaoAcompanha getFormularioAcompanhaAmostra() {
		return formularioAcompanhaAmostra;
	}

	public void setFormularioAcompanhaAmostra(AcompanhaNaoAcompanha formularioAcompanhaAmostra) {
		this.formularioAcompanhaAmostra = formularioAcompanhaAmostra;
	}

	public Calendar getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Calendar dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public RegistroSaidaLASA getRegistroSaidaLASA() {
		return registroSaidaLASA;
	}

	public void setRegistroSaidaLASA(RegistroSaidaLASA registroSaidaLASA) {
		this.registroSaidaLASA = registroSaidaLASA;
	}

	public FormIN getFormIN() {
		return formIN;
	}

	public void setFormIN(FormIN formIN) {
		this.formIN = formIN;
	}

	public VisitaPropriedadeRural getVisitaPropriedadeRural() {
		return visitaPropriedadeRural;
	}

	public void setVisitaPropriedadeRural(VisitaPropriedadeRural visitaPropriedadeRural) {
		this.visitaPropriedadeRural = visitaPropriedadeRural;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RegistroEntradaLASA other = (RegistroEntradaLASA) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
