package br.gov.mt.indea.sistemaformin.managedbeans;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;

import br.gov.mt.indea.sistemaformin.entity.Municipio;
import br.gov.mt.indea.sistemaformin.entity.Pessoa;
import br.gov.mt.indea.sistemaformin.entity.UF;
import br.gov.mt.indea.sistemaformin.service.MunicipioService;
import br.gov.mt.indea.sistemaformin.service.TipoLogradouroService;
import br.gov.mt.indea.sistemaformin.util.FacesMessageUtil;
import br.gov.mt.indea.sistemaformin.webservice.client.CepService;
import br.gov.mt.indea.sistemaformin.webservice.entity.Endereco;
import br.gov.mt.indea.sistemaformin.webservice.entity.TipoLogradouro;

public abstract class PessoaManagedBean implements Serializable {

	private static final long serialVersionUID = 2716429629317891472L;

	private List<TipoLogradouro> listaTipoLogradouro;
	
	private UF uf;
	
	private String cep;
	
	@Inject
	private MunicipioService municipioService;
	
	@Inject
	private CepService cepService;
	
	@Inject
	private TipoLogradouroService tipoLogradouroService;
	
	@PostConstruct
	public void init(){
		this.listaTipoLogradouro = this.tipoLogradouroService.findAll();
	}
	
	public List<Municipio> getListaMunicipios(){
		if (this.getUf() == null)
			return null;
		
		return municipioService.findAllByUF(this.getUf());
	}
	
	public void buscarCEP(){
		if (this.getPessoa() == null)
			return;
		
		if (StringUtils.isEmpty(this.cep))
			return;
		
		if (this.cep.equals("_____-___"))
			return;
		
		Endereco endereco = cepService.buscarCep(cep);
		
		if (endereco == null){
			FacesMessageUtil.addWarnContextFacesMessage("Nenhum endere�o encontrado", "");
			this.getPessoa().setEndereco(null);
			this.uf = null;
		}else{
			if (this.getPessoa().getEndereco() == null)
				this.getPessoa().setEndereco(new Endereco());
			
			if (endereco.getMunicipio() != null)
				this.uf = endereco.getMunicipio().getUf();
			
			if (this.getPessoa().getEndereco() == null)
				this.getPessoa().setEndereco(endereco);
			else {
				this.getPessoa().getEndereco().setCep(endereco.getCep());
				this.getPessoa().getEndereco().setBairro(endereco.getBairro());
				this.getPessoa().getEndereco().setComplemento(endereco.getComplemento());
				this.getPessoa().getEndereco().setLogradouro(endereco.getLogradouro());
				this.getPessoa().getEndereco().setMunicipio(endereco.getMunicipio());
			}
			
			String[] logradouro = endereco.getLogradouro().split(" ");
			
			for (TipoLogradouro tipoLogradouro : listaTipoLogradouro) {
				if (StringUtils.lowerCase(tipoLogradouro.getNome()).equals(StringUtils.lowerCase(logradouro[0]))){
					this.getPessoa().getEndereco().setTipoLogradouro(tipoLogradouro);
					break;
				}
			}
		}
		
	}
	
	public abstract Pessoa getPessoa();
	
	public abstract void setPessoa(Pessoa pessoa);
	
	public List<TipoLogradouro> getListaTipoLogradouro() {
		return listaTipoLogradouro;
	}

	public void setListaTipoLogradouro(List<TipoLogradouro> listaTipoLogradouro) {
		this.listaTipoLogradouro = listaTipoLogradouro;
	}

	public UF getUf() {
		return uf;
	}

	public void setUf(UF uf) {
		this.uf = uf;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

}
