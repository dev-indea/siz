package br.gov.mt.indea.sistemaformin.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import org.hibernate.envers.Audited;

@Audited
@Entity(name="info_animais_form_com")
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="tipo", discriminatorType=DiscriminatorType.STRING)
public abstract class AbstractInformacoesDeAnimais_FormCOM extends BaseEntity<Long>{
	
	protected abstract List<AbstractDetalhesInformacoesDeAnimais_FormCOM> getDetalhesInformacoesDeAnimais();
	
	private static final long serialVersionUID = -3979755566569039862L;

	@Id
	@SequenceGenerator(name = "info_animais_form_com_seq", sequenceName="info_animais_form_com_seq", allocationSize=1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "info_animais_form_com_seq")
	private Long id;

	@Column(insertable=false, updatable=false)
    private String tipo;
	
	@OneToOne
	@JoinColumn(name = "id_formcom")
	private FormCOM formCOM;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public FormCOM getFormCOM() {
		return formCOM;
	}

	public void setFormCOM(FormCOM formCOM) {
		this.formCOM = formCOM;
	}

	public Long getTotalQuantidadeNoDiaDaInspecao() {
		Long total = null;
		if (this.getDetalhesInformacoesDeAnimais() != null)
			for (AbstractDetalhesInformacoesDeAnimais_FormCOM detalhes : this.getDetalhesInformacoesDeAnimais()) {
				if (detalhes.getQuantidadeNoDiaDaInspecao() != null){
					if (total == null)
						total = new Long(0);
					total += detalhes.getQuantidadeNoDiaDaInspecao();
				}
			}

		return total;
	}
	
	public Long getTotalQuantidadeNovosCasosConfirmados() {
		Long total = null;
		if (this.getDetalhesInformacoesDeAnimais() != null)
			for (AbstractDetalhesInformacoesDeAnimais_FormCOM detalhes : this.getDetalhesInformacoesDeAnimais()) {
				if (detalhes.getQuantidadeNovosCasosConfirmados() != null){
					if (total == null)
						total = new Long(0);
					total += detalhes.getQuantidadeNovosCasosConfirmados();
				}
			}

		return total;
	}
	
	public Long getTotalAcumuladoQuantidadeNovosCasosConfirmados() {
		Long total = null;
		if (this.getDetalhesInformacoesDeAnimais() != null)
			for (AbstractDetalhesInformacoesDeAnimais_FormCOM detalhes : this.getDetalhesInformacoesDeAnimais()) {
				if (detalhes.getAcumuladoQuantidadeNovosCasosConfirmados() != null){
					if (total == null)
						total = new Long(0);
					total += detalhes.getAcumuladoQuantidadeNovosCasosConfirmados();
				}
			}

		return total;
	}
	
	public Long getTotalQuantidadeNovosCasosProvaveis() {
		Long total = null;
		if (this.getDetalhesInformacoesDeAnimais() != null)
			for (AbstractDetalhesInformacoesDeAnimais_FormCOM detalhes : this.getDetalhesInformacoesDeAnimais()) {
				if (detalhes.getQuantidadeNovosCasosProvaveis() != null){
					if (total == null)
						total = new Long(0);
					total += detalhes.getQuantidadeNovosCasosProvaveis();
				}
			}

		return total;
	}
	
	public Long getTotalAcumuladoQuantidadeNovosCasosProvaveis() {
		Long total = null;
		if (this.getDetalhesInformacoesDeAnimais() != null)
			for (AbstractDetalhesInformacoesDeAnimais_FormCOM detalhes : this.getDetalhesInformacoesDeAnimais()) {
				if (detalhes.getAcumuladoQuantidadeNovosCasosProvaveis() != null){
					if (total == null)
						total = new Long(0);
					total += detalhes.getAcumuladoQuantidadeNovosCasosProvaveis();
				}
			}

		return total;
	}
	
	public Long getTotalQuantidadeNovosMortos() {
		Long total = null;
		if (this.getDetalhesInformacoesDeAnimais() != null)
			for (AbstractDetalhesInformacoesDeAnimais_FormCOM detalhes : this.getDetalhesInformacoesDeAnimais()) {
				if (detalhes.getQuantidadeNovosMortos() != null){
					if (total == null)
						total = new Long(0);
					total += detalhes.getQuantidadeNovosMortos();
				}
			}

		return total;
	}
	
	public Long getTotalAcumuladoQuantidadeNovosMortos() {
		Long total = null;
		if (this.getDetalhesInformacoesDeAnimais() != null)
			for (AbstractDetalhesInformacoesDeAnimais_FormCOM detalhes : this.getDetalhesInformacoesDeAnimais()) {
				if (detalhes.getAcumuladoQuantidadeNovosMortos() != null){
					if (total == null)
						total = new Long(0);
					total += detalhes.getAcumuladoQuantidadeNovosMortos();
				}
			}

		return total;
	}
	
	public Long getTotalQuantidadeNovosAbatidos() {
		Long total = null;
		if (this.getDetalhesInformacoesDeAnimais() != null)
			for (AbstractDetalhesInformacoesDeAnimais_FormCOM detalhes : this.getDetalhesInformacoesDeAnimais()) {
				if (detalhes.getQuantidadeNovosAbatidos() != null){
					if (total == null)
						total = new Long(0);
					total += detalhes.getQuantidadeNovosAbatidos();
				}
			}

		return total;
	}
	
	public Long getTotalAcumuladoQuantidadeNovosAbatidos() {
		Long total = null;
		if (this.getDetalhesInformacoesDeAnimais() != null)
			for (AbstractDetalhesInformacoesDeAnimais_FormCOM detalhes : this.getDetalhesInformacoesDeAnimais()) {
				if (detalhes.getAcumuladoQuantidadeNovosAbatidos() != null){
					if (total == null)
						total = new Long(0);
					total += detalhes.getAcumuladoQuantidadeNovosAbatidos();
				}
			}

		return total;
	}
	
	public Long getTotalQuantidadeNovosDestruidos() {
		Long total = null;
		if (this.getDetalhesInformacoesDeAnimais() != null)
			for (AbstractDetalhesInformacoesDeAnimais_FormCOM detalhes : this.getDetalhesInformacoesDeAnimais()) {
				if (detalhes.getQuantidadeNovosDestruidos() != null){
					if (total == null)
						total = new Long(0);
					total += detalhes.getQuantidadeNovosDestruidos();
				}
			}

		return total;
	}
	
	public Long getTotalAcumuladoQuantidadeNovosDestruidos() {
		Long total = null;
		if (this.getDetalhesInformacoesDeAnimais() != null)
			for (AbstractDetalhesInformacoesDeAnimais_FormCOM detalhes : this.getDetalhesInformacoesDeAnimais()) {
				if (detalhes.getAcumuladoQuantidadeNovosDestruidos() != null){
					if (total == null)
						total = new Long(0);
					total += detalhes.getAcumuladoQuantidadeNovosDestruidos();
				}
			}

		return total;
	}
	
	public Long getTotalQuantidadeExaminados() {
		Long total = null;
		if (this.getDetalhesInformacoesDeAnimais() != null)
			for (AbstractDetalhesInformacoesDeAnimais_FormCOM detalhes : this.getDetalhesInformacoesDeAnimais()) {
				if (detalhes.getQuantidadeExaminados() != null){
					if (total == null)
						total = new Long(0);
					total += detalhes.getQuantidadeExaminados();
				}
			}

		return total;
	}
	
	public Long getTotalQuantidadeAnimaisIngressos() {
		Long total = null;
		if (this.getDetalhesInformacoesDeAnimais() != null)
			for (AbstractDetalhesInformacoesDeAnimais_FormCOM detalhes : this.getDetalhesInformacoesDeAnimais()) {
				if (detalhes.getQuantidadeAnimaisIngressos() != null){
					if (total == null)
						total = new Long(0);
					total += detalhes.getQuantidadeAnimaisIngressos();
				}
			}

		return total;
	}
	
	public Long getTotalQuantidadeAnimaisEgressos() {
		Long total = null;
		if (this.getDetalhesInformacoesDeAnimais() != null)
			for (AbstractDetalhesInformacoesDeAnimais_FormCOM detalhes : this.getDetalhesInformacoesDeAnimais()) {
				if (detalhes.getQuantidadeAnimaisEgressos() != null){
					if (total == null)
						total = new Long(0);
					total += detalhes.getQuantidadeAnimaisEgressos();
				}
			}

		return total;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbstractInformacoesDeAnimais_FormCOM other = (AbstractInformacoesDeAnimais_FormCOM) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
