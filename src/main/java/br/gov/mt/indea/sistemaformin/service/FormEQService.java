package br.gov.mt.indea.sistemaformin.service;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.mt.indea.sistemaformin.entity.AvaliacaoClinicaEQ;
import br.gov.mt.indea.sistemaformin.entity.FormEQ;
import br.gov.mt.indea.sistemaformin.entity.NovoEstabelecimentoParaInvestigacao;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class FormEQService extends PaginableService<FormEQ, Long> {

	private static final Logger log = LoggerFactory.getLogger(FormEQService.class);
	
	protected FormEQService() {
		super(FormEQ.class);
	}
	
	public FormEQ findByIdFetchAll(Long id){
		FormEQ formEQ;
		
		StringBuilder sql = new StringBuilder();
		sql.append("  from FormEQ formEQ ")
		   .append("  join fetch formEQ.formIN formin")
		   .append("  left join fetch formEQ.formCOM")
		   .append("  join fetch formin.propriedade propriedade")
		   .append("  join fetch formin.proprietario proprietario")
		   .append("  left join fetch proprietario.endereco")
		   .append("  join fetch propriedade.municipio mun")
		   .append("  join fetch mun.uf")
		   .append("  join fetch propriedade.ule")
		   .append("  left join fetch formEQ.veterinarioAssistenteDoEstabelecimento veterinarioAssistenteDoEstabelecimento ")
		   .append("  left join fetch veterinarioAssistenteDoEstabelecimento.endereco ")
		   .append("  join fetch formEQ.veterinarioResponsavelPeloAtendimento vet")
		   .append("  join fetch vet.conselho")
		   .append("  join fetch vet.tipoEmitente")
		   .append("  left join fetch vet.ule uu")
		   .append("  left join fetch uu.urs")
		   .append("  left join fetch vet.endereco")
		   .append(" where formEQ.id = :id ");
		
		Query query = getSession().createQuery(sql.toString()).setLong("id", id);
		formEQ = (FormEQ) query.uniqueResult();
		
		if (formEQ != null){
		
			Hibernate.initialize(formEQ.getAvaliacoesClinicas());
			if (formEQ.getAvaliacoesClinicas() != null){
				for (AvaliacaoClinicaEQ avaliacao : formEQ.getAvaliacoesClinicas()) {
					Hibernate.initialize(avaliacao.getSinaisClinicosCavidadeNasal());
					Hibernate.initialize(avaliacao.getSinaisClinicosPele());
					Hibernate.initialize(avaliacao.getDoencasRespiratorias());
					Hibernate.initialize(avaliacao.getVacinacoesEQ());
				}
			}
			
			Hibernate.initialize(formEQ.getManejoDosAnimais());
			Hibernate.initialize(formEQ.getNovoEstabelecimentoParaInvestigacaos());
			
			if (formEQ.getNovoEstabelecimentoParaInvestigacaos() != null)
				for (NovoEstabelecimentoParaInvestigacao estabelecimento : formEQ.getNovoEstabelecimentoParaInvestigacaos()) {
					Hibernate.initialize(estabelecimento.getPropriedade());
					Hibernate.initialize(estabelecimento.getPropriedade().getEndereco());
					Hibernate.initialize(estabelecimento.getPropriedade().getMunicipio());
					Hibernate.initialize(estabelecimento.getPropriedade().getUle());
					Hibernate.initialize(estabelecimento.getListaVinculoEpidemiologico());
				}
			
		}
		
		return formEQ;
	}
	
	@Override
	public void saveOrUpdate(FormEQ formEQ) {
		formEQ = (FormEQ) this.getSession().merge(formEQ);
		
		super.saveOrUpdate(formEQ);

		log.info("Salvando Form EQ {}", formEQ.getId());
	}
	
	@Override
	public void delete(FormEQ formEQ) {
		super.delete(formEQ);
		
		log.info("Removendo Form EQ {}", formEQ.getId());
	}
	
	@Override
	public void validar(FormEQ FormEQ) {

	}

	@Override
	public void validarPersist(FormEQ FormEQ) {

	}

	@Override
	public void validarMerge(FormEQ FormEQ) {

	}

	@Override
	public void validarDelete(FormEQ FormEQ) {

	}

}
