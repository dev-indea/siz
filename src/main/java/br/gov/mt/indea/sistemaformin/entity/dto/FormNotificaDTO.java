package br.gov.mt.indea.sistemaformin.entity.dto;

import java.io.Serializable;
import java.util.Date;

import org.springframework.util.ObjectUtils;

import br.gov.mt.indea.sistemaformin.entity.Municipio;
import br.gov.mt.indea.sistemaformin.entity.UF;
import br.gov.mt.indea.sistemaformin.enums.Dominio.SimNao;

public class FormNotificaDTO implements AbstractDTO, Serializable{
	
	private static final long serialVersionUID = 6205763755360605521L;

	private UF uf = UF.MATO_GROSSO;

	private Municipio municipio;
	
	private Date dataDaNotificacao;
    
    private SimNao anonimo;
    
    private String notificante;
    
    private String nomePropriedade;
    
    private Long codigoPropriedade;
    
    private Long id;
	
	public boolean isNull(){
		return ObjectUtils.isEmpty(this);
	}

	public Date getDataDaNotificacao() {
		return dataDaNotificacao;
	}

	public void setDataDaNotificacao(Date dataDaNotificacao) {
		this.dataDaNotificacao = dataDaNotificacao;
	}

	public SimNao getAnonimo() {
		return anonimo;
	}

	public void setAnonimo(SimNao anonimo) {
		this.anonimo = anonimo;
	}

	public String getNotificante() {
		return notificante;
	}

	public void setNotificante(String notificante) {
		this.notificante = notificante;
	}

	public String getNomePropriedade() {
		return nomePropriedade;
	}

	public void setNomePropriedade(String nomePropriedade) {
		this.nomePropriedade = nomePropriedade;
	}

	public Long getCodigoPropriedade() {
		return codigoPropriedade;
	}

	public void setCodigoPropriedade(Long codigoPropriedade) {
		this.codigoPropriedade = codigoPropriedade;
	}

	public UF getUf() {
		return uf;
	}

	public void setUf(UF uf) {
		this.uf = uf;
	}

	public Municipio getMunicipio() {
		return municipio;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}