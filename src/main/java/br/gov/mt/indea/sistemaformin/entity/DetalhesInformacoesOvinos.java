package br.gov.mt.indea.sistemaformin.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.envers.Audited;

@Audited
@Entity
@DiscriminatorValue("ovinos")
public class DetalhesInformacoesOvinos extends AbstractDetalhesInformacoesDeAnimais implements Cloneable{
	
	private static final long serialVersionUID = 1355044082455412390L;

	@ManyToOne
	@JoinColumn(name="id_informacoes_ovinos")
	private InformacoesOvinos informacoesOvinos;

	public InformacoesOvinos getInformacoesOvinos() {
		return informacoesOvinos;
	}

	public void setInformacoesOvinos(InformacoesOvinos informacoesOvinos) {
		this.informacoesOvinos = informacoesOvinos;
	}
	
	protected Object clone(InformacoesOvinos informacoesOvinos) throws CloneNotSupportedException{
		DetalhesInformacoesOvinos cloned = (DetalhesInformacoesOvinos) super.clone();
		
		cloned.setId(null);
		cloned.setInformacoesOvinos(informacoesOvinos);
		
		return cloned;
	}

}