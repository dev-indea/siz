package br.gov.mt.indea.sistemaformin.service;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.mt.indea.sistemaformin.entity.Municipio;
import br.gov.mt.indea.sistemaformin.entity.NumeroVigilanciaVeterinaria;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class NumeroVigilanciaVeterinariaService extends PaginableService<NumeroVigilanciaVeterinaria, Long> {

	private static final Logger log = LoggerFactory.getLogger(NumeroVigilanciaVeterinariaService.class);
	
	protected NumeroVigilanciaVeterinariaService() {
		super(NumeroVigilanciaVeterinaria.class);
	}
	
	public NumeroVigilanciaVeterinaria findByIdFetchAll(Long id){
		NumeroVigilanciaVeterinaria numeroVigilanciaVeterinaria;
		
		StringBuilder sql = new StringBuilder();
		sql.append("select numeroVigilanciaVeterinaria ")
		   .append("  from NumeroVigilanciaVeterinaria numeroVigilanciaVeterinaria ")
		   .append(" where numeroVigilanciaVeterinaria.id = :id ");
		
		Query query = getSession().createQuery(sql.toString()).setLong("id", id);
		numeroVigilanciaVeterinaria = (NumeroVigilanciaVeterinaria) query.uniqueResult();
		
		return numeroVigilanciaVeterinaria;
	}
	
	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	public NumeroVigilanciaVeterinaria getNumeroVigilanciaVeterinariaByMunicipio(Municipio municipio, int ano){
		NumeroVigilanciaVeterinaria numeroVigilanciaVeterinaria;
		
		StringBuilder sql = new StringBuilder();
		sql.append("  from NumeroVigilanciaVeterinaria entity ")
		   .append("  join fetch entity.municipio ")
		   .append(" where entity.municipio = :municipio ")
		   .append("   and entity.ano = :ano");
		
		Query query = getSession().createQuery(sql.toString()).setParameter("municipio", municipio).setInteger("ano", ano);
		numeroVigilanciaVeterinaria = (NumeroVigilanciaVeterinaria) query.uniqueResult();

		return numeroVigilanciaVeterinaria;
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	public void saveOrUpdate(NumeroVigilanciaVeterinaria numeroVigilanciaVeterinaria) {
		super.saveOrUpdate(numeroVigilanciaVeterinaria);
		
		log.info("Salvando Numero Vigilancia Veterinaria {}", numeroVigilanciaVeterinaria.getId());
	}
	
	@Override
	public void validar(NumeroVigilanciaVeterinaria NumeroVigilanciaVeterinaria) {

	}

	@Override
	public void validarPersist(NumeroVigilanciaVeterinaria NumeroVigilanciaVeterinaria) {

	}

	@Override
	public void validarMerge(NumeroVigilanciaVeterinaria NumeroVigilanciaVeterinaria) {

	}

	@Override
	public void validarDelete(NumeroVigilanciaVeterinaria NumeroVigilanciaVeterinaria) {

	}

}
