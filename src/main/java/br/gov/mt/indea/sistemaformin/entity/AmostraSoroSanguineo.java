package br.gov.mt.indea.sistemaformin.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.envers.Audited;

@Audited
@Entity
@DiscriminatorValue("soro sanguineo")
public class AmostraSoroSanguineo extends AbstractAmostra implements Serializable{
	
	private static final long serialVersionUID = 3534166665660362524L;
	
	@ManyToOne
	@JoinColumn(name="id_formLAB1")
	private FormLAB formLAB1;
	
	@Column(name="quantidade_soros")
	private Long quantidadeSoros;

	public Long getQuantidadeSoros() {
		return quantidadeSoros;
	}

	public FormLAB getFormLAB1() {
		return formLAB1;
	}

	public void setFormLAB1(FormLAB formLAB) {
		this.formLAB1 = formLAB;
	}

	public void setQuantidadeSoros(Long quantidadeSoros) {
		this.quantidadeSoros = quantidadeSoros;
	}

}
