package br.gov.mt.indea.sistemaformin.service;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.mt.indea.sistemaformin.entity.Municipio;
import br.gov.mt.indea.sistemaformin.entity.NumeroFormIN;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class NumeroFormINService extends PaginableService<NumeroFormIN, Long> {

	private static final Logger log = LoggerFactory.getLogger(NumeroFormINService.class);
	
	protected NumeroFormINService() {
		super(NumeroFormIN.class);
	}
	
	public NumeroFormIN findByIdFetchAll(Long id){
		NumeroFormIN numeroFormIN;
		
		StringBuilder sql = new StringBuilder();
		sql.append("select numeroFormIN ")
		   .append("  from NumeroFormIN numeroFormIN ")
		   .append(" where numeroFormIN.id = :id ");
		
		Query query = getSession().createQuery(sql.toString()).setLong("id", id);
		numeroFormIN = (NumeroFormIN) query.uniqueResult();
		
		return numeroFormIN;
	}
	
	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	public NumeroFormIN getNumeroFormINByMunicipio(Municipio municipio){
		NumeroFormIN numeroFormIN;
		
		StringBuilder sql = new StringBuilder();
		sql.append("  from NumeroFormIN entity ")
		   .append("  join fetch entity.municipio ")
		   .append(" where entity.municipio = :municipio ");
		
		Query query = getSession().createQuery(sql.toString()).setParameter("municipio", municipio);
		numeroFormIN = (NumeroFormIN) query.uniqueResult();

		return numeroFormIN;
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	public void saveOrUpdate(NumeroFormIN numeroFormIN) {
		super.saveOrUpdate(numeroFormIN);
		
		log.info("Salvando Numero Form IN {}", numeroFormIN.getId());
	}
	
	@Override
	public void validar(NumeroFormIN NumeroFormIN) {

	}

	@Override
	public void validarPersist(NumeroFormIN NumeroFormIN) {

	}

	@Override
	public void validarMerge(NumeroFormIN NumeroFormIN) {

	}

	@Override
	public void validarDelete(NumeroFormIN NumeroFormIN) {

	}

}
