package br.gov.mt.indea.sistemaformin.entity.dto;

import java.io.Serializable;
import java.util.Date;

import br.gov.mt.indea.sistemaformin.entity.Municipio;
import br.gov.mt.indea.sistemaformin.entity.UF;

public class FormINDTO implements AbstractDTO, Serializable {

	private static final long serialVersionUID = 958534186952468759L;

	private UF uf = UF.MATO_GROSSO;

	private Municipio municipio;

	private String numeroFormIN;

	private Date data;

	private String propriedade;
	
	public boolean isNull(){
		if (uf == null && municipio == null && numeroFormIN == null && data == null && propriedade == null)
			return true;
		return false;
	}

	public UF getUf() {
		return uf;
	}

	public void setUf(UF uf) {
		this.uf = uf;
	}

	public Municipio getMunicipio() {
		return municipio;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}

	public String getNumeroFormIN() {
		return numeroFormIN;
	}

	public void setNumeroFormIN(String numeroFormIN) {
		this.numeroFormIN = numeroFormIN;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public String getPropriedade() {
		return propriedade;
	}

	public void setPropriedade(String propriedade) {
		this.propriedade = propriedade;
	}
	
}
