package br.gov.mt.indea.sistemaformin.service;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.mt.indea.sistemaformin.entity.AbstractAmostra;
import br.gov.mt.indea.sistemaformin.entity.FormLAB;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class FormLABService extends PaginableService<FormLAB, Long> {

	private static final Logger log = LoggerFactory.getLogger(FormLABService.class);
	
	protected FormLABService() {
		super(FormLAB.class);
	}
	
	public FormLAB findByIdFetchAll(Long id){
		FormLAB formLAB;
		
		StringBuilder sql = new StringBuilder();
		sql.append("  from FormLAB formlab ")
		   .append("  join fetch formlab.formIN formin")
		   .append("  left join fetch formlab.formCOM")
		   .append("  join fetch formin.propriedade propriedade")
		   .append("  join fetch formin.proprietario proprietario")
		   .append("  left join fetch proprietario.endereco")
		   .append("  join fetch propriedade.municipio")
		   .append("  join fetch propriedade.ule")
		   .append("  join fetch formlab.veterinario vet")
		   .append("  join fetch vet.conselho")
		   .append("  join fetch vet.tipoEmitente")
		   .append("  left join fetch vet.ule uu")
		   .append("  left join fetch uu.urs")
		   .append("  left join fetch vet.endereco")
		   .append(" where formlab.id = :id ");
		
		Query query = getSession().createQuery(sql.toString()).setLong("id", id);
		formLAB = (FormLAB) query.uniqueResult();
		
		if (formLAB != null){
			
			Hibernate.initialize(formLAB.getAmostrasSoroSanguineo());
			if (formLAB.getAmostrasSoroSanguineo() != null)
				for (AbstractAmostra amostra : formLAB.getAmostrasSoroSanguineo()) {
					Hibernate.initialize(amostra.getInformacoesComplementaresEspeciesFormLAB());
					Hibernate.initialize(amostra.getInformacoesComplementaresMedicamentosFormLAB());
				}
			
			if (formLAB.getOutrasAmostras() != null)
				for (AbstractAmostra amostra : formLAB.getOutrasAmostras()) {
					Hibernate.initialize(amostra.getInformacoesComplementaresEspeciesFormLAB());
					Hibernate.initialize(amostra.getInformacoesComplementaresMedicamentosFormLAB());
				}
			
			Hibernate.initialize(formLAB.getOutrasAmostras());
			
		}
		
		return formLAB;
	}
	
	@Override
	public void saveOrUpdate(FormLAB formLAB) {
		formLAB = (FormLAB) this.getSession().merge(formLAB);
		super.saveOrUpdate(formLAB);
		
		log.info("Salvando Form LAB {}", formLAB.getId());
	}
	
	@Override
	public void delete(FormLAB formLAB) {
		super.delete(formLAB);
		
		log.info("Removendo Form LAB {}", formLAB.getId());
	}
	
	@Override
	public void validar(FormLAB FormLAB) {

	}

	@Override
	public void validarPersist(FormLAB FormLAB) {

	}

	@Override
	public void validarMerge(FormLAB FormLAB) {

	}

	@Override
	public void validarDelete(FormLAB FormLAB) {

	}

}
