package br.gov.mt.indea.sistemaformin.dao;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.inject.Inject;


import br.gov.mt.indea.sistemaformin.entity.FormMormo;
import br.gov.mt.indea.sistemaformin.exception.ApplicationException;

@Stateless
public class FormMormoDAO extends DAO<FormMormo> {
	
	@Inject
	private EntityManager em;
	
	public FormMormoDAO() {
		super(FormMormo.class);
	}

	@Override
	public EntityManager getEntityManager() {
		return this.em;
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void add(FormMormo formMormo) throws ApplicationException{
		formMormo = this.getEntityManager().merge(formMormo);
		if (formMormo.getFormCOM() != null)
			formMormo.setFormCOM(this.getEntityManager().merge(formMormo.getFormCOM()));
		super.add(formMormo);
	}

}
