package br.gov.mt.indea.sistemaformin.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.mt.indea.sistemaformin.entity.DoencaEnfermidadeAbatedouroFrigorifico;
import br.gov.mt.indea.sistemaformin.enums.Dominio.AtivoInativo;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class DoencaEnfermidadeAbatedouroFrigorificoService extends PaginableService<DoencaEnfermidadeAbatedouroFrigorifico, Long> {
	
	private static final Logger log = LoggerFactory.getLogger(DoencaEnfermidadeAbatedouroFrigorificoService.class);

	protected DoencaEnfermidadeAbatedouroFrigorificoService() {
		super(DoencaEnfermidadeAbatedouroFrigorifico.class);
	}
	
	public DoencaEnfermidadeAbatedouroFrigorifico findByIdFetchAll(Long id){
		DoencaEnfermidadeAbatedouroFrigorifico doencaEnfermidadeAbatedouroFrigorifico;
		
		StringBuilder sql = new StringBuilder();
		sql.append("select v ")
		   .append("  from DoencaEnfermidadeAbatedouroFrigorifico v ")
		   .append(" where v.id = :id ");
		
		Query query = getSession().createQuery(sql.toString()).setLong("id", id);
		doencaEnfermidadeAbatedouroFrigorifico = (DoencaEnfermidadeAbatedouroFrigorifico) query.uniqueResult();
		
		return doencaEnfermidadeAbatedouroFrigorifico;
	}
	
	@SuppressWarnings("unchecked")
	public List<DoencaEnfermidadeAbatedouroFrigorifico> findAllByStatus(AtivoInativo status) {
		List<DoencaEnfermidadeAbatedouroFrigorifico> doencaEnfermidadeAbatedouroFrigorifico;
		
		StringBuilder sql = new StringBuilder();
		sql.append("select v ")
		   .append("  from DoencaEnfermidadeAbatedouroFrigorifico v ")
		   .append(" where v.status = :status ");
		
		Query query = getSession().createQuery(sql.toString()).setParameter("status", status);
		doencaEnfermidadeAbatedouroFrigorifico = query.list();
		
		return doencaEnfermidadeAbatedouroFrigorifico;
	}
	
	@Override
	public void saveOrUpdate(DoencaEnfermidadeAbatedouroFrigorifico doencaEnfermidadeAbatedouroFrigorifico) {
		super.saveOrUpdate(doencaEnfermidadeAbatedouroFrigorifico);
		
		log.info("Salvando Tipo Chave Secundaria Visita Propriedade Rural {}", doencaEnfermidadeAbatedouroFrigorifico.getId());
	}
	
	@Override
	public void delete(DoencaEnfermidadeAbatedouroFrigorifico doencaEnfermidadeAbatedouroFrigorifico) {
		super.delete(doencaEnfermidadeAbatedouroFrigorifico);
		
		log.info("Removendo Tipo Chave Secundaria Visita Propriedade Rural {}", doencaEnfermidadeAbatedouroFrigorifico.getId());
	}
	
	@Override
	public List<DoencaEnfermidadeAbatedouroFrigorifico> findAll() {
		return super.findAll("nome");
	}
	
	@Override
	public void validar(DoencaEnfermidadeAbatedouroFrigorifico DoencaEnfermidadeAbatedouroFrigorifico) {

	}

	@Override
	public void validarPersist(DoencaEnfermidadeAbatedouroFrigorifico DoencaEnfermidadeAbatedouroFrigorifico) {

	}

	@Override
	public void validarMerge(DoencaEnfermidadeAbatedouroFrigorifico DoencaEnfermidadeAbatedouroFrigorifico) {

	}

	@Override
	public void validarDelete(DoencaEnfermidadeAbatedouroFrigorifico DoencaEnfermidadeAbatedouroFrigorifico) {

	}

}
