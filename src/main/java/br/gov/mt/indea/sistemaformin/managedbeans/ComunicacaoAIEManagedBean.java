package br.gov.mt.indea.sistemaformin.managedbeans;

import java.io.IOException;
import java.io.Serializable;
import java.util.Calendar;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLBeanName;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;

import br.gov.mt.indea.sistemaformin.entity.ComunicacaoAIE;
import br.gov.mt.indea.sistemaformin.entity.Laboratorio;
import br.gov.mt.indea.sistemaformin.entity.dto.ComunicacaoAIEDTO;
import br.gov.mt.indea.sistemaformin.enums.Dominio.Mes;
import br.gov.mt.indea.sistemaformin.exception.XlsxFillingException;
import br.gov.mt.indea.sistemaformin.exception.XlsxFormatException;
import br.gov.mt.indea.sistemaformin.security.UserSecurity;
import br.gov.mt.indea.sistemaformin.service.ComunicacaoAIEService;
import br.gov.mt.indea.sistemaformin.service.LaboratorioService;
import br.gov.mt.indea.sistemaformin.service.LazyObjectDataModel;
import br.gov.mt.indea.sistemaformin.util.DataUtil;
import br.gov.mt.indea.sistemaformin.util.FacesMessageUtil;
import br.gov.mt.indea.sistemaformin.xlsximport.ErroImportacao;
import br.gov.mt.indea.sistemaformin.xlsximport.ImportComunicacaoAIE;

@Named
@ViewScoped
@URLBeanName("comunicacaoAIEManagedBean")
@URLMappings(mappings = {
		@URLMapping(id = "importarComunicacaoAIE", pattern = "/comunicacaoAIE/importar", viewId = "/pages/comunicacaoAIE/comunicacaoAIE.jsf"),
		@URLMapping(id = "pesquisarComunicacaoAIE", pattern = "/comunicacaoAIE/pesquisar", viewId = "/pages/comunicacaoAIE/lista.jsf")})
public class ComunicacaoAIEManagedBean implements Serializable{

	private static final long serialVersionUID = -7945428844323041678L;
	
	@Inject
	private ComunicacaoAIEDTO comunicacaoAIEDTO;
	
	private Mes mes;

	private int ano;
	
	private LazyObjectDataModel<ComunicacaoAIE> listaComunicacaoAIEBusca;
	
	List<ErroImportacao> listaErros;
	
	private List<Laboratorio> listaLaboratorio;
	
	List<ComunicacaoAIE> listaComunicacaoAIEPreExistentes;
	
	@Inject
	ImportComunicacaoAIE importComunicacaoAIE;
	
	@Inject
	UserSecurity userSecurity;
	
	@Inject
	ComunicacaoAIEService comunicacaoAIEService;
	
	@Inject
	private LaboratorioService laboratorioService;

	ComunicacaoAIE comunicacaoAIE;
	
	@Inject
	LoginManagedBean loginManagedBean;
	
	@URLAction(mappingId = "pesquisarComunicacaoAIE", onPostback = false)
	public void pesquisar(){
		limpar();

		this.buscarLaboratorios();
		
		if (loginManagedBean.getUserSecurity().hasAuthorityAdmin())
			return;
		else if (loginManagedBean.getUserSecurity().getUsuario().getLaboratorio() == null)
			FacesMessageUtil.addErrorContextFacesMessage("O usu�rio logado n�o possui um laborat�rio vinculado", "");
		else
			this.comunicacaoAIEDTO.setLaboratorio(loginManagedBean.getUserSecurity().getUsuario().getLaboratorio());
	}
	
	@URLAction(mappingId = "importarComunicacaoAIE", onPostback = false)
	public void iniciarImport(){
		limpar();
	}
	
	public void limpar(){
		this.comunicacaoAIE = null;
		this.listaErros = null;
		this.listaLaboratorio = null;
		this.listaComunicacaoAIEPreExistentes = null;
		this.listaComunicacaoAIEBusca = null;
	}
	
	public void buscarLaboratorios(){
		listaLaboratorio = laboratorioService.findAll();
	}
	
	public void buscarComunicacaoAIE(){
		this.comunicacaoAIEDTO.setDataComunicacao(null);
		
		if (mes == null ^ ano == 0){
			FacesContext context = FacesContext.getCurrentInstance();
			
			context.addMessage("formListaComunicacaoAIEs:fieldDataComunicacao:comboMes", new FacesMessage(FacesMessage.SEVERITY_ERROR, "M�s e ano: os dois campos devem ser preenchidos", "M�s e ano: os dois campos devem ser preenchidos"));
			return;
		} else if (mes != null && ano != 0){
			Calendar c = Calendar.getInstance();
			c.set(Calendar.DAY_OF_MONTH, 1);
			c.set(Calendar.MONTH, mes.getOrdem() - 1);
			c.set(Calendar.YEAR, ano);
			
			this.comunicacaoAIEDTO.setDataComunicacao(c.getTime());
		}
		
		listaComunicacaoAIEBusca = new LazyObjectDataModel<ComunicacaoAIE>(this.comunicacaoAIEService, this.comunicacaoAIEDTO);
	}
	
	public void buscarDetalhesComunicacaoAIE(ComunicacaoAIE comunicacaoAIE){
		this.comunicacaoAIE = comunicacaoAIE;
		
		this.comunicacaoAIEService.attachDetalhes(comunicacaoAIE);
	}
	
	public void handleFileUpload(FileUploadEvent event) {
		UploadedFile file;
		Laboratorio laboratorio;
		
		file = event.getFile();
		
		if (file == null){
			FacesMessageUtil.addErrorContextFacesMessage("Erro", "N�o foi encontrado nenhum arquivo para importar");
			return ;
		}
		
		laboratorio = userSecurity.getUsuario().getLaboratorio();
		
		if (laboratorio == null){
			FacesMessageUtil.addErrorContextFacesMessage("Erro", "O usu�rio " + this.userSecurity.getUsuario().getNome() + " n�o possui Laborat�rio vinculado");
			return;
		}
		
		try {
			comunicacaoAIE = this.importFile(file, laboratorio);
			
			if (comunicacaoAIE != null){
				listaComunicacaoAIEPreExistentes = comunicacaoAIEService.findAll(comunicacaoAIE.getLaboratorio(), comunicacaoAIE.getDataComunicacao().getTime());
				
				if (listaComunicacaoAIEPreExistentes != null && !listaComunicacaoAIEPreExistentes.isEmpty()){
					FacesMessageUtil.addWarnContextFacesMessage("ATEN��O", "Foi encontrada uma comunica��o para o laborat�rio " + comunicacaoAIE.getLaboratorio().getNome() + ", do m�s " + DataUtil.getCalendar_MM_yyyy(comunicacaoAIE.getDataCadastro()));
					FacesMessageUtil.addWarnContextFacesMessage("ATEN��O", "Ao clicar em salvar, a comunica��o mais antiga ser� sobrescrita pela atual");
				}
				
			}
		} catch (IOException e) {
			FacesMessageUtil.addErrorContextFacesMessage("Erro", "Ocorreu um erro ao importar o arquivo " + file.getFileName());
		}
        
    }
	
	private ComunicacaoAIE importFile(UploadedFile file, Laboratorio laboratorio) throws IOException{
		XSSFWorkbook workbook;
		
		workbook = new XSSFWorkbook(file.getInputstream());
	
        this.listaErros = null;
    
        try {
        	comunicacaoAIE = importComunicacaoAIE.importFile(workbook, laboratorio);
        } catch (XlsxFormatException e) {
        	FacesMessageUtil.addErrorContextFacesMessage("Erro ao importar o arquivo", e.getMensagem());
		} catch (XlsxFillingException e) {
			FacesMessageUtil.addErrorContextFacesMessage("Erro ao importar o arquivo", e.getMensagem());
		} finally {
			this.listaErros = importComunicacaoAIE.getListaErros();
			
        	workbook.close();
		}
		
		return comunicacaoAIE;
	}
	
	public String salvarImportacao(){
		if (listaComunicacaoAIEPreExistentes != null && !listaComunicacaoAIEPreExistentes.isEmpty())
			comunicacaoAIEService.delete(listaComunicacaoAIEPreExistentes);
		
		if (comunicacaoAIE != null){
    		comunicacaoAIEService.saveOrUpdate(comunicacaoAIE);
    		
    		FacesMessageUtil.addInfoContextFacesMessage("Comunica��o de AIE salva com sucesso!", "");
    		return "pretty:pesquisarComunicacaoAIE";
    	}
		
		return "";
	}

	public List<ErroImportacao> getListaErros() {
		return listaErros;
	}

	public void setListaErros(List<ErroImportacao> listaErros) {
		this.listaErros = listaErros;
	}

	public LazyObjectDataModel<ComunicacaoAIE> getListaComunicacaoAIEBusca() {
		return listaComunicacaoAIEBusca;
	}

	public void setListaComunicacaoAIEBusca(LazyObjectDataModel<ComunicacaoAIE> listaComunicacaoAIEBusca) {
		this.listaComunicacaoAIEBusca = listaComunicacaoAIEBusca;
	}

	public ComunicacaoAIEDTO getComunicacaoAIEDTO() {
		return comunicacaoAIEDTO;
	}

	public void setComunicacaoAIEDTO(ComunicacaoAIEDTO comunicacaoAIEDTO) {
		this.comunicacaoAIEDTO = comunicacaoAIEDTO;
	}

	public Mes getMes() {
		return mes;
	}

	public void setMes(Mes mes) {
		this.mes = mes;
	}

	public int getAno() {
		return ano;
	}

	public void setAno(int ano) {
		this.ano = ano;
	}

	public ComunicacaoAIE getComunicacaoAIE() {
		return comunicacaoAIE;
	}

	public void setComunicacaoAIE(ComunicacaoAIE comunicacaoAIE) {
		this.comunicacaoAIE = comunicacaoAIE;
	}

	public List<Laboratorio> getListaLaboratorio() {
		return listaLaboratorio;
	}

	public void setListaLaboratorio(List<Laboratorio> listaLaboratorio) {
		this.listaLaboratorio = listaLaboratorio;
	}
	
}
