package br.gov.mt.indea.sistemaformin.service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.transform.Transformers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.mt.indea.sistemaformin.entity.ChavePrincipalVisitaPropriedadeRural;
import br.gov.mt.indea.sistemaformin.entity.Municipio;
import br.gov.mt.indea.sistemaformin.entity.NumeroVisitaPropriedadeRural;
import br.gov.mt.indea.sistemaformin.entity.VisitaPropriedadeRural;
import br.gov.mt.indea.sistemaformin.entity.dto.AbstractDTO;
import br.gov.mt.indea.sistemaformin.entity.dto.VisitaPropriedadeRuralDTO;
import br.gov.mt.indea.sistemaformin.entity.view.RelatorioVisitaPropriedadeRuralGeral;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoEstabelecimento;
import br.gov.mt.indea.sistemaformin.exception.ApplicationException;
import br.gov.mt.indea.sistemaformin.exception.ApplicationRuntimeException;
import br.gov.mt.indea.sistemaformin.webservice.entity.Exploracao;
import br.gov.mt.indea.sistemaformin.webservice.entity.Produtor;
import br.gov.mt.indea.sistemaformin.webservice.entity.Servidor;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class VisitaPropriedadeRuralService extends PaginableService<VisitaPropriedadeRural, Long> {

	private static final Logger log = LoggerFactory.getLogger(VisitaPropriedadeRuralService.class);
	
	@Inject
	private NumeroVisitaPropriedadeRuralService numeroVisitaPropriedadeRuralService;

	protected VisitaPropriedadeRuralService() {
		super(VisitaPropriedadeRural.class);
	}
	
	public VisitaPropriedadeRural findByIdFetchAll(Long id){
		VisitaPropriedadeRural visitaPropriedadeRural;
		
		StringBuilder sql = new StringBuilder();
		sql.append("select entity ")
		   .append("  from VisitaPropriedadeRural entity ")
		   .append("  left join fetch entity.propriedade propriedade ")
		   .append("  left join fetch propriedade.endereco ")
		   .append("  left join fetch propriedade.municipio ")
		   .append("  left join fetch entity.proprietario proprietario")
		   .append("  left join fetch proprietario.endereco")
		   .append("  left join fetch propriedade.coordenadaGeografica ")
		   .append("  left join fetch entity.recinto r ")
		   .append("  left join fetch r.endereco ")
		   .append("  left join fetch r.responsavel ")
		   .append("  left join fetch r.responsavelTecnico ")
		   .append("  left join fetch entity.abatedouro abatedouro ")
		   .append("  left join fetch abatedouro.municipio ")
		   .append("  left join fetch abatedouro.coordenadaGeografica ")
		   .append("  join fetch entity.listChavePrincipalVisitaPropriedadeRural cp ")
		   .append("  left join fetch entity.detalheComunicacaoAIE")
		   .append(" where entity.id = :id ")
		   .append(" order by entity.dataDaVisita ");
		
		Query query = getSession().createQuery(sql.toString()).setLong("id", id);
		visitaPropriedadeRural = (VisitaPropriedadeRural) query.uniqueResult();
		
		if (visitaPropriedadeRural != null){
		
			Hibernate.initialize(visitaPropriedadeRural.getListServidores());
			for (Servidor servidor : visitaPropriedadeRural.getListServidores()){
				servidor.getEndereco();
				servidor.getMunicipioNascimento();
				servidor.getProfissao();
				servidor.getUle();
				if (servidor.getUle() != null)
					servidor.getUle().getUrs();
			}
			
			if (visitaPropriedadeRural.getPropriedade() != null){
				Hibernate.initialize(visitaPropriedadeRural.getPropriedade().getProprietarios());
				for (Produtor produtor : visitaPropriedadeRural.getPropriedade().getProprietarios()) {
					Hibernate.initialize(produtor.getEndereco());
					Hibernate.initialize(produtor.getEndereco().getMunicipio());
					Hibernate.initialize(produtor.getEndereco().getMunicipio().getUf());
				}
				
				Hibernate.initialize(visitaPropriedadeRural.getPropriedade().getUle());
				Hibernate.initialize(visitaPropriedadeRural.getPropriedade().getExploracaos());
				for (Exploracao exploracao : visitaPropriedadeRural.getPropriedade().getExploracaos()){
					Hibernate.initialize(exploracao.getProdutores());
					for (Produtor produtor : exploracao.getProdutores()) {
						Hibernate.initialize(produtor.getEndereco());
					}
				}
			}
			
			if (visitaPropriedadeRural.getRecinto() != null){
				Hibernate.initialize(visitaPropriedadeRural.getRecinto().getEndereco());
				Hibernate.initialize(visitaPropriedadeRural.getRecinto().getMunicipio());
				if (visitaPropriedadeRural.getRecinto().getEndereco() != null)
					Hibernate.initialize(visitaPropriedadeRural.getRecinto().getEndereco().getMunicipio());
				Hibernate.initialize(visitaPropriedadeRural.getRecinto().getResponsavel());
				Hibernate.initialize(visitaPropriedadeRural.getRecinto().getUle());
			}
				
			for (ChavePrincipalVisitaPropriedadeRural chave : visitaPropriedadeRural.getListChavePrincipalVisitaPropriedadeRural()) {
				Hibernate.initialize(chave.getListChaveSecundariaVisitaPropriedadeRural());
			}
			
		}
		
		return visitaPropriedadeRural;
	}
	
	public VisitaPropriedadeRural findByNumero(String numero){
		VisitaPropriedadeRural visitaPropriedadeRural;
		
		StringBuilder sql = new StringBuilder();
		sql.append("select entity ")
		   .append("  from VisitaPropriedadeRural entity ")
		   .append("  left join fetch entity.propriedade propriedade ")
		   .append("  left join fetch propriedade.endereco ")
		   .append("  left join fetch entity.proprietario proprietario")
		   .append("  left join fetch proprietario.endereco")
		   .append("  left join fetch propriedade.coordenadaGeografica ")
		   .append("  left join fetch entity.recinto r ")
		   .append("  left join fetch r.endereco ")
		   .append("  left join fetch entity.abatedouro abatedouro ")
		   .append("  left join fetch abatedouro.municipio ")
		   .append(" where entity.numero = :numero ")
		   .append(" order by entity.dataDaVisita ");
		
		Query query = getSession().createQuery(sql.toString()).setString("numero", numero);
		visitaPropriedadeRural = (VisitaPropriedadeRural) query.uniqueResult();
		
		if (visitaPropriedadeRural != null){
		
			if (visitaPropriedadeRural.getPropriedade() != null)
				Hibernate.initialize(visitaPropriedadeRural.getPropriedade().getMunicipio());
			
			if (visitaPropriedadeRural.getRecinto() != null)
				Hibernate.initialize(visitaPropriedadeRural.getRecinto().getMunicipio());
			
		}
		
		return visitaPropriedadeRural;
	}
	
	@SuppressWarnings("unchecked")
	public List<VisitaPropriedadeRural> findByNumero(String numero, String codigo) {
		List<VisitaPropriedadeRural> listaVisitaPropriedadeRural;
		
		StringBuilder sql = new StringBuilder();
		sql.append("select entity ")
		   .append("  from VisitaPropriedadeRural entity ")
		   .append("  left join fetch entity.propriedade propriedade ")
		   .append("  left join fetch propriedade.endereco ")
		   .append("  left join fetch entity.proprietario proprietario")
		   .append("  left join fetch proprietario.endereco")
		   .append("  left join fetch propriedade.coordenadaGeografica ")
		   .append("  left join fetch entity.recinto r ")
		   .append("  left join fetch r.endereco ")
		   .append("  left join fetch entity.abatedouro abatedouro ")
		   .append("  left join fetch abatedouro.municipio ")
		   .append(" where 1 = 1 ");
			
		if (!StringUtils.isEmpty(numero))
			sql.append("   and entity.numero = :numero ");

		if (!StringUtils.isEmpty(codigo))
			sql.append("   and propriedade.codigo = :codigo ");
		
		sql.append(" order by entity.dataDaVisita desc");
		
		Query query = getSession().createQuery(sql.toString());
		
		if (!StringUtils.isEmpty(numero))
			query.setString("numero", numero);

		if (!StringUtils.isEmpty(codigo))
			query.setString("codigo", codigo);
		
		listaVisitaPropriedadeRural = (List<VisitaPropriedadeRural>) query.list();
		
		if (listaVisitaPropriedadeRural != null){
			for (VisitaPropriedadeRural visitaPropriedadeRural : listaVisitaPropriedadeRural) {
				if (visitaPropriedadeRural.getPropriedade() != null)
					Hibernate.initialize(visitaPropriedadeRural.getPropriedade().getMunicipio());
				
				if (visitaPropriedadeRural.getRecinto() != null)
					Hibernate.initialize(visitaPropriedadeRural.getRecinto().getMunicipio());
			}
			
		}
		
		return listaVisitaPropriedadeRural;
	}
	
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<VisitaPropriedadeRural> findAllComPaginacao(AbstractDTO dto, int first, int pageSize, String sortField, String sortOrder) {
		StringBuilder sql = new StringBuilder();
		sql.append("select entity ")
		   .append("  from " + this.getType().getSimpleName() + " as entity");
		   
		   
	    if (dto != null && !dto.isNull()){
			VisitaPropriedadeRuralDTO visitaPropriedadeRuralDTO = (VisitaPropriedadeRuralDTO) dto;
			
			if (visitaPropriedadeRuralDTO.getTipoEstabelecimento() == null){
				sql.append("  left join fetch entity.propriedade propriedade ")
				   .append("  left join fetch entity.recinto recinto ")
				   .append("  left join fetch entity.abatedouro abatedouro ");
			} else {
				if (visitaPropriedadeRuralDTO.getTipoEstabelecimento().equals(TipoEstabelecimento.PROPRIEDADE))
					sql.append("  join fetch entity.propriedade propriedade ");
				else if (visitaPropriedadeRuralDTO.getTipoEstabelecimento().equals(TipoEstabelecimento.RECINTO))
		 			sql.append("  join fetch entity.recinto recinto ");
				else
					sql.append("  join fetch entity.abatedouro abatedouro ");
		 	}
				
	    }
		   
	    sql.append(" where entity.codigoVerificador is null ");
		
		if (dto != null && !dto.isNull()){
			VisitaPropriedadeRuralDTO visitaPropriedadeRuralDTO = (VisitaPropriedadeRuralDTO) dto;
			
			if (visitaPropriedadeRuralDTO.getNumero() != null && !visitaPropriedadeRuralDTO.getNumero().equals(""))
				sql.append("  and entity.numero = :numero");
			if (visitaPropriedadeRuralDTO.getData() != null){
				sql.append("  and to_char(entity.dataDaVisita, 'YYYY-MM-DD HH:MI:SS') between :inicio and :final");
			}
			
			if (visitaPropriedadeRuralDTO.getTipoEstabelecimento() != null){
				if (visitaPropriedadeRuralDTO.getTipoEstabelecimento().equals(TipoEstabelecimento.PROPRIEDADE)){
					if (visitaPropriedadeRuralDTO.getMunicipio() != null)
						sql.append("  and propriedade.municipio = :municipio");
					if (visitaPropriedadeRuralDTO.getEstabelecimento() != null && !visitaPropriedadeRuralDTO.getEstabelecimento().equals(""))
						sql.append("  and lower(remove_acento(propriedade.nome)) like :estabelecimento");
				} else if (visitaPropriedadeRuralDTO.getTipoEstabelecimento().equals(TipoEstabelecimento.RECINTO)){
					if (visitaPropriedadeRuralDTO.getMunicipio() != null)
						sql.append("  and recinto.municipio = :municipio");
					if (visitaPropriedadeRuralDTO.getEstabelecimento() != null && !visitaPropriedadeRuralDTO.getEstabelecimento().equals(""))
						sql.append("  and lower(remove_acento(recinto.nome)) like :estabelecimento");
				} else {
					if (visitaPropriedadeRuralDTO.getMunicipio() != null)
						sql.append("  and abatedouro.municipio = :municipio");
					if (visitaPropriedadeRuralDTO.getEstabelecimento() != null && !visitaPropriedadeRuralDTO.getEstabelecimento().equals(""))
						sql.append("  and lower(remove_acento(abatedouro.nome)) like :estabelecimento");
				}
			} else {
				if (visitaPropriedadeRuralDTO.getMunicipio() != null)
					sql.append(" and (propriedade.municipio = :municipio or recinto.municipio = :municipio or abatedouro.municipio = :municipio) ");
				
				if (visitaPropriedadeRuralDTO.getEstabelecimento() != null && !visitaPropriedadeRuralDTO.getEstabelecimento().equals("")){
					sql.append(" and (lower(remove_acento(propriedade.nome)) like :estabelecimento or lower(remove_acento(recinto.nome)) like :estabelecimento or lower(remove_acento(abatedouro.nome)) like :estabelecimento) ");
				} 
			}
		}
		
		if (StringUtils.isNotBlank(sortField))
			sql.append(" order by entity." + sortField + " " + sortOrder);

		Query query = getSession().createQuery(sql.toString());
		
		if (dto != null && !dto.isNull()){
			VisitaPropriedadeRuralDTO visitaPropriedadeRuralDTO = (VisitaPropriedadeRuralDTO) dto;
			
			if (visitaPropriedadeRuralDTO.getNumero() != null && !visitaPropriedadeRuralDTO.getNumero().equals(""))
				query.setString("numero", visitaPropriedadeRuralDTO.getNumero());
			
			
			if (visitaPropriedadeRuralDTO.getData() != null){
				Calendar i = Calendar.getInstance();
				i.setTime(visitaPropriedadeRuralDTO.getData());
				
				StringBuilder sbI = new StringBuilder();
				sbI.append(i.get(Calendar.YEAR)).append("-")
				   .append(((i.get(Calendar.MONTH) + 1) + "").length() > 1 ? (i.get(Calendar.MONTH)+1) : "0" + (i.get(Calendar.MONTH)+1)).append("-")
				   .append((i.get(Calendar.DAY_OF_MONTH) + "").length() > 1 ? i.get(Calendar.DAY_OF_MONTH) : "0" + i.get(Calendar.DAY_OF_MONTH))
				   .append(" ")
				   .append("00").append(":")
				   .append("00").append(":")
				   .append("00");
				
				Calendar f = Calendar.getInstance();
				f.setTime(visitaPropriedadeRuralDTO.getData());
				
				StringBuilder sbF = new StringBuilder();
				sbF.append(f.get(Calendar.YEAR)).append("-")
				   .append(((f.get(Calendar.MONTH) + 1) + "").length() > 1 ? (f.get(Calendar.MONTH)+1) : "0" + (f.get(Calendar.MONTH)+1)).append("-")
				   .append((f.get(Calendar.DAY_OF_MONTH) + "").length() > 1 ? f.get(Calendar.DAY_OF_MONTH) : "0" + f.get(Calendar.DAY_OF_MONTH))
				   .append(" ")
				   .append("23").append(":")
				   .append("23").append(":")
				   .append("59");
				
				query.setString("inicio", sbI.toString());
				query.setString("final", sbF.toString());
			}
			
			if (visitaPropriedadeRuralDTO.getTipoEstabelecimento() != null){
				if (visitaPropriedadeRuralDTO.getTipoEstabelecimento().equals(TipoEstabelecimento.PROPRIEDADE)){
					if (visitaPropriedadeRuralDTO.getMunicipio() != null)
						query.setParameter("municipio", visitaPropriedadeRuralDTO.getMunicipio());
					if (visitaPropriedadeRuralDTO.getEstabelecimento() != null && !visitaPropriedadeRuralDTO.getEstabelecimento().equals(""))
						query.setString("estabelecimento", "%" + visitaPropriedadeRuralDTO.getEstabelecimento().toLowerCase() + "%");
				} else {
					if (visitaPropriedadeRuralDTO.getMunicipio() != null)
						query.setParameter("municipio", visitaPropriedadeRuralDTO.getMunicipio());
					if (visitaPropriedadeRuralDTO.getEstabelecimento() != null && !visitaPropriedadeRuralDTO.getEstabelecimento().equals(""))
						query.setString("estabelecimento", "%" + visitaPropriedadeRuralDTO.getEstabelecimento().toLowerCase() + "%");
				}
			} else {
				if (visitaPropriedadeRuralDTO.getMunicipio() != null)
					query.setParameter("municipio", visitaPropriedadeRuralDTO.getMunicipio());
				
				if (visitaPropriedadeRuralDTO.getEstabelecimento() != null && !visitaPropriedadeRuralDTO.getEstabelecimento().equals("")){
					query.setString("estabelecimento", "%" + visitaPropriedadeRuralDTO.getEstabelecimento().toLowerCase() + "%");
				} 
			}
		}
		
		query.setFirstResult(first);
		query.setMaxResults(pageSize);

		List<VisitaPropriedadeRural> lista = query.list();
		
		return lista;
    }
	
	@Override
	public Long countAll(AbstractDTO dto) {
		StringBuilder sql = new StringBuilder();
		sql.append("select count(entity) ")
		   .append("  from " + this.getType().getSimpleName() + " as entity");
		   
		if (dto != null && !dto.isNull()){
			VisitaPropriedadeRuralDTO visitaPropriedadeRuralDTO = (VisitaPropriedadeRuralDTO) dto;
			
			if (visitaPropriedadeRuralDTO.getTipoEstabelecimento() == null){
				sql.append("  left join  entity.propriedade propriedade ")
				   .append("  left join  entity.recinto recinto ")
				   .append("  left join  entity.abatedouro abatedouro ");
			} else {
				if (visitaPropriedadeRuralDTO.getTipoEstabelecimento().equals(TipoEstabelecimento.PROPRIEDADE))
					sql.append("  join  entity.propriedade propriedade ");
				else if (visitaPropriedadeRuralDTO.getTipoEstabelecimento().equals(TipoEstabelecimento.RECINTO))
		 			sql.append("  join  entity.recinto recinto ");
				else
					sql.append("  join  entity.abatedouro abatedouro ");
		 	}
				
	    }
		   
	    sql.append(" where entity.codigoVerificador is null ");
		
	    if (dto != null && !dto.isNull()){
			VisitaPropriedadeRuralDTO visitaPropriedadeRuralDTO = (VisitaPropriedadeRuralDTO) dto;
			
			if (visitaPropriedadeRuralDTO.getNumero() != null && !visitaPropriedadeRuralDTO.getNumero().equals(""))
				sql.append("  and entity.numero = :numero");
			if (visitaPropriedadeRuralDTO.getData() != null){
				sql.append("  and to_char(entity.dataDaVisita, 'YYYY-MM-DD HH:MI:SS') between :inicio and :final");
			}
			
			if (visitaPropriedadeRuralDTO.getTipoEstabelecimento() != null){
				if (visitaPropriedadeRuralDTO.getTipoEstabelecimento().equals(TipoEstabelecimento.PROPRIEDADE)){
					if (visitaPropriedadeRuralDTO.getMunicipio() != null)
						sql.append("  and propriedade.municipio = :municipio");
					if (visitaPropriedadeRuralDTO.getEstabelecimento() != null && !visitaPropriedadeRuralDTO.getEstabelecimento().equals(""))
						sql.append("  and lower(remove_acento(propriedade.nome)) like :estabelecimento");
				} else if (visitaPropriedadeRuralDTO.getTipoEstabelecimento().equals(TipoEstabelecimento.RECINTO)){
					if (visitaPropriedadeRuralDTO.getMunicipio() != null)
						sql.append("  and recinto.municipio = :municipio");
					if (visitaPropriedadeRuralDTO.getEstabelecimento() != null && !visitaPropriedadeRuralDTO.getEstabelecimento().equals(""))
						sql.append("  and lower(remove_acento(recinto.nome)) like :estabelecimento");
				} else {
					if (visitaPropriedadeRuralDTO.getMunicipio() != null)
						sql.append("  and abatedouro.municipio = :municipio");
					if (visitaPropriedadeRuralDTO.getEstabelecimento() != null && !visitaPropriedadeRuralDTO.getEstabelecimento().equals(""))
						sql.append("  and lower(remove_acento(abatedouro.nome)) like :estabelecimento");
				}
			} else {
				if (visitaPropriedadeRuralDTO.getMunicipio() != null)
					sql.append(" and (propriedade.municipio = :municipio or recinto.municipio = :municipio or abatedouro.municipio = :municipio) ");
				
				if (visitaPropriedadeRuralDTO.getEstabelecimento() != null && !visitaPropriedadeRuralDTO.getEstabelecimento().equals("")){
					sql.append(" and (lower(remove_acento(propriedade.nome)) like :estabelecimento or lower(remove_acento(recinto.nome)) like :estabelecimento or lower(remove_acento(abatedouro.nome)) like :estabelecimento) ");
				} 
			}
		}
		
		Query query = getSession().createQuery(sql.toString());
		
		if (dto != null && !dto.isNull()){
			VisitaPropriedadeRuralDTO visitaPropriedadeRuralDTO = (VisitaPropriedadeRuralDTO) dto;
			
			if (visitaPropriedadeRuralDTO.getNumero() != null && !visitaPropriedadeRuralDTO.getNumero().equals(""))
				query.setString("numero", visitaPropriedadeRuralDTO.getNumero());
			
			
			if (visitaPropriedadeRuralDTO.getData() != null){
				Calendar i = Calendar.getInstance();
				i.setTime(visitaPropriedadeRuralDTO.getData());
				
				StringBuilder sbI = new StringBuilder();
				sbI.append(i.get(Calendar.YEAR)).append("-")
				   .append(((i.get(Calendar.MONTH) + 1) + "").length() > 1 ? (i.get(Calendar.MONTH)+1) : "0" + (i.get(Calendar.MONTH)+1)).append("-")
				   .append((i.get(Calendar.DAY_OF_MONTH) + "").length() > 1 ? i.get(Calendar.DAY_OF_MONTH) : "0" + i.get(Calendar.DAY_OF_MONTH))
				   .append(" ")
				   .append("00").append(":")
				   .append("00").append(":")
				   .append("00");
				
				Calendar f = Calendar.getInstance();
				f.setTime(visitaPropriedadeRuralDTO.getData());
				
				StringBuilder sbF = new StringBuilder();
				sbF.append(f.get(Calendar.YEAR)).append("-")
				   .append(((f.get(Calendar.MONTH) + 1) + "").length() > 1 ? (f.get(Calendar.MONTH)+1) : "0" + (f.get(Calendar.MONTH)+1)).append("-")
				   .append((f.get(Calendar.DAY_OF_MONTH) + "").length() > 1 ? f.get(Calendar.DAY_OF_MONTH) : "0" + f.get(Calendar.DAY_OF_MONTH))
				   .append(" ")
				   .append("23").append(":")
				   .append("23").append(":")
				   .append("59");
				
				query.setString("inicio", sbI.toString());
				query.setString("final", sbF.toString());
			}
			
			if (visitaPropriedadeRuralDTO.getTipoEstabelecimento() != null){
				if (visitaPropriedadeRuralDTO.getTipoEstabelecimento().equals(TipoEstabelecimento.PROPRIEDADE)){
					if (visitaPropriedadeRuralDTO.getMunicipio() != null)
						query.setParameter("municipio", visitaPropriedadeRuralDTO.getMunicipio());
					if (visitaPropriedadeRuralDTO.getEstabelecimento() != null && !visitaPropriedadeRuralDTO.getEstabelecimento().equals(""))
						query.setString("estabelecimento", "%" + visitaPropriedadeRuralDTO.getEstabelecimento().toLowerCase() + "%");
				} else {
					if (visitaPropriedadeRuralDTO.getMunicipio() != null)
						query.setParameter("municipio", visitaPropriedadeRuralDTO.getMunicipio());
					if (visitaPropriedadeRuralDTO.getEstabelecimento() != null && !visitaPropriedadeRuralDTO.getEstabelecimento().equals(""))
						query.setString("estabelecimento", "%" + visitaPropriedadeRuralDTO.getEstabelecimento().toLowerCase() + "%");
				}
			} else {
				if (visitaPropriedadeRuralDTO.getMunicipio() != null)
					query.setParameter("municipio", visitaPropriedadeRuralDTO.getMunicipio());
				
				if (visitaPropriedadeRuralDTO.getEstabelecimento() != null && !visitaPropriedadeRuralDTO.getEstabelecimento().equals("")){
					query.setString("estabelecimento", "%" + visitaPropriedadeRuralDTO.getEstabelecimento().toLowerCase() + "%");
				} 
			}
		}
		
		return (Long) query.uniqueResult();
	}
	
	@Override
	public void saveOrUpdate(VisitaPropriedadeRural visitaPropriedadeRural) {
		if (visitaPropriedadeRural.getNumero() == null)
			try {
				visitaPropriedadeRural.setNumero(getProximoNumeroVisitaPropriedadeRural(visitaPropriedadeRural));
			} catch (ApplicationException e) {
				throw new ApplicationRuntimeException(e);
			}
		
		visitaPropriedadeRural = this.getEntityManager().merge(visitaPropriedadeRural);
		super.saveOrUpdate(visitaPropriedadeRural);
		
		log.info("Salvando Visita a Propriedade Rural {}", visitaPropriedadeRural.getId());
	}
	
	@Override
	public void delete(VisitaPropriedadeRural visitaPropriedadeRural) {
		super.delete(visitaPropriedadeRural);
		
		log.info("Removendo Visita a Propriedade Rural {}", visitaPropriedadeRural.getId());
	}
	
	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	private String getProximoNumeroVisitaPropriedadeRural(VisitaPropriedadeRural visitaPropriedadeRural) throws ApplicationException{
		StringBuilder sb = new StringBuilder();
		
		String codgIBGEMunicipio = null;
		String codgIBGEUF = null;
		Municipio municipio = null;
		
		if (visitaPropriedadeRural.getPropriedade() != null){
			if (visitaPropriedadeRural.getPropriedade().getMunicipio().getUf() == null)
				throw new ApplicationException("A UF da Visita a propriedade rural n�o pode ser nula.");
			else if (visitaPropriedadeRural.getPropriedade().getMunicipio().getUf().getCodgIBGE() == null)
				throw new ApplicationException("A UF informada n�o possui c�digo do IBGE");
			
			if (visitaPropriedadeRural.getPropriedade().getMunicipio() == null)
				throw new ApplicationException("O Munic�pio da Visita a propriedade rural n�o pode ser nulo.");
			else if (visitaPropriedadeRural.getPropriedade().getMunicipio().getCodgIBGE() == null)
				throw new ApplicationException("O Munic�pio informada n�o possui c�digo do IBGE");
			
			codgIBGEMunicipio = visitaPropriedadeRural.getPropriedade().getMunicipio().getCodgIBGE();
			codgIBGEUF = visitaPropriedadeRural.getPropriedade().getMunicipio().getUf().getCodgIBGE();
			municipio =  visitaPropriedadeRural.getPropriedade().getMunicipio();
		} else if (visitaPropriedadeRural.getRecinto() != null){
			if (visitaPropriedadeRural.getRecinto().getMunicipio().getUf() == null)
				throw new ApplicationException("A UF da Visita a propriedade rural n�o pode ser nula.");
			else if (visitaPropriedadeRural.getRecinto().getMunicipio().getUf().getCodgIBGE() == null)
				throw new ApplicationException("A UF informada n�o possui c�digo do IBGE");
			
			if (visitaPropriedadeRural.getRecinto().getMunicipio() == null)
				throw new ApplicationException("O Munic�pio da Visita a propriedade rural n�o pode ser nulo.");
			else if (visitaPropriedadeRural.getRecinto().getMunicipio().getCodgIBGE() == null)
				throw new ApplicationException("O Munic�pio informada n�o possui c�digo do IBGE");
			
			codgIBGEMunicipio = visitaPropriedadeRural.getRecinto().getMunicipio().getCodgIBGE();
			codgIBGEUF = visitaPropriedadeRural.getRecinto().getMunicipio().getUf().getCodgIBGE();
			municipio =  visitaPropriedadeRural.getRecinto().getMunicipio();
		} else {
			if (visitaPropriedadeRural.getAbatedouro().getMunicipio().getUf() == null)
				throw new ApplicationException("A UF da Visita a propriedade rural n�o pode ser nula.");
			else if (visitaPropriedadeRural.getAbatedouro().getMunicipio().getUf().getCodgIBGE() == null)
				throw new ApplicationException("A UF informada n�o possui c�digo do IBGE");
			
			if (visitaPropriedadeRural.getAbatedouro().getMunicipio() == null)
				throw new ApplicationException("O Munic�pio da Visita a propriedade rural n�o pode ser nulo.");
			else if (visitaPropriedadeRural.getAbatedouro().getMunicipio().getCodgIBGE() == null)
				throw new ApplicationException("O Munic�pio informada n�o possui c�digo do IBGE");
			
			codgIBGEMunicipio = visitaPropriedadeRural.getAbatedouro().getMunicipio().getCodgIBGE();
			codgIBGEUF = visitaPropriedadeRural.getAbatedouro().getMunicipio().getUf().getCodgIBGE();
			municipio =  visitaPropriedadeRural.getAbatedouro().getMunicipio();
		}
		
		
		sb.append(codgIBGEUF);
		sb.append(codgIBGEMunicipio);
		sb.append("-");
		
		int ano = visitaPropriedadeRural.getDataDaVisita().get(java.util.Calendar.YEAR);
		
		NumeroVisitaPropriedadeRural numeroVisitaPropriedadeRural = this.getNumeroVisitaPropriedadeRuralByMunicipio(municipio, ano);
		if (numeroVisitaPropriedadeRural.getUltimoNumero() >= 9999)
			throw new ApplicationException("A numera��o de Visita a propriedade rural para o munic�pio de " + municipio.getNome() + " j� atingiu o limite de 9999");
		
		String proximoNumero = String.format("%04d", numeroVisitaPropriedadeRural.getUltimoNumero());
		sb.append(proximoNumero);
	    sb.append("/");
	    sb.append(ano);
		
		return sb.toString();
	}
	
	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	private NumeroVisitaPropriedadeRural getNumeroVisitaPropriedadeRuralByMunicipio(Municipio municipio, int ano) throws ApplicationException{
		NumeroVisitaPropriedadeRural numeroVisitaPropriedadeRural = numeroVisitaPropriedadeRuralService.getNumeroVisitaPropriedadeRuralByMunicipio(municipio, ano);
		
		if (numeroVisitaPropriedadeRural == null){
			numeroVisitaPropriedadeRural = new NumeroVisitaPropriedadeRural();
			numeroVisitaPropriedadeRural.setMunicipio(municipio);
			numeroVisitaPropriedadeRural.setUltimoNumero(0L);
			numeroVisitaPropriedadeRural.setAno((long) ano);
			
			numeroVisitaPropriedadeRuralService.saveOrUpdate(numeroVisitaPropriedadeRural);
		}
		
		numeroVisitaPropriedadeRural.incrementNumeroVisitaPropriedadeRural();
		numeroVisitaPropriedadeRuralService.merge(numeroVisitaPropriedadeRural);
		
		return numeroVisitaPropriedadeRural;
	}
	
	@SuppressWarnings("unchecked")
	public List<Object[]> relatorio(Date dataInicial, Date dataFinal){
		StringBuilder sql = new StringBuilder();
		sql.append("select distinct case  ")
		   .append("	when nome_propriedade is null then urs_recinto ")
		   .append("       else urs_propriedade ")
		   .append("       end as urs, ")
		   .append(" ")
		   .append("       case  ")
		   .append("	when nome_propriedade is null then municipio_recinto ")
		   .append("       else municipio_propriedade ")
		   .append("       end as municipio, ")
		   .append(" ")
		   .append("       q.numero, ")
		   .append("       to_char(data_da_visita, 'dd/mm/yyyy') as data_da_visita, ")
		   .append("       to_char(data_cadastro, 'dd/mm/yyyy HH24:MI:SS') as data_cadastro, ")
		   .append("       case  ")
		   .append("	when nome_propriedade is null then 'recinto' ")
		   .append("       else 'propriedade' ")
		   .append("       end as tipo_estabelecimento, ")
		   .append(" ")
		   .append("       case  ")
		   .append("	when nome_propriedade is null then nome_recinto ")
		   .append("       else nome_propriedade ")
		   .append("       end as nome, ")
		   .append(" ")
		   .append("       case  ")
		   .append("	when nome_propriedade is null then codigo_recinto ")
		   .append("       else codigo_propriedade ")
		   .append("       end as codigo_estabelecimento, ")
		   .append(" ")
		   .append("       q.chave_principal, ")
		   .append("        ")
		   .append("       q.vigilancia_veterinaria, ")
		   .append("              cadastramento_ou_atualizacao, ")
		   .append("       educacao_sanitaria, ")
		   .append("       vacinacao, ")
		   .append("       apreensao, ")
		   .append("       sacrificio, ")
		   .append("       destruicao, ")
		   .append("       captura, ")
		   .append("       deseinfeccao, ")
		   .append("       saneamento, ")
		   .append("       marcacao, ")
		   .append("       isolamento, ")
		   .append("       interdicao, ")
		   .append("       desinterdicao, ")
		   .append("       notificacao, ")
		   .append("       autuacao, ")
		   .append("       cadastro_de_aglomeracao, ")
		   .append("       cadastro_de_evento, ")
		   .append("       cadastro_de_recinto, ")
		   .append("       denuncia, ")
		   .append("       formulario_de_fiscalizacao_sanitaria_de_estabelecimento_avicola, ")
		   .append("       inspecao_de_animais, ")
		   .append("       laudo_de_inspecao_fisica_e_sanitaria_a_estabelevimento_avicola, ")
		   .append("       termo_de_verificacao, ")
		   .append("       vistoria_de_rebanho, ")
		   .append("       outro, ")
		   .append(" ")
		   .append(" ")
		   .append("       q.servidor_que_cadastrou, ")
		   .append("       q.servidores_que_realizaram_visita ")
		   .append(" ")
		   .append("  from (select v.numero, ")
		   .append("	       v.id, ")
		   .append("	       v.data_da_visita, ")
		   .append("	       v.data_cadastro, ")
		   .append("	       s1.municipio as municipio_propriedade, ")
		   .append("	       s2.nome as municipio_recinto, ")
		   .append("	       s1.nome_propriedade, ")
		   .append("	       s2.nome_recinto, ")
		   .append("	       s1.codigo_propriedade, ")
		   .append("	       s2.codigo_recinto, ")
		   .append("	       s1.urs_propriedade, ")
		   .append("	       s2.urs_recinto, ")
		   .append("	       tcp.nome as chave_principal, ")
		   .append("	       tcs.nome as chave_secundaria, ")
		   .append("	       s3.vigilancia_veterinaria, ")
		   .append("	       cadastramento_ou_atualizacao, ")
		   .append("	       educacao_sanitaria, ")
		   .append("	       vacinacao, ")
		   .append("	       apreensao, ")
		   .append("	       sacrificio, ")
		   .append("	       destruicao, ")
		   .append("	       captura, ")
		   .append("	       deseinfeccao, ")
		   .append("	       saneamento, ")
		   .append("	       marcacao, ")
		   .append("	       isolamento, ")
		   .append("	       interdicao, ")
		   .append("	       desinterdicao, ")
		   .append("	       notificacao, ")
		   .append("	       autuacao, ")
		   .append("	       cadastro_de_aglomeracao, ")
		   .append("	       cadastro_de_evento, ")
		   .append("	       cadastro_de_recinto, ")
		   .append("	       denuncia, ")
		   .append("	       formulario_de_fiscalizacao_sanitaria_de_estabelecimento_avicola, ")
		   .append("	       inspecao_de_animais, ")
		   .append("	       laudo_de_inspecao_fisica_e_sanitaria_a_estabelevimento_avicola, ")
		   .append("	       termo_de_verificacao, ")
		   .append("	       vistoria_de_rebanho, ")
		   .append("	       outro, ")
		   .append("	       u.nome as servidor_que_cadastrou, ")
		   .append("	       ss.servidores_que_realizaram_visita ")
		   .append("	  from visita_propriedade_rural v ")
		   .append("	  left join (select m.nome as municipio, ")
		   .append("			    p.id, ")
		   .append("			    p.nome as nome_propriedade, ")
		   .append("			    p.codigo_propriedade, ")
		   .append("			    urs.nome as urs_propriedade ")
		   .append("		       from propriedade p, ")
		   .append("			    municipio m, ")
		   .append("			    unidade ule, ")
		   .append("			    unidade urs ")
		   .append("		      where p.id_municipio = m.id ")
		   .append("			and p.id_unidade = ule.id ")
		   .append("			and ule.id_unidade_pai = urs.id) as s1 on s1.id = v.id_propriedade ")
		   .append("	  left join (select m.nome, ")
		   .append("			    r.id, ")
		   .append("			    r.nome as nome_recinto, ")
		   .append("			    r.codigo_recinto, ")
		   .append("			    urs.nome as urs_recinto ")
		   .append("		       from recinto r, ")
		   .append("			    municipio m, ")
		   .append("			    unidade ule, ")
		   .append("			    unidade urs ")
		   .append("		      where r.id_municipio = m.id ")
		   .append("			and r.id_unidade = ule.id ")
		   .append("			and ule.id_unidade_pai = urs.id) as s2 on s2.id = v.id_recinto ")
		   .append(" ")
		   .append(" ")
		   .append("	  left join (SELECT vv.id, ")
		   .append("		            array_to_string(array_agg(distinct p.nome), ',') as servidores_que_realizaram_visita ")
		   .append("		       FROM visita_propriedade_rural vv, ")
		   .append("  		            visita_prop_rural_servidores r, ")
		   .append("		            pessoa p ")
		   .append("		      WHERE r.id_visita_propriedade_rural = vv.id ")
		   .append("		        AND r.id_pessoa = p.id ")
		   .append("		      GROUP BY 1) as ss on ss.id = v.id ")
		   .append(" ")
		   .append("			, ")
		   .append("	   ")
		   .append("		      ")
		   .append("	       chave_principal_visita cp ")
		   .append("	  left join(select cp.id, ")
		   .append("			       MAX(case when t.nome = 'VIGIL�NCIA SANIT�RIA' then '1' else '0' end) as \"vigilancia_sanitaria\", ")
		   .append("			        ")
		   .append("			       MAX(case when t.nome = 'VIGIL�NCIA VETERIN�RIA' then '1' else '0' end) as \"vigilancia_veterinaria\", ")
		   .append(" ")
		   .append("			       MAX(case when t.nome = 'CADASTRAMENTO OU ATUALIZA��O' then '1' else '0' end) as \"cadastramento_ou_atualizacao\", ")
		   .append(" ")
		   .append("			       MAX(case when t.nome = 'EDUCA��O SANIT�RIA' then '1' else '0' end) as \"educacao_sanitaria\", ")
		   .append(" ")
		   .append("			       MAX(case when t.nome = 'VACINA��O' then '1' else '0' end) as \"vacinacao\", ")
		   .append(" ")
		   .append("			       MAX(case when t.nome = 'APREENS�O' then '1' else '0' end) as \"apreensao\", ")
		   .append(" ")
		   .append("			       MAX(case when t.nome = 'SACRIF�CIO' then '1' else '0' end) as \"sacrificio\", ")
		   .append(" ")
		   .append("			       MAX(case when t.nome = 'DESTRUI��O' then '1' else '0' end) as \"destruicao\", ")
		   .append(" ")
		   .append("			       MAX(case when t.nome = 'CAPTURA' then '1' else '0' end) as \"captura\", ")
		   .append(" ")
		   .append("			       MAX(case when t.nome = 'DESINFEC��O' then '1' else '0' end) as \"deseinfeccao\", ")
		   .append(" ")
		   .append("			       MAX(case when t.nome = 'SANEAMENTO' then '1' else '0' end) as \"saneamento\", ")
		   .append(" ")
		   .append("			       MAX(case when t.nome = 'MARCA��O' then '1' else '0' end) as \"marcacao\", ")
		   .append(" ")
		   .append("			       MAX(case when t.nome = 'ISOLAMENTO' then '1' else '0' end) as \"isolamento\", ")
		   .append(" ")
		   .append("			       MAX(case when t.nome = 'INTERDI��O' then '1' else '0' end) as \"interdicao\", ")
		   .append(" ")
		   .append("			       MAX(case when t.nome = 'DESINTERDI��O' then '1' else '0' end) as \"desinterdicao\", ")
		   .append(" ")
		   .append("			       MAX(case when t.nome = 'NOTIFICA��O' then '1' else '0' end) as \"notificacao\", ")
		   .append(" ")
		   .append("			       MAX(case when t.nome = 'AUTUA��O' then '1' else '0' end) as \"autuacao\", ")
		   .append(" ")
		   .append("			       MAX(case when t.nome = 'CADASTRO DE AGLOMERA��O' then '1' else '0' end) as \"cadastro_de_aglomeracao\", ")
		   .append(" ")
		   .append("			       MAX(case when t.nome = 'CADASTRO DE EVENTO' then '1' else '0' end) as \"cadastro_de_evento\", ")
		   .append(" ")
		   .append("			       MAX(case when t.nome = 'CADASTRO DE RECINTO' then '1' else '0' end) as \"cadastro_de_recinto\", ")
		   .append(" ")
		   .append("			       MAX(case when t.nome = 'DEN�NCIA' then '1' else '0' end) as \"denuncia\", ")
		   .append(" ")
		   .append("			       MAX(case when t.nome = 'FORMULARIO DE FISCALIZA�AO SANITARIA DE ESTABELECIMENTO AVICOLA' then '1' else '0' end) as \"formulario_de_fiscalizacao_sanitaria_de_estabelecimento_avicola\", ")
		   .append(" ")
		   .append("			       MAX(case when t.nome = 'INSPE��O DE ANIMAIS' then '1' else '0' end) as \"inspecao_de_animais\", ")
		   .append(" ")
		   .append("			       MAX(case when t.nome = 'LAUDO DE INSPE�AO FISICA E SANITARIA A ESTAELECIMENTO AVICOLA' then '1' else '0' end) as \"laudo_de_inspecao_fisica_e_sanitaria_a_estabelevimento_avicola\", ")
		   .append(" ")
		   .append("			       MAX(case when t.nome = 'TERMO DE VERIFICA��O' then '1' else '0' end) as \"termo_de_verificacao\", ")
		   .append(" ")
		   .append("			       MAX(case when t.nome = 'VISTORIA DE REBANHO' then '1' else '0' end) as \"vistoria_de_rebanho\", ")
		   .append(" ")
		   .append("			       MAX(case when t.nome = 'OUTRO' then '1' else '0' end) as \"outro\" ")
		   .append(" ")
		   .append("			  from chave_principal_visita cp, ")
		   .append("			       chave_secundaria_visita cs, ")
		   .append("			       tipo_chave_secundaria_visita t ")
		   .append("			 where cp.id = cs.id_chave_principal_visita ")
		   .append("			   and cs.id_tipo_chave_secundaria_visit = t.id ")
		   .append("			 group by cp.id ")
		   .append("			 order by cp.id) as s3 on s3.id = cp.id, ")
		   .append(" ")
		   .append("		      ")
		   .append(" ")
		   .append("	       tipo_chave_principal_visita tcp, ")
		   .append("	       chave_secundaria_visita cs, ")
		   .append("	       tipo_chave_secundaria_visita tcs, ")
		   .append("	       usuario u ")
		   .append("	 where v.id = cp.id_principal_visita ")
		   .append("	   and cp.id_tipo_chave_principal_visit = tcp.id ")
		   .append("	   and cp.id = cs.id_chave_principal_visita ")
		   .append("	   and cs.id_tipo_chave_secundaria_visit = tcs.id ")
		   .append("	   and v.id_usuario = u.id ")
		   .append("       and to_char(v.data_da_visita, 'YYYY-MM-DD HH:MI:SS') between :dataInicial and :dataFinal ")
		   .append("	 ) as q ");
		   
		sql.append(" order by  ")
		   .append("       urs, ")
		   .append("       municipio, ")
		   .append("       tipo_estabelecimento, ")
		   .append("       data_da_visita ");
		
		Query query = getSession().createSQLQuery(sql.toString());
		
		Calendar i = Calendar.getInstance();
		i.setTime(dataInicial);
		
		StringBuilder sbI = new StringBuilder();
		sbI.append(i.get(Calendar.YEAR)).append("-")
		   .append(((i.get(Calendar.MONTH) + 1) + "").length() > 1 ? (i.get(Calendar.MONTH)+1) : "0" + (i.get(Calendar.MONTH)+1)).append("-")
		   .append((i.get(Calendar.DAY_OF_MONTH) + "").length() > 1 ? i.get(Calendar.DAY_OF_MONTH) : "0" + i.get(Calendar.DAY_OF_MONTH))
		   .append(" ")
		   .append("00").append(":")
		   .append("00").append(":")
		   .append("00");
		
		Calendar f = Calendar.getInstance();
		f.setTime(dataFinal);
		
		StringBuilder sbF = new StringBuilder();
		sbF.append(f.get(Calendar.YEAR)).append("-")
		   .append(((f.get(Calendar.MONTH) + 1) + "").length() > 1 ? (f.get(Calendar.MONTH)+1) : "0" + (f.get(Calendar.MONTH)+1)).append("-")
		   .append((f.get(Calendar.DAY_OF_MONTH) + "").length() > 1 ? f.get(Calendar.DAY_OF_MONTH) : "0" + f.get(Calendar.DAY_OF_MONTH))
		   .append(" ")
		   .append("23").append(":")
		   .append("23").append(":")
		   .append("59");
		
		query.setString("dataInicial", sbI.toString());
		query.setString("dataFinal", sbF.toString());
				
		return query.list();
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<RelatorioVisitaPropriedadeRuralGeral> relatorioTeste(Date dataInicial, Date dataFinal){
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT * ")
		   .append("  FROM relatorio_visita_geral relatorio ")
		   .append(" WHERE relatorio.data_da_visita between :dataInicial and :dataFinal");
		
		
		Query query = getSession().createSQLQuery(sql.toString()); 

		query.setParameter("dataInicial", dataInicial);
		query.setParameter("dataFinal", dataFinal);
		
		query.setResultTransformer(Transformers.aliasToBean(RelatorioVisitaPropriedadeRuralGeral.class));

		List list = query.list();
		
		return list;
	}

	@Override
	public void validar(VisitaPropriedadeRural VisitaPropriedadeRural) {

	}

	@Override
	public void validarPersist(VisitaPropriedadeRural VisitaPropriedadeRural) {

	}

	@Override
	public void validarMerge(VisitaPropriedadeRural VisitaPropriedadeRural) {

	}

	@Override
	public void validarDelete(VisitaPropriedadeRural VisitaPropriedadeRural) {

	}

}
