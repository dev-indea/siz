package br.gov.mt.indea.sistemaformin.dao;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.inject.Inject;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import br.gov.mt.indea.sistemaformin.entity.Usuario;
import br.gov.mt.indea.sistemaformin.exception.ApplicationException;
import br.gov.mt.indea.sistemaformin.util.StringUtil;

@Stateless
public class UsuarioDAO extends DAO<Usuario> {
	
	@Inject
	private EntityManager em;
	
	public UsuarioDAO() {
		super(Usuario.class);
	}

	@Override
	public EntityManager getEntityManager() {
		return this.em;
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void add(Usuario usuario) throws ApplicationException{
		usuario = this.getEntityManager().merge(usuario);
		super.add(usuario);
	}
	
	public Usuario findByUser(String user) throws ApplicationException{
		try{
			CriteriaBuilder cb = this.getEntityManager().getCriteriaBuilder();
			CriteriaQuery<Usuario> query = cb.createQuery(Usuario.class);
			
			Root<Usuario> t = query.from(Usuario.class);
			
			query.where(cb.equal(t.get("id"), user));
			
			return this.getEntityManager().createQuery(query).getSingleResult();
		}catch(Exception e){
			throw new ApplicationException("N�o foi poss�vel buscar o registro", e);
		}
	}
	
	public List<Usuario> findBy(String nome, String cpf) throws ApplicationException{
		try	{
			CriteriaBuilder builder = this.getEntityManager().getCriteriaBuilder();
			CriteriaQuery<Usuario> query = builder.createQuery(Usuario.class);
			
			Root<Usuario> root = query.from(Usuario.class);
			
			List<Predicate> predicados = new ArrayList<Predicate>();
			
			if (nome != null && !nome.equals("")){
				predicados.add(builder.like(
					builder.function("remove_acento", String.class, builder.lower(root.<String>get("nome"))), 
					"%" + StringUtil.removeAcentos(nome.toLowerCase() + "%")));
				
			}
			
			if (cpf != null && !cpf.equals(""))
				predicados.add(builder.equal(root.get("cpf"), cpf));
			
			query.where(builder.and(predicados.toArray(new Predicate[]{})));
			query.orderBy(builder.asc(root.get("nome")));
			
			return this.getEntityManager().createQuery(query).getResultList();
		}catch(Exception e){
			throw new ApplicationException("N�o foi poss�vel listar os registros", e);
		}
	}

}
