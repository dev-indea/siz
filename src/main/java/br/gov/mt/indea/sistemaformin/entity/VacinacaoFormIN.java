package br.gov.mt.indea.sistemaformin.entity;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.envers.Audited;

@Audited
@Entity(name="vacinacao_form_in")
@XmlRootElement(name="vacinacaoFormIN")
public class VacinacaoFormIN extends BaseEntity<Long> implements Cloneable{

	private static final long serialVersionUID = 3572099774562833247L;

	@Id
	@SequenceGenerator(name="vacinacao_form_in_seq", sequenceName="vacinacao_form_in_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="vacinacao_form_in_seq")
	private Long id;
	
	@ManyToOne
	@JoinColumn(name="id_formIN")
	private FormIN formIN;
	
	@ManyToOne
	@JoinColumn(name="id_formCOM")
	private FormCOM formCOM;
	
	private String doenca;
	
	@Column(name="nome_comercial_vacina")
	private String nomeComercialDaVacina;
	
	private String fabricante;
	
	private String partida;
	
	@Column(name="data_notificacao")
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar data;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public FormIN getFormIN() {
		return formIN;
	}

	public void setFormIN(FormIN formIN) {
		this.formIN = formIN;
	}

	public FormCOM getFormCOM() {
		return formCOM;
	}

	public void setFormCOM(FormCOM formCOM) {
		this.formCOM = formCOM;
	}

	public String getDoenca() {
		return doenca;
	}

	public void setDoenca(String doenca) {
		this.doenca = doenca;
	}

	public String getNomeComercialDaVacina() {
		return nomeComercialDaVacina;
	}

	public void setNomeComercialDaVacina(String nomeComercialDaVacina) {
		this.nomeComercialDaVacina = nomeComercialDaVacina;
	}

	public String getFabricante() {
		return fabricante;
	}

	public void setFabricante(String fabricante) {
		this.fabricante = fabricante;
	}

	public String getPartida() {
		return partida;
	}

	public void setPartida(String partida) {
		this.partida = partida;
	}

	public Calendar getData() {
		return data;
	}

	public void setData(Calendar data) {
		this.data = data;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VacinacaoFormIN other = (VacinacaoFormIN) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	protected Object clone(FormIN formIN) throws CloneNotSupportedException {
		VacinacaoFormIN cloned = (VacinacaoFormIN) super.clone();
		
		cloned.setId(null);
		cloned.setFormIN(formIN);
		
		return cloned;
	}
	
}
