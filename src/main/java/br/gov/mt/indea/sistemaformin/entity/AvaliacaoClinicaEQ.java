package br.gov.mt.indea.sistemaformin.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import org.hibernate.envers.Audited;

import br.gov.mt.indea.sistemaformin.enums.Dominio.DoencasRespiratorias;
import br.gov.mt.indea.sistemaformin.enums.Dominio.Especie;
import br.gov.mt.indea.sistemaformin.enums.Dominio.LocalizacaoCorrimentoNasal;
import br.gov.mt.indea.sistemaformin.enums.Dominio.OrigemDoAnimal;
import br.gov.mt.indea.sistemaformin.enums.Dominio.Sexo;
import br.gov.mt.indea.sistemaformin.enums.Dominio.SimNao;
import br.gov.mt.indea.sistemaformin.enums.Dominio.SinaisClinicosCavidadeNasal;
import br.gov.mt.indea.sistemaformin.enums.Dominio.SinaisClinicosPele;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoCorrimentoNasal;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoPeriodo;

@Audited
@Entity(name="avaliacao_clinica_eq")
public class AvaliacaoClinicaEQ extends BaseEntity<Long>{

	private static final long serialVersionUID = 101783470917726616L;

	@Id
	@SequenceGenerator(name="avaliacao_clinica_eq_seq", sequenceName="avaliacao_clinica_eq_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="avaliacao_clinica_eq_seq")
	private Long id;
	
	@ManyToOne
	@JoinColumn(name="id_formeq")
	private FormEQ formEQ;
	
	@Column(name="id_amostra")
	private Long idAmostra;
	
	@Column(name="identificacao_animal")
	private String identificacaoAnimal;
	
	@Enumerated(EnumType.STRING)
	private Especie especie;
	
	@Enumerated(EnumType.STRING)
	private Sexo sexo;
	
	private Long idade = null;
	
	@Enumerated(EnumType.STRING)
	@Column(name="tipo_periodo_idade")
	private TipoPeriodo tipoPeriodoIdade;
	
	@Enumerated(EnumType.STRING)
	@Column(name="local_corr_nasal")
	private LocalizacaoCorrimentoNasal localizacaoCorrimentoNasal;
	
	@Enumerated(EnumType.STRING)
	@Column(name="tipo_corr_nasal")
	private TipoCorrimentoNasal tipoCorrimentoNasal;
	
	@Enumerated(EnumType.STRING)
	@Column(name="corr_nasal_com_sangue")
	private SimNao corrimentoNasalComSangue;
	
	@ElementCollection(targetClass = SinaisClinicosCavidadeNasal.class)
	@JoinTable(name = "av_eq_cavidade_nasal", joinColumns = @JoinColumn(name = "id_av_eq", nullable = false))
	@Column(name = "id_av_eq_cavidade_nasal")
	@Enumerated(EnumType.STRING)
	private List<SinaisClinicosCavidadeNasal> sinaisClinicosCavidadeNasal = new ArrayList<SinaisClinicosCavidadeNasal>();
	
	@ElementCollection(targetClass = SinaisClinicosPele.class)
	@JoinTable(name = "av_eq_pele", joinColumns = @JoinColumn(name = "id_av_eq", nullable = false))
	@Column(name = "id_av_eq_pele")
	@Enumerated(EnumType.STRING)
	private List<SinaisClinicosPele> sinaisClinicosPele = new ArrayList<SinaisClinicosPele>();
	
	@Enumerated(EnumType.STRING)
	private SimNao dispneia;
	
	@Enumerated(EnumType.STRING)
	private SimNao tosse;
	
	@Enumerated(EnumType.STRING)
	private SimNao febre;
	
	@Enumerated(EnumType.STRING)
	@Column(name="aumento_vasos_e_ganglios")
	private SimNao aumentoDeVasosEGanglios;
	
	@Enumerated(EnumType.STRING)
	@Column(name="perda_de_peso")
	private SimNao perdaDePeso;
	
	@Enumerated(EnumType.STRING)
	@Column(name="edema_de_membros")
	private SimNao edemaDeMembros;
	
	@Enumerated(EnumType.STRING)
	private SimNao tratamento;
	
	@Enumerated(EnumType.STRING)
	private SimNao amostra;
	
	@Enumerated(EnumType.STRING)
	@Column(name="origem_do_animal")
	private OrigemDoAnimal origemDoAnimal;
	
	@Column(name="uf_de_origem")
	private String ufDeOrigem;
	
	@ElementCollection(targetClass = DoencasRespiratorias.class)
	@JoinTable(name = "av_eq_doencas_respiratorias", joinColumns = @JoinColumn(name = "id_av_eq", nullable = false))
	@Column(name = "id_av_eq_doencas_respiratorias")
	@Enumerated(EnumType.STRING)
	private List<DoencasRespiratorias> doencasRespiratorias = new ArrayList<DoencasRespiratorias>();
	
	@OneToMany(mappedBy="avaliacaoClinicaEQ", cascade=CascadeType.ALL, orphanRemoval=true)
	private List<VacinacaoEQ> vacinacoesEQ = new ArrayList<VacinacaoEQ>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public FormEQ getFormEQ() {
		return formEQ;
	}

	public void setFormEQ(FormEQ formEQ) {
		this.formEQ = formEQ;
	}

	public String getIdentificacaoAnimal() {
		return identificacaoAnimal;
	}

	public void setIdentificacaoAnimal(String identificacaoAnimal) {
		this.identificacaoAnimal = identificacaoAnimal;
	}

	public Especie getEspecie() {
		return especie;
	}

	public void setEspecie(Especie especie) {
		this.especie = especie;
	}

	public Sexo getSexo() {
		return sexo;
	}

	public void setSexo(Sexo sexo) {
		this.sexo = sexo;
	}

	public Long getIdade() {
		return idade;
	}

	public void setIdade(Long idade) {
		this.idade = idade;
	}

	public TipoPeriodo getTipoPeriodoIdade() {
		return tipoPeriodoIdade;
	}

	public void setTipoPeriodoIdade(TipoPeriodo tipoPeriodoIdade) {
		this.tipoPeriodoIdade = tipoPeriodoIdade;
	}

	public LocalizacaoCorrimentoNasal getLocalizacaoCorrimentoNasal() {
		return localizacaoCorrimentoNasal;
	}

	public void setLocalizacaoCorrimentoNasal(
			LocalizacaoCorrimentoNasal localizacaoCorrimentoNasal) {
		this.localizacaoCorrimentoNasal = localizacaoCorrimentoNasal;
	}

	public TipoCorrimentoNasal getTipoCorrimentoNasal() {
		return tipoCorrimentoNasal;
	}

	public void setTipoCorrimentoNasal(TipoCorrimentoNasal tipoCorrimentoNasal) {
		this.tipoCorrimentoNasal = tipoCorrimentoNasal;
	}

	public SimNao getCorrimentoNasalComSangue() {
		return corrimentoNasalComSangue;
	}

	public void setCorrimentoNasalComSangue(SimNao corrimentoNasalComSangue) {
		this.corrimentoNasalComSangue = corrimentoNasalComSangue;
	}

	public List<SinaisClinicosCavidadeNasal> getSinaisClinicosCavidadeNasal() {
		return sinaisClinicosCavidadeNasal;
	}

	public void setSinaisClinicosCavidadeNasal(
			List<SinaisClinicosCavidadeNasal> sinaisClinicosCavidadeNasal) {
		this.sinaisClinicosCavidadeNasal = sinaisClinicosCavidadeNasal;
	}

	public List<SinaisClinicosPele> getSinaisClinicosPele() {
		return sinaisClinicosPele;
	}

	public void setSinaisClinicosPele(List<SinaisClinicosPele> sinaisClinicosPele) {
		this.sinaisClinicosPele = sinaisClinicosPele;
	}

	public SimNao getDispneia() {
		return dispneia;
	}

	public void setDispneia(SimNao dispneia) {
		this.dispneia = dispneia;
	}

	public SimNao getTosse() {
		return tosse;
	}

	public void setTosse(SimNao tosse) {
		this.tosse = tosse;
	}

	public SimNao getFebre() {
		return febre;
	}

	public void setFebre(SimNao febre) {
		this.febre = febre;
	}

	public SimNao getAumentoDeVasosEGanglios() {
		return aumentoDeVasosEGanglios;
	}

	public void setAumentoDeVasosEGanglios(SimNao aumentoDeVasosEGanglios) {
		this.aumentoDeVasosEGanglios = aumentoDeVasosEGanglios;
	}

	public SimNao getPerdaDePeso() {
		return perdaDePeso;
	}

	public void setPerdaDePeso(SimNao perdaDePeso) {
		this.perdaDePeso = perdaDePeso;
	}

	public SimNao getEdemaDeMembros() {
		return edemaDeMembros;
	}

	public void setEdemaDeMembros(SimNao edemaDeMembros) {
		this.edemaDeMembros = edemaDeMembros;
	}

	public SimNao getTratamento() {
		return tratamento;
	}

	public void setTratamento(SimNao tratamento) {
		this.tratamento = tratamento;
	}

	public SimNao getAmostra() {
		return amostra;
	}

	public void setAmostra(SimNao amostra) {
		this.amostra = amostra;
	}

	public OrigemDoAnimal getOrigemDoAnimal() {
		return origemDoAnimal;
	}

	public void setOrigemDoAnimal(OrigemDoAnimal origemDoAnimal) {
		this.origemDoAnimal = origemDoAnimal;
	}

	public String getUfDeOrigem() {
		return ufDeOrigem;
	}

	public void setUfDeOrigem(String ufDeOrigem) {
		this.ufDeOrigem = ufDeOrigem;
	}

	public List<DoencasRespiratorias> getDoencasRespiratorias() {
		return doencasRespiratorias;
	}

	public void setDoencasRespiratorias(
			List<DoencasRespiratorias> doencasRespiratorias) {
		this.doencasRespiratorias = doencasRespiratorias;
	}

	public Long getIdAmostra() {
		return idAmostra;
	}

	public void setIdAmostra(Long idAmostra) {
		this.idAmostra = idAmostra;
	}

	public List<VacinacaoEQ> getVacinacoesEQ() {
		return vacinacoesEQ;
	}

	public void setVacinacoesEQ(List<VacinacaoEQ> vacinacoesEQ) {
		this.vacinacoesEQ = vacinacoesEQ;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AvaliacaoClinicaEQ other = (AvaliacaoClinicaEQ) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
