package br.gov.mt.indea.sistemaformin.service;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.hibernate.Query;

import br.gov.mt.indea.sistemaformin.entity.Municipio;
import br.gov.mt.indea.sistemaformin.entity.UF;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class MunicipioService extends PaginableService<Municipio, Long> {

	protected MunicipioService() {
		super(Municipio.class);
	}
	
	public Municipio findByIdFetchAll(Long id){
		Municipio municipio;
		
		StringBuilder sql = new StringBuilder();
		sql.append("select municipio ")
		   .append("  from Municipio municipio ")
		   .append(" where municipio.id = :id ");
		
		Query query = getSession().createQuery(sql.toString()).setLong("id", id);
		municipio = (Municipio) query.uniqueResult();
		
		return municipio;
	}
	
	public Municipio findByCodgIBGE(String codgIbgeUf, String codgIbgeMunicipio){
		Municipio municipio;
		
		StringBuilder sql = new StringBuilder();
		sql.append("  from Municipio municipio ")
		   .append("  join fetch municipio.uf uf")
		   .append(" where municipio.codgIBGE = :codgIbgeMunicipio ")
		   .append("   and uf.codgIBGE = :codgIbgeUf ");
		
		Query query = getSession().createQuery(sql.toString());
		query.setString("codgIbgeUf", codgIbgeUf);
		query.setString("codgIbgeMunicipio", codgIbgeMunicipio);
		municipio = (Municipio) query.uniqueResult();
		
		return municipio;
	}
	
	@SuppressWarnings("unchecked")
	public List<Municipio> findAllByUF(UF uf){
		List<Municipio> listaMunicipio = new ArrayList<>();
		
		StringBuilder sql = new StringBuilder();
		sql.append("select entity ")
		   .append("  from Municipio entity ")
		   .append("  join fetch entity.uf ")
		   .append(" where entity.uf = :uf ")
		   .append(" order by entity.nome");
		
		Query query = getSession().createQuery(sql.toString()).setParameter("uf", uf);
		listaMunicipio = query.list();
		
		return listaMunicipio;
	}
	
	@Override
	public void validar(Municipio Municipio) {

	}

	@Override
	public void validarPersist(Municipio Municipio) {

	}

	@Override
	public void validarMerge(Municipio Municipio) {

	}

	@Override
	public void validarDelete(Municipio Municipio) {

	}

}
