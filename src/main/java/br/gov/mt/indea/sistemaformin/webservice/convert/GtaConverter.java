package br.gov.mt.indea.sistemaformin.webservice.convert;

import java.io.Serializable;
import java.util.ArrayList;

import javax.inject.Inject;

import br.gov.mt.indea.sistemaformin.entity.Municipio;
import br.gov.mt.indea.sistemaformin.entity.Pessoa;
import br.gov.mt.indea.sistemaformin.entity.UF;
import br.gov.mt.indea.sistemaformin.exception.ApplicationException;
import br.gov.mt.indea.sistemaformin.service.MunicipioService;
import br.gov.mt.indea.sistemaformin.service.PropriedadeService;
import br.gov.mt.indea.sistemaformin.service.UFService;
import br.gov.mt.indea.sistemaformin.webservice.entity.Endereco;
import br.gov.mt.indea.sistemaformin.webservice.entity.Especie;
import br.gov.mt.indea.sistemaformin.webservice.entity.Estratificacao;
import br.gov.mt.indea.sistemaformin.webservice.entity.EstratificacaoGta;
import br.gov.mt.indea.sistemaformin.webservice.entity.FinalidadeGta;
import br.gov.mt.indea.sistemaformin.webservice.entity.GrupoEspecie;
import br.gov.mt.indea.sistemaformin.webservice.entity.Gta;
import br.gov.mt.indea.sistemaformin.webservice.entity.PessoaDestinoGta;
import br.gov.mt.indea.sistemaformin.webservice.entity.PessoaOrigemGta;
import br.gov.mt.indea.sistemaformin.webservice.entity.Propriedade;
import br.gov.mt.indea.sistemaformin.webservice.entity.SituacaoGta;
import br.gov.mt.indea.sistemaformin.webservice.entity.TipoEmitente;
import br.gov.mt.indea.sistemaformin.webservice.entity.TipoGta;
import br.gov.mt.indea.sistemaformin.webservice.entity.TipoLogradouro;
import br.gov.mt.indea.sistemaformin.webservice.entity.TipoSaldo;

public class GtaConverter implements Serializable{
	
	private static final long serialVersionUID = 5803929349614827618L;

	@Inject
	private MunicipioService municipioService;
	
	@Inject
	private UFService ufService;
	
	@Inject
	private PropriedadeService propriedadeService;
	
	public Gta fromModelToEntity(br.gov.mt.indea.sistemaformin.webservice.model.Gta modelo) throws ApplicationException{
		if (modelo == null)
			return null;
		
		Gta gta = new Gta();
		
		gta.setSelecionadoParaInclusao(modelo.isSelecionadoParaInclusao());
		
		gta.setDataAtualizacao(modelo.getDataAtualizacao());
		gta.setDataCancelamento(modelo.getDataCancelamento());
		gta.setDataChegada(modelo.getDataChegada());
		gta.setDataDigitacaoEntrada(modelo.getDataDigitacaoEntrada());
		gta.setDataEmissao(modelo.getDataEmissao());
		gta.setDataSaida(modelo.getDataSaida());
		gta.setDataValidade(modelo.getDataValidade());
		gta.setNumero(modelo.getNumero());
		gta.setSerie(modelo.getSerie());
		
		if (modelo.getEmitente() != null){
			Pessoa emitente = new Pessoa();
			
			emitente.setApelido(modelo.getEmitente().getApelido());
			emitente.setCpf(modelo.getEmitente().getCpfCnpj());
			emitente.setNome(modelo.getEmitente().getNome());
			emitente.setEmail(modelo.getEmitente().getEmail());
			emitente.setTipoPessoa(modelo.getEmitente().getTipoPessoa());
			emitente.setCodigo(modelo.getEmitente().getCodigo());
			
			if (modelo.getEmitente().getEndereco() != null){
				Endereco enderecoEmitente = new Endereco();
				
				enderecoEmitente.setBairro(modelo.getEmitente().getEndereco().getBairro());
				enderecoEmitente.setCep(modelo.getEmitente().getEndereco().getCep());
				enderecoEmitente.setComplemento(modelo.getEmitente().getEndereco().getComplemento());
				enderecoEmitente.setLogradouro(modelo.getEmitente().getEndereco().getLogradouro());
				enderecoEmitente.setNumero(modelo.getEmitente().getEndereco().getNumero());
				enderecoEmitente.setReferencia(modelo.getEmitente().getEndereco().getReferencia());
				enderecoEmitente.setTelefone(modelo.getEmitente().getEndereco().getTelefone());
				
				if (modelo.getEmitente().getEndereco().getTipoLogradouro() != null){
					TipoLogradouro tipoLogradouroEnderecoPropriedade = new TipoLogradouro();
					tipoLogradouroEnderecoPropriedade.setNome(modelo.getEmitente().getEndereco().getTipoLogradouro().getNome());
					
					enderecoEmitente.setTipoLogradouro(tipoLogradouroEnderecoPropriedade);
				}
				
				if (modelo.getEmitente().getEndereco().getMunicipio() != null)
					enderecoEmitente.setMunicipio(this.findMunicipio(modelo.getEmitente().getEndereco().getMunicipio()));
				
				emitente.setEndereco(enderecoEmitente);
			}
			
			if (modelo.getEmitente().getPessoaFisica() != null){
				emitente.setDataNascimento(modelo.getEmitente().getPessoaFisica().getDataNascimento());
				emitente.setEmissorRg(modelo.getEmitente().getPessoaFisica().getEmissorRg());
				emitente.setMunicipioNascimento(this.findMunicipio(modelo.getEmitente().getPessoaFisica().getMunicipioNascimento()));
				emitente.setNomeMae(modelo.getEmitente().getPessoaFisica().getNomeMae());
				emitente.setRg(modelo.getEmitente().getPessoaFisica().getRg());
				emitente.setSexo(modelo.getEmitente().getPessoaFisica().getSexo());
			}
			
			
			gta.setEmitente(emitente);
		}
		
		if (modelo.getEspecie() != null){
			Especie especieGta = new Especie();
			
			if (modelo.getEspecie().getGrupoEspecie() != null){
				GrupoEspecie grupoEspecieGta = new GrupoEspecie();
				
				grupoEspecieGta.setNome(modelo.getEspecie().getGrupoEspecie().getNome());
				grupoEspecieGta.setId(modelo.getEspecie().getGrupoEspecie().getId());
				
				especieGta.setGrupoEspecie(grupoEspecieGta);
			}
			
			especieGta.setEsp_nome(modelo.getEspecie().getNome());
			
			gta.setEspecie(especieGta);
		}
		
		if (modelo.getFinalidade() != null){
			FinalidadeGta finalidadeGta = new FinalidadeGta();
			
			finalidadeGta.setAtualizaSaldoDestino(modelo.getFinalidade().getAtualizaSaldoDestino());
			finalidadeGta.setCodigoPga(modelo.getFinalidade().getCodigoPga());
			finalidadeGta.setDeAglomeracao(modelo.getFinalidade().getDeAglomeracao());
			finalidadeGta.setDeEstabelecimento(modelo.getFinalidade().getDeEstabelecimento());
			finalidadeGta.setDePropriedade(modelo.getFinalidade().getDePropriedade());
			finalidadeGta.setNome(modelo.getFinalidade().getNome());
			finalidadeGta.setParaAglomeracao(modelo.getFinalidade().getParaAglomeracao());
			finalidadeGta.setParaEstabelecimento(modelo.getFinalidade().getParaEstabelecimento());
			finalidadeGta.setParaPropriedade(modelo.getFinalidade().getParaPropriedade());
			
			gta.setFinalidade(finalidadeGta);
		}
		
		if (modelo.getGrupoEspecie() != null){
			GrupoEspecie grupoEspecieGta = new GrupoEspecie();
			
			grupoEspecieGta.setNome(modelo.getGrupoEspecie().getNome());
			
			gta.setGrupoEspecie(grupoEspecieGta);
		}
		
		if (modelo.getPessoaDestino() != null){
			PessoaDestinoGta destino = new PessoaDestinoGta();
			
			destino.setCnpjCpf(modelo.getPessoaDestino().getCnpjCpf());
			destino.setCodigoAglomeracao(modelo.getPessoaDestino().getCodigoAglomeracao());
			destino.setCodigoEstabelecimento(modelo.getPessoaDestino().getCodigoEstabelecimento());
			destino.setCodigoExploracao(modelo.getPessoaDestino().getCodigoExploracao());
			destino.setMunicipio(this.findMunicipio(modelo.getPessoaDestino().getMunicipio()));
			destino.setNomeEstabelecimento(modelo.getPessoaDestino().getNomeEstabelecimento());
			destino.setNomeProdutor(modelo.getPessoaDestino().getNomeProdutor());
			
			if (modelo.getPessoaDestino().getEstabelecimento() != null){
				Pessoa estabelecimentoDestino = new Pessoa();
				
				estabelecimentoDestino.setApelido(modelo.getPessoaDestino().getEstabelecimento().getApelido());
				estabelecimentoDestino.setCpf(modelo.getPessoaDestino().getEstabelecimento().getCpfCnpj());
				estabelecimentoDestino.setEmail(modelo.getPessoaDestino().getEstabelecimento().getEmail());
				estabelecimentoDestino.setNome(modelo.getPessoaDestino().getEstabelecimento().getNome());
				estabelecimentoDestino.setTipoPessoa(modelo.getPessoaDestino().getEstabelecimento().getTipoPessoa());
				estabelecimentoDestino.setCodigo(modelo.getPessoaDestino().getEstabelecimento().getCodigo());
				
				if (modelo.getPessoaDestino().getEstabelecimento().getEndereco() != null){
					Endereco enderecoEstabelecimentoDestino = new Endereco();
					
					enderecoEstabelecimentoDestino.setBairro(modelo.getPessoaDestino().getEstabelecimento().getEndereco().getBairro());
					enderecoEstabelecimentoDestino.setCep(modelo.getPessoaDestino().getEstabelecimento().getEndereco().getCep());
					enderecoEstabelecimentoDestino.setComplemento(modelo.getPessoaDestino().getEstabelecimento().getEndereco().getComplemento());
					enderecoEstabelecimentoDestino.setLogradouro(modelo.getPessoaDestino().getEstabelecimento().getEndereco().getLogradouro());
					enderecoEstabelecimentoDestino.setNumero(modelo.getPessoaDestino().getEstabelecimento().getEndereco().getNumero());
					enderecoEstabelecimentoDestino.setReferencia(modelo.getPessoaDestino().getEstabelecimento().getEndereco().getReferencia());
					enderecoEstabelecimentoDestino.setTelefone(modelo.getPessoaDestino().getEstabelecimento().getEndereco().getTelefone());
					
					if (modelo.getPessoaDestino().getEstabelecimento().getEndereco().getTipoLogradouro() != null){
						TipoLogradouro tipoLogradouroEnderecoPropriedade = new TipoLogradouro();
						tipoLogradouroEnderecoPropriedade.setNome(modelo.getPessoaDestino().getEstabelecimento().getEndereco().getTipoLogradouro().getNome());
						
						enderecoEstabelecimentoDestino.setTipoLogradouro(tipoLogradouroEnderecoPropriedade);
					}
					
					if (modelo.getPessoaDestino().getEstabelecimento().getEndereco().getMunicipio() != null)
						enderecoEstabelecimentoDestino.setMunicipio(this.findMunicipio(modelo.getPessoaDestino().getEstabelecimento().getEndereco().getMunicipio()));
					
					estabelecimentoDestino.setEndereco(enderecoEstabelecimentoDestino);
				}
				
				if (modelo.getPessoaDestino().getEstabelecimento().getPessoaFisica() != null){
					estabelecimentoDestino.setDataNascimento(modelo.getPessoaDestino().getEstabelecimento().getPessoaFisica().getDataNascimento());
					estabelecimentoDestino.setEmissorRg(modelo.getPessoaDestino().getEstabelecimento().getPessoaFisica().getEmissorRg());
					estabelecimentoDestino.setMunicipioNascimento(this.findMunicipio(modelo.getPessoaDestino().getEstabelecimento().getPessoaFisica().getMunicipioNascimento()));
					estabelecimentoDestino.setNomeMae(modelo.getPessoaDestino().getEstabelecimento().getPessoaFisica().getNomeMae());
					estabelecimentoDestino.setRg(modelo.getPessoaDestino().getEstabelecimento().getPessoaFisica().getRg());
					estabelecimentoDestino.setSexo(modelo.getPessoaDestino().getEstabelecimento().getPessoaFisica().getSexo());
				}
				
				destino.setEstabelecimento(estabelecimentoDestino);
			}
			
			if (modelo.getPessoaDestino().getProdutor() != null){
				Pessoa produtorDestino = new Pessoa();
				
				produtorDestino.setApelido(modelo.getPessoaDestino().getProdutor().getApelido());
				produtorDestino.setCpf(modelo.getPessoaDestino().getProdutor().getCpfCnpj());
				produtorDestino.setEmail(modelo.getPessoaDestino().getProdutor().getEmail());
				produtorDestino.setNome(modelo.getPessoaDestino().getProdutor().getNome());
				produtorDestino.setTipoPessoa(modelo.getPessoaDestino().getProdutor().getTipoPessoa());
				produtorDestino.setCodigo(modelo.getPessoaDestino().getProdutor().getCodigo());
				
				if (modelo.getPessoaDestino().getProdutor().getEndereco() != null){
					Endereco enderecoProdutorDestino = new Endereco();
					
					enderecoProdutorDestino.setBairro(modelo.getPessoaDestino().getProdutor().getEndereco().getBairro());
					enderecoProdutorDestino.setCep(modelo.getPessoaDestino().getProdutor().getEndereco().getCep());
					enderecoProdutorDestino.setComplemento(modelo.getPessoaDestino().getProdutor().getEndereco().getComplemento());
					enderecoProdutorDestino.setLogradouro(modelo.getPessoaDestino().getProdutor().getEndereco().getLogradouro());
					enderecoProdutorDestino.setNumero(modelo.getPessoaDestino().getProdutor().getEndereco().getNumero());
					enderecoProdutorDestino.setReferencia(modelo.getPessoaDestino().getProdutor().getEndereco().getReferencia());
					enderecoProdutorDestino.setTelefone(modelo.getPessoaDestino().getProdutor().getEndereco().getTelefone());
					
					if (modelo.getPessoaDestino().getProdutor().getEndereco().getTipoLogradouro() != null){
						TipoLogradouro tipoLogradouroEnderecoPropriedade = new TipoLogradouro();
						tipoLogradouroEnderecoPropriedade.setNome(modelo.getPessoaDestino().getProdutor().getEndereco().getTipoLogradouro().getNome());
						
						enderecoProdutorDestino.setTipoLogradouro(tipoLogradouroEnderecoPropriedade);
					}
					
					if (modelo.getPessoaDestino().getProdutor().getEndereco().getMunicipio() != null)
						enderecoProdutorDestino.setMunicipio(this.findMunicipio(modelo.getPessoaDestino().getProdutor().getEndereco().getMunicipio()));
					
					produtorDestino.setEndereco(enderecoProdutorDestino);
				}
				
				if (modelo.getPessoaDestino().getProdutor().getPessoaFisica() != null){
					produtorDestino.setDataNascimento(modelo.getPessoaDestino().getProdutor().getPessoaFisica().getDataNascimento());
					produtorDestino.setEmissorRg(modelo.getPessoaDestino().getProdutor().getPessoaFisica().getEmissorRg());
					produtorDestino.setMunicipioNascimento(this.findMunicipio(modelo.getPessoaDestino().getProdutor().getPessoaFisica().getMunicipioNascimento()));
					produtorDestino.setNomeMae(modelo.getPessoaDestino().getProdutor().getPessoaFisica().getNomeMae());
					produtorDestino.setRg(modelo.getPessoaDestino().getProdutor().getPessoaFisica().getRg());
					produtorDestino.setSexo(modelo.getPessoaDestino().getProdutor().getPessoaFisica().getSexo());
				}
				
				destino.setProdutor(produtorDestino);
			}
			
			gta.setPessoaDestino(destino);
		}
		
		if (modelo.getPessoaOrigemGta() != null){
			PessoaOrigemGta pessoaOrigem = new PessoaOrigemGta();
			
			Propriedade propriedade = null;
			
			try {
				propriedade = propriedadeService.findByCodigo(modelo.getPessoaOrigemGta().getCodigoEstabelecimento());
				pessoaOrigem.setPropriedade(propriedade);
				pessoaOrigem.setExploracao(propriedade.getExploracao(modelo.getPessoaOrigemGta().getCodigoExploracao()));
			} catch (ApplicationException e) {
				throw e;
			}
			
			pessoaOrigem.setCnpjCpf(modelo.getPessoaOrigemGta().getCnpjCpf());
			pessoaOrigem.setCodigoAglomeracao(modelo.getPessoaOrigemGta().getCodigoAglomeracao());
			pessoaOrigem.setMunicipio(this.findMunicipio(modelo.getPessoaOrigemGta().getMunicipio()));
			
			if (modelo.getPessoaOrigemGta().getEstabelecimento() != null){
				Pessoa estabelecimentoOrigemGta = new Pessoa();
				
				estabelecimentoOrigemGta.setApelido(modelo.getPessoaOrigemGta().getEstabelecimento().getApelido());
				estabelecimentoOrigemGta.setCpf(modelo.getPessoaOrigemGta().getEstabelecimento().getCpfCnpj());
				estabelecimentoOrigemGta.setEmail(modelo.getPessoaOrigemGta().getEstabelecimento().getEmail());
				estabelecimentoOrigemGta.setNome(modelo.getPessoaOrigemGta().getEstabelecimento().getNome());
				estabelecimentoOrigemGta.setTipoPessoa(modelo.getPessoaOrigemGta().getEstabelecimento().getTipoPessoa());
				estabelecimentoOrigemGta.setCodigo(modelo.getPessoaOrigemGta().getEstabelecimento().getCodigo());
				
				if (modelo.getPessoaOrigemGta().getEstabelecimento().getEndereco() != null){
					Endereco enderecoEstabelecimentoOrigemGta = new Endereco();
					
					enderecoEstabelecimentoOrigemGta.setBairro(modelo.getPessoaOrigemGta().getEstabelecimento().getEndereco().getBairro());
					enderecoEstabelecimentoOrigemGta.setCep(modelo.getPessoaOrigemGta().getEstabelecimento().getEndereco().getCep());
					enderecoEstabelecimentoOrigemGta.setComplemento(modelo.getPessoaOrigemGta().getEstabelecimento().getEndereco().getComplemento());
					enderecoEstabelecimentoOrigemGta.setLogradouro(modelo.getPessoaOrigemGta().getEstabelecimento().getEndereco().getLogradouro());
					enderecoEstabelecimentoOrigemGta.setNumero(modelo.getPessoaOrigemGta().getEstabelecimento().getEndereco().getNumero());
					enderecoEstabelecimentoOrigemGta.setReferencia(modelo.getPessoaOrigemGta().getEstabelecimento().getEndereco().getReferencia());
					enderecoEstabelecimentoOrigemGta.setTelefone(modelo.getPessoaOrigemGta().getEstabelecimento().getEndereco().getTelefone());
					
					if (modelo.getPessoaOrigemGta().getEstabelecimento().getEndereco().getTipoLogradouro() != null){
						TipoLogradouro tipoLogradouroEnderecoPropriedade = new TipoLogradouro();
						tipoLogradouroEnderecoPropriedade.setNome(modelo.getPessoaOrigemGta().getEstabelecimento().getEndereco().getTipoLogradouro().getNome());
						
						enderecoEstabelecimentoOrigemGta.setTipoLogradouro(tipoLogradouroEnderecoPropriedade);
					}
					
					if (modelo.getPessoaOrigemGta().getEstabelecimento().getEndereco().getMunicipio() != null)
						enderecoEstabelecimentoOrigemGta.setMunicipio(this.findMunicipio(modelo.getPessoaOrigemGta().getEstabelecimento().getEndereco().getMunicipio()));
					
					estabelecimentoOrigemGta.setEndereco(enderecoEstabelecimentoOrigemGta);
				}
				
			}
			
			if (modelo.getPessoaOrigemGta().getProdutor() != null){
				Pessoa produtorOrigemGta = new Pessoa();
				
				produtorOrigemGta.setApelido(modelo.getPessoaOrigemGta().getProdutor().getApelido());
				produtorOrigemGta.setCpf(modelo.getPessoaOrigemGta().getProdutor().getCpfCnpj());
				produtorOrigemGta.setEmail(modelo.getPessoaOrigemGta().getProdutor().getEmail());
				produtorOrigemGta.setNome(modelo.getPessoaOrigemGta().getProdutor().getNome());
				produtorOrigemGta.setTipoPessoa(modelo.getPessoaOrigemGta().getProdutor().getTipoPessoa());
				produtorOrigemGta.setCodigo(modelo.getPessoaOrigemGta().getProdutor().getCodigo());
				
				if (modelo.getPessoaOrigemGta().getProdutor().getEndereco() != null){
					Endereco enderecoProdutorOrigemGta = new Endereco();
					
					enderecoProdutorOrigemGta.setBairro(modelo.getPessoaOrigemGta().getProdutor().getEndereco().getBairro());
					enderecoProdutorOrigemGta.setCep(modelo.getPessoaOrigemGta().getProdutor().getEndereco().getCep());
					enderecoProdutorOrigemGta.setComplemento(modelo.getPessoaOrigemGta().getProdutor().getEndereco().getComplemento());
					enderecoProdutorOrigemGta.setLogradouro(modelo.getPessoaOrigemGta().getProdutor().getEndereco().getLogradouro());
					enderecoProdutorOrigemGta.setNumero(modelo.getPessoaOrigemGta().getProdutor().getEndereco().getNumero());
					enderecoProdutorOrigemGta.setReferencia(modelo.getPessoaOrigemGta().getProdutor().getEndereco().getReferencia());
					enderecoProdutorOrigemGta.setTelefone(modelo.getPessoaOrigemGta().getProdutor().getEndereco().getTelefone());
					
					if (modelo.getPessoaOrigemGta().getProdutor().getEndereco().getTipoLogradouro() != null){
						TipoLogradouro tipoLogradouroEnderecoPropriedade = new TipoLogradouro();
						tipoLogradouroEnderecoPropriedade.setNome(modelo.getPessoaOrigemGta().getProdutor().getEndereco().getTipoLogradouro().getNome());
						
						enderecoProdutorOrigemGta.setTipoLogradouro(tipoLogradouroEnderecoPropriedade);
					}
					
					if (modelo.getPessoaOrigemGta().getProdutor().getEndereco().getMunicipio() != null)
						enderecoProdutorOrigemGta.setMunicipio(this.findMunicipio(modelo.getPessoaOrigemGta().getProdutor().getEndereco().getMunicipio()));
					
					produtorOrigemGta.setEndereco(enderecoProdutorOrigemGta);
				}
				
				if (modelo.getPessoaOrigemGta().getProdutor().getPessoaFisica() != null){
					produtorOrigemGta.setDataNascimento(modelo.getPessoaOrigemGta().getProdutor().getPessoaFisica().getDataNascimento());
					produtorOrigemGta.setEmissorRg(modelo.getPessoaOrigemGta().getProdutor().getPessoaFisica().getEmissorRg());
					produtorOrigemGta.setMunicipioNascimento(this.findMunicipio(modelo.getPessoaOrigemGta().getProdutor().getPessoaFisica().getMunicipioNascimento()));
					produtorOrigemGta.setNomeMae(modelo.getPessoaOrigemGta().getProdutor().getPessoaFisica().getNomeMae());
					produtorOrigemGta.setRg(modelo.getPessoaOrigemGta().getProdutor().getPessoaFisica().getRg());
					produtorOrigemGta.setSexo(modelo.getPessoaOrigemGta().getProdutor().getPessoaFisica().getSexo());
				}
				
				pessoaOrigem.setProdutor(produtorOrigemGta);
			}
			
			gta.setPessoaOrigemGta(pessoaOrigem);
		}
		
		if (modelo.getSituacaoGta() != null){
			SituacaoGta situacaoGta = new SituacaoGta();
			
			situacaoGta.setCodigoPga(modelo.getSituacaoGta().getCodigoPga());
			situacaoGta.setId(modelo.getSituacaoGta().getId());
			situacaoGta.setNome(modelo.getSituacaoGta().getNome());
			
			gta.setSituacaoGta(situacaoGta);
		}

		if (modelo.getTipoEmitente() != null){
			TipoEmitente tipoEmitente = new TipoEmitente();
			
			tipoEmitente.setCodigoPga(modelo.getTipoEmitente().getCodigoPga());
			tipoEmitente.setId(modelo.getTipoEmitente().getId());
			tipoEmitente.setIdTable(modelo.getTipoEmitente().getIdTable());
			tipoEmitente.setNome(modelo.getTipoEmitente().getNome());
			
			gta.setTipoEmitente(tipoEmitente);
		}
		
		if (modelo.getTipoGta() != null){
			TipoGta tipoGta = new TipoGta();
			
			tipoGta.setId(modelo.getTipoGta().getId());
			tipoGta.setNome(modelo.getTipoGta().getNome());
			
			gta.setTipoGta(tipoGta);
		}
		
		if (modelo.getUfOrigem() != null)
			gta.setUfOrigem(this.findUf(modelo.getUfOrigem()));

		if (modelo.getUfDestino() != null)
			gta.setUfOrigem(this.findUf(modelo.getUfOrigem()));
		
		if (modelo.getEstratificacoes() != null && !modelo.getEstratificacoes().isEmpty()){
			gta.setEstratificacoes(new ArrayList<>());
			
			EstratificacaoGta estratificacaoGta;
			Estratificacao estratificacao;
			Especie especie;
			GrupoEspecie grupoEspecie;
			TipoSaldo tipoSaldo;
			for (br.gov.mt.indea.sistemaformin.webservice.model.EstratificacaoGta modelEstratificacaoGta : modelo.getEstratificacoes()) {
				estratificacaoGta = new EstratificacaoGta();
				estratificacaoGta.setGta(gta);
				
				if (modelEstratificacaoGta.getEstratificacao() != null){
					estratificacao = new Estratificacao();
					
					if (modelEstratificacaoGta.getEstratificacao().getEspecie() != null){
						especie = new Especie();
						
						if (modelEstratificacaoGta.getEstratificacao().getEspecie().getGrupoEspecie() != null){
							grupoEspecie = new GrupoEspecie();
							
							grupoEspecie.setNome(modelEstratificacaoGta.getEstratificacao().getEspecie().getGrupoEspecie().getNome());
							grupoEspecie.setId(modelEstratificacaoGta.getEstratificacao().getEspecie().getGrupoEspecie().getId());
							
							especie.setGrupoEspecie(grupoEspecie);
						}
						
						especie.setEsp_nome(modelEstratificacaoGta.getEstratificacao().getEspecie().getNome());
						
						estratificacao.setEspecie(especie);
					}
					
					estratificacao.setNome(modelEstratificacaoGta.getEstratificacao().getNome());
					estratificacao.setSexo(modelEstratificacaoGta.getEstratificacao().getSexo());
					
					estratificacaoGta.setEstratificacao(estratificacao);
				}
				
				estratificacaoGta.setItemValido(modelEstratificacaoGta.getItemValido());
				estratificacaoGta.setQntEnviada(modelEstratificacaoGta.getQntEnviada());
				estratificacaoGta.setQntRecebida(modelEstratificacaoGta.getQntRecebida());
				
//				if (modelEstratificacaoGta.getTipoSaldo() != null){
//					tipoSaldo = new TipoSaldo();
//					
//					tipoSaldo.setNome(modelEstratificacaoGta.getTipoSaldo().getNome());
//					tipoSaldo.setNomeReduzido(modelEstratificacaoGta.getTipoSaldo().getNomeReduzido());
//					
//					estratificacaoGta.setTipoSaldo(tipoSaldo);
//				}
				
				gta.getEstratificacoes().add(estratificacaoGta);
			}
		}
		
		
		return gta;
	}

	private Municipio findMunicipio(br.gov.mt.indea.sistemaformin.webservice.model.Municipio municipioModel){
		if (municipioModel == null)
			return null;
		
		String codgIBGE = municipioModel.getId() + "";
		
		Municipio municipio = municipioService.findByCodgIBGE(codgIBGE.substring(0, 2), codgIBGE.substring(2, codgIBGE.length()));
		
		return municipio;
	}
	
	private UF findUf(br.gov.mt.indea.sistemaformin.webservice.model.Uf ufModel){
		if (ufModel == null)
			return null;
		
		UF uf = ufService.findBySigla(ufModel.getSigla());
		
		return uf;
	}
	

}
