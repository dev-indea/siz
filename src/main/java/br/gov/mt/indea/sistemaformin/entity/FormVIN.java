package br.gov.mt.indea.sistemaformin.entity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

import br.gov.mt.indea.sistemaformin.enums.Dominio.SimNao;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoVinculoEpidemiologico;
import br.gov.mt.indea.sistemaformin.webservice.entity.Abatedouro;
import br.gov.mt.indea.sistemaformin.webservice.entity.Produtor;
import br.gov.mt.indea.sistemaformin.webservice.entity.Propriedade;

@Audited
@Entity
@Table(name="form_vin")
public class FormVIN extends BaseEntity<Long>{
	
	private static final long serialVersionUID = 4494710625023232072L;

	@Id
	@SequenceGenerator(name="form_vin_seq", sequenceName="form_vin_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="form_vin_seq")
	private Long id;

	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_usuario")
	private Usuario usuario;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_cadastro")
	private Calendar dataCadastro;
	
	@ManyToOne
	@JoinColumn(name="id_formin")
	private FormIN formIN;

	@Column(name="numero_inspecao")
	private Long numeroInspecao;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_inspecao")
	private Calendar dataInspecao;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="id_propriedade")
	private Propriedade propriedade;
	
	@OneToOne(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH}, fetch=FetchType.LAZY)
	@JoinColumn(name="id_proprietario")
    private Produtor proprietario;
	
	@Column(name="codigo_propriedade")
    private Long codigoPropriedade;
	
	@Column(name = "nome_propriedade")
    private String nomePropriedade;
	
	@OneToOne(cascade={CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH}, orphanRemoval=true, fetch=FetchType.LAZY)
	@JoinColumn(name="id_abatedouro")
	private Abatedouro abatedouro;
	
	@OneToOne(cascade={CascadeType.ALL}, orphanRemoval=true)
	@JoinColumn(name="id_pessoa")
	private RepresentanteEstabelecimento representanteEstabelecimento = new RepresentanteEstabelecimento();
	
	@ElementCollection(targetClass = TipoVinculoEpidemiologico.class)
	@JoinTable(name = "formvin_vinculo_epidemiologico", joinColumns = @JoinColumn(name = "id_form_vin", nullable = false))
	@Column(name = "id_formvin_vinculo_epidemiologico")
	@Enumerated(EnumType.STRING)
	private List<TipoVinculoEpidemiologico> vinculoEpidemiologico = new ArrayList<TipoVinculoEpidemiologico>();
	
	@OneToMany(mappedBy="formVIN", cascade={CascadeType.ALL}, orphanRemoval=true)
	private List<VistoriaFormVIN> vistoriasFormVIN = new ArrayList<VistoriaFormVIN>();
	
	@OneToMany(mappedBy="formVIN", cascade={CascadeType.ALL}, orphanRemoval=true)
	private List<NovoEstabelecimentoParaInvestigacao> novoEstabelecimentoParaInvestigacaos = new ArrayList<NovoEstabelecimentoParaInvestigacao>();
	
	@Enumerated(EnumType.STRING)
	@Column(name="ausencia_de_animais_doentes")
	private SimNao ausenciaDeAnimaisCompativaeisComADoencaInvestigada;
	
	@Column(length=4000)
	private String observacoes;
	
	@OneToOne(cascade={CascadeType.ALL}, orphanRemoval=true)
	@JoinColumn(name="id_veterinario")
	private br.gov.mt.indea.sistemaformin.webservice.entity.Veterinario veterinario;
	
	public String getResumoObservacoes(){
		if (this.observacoes == null)
			return null;
		
		StringBuilder sb = new StringBuilder(this.observacoes);
		
		if (sb.length() < 50)
			return sb.toString();
		else
			return sb.substring(0, 47) + "...";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Calendar getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Calendar dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public FormIN getFormIN() {
		return formIN;
	}

	public void setFormIN(FormIN formIN) {
		this.formIN = formIN;
	}

	public Long getNumeroInspecao() {
		return numeroInspecao;
	}

	public void setNumeroInspecao(Long numeroInspecao) {
		this.numeroInspecao = numeroInspecao;
	}

	public Calendar getDataInspecao() {
		return dataInspecao;
	}

	public void setDataInspecao(Calendar dataInspecao) {
		this.dataInspecao = dataInspecao;
	}

	public List<TipoVinculoEpidemiologico> getVinculoEpidemiologico() {
		return vinculoEpidemiologico;
	}

	public void setVinculoEpidemiologico(
			List<TipoVinculoEpidemiologico> vinculoEpidemiologico) {
		this.vinculoEpidemiologico = vinculoEpidemiologico;
	}

	public String getObservacoes() {
		return observacoes;
	}

	public void setObservacoes(String observacoes) {
		this.observacoes = observacoes;
	}

	public List<VistoriaFormVIN> getVistoriasFormVIN() {
		return vistoriasFormVIN;
	}

	public void setVistoriasFormVIN(List<VistoriaFormVIN> vistoriasFormVIN) {
		this.vistoriasFormVIN = vistoriasFormVIN;
	}

	public List<NovoEstabelecimentoParaInvestigacao> getNovoEstabelecimentoParaInvestigacaos() {
		return novoEstabelecimentoParaInvestigacaos;
	}

	public void setNovoEstabelecimentoParaInvestigacaos(
			List<NovoEstabelecimentoParaInvestigacao> novoEstabelecimentosParaInvestigacaos) {
		this.novoEstabelecimentoParaInvestigacaos = novoEstabelecimentosParaInvestigacaos;
	}

	public SimNao getAusenciaDeAnimaisCompativaeisComADoencaInvestigada() {
		return ausenciaDeAnimaisCompativaeisComADoencaInvestigada;
	}

	public void setAusenciaDeAnimaisCompativaeisComADoencaInvestigada(
			SimNao ausenciaDeAnimaisCompativaeisComADoencaInvestigada) {
		this.ausenciaDeAnimaisCompativaeisComADoencaInvestigada = ausenciaDeAnimaisCompativaeisComADoencaInvestigada;
	}

	public br.gov.mt.indea.sistemaformin.webservice.entity.Veterinario getVeterinario() {
		return veterinario;
	}

	public void setVeterinario(
			br.gov.mt.indea.sistemaformin.webservice.entity.Veterinario veterinario) {
		this.veterinario = veterinario;
	}

	public Propriedade getPropriedade() {
		return propriedade;
	}

	public void setPropriedade(Propriedade propriedade) {
		this.propriedade = propriedade;
	}

	public RepresentanteEstabelecimento getRepresentanteEstabelecimento() {
		return representanteEstabelecimento;
	}

	public void setRepresentanteEstabelecimento(
			RepresentanteEstabelecimento representanteEstabelecimento) {
		this.representanteEstabelecimento = representanteEstabelecimento;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Abatedouro getAbatedouro() {
		return abatedouro;
	}

	public void setAbatedouro(Abatedouro abatedouro) {
		this.abatedouro = abatedouro;
	}

	public Produtor getProprietario() {
		return proprietario;
	}

	public void setProprietario(Produtor proprietario) {
		this.proprietario = proprietario;
	}

	public Long getCodigoPropriedade() {
		return codigoPropriedade;
	}

	public void setCodigoPropriedade(Long codigoPropriedade) {
		this.codigoPropriedade = codigoPropriedade;
	}

	public String getNomePropriedade() {
		return nomePropriedade;
	}

	public void setNomePropriedade(String nomePropriedade) {
		this.nomePropriedade = nomePropriedade;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FormVIN other = (FormVIN) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}