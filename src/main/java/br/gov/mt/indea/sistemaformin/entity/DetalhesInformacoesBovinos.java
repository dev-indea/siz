package br.gov.mt.indea.sistemaformin.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.envers.Audited;

@Audited
@Entity
@DiscriminatorValue("bovinos")
public class DetalhesInformacoesBovinos extends AbstractDetalhesInformacoesDeAnimais implements Cloneable{
	
	private static final long serialVersionUID = 1355044082455412390L;

	@ManyToOne
	@JoinColumn(name="id_informacoes_bovinos")
	private InformacoesBovinos informacoesBovinos;

	public InformacoesBovinos getInformacoesBovinos() {
		return informacoesBovinos;
	}

	public void setInformacoesBovinos(InformacoesBovinos informacoesBovinos) {
		this.informacoesBovinos = informacoesBovinos;
	}
	
	protected Object clone(InformacoesBovinos informacoesBovinos) throws CloneNotSupportedException{
		DetalhesInformacoesBovinos cloned = (DetalhesInformacoesBovinos) super.clone();
		
		cloned.setId(null);
		cloned.setInformacoesBovinos(informacoesBovinos);
		
		return cloned;
	}
	
}