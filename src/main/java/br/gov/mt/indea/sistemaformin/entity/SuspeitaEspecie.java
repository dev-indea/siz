package br.gov.mt.indea.sistemaformin.entity;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

import br.gov.mt.indea.sistemaformin.enums.Dominio.Especie;

@Audited
@Entity
@Table(name="suspeita_especie")
public class SuspeitaEspecie extends BaseEntity<Long>{

	private static final long serialVersionUID = 1331282233749031338L;

	@Id
	@SequenceGenerator(name="suspeita_especie_seq", sequenceName="suspeita_especie_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="suspeita_especie_seq")
	private Long id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_cadastro")
	private Calendar dataCadastro;
	
	@ManyToOne
	@JoinColumn(name="id_suspeita_doenca_animal")
	private SuspeitaDoencaAnimal suspeitaDoencaAnimal;

	@Enumerated(EnumType.STRING)
	@Column(name="especie")
	private Especie especie;
	
	@Column(name="quantidade_total")
	private Long quantidadeTotal;
	
	@Column(name="quantidade_doentes")
	private Long quantidadeDoentes;
	
	@Column(name="quantidade_mortos")
	private Long quantidadeMortos;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_inicio_sinais_clinicos")
	private Calendar dataInicioDosSinaisClinicos;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Calendar getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Calendar dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public SuspeitaDoencaAnimal getSuspeitaDoencaAnimal() {
		return suspeitaDoencaAnimal;
	}

	public void setSuspeitaDoencaAnimal(SuspeitaDoencaAnimal suspeitaDoencaAnimal) {
		this.suspeitaDoencaAnimal = suspeitaDoencaAnimal;
	}

	public Especie getEspecie() {
		return especie;
	}

	public void setEspecie(Especie especie) {
		this.especie = especie;
	}

	public Long getQuantidadeTotal() {
		return quantidadeTotal;
	}

	public void setQuantidadeTotal(Long quantidadeTotal) {
		this.quantidadeTotal = quantidadeTotal;
	}

	public Long getQuantidadeDoentes() {
		return quantidadeDoentes;
	}

	public void setQuantidadeDoentes(Long quantidadeDoentes) {
		this.quantidadeDoentes = quantidadeDoentes;
	}

	public Long getQuantidadeMortos() {
		return quantidadeMortos;
	}

	public void setQuantidadeMortos(Long quantidadeMortos) {
		this.quantidadeMortos = quantidadeMortos;
	}

	public Calendar getDataInicioDosSinaisClinicos() {
		return dataInicioDosSinaisClinicos;
	}

	public void setDataInicioDosSinaisClinicos(Calendar dataInicioDosSinaisClinicos) {
		this.dataInicioDosSinaisClinicos = dataInicioDosSinaisClinicos;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SuspeitaEspecie other = (SuspeitaEspecie) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
