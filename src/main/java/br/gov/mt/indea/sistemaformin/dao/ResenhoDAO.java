package br.gov.mt.indea.sistemaformin.dao;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.inject.Inject;


import br.gov.mt.indea.sistemaformin.entity.Resenho;
import br.gov.mt.indea.sistemaformin.exception.ApplicationException;

@Stateless
public class ResenhoDAO extends DAO<Resenho> {
	
	@Inject
	private EntityManager em;
	
	public ResenhoDAO() {
		super(Resenho.class);
	}

	@Override
	public EntityManager getEntityManager() {
		return this.em;
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void add(Resenho resenho) throws ApplicationException{
		resenho = this.getEntityManager().merge(resenho);
		super.add(resenho);
	}

}
