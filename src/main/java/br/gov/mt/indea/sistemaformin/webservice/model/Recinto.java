package br.gov.mt.indea.sistemaformin.webservice.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Recinto implements Serializable {

	private static final long serialVersionUID = -4118158514275008492L;

	@XmlElement
	private Pessoa pessoa;
	
	@XmlElement
    private Pessoa responsavel;
    
    @XmlElement
    private Veterinario responsavelTecnico;
    
    @XmlElement
    private Municipio municipio;
    
    @XmlElement
    private ULE ule;
    
    private String enderecoRecinto;
    
    private String viaAcesso;
    
    private String observacao;

    public Pessoa getResponsavel() {
        return responsavel;
    }

    public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	public void setResponsavel(Pessoa responsavel) {
        this.responsavel = responsavel;
    }

    public Veterinario getResponsavelTecnico() {
        return responsavelTecnico;
    }

    public void setResponsavelTecnico(Veterinario responsavelTecnico) {
        this.responsavelTecnico = responsavelTecnico;
    }

    public Municipio getMunicipio() {
        return municipio;
    }

    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }

    public ULE getUle() {
        return ule;
    }

    public void setUle(ULE ule) {
        this.ule = ule;
    }

    public String getEnderecoRecinto() {
        return enderecoRecinto;
    }

    public void setEnderecoRecinto(String enderecoRecinto) {
        this.enderecoRecinto = enderecoRecinto;
    }

    public String getViaAcesso() {
        return viaAcesso;
    }

    public void setViaAcesso(String viaAcesso) {
        this.viaAcesso = viaAcesso;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }
}