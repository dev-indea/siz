package br.gov.mt.indea.sistemaformin.managedbeans;

import java.io.IOException;
import java.io.Serializable;
import java.util.Calendar;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLBeanName;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;

import br.gov.mt.indea.sistemaformin.entity.TipoChavePrincipalTermoFiscalizacao;
import br.gov.mt.indea.sistemaformin.enums.Dominio.AtivoInativo;
import br.gov.mt.indea.sistemaformin.service.LazyObjectDataModel;
import br.gov.mt.indea.sistemaformin.service.TipoChavePrincipalTermoFiscalizacaoService;
import br.gov.mt.indea.sistemaformin.util.FacesMessageUtil;

@Named
@ViewScoped
@URLBeanName("tipoChavePrincipalTermoFiscalizacaoManagedBean")
@URLMappings(mappings = {
		@URLMapping(id = "pesquisarTipoChavePrincipalTermoFiscalizacao", pattern = "/tipoChavePrincipalTermoFiscalizacao/pesquisar", viewId = "/pages/tipoChavePrincipalTermoFiscalizacao/lista.jsf"),
		@URLMapping(id = "incluirTipoChavePrincipalTermoFiscalizacao", pattern = "/tipoChavePrincipalTermoFiscalizacao/incluir", viewId = "/pages/tipoChavePrincipalTermoFiscalizacao/novo.jsf"),
		@URLMapping(id = "alterarTipoChavePrincipalTermoFiscalizacao", pattern = "/tipoChavePrincipalTermoFiscalizacao/alterar/#{tipoChavePrincipalTermoFiscalizacaoManagedBean.id}", viewId = "/pages/tipoChavePrincipalTermoFiscalizacao/novo.jsf")})
public class TipoChavePrincipalTermoFiscalizacaoManagedBean implements Serializable {
	
	private static final long serialVersionUID = -7409333626528791621L;

	private Long id;

	@Inject
	private TipoChavePrincipalTermoFiscalizacaoService tipoChavePrincipalTermoFiscalizacaoService;
	
	@Inject
	private TipoChavePrincipalTermoFiscalizacao tipoChavePrincipalTermoFiscalizacao;
	
	private LazyObjectDataModel<TipoChavePrincipalTermoFiscalizacao> listaTipoChavePrincipalTermoFiscalizacao;
	
	@PostConstruct
	public void init(){
		this.listaTipoChavePrincipalTermoFiscalizacao = new LazyObjectDataModel<TipoChavePrincipalTermoFiscalizacao>(this.tipoChavePrincipalTermoFiscalizacaoService);
	}
	
	public void limpar(){
		this.tipoChavePrincipalTermoFiscalizacao = new TipoChavePrincipalTermoFiscalizacao();
	}
	
	@URLAction(mappingId = "incluirTipoChavePrincipalTermoFiscalizacao", onPostback = false)
	public void novo(){
		limpar();
	}
	
	@URLAction(mappingId = "alterarTipoChavePrincipalTermoFiscalizacao", onPostback = false)
	public void editar(){
		this.tipoChavePrincipalTermoFiscalizacao = tipoChavePrincipalTermoFiscalizacaoService.findByIdFetchAll(this.getId());
	}
	
	public String adicionar() throws IOException{
		
		if (tipoChavePrincipalTermoFiscalizacao.getId() != null){
			this.tipoChavePrincipalTermoFiscalizacaoService.saveOrUpdate(tipoChavePrincipalTermoFiscalizacao);
			FacesMessageUtil.addInfoContextFacesMessage("Tipo chave principal atualizado", "");
		}else{
			this.tipoChavePrincipalTermoFiscalizacao.setDataCadastro(Calendar.getInstance());
			this.tipoChavePrincipalTermoFiscalizacao.setStatus(AtivoInativo.ATIVO);
			this.tipoChavePrincipalTermoFiscalizacaoService.saveOrUpdate(tipoChavePrincipalTermoFiscalizacao);
			FacesMessageUtil.addInfoContextFacesMessage("Tipo chave principal adicionado", "");
		}
		
		this.tipoChavePrincipalTermoFiscalizacao = new TipoChavePrincipalTermoFiscalizacao();
		
		return "pretty:pesquisarTipoChavePrincipalTermoFiscalizacao";
	}
	
	public void remover(TipoChavePrincipalTermoFiscalizacao tipoChavePrincipalTermoFiscalizacao){
		this.tipoChavePrincipalTermoFiscalizacaoService.delete(tipoChavePrincipalTermoFiscalizacao);
		FacesMessageUtil.addInfoContextFacesMessage("Tipo chave principal exclu�do com sucesso", "");
	}
	
	public void ativar(TipoChavePrincipalTermoFiscalizacao tipoChavePrincipalTermoFiscalizacao){
		tipoChavePrincipalTermoFiscalizacao.setStatus(AtivoInativo.ATIVO);
		
		this.tipoChavePrincipalTermoFiscalizacaoService.saveOrUpdate(tipoChavePrincipalTermoFiscalizacao);
		FacesMessageUtil.addInfoContextFacesMessage("Tipo chave principal ativado", "");
	}
	
	public void desativar(TipoChavePrincipalTermoFiscalizacao tipoChavePrincipalTermoFiscalizacao){
		tipoChavePrincipalTermoFiscalizacao.setStatus(AtivoInativo.INATIVO);
		
		this.tipoChavePrincipalTermoFiscalizacaoService.saveOrUpdate(tipoChavePrincipalTermoFiscalizacao);
		FacesMessageUtil.addInfoContextFacesMessage("Tipo chave principal desativado", "");
	}
	
	public boolean isAtivado(TipoChavePrincipalTermoFiscalizacao tipoChavePrincipalTermoFiscalizacao){
		return tipoChavePrincipalTermoFiscalizacao.getStatus().equals(AtivoInativo.ATIVO);
	}
	
	public TipoChavePrincipalTermoFiscalizacao getTipoChavePrincipalTermoFiscalizacao() {
		return tipoChavePrincipalTermoFiscalizacao;
	}

	public void setTipoChavePrincipalTermoFiscalizacao(TipoChavePrincipalTermoFiscalizacao tipoChavePrincipalTermoFiscalizacao) {
		this.tipoChavePrincipalTermoFiscalizacao = tipoChavePrincipalTermoFiscalizacao;
	}

	public LazyObjectDataModel<TipoChavePrincipalTermoFiscalizacao> getListaTipoChavePrincipalTermoFiscalizacao() {
		return listaTipoChavePrincipalTermoFiscalizacao;
	}

	public void setListaTipoChavePrincipalTermoFiscalizacao(
			LazyObjectDataModel<TipoChavePrincipalTermoFiscalizacao> listaTipoChavePrincipalTermoFiscalizacao) {
		this.listaTipoChavePrincipalTermoFiscalizacao = listaTipoChavePrincipalTermoFiscalizacao;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
