package br.gov.mt.indea.sistemaformin.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;

import org.hibernate.envers.Audited;

import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoDestinoExploracao;

@Audited
@Entity
@DiscriminatorValue("outros")
public class InformacoesOutrosAnimais extends AbstractInformacoesDeAnimais implements Cloneable{

	private static final long serialVersionUID = 5555757893786215468L;

	@Column(length=255)
	private String nome;

	@OneToMany(mappedBy = "informacoesOutrosAnimais", cascade={CascadeType.ALL}, orphanRemoval=true)
	@OrderBy("id")
	private List<DetalhesInformacoesOutrosAnimais> detalhes = new ArrayList<DetalhesInformacoesOutrosAnimais>();

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<DetalhesInformacoesOutrosAnimais> getDetalhes() {
		return detalhes;
	}

	public void setDetalhes(List<DetalhesInformacoesOutrosAnimais> detalhes) {
		this.detalhes = detalhes;
	}

	@Override
	protected List<AbstractDetalhesInformacoesDeAnimais> getDetalhesInformacoesDeAnimais() {
		List<AbstractDetalhesInformacoesDeAnimais> lista = new ArrayList<AbstractDetalhesInformacoesDeAnimais>();
		lista.addAll(this.detalhes);
		return lista;
	}
	
	protected Object clone(FormIN formIN) throws CloneNotSupportedException{
		InformacoesOutrosAnimais cloned = (InformacoesOutrosAnimais) super.clone();
		
		cloned.setId(null);
		
		if (this.getDestinos() != null){
			cloned.setDestinos(new ArrayList<TipoDestinoExploracao>());
			for (TipoDestinoExploracao destino : this.getDestinos()) {
				cloned.getDestinos().add(destino);
			}
		}
		
		if (this.detalhes != null){
			cloned.setDetalhes(new ArrayList<DetalhesInformacoesOutrosAnimais>());
			for (DetalhesInformacoesOutrosAnimais detalhe : this.detalhes) {
				cloned.getDetalhes().add((DetalhesInformacoesOutrosAnimais) detalhe.clone(cloned));
			}
		}
		
		cloned.setFormIN(formIN);
		
		return cloned;
	}

}
