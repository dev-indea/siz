package br.gov.mt.indea.sistemaformin.webservice.model;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class AglomeracaoSimple implements Serializable {

	private static final long serialVersionUID = 1685281012229039785L;

	private Long id;
    
    @XmlElement
    private Recinto recinto;
    
    @XmlElement
    private TipoEvento tipoEvento;
    
    @XmlElement
    private CondicaoGta condicaoGta;
    
    private String nome;
    
    private Date dataInicio;
    
    private Date dataTermino;
    
    private Date dataInspecao;
    
    private String eventoLiberado;
    
    private String motivoNaoLiberacao;
    
    private String codigo;
    
    private Date dataEncerramento;
    
    private String obsEncerramento;
    
    private Date dataCadastro;
    
    private Date dataAtualizacao;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Recinto getRecinto() {
        return recinto;
    }

    public void setRecinto(Recinto recinto) {
        this.recinto = recinto;
    }

    public TipoEvento getTipoEvento() {
        return tipoEvento;
    }

    public void setTipoEvento(TipoEvento tipoEvento) {
        this.tipoEvento = tipoEvento;
    }

    public CondicaoGta getCondicaoGta() {
        return condicaoGta;
    }

    public void setCondicaoGta(CondicaoGta condicaoGta) {
        this.condicaoGta = condicaoGta;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Date getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(Date dataInicio) {
        this.dataInicio = dataInicio;
    }

    public Date getDataTermino() {
        return dataTermino;
    }

    public void setDataTermino(Date dataTermino) {
        this.dataTermino = dataTermino;
    }

    public Date getDataInspecao() {
        return dataInspecao;
    }

    public void setDataInspecao(Date dataInspecao) {
        this.dataInspecao = dataInspecao;
    }

    public String getEventoLiberado() {
        return eventoLiberado;
    }

    public void setEventoLiberado(String eventoLiberado) {
        this.eventoLiberado = eventoLiberado;
    }

    public String getMotivoNaoLiberacao() {
        return motivoNaoLiberacao;
    }

    public void setMotivoNaoLiberacao(String motivoNaoLiberacao) {
        this.motivoNaoLiberacao = motivoNaoLiberacao;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Date getDataEncerramento() {
        return dataEncerramento;
    }

    public void setDataEncerramento(Date dataEncerramento) {
        this.dataEncerramento = dataEncerramento;
    }

    public String getObsEncerramento() {
        return obsEncerramento;
    }

    public void setObsEncerramento(String obsEncerramento) {
        this.obsEncerramento = obsEncerramento;
    }

    public Date getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(Date dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

    public Date getDataAtualizacao() {
        return dataAtualizacao;
    }

    public void setDataAtualizacao(Date dataAtualizacao) {
        this.dataAtualizacao = dataAtualizacao;
    }
}
