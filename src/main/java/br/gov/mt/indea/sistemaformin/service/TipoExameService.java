package br.gov.mt.indea.sistemaformin.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.mt.indea.sistemaformin.entity.TipoExame;
import br.gov.mt.indea.sistemaformin.util.StringUtil;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class TipoExameService extends PaginableService<TipoExame, Long> {
	
	private static final Logger log = LoggerFactory.getLogger(TipoExameService.class);

	protected TipoExameService() {
		super(TipoExame.class);
	}
	
	public TipoExame findByIdFetchAll(Long id){
		TipoExame tipoExame;
		
		StringBuilder sql = new StringBuilder();
		sql.append("  from TipoExame tipoExame ")
		   .append(" where tipoExame.id = :id ");
		
		Query query = getSession().createQuery(sql.toString()).setLong("id", id);
		tipoExame = (TipoExame) query.uniqueResult();
		
		return tipoExame;
	}
	
	@SuppressWarnings("unchecked")
	public List<TipoExame> findAll(String nome){
		StringBuilder sql = new StringBuilder();
		sql.append("  from TipoExame tipoExame ")
		   .append(" where lower(remove_acento(tipoExame.nome)) like :nome ")
		   .append(" order by tipoExame.nome");
		
		Query query = getSession().createQuery(sql.toString()).setString("nome", '%' + StringUtil.removeAcentos(nome.toLowerCase() + '%'));
		
		return query.list();
	}
	
	@Override
	public void saveOrUpdate(TipoExame tipoExame) {
		super.saveOrUpdate(tipoExame);
		
		log.info("Salvando Tipo Exame {}", tipoExame.getId());
	}
	
	@Override
	public void delete(TipoExame tipoExame) {
		super.delete(tipoExame);
		
		log.info("Removendo Tipo Exame {}", tipoExame.getId());
	}
	
	@Override
	public void validar(TipoExame TipoExame) {

	}

	@Override
	public void validarPersist(TipoExame TipoExame) {

	}

	@Override
	public void validarMerge(TipoExame TipoExame) {

	}

	@Override
	public void validarDelete(TipoExame TipoExame) {

	}

}
