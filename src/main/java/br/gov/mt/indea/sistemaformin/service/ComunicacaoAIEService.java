package br.gov.mt.indea.sistemaformin.service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.mt.indea.sistemaformin.entity.ComunicacaoAIE;
import br.gov.mt.indea.sistemaformin.entity.DetalheComunicacaoAIE;
import br.gov.mt.indea.sistemaformin.entity.Laboratorio;
import br.gov.mt.indea.sistemaformin.entity.Municipio;
import br.gov.mt.indea.sistemaformin.entity.dto.AbstractDTO;
import br.gov.mt.indea.sistemaformin.entity.dto.ComunicacaoAIEDTO;


@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class ComunicacaoAIEService extends PaginableService<ComunicacaoAIE, Long> {
	
	private static final Logger log = LoggerFactory.getLogger(ComunicacaoAIEService.class);

	protected ComunicacaoAIEService() {
		super(ComunicacaoAIE.class);
	}
	
	public ComunicacaoAIE findByIdFetchAll(Long id){
		ComunicacaoAIE comunicacaoAIE;
		
		StringBuilder sql = new StringBuilder();
		sql.append("  from ComunicacaoAIE comunicacaoAIE ")
		   .append("  join fetch comunicacaoAIE.laboratorio laboratorio")
		   .append("  join fetch comunicacaoAIE.listaDetalheComunicacaoAIE")
		   .append(" where comunicacaoAIE.id = :id ");
		
		Query query = getSession().createQuery(sql.toString()).setLong("id", id);
		comunicacaoAIE = (ComunicacaoAIE) query.uniqueResult();
		
		if (comunicacaoAIE != null){

			
		}
		
		return comunicacaoAIE;
	}
	
	@SuppressWarnings("unchecked")
	public List<ComunicacaoAIE> findAll(Laboratorio laboratorio){
		if (laboratorio == null)
			return null;
		
		List<ComunicacaoAIE> comunicacaoAIE;
		
		StringBuilder sql = new StringBuilder();
		sql.append("  from ComunicacaoAIE comunicacaoAIE ")
		   .append("  left join fetch comunicacaoAIE.laboratorio laboratorio")
		   .append("  left join fetch comunicacaoAIE.listaDetalheComunicacaoAIE")
		   .append(" where laboratorio.id = :id ");
		
		Query query = getSession().createQuery(sql.toString()).setLong("id", laboratorio.getId());
		comunicacaoAIE = (List<ComunicacaoAIE>) query.list();
		
		return comunicacaoAIE;
	}
	
	@SuppressWarnings("unchecked")
	public List<ComunicacaoAIE> findAll(Laboratorio laboratorio, Date dataComunicacao){
		if (laboratorio == null)
			return null;
		
		List<ComunicacaoAIE> comunicacaoAIE;
		
		StringBuilder sql = new StringBuilder();
		sql.append("  from ComunicacaoAIE comunicacaoAIE ")
		   .append("  left join fetch comunicacaoAIE.laboratorio laboratorio")
		   .append(" where laboratorio.id = :id ")
		   .append("   and to_char(comunicacaoAIE.dataComunicacao, 'YYYY-MM-DD HH:MI:SS') between :inicio and :final");
		
		Query query = getSession().createQuery(sql.toString());
		query.setLong("id", laboratorio.getId());
		
		Calendar i = Calendar.getInstance();
		i.setTime(dataComunicacao);
		
		StringBuilder sbI = new StringBuilder();
		sbI.append(i.get(Calendar.YEAR)).append("-")
		   .append(((i.get(Calendar.MONTH) + 1) + "").length() > 1 ? (i.get(Calendar.MONTH)+1) : "0" + (i.get(Calendar.MONTH)+1)).append("-")
		   .append("01")
		   .append(" ")
		   .append("00").append(":")
		   .append("00").append(":")
		   .append("00");
		
		Calendar f = Calendar.getInstance();
		f.setTime(dataComunicacao);
		
		StringBuilder sbF = new StringBuilder();
		sbF.append(f.get(Calendar.YEAR)).append("-")
		   .append(((f.get(Calendar.MONTH) + 1) + "").length() > 1 ? (f.get(Calendar.MONTH)+1) : "0" + (f.get(Calendar.MONTH)+1)).append("-")
		   .append(f.getActualMaximum(Calendar.DAY_OF_MONTH))
		   .append(" ")
		   .append("23").append(":")
		   .append("23").append(":")
		   .append("59");
		
		query.setString("inicio", sbI.toString());
		query.setString("final", sbF.toString());
		
		comunicacaoAIE = (List<ComunicacaoAIE>) query.list();
		
		return comunicacaoAIE;
	}
	
	@SuppressWarnings("unchecked")
	public List<DetalheComunicacaoAIE> findAll(Municipio municipio){
		if (municipio == null)
			return null;
		
		List<DetalheComunicacaoAIE> comunicacaoAIE;
		
		StringBuilder sql = new StringBuilder();
		sql.append("  from DetalheComunicacaoAIE detalhe ")
		   .append("  left join fetch detalhe.comunicacaoAIE comunicacaoAIE")
		   .append("  left join fetch comunicacaoAIE.laboratorio")
		   .append("  left join fetch detalhe.municipio municipio")
		   .append(" where municipio.id = :id ");
		
		Query query = getSession().createQuery(sql.toString()).setLong("id", municipio.getId());
		comunicacaoAIE = (List<DetalheComunicacaoAIE>) query.list();
		
		return comunicacaoAIE;
	}
	
	@SuppressWarnings("unchecked")
	public List<Date> findAllMonths(Laboratorio laboratorio){
		if (laboratorio == null)
			return null;
		
		List<Date> listaDatas;
		
		StringBuilder sql = new StringBuilder();
		sql.append("select distinct comunicacaoAIE.dataComunicacao")
		   .append("  from ComunicacaoAIE comunicacaoAIE ")
		   .append(" where laboratorio.id = :id ")
		   .append(" order by comunicacaoAIE.dataComunicacao ");
		
		Query query = getSession().createQuery(sql.toString()).setLong("id", laboratorio.getId());
		listaDatas = (List<Date>) query.list();
		
		return listaDatas;
	}
	
	
	@SuppressWarnings("unchecked")
	public void attachDetalhes(ComunicacaoAIE comunicacaoAIE){
		if (comunicacaoAIE == null)
			return;
		
		List<DetalheComunicacaoAIE> listaDatas;
		
		StringBuilder sql = new StringBuilder();
		sql.append("select detalheComunicacaoAIE")
		   .append("  from DetalheComunicacaoAIE detalheComunicacaoAIE ")
		   .append(" where detalheComunicacaoAIE.comunicacaoAIE = :comunicacaoAIE ")
		   .append(" order by comunicacaoAIE.dataComunicacao ");
		
		Query query = getSession().createQuery(sql.toString()).setParameter("comunicacaoAIE", comunicacaoAIE);
		listaDatas = (List<DetalheComunicacaoAIE>) query.list();
		
		comunicacaoAIE.setListaDetalheComunicacaoAIE(listaDatas);
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public Long countAll(AbstractDTO dto) {
		StringBuilder sql = new StringBuilder();
		sql.append("select count(comunicacaoAIE) ")
		   .append("  from " + this.getType().getSimpleName() + " as comunicacaoAIE")
		   .append("  join comunicacaoAIE.laboratorio ")
		   .append("  where 1 = 1 ");
		
		if (dto != null && !dto.isNull()){
			ComunicacaoAIEDTO comunicacaoAIEDTO = (ComunicacaoAIEDTO) dto;
			
			if (comunicacaoAIEDTO.getDataComunicacao() != null)
				sql.append("  and to_char(comunicacaoAIE.dataComunicacao, 'YYYY-MM-DD HH:MI:SS') between :inicio and :final");
			if (comunicacaoAIEDTO.getLaboratorio() != null)
				sql.append("  and comunicacaoAIE.laboratorio = :laboratorio");
		}
		
		Query query = getSession().createQuery(sql.toString());
		
		if (dto != null && !dto.isNull()){
			ComunicacaoAIEDTO comunicacaoAIEDTO = (ComunicacaoAIEDTO) dto;
			
			if (comunicacaoAIEDTO.getDataComunicacao() != null){
				Calendar i = Calendar.getInstance();
				i.setTime(comunicacaoAIEDTO.getDataComunicacao());
				
				StringBuilder sbI = new StringBuilder();
				sbI.append(i.get(Calendar.YEAR)).append("-")
				   .append(((i.get(Calendar.MONTH) + 1) + "").length() > 1 ? (i.get(Calendar.MONTH)+1) : "0" + (i.get(Calendar.MONTH)+1)).append("-")
				   .append("01")
				   .append(" ")
				   .append("00").append(":")
				   .append("00").append(":")
				   .append("00");
				
				Calendar f = Calendar.getInstance();
				f.setTime(comunicacaoAIEDTO.getDataComunicacao());
				
				StringBuilder sbF = new StringBuilder();
				sbF.append(f.get(Calendar.YEAR)).append("-")
				   .append(((f.get(Calendar.MONTH) + 1) + "").length() > 1 ? (f.get(Calendar.MONTH)+1) : "0" + (f.get(Calendar.MONTH)+1)).append("-")
				   .append(f.getActualMaximum(Calendar.DAY_OF_MONTH))
				   .append(" ")
				   .append("23").append(":")
				   .append("23").append(":")
				   .append("59");
				
				query.setString("inicio", sbI.toString());
				query.setString("final", sbF.toString());
			} 
			
			if (comunicacaoAIEDTO.getLaboratorio() != null)
				query.setParameter("laboratorio", comunicacaoAIEDTO.getLaboratorio());
		}

		return (Long) query.uniqueResult();
    }
	
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<ComunicacaoAIE> findAllComPaginacao(AbstractDTO dto, int first, int pageSize, String sortField, String sortOrder) {
		StringBuilder sql = new StringBuilder();
		sql.append("select comunicacaoAIE")
		   .append("  from " + this.getType().getSimpleName() + " as comunicacaoAIE")
		   .append("  join comunicacaoAIE.laboratorio ")
		   .append("  where 1 = 1 ");
		
		if (dto != null && !dto.isNull()){
			ComunicacaoAIEDTO comunicacaoAIEDTO = (ComunicacaoAIEDTO) dto;
			
			if (comunicacaoAIEDTO.getDataComunicacao() != null)
				sql.append("  and to_char(comunicacaoAIE.dataComunicacao, 'YYYY-MM-DD HH:MI:SS') between :inicio and :final");
			if (comunicacaoAIEDTO.getLaboratorio() != null)
				sql.append("  and comunicacaoAIE.laboratorio = :laboratorio");
		}
		
		if (StringUtils.isNotBlank(sortField))
			sql.append(" order by comunicacaoAIE." + sortField + " " + sortOrder);

		Query query = getSession().createQuery(sql.toString());
		
		if (dto != null && !dto.isNull()){
			ComunicacaoAIEDTO comunicacaoAIEDTO = (ComunicacaoAIEDTO) dto;
			
			if (comunicacaoAIEDTO.getDataComunicacao() != null){
				Calendar i = Calendar.getInstance();
				i.setTime(comunicacaoAIEDTO.getDataComunicacao());
				
				StringBuilder sbI = new StringBuilder();
				sbI.append(i.get(Calendar.YEAR)).append("-")
				   .append(((i.get(Calendar.MONTH) + 1) + "").length() > 1 ? (i.get(Calendar.MONTH)+1) : "0" + (i.get(Calendar.MONTH)+1)).append("-")
				   .append("01")
				   .append(" ")
				   .append("00").append(":")
				   .append("00").append(":")
				   .append("00");
				
				Calendar f = Calendar.getInstance();
				f.setTime(comunicacaoAIEDTO.getDataComunicacao());
				
				StringBuilder sbF = new StringBuilder();
				sbF.append(f.get(Calendar.YEAR)).append("-")
				   .append(((f.get(Calendar.MONTH) + 1) + "").length() > 1 ? (f.get(Calendar.MONTH)+1) : "0" + (f.get(Calendar.MONTH)+1)).append("-")
				   .append(f.getActualMaximum(Calendar.DAY_OF_MONTH))
				   .append(" ")
				   .append("23").append(":")
				   .append("23").append(":")
				   .append("59");
				
				query.setString("inicio", sbI.toString());
				query.setString("final", sbF.toString());
			} 
			
			if (comunicacaoAIEDTO.getLaboratorio() != null)
				query.setParameter("laboratorio", comunicacaoAIEDTO.getLaboratorio());
		}

		query.setFirstResult(first);
		query.setMaxResults(pageSize);

		List<ComunicacaoAIE> lista = query.list();
		
		return lista;
    }
	
	@Override
	public void saveOrUpdate(ComunicacaoAIE comunicacaoAIE) {
		super.saveOrUpdate(comunicacaoAIE);
		
		log.info("Salvando Comunicação AIE {}", comunicacaoAIE.getId());
	}
	
	public void saveOrUpdate(DetalheComunicacaoAIE detalheComunicacaoAIE) {
		ComunicacaoAIE comunicacaoAIE = detalheComunicacaoAIE.getComunicacaoAIE(); 
		comunicacaoAIE = (ComunicacaoAIE) this.getSession().merge(comunicacaoAIE);
		
		detalheComunicacaoAIE = (DetalheComunicacaoAIE) this.getSession().merge(detalheComunicacaoAIE);
		super.saveOrUpdate(comunicacaoAIE);
		
		log.info("Salvando Comunicação AIE {}", comunicacaoAIE.getId());
	}
	
	@Override
	public void delete(ComunicacaoAIE comunicacaoAIE) {
		super.delete(comunicacaoAIE);
		
		log.info("Removendo Comunicação AIE {}", comunicacaoAIE.getId());
	}
	
	@Override
	public void validar(ComunicacaoAIE ComunicacaoAIE) {

	}

	@Override
	public void validarPersist(ComunicacaoAIE ComunicacaoAIE) {

	}

	@Override
	public void validarMerge(ComunicacaoAIE ComunicacaoAIE) {

	}

	@Override
	public void validarDelete(ComunicacaoAIE ComunicacaoAIE) {

	}

}
