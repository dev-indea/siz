package br.gov.mt.indea.sistemaformin.entity;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

import br.gov.mt.indea.sistemaformin.enums.Dominio.AtivoInativo;

@Audited
@Entity
@Table(name="doenca_enf_abat_frig")
public class DoencaEnfermidadeAbatedouroFrigorifico extends BaseEntity<Long>{
	
	private static final long serialVersionUID = 8054364136793908455L;

	@Id
	@SequenceGenerator(name="doenca_enf_abat_frig_seq", sequenceName="doenca_enf_abat_frig_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="doenca_enf_abat_frig_seq")
	private Long id;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_cadastro")
	private Calendar dataCadastro;
	
	@Column(unique=true)
	private String nome;
	
	@Enumerated(EnumType.STRING)
	@Column(name="status")
    private AtivoInativo status;
	
	@Column(name="possui_formulario_colheita")
	private boolean possuiFormularioColheita;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Calendar getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Calendar dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public AtivoInativo getStatus() {
		return status;
	}

	public void setStatus(AtivoInativo status) {
		this.status = status;
	}

	public boolean isPossuiFormularioColheita() {
		return possuiFormularioColheita;
	}

	public void setPossuiFormularioColheita(boolean possuiFormularioColheita) {
		this.possuiFormularioColheita = possuiFormularioColheita;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DoencaEnfermidadeAbatedouroFrigorifico other = (DoencaEnfermidadeAbatedouroFrigorifico) obj;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		return true;
	}

}