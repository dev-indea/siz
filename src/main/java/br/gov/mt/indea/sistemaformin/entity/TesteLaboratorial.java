package br.gov.mt.indea.sistemaformin.entity;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.apache.commons.io.FileUtils;
import org.hibernate.envers.Audited;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

@Audited
@Entity
@Table(name="teste_laboratorial")
public class TesteLaboratorial extends BaseEntity<Long>{

	private static final long serialVersionUID = 1331282233749031338L;

	@Id
	@SequenceGenerator(name="teste_laboratorial_seq", sequenceName="teste_laboratorial_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="teste_laboratorial_seq")
	private Long id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_cadastro")
	private Calendar dataCadastro;
	
	@ManyToOne
	@JoinColumn(name="id_suspeita_doenca_animal")
	private SuspeitaDoencaAnimal suspeitaDoencaAnimal;

	@Column(name="teste_realizado", length=300)
	private String testeRealizado;
	
	@Column(name="material_testado", length=300)
	private String materialTestado;
	
	@Column(name="resultado", length=300)
	private String resultado;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_resultado")
	private Calendar dataResultado;
	
	@Column(name="laboratorio", length=300)
	private String laboratorio;
	
	@Lob
	private byte[] laudo;
	
	@Column(name="laudo_name", length=300)
	private String laudoName;
	
	@Transient
	private File laudoFile;
	
	public StreamedContent getLaudoAsFile() throws IOException{
		if (this.laudo == null)
			return null;

		laudoFile = File.createTempFile("siz-", ".tmp");
		
		FileUtils.writeByteArrayToFile(laudoFile, this.laudo);
		
		StreamedContent streamedContent = new DefaultStreamedContent(new FileInputStream(laudoFile), "application/octet-stream", this.laudoName);
		
		return streamedContent;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Calendar getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Calendar dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public SuspeitaDoencaAnimal getSuspeitaDoencaAnimal() {
		return suspeitaDoencaAnimal;
	}

	public void setSuspeitaDoencaAnimal(SuspeitaDoencaAnimal suspeitaDoencaAnimal) {
		this.suspeitaDoencaAnimal = suspeitaDoencaAnimal;
	}

	public String getTesteRealizado() {
		return testeRealizado;
	}

	public void setTesteRealizado(String testeRealizado) {
		this.testeRealizado = testeRealizado;
	}

	public String getMaterialTestado() {
		return materialTestado;
	}

	public void setMaterialTestado(String materialTestado) {
		this.materialTestado = materialTestado;
	}

	public String getResultado() {
		return resultado;
	}

	public void setResultado(String resultado) {
		this.resultado = resultado;
	}

	public Calendar getDataResultado() {
		return dataResultado;
	}

	public void setDataResultado(Calendar dataResultado) {
		this.dataResultado = dataResultado;
	}

	public String getLaboratorio() {
		return laboratorio;
	}

	public void setLaboratorio(String laboratorio) {
		this.laboratorio = laboratorio;
	}

	public byte[] getLaudo() {
		return laudo;
	}

	public void setLaudo(byte[] laudo) {
		this.laudo = laudo;
	}

	public String getLaudoName() {
		return laudoName;
	}

	public void setLaudoName(String laudoName) {
		this.laudoName = laudoName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TesteLaboratorial other = (TesteLaboratorial) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	@Override
	protected void finalize() throws Throwable {
		this.laudoFile.delete();
		super.finalize();
	}	
}
