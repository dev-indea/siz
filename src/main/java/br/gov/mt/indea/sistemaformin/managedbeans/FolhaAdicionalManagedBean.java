package br.gov.mt.indea.sistemaformin.managedbeans;

import java.io.IOException;
import java.io.Serializable;
import java.util.Calendar;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLBeanName;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;

import br.gov.mt.indea.sistemaformin.entity.FolhaAdicional;
import br.gov.mt.indea.sistemaformin.entity.FormCOM;
import br.gov.mt.indea.sistemaformin.entity.FormIN;
import br.gov.mt.indea.sistemaformin.exception.ApplicationErrorException;
import br.gov.mt.indea.sistemaformin.service.FolhaAdicionalService;
import br.gov.mt.indea.sistemaformin.service.FormCOMService;
import br.gov.mt.indea.sistemaformin.service.FormINService;
import br.gov.mt.indea.sistemaformin.util.FacesMessageUtil;

@Named
@ViewScoped
@URLBeanName("folhaAdicionalManagedBean")
@URLMappings(mappings = {
		@URLMapping(id = "pesquisarFolhaAdicional", pattern = "/formIN/#{folhaAdicionalManagedBean.idFormIN}/folhaAdicional/pesquisar", viewId = "/pages/folhaAdicional/lista.jsf"),
		@URLMapping(id = "incluirFolhaAdicional", pattern = "/formIN/#{folhaAdicionalManagedBean.idFormIN}/folhaAdicional/incluir", viewId = "/pages/folhaAdicional/novo.jsf"),
		@URLMapping(id = "alterarFolhaAdicional", pattern = "/formIN/#{folhaAdicionalManagedBean.idFormIN}/folhaAdicional/alterar/#{folhaAdicionalManagedBean.id}", viewId = "/pages/folhaAdicional/novo.jsf"),
		@URLMapping(id = "impressaoFolhaAdicional", pattern = "/formIN/#{folhaAdicionalManagedBean.idFormIN}/folhaAdicional/impressao/#{folhaAdicionalManagedBean.id}", viewId = "/pages/impressao.jsf")})
public class FolhaAdicionalManagedBean implements Serializable {

	private static final long serialVersionUID = 5401332308697941357L;
	
	private Long id;
	
	private Long idFormIN;
	
	private FormIN formIN;
	
	@Inject
	private FormINService formINService;
	
	@Inject
	private FolhaAdicionalService folhaAdicionalService;
	
	@Inject
	private FormCOMService formCOMService;
	
	@Inject
	private FolhaAdicional folhaAdicional;
	
	@PostConstruct
	private void init(){

	}
	
	private void carregarFormIN(){
		this.formIN = formINService.findByIdFetchPropriedade(this.idFormIN);
	}
	
	private void limpar() {
		this.folhaAdicional = new FolhaAdicional();
	}

	@URLAction(mappingId = "incluirFolhaAdicional", onPostback = false)
	public void novo(){
		this.limpar();
		this.carregarFormIN();
		this.folhaAdicional.setFormIN(formIN);
	}
	
//	public String novo(FormCOM formCOM){
//		this.folhaAdicional = new FolhaAdicional();
//		this.folhaAdicional.setFormCOM(formCOM);
//		this.folhaAdicional.setFormIN(formCOM.getFormIN());
//		
//		return "/pages/folhaAdicional/novo.xhtml";
//	}
	
	@URLAction(mappingId = "alterarFolhaAdicional", onPostback = false)
	public void editar(){
		this.folhaAdicional = folhaAdicionalService.findByIdFetchAll(this.getId());
	}
	
	public String adicionar() throws IOException{
				
		if (folhaAdicional.getId() != null){
			this.folhaAdicionalService.saveOrUpdate(folhaAdicional);
			FacesMessageUtil.addInfoContextFacesMessage("Folha Adicional atualizada", "");
		}else{
			this.folhaAdicional.setDataCadastro(Calendar.getInstance());
			this.folhaAdicionalService.saveOrUpdate(folhaAdicional);
			FacesMessageUtil.addInfoContextFacesMessage("Folha Adicional adicionada", "");
		}
		
		limpar();
	    return "pretty:visaoGeralFormIN";
	}
	
	@Inject
	private VisualizadorImpressaoManagedBean visualizadorImpressaoManagedBean;
	
	@URLAction(mappingId = "impressaoFolhaAdicional", onPostback = false)
	public void imprimir() throws ApplicationErrorException{
		this.folhaAdicional = folhaAdicionalService.findByIdFetchAll(this.getId());
		
		visualizadorImpressaoManagedBean.exibirFolhaAdicional(folhaAdicional);
	}
	
	public void remover(FolhaAdicional folhaAdicional){
		this.folhaAdicionalService.delete(folhaAdicional);
		FacesMessageUtil.addInfoContextFacesMessage("Folha Adicional exclu�da com sucesso", "");
	}
	
	public List<FormCOM> getListaFormCOM(){
		return formCOMService.findAllBy(this.folhaAdicional.getFormIN());
	}

	public FolhaAdicional getFolhaAdicional() {
		return folhaAdicional;
	}

	public void setFolhaAdicional(FolhaAdicional folhaAdicional) {
		this.folhaAdicional = folhaAdicional;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public FormIN getFormIN() {
		return formIN;
	}

	public void setFormIN(FormIN formIN) {
		this.formIN = formIN;
	}

	public Long getIdFormIN() {
		return idFormIN;
	}

	public void setIdFormIN(Long idFormIN) {
		this.idFormIN = idFormIN;
	}

}
