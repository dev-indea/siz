package br.gov.mt.indea.sistemaformin.dao;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.inject.Inject;


import br.gov.mt.indea.sistemaformin.entity.FormAIE;
import br.gov.mt.indea.sistemaformin.exception.ApplicationException;

@Stateless
public class FormAIEDAO extends DAO<FormAIE> {
	
	@Inject
	private EntityManager em;
	
	public FormAIEDAO() {
		super(FormAIE.class);
	}

	@Override
	public EntityManager getEntityManager() {
		return this.em;
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void add(FormAIE formAIE) throws ApplicationException{
		formAIE = this.getEntityManager().merge(formAIE);
		if (formAIE.getFormCOM() != null)
			formAIE.setFormCOM(this.getEntityManager().merge(formAIE.getFormCOM()));
		super.add(formAIE);
	}

}
