package br.gov.mt.indea.sistemaformin.entity.dto;

import java.io.Serializable;
import java.util.Date;

import br.gov.mt.indea.sistemaformin.entity.Municipio;
import br.gov.mt.indea.sistemaformin.entity.UF;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoEstabelecimento;

public class VisitaPropriedadeRuralDTO implements AbstractDTO, Serializable{
	
	private static final long serialVersionUID = 6205763755360605521L;

	private UF uf = UF.MATO_GROSSO;
	
	private Municipio municipio;
	
	private TipoEstabelecimento tipoEstabelecimento;
	
    private String numero;
	
	private String estabelecimento;
	
	private Date data;

	public boolean isNull(){
		if (uf == null && municipio == null && tipoEstabelecimento == null && data == null && numero == null && estabelecimento == null)
			return true;
		return false;
	}
	public UF getUf() {
		return uf;
	}

	public void setUf(UF uf) {
		this.uf = uf;
	}

	public Municipio getMunicipio() {
		return municipio;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}

	public TipoEstabelecimento getTipoEstabelecimento() {
		return tipoEstabelecimento;
	}

	public void setTipoEstabelecimento(TipoEstabelecimento tipoEstabelecimento) {
		this.tipoEstabelecimento = tipoEstabelecimento;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getEstabelecimento() {
		return estabelecimento;
	}

	public void setEstabelecimento(String estabelecimento) {
		this.estabelecimento = estabelecimento;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}
	
}