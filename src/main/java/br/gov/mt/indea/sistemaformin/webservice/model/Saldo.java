package br.gov.mt.indea.sistemaformin.webservice.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Saldo implements Serializable {

	private static final long serialVersionUID = -6872901189816044554L;

	private Long id;
    
    private Integer exploracao;
    
    @XmlElement(name = "estratificacao")
    private Estratificacao estratificacao;
   
    private Integer qntSaldo;
    
    private Integer qntTemporario;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getExploracao() {
        return exploracao;
    }

    public void setExploracao(Integer exploracao) {
        this.exploracao = exploracao;
    }

    public Estratificacao getEstratificacao() {
        return estratificacao;
    }

    public void setEstratificacao(Estratificacao estratificacao) {
        this.estratificacao = estratificacao;
    }

    public Integer getQntSaldo() {
        return qntSaldo;
    }

    public void setQntSaldo(Integer qntSaldo) {
        this.qntSaldo = qntSaldo;
    }

    public Integer getQntTemporario() {
        return qntTemporario;
    }

    public void setQntTemporario(Integer qntTemporario) {
        this.qntTemporario = qntTemporario;
    }
}