package br.gov.mt.indea.sistemaformin.managedbeans.seguranca;

import java.io.IOException;
import java.io.Serializable;
import java.util.Calendar;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLBeanName;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;

import br.gov.mt.indea.sistemaformin.entity.seguranca.Grupo;
import br.gov.mt.indea.sistemaformin.entity.seguranca.Permissao;
import br.gov.mt.indea.sistemaformin.service.LazyObjectDataModel;
import br.gov.mt.indea.sistemaformin.service.seguranca.GrupoService;
import br.gov.mt.indea.sistemaformin.service.seguranca.PermissaoService;
import br.gov.mt.indea.sistemaformin.util.FacesMessageUtil;


@Named
@ViewScoped
@URLBeanName("grupoManagedBean")
@URLMappings(mappings = {
		@URLMapping(id = "pesquisarGrupo", pattern = "/grupo/pesquisar", viewId = "/pages/security/grupo/lista.jsf"),
		@URLMapping(id = "incluirGrupo", pattern = "/grupo/incluir", viewId = "/pages/security/grupo/novo.jsf"),
		@URLMapping(id = "alterarGrupo", pattern = "/grupo/alterar/#{grupoManagedBean.id}", viewId = "/pages/security/grupo/novo.jsf"),
		@URLMapping(id = "permissoesGrupo", pattern = "/grupo/#{grupoManagedBean.id}/permissoes", viewId = "/pages/security/grupo/permissoes.jsf")})
public class GrupoManagedBean implements Serializable{

	private static final long serialVersionUID = -107693789607909430L;

	private Long id;
	
	private Grupo grupo;
	
	private LazyObjectDataModel<Grupo> listaGrupo;
	
	private List<Permissao> listaPermissao;
	
	@Inject
	private GrupoService grupoService;
	
	@Inject
	private PermissaoService permissaoService; 
	
	@PostConstruct
	private void init(){
		this.listaGrupo = new LazyObjectDataModel<Grupo>(this.grupoService);
		this.listaPermissao = permissaoService.findAll("nome");
	}
	
	private void limpar() {
		this.grupo = new Grupo();
	}
	
	@URLAction(mappingId = "incluirGrupo", onPostback = false)
	public void novo(){
		this.limpar();
	}
	
	@URLAction(mappingId = "alterarGrupo", onPostback = false)
	public void editar(){
		this.grupo = grupoService.findByIdFetchAll(this.getId());
	}
	
	@URLAction(mappingId = "permissoesGrupo", onPostback = false)
	public void permissoes(){
		this.grupo = grupoService.findByIdFetchAll(this.getId());
	}
	
	public String adicionar() throws IOException{
		
		if (grupo.getId() != null){
			this.grupoService.saveOrUpdate(grupo);
			FacesMessageUtil.addInfoContextFacesMessage("Grupo atualizado", "");
		}else{
			this.grupo.setDataCadastro(Calendar.getInstance());
			this.grupoService.saveOrUpdate(grupo);
			FacesMessageUtil.addInfoContextFacesMessage("Grupo adicionado", "");
		}
		
		limpar();
	    return "pretty:pesquisarGrupo";
	}
	
	public void remover(Grupo grupo){
		this.grupoService.delete(grupo);
		FacesMessageUtil.addInfoContextFacesMessage("Grupo exclu�do com sucesso", "");
	}
	
	public void adicionar(Permissao permissao){
		this.grupo.getListaPermissao().add(permissao);
		
		this.grupoService.saveOrUpdate(grupo);
		FacesMessageUtil.addInfoContextFacesMessage("Permiss�o adicionada", "");
	}
	
	public void remover(Permissao permissao){
		this.grupo.getListaPermissao().remove(permissao);
		
		this.grupoService.saveOrUpdate(grupo);
		FacesMessageUtil.addInfoContextFacesMessage("Permiss�o removida", "");
	}
	
	public boolean grupoHavePermissao(Permissao permissao){
		return this.grupo.getListaPermissao().contains(permissao);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Grupo getGrupo() {
		return grupo;
	}

	public void setGrupo(Grupo grupo) {
		this.grupo = grupo;
	}

	public LazyObjectDataModel<Grupo> getListaGrupo() {
		return listaGrupo;
	}

	public void setListaGrupo(
			LazyObjectDataModel<Grupo> listaGrupo) {
		this.listaGrupo = listaGrupo;
	}

	public List<Permissao> getListaPermissao() {
		return listaPermissao;
	}

	public void setListaPermissao(List<Permissao> listaPermissao) {
		this.listaPermissao = listaPermissao;
	}
	
	
}
