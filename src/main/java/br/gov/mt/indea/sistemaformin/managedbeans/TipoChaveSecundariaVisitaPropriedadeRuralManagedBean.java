package br.gov.mt.indea.sistemaformin.managedbeans;

import java.io.IOException;
import java.io.Serializable;
import java.util.Calendar;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLBeanName;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;

import br.gov.mt.indea.sistemaformin.entity.TipoChaveSecundariaVisitaPropriedadeRural;
import br.gov.mt.indea.sistemaformin.enums.Dominio.AtivoInativo;
import br.gov.mt.indea.sistemaformin.service.LazyObjectDataModel;
import br.gov.mt.indea.sistemaformin.service.TipoChaveSecundariaVisitaPropriedadeRuralService;
import br.gov.mt.indea.sistemaformin.util.FacesMessageUtil;

@Named
@ViewScoped
@URLBeanName("tipoChaveSecundariaVisitaPropriedadeRuralManagedBean")
@URLMappings(mappings = {
		@URLMapping(id = "pesquisarTipoChaveSecundariaVisitaPropriedadeRural", pattern = "/tipoChaveSecundariaVisitaPropriedadeRural/pesquisar", viewId = "/pages/tipoChaveSecundariaVisitaPropriedadeRural/lista.jsf"),
		@URLMapping(id = "incluirTipoChaveSecundariaVisitaPropriedadeRural", pattern = "/tipoChaveSecundariaVisitaPropriedadeRural/incluir", viewId = "/pages/tipoChaveSecundariaVisitaPropriedadeRural/novo.jsf"),
		@URLMapping(id = "alterarTipoChaveSecundariaVisitaPropriedadeRural", pattern = "/tipoChaveSecundariaVisitaPropriedadeRural/alterar/#{tipoChaveSecundariaVisitaPropriedadeRuralManagedBean.id}", viewId = "/pages/tipoChaveSecundariaVisitaPropriedadeRural/novo.jsf")})
public class TipoChaveSecundariaVisitaPropriedadeRuralManagedBean implements Serializable {
	
	private static final long serialVersionUID = -7409333626528791621L;

	private Long id;
	
	@Inject
	private TipoChaveSecundariaVisitaPropriedadeRuralService tipoChaveSecundariaVisitaPropriedadeRuralService;
	
	@Inject
	private TipoChaveSecundariaVisitaPropriedadeRural tipoChaveSecundariaVisitaPropriedadeRural;
	
	private LazyObjectDataModel<TipoChaveSecundariaVisitaPropriedadeRural> listaTipoChaveSecundariaVisitaPropriedadeRural;
	
	@PostConstruct
	public void init(){
		this.listaTipoChaveSecundariaVisitaPropriedadeRural = new LazyObjectDataModel<TipoChaveSecundariaVisitaPropriedadeRural>(this.tipoChaveSecundariaVisitaPropriedadeRuralService);
	}
	
	public void limpar(){
		this.tipoChaveSecundariaVisitaPropriedadeRural = new TipoChaveSecundariaVisitaPropriedadeRural();
	}
	
	@URLAction(mappingId = "incluirTipoChaveSecundariaVisitaPropriedadeRural", onPostback = false)
	public void novo(){
		limpar();
	}
	
	@URLAction(mappingId = "alterarTipoChaveSecundariaVisitaPropriedadeRural", onPostback = false)
	public void editar(){
		this.tipoChaveSecundariaVisitaPropriedadeRural = tipoChaveSecundariaVisitaPropriedadeRuralService.findByIdFetchAll(this.getId());
	}
	
	public String adicionar() throws IOException{
		
		if (tipoChaveSecundariaVisitaPropriedadeRural.getId() != null){
			this.tipoChaveSecundariaVisitaPropriedadeRuralService.saveOrUpdate(tipoChaveSecundariaVisitaPropriedadeRural);
			FacesMessageUtil.addInfoContextFacesMessage("Tipo chave secundaria atualizado", "");
		}else{
			this.tipoChaveSecundariaVisitaPropriedadeRural.setDataCadastro(Calendar.getInstance());
			this.tipoChaveSecundariaVisitaPropriedadeRural.setStatus(AtivoInativo.ATIVO);
			this.tipoChaveSecundariaVisitaPropriedadeRuralService.saveOrUpdate(tipoChaveSecundariaVisitaPropriedadeRural);
			FacesMessageUtil.addInfoContextFacesMessage("Tipo chave secundaria adicionado", "");
		}
		
		this.tipoChaveSecundariaVisitaPropriedadeRural = new TipoChaveSecundariaVisitaPropriedadeRural();
		
		return "pretty:pesquisarTipoChaveSecundariaVisitaPropriedadeRural";
	}
	
	public void remover(TipoChaveSecundariaVisitaPropriedadeRural tipoChaveSecundariaVisitaPropriedadeRural){
		this.tipoChaveSecundariaVisitaPropriedadeRuralService.delete(tipoChaveSecundariaVisitaPropriedadeRural);
		FacesMessageUtil.addInfoContextFacesMessage("Tipo chave secundaria exclu�do com sucesso", "");
	}
	
	public void ativar(TipoChaveSecundariaVisitaPropriedadeRural tipoChaveSecundariaVisitaPropriedadeRural){
		tipoChaveSecundariaVisitaPropriedadeRural.setStatus(AtivoInativo.ATIVO);
		
		this.tipoChaveSecundariaVisitaPropriedadeRuralService.saveOrUpdate(tipoChaveSecundariaVisitaPropriedadeRural);
		FacesMessageUtil.addInfoContextFacesMessage("Tipo chave secundaria ativado", "");
	}
	
	public void desativar(TipoChaveSecundariaVisitaPropriedadeRural tipoChaveSecundariaVisitaPropriedadeRural){
		tipoChaveSecundariaVisitaPropriedadeRural.setStatus(AtivoInativo.INATIVO);
		
		this.tipoChaveSecundariaVisitaPropriedadeRuralService.saveOrUpdate(tipoChaveSecundariaVisitaPropriedadeRural);
		FacesMessageUtil.addInfoContextFacesMessage("Tipo chave secundaria desativado", "");
	}
	
	public boolean isAtivado(TipoChaveSecundariaVisitaPropriedadeRural tipoChaveSecundariaVisitaPropriedadeRural){
		return tipoChaveSecundariaVisitaPropriedadeRural.getStatus().equals(AtivoInativo.ATIVO);
	}
	
	public TipoChaveSecundariaVisitaPropriedadeRural getTipoChaveSecundariaVisitaPropriedadeRural() {
		return tipoChaveSecundariaVisitaPropriedadeRural;
	}

	public void setTipoChaveSecundariaVisitaPropriedadeRural(TipoChaveSecundariaVisitaPropriedadeRural tipoChaveSecundariaVisitaPropriedadeRural) {
		this.tipoChaveSecundariaVisitaPropriedadeRural = tipoChaveSecundariaVisitaPropriedadeRural;
	}

	public LazyObjectDataModel<TipoChaveSecundariaVisitaPropriedadeRural> getListaTipoChaveSecundariaVisitaPropriedadeRural() {
		return listaTipoChaveSecundariaVisitaPropriedadeRural;
	}

	public void setListaTipoChaveSecundariaVisitaPropriedadeRural(
			LazyObjectDataModel<TipoChaveSecundariaVisitaPropriedadeRural> listaTipoChaveSecundariaVisitaPropriedadeRural) {
		this.listaTipoChaveSecundariaVisitaPropriedadeRural = listaTipoChaveSecundariaVisitaPropriedadeRural;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
