package br.gov.mt.indea.sistemaformin.service;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.mt.indea.sistemaformin.entity.FormMaleina;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class FormMaleinaService extends PaginableService<FormMaleina, Long> {

	private static final Logger log = LoggerFactory.getLogger(FormMaleinaService.class);
	
	protected FormMaleinaService() {
		super(FormMaleina.class);
	}
	
	public FormMaleina findByIdFetchAll(Long id){
		FormMaleina formMaleina;
		
		StringBuilder sql = new StringBuilder();
		sql.append("  from FormMaleina formMaleina ")
		   .append("  join fetch formMaleina.formIN formin")
		   .append("  left join fetch formMaleina.formCOM")
		   .append("  join fetch formin.propriedade propriedade")
		   .append("  join fetch formin.proprietario proprietario")
		   .append("  left join fetch proprietario.endereco")
		   .append("  join fetch propriedade.municipio")
		   .append("  join fetch propriedade.ule")
		   .append("  join fetch formMaleina.veterinario vet")
		   .append("  join fetch vet.conselho")
		   .append("  join fetch vet.tipoEmitente")
		   .append("  left join fetch vet.ule uu")
		   .append("  left join fetch uu.urs")
		   .append("  left join fetch vet.endereco")
		   .append(" where formMaleina.id = :id ");
		
		Query query = getSession().createQuery(sql.toString()).setLong("id", id);
		formMaleina = (FormMaleina) query.uniqueResult();
		
		if (formMaleina != null){
		
			Hibernate.initialize(formMaleina.getTestesDeFixacaoDeComplementos());
			Hibernate.initialize(formMaleina.getOlhosDaAplicacao());
			Hibernate.initialize(formMaleina.getSinaisClinicosExameMaleina());
			
		}
		
		return formMaleina;
	}
	
	@Override
	public void saveOrUpdate(FormMaleina formMaleina) {
		super.saveOrUpdate(formMaleina);
		
		log.info("Salvando Form Maleina {}", formMaleina.getId());
	}
	
	@Override
	public void delete(FormMaleina formMaleina) {
		super.delete(formMaleina);
		
		log.info("Removendo Form Maleina {}", formMaleina.getId());
	}
	
	@Override
	public void validar(FormMaleina FormMaleina) {

	}

	@Override
	public void validarPersist(FormMaleina FormMaleina) {

	}

	@Override
	public void validarMerge(FormMaleina FormMaleina) {

	}

	@Override
	public void validarDelete(FormMaleina FormMaleina) {

	}

}
