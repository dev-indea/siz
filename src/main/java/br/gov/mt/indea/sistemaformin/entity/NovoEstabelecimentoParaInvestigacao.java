package br.gov.mt.indea.sistemaformin.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import org.hibernate.envers.Audited;

import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoVinculoEpidemiologico;
import br.gov.mt.indea.sistemaformin.webservice.entity.Propriedade;

@Audited
@Entity(name="novos_estabelecimentos")
public class NovoEstabelecimentoParaInvestigacao extends BaseEntity<Long>{

	private static final long serialVersionUID = 640858428188874629L;

	@Id
	@SequenceGenerator(name="novos_estabelecimentos_seq", sequenceName="novos_estabelecimentos_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="novos_estabelecimentos_seq")
	private Long id;

	@ManyToOne
	@JoinColumn(name="id_formVIN")
	private FormVIN formVIN;
	
	@ManyToOne
	@JoinColumn(name="id_formSV")
	private FormSV formSV;
	
	@ManyToOne
	@JoinColumn(name="id_formSH")
	private FormSH formSH;
	
	@ManyToOne
	@JoinColumn(name="id_formEQ")
	private FormEQ formEQ;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="id_propriedade")
	private Propriedade propriedade;
	
	@ElementCollection(targetClass = TipoVinculoEpidemiologico.class)
	@JoinTable(name = "novos_estab_vinculo_epidem", joinColumns = @JoinColumn(name = "id_novo_estabelecimento", nullable = false))
	@Column(name = "id_novos_estab_vinculo_epidem")
	@Enumerated(EnumType.STRING)
	private List<TipoVinculoEpidemiologico> listaVinculoEpidemiologico = new ArrayList<TipoVinculoEpidemiologico>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public FormVIN getFormVIN() {
		return formVIN;
	}

	public void setFormVIN(FormVIN formVIN) {
		this.formVIN = formVIN;
	}

	public FormSV getFormSV() {
		return formSV;
	}

	public void setFormSV(FormSV formSV) {
		this.formSV = formSV;
	}

	public FormSH getFormSH() {
		return formSH;
	}

	public void setFormSH(FormSH formSH) {
		this.formSH = formSH;
	}

	public Propriedade getPropriedade() {
		return propriedade;
	}

	public void setPropriedade(Propriedade propriedade) {
		this.propriedade = propriedade;
	}	

	public List<TipoVinculoEpidemiologico> getListaVinculoEpidemiologico() {
		return listaVinculoEpidemiologico;
	}

	public void setListaVinculoEpidemiologico(
			List<TipoVinculoEpidemiologico> listaVinculoEpidemiologico) {
		this.listaVinculoEpidemiologico = listaVinculoEpidemiologico;
	}
	
	public FormEQ getFormEQ() {
		return formEQ;
	}

	public void setFormEQ(FormEQ formEQ) {
		this.formEQ = formEQ;
	}

	public String getListaVinculoEpidmiologicoAsString(){
		StringBuilder sb = null;
		
		if (this.listaVinculoEpidemiologico != null && !this.listaVinculoEpidemiologico.isEmpty()){
			for (TipoVinculoEpidemiologico tipoVinculoEpidemiologico : this.listaVinculoEpidemiologico) {
				if (sb == null){
					sb = new StringBuilder();
					sb.append(tipoVinculoEpidemiologico.getValorFormVIN());
				}else
					sb.append(",").append(tipoVinculoEpidemiologico.getValorFormVIN());
			}
		}
		
		if (sb == null)
			return "";
		else
			return sb.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NovoEstabelecimentoParaInvestigacao other = (NovoEstabelecimentoParaInvestigacao) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}