package br.gov.mt.indea.sistemaformin.entity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

import br.gov.mt.indea.sistemaformin.enums.Dominio.PontosDeRisco;
import br.gov.mt.indea.sistemaformin.enums.Dominio.SimNao;
import br.gov.mt.indea.sistemaformin.webservice.entity.Produtor;
import br.gov.mt.indea.sistemaformin.webservice.entity.Propriedade;

@Audited
@Entity
@Table(name="vigilancia_veterinaria")
public class VigilanciaVeterinaria extends BaseEntity<Long>{

	private static final long serialVersionUID = -7549908729756174081L;
	
	@Id
	@SequenceGenerator(name="vigilancia_veterinaria_seq", sequenceName="vigilancia_veterinaria_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="vigilancia_veterinaria_seq")
	private Long id;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_usuario")
	private Usuario usuario;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_cadastro")
	private Calendar dataCadastro;
	
	@Column(length=17)
	private String numero;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_vigilancia")
	private Calendar dataVigilancia;
	
	@OneToOne(cascade={CascadeType.ALL}, orphanRemoval=true, fetch=FetchType.LAZY)
	@JoinColumn(name="id_motivos_vigilancia")
	private MotivosVigilanciaVeterinaria motivosVigilanciaVeterinaria = new MotivosVigilanciaVeterinaria();
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_propriedade")
	private Propriedade propriedade;
	
	@OneToOne(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH}, fetch=FetchType.LAZY)
	@JoinColumn(name="id_proprietario")
    private Produtor proprietario;
	
	@Column(name="codigo_propriedade")
    private Long codigoPropriedade;
	
	@Column(name = "nome_propriedade")
    private String nomePropriedade;
	
	@ElementCollection(targetClass = PontosDeRisco.class, fetch=FetchType.LAZY)
	@JoinTable(name = "vig_vet_ponto_de_risco", joinColumns = @JoinColumn(name = "id_vigilancia_veterinaria", nullable = false))
	@Column(name = "id_vig_vet_ponto_de_risco")
	@Enumerated(EnumType.STRING)
	private List<PontosDeRisco> pontosDeRisco;
	
	@OneToOne(cascade={CascadeType.ALL}, orphanRemoval=true, fetch=FetchType.LAZY)
	@JoinColumn(name="id_veterinario")
	private br.gov.mt.indea.sistemaformin.webservice.entity.Veterinario veterinario;
	
	@OneToOne(cascade={CascadeType.ALL}, orphanRemoval=true, fetch=FetchType.LAZY)
	@JoinColumn(name="id_vigilancia_alim_rumin")
	private VigilanciaAlimentosRuminantes vigilanciaAlimentosRuminantes;
	
	@OneToOne(cascade={CascadeType.ALL}, orphanRemoval=true, fetch=FetchType.LAZY)
	@JoinColumn(name="id_vigilancia_bov_bub")
	private VigilanciaBovinosEBubalinos vigilanciaBovinosEBubalinos;
	
	@OneToOne(cascade={CascadeType.ALL}, orphanRemoval=true, fetch=FetchType.LAZY)
	@JoinColumn(name="id_vigilancia_suideos")
	private VigilanciaSuideos vigilanciaSuideos;
	
	@OneToMany(mappedBy="vigilanciaVeterinaria", cascade={CascadeType.ALL}, orphanRemoval=true, fetch=FetchType.LAZY)
	private List<VigilanciaAves> listaVigilanciaAves = new ArrayList<VigilanciaAves>();
	
	@OneToOne(cascade={CascadeType.ALL}, orphanRemoval=true, fetch=FetchType.LAZY)
	@JoinColumn(name="id_vigilancia_outras")
	private VigilanciaOutrasEspecies vigilanciaOutrasEspecies;
	
	@OneToMany(mappedBy="vigilanciaVeterinaria", cascade={CascadeType.ALL}, orphanRemoval=true, fetch=FetchType.LAZY)
	private List<InvestigacaoEpidemiologica> listaInvestigacaoEpidemiologica = new ArrayList<InvestigacaoEpidemiologica>();
	
	@Enumerated(EnumType.STRING)
	@Column(name="historico_sugadura_morcegos")
	private SimNao historicoDeSugadurasDeMorcegos;
	
	@Column(name="qtdade_sugaduras_bovinos")
	private Integer quantidadeSugadurasEmBovinos;
	
	@Column(name="qtdade_sugaduras_bubalinos")
	private Integer quantidadeSugadurasEmBubalinos;
	
	@Column(name="qtdade_sugaduras_aves")
	private Integer quantidadeSugadurasEmAves;
	
	@Column(name="qtdade_sugaduras_caprinos")
	private Integer quantidadeSugadurasEmCaprinos;
	
	@Column(name="qtdade_sugaduras_suideos")
	private Integer quantidadeSugadurasEmSuideos;
	
	@Column(name="qtdade_sugaduras_equideos")
	private Integer quantidadeSugadurasEmEquideos;
	
	@Column(name="observacao", length=4000)
	private String observacao;
	
	@Enumerated(EnumType.STRING)
	@Column(name="emissao_form_in")
	private SimNao emissaoDeFormIN;
	
	@Column(name = "codg_verificador", length = 225)
	private String codigoVerificador;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Calendar getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Calendar dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Propriedade getPropriedade() {
		return propriedade;
	}

	public void setPropriedade(Propriedade propriedade) {
		this.propriedade = propriedade;
	}

	public br.gov.mt.indea.sistemaformin.webservice.entity.Veterinario getVeterinario() {
		return veterinario;
	}

	public void setVeterinario(
			br.gov.mt.indea.sistemaformin.webservice.entity.Veterinario veterinario) {
		this.veterinario = veterinario;
	}

	public MotivosVigilanciaVeterinaria getMotivosVigilanciaVeterinaria() {
		return motivosVigilanciaVeterinaria;
	}

	public void setMotivosVigilanciaVeterinaria(
			MotivosVigilanciaVeterinaria motivosVigilanciaVeterinaria) {
		this.motivosVigilanciaVeterinaria = motivosVigilanciaVeterinaria;
	}

	public Calendar getDataVigilancia() {
		return dataVigilancia;
	}

	public void setDataVigilancia(Calendar dataVigilancia) {
		this.dataVigilancia = dataVigilancia;
	}

	public List<PontosDeRisco> getPontosDeRisco() {
		return pontosDeRisco;
	}

	public void setPontosDeRisco(List<PontosDeRisco> pontosDeRisco) {
		this.pontosDeRisco = pontosDeRisco;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public VigilanciaAlimentosRuminantes getVigilanciaAlimentosRuminantes() {
		return vigilanciaAlimentosRuminantes;
	}

	public void setVigilanciaAlimentosRuminantes(
			VigilanciaAlimentosRuminantes vigilanciaAlimentosRuminantes) {
		this.vigilanciaAlimentosRuminantes = vigilanciaAlimentosRuminantes;
	}

	public VigilanciaBovinosEBubalinos getVigilanciaBovinosEBubalinos() {
		return vigilanciaBovinosEBubalinos;
	}

	public void setVigilanciaBovinosEBubalinos(
			VigilanciaBovinosEBubalinos vigilanciaBovinosEBubalinos) {
		this.vigilanciaBovinosEBubalinos = vigilanciaBovinosEBubalinos;
	}

	public VigilanciaSuideos getVigilanciaSuideos() {
		return vigilanciaSuideos;
	}

	public void setVigilanciaSuideos(VigilanciaSuideos vigilanciaSuideos) {
		this.vigilanciaSuideos = vigilanciaSuideos;
	}

	public List<VigilanciaAves> getListaVigilanciaAves() {
		return listaVigilanciaAves;
	}

	public void setListaVigilanciaAves(List<VigilanciaAves> listaVigilanciaAves) {
		this.listaVigilanciaAves = listaVigilanciaAves;
	}

	public VigilanciaOutrasEspecies getVigilanciaOutrasEspecies() {
		return vigilanciaOutrasEspecies;
	}

	public void setVigilanciaOutrasEspecies(
			VigilanciaOutrasEspecies vigilanciaOutrasEspecies) {
		this.vigilanciaOutrasEspecies = vigilanciaOutrasEspecies;
	}

	public List<InvestigacaoEpidemiologica> getListaInvestigacaoEpidemiologica() {
		return listaInvestigacaoEpidemiologica;
	}

	public void setListaInvestigacaoEpidemiologica(
			List<InvestigacaoEpidemiologica> listaInvestigacaoEpidemiologica) {
		this.listaInvestigacaoEpidemiologica = listaInvestigacaoEpidemiologica;
	}

	public SimNao getHistoricoDeSugadurasDeMorcegos() {
		return historicoDeSugadurasDeMorcegos;
	}

	public void setHistoricoDeSugadurasDeMorcegos(
			SimNao historicoDeSugadurasDeMorcegos) {
		this.historicoDeSugadurasDeMorcegos = historicoDeSugadurasDeMorcegos;
	}

	public Integer getQuantidadeSugadurasEmBovinos() {
		return quantidadeSugadurasEmBovinos;
	}

	public void setQuantidadeSugadurasEmBovinos(Integer quantidadeSugadurasEmBovinos) {
		this.quantidadeSugadurasEmBovinos = quantidadeSugadurasEmBovinos;
	}

	public Integer getQuantidadeSugadurasEmBubalinos() {
		return quantidadeSugadurasEmBubalinos;
	}

	public void setQuantidadeSugadurasEmBubalinos(
			Integer quantidadeSugadurasEmBubalinos) {
		this.quantidadeSugadurasEmBubalinos = quantidadeSugadurasEmBubalinos;
	}

	public Integer getQuantidadeSugadurasEmAves() {
		return quantidadeSugadurasEmAves;
	}

	public void setQuantidadeSugadurasEmAves(Integer quantidadeSugadurasEmAves) {
		this.quantidadeSugadurasEmAves = quantidadeSugadurasEmAves;
	}

	public Integer getQuantidadeSugadurasEmCaprinos() {
		return quantidadeSugadurasEmCaprinos;
	}

	public void setQuantidadeSugadurasEmCaprinos(
			Integer quantidadeSugadurasEmCaprinos) {
		this.quantidadeSugadurasEmCaprinos = quantidadeSugadurasEmCaprinos;
	}

	public Integer getQuantidadeSugadurasEmSuideos() {
		return quantidadeSugadurasEmSuideos;
	}

	public void setQuantidadeSugadurasEmSuideos(Integer quantidadeSugadurasEmSuideos) {
		this.quantidadeSugadurasEmSuideos = quantidadeSugadurasEmSuideos;
	}

	public Integer getQuantidadeSugadurasEmEquideos() {
		return quantidadeSugadurasEmEquideos;
	}

	public void setQuantidadeSugadurasEmEquideos(
			Integer quantidadeSugadurasEmEquideos) {
		this.quantidadeSugadurasEmEquideos = quantidadeSugadurasEmEquideos;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public SimNao getEmissaoDeFormIN() {
		return emissaoDeFormIN;
	}

	public void setEmissaoDeFormIN(SimNao emissaoDeFormIN) {
		this.emissaoDeFormIN = emissaoDeFormIN;
	}

	public Produtor getProprietario() {
		return proprietario;
	}

	public void setProprietario(Produtor proprietario) {
		this.proprietario = proprietario;
	}

	public Long getCodigoPropriedade() {
		return codigoPropriedade;
	}

	public void setCodigoPropriedade(Long codigoPropriedade) {
		this.codigoPropriedade = codigoPropriedade;
	}

	public String getNomePropriedade() {
		return nomePropriedade;
	}

	public void setNomePropriedade(String nomePropriedade) {
		this.nomePropriedade = nomePropriedade;
	}

	public String getCodigoVerificador() {
		return codigoVerificador;
	}

	public void setCodigoVerificador(String codigoVerificador) {
		this.codigoVerificador = codigoVerificador;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VigilanciaVeterinaria other = (VigilanciaVeterinaria) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}