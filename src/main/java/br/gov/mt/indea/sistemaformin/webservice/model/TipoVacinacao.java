package br.gov.mt.indea.sistemaformin.webservice.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class TipoVacinacao implements Serializable {

	private static final long serialVersionUID = -8784109146010875160L;

	private Long idTable;
	
	private String id;
    
	private String nome;

    public TipoVacinacao() {
        
    }

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNome() {
		return nome.replaceAll("  ", "");
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public TipoVacinacao(String id, String nome) {
		super();
		this.id = id;
		this.nome = nome;
	}

	public Long getIdTable() {
		return idTable;
	}

	public void setIdTable(Long idTable) {
		this.idTable = idTable;
	}

}