package br.gov.mt.indea.sistemaformin.dao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import br.gov.mt.indea.sistemaformin.entity.FormIN;
import br.gov.mt.indea.sistemaformin.entity.Municipio;
import br.gov.mt.indea.sistemaformin.enums.Dominio.StatusFormIN;
import br.gov.mt.indea.sistemaformin.exception.ApplicationException;
import br.gov.mt.indea.sistemaformin.util.StringUtil;

@Stateless
public class FormINDAO extends DAO<FormIN> {
	
	@Inject
	private EntityManager em;
	
	public FormINDAO() {
		super(FormIN.class);
	}

	@Override
	public EntityManager getEntityManager() {
		return this.em;
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void add(FormIN formIN) throws ApplicationException{
		formIN = this.getEntityManager().merge(formIN);
		super.add(formIN);
	}
	
	public void remove(FormIN formIN) throws ApplicationException{
		super.remove(formIN);
	}
	
	public FormIN findByNumero(String numero) throws ApplicationException{
		try{
			CriteriaBuilder cb = this.getEntityManager().getCriteriaBuilder();
			CriteriaQuery<FormIN> query = cb.createQuery(FormIN.class);
			
			Root<FormIN> root = query.from(FormIN.class);
			
			List<Predicate> predicados = new ArrayList<Predicate>();
			
			predicados.add(cb.equal(root.get("numero"), numero));
			predicados.add(cb.notEqual(root.get("statusFormIN"), StatusFormIN.COPIA_VALIDADA));
			
			query.where(cb.and(predicados.toArray(new Predicate[]{})));
			
			return this.getEntityManager().createQuery(query).getSingleResult();
		}catch(Exception e){
			throw new ApplicationException("N�o foi poss�vel buscar o registro", e);
		}
	}
	
	public List<FormIN> findAllWithNumeroFormINOrderByAsc(String nomeCampo) throws ApplicationException{
		try	{
			CriteriaBuilder cb = this.getEntityManager().getCriteriaBuilder();
			CriteriaQuery<FormIN> query = cb.createQuery(FormIN.class);
			
			Root<FormIN> t = query.from(FormIN.class);
			query.where(cb.notEqual(t.get("numero"), ""));
			query.orderBy(cb.asc(t.get(nomeCampo)));
			
			return this.getEntityManager().createQuery(query).getResultList();
		}catch(Exception e){
			throw new ApplicationException("N�o foi poss�vel listar os registros", e);
		}
	}
	
	public List<FormIN> findAllNotCOPIA_VALIDADAByAsc(String nomeCampo) throws ApplicationException{
		try	{
			CriteriaBuilder cb = this.getEntityManager().getCriteriaBuilder();
			CriteriaQuery<FormIN> query = cb.createQuery(FormIN.class);
			
			Root<FormIN> t = query.from(FormIN.class);
			query.where(cb.notEqual(t.get("statusFormIN"), StatusFormIN.COPIA_VALIDADA));
			query.orderBy(cb.asc(t.get(nomeCampo)));
			
			return this.getEntityManager().createQuery(query).getResultList();
		}catch(Exception e){
			throw new ApplicationException("N�o foi poss�vel listar os registros", e);
		}
	}
	
	public List<FormIN> findBy(Municipio municipio, String numeroFormIN, String propriedade, Date data) throws ApplicationException{
		try	{
			CriteriaBuilder builder = this.getEntityManager().getCriteriaBuilder();
			CriteriaQuery<FormIN> query = builder.createQuery(FormIN.class);
			
			Root<FormIN> root = query.from(FormIN.class);
			
			List<Predicate> predicados = new ArrayList<Predicate>();
			
			if (municipio != null)
				predicados.add(builder.equal(root.get("municipio"), municipio));
			
			if (numeroFormIN != null && !numeroFormIN.equals(""))
				predicados.add(builder.equal(root.get("numero"), numeroFormIN));
			
			if (data != null){
				Calendar i = Calendar.getInstance();
				
				i.setTime(data);
				i.set(Calendar.HOUR_OF_DAY, 0);
				i.set(Calendar.MINUTE, 0);
				i.set(Calendar.SECOND, 0);
				
				Calendar f = Calendar.getInstance();
				
				f.setTime(data);
				f.set(Calendar.HOUR_OF_DAY, 23);
				f.set(Calendar.MINUTE, 59);
				f.set(Calendar.SECOND, 59);
				
				Path<Date> dataDaNotificacaoPath = root.get("dataDaNotificacao");
				predicados.add(builder.between(dataDaNotificacaoPath, i.getTime(), f.getTime()));
			}
			
			if (propriedade != null && !propriedade.equals("")){
				predicados.add(builder.like(
					builder.function("remove_acento", String.class, builder.lower(root.get("propriedade").<String>get("nome"))), 
					"%" + StringUtil.removeAcentos(propriedade.toLowerCase() + "%")));
				
			}
			
			predicados.add(builder.notEqual(root.get("statusFormIN"), StatusFormIN.COPIA_VALIDADA));
			
			query.where(builder.and(predicados.toArray(new Predicate[]{})));
			query.orderBy(builder.desc(root.get("dataDaNotificacao")));
			
			return this.getEntityManager().createQuery(query).getResultList();
		}catch(Exception e){
			throw new ApplicationException("N�o foi poss�vel listar os registros", e);
		}
	}

}
