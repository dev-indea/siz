package br.gov.mt.indea.sistemaformin.managedbeans;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import com.ocpsoft.pretty.faces.annotation.URLBeanName;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;

import br.gov.mt.indea.sistemaformin.entity.DetalheFormNotifica;
import br.gov.mt.indea.sistemaformin.entity.FormIN;
import br.gov.mt.indea.sistemaformin.entity.LoteEnfermidadeAbatedouroFrigorifico;
import br.gov.mt.indea.sistemaformin.entity.Notificacao;
import br.gov.mt.indea.sistemaformin.entity.RegistroEntradaLASA;
import br.gov.mt.indea.sistemaformin.entity.VisitaPropriedadeRural;
import br.gov.mt.indea.sistemaformin.security.UserSecurity;
import br.gov.mt.indea.sistemaformin.service.EnfermidadeAbatedouroFrigorificoService;
import br.gov.mt.indea.sistemaformin.service.FormINService;
import br.gov.mt.indea.sistemaformin.service.FormNotificaService;
import br.gov.mt.indea.sistemaformin.service.NotificacaoService;
import br.gov.mt.indea.sistemaformin.service.RegistroEntradaLASAService;
import br.gov.mt.indea.sistemaformin.service.VisitaPropriedadeRuralService;
import br.gov.mt.indea.sistemaformin.util.FacesMessageUtil;
import br.gov.mt.indea.sistemaformin.webservice.entity.Propriedade;

@Named
@ViewScoped
@URLBeanName("dashBoardManagedBean")
@URLMappings(mappings = {
		@URLMapping(id = "dashboard", pattern = "/dashboard", viewId = "/dashboard.jsf")})
public class DashBoardManagedBean implements Serializable{

	private static final long serialVersionUID = 4902041813362871313L;

	@Inject
	private FormNotificaService formNotificaService;
	
	@Inject
	private RegistroEntradaLASAService registroEntradaLASAService;
	
	@Inject
	private EnfermidadeAbatedouroFrigorificoService enfermidadeAbatedouroFrigorificoService;
	
	@Inject
	private NotificacaoService notificacaoService;
	
	@Inject
	private UserSecurity userSecurity;
	
	private DetalheFormNotifica detalheFormNotifica;
	
	private RegistroEntradaLASA registroEntradaLASA;
	
	private LoteEnfermidadeAbatedouroFrigorifico loteEnfermidadeAbatedouroFrigorifico;
	
	private String numeroVisitaPropriedadeRural;
	
	private Propriedade propriedade;
	
	private List<VisitaPropriedadeRural> listaVisitaPropriedadeRural;
	
	@Inject
	private VisitaPropriedadeRuralService visitaPropriedadeRuralService;
	
	@Inject
	private FormINService formINService;

	private List<DetalheFormNotifica> listaDetalheFormNotifica;

	private List<RegistroEntradaLASA> listaRegistroEntradaLASA;

	private List<LoteEnfermidadeAbatedouroFrigorifico> listaLoteEnfermidadeAbatedouroFrigorifico;
	
	private List<Notificacao> listaNotificacao;
	
	private String numeroFormIN;
	
	private List<FormIN> listaFormIN;
	
	@PostConstruct
	private void init(){
		listaDetalheFormNotifica = formNotificaService.findAll(userSecurity.getUsuario().getMunicipio());
//		listaRegistroEntradaLASA = registroEntradaLASAService.findAll(userSecurity.getUsuario().getMunicipio());
		listaNotificacao = notificacaoService.findAll(userSecurity.getUsuario().getMunicipio());
		
		
//		if (!this.userSecurity.hasAuthorityPefa())
//			listaLoteEnfermidadeAbatedouroFrigorifico = enfermidadeAbatedouroFrigorificoService.findAllLoteEnfermidadeAbatedouroFrigorifico(userSecurity.getUsuario().getMunicipio());
//		else
//			listaLoteEnfermidadeAbatedouroFrigorifico = enfermidadeAbatedouroFrigorificoService.findAllLoteEnfermidadeAbatedouroFrigorifico(null);
	}
	
	public String novoFormIN(RegistroEntradaLASA registroEntradaLASA){
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		request.getSession().setAttribute("registroEntradaLASA", registroEntradaLASA);
		
		return "pretty:incluirFormIN";
	}
	
	public String novaVisitaPropriedadeRural(RegistroEntradaLASA registroEntradaLASA){
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		request.getSession().setAttribute("registroEntradaLASA", registroEntradaLASA);
		
		return "pretty:incluirVisitaPropriedadeRural";
	}
	
	public String novoFormIN(DetalheFormNotifica detalheFormNotifica){
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		request.getSession().setAttribute("detalheFormNotifica", detalheFormNotifica);
		
		return "pretty:incluirFormIN";
	}
	
	public String novaVisitaPropriedadeRural(DetalheFormNotifica detalheFormNotifica){
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		request.getSession().setAttribute("detalheFormNotifica", detalheFormNotifica);
		
		return "pretty:incluirVisitaPropriedadeRural";
	}
	
	public String novoFormIN(LoteEnfermidadeAbatedouroFrigorifico loteEnfermidadeAbatedouroFrigorifico){
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		request.getSession().setAttribute("loteEnfermidadeAbatedouroFrigorifico", loteEnfermidadeAbatedouroFrigorifico);
		
		return "pretty:incluirFormIN";
	}
	
	public String novaVisitaPropriedadeRural(LoteEnfermidadeAbatedouroFrigorifico loteEnfermidadeAbatedouroFrigorifico){
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		request.getSession().setAttribute("loteEnfermidadeAbatedouroFrigorifico", loteEnfermidadeAbatedouroFrigorifico);
		
		return "pretty:incluirVisitaPropriedadeRural";
	}
	
	public void abrirTelaDeBuscaDeVisita(Object object){
		this.numeroVisitaPropriedadeRural = null;
		this.listaVisitaPropriedadeRural = null;
		this.detalheFormNotifica = null;
		this.registroEntradaLASA = null;
		this.loteEnfermidadeAbatedouroFrigorifico = null;
		
		if (object instanceof DetalheFormNotifica){
			this.detalheFormNotifica = (DetalheFormNotifica) object;
			this.propriedade = detalheFormNotifica.getPropriedade();
		}
		if (object instanceof RegistroEntradaLASA){
			this.registroEntradaLASA = (RegistroEntradaLASA) object;
		}
		if (object instanceof LoteEnfermidadeAbatedouroFrigorifico){
			this.loteEnfermidadeAbatedouroFrigorifico = (LoteEnfermidadeAbatedouroFrigorifico) object;
			this.propriedade = this.loteEnfermidadeAbatedouroFrigorifico.getProprietario().getPropriedade();
		}
	}
	
	public void buscarVisitaPropriedadeRural(){
		listaVisitaPropriedadeRural = this.visitaPropriedadeRuralService.findByNumero(this.numeroVisitaPropriedadeRural, this.propriedade.getCodigo());
		if (listaVisitaPropriedadeRural == null || listaVisitaPropriedadeRural.isEmpty())
			FacesMessageUtil.addWarnContextFacesMessage("Nenhuma visita foi encontrada", "");
	}
	
	public void selecionarVisitaPropriedadeRural(VisitaPropriedadeRural visitaPropriedadeRural){
		if (this.detalheFormNotifica != null){
			detalheFormNotifica.setVisitaPropriedadeRural(visitaPropriedadeRural);
			formNotificaService.saveOrUpdate(detalheFormNotifica);
		}
		if (this.registroEntradaLASA != null){
			registroEntradaLASA.setVisitaPropriedadeRural(visitaPropriedadeRural);
			registroEntradaLASAService.saveOrUpdate(registroEntradaLASA);
		}
		if (this.loteEnfermidadeAbatedouroFrigorifico != null){
			loteEnfermidadeAbatedouroFrigorifico.setVisitaPropriedadeRural(visitaPropriedadeRural);
			enfermidadeAbatedouroFrigorificoService.saveOrUpdate(loteEnfermidadeAbatedouroFrigorifico);
		}
		
		FacesMessageUtil.addInfoContextFacesMessage("Visita propriedade rural n� " + visitaPropriedadeRural.getNumero() + " selecionada", "");
		this.listaVisitaPropriedadeRural = null;
	}
	
	public void abrirTelaDeBuscaDeFormIN(Object object){
		this.numeroFormIN = null;
		this.listaFormIN = null;
		this.detalheFormNotifica = null;
		this.registroEntradaLASA = null;
		this.loteEnfermidadeAbatedouroFrigorifico = null;
		
		if (object instanceof DetalheFormNotifica){
			this.detalheFormNotifica = (DetalheFormNotifica) object;
			this.propriedade = detalheFormNotifica.getPropriedade();
		}
		if (object instanceof RegistroEntradaLASA){
			this.registroEntradaLASA = (RegistroEntradaLASA) object;
		}
		if (object instanceof LoteEnfermidadeAbatedouroFrigorifico){
			this.loteEnfermidadeAbatedouroFrigorifico = (LoteEnfermidadeAbatedouroFrigorifico) object;
			this.propriedade = this.loteEnfermidadeAbatedouroFrigorifico.getProprietario().getPropriedade();
		}
	}
	
	public void buscarFormIN(){
		listaFormIN = this.formINService.findByNumero(this.numeroFormIN, this.propriedade.getCodigo());
		if (listaFormIN == null || listaFormIN.isEmpty())
			FacesMessageUtil.addWarnContextFacesMessage("Nenhum Form IN foi encontrado", "");
	}
	
	public void selecionarFormIN(FormIN formIN){
		if (this.detalheFormNotifica != null){
			detalheFormNotifica.setFormIN(formIN);
			formNotificaService.saveOrUpdate(detalheFormNotifica);
		}
		if (this.registroEntradaLASA != null){
			registroEntradaLASA.setFormIN(formIN);
			registroEntradaLASAService.saveOrUpdate(registroEntradaLASA);
		}
		
		FacesMessageUtil.addInfoContextFacesMessage("FormIN propriedade rural n� " + formIN.getNumero() + " selecionada", "");
		this.listaFormIN = null;
	}
	
	public RegistroEntradaLASA getRegistroEntradaLASA() {
		return registroEntradaLASA;
	}

	public void setRegistroEntradaLASA(RegistroEntradaLASA registroEntradaLASA) {
		this.registroEntradaLASA = registroEntradaLASA;
	}
	
	public DetalheFormNotifica getDetalheFormNotifica() {
		return detalheFormNotifica;
	}

	public void setDetalheFormNotifica(DetalheFormNotifica detalheFormNotifica) {
		this.detalheFormNotifica = detalheFormNotifica;
	}
	
	public Propriedade getPropriedade(){
		return propriedade;
	}
	
	public void setPropriedade(Propriedade propriedade){
		this.propriedade = propriedade;
	}

	public String getNumeroVisitaPropriedadeRural() {
		return numeroVisitaPropriedadeRural;
	}

	public void setNumeroVisitaPropriedadeRural(String numeroVisitaPropriedadeRural) {
		this.numeroVisitaPropriedadeRural = numeroVisitaPropriedadeRural;
	}

	public List<VisitaPropriedadeRural> getListaVisitaPropriedadeRural() {
		return listaVisitaPropriedadeRural;
	}

	public void setListaVisitaPropriedadeRural(List<VisitaPropriedadeRural> listaVisitaPropriedadeRural) {
		this.listaVisitaPropriedadeRural = listaVisitaPropriedadeRural;
	}

	public List<DetalheFormNotifica> getListaDetalheFormNotifica() {
		return listaDetalheFormNotifica;
	}

	public void setListaDetalheFormNotifica(List<DetalheFormNotifica> listaDetalheFormNotifica) {
		this.listaDetalheFormNotifica = listaDetalheFormNotifica;
	}

	public List<RegistroEntradaLASA> getListaRegistroEntradaLASA() {
		return listaRegistroEntradaLASA;
	}

	public void setListaRegistroEntradaLASA(List<RegistroEntradaLASA> listaRegistroEntradaLASA) {
		this.listaRegistroEntradaLASA = listaRegistroEntradaLASA;
	}

	public List<LoteEnfermidadeAbatedouroFrigorifico> getListaLoteEnfermidadeAbatedouroFrigorifico() {
		return listaLoteEnfermidadeAbatedouroFrigorifico;
	}

	public void setListaLoteEnfermidadeAbatedouroFrigorifico(
			List<LoteEnfermidadeAbatedouroFrigorifico> listaLoteEnfermidadeAbatedouroFrigorifico) {
		this.listaLoteEnfermidadeAbatedouroFrigorifico = listaLoteEnfermidadeAbatedouroFrigorifico;
	}

	public String getNumeroFormIN() {
		return numeroFormIN;
	}

	public void setNumeroFormIN(String numeroFormIN) {
		this.numeroFormIN = numeroFormIN;
	}

	public List<FormIN> getListaFormIN() {
		return listaFormIN;
	}

	public void setListaFormIN(List<FormIN> listaFormIN) {
		this.listaFormIN = listaFormIN;
	}

	public List<Notificacao> getListaNotificacao() {
		return listaNotificacao;
	}

	public void setListaNotificacao(List<Notificacao> listaNotificacao) {
		this.listaNotificacao = listaNotificacao;
	}

	
}