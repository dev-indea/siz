package br.gov.mt.indea.sistemaformin.managedbeans;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.constraints.Past;

import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLBeanName;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;

import br.gov.mt.indea.sistemaformin.entity.AlteracaoSindromeNeurologica;
import br.gov.mt.indea.sistemaformin.entity.FormCOM;
import br.gov.mt.indea.sistemaformin.entity.FormIN;
import br.gov.mt.indea.sistemaformin.entity.FormSN;
import br.gov.mt.indea.sistemaformin.entity.Municipio;
import br.gov.mt.indea.sistemaformin.entity.UF;
import br.gov.mt.indea.sistemaformin.entity.VacinacaoFormSN;
import br.gov.mt.indea.sistemaformin.enums.Dominio.CategoriaDoAnimalSubmetidoAVigilancia;
import br.gov.mt.indea.sistemaformin.enums.Dominio.Especie;
import br.gov.mt.indea.sistemaformin.enums.Dominio.LocalDeColhimentoDeAmostra;
import br.gov.mt.indea.sistemaformin.enums.Dominio.SimNao;
import br.gov.mt.indea.sistemaformin.enums.Dominio.SimNaoSI;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoAlteracaoSindromeNeurologica;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoAmostra;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoDisturbioSindromeNeurologica;
import br.gov.mt.indea.sistemaformin.exception.ApplicationErrorException;
import br.gov.mt.indea.sistemaformin.exception.ApplicationException;
import br.gov.mt.indea.sistemaformin.service.FormCOMService;
import br.gov.mt.indea.sistemaformin.service.FormINService;
import br.gov.mt.indea.sistemaformin.service.FormSNService;
import br.gov.mt.indea.sistemaformin.service.MunicipioService;
import br.gov.mt.indea.sistemaformin.util.FacesMessageUtil;
import br.gov.mt.indea.sistemaformin.webservice.client.ClientWebServiceVeterinario;
import br.gov.mt.indea.sistemaformin.webservice.entity.Propriedade;
import br.gov.mt.indea.sistemaformin.webservice.entity.Veterinario;

@Named
@ViewScoped
@URLBeanName("formSNManagedBean")
@URLMappings(mappings = {
		@URLMapping(id = "pesquisarFormSN", pattern = "/formIN/#{formSNManagedBean.idFormIN}/formSN/pesquisar", viewId = "/pages/formSN/lista.jsf"),
		@URLMapping(id = "incluirFormSN", pattern = "/formIN/#{formSNManagedBean.idFormIN}/formSN/incluir", viewId = "/pages/formSN/novo.jsf"),
		@URLMapping(id = "alterarFormSN", pattern = "/formIN/#{formSNManagedBean.idFormIN}/formSN/alterar/#{formSNManagedBean.id}", viewId = "/pages/formSN/novo.jsf"),
		@URLMapping(id = "impressaoFormSN", pattern = "/formIN/#{formSNManagedBean.idFormIN}/formSN/impressao/#{formSNManagedBean.id}", viewId = "/pages/impressao.jsf")})
public class FormSNManagedBean implements Serializable {

	private static final long serialVersionUID = 1760023349204758559L;

	private Long id;
	
	private Long idFormIN;
	
	private FormIN formIN;
	
	@Inject
	private FormINService formINService;
	
	@Inject
	private FormSNService formSNService;
	
	@Inject
	private FormCOMService formCOMService;
	
	@Inject
	private MunicipioService municipioService;
	
	@Inject
	private FormSN formSN;
	
	@Past(message="Data precisa ser menor ou igual a data atual")
	private Date dataDaColheita;
	
	@Past(message="Data precisa ser menor ou igual a data atual")
	private Date dataDaMorte;
	
	@Past(message="Data precisa ser menor ou igual a data atual")
	private Date dataNotificacao;
	
	@Past(message="Data precisa ser menor ou igual a data atual")
	private Date dataOutraVacina;
	
	@Past(message="Data precisa ser menor ou igual a data atual")
	private Date dataPrimeiraVisita;
	
	@Past(message="Data precisa ser menor ou igual a data atual")
	private Date dataProvavelInicioDaDoenca;
	
	@Past(message="Data precisa ser menor ou igual a data atual")
	private Date dataVacinaBotulismo;
	
	@Past(message="Data precisa ser menor ou igual a data atual")
	private Date dataVacinaCinomose;
	
	@Past(message="Data precisa ser menor ou igual a data atual")
	private Date dataVacinaClostridiose;
	
	@Past(message="Data precisa ser menor ou igual a data atual")
	private Date dataVacinaEncefalomielite;
	
	@Past(message="Data precisa ser menor ou igual a data atual")
	private Date dataVacinaLeptospirose;
	
	@Past(message="Data precisa ser menor ou igual a data atual")
	private Date dataVacinaRaiva;
	
	private String cpfVeterinario;

	private String crmvVeterinario;

	private List<Veterinario> listaVeterinarios;
	
	private boolean editandoVacinacao = false;
	
	private boolean visualizandoVacinacao = false;
	
	private Date dataVacinacao;
	
	private VacinacaoFormSN vacinacao = new VacinacaoFormSN();
	
	@Inject
	private ClientWebServiceVeterinario clientWebServiceVeterinario;
	
	private SimNao propriedadeOndeOAnimalSeEncontraEAMesmaDoFormIN = SimNao.SIM;
	
	private UF uf;
	
	private boolean editandoAlteracaoSindromeNeurologica = false;
	
	private boolean visualizandoAlteracaoSindromeNeurologica = false;
	
	private AlteracaoSindromeNeurologica alteracaoSindromeNeurologica;
	
	public List<Municipio> getListaMunicipios(){
		if (this.uf == null)
			return null;
		
		return municipioService.findAllByUF(this.uf);
	}
	
	@PostConstruct
	private void init(){
		
	}
	
	private void carregarFormIN(){
		this.formIN = formINService.findByIdFetchPropriedade(this.idFormIN);
	}
	
	private void limpar() {
		this.formSN = new FormSN();
		this.dataDaColheita = null;
		this.dataDaMorte = null;
		this.dataNotificacao = null;
		this.dataOutraVacina = null;
		this.dataPrimeiraVisita = null;
		this.dataProvavelInicioDaDoenca = null;
		this.dataVacinaBotulismo = null;
		this.dataVacinaCinomose = null;
		this.dataVacinaClostridiose = null;
		this.dataVacinaEncefalomielite = null;
		this.dataVacinaLeptospirose = null;
		this.dataVacinaRaiva = null;
	}
	
	@URLAction(mappingId = "incluirFormSN", onPostback = false)
	public void novo(){
		this.limpar();
		this.carregarFormIN();
		
		this.formSN.setFormIN(formIN);
		
		this.formSN.setPropriedade(this.formSN.getFormIN().getPropriedade());
	}
	
	@URLAction(mappingId = "alterarFormSN", onPostback = false)
	public void editar(){
		this.formSN = formSNService.findByIdFetchAll(this.getId());
		
		if (this.formSN.getDataDaColheita() != null)
			this.dataDaColheita = this.formSN.getDataDaColheita().getTime();
		if (this.formSN.getDataDaMorte() != null)
			this.dataDaMorte = this.formSN.getDataDaMorte().getTime();
		if (this.formSN.getDataNotificacao() != null)
			this.dataNotificacao = this.formSN.getDataNotificacao().getTime();
		if (this.formSN.getDataOutraVacina() != null)
			this.dataOutraVacina = this.formSN.getDataOutraVacina().getTime();
		if (this.formSN.getDataPrimeiraVisita() != null)
			this.dataPrimeiraVisita = this.formSN.getDataPrimeiraVisita().getTime();
		if (this.formSN.getDataProvavelInicioDaDoenca() != null)
			this.dataProvavelInicioDaDoenca = this.formSN.getDataProvavelInicioDaDoenca().getTime();
		if (this.formSN.getDataVacinaBotulismo() != null)
			this.dataVacinaBotulismo = this.formSN.getDataVacinaBotulismo().getTime();
		if (this.formSN.getDataVacinaCinomose() != null)
			this.dataVacinaCinomose = this.formSN.getDataVacinaCinomose().getTime();
		if (this.formSN.getDataVacinaClostridiose() != null)
			this.dataVacinaClostridiose = this.formSN.getDataVacinaClostridiose().getTime();
		if (this.formSN.getDataVacinaEncefalomielite() != null)
			this.dataVacinaEncefalomielite = this.formSN.getDataVacinaEncefalomielite().getTime();
		if (this.formSN.getDataVacinaLeptospirose() != null)
			this.dataVacinaLeptospirose = this.formSN.getDataVacinaLeptospirose().getTime();
		if (this.formSN.getDataVacinaRaiva() != null)
			this.dataVacinaRaiva = this.formSN.getDataVacinaRaiva().getTime();
			
//		if (this.formSN.getPropriedade().getCodigoPropriedade().equals(this.formSN.getFormIN().getPropriedade().getCodigoPropriedade())){
//			this.propriedadeOndeOAnimalSeEncontraEAMesmaDoFormIN = SimNao.SIM;
//		}else{
//			this.propriedadeOndeOAnimalSeEncontraEAMesmaDoFormIN = SimNao.NAO;
//			this.uf = this.formSN.getPropriedade().getMunicipio().getUf();
//		}
	}
	
	public String adicionar() throws IOException{
		
		if (this.dataDaColheita != null){
			if (this.formSN.getDataDaColheita() == null)
				this.formSN.setDataDaColheita(Calendar.getInstance());
			this.formSN.getDataDaColheita().setTime(dataDaColheita);
		}else
			this.formSN.setDataDaColheita(null);

		if (this.dataDaMorte != null){
			if (this.formSN.getDataDaMorte() == null)
				this.formSN.setDataDaMorte(Calendar.getInstance());
			this.formSN.getDataDaMorte().setTime(dataDaMorte);
		}else
			this.formSN.setDataDaMorte(null);

		if (this.dataNotificacao != null){
			if (this.formSN.getDataNotificacao() == null)
				this.formSN.setDataNotificacao(Calendar.getInstance());
			this.formSN.getDataNotificacao().setTime(dataNotificacao);
		}else
			this.formSN.setDataNotificacao(null);

		if (this.dataOutraVacina != null){
			if (this.formSN.getDataOutraVacina() == null)
				this.formSN.setDataOutraVacina(Calendar.getInstance());
			this.formSN.getDataOutraVacina().setTime(dataOutraVacina);
		}else
			this.formSN.setDataOutraVacina(null);

		if (this.dataPrimeiraVisita != null){
			if (this.formSN.getDataPrimeiraVisita() == null)
				this.formSN.setDataPrimeiraVisita(Calendar.getInstance());
			this.formSN.getDataPrimeiraVisita().setTime(dataPrimeiraVisita);
		}else
			this.formSN.setDataPrimeiraVisita(null);
		
		if (this.dataProvavelInicioDaDoenca != null){
			if (this.formSN.getDataProvavelInicioDaDoenca() == null)
				this.formSN.setDataProvavelInicioDaDoenca(Calendar.getInstance());
			this.formSN.getDataProvavelInicioDaDoenca().setTime(dataProvavelInicioDaDoenca);
		}else
			this.formSN.setDataProvavelInicioDaDoenca(null);

		if (this.dataVacinaBotulismo != null){
			if (this.formSN.getDataVacinaBotulismo() == null)
				this.formSN.setDataVacinaBotulismo(Calendar.getInstance());
			this.formSN.getDataVacinaBotulismo().setTime(dataVacinaBotulismo);
		}else
			this.formSN.setDataVacinaBotulismo(null);

		if (this.dataVacinaCinomose != null){
			if (this.formSN.getDataVacinaCinomose() == null)
				this.formSN.setDataVacinaCinomose(Calendar.getInstance());
			this.formSN.getDataVacinaCinomose().setTime(dataVacinaCinomose);
		}else
			this.formSN.setDataVacinaCinomose(null);

		if (this.dataVacinaClostridiose != null){
			if (this.formSN.getDataVacinaClostridiose() == null)
				this.formSN.setDataVacinaClostridiose(Calendar.getInstance());
			this.formSN.getDataVacinaClostridiose().setTime(dataVacinaClostridiose);
		}else
			this.formSN.setDataVacinaClostridiose(null);	

		if (this.dataVacinaEncefalomielite != null){
			if (this.formSN.getDataVacinaEncefalomielite() == null)
				this.formSN.setDataVacinaEncefalomielite(Calendar.getInstance());
			this.formSN.getDataVacinaEncefalomielite().setTime(dataVacinaEncefalomielite);
		}else
			this.formSN.setDataVacinaEncefalomielite(null);

		if (this.getDataVacinaLeptospirose() != null){
			if (this.formSN.getDataVacinaLeptospirose() == null)
				this.formSN.setDataVacinaLeptospirose(Calendar.getInstance());
			this.formSN.getDataVacinaLeptospirose().setTime(dataVacinaLeptospirose);
		}else
			this.formSN.setDataVacinaLeptospirose(null);
		
		if (this.dataVacinaRaiva != null){
			if (this.formSN.getDataVacinaRaiva() == null)
				this.formSN.setDataVacinaRaiva(Calendar.getInstance());
			this.formSN.getDataVacinaRaiva().setTime(dataVacinaRaiva);
		}else
			this.formSN.setDataVacinaRaiva(null);
		
		boolean isFormOk = true;
		
		if (this.formSN.getVeterinarioColheita() == null){
			FacesContext context = FacesContext.getCurrentInstance();
			
			context.addMessage("cadastro:fieldVeterinarioColheita:campoVeterinarioColheita", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Veterin�rio colheita: valor � obrigat�rio.", "Veterin�rio colheita: valor � obrigat�rio."));
			isFormOk = false;;
		}
		
		if (!isFormOk)
			return "";
		
		if (formSN.getId() != null){
			this.formSNService.saveOrUpdate(formSN);
			FacesMessageUtil.addInfoContextFacesMessage("Form SN atualizado", "");
		}else{
			this.formSN.setDataCadastro(Calendar.getInstance());
			this.formSNService.saveOrUpdate(formSN);
			FacesMessageUtil.addInfoContextFacesMessage("Form SN adicionado", "");
		}
		
		limpar();
	    return "pretty:visaoGeralFormIN";
	}
	
	@Inject
	private VisualizadorImpressaoManagedBean visualizadorImpressaoManagedBean;
	
	@URLAction(mappingId = "impressaoFormSN", onPostback = false)
	public void imprimir() throws ApplicationErrorException{
		this.formSN = formSNService.findByIdFetchAll(this.getId());
		
		visualizadorImpressaoManagedBean.exibirFormSN(formSN);
	}
	
	public void remover(FormSN formSN){
		this.formSNService.delete(formSN);
		FacesMessageUtil.addInfoContextFacesMessage("Form SN exclu�do com sucesso", "");
	}
	
	public List<FormCOM> getListaFormCOM(){
		return formCOMService.findAllBy(this.formSN.getFormIN());
	}
	
	public boolean isEspecieAnimalSilvestre(){
		boolean resultado = false;
		if (this.formSN.getEspecie() == null)
			return resultado;
		else
			resultado = this.formSN.getEspecie().equals(Especie.ANIMAL_SILVESTRE);
		
		if (!resultado)
			this.formSN.setEspecieDoAnimalSilvestre(null);
		
		return resultado;
	}
	
	public boolean isEspecieBovinoOuBubalino(){
		boolean resultado = false;
		if (this.formSN.getEspecie() == null)
			return resultado;
		else
			resultado = this.formSN.getEspecie().equals(Especie.BOVINA) || this.formSN.getEspecie().equals(Especie.BUBALINA);
		
		if (!resultado)
			this.formSN.setPaisDeOrigemDoAnimal(null);
		
		return resultado;
	}
	
	public boolean isOutroLocalDeColhimentoDeAmostraIgual(){
		boolean resultado = false;
		if (this.formSN.getLocalDeColhimentoDeAmostra() == null)
			return resultado;
		else
			resultado = this.formSN.getLocalDeColhimentoDeAmostra().equals(LocalDeColhimentoDeAmostra.OUTROS);
		
		if (!resultado)
			this.formSN.setOutroLocalDeColhimentoDeAmostra(null);
		
		return resultado;
	}
	
	public boolean isOutrasEspeciesAfetadas(){
		boolean resultado = false;
		if (this.formSN.getHaviaOutrasEspeciesAfetadas() == null)
			return resultado;
		else
			resultado = this.formSN.getHaviaOutrasEspeciesAfetadas().equals(SimNao.SIM);
		
		if (!resultado)
			this.formSN.setOutrasEspeciesAfetadas(null);
		
		return resultado;
	}
	
	public TipoDisturbioSindromeNeurologica[] getListaTipoAlteraTipoDisturbioSindromeNeurologica(){
		if (this.alteracaoSindromeNeurologica != null){
			if (this.alteracaoSindromeNeurologica.getTipoAlteracaoSindromeNeurologica() != null){
				if (this.alteracaoSindromeNeurologica.getTipoAlteracaoSindromeNeurologica().equals(TipoAlteracaoSindromeNeurologica.NEUROLOGICA_OU_DE_SENSIBILIDADE))
					return TipoDisturbioSindromeNeurologica.getListaTiposDeAlteracaoNeurologicaOuDeSensibilidade();
				else if (this.alteracaoSindromeNeurologica.getTipoAlteracaoSindromeNeurologica().equals(TipoAlteracaoSindromeNeurologica.POSTURA_OU_LOCOMOCAO))
					return TipoDisturbioSindromeNeurologica.getListaTiposDeAlteracaoDePosturaOuLocomocao();
				else if (this.alteracaoSindromeNeurologica.getTipoAlteracaoSindromeNeurologica().equals(TipoAlteracaoSindromeNeurologica.COMPORTAMENTAL))
					return TipoDisturbioSindromeNeurologica.getListaTiposDeAlteracaoComportamental(); 
			}
		}
		return null;
		
	}
	
	public boolean isCategoriaDoAnimalIgualADisturbioNeurologico(){
		boolean resultado = false;
		if (this.formSN.getCategoriaDoAnimalSubmetidoAVigilancia() == null)
			return resultado;
		else
			resultado = this.formSN.getCategoriaDoAnimalSubmetidoAVigilancia().equals(CategoriaDoAnimalSubmetidoAVigilancia.COM_SINAIS_CLINICOS_DE_DOENCA_NERVOSA);
		
		if (!resultado){
			this.formSN.setQuantidadeDiasDisturbioNeurologico(null);
			if (this.formSN.getListaAlteracoesSindromeNeurologica() != null)
				this.formSN.getListaAlteracoesSindromeNeurologica().clear();
		}
		
		return resultado;
	}
	
	public boolean isCategoriaDoAnimalIgualADoencaCronica(){
		boolean resultado = false;
		if (this.formSN.getCategoriaDoAnimalSubmetidoAVigilancia() == null)
			return resultado;
		else
			resultado = this.formSN.getCategoriaDoAnimalSubmetidoAVigilancia().equals(CategoriaDoAnimalSubmetidoAVigilancia.COM_DOENCA_CRONICA);
		
		if (!resultado)
			this.formSN.setQuantidadeDiasDoencaCronica(null);
		
		return resultado;
	}
	
	public boolean isAnimaisRecuperados(){
		boolean resultado = false;
		if (this.formSN.getHaAnimaisQueSerecuperaramDosSinaisClinicos() == null)
			return resultado;
		else
			resultado = this.formSN.getHaAnimaisQueSerecuperaramDosSinaisClinicos().equals(SimNaoSI.SIM);
		
		if (!resultado)
			this.formSN.setPercentualDeAnimaisRecuperados(null);
		
		return resultado;
	}
	
	public boolean isTipoDeAmostraIgualAOutro(){
		boolean resultado = false;
		if (this.formSN.getTipoAmostra() == null)
			return resultado;
		else
			resultado = this.formSN.getTipoAmostra().contains(TipoAmostra.OUTRAS);
		
		if (!resultado)
			this.formSN.setOutroTipoDeAmostra(null);
		
		return resultado;
	}
	
	public boolean isAnimalMortoVacinadoRaiva(){
		boolean resultado = false;
		if (this.formSN.getVacinadoRaiva() == null)
			return resultado;
		else
			resultado = this.formSN.getVacinadoRaiva().equals(SimNao.SIM);
		
		if (!resultado)
			this.dataVacinaRaiva = null;
		
		return resultado;
	}
	
	public boolean isAnimalMortoVacinadoClostridiose(){
		boolean resultado = false;
		if (this.formSN.getVacinadoClostridiose() == null)
			return resultado;
		else
			resultado = this.formSN.getVacinadoClostridiose().equals(SimNao.SIM);
		
		if (!resultado)
			this.dataVacinaClostridiose = null;
		
		return resultado;
	}
	
	public boolean isAnimalMortoVacinadoCinomose(){
		boolean resultado = false;
		if (this.formSN.getVacinadoCinomose() == null)
			return resultado;
		else
			resultado = this.formSN.getVacinadoCinomose().equals(SimNao.SIM);
		
		if (!resultado)
			this.dataVacinaCinomose = null;
		
		return resultado;
	}
	
	public boolean isAnimalMortoVacinadoLeptospirose(){
		boolean resultado = false;
		if (this.formSN.getVacinadoLeptospirose() == null)
			return resultado;
		else
			resultado = this.formSN.getVacinadoLeptospirose().equals(SimNao.SIM);
		
		if (!resultado)
			this.dataVacinaLeptospirose = null;
		
		return resultado;
	}
	
	public boolean isAnimalMortoVacinadoBotulismo(){
		boolean resultado = false;
		if (this.formSN.getVacinadoBotulismo() == null)
			return resultado;
		else
			resultado = this.formSN.getVacinadoBotulismo().equals(SimNao.SIM);
		
		if (!resultado)
			this.dataVacinaBotulismo = null;
		
		return resultado;
	}
	
	public boolean isAnimalMortoVacinadoEncefalomielite(){
		boolean resultado = false;
		if (this.formSN.getVacinadoEncefalomielite() == null)
			return resultado;
		else
			resultado = this.formSN.getVacinadoEncefalomielite().equals(SimNao.SIM);
		
		if (!resultado)
			this.dataVacinaEncefalomielite = null;
		
		return resultado;
	}
	
	public boolean isAnimalMortoOutraVacina(){
		boolean resultado = false;
		if (this.formSN.getOutraVacina() == null)
			return resultado;
		else
			resultado = this.formSN.getOutraVacina().equals(SimNao.SIM);
		
		if (!resultado)
			this.dataOutraVacina = null;
		
		return resultado;
	}
	
	public void abrirTelaDeBuscaDeVeterinarioColheita(){
		this.cpfVeterinario = null;
		this.crmvVeterinario = null;
		this.listaVeterinarios = null;
	}
	
	public void buscarVeterinarioPorCpf(){
		this.listaVeterinarios = null;
		
		Veterinario v;
		try {
			v = clientWebServiceVeterinario.getVeterinarioByCpf(cpfVeterinario);
			
			if (v != null){
				this.listaVeterinarios = new ArrayList<Veterinario>();
				this.listaVeterinarios.add(v);
			}
		} catch (ApplicationException e) {
			FacesMessageUtil.addErrorContextFacesMessage("Erro", e.getLocalizedMessage());
			return;
		}
		
		if (this.listaVeterinarios == null || this.listaVeterinarios.isEmpty())
			FacesMessageUtil.addWarnContextFacesMessage("Nenhum veterin�rio foi encontrado", "");
	}
	
	public void buscarVeterinarioPorCrmv(){
		this.listaVeterinarios = null;
		
		Veterinario v;
		try {
			v = clientWebServiceVeterinario.getVeterinarioByCRMV(crmvVeterinario);
			
			if (v != null){
				this.listaVeterinarios = new ArrayList<Veterinario>();
				this.listaVeterinarios.add(v);
			}
		} catch (ApplicationException e) {
			FacesMessageUtil.addErrorContextFacesMessage("Erro", e.getLocalizedMessage());
			return;
		}
		
		if (this.listaVeterinarios == null || this.listaVeterinarios.isEmpty())
			FacesMessageUtil.addWarnContextFacesMessage("Nenhum veterin�rio foi encontrado", "");
	}
	
	public void selecionarVeterinario(Veterinario veterinario){
		this.formSN.setVeterinarioColheita(veterinario);
		FacesMessageUtil.addInfoContextFacesMessage("Veterinario " + this.formSN.getVeterinarioColheita().getNome() + " selecionado", "");
	}
	
	public void adicionarVacinacao(){
		this.vacinacao.setFormSN(formSN);
		
		if (this.dataVacinacao != null){
			if (this.vacinacao.getData() == null)
				this.vacinacao.setData(Calendar.getInstance());
			this.vacinacao.getData().setTime(dataVacinacao);
		}else
			this.vacinacao.setData(null);
		this.dataVacinacao = null;
		
		if (!editandoVacinacao)
			this.formSN.getOutrasVacinas().add(vacinacao);
		else
			editandoVacinacao = false;
		
		this.vacinacao = new VacinacaoFormSN();
		this.dataVacinacao = null;
		
		FacesMessageUtil.addInfoContextFacesMessage("Vacina��o inclu�da com sucesso", "");
	}
	
	public void novaVacinacao(){
		this.vacinacao= new VacinacaoFormSN();
		this.editandoVacinacao = false;
		this.visualizandoVacinacao = false;

		this.dataVacinacao = null;
	}
	
	public void removerVacinacao(VacinacaoFormSN vacinacao){
		this.formSN.getOutrasVacinas().remove(vacinacao);
	}
	
	public void editarVacinacao(VacinacaoFormSN vacinacao){
		this.editandoVacinacao = true;
		this.visualizandoVacinacao = false;
		this.vacinacao = vacinacao;
		
		if (this.vacinacao.getData() != null)
			this.dataVacinacao = this.vacinacao.getData().getTime();
	}
	
	public void visualizarVacinacao(VacinacaoFormSN vacinacao){
		this.visualizandoVacinacao = true;
		this.editandoVacinacao = false;
		this.vacinacao = vacinacao;
		
		if (this.vacinacao.getData() != null)
			this.dataVacinacao = this.vacinacao.getData().getTime();
	}
	
	public void alterarPropriedade(){
		
		if (this.propriedadeOndeOAnimalSeEncontraEAMesmaDoFormIN.equals(SimNao.SIM))
			this.formSN.setPropriedade(this.formSN.getFormIN().getPropriedade());
		else
			this.formSN.setPropriedade(new Propriedade());
		
	}
	
	public void adicionarAlteracao(){
		this.alteracaoSindromeNeurologica.setFormSN(formSN);
		
		if (!editandoAlteracaoSindromeNeurologica){
			if (this.formSN.getListaAlteracoesSindromeNeurologica() == null)
				this.formSN.setListaAlteracoesSindromeNeurologica(new ArrayList<AlteracaoSindromeNeurologica>());
			
			this.formSN.getListaAlteracoesSindromeNeurologica().add(alteracaoSindromeNeurologica);
		}else
			editandoAlteracaoSindromeNeurologica = false;
		
		this.alteracaoSindromeNeurologica = new AlteracaoSindromeNeurologica();
		
		FacesMessageUtil.addInfoContextFacesMessage("Tipo de altera��o inclu�do com sucesso", "");
	}
	
	public void novaAlteracao(){
		this.alteracaoSindromeNeurologica= new AlteracaoSindromeNeurologica();
		this.editandoAlteracaoSindromeNeurologica = false;
		this.visualizandoAlteracaoSindromeNeurologica = false;
	}
	
	public void removerAlteracao(AlteracaoSindromeNeurologica alteracao){
		this.formSN.getListaAlteracoesSindromeNeurologica().remove(alteracao);
	}
	
	public void editarAlteracao(AlteracaoSindromeNeurologica alteracao){
		this.editandoAlteracaoSindromeNeurologica = true;
		this.visualizandoAlteracaoSindromeNeurologica = false;
		this.alteracaoSindromeNeurologica = alteracao;
	}
	
	public void visualizarAlteracao(AlteracaoSindromeNeurologica alteracao){
		this.visualizandoAlteracaoSindromeNeurologica = true;
		this.editandoAlteracaoSindromeNeurologica = false;
		this.alteracaoSindromeNeurologica = alteracao;
	}
	
	public FormSN getFormSN() {
		return formSN;
	}

	public void setFormSN(FormSN formSN) {
		this.formSN = formSN;
	}

	public Date getDataDaColheita() {
		return dataDaColheita;
	}

	public void setDataDaColheita(Date dataDaColheita) {
		this.dataDaColheita = dataDaColheita;
	}

	public Date getDataDaMorte() {
		return dataDaMorte;
	}

	public void setDataDaMorte(Date dataDaMorte) {
		this.dataDaMorte = dataDaMorte;
	}

	public Date getDataNotificacao() {
		return dataNotificacao;
	}

	public void setDataNotificacao(Date dataNotificacao) {
		this.dataNotificacao = dataNotificacao;
	}

	public Date getDataOutraVacina() {
		return dataOutraVacina;
	}

	public void setDataOutraVacina(Date dataOutraVacina) {
		this.dataOutraVacina = dataOutraVacina;
	}

	public Date getDataPrimeiraVisita() {
		return dataPrimeiraVisita;
	}

	public void setDataPrimeiraVisita(Date dataPrimeiraVisita) {
		this.dataPrimeiraVisita = dataPrimeiraVisita;
	}

	public Date getDataProvavelInicioDaDoenca() {
		return dataProvavelInicioDaDoenca;
	}

	public void setDataProvavelInicioDaDoenca(Date dataProvavelInicioDaDoenca) {
		this.dataProvavelInicioDaDoenca = dataProvavelInicioDaDoenca;
	}

	public Date getDataVacinaBotulismo() {
		return dataVacinaBotulismo;
	}

	public void setDataVacinaBotulismo(Date dataVacinaBotulismo) {
		this.dataVacinaBotulismo = dataVacinaBotulismo;
	}

	public Date getDataVacinaCinomose() {
		return dataVacinaCinomose;
	}

	public void setDataVacinaCinomose(Date dataVacinaCinomose) {
		this.dataVacinaCinomose = dataVacinaCinomose;
	}

	public Date getDataVacinaClostridiose() {
		return dataVacinaClostridiose;
	}

	public void setDataVacinaClostridiose(Date dataVacinaClostridiose) {
		this.dataVacinaClostridiose = dataVacinaClostridiose;
	}

	public Date getDataVacinaEncefalomielite() {
		return dataVacinaEncefalomielite;
	}

	public void setDataVacinaEncefalomielite(Date dataVacinaEncefalomielite) {
		this.dataVacinaEncefalomielite = dataVacinaEncefalomielite;
	}

	public Date getDataVacinaLeptospirose() {
		return dataVacinaLeptospirose;
	}

	public void setDataVacinaLeptospirose(Date dataVacinaLeptospirose) {
		this.dataVacinaLeptospirose = dataVacinaLeptospirose;
	}

	public Date getDataVacinaRaiva() {
		return dataVacinaRaiva;
	}

	public void setDataVacinaRaiva(Date dataVacinaRaiva) {
		this.dataVacinaRaiva = dataVacinaRaiva;
	}

	public String getCpfVeterinario() {
		return cpfVeterinario;
	}

	public void setCpfVeterinario(String cpfVeterinario) {
		this.cpfVeterinario = cpfVeterinario;
	}

	public String getCrmvVeterinario() {
		return crmvVeterinario;
	}

	public void setCrmvVeterinario(String crmvVeterinario) {
		this.crmvVeterinario = crmvVeterinario;
	}

	public List<Veterinario> getListaVeterinarios() {
		return listaVeterinarios;
	}

	public void setListaVeterinarios(List<Veterinario> listaVeterinarios) {
		this.listaVeterinarios = listaVeterinarios;
	}

	public boolean isEditandoVacinacao() {
		return editandoVacinacao;
	}

	public void setEditandoVacinacao(boolean editandoVacinacao) {
		this.editandoVacinacao = editandoVacinacao;
	}

	public boolean isVisualizandoVacinacao() {
		return visualizandoVacinacao;
	}

	public void setVisualizandoVacinacao(boolean visualizandoVacinacao) {
		this.visualizandoVacinacao = visualizandoVacinacao;
	}

	public VacinacaoFormSN getVacinacao() {
		return vacinacao;
	}

	public void setVacinacao(VacinacaoFormSN vacinacao) {
		this.vacinacao = vacinacao;
	}

	public Date getDataVacinacao() {
		return dataVacinacao;
	}

	public void setDataVacinacao(Date dataVacinacao) {
		this.dataVacinacao = dataVacinacao;
	}

	public SimNao getPropriedadeOndeOAnimalSeEncontraEAMesmaDoFormIN() {
		return propriedadeOndeOAnimalSeEncontraEAMesmaDoFormIN;
	}

	public void setPropriedadeOndeOAnimalSeEncontraEAMesmaDoFormIN(
			SimNao propriedadeOndeOAnimalSeEncontraEAMesmaDoFormIN) {
		this.propriedadeOndeOAnimalSeEncontraEAMesmaDoFormIN = propriedadeOndeOAnimalSeEncontraEAMesmaDoFormIN;
	}
	
	public boolean isPropriedadeOndeOAnimalSeEncontraEAMesmaDoFormIN_SIM(){
		return propriedadeOndeOAnimalSeEncontraEAMesmaDoFormIN.equals(SimNao.SIM);
	}

	public UF getUf() {
		return uf;
	}

	public void setUf(UF uf) {
		this.uf = uf;
	}

	public boolean isEditandoAlteracaoSindromeNeurologica() {
		return editandoAlteracaoSindromeNeurologica;
	}

	public void setEditandoAlteracaoSindromeNeurologica(
			boolean editandoAlteracaoSindromeNeurologica) {
		this.editandoAlteracaoSindromeNeurologica = editandoAlteracaoSindromeNeurologica;
	}

	public boolean isVisualizandoAlteracaoSindromeNeurologica() {
		return visualizandoAlteracaoSindromeNeurologica;
	}

	public void setVisualizandoAlteracaoSindromeNeurologica(
			boolean visualizandoAlteracaoSindromeNeurologica) {
		this.visualizandoAlteracaoSindromeNeurologica = visualizandoAlteracaoSindromeNeurologica;
	}

	public AlteracaoSindromeNeurologica getAlteracaoSindromeNeurologica() {
		return alteracaoSindromeNeurologica;
	}

	public void setAlteracaoSindromeNeurologica(
			AlteracaoSindromeNeurologica alteracaoSindromeNeurologica) {
		this.alteracaoSindromeNeurologica = alteracaoSindromeNeurologica;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdFormIN() {
		return idFormIN;
	}

	public void setIdFormIN(Long idFormIN) {
		this.idFormIN = idFormIN;
	}

	public FormIN getFormIN() {
		return formIN;
	}

	public void setFormIN(FormIN formIN) {
		this.formIN = formIN;
	}

}
