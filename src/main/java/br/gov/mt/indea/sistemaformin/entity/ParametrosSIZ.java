package br.gov.mt.indea.sistemaformin.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;
import org.hibernate.validator.constraints.Email;

@Audited
@Entity
@Table(name="parametros_siz")
public class ParametrosSIZ extends BaseEntity<Long>{

	private static final long serialVersionUID = -687298611059159152L;

	@Id
	@SequenceGenerator(name="parametros_siz_seq", sequenceName="parametros_siz_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="parametros_siz_seq")
	private Long id;
	
	@Column(name="email_sfa")
	@Email
	private String emailSFA;
	
	@Column
	private Long diasParaExpiracaoDeUsuario;
	
	protected ParametrosSIZ() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmailSFA() {
		return emailSFA;
	}

	public void setEmailSFA(String emailSFA) {
		this.emailSFA = emailSFA;
	}

	public Long getDiasParaExpiracaoDeUsuario() {
		if (this.diasParaExpiracaoDeUsuario == null)
			return 1000L;
		return diasParaExpiracaoDeUsuario;
	}

	public void setDiasParaExpiracaoDeUsuario(Long diasParaExpiracaoDeUsuario) {
		this.diasParaExpiracaoDeUsuario = diasParaExpiracaoDeUsuario;
	}
	
}
