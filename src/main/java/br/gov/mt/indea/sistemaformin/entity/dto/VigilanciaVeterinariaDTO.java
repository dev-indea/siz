package br.gov.mt.indea.sistemaformin.entity.dto;

import java.io.Serializable;
import java.util.Date;

import br.gov.mt.indea.sistemaformin.entity.Municipio;
import br.gov.mt.indea.sistemaformin.entity.UF;

public class VigilanciaVeterinariaDTO implements AbstractDTO, Serializable {

	private static final long serialVersionUID = 958534186952468759L;

	private UF uf = UF.MATO_GROSSO;

	private Municipio municipio;

	private String numero;

	private Date data;

	private String propriedade;
	
	public boolean isNull(){
		if (uf == null && municipio == null && numero == null && data == null && propriedade == null)
			return true;
		return false;
	}

	public UF getUf() {
		return uf;
	}

	public void setUf(UF uf) {
		this.uf = uf;
	}

	public Municipio getMunicipio() {
		return municipio;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public String getPropriedade() {
		return propriedade;
	}

	public void setPropriedade(String propriedade) {
		this.propriedade = propriedade;
	}
	
}
