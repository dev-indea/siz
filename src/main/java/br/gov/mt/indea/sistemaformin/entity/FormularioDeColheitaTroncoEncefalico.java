package br.gov.mt.indea.sistemaformin.entity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

import br.gov.mt.indea.sistemaformin.enums.Dominio.CategoriaAnimal;
import br.gov.mt.indea.sistemaformin.enums.Dominio.Especie;
import br.gov.mt.indea.sistemaformin.enums.Dominio.MachoFemea;
import br.gov.mt.indea.sistemaformin.enums.Dominio.MotivacaoParaAbateDeEmergenciaEmFrigorifico;
import br.gov.mt.indea.sistemaformin.enums.Dominio.SinaisClinicosNervososEncefalopatoaEspongiformeTransmissivel;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoDeMorteEmFrigorifico;
import br.gov.mt.indea.sistemaformin.webservice.entity.Gta;
import br.gov.mt.indea.sistemaformin.webservice.entity.Veterinario;

@Audited
@Entity
@Table(name = "formulario_colheita_tronco")
public class FormularioDeColheitaTroncoEncefalico extends BaseEntity<Long> implements Cloneable {

	private static final long serialVersionUID = 8959003967936831055L;

	@Id
	@SequenceGenerator(name = "formulario_colheita_tronco_seq", sequenceName = "formulario_colheita_tronco_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "formulario_colheita_tronco_seq")
	private Long id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_cadastro")
	private Calendar dataCadastro;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_coleta")
	private Calendar dataColeta;
	
	@ManyToOne
	@JoinColumn(name="id_achado_lote_enferm_abat_frig")
	private AchadosLoteEnfermidadeAbatedouroFrigorifico achadosLoteEnfermidadeAbatedouroFrigorifico;
	
	@Column(length=50)
	private String numero;
	
	@Enumerated(EnumType.STRING)
	@Column(name="tipo_morte")
    private TipoDeMorteEmFrigorifico tipoDeMorteEmFrigorifico;;
    
    @ElementCollection(targetClass = MotivacaoParaAbateDeEmergenciaEmFrigorifico.class, fetch=FetchType.LAZY)
	@JoinTable(name = "formulario_colheita_motivacao", joinColumns = @JoinColumn(name = "id_formulario_colheita", nullable = false))
	@Column(name = "id_motivacao")
	@Enumerated(EnumType.STRING)
	private List<MotivacaoParaAbateDeEmergenciaEmFrigorifico> listaMotivacaoParaAbateDeEmergenciaEmFrigorifico = new ArrayList<MotivacaoParaAbateDeEmergenciaEmFrigorifico>();
    
    @Column(name="outra_motivacao", length=255)
    private String outraMotivacao;
    
    @ElementCollection(targetClass = SinaisClinicosNervososEncefalopatoaEspongiformeTransmissivel.class, fetch=FetchType.LAZY)
	@JoinTable(name = "formulario_colheita_sinais", joinColumns = @JoinColumn(name = "id_formulario_colheita", nullable = false))
	@Column(name = "id_sinais_clinicos_nervosos")
	@Enumerated(EnumType.STRING)
	private List<SinaisClinicosNervososEncefalopatoaEspongiformeTransmissivel> listaSinaisClinicosNervososEncefalopatoaEspongiformeTransmissivel = new ArrayList<SinaisClinicosNervososEncefalopatoaEspongiformeTransmissivel>();

    @Column(name="outro_sinal", length=255)
    private String outroSinal;
    
    @Enumerated(EnumType.STRING)
	@Column(name="especie")
	private Especie especie;
    
    @OneToOne(cascade={CascadeType.REFRESH})
	@JoinColumn(name="id_pais")
	private Pais pais;
    
    @Enumerated(EnumType.STRING)
	@Column(name="sexo")
	private MachoFemea sexo;
    
    private Double idade;
    
    @Column(name="raca", length=255)
    private String raca;
    
    @Enumerated(EnumType.STRING)
	@Column(name="categoria_animal")
	private CategoriaAnimal categoriaAnimal;
    
    @OneToOne(cascade={CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
	@JoinColumn(name="id_gta")
	private Gta gta;
    
    @Column(name="id_animal", length=255)
    private String idAnimal;
    
    @Column(name="id_carcaca", length=255)
    private String idCarcaca;
    
    @OneToOne(cascade={CascadeType.REFRESH, CascadeType.DETACH}, fetch=FetchType.EAGER)
	@JoinColumn(name="id_veterinario")
	private Veterinario veterinario;
    
    @OneToOne(cascade={CascadeType.ALL}, orphanRemoval=true, fetch=FetchType.EAGER)
	@JoinColumn(name="id_relatorio_de_ensaio")
	private RelatorioDeEnsaio relatorioDeEnsaio;
    
    public String getListaMotivacaoParaAbateDeEmergenciaEmFrigorificoAsString() {
    	if (this.getListaMotivacaoParaAbateDeEmergenciaEmFrigorifico() == null || this.getListaMotivacaoParaAbateDeEmergenciaEmFrigorifico().isEmpty())
			return null;
		
		StringBuilder sb = null;
		
		for (MotivacaoParaAbateDeEmergenciaEmFrigorifico motivacaoParaAbateDeEmergenciaEmFrigorifico : getListaMotivacaoParaAbateDeEmergenciaEmFrigorifico()) {
			if (sb == null){
				sb = new StringBuilder();
				sb.append(motivacaoParaAbateDeEmergenciaEmFrigorifico.getDescricao());
			}else
				sb.append(", ")
				  .append(motivacaoParaAbateDeEmergenciaEmFrigorifico.getDescricao());
		}
		
		return sb.toString();
    }
    
    public String getListaSinaisClinicosNervososEncefalopatoaEspongiformeTransmissivelAsString() {
    	if (this.getListaSinaisClinicosNervososEncefalopatoaEspongiformeTransmissivel() == null || this.getListaSinaisClinicosNervososEncefalopatoaEspongiformeTransmissivel().isEmpty())
			return null;
		
		StringBuilder sb = null;
		
		for (SinaisClinicosNervososEncefalopatoaEspongiformeTransmissivel sinaisClinicosNervososEncefalopatoaEspongiformeTransmissivel : getListaSinaisClinicosNervososEncefalopatoaEspongiformeTransmissivel()) {
			if (sb == null){
				sb = new StringBuilder();
				sb.append(sinaisClinicosNervososEncefalopatoaEspongiformeTransmissivel.getDescricao());
			}else
				sb.append(", ")
				  .append(sinaisClinicosNervososEncefalopatoaEspongiformeTransmissivel.getDescricao());
		}
		
		return sb.toString();
    }
    
    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Calendar getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Calendar dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public AchadosLoteEnfermidadeAbatedouroFrigorifico getAchadosLoteEnfermidadeAbatedouroFrigorifico() {
		return achadosLoteEnfermidadeAbatedouroFrigorifico;
	}

	public void setAchadosLoteEnfermidadeAbatedouroFrigorifico(
			AchadosLoteEnfermidadeAbatedouroFrigorifico achadosLoteEnfermidadeAbatedouroFrigorifico) {
		this.achadosLoteEnfermidadeAbatedouroFrigorifico = achadosLoteEnfermidadeAbatedouroFrigorifico;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public TipoDeMorteEmFrigorifico getTipoDeMorteEmFrigorifico() {
		return tipoDeMorteEmFrigorifico;
	}

	public void setTipoDeMorteEmFrigorifico(TipoDeMorteEmFrigorifico tipoDeMorteEmFrigorifico) {
		this.tipoDeMorteEmFrigorifico = tipoDeMorteEmFrigorifico;
	}

	public List<MotivacaoParaAbateDeEmergenciaEmFrigorifico> getListaMotivacaoParaAbateDeEmergenciaEmFrigorifico() {
		return listaMotivacaoParaAbateDeEmergenciaEmFrigorifico;
	}

	public void setListaMotivacaoParaAbateDeEmergenciaEmFrigorifico(
			List<MotivacaoParaAbateDeEmergenciaEmFrigorifico> listaMotivacaoParaAbateDeEmergenciaEmFrigorifico) {
		this.listaMotivacaoParaAbateDeEmergenciaEmFrigorifico = listaMotivacaoParaAbateDeEmergenciaEmFrigorifico;
	}

	public List<SinaisClinicosNervososEncefalopatoaEspongiformeTransmissivel> getListaSinaisClinicosNervososEncefalopatoaEspongiformeTransmissivel() {
		return listaSinaisClinicosNervososEncefalopatoaEspongiformeTransmissivel;
	}

	public void setListaSinaisClinicosNervososEncefalopatoaEspongiformeTransmissivel(
			List<SinaisClinicosNervososEncefalopatoaEspongiformeTransmissivel> listaSinaisClinicosNervososEncefalopatoaEspongiformeTransmissivel) {
		this.listaSinaisClinicosNervososEncefalopatoaEspongiformeTransmissivel = listaSinaisClinicosNervososEncefalopatoaEspongiformeTransmissivel;
	}

	public String getOutraMotivacao() {
		return outraMotivacao;
	}

	public void setOutraMotivacao(String outraMotivacao) {
		this.outraMotivacao = outraMotivacao;
	}

	public String getOutroSinal() {
		return outroSinal;
	}

	public void setOutroSinal(String outroSinal) {
		this.outroSinal = outroSinal;
	}

	public Especie getEspecie() {
		return especie;
	}

	public void setEspecie(Especie especie) {
		this.especie = especie;
	}

	public MachoFemea getSexo() {
		return sexo;
	}

	public void setSexo(MachoFemea sexo) {
		this.sexo = sexo;
	}

	public Double getIdade() {
		return idade;
	}

	public void setIdade(Double idade) {
		this.idade = idade;
	}

	public String getRaca() {
		return raca;
	}

	public void setRaca(String raca) {
		this.raca = raca;
	}

	public CategoriaAnimal getCategoriaAnimal() {
		return categoriaAnimal;
	}

	public void setCategoriaAnimal(CategoriaAnimal categoriaAnimal) {
		this.categoriaAnimal = categoriaAnimal;
	}

	public Calendar getDataColeta() {
		return dataColeta;
	}

	public void setDataColeta(Calendar dataColeta) {
		this.dataColeta = dataColeta;
	}

	public Gta getGta() {
		return gta;
	}

	public void setGta(Gta gta) {
		this.gta = gta;
	}

	public String getIdAnimal() {
		return idAnimal;
	}

	public void setIdAnimal(String idAnimal) {
		this.idAnimal = idAnimal;
	}

	public String getIdCarcaca() {
		return idCarcaca;
	}

	public void setIdCarcaca(String idCarcaca) {
		this.idCarcaca = idCarcaca;
	}

	public Pais getPais() {
		return pais;
	}

	public void setPais(Pais pais) {
		this.pais = pais;
	}

	public Veterinario getVeterinario() {
		return veterinario;
	}

	public void setVeterinario(Veterinario veterinario) {
		this.veterinario = veterinario;
	}

	public RelatorioDeEnsaio getRelatorioDeEnsaio() {
		return relatorioDeEnsaio;
	}

	public void setRelatorioDeEnsaio(RelatorioDeEnsaio relatorioDeEnsaio) {
		this.relatorioDeEnsaio = relatorioDeEnsaio;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FormularioDeColheitaTroncoEncefalico other = (FormularioDeColheitaTroncoEncefalico) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
