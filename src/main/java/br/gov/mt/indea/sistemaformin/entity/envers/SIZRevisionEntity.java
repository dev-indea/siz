package br.gov.mt.indea.sistemaformin.entity.envers;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import org.hibernate.envers.RevisionEntity;
import org.hibernate.envers.RevisionNumber;
import org.hibernate.envers.RevisionTimestamp;

import br.gov.mt.indea.sistemaformin.listener.SIZEnversListener;

@Entity(name="siz_auditoria")
@RevisionEntity(SIZEnversListener.class)
public class SIZRevisionEntity implements Serializable{

	private static final long serialVersionUID = 1280119129267690772L;

	@Id
	@SequenceGenerator(name = "revision_entity_seq", sequenceName = "revision_entity_seq", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "revision_entity_seq")
	@Column(name = "CODG_AUDITORIA", nullable = false)
	@RevisionNumber
	private int id;
	
	@Column(name="username_usuario", nullable=false)
	private String username;
	
	@Column(name="nome_usuario", nullable=false)
	private String usuario;
	
	private String ip;
	
	@RevisionTimestamp
	@Column(name="data_operacao")
	private Date dataOperacao;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public Date getDataOperacao() {
		return dataOperacao;
	}

	public void setDataOperacao(Date dataOperacao) {
		this.dataOperacao = dataOperacao;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

}
