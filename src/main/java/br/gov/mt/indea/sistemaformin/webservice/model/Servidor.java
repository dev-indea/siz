package br.gov.mt.indea.sistemaformin.webservice.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Servidor implements Serializable{

	private static final long serialVersionUID = -4859952141712886289L;
	
	@XmlElement
	private Pessoa pessoa;
	
	@XmlElement(name = "ule")
	private ULE ule;
	
	private String matricula;
	
	@XmlElement(name = "profissao")
	private Profissao profissao;

	public ULE getUle() {
		return ule;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	public void setUle(ULE ule) {
		this.ule = ule;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public Profissao getProfissao() {
		return profissao;
	}

	public void setProfissao(Profissao profissao) {
		this.profissao = profissao;
	}

}
