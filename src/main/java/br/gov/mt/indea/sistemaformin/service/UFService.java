package br.gov.mt.indea.sistemaformin.service;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.hibernate.Query;

import br.gov.mt.indea.sistemaformin.entity.UF;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class UFService extends PaginableService<UF, Long> {

	protected UFService() {
		super(UF.class);
	}
	
	public UF findByIdFetchAll(Long id){
		UF uf;
		
		StringBuilder sql = new StringBuilder();
		sql.append("select uf ")
		   .append("  from UF uf ")
		   .append(" where uf.id = :id ");
		
		Query query = getSession().createQuery(sql.toString()).setLong("id", id);
		uf = (UF) query.uniqueResult();
		
		return uf;
	}
	
	public UF findBySigla(String sigla){
		UF uf;
		
		StringBuilder sql = new StringBuilder();
		sql.append("select uf ")
		   .append("  from UF uf ")
		   .append(" where uf.uf = :uf ");
		
		Query query = getSession().createQuery(sql.toString());
		query.setString("uf", sigla);
		uf = (UF) query.uniqueResult();
		
		return uf;
	}
	
	@SuppressWarnings("unchecked")
	public List<UF> findAllByUF(UF uf){
		List<UF> listaUF = new ArrayList<>();
		
		StringBuilder sql = new StringBuilder();
		sql.append("select entity ")
		   .append("  from UF entity ")
		   .append("  join fetch entity.uf ")
		   .append(" where entity.uf = :uf ");
		
		Query query = getSession().createQuery(sql.toString()).setParameter("uf", uf);
		listaUF = query.list();
		
		return listaUF;
	}
	
	@SuppressWarnings("unchecked")
	public List<UF> findAllFetchMunicipio(){
		List<UF> listaUF = new ArrayList<>();
		
		StringBuilder sql = new StringBuilder();
		sql.append("select distinct entity ")
		   .append("  from UF as entity ")
		   .append("  join fetch entity.municipios ");
		
		Query query = getSession().createQuery(sql.toString());
		listaUF = query.list();
		
		return listaUF;
	}
	
	@Override
	public void validar(UF UF) {

	}

	@Override
	public void validarPersist(UF UF) {

	}

	@Override
	public void validarMerge(UF UF) {

	}

	@Override
	public void validarDelete(UF UF) {

	}

}
