package br.gov.mt.indea.sistemaformin.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import org.hibernate.envers.Audited;

import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoOcorrenciaObservadaFormCOM;

@Audited
@Entity(name="ocorrencia_observada")
public class OcorrenciaObservadaFormCOM extends BaseEntity<Long>{

	private static final long serialVersionUID = -3350452967401501532L;
	
	@Id
	@SequenceGenerator(name="ocorrencia_observada_seq", sequenceName="ocorrencia_observada_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ocorrencia_observada_seq")
	private Long id;
	
	@ManyToOne
	@JoinColumn(name="id_formCOM")
	private FormCOM formCOM;
	
	private String especie;
	
	@Enumerated(EnumType.STRING)
	@Column(name="tipo")
	private TipoOcorrenciaObservadaFormCOM tipo;
	
	private Long quantidade;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public FormCOM getFormCOM() {
		return formCOM;
	}

	public void setFormCOM(FormCOM formCOM) {
		this.formCOM = formCOM;
	}

	public String getEspecie() {
		return especie;
	}

	public void setEspecie(String especie) {
		this.especie = especie;
	}

	public TipoOcorrenciaObservadaFormCOM getTipo() {
		return tipo;
	}

	public void setTipo(TipoOcorrenciaObservadaFormCOM tipo) {
		this.tipo = tipo;
	}

	public Long getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Long quantidade) {
		this.quantidade = quantidade;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OcorrenciaObservadaFormCOM other = (OcorrenciaObservadaFormCOM) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}