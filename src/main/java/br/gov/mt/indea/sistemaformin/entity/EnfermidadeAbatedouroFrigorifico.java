package br.gov.mt.indea.sistemaformin.entity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

@Entity
@Table(name = "enfermidade_abat_frig")
@Audited
public class EnfermidadeAbatedouroFrigorifico extends BaseEntity<Long>{

	private static final long serialVersionUID = 6945160424281951286L;
	
	@Id
	@GeneratedValue(generator = "enfermidade_abat_frig_seq", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "enfermidade_abat_frig_seq", sequenceName = "enfermidade_abat_frig_seq", allocationSize = 1)
	private Long id;
	
	@Column(name = "data_cadastro")
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar dataCadastro;
	
	@Column(name = "data_abate")
	@Temporal(TemporalType.DATE)
	private Calendar dataAbate;
	
	@OneToMany(mappedBy="enfermidadeAbatedouroFrigorifico", orphanRemoval=true, cascade={CascadeType.ALL}, fetch=FetchType.LAZY)
	private List<LoteEnfermidadeAbatedouroFrigorifico> listaLoteEnfermidadeAbatedouroFrigorifico = new ArrayList<LoteEnfermidadeAbatedouroFrigorifico>();
	
	@OneToOne(fetch=FetchType.EAGER)
	private ServicoDeInspecao servicoDeInspecao;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Calendar getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Calendar dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Calendar getDataAbate() {
		return dataAbate;
	}

	public void setDataAbate(Calendar dataAbate) {
		this.dataAbate = dataAbate;
	}

	public List<LoteEnfermidadeAbatedouroFrigorifico> getListaLoteEnfermidadeAbatedouroFrigorifico() {
		return listaLoteEnfermidadeAbatedouroFrigorifico;
	}

	public void setListaLoteEnfermidadeAbatedouroFrigorifico(
			List<LoteEnfermidadeAbatedouroFrigorifico> listaLoteEnfermidadeAbatedouroFrigorifico) {
		this.listaLoteEnfermidadeAbatedouroFrigorifico = listaLoteEnfermidadeAbatedouroFrigorifico;
	}

	public ServicoDeInspecao getServicoDeInspecao() {
		return servicoDeInspecao;
	}

	public void setServicoDeInspecao(ServicoDeInspecao servicoDeInspecao) {
		this.servicoDeInspecao = servicoDeInspecao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EnfermidadeAbatedouroFrigorifico other = (EnfermidadeAbatedouroFrigorifico) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
