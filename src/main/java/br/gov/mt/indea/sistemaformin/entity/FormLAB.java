package br.gov.mt.indea.sistemaformin.entity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

import br.gov.mt.indea.sistemaformin.enums.Dominio.CategoriaAves;
import br.gov.mt.indea.sistemaformin.enums.Dominio.SimNao;

@Audited
@Entity
@Table(name="form_lab")
public class FormLAB extends BaseEntity<Long>{

	private static final long serialVersionUID = 774070109950744456L;
	
	@Id
	@SequenceGenerator(name="form_lab_seq", sequenceName="form_lab_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="form_lab_seq")
	private Long id;

	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_usuario")
	private Usuario usuario;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_cadastro")
	private Calendar dataCadastro;
	
	@ManyToOne
	@JoinColumn(name="id_formin")
	private FormIN formIN;
	
	@ManyToOne
	@JoinColumn(name="id_formcom")
	private FormCOM formCOM;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_colheita")
	private Calendar dataColheita;
	
	@Column(name="sindrome_investigada", length=255)
	private String sindromeInvestigada;
	
	@Enumerated(EnumType.STRING)
	@Column(name="teste_padrao_solicitado")
	private SimNao testePadraoSolicitado;
	
	@Column(name="outro_teste_solicitado", length=255)
	private String outroTesteSolicitado;
	
	@Enumerated(EnumType.STRING)
	@Column(name="categoria_aves")
	private CategoriaAves categoriaAves;
	
	@OneToMany(mappedBy="formLAB1", cascade={CascadeType.ALL}, orphanRemoval=true)
	private List<AmostraSoroSanguineo> amostrasSoroSanguineo = new ArrayList<AmostraSoroSanguineo>();
	
	@OneToMany(mappedBy="formLAB2", cascade={CascadeType.ALL}, orphanRemoval=true)
	private List<OutrasAmostras> outrasAmostras = new ArrayList<OutrasAmostras>();
	
	@OneToOne(cascade={CascadeType.ALL})
	@JoinColumn(name="id_veterinario")
	private br.gov.mt.indea.sistemaformin.webservice.entity.Veterinario veterinario;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_de_envio_das_amostras")
	private Calendar dataDeEnvioDasAmostras;
	
	public List<Long> getListaIDsCadastrados_AmostraSoroSanguineo(){
		List<Long> lista = new ArrayList<Long>();
		for (AmostraSoroSanguineo amostraSoroSanguineo : amostrasSoroSanguineo) {
			lista.add(amostraSoroSanguineo.getIdAmostra());
		}
		
		return lista;
	}
	
	public List<Long> getListaIDsCadastrados_OutrasAmostras(){
		List<Long> lista = new ArrayList<Long>();
		for (OutrasAmostras outraAmostra : outrasAmostras) {
			lista.add(outraAmostra.getIdAmostra());
		}
		
		return lista;
	}
	
	public List<Long> getListaIDsCadastrados(){
		List<Long> lista = new ArrayList<Long>();
		lista.addAll(getListaIDsCadastrados_AmostraSoroSanguineo());
		lista.addAll(getListaIDsCadastrados_OutrasAmostras());
		
		return lista;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Calendar getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Calendar dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public FormIN getFormIN() {
		return formIN;
	}

	public void setFormIN(FormIN formIN) {
		this.formIN = formIN;
	}

	public FormCOM getFormCOM() {
		return formCOM;
	}

	public void setFormCOM(FormCOM formCOM) {
		this.formCOM = formCOM;
	}

	public Calendar getDataColheita() {
		return dataColheita;
	}

	public void setDataColheita(Calendar dataColheita) {
		this.dataColheita = dataColheita;
	}

	public String getSindromeInvestigada() {
		return sindromeInvestigada;
	}

	public void setSindromeInvestigada(String sindromeInvestigada) {
		this.sindromeInvestigada = sindromeInvestigada;
	}

	public SimNao getTestePadraoSolicitado() {
		return testePadraoSolicitado;
	}

	public void setTestePadraoSolicitado(SimNao testePadraoSolicitado) {
		this.testePadraoSolicitado = testePadraoSolicitado;
	}

	public String getOutroTesteSolicitado() {
		return outroTesteSolicitado;
	}

	public void setOutroTesteSolicitado(String outroTesteSolicitado) {
		this.outroTesteSolicitado = outroTesteSolicitado;
	}
	
	public List<AmostraSoroSanguineo> getAmostrasSoroSanguineo() {
		return amostrasSoroSanguineo;
	}

	public void setAmostrasSoroSanguineo(
			List<AmostraSoroSanguineo> amostrasSoroSanguineo) {
		this.amostrasSoroSanguineo = amostrasSoroSanguineo;
	}

	public List<OutrasAmostras> getOutrasAmostras() {
		return outrasAmostras;
	}

	public void setOutrasAmostras(List<OutrasAmostras> outrasAmostras) {
		this.outrasAmostras = outrasAmostras;
	}

	public CategoriaAves getCategoriaAves() {
		return categoriaAves;
	}

	public void setCategoriaAves(CategoriaAves categoriaAves) {
		this.categoriaAves = categoriaAves;
	}

	public String getTesteSolicitado(){
		if (this.testePadraoSolicitado == null)
			return "Nenhum";
		else{
			if (this.testePadraoSolicitado.equals(SimNao.SIM))
				return "Teste padr�o";
			else
				return this.outroTesteSolicitado;
		}
	}

	public br.gov.mt.indea.sistemaformin.webservice.entity.Veterinario getVeterinario() {
		return veterinario;
	}

	public void setVeterinario(
			br.gov.mt.indea.sistemaformin.webservice.entity.Veterinario veterinario) {
		this.veterinario = veterinario;
	}

	public Calendar getDataDeEnvioDasAmostras() {
		return dataDeEnvioDasAmostras;
	}

	public void setDataDeEnvioDasAmostras(Calendar dataDeEnvioDasAmostras) {
		this.dataDeEnvioDasAmostras = dataDeEnvioDasAmostras;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FormLAB other = (FormLAB) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}