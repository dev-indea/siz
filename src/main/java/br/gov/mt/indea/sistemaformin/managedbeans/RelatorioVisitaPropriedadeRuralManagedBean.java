package br.gov.mt.indea.sistemaformin.managedbeans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang.time.DateUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;

import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLBeanName;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;

import br.gov.mt.indea.sistemaformin.entity.view.RelatorioVisitaPropriedadeRuralGeral;
import br.gov.mt.indea.sistemaformin.service.VisitaPropriedadeRuralService;
import br.gov.mt.indea.sistemaformin.util.FacesMessageUtil;

@Named
@ViewScoped
@URLBeanName("relatorioVisitaPropriedadeRuralManagedBean")
@URLMappings(mappings = {
		@URLMapping(id = "relatorioVisitaPropriedadeRural", pattern = "/relatorio/visitaPropriedadeRural", viewId = "/pages/relatorios/visitapropriedaderural.jsf")})
public class RelatorioVisitaPropriedadeRuralManagedBean implements Serializable {

	private static final long serialVersionUID = 1760023349204758559L;

	@Inject
	private VisitaPropriedadeRuralService visitaPropriedadeRuralService;
	
	private List<RelatorioVisitaPropriedadeRuralGeral> listaVisitaPropriedadeRural;
	
	private List<RelatorioVisitaPropriedadeRuralGeral> listaVisitaPropriedadeRuralFiltered;
	
	private Date dataInicial;
	
	private Date dataFinal;
	
	@PostConstruct
	private void init(){
		dataInicial = new Date();
		dataFinal = new Date();
		
		Calendar c = Calendar.getInstance();
		c.set(Calendar.DAY_OF_MONTH, 1);
		
		dataInicial.setTime(c.getTimeInMillis());
		
		c.set(Calendar.DAY_OF_MONTH, 30);
		
		dataFinal.setTime(c.getTimeInMillis());
	}
	
	@URLAction(mappingId = "relatorioVisitaPropriedadeRural", onPostback = false)
	public void novo(){
		
	}
	
	public void buscar(){
		long diff = dataFinal.getTime() - dataInicial.getTime();
		
	    if (TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS) > 366){
	    	FacesMessageUtil.addWarnContextFacesMessage("O per�odo de busca n�o deve ser superior a 1 ano", "");
	    	return;
	    }
	    
		listaVisitaPropriedadeRural = (ArrayList<RelatorioVisitaPropriedadeRuralGeral>) visitaPropriedadeRuralService.relatorioTeste(this.dataInicial, this.dataFinal);
	}
	
	public void postProcessor(Object doc){
		HSSFWorkbook book = (HSSFWorkbook)doc;
	    HSSFSheet sheet = book.getSheetAt(0); 

	    HSSFRow header = sheet.getRow(0);
	    
	    int colCount = header.getPhysicalNumberOfCells();
	    int rowCount = sheet.getPhysicalNumberOfRows();

	    HSSFCellStyle intStyle = book.createCellStyle();
		intStyle.setDataFormat((short)1);

		HSSFCellStyle decStyle = book.createCellStyle();
		decStyle.setDataFormat((short)2);
		
		HSSFCellStyle headerStyle = book.createCellStyle();
		headerStyle.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
		headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		HSSFFont font = book.createFont();
        font.setBold(true);
        headerStyle.setFont(font);

        //Transforma o t�tulo em negrito
        for(int colNum = 0; colNum < header.getLastCellNum(); colNum++){   
	    	HSSFCell cell = header.getCell(colNum);
	    	cell.setCellStyle(headerStyle);
	    	book.getSheetAt(0).autoSizeColumn(colNum);
	    	
	    	if (colNum == 4)
	    		cell.setCellValue("Data da visita");
	    	if (colNum == 5)
	    		cell.setCellValue("Data de cadastro");
        }
	    
		for(int rowInd = 1; rowInd < rowCount; rowInd++) {
		    HSSFRow row = sheet.getRow(rowInd);
		    for(int cellInd = 1; cellInd < colCount; cellInd++) {
		        HSSFCell cell = row.getCell(cellInd);
		        
		        //Latitude (grau)
		        if (cell.getColumnIndex() == 9){
		        	this.setCellIntegerStyle(cell, intStyle);
		        }
		        
		        if (cell.getColumnIndex() == 10){
		        	this.setCellIntegerStyle(cell, intStyle);
		        }
		        
		        if (cell.getColumnIndex() == 11){
		        	this.setCellDoubleStyle(cell, decStyle);
		        }
		        
		        if (cell.getColumnIndex() == 12){
		        	this.setCellIntegerStyle(cell, intStyle);
		        }
		        
		        if (cell.getColumnIndex() == 13){
		        	this.setCellIntegerStyle(cell, intStyle);
		        }
		        
		        if (cell.getColumnIndex() == 14){
		        	this.setCellDoubleStyle(cell, decStyle);
		        }
		    }
		}
	}
	
	private void setCellIntegerStyle(HSSFCell cell, HSSFCellStyle style){
		String strVal = cell.getStringCellValue();
		cell.setCellStyle(style);
        
		strVal = strVal.replaceAll("[^\\d.]", "");
		if (strVal != null && !strVal.equals("")){
	    	int intVal = Integer.valueOf(strVal);
	    	cell.setCellValue(intVal);
		}
	}
	
	private void setCellDoubleStyle(HSSFCell cell, HSSFCellStyle style){
		String strVal = cell.getStringCellValue();
    	cell.setCellStyle(style);
		
    	if (strVal != null && !strVal.equals("")){
    	    double dblVal = Double.valueOf(strVal);
    	    cell.setCellValue(dblVal);
    	}
	}
	
	public List<String> getListaURSFilter(){
		if (this.listaVisitaPropriedadeRural == null)
			return null;
		else{
			List<String> lista = new ArrayList<>();
			
			for (RelatorioVisitaPropriedadeRuralGeral relatorio : listaVisitaPropriedadeRural) {
				if (!lista.contains(relatorio.getUrs()))
					lista.add((String) relatorio.getUrs());
			}
			
			List<String> sortedLista = new ArrayList<>();
			sortedLista = lista;
			
			try{
				Collections.sort(lista);
				sortedLista = lista;
			} catch (Exception e) {
			}
			
			return sortedLista;
		}
	}
	
	public List<String> getListaMunicipioFilter(){
		if (this.listaVisitaPropriedadeRural == null)
			return null;
		else{
			List<String> lista = new ArrayList<>();
			
			for (RelatorioVisitaPropriedadeRuralGeral relatorio : listaVisitaPropriedadeRural) {
				if (!lista.contains(relatorio.getMunicipio()))
					lista.add((String) relatorio.getMunicipio());
			}
			
			List<String> sortedLista = new ArrayList<>();
			sortedLista = lista;
			
			try{
				Collections.sort(lista);
				sortedLista = lista;
			} catch (Exception e) {
			}
			
			return sortedLista;
		}
	}
	
	public List<String> getListaTipoEstabelecimentoFilter(){
		if (this.listaVisitaPropriedadeRural == null)
			return null;
		else{
			List<String> lista = new ArrayList<>();
			
			for (RelatorioVisitaPropriedadeRuralGeral relatorio : listaVisitaPropriedadeRural) {
				if (!lista.contains(relatorio.getTipo_estabelecimento()))
					lista.add((String) relatorio.getTipo_estabelecimento());
			}
			
			List<String> sortedLista = new ArrayList<>();
			sortedLista = lista;
			
			try{
				Collections.sort(lista);
				sortedLista = lista;
			} catch (Exception e) {
			}
			
			return sortedLista;
		}
	}
	
	public List<String> getListaChavePrincipalFilter(){
		if (this.listaVisitaPropriedadeRural == null)
			return null;
		else{
			List<String> lista = new ArrayList<>();
			
			for (RelatorioVisitaPropriedadeRuralGeral relatorio : listaVisitaPropriedadeRural) {
				if (!lista.contains(relatorio.getChave_principal()))
					lista.add((String) relatorio.getChave_principal());
			}
			
			List<String> sortedLista = new ArrayList<>();
			sortedLista = lista;
			
			try{
				Collections.sort(lista);
				sortedLista = lista;
			} catch (Exception e) {
			}
			
			return sortedLista;
		}
	}
	
	public List<String> getListaServidoresQueRealizaramAVisitaFilter(){
		if (this.listaVisitaPropriedadeRural == null)
			return null;
		else{
			List<String> lista = new ArrayList<>();
			
			for (RelatorioVisitaPropriedadeRuralGeral relatorio : listaVisitaPropriedadeRural) {
				String servidores_que_realizaram_visita = relatorio.getServidores_que_realizaram_visita();
				
				if (servidores_que_realizaram_visita != null){
				
					String[] tipos = servidores_que_realizaram_visita.split(",");
					
					for (String tipo : tipos) {
						if (!lista.contains(tipo))
							lista.add(tipo);
					}
				}
				
			}
			
			List<String> sortedLista = new ArrayList<>();
			sortedLista = lista;
			
			try{
				Collections.sort(lista);
				sortedLista = lista;
			} catch (Exception e) {
			}
			
			return sortedLista;
		}
	}
	
	public boolean filterByDate(Object value, Object filter, Locale locale){
		if( filter == null ) {
            return true;
        }

        if( value == null ) {
            return false;
        }
        
        if (value instanceof String){
        	String[] date = ((String) value).split("/");

        	Calendar c = Calendar.getInstance();
        	c.set(Calendar.DAY_OF_MONTH, Integer.parseInt(date[0]));
        	c.set(Calendar.MONTH, Integer.parseInt(date[1]) - 1);
        	c.set(Calendar.YEAR, Integer.parseInt(date[2]));
        	
        	return DateUtils.truncatedEquals((Date) filter, c.getTime(), Calendar.DATE);
        } else {
        	return DateUtils.truncatedEquals((Date) filter, (Date) value, Calendar.DATE);
        }
	}
	
	public boolean filterInsideList(Object value, Object filter, Locale locale){
		String[] list = (String[]) filter;
		
		
		if( list.length < 1 ) {
            return true;
        }
		
		if( value == null ) {
            return false;
        }
		
		String value_ = (String)value;
		
		boolean hasAll = true;
		
		for (String motivo : list) {
			if (!value_.contains(motivo))
				hasAll = false;
		}

        return hasAll;	
	}

	public List<RelatorioVisitaPropriedadeRuralGeral> getListaVisitaPropriedadeRural() {
		return listaVisitaPropriedadeRural;
	}

	public void setListaVisitaPropriedadeRural(List<RelatorioVisitaPropriedadeRuralGeral> listaVisitaPropriedadeRural) {
		this.listaVisitaPropriedadeRural = listaVisitaPropriedadeRural;
	}

	public Date getDataInicial() {
		return dataInicial;
	}

	public void setDataInicial(Date dataInicial) {
		this.dataInicial = dataInicial;
	}

	public Date getDataFinal() {
		return dataFinal;
	}

	public void setDataFinal(Date dataFinal) {
		this.dataFinal = dataFinal;
	}

	public List<RelatorioVisitaPropriedadeRuralGeral> getListaVisitaPropriedadeRuralFiltered() {
		return listaVisitaPropriedadeRuralFiltered;
	}

	public void setListaVisitaPropriedadeRuralFiltered(List<RelatorioVisitaPropriedadeRuralGeral> listaVisitaPropriedadeRuralFiltered) {
		this.listaVisitaPropriedadeRuralFiltered = listaVisitaPropriedadeRuralFiltered;
	}
	
}
