package br.gov.mt.indea.sistemaformin.managedbeans;

import java.io.IOException;
import java.io.Serializable;
import java.util.Calendar;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLBeanName;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;

import br.gov.mt.indea.sistemaformin.entity.TipoChavePrincipalTermoFiscalizacao;
import br.gov.mt.indea.sistemaformin.entity.TipoChaveSecundariaTermoFiscalizacao;
import br.gov.mt.indea.sistemaformin.entity.dto.TipoChaveSecundariaTermoFiscalizacaoDTO;
import br.gov.mt.indea.sistemaformin.enums.Dominio.AtivoInativo;
import br.gov.mt.indea.sistemaformin.service.LazyObjectDataModel;
import br.gov.mt.indea.sistemaformin.service.TipoChavePrincipalTermoFiscalizacaoService;
import br.gov.mt.indea.sistemaformin.service.TipoChaveSecundariaTermoFiscalizacaoService;
import br.gov.mt.indea.sistemaformin.util.FacesMessageUtil;

@Named
@ViewScoped
@URLBeanName("tipoChaveSecundariaTermoFiscalizacaoManagedBean")
@URLMappings(mappings = {
		@URLMapping(id = "pesquisarTipoChaveSecundariaTermoFiscalizacao", pattern = "/tipoChaveSecundariaTermoFiscalizacao/pesquisar", viewId = "/pages/tipoChaveSecundariaTermoFiscalizacao/lista.jsf"),
		@URLMapping(id = "incluirTipoChaveSecundariaTermoFiscalizacao", pattern = "/tipoChaveSecundariaTermoFiscalizacao/incluir", viewId = "/pages/tipoChaveSecundariaTermoFiscalizacao/novo.jsf"),
		@URLMapping(id = "alterarTipoChaveSecundariaTermoFiscalizacao", pattern = "/tipoChaveSecundariaTermoFiscalizacao/alterar/#{tipoChaveSecundariaTermoFiscalizacaoManagedBean.id}", viewId = "/pages/tipoChaveSecundariaTermoFiscalizacao/novo.jsf")})
public class TipoChaveSecundariaTermoFiscalizacaoManagedBean implements Serializable {
	
	private static final long serialVersionUID = -7409333626528791621L;
	
	private Long id;

	@Inject
	private TipoChavePrincipalTermoFiscalizacaoService tipoChavePrincipalTermoFiscalizacaoService;
	
	@Inject
	private TipoChaveSecundariaTermoFiscalizacaoService tipoChaveSecundariaTermoFiscalizacaoService;
	
	@Inject
	private TipoChaveSecundariaTermoFiscalizacaoDTO tipoChaveSecundariaTermoFiscalizacaoDTO;
	
	@Inject
	private TipoChaveSecundariaTermoFiscalizacao tipoChaveSecundariaTermoFiscalizacao;
	
	private LazyObjectDataModel<TipoChaveSecundariaTermoFiscalizacao> listaTipoChaveSecundariaTermoFiscalizacao;
	
	@PostConstruct
	public void init(){
		this.listaTipoChaveSecundariaTermoFiscalizacao = new LazyObjectDataModel<TipoChaveSecundariaTermoFiscalizacao>(this.tipoChaveSecundariaTermoFiscalizacaoService, tipoChaveSecundariaTermoFiscalizacaoDTO);
	}
	
	public void limpar(){
	}
	
	public List<TipoChavePrincipalTermoFiscalizacao> getListaTipoChavePrincipalTermoFiscalizacao(){
		return this.tipoChavePrincipalTermoFiscalizacaoService.findAll();
	}
	
	@URLAction(mappingId = "incluirTipoChaveSecundariaTermoFiscalizacao", onPostback = false)
	public void novo(){
		limpar();
	}
	
	@URLAction(mappingId = "alterarTipoChaveSecundariaTermoFiscalizacao", onPostback = false)
	public void editar(){
		this.tipoChaveSecundariaTermoFiscalizacao = tipoChaveSecundariaTermoFiscalizacaoService.findByIdFetchAll(this.getId());
	}
	
	public String adicionar() throws IOException{
		
		if (tipoChaveSecundariaTermoFiscalizacao.getId() != null){
			this.tipoChaveSecundariaTermoFiscalizacaoService.saveOrUpdate(tipoChaveSecundariaTermoFiscalizacao);

			FacesMessageUtil.addInfoContextFacesMessage("Tipo chave secundaria atualizado", "");
		}else{
			this.tipoChaveSecundariaTermoFiscalizacao.setDataCadastro(Calendar.getInstance());
			this.tipoChaveSecundariaTermoFiscalizacao.setStatus(AtivoInativo.ATIVO);
			this.tipoChaveSecundariaTermoFiscalizacaoService.saveOrUpdate(tipoChaveSecundariaTermoFiscalizacao);

			FacesMessageUtil.addInfoContextFacesMessage("Tipo chave secundaria adicionado", "");
		}
		
		this.tipoChaveSecundariaTermoFiscalizacao = new TipoChaveSecundariaTermoFiscalizacao();
		
		return "pretty:pesquisarTipoChaveSecundariaTermoFiscalizacao";
	}
	
	public void remover(TipoChaveSecundariaTermoFiscalizacao tipoChaveSecundariaTermoFiscalizacao){
		this.tipoChaveSecundariaTermoFiscalizacaoService.delete(tipoChaveSecundariaTermoFiscalizacao);
		FacesMessageUtil.addInfoContextFacesMessage("Tipo chave secundaria exclu�do com sucesso", "");
	}
	
	public void ativar(TipoChaveSecundariaTermoFiscalizacao tipoChaveSecundariaTermoFiscalizacao){
		tipoChaveSecundariaTermoFiscalizacao.setStatus(AtivoInativo.ATIVO);
		
		this.tipoChaveSecundariaTermoFiscalizacaoService.saveOrUpdate(tipoChaveSecundariaTermoFiscalizacao);
		
		FacesMessageUtil.addInfoContextFacesMessage("Tipo chave secundaria ativado", "");
	}
	
	public void desativar(TipoChaveSecundariaTermoFiscalizacao tipoChaveSecundariaTermoFiscalizacao){
		tipoChaveSecundariaTermoFiscalizacao.setStatus(AtivoInativo.INATIVO);
		
		this.tipoChaveSecundariaTermoFiscalizacaoService.saveOrUpdate(tipoChaveSecundariaTermoFiscalizacao);
		
		FacesMessageUtil.addInfoContextFacesMessage("Tipo chave secundaria desativado", "");
	}
	
	public boolean isAtivado(TipoChaveSecundariaTermoFiscalizacao tipoChaveSecundariaTermoFiscalizacao){
		return tipoChaveSecundariaTermoFiscalizacao.getStatus().equals(AtivoInativo.ATIVO);
	}
	
	public TipoChaveSecundariaTermoFiscalizacao getTipoChaveSecundariaTermoFiscalizacao() {
		return tipoChaveSecundariaTermoFiscalizacao;
	}

	public void setTipoChaveSecundariaTermoFiscalizacao(TipoChaveSecundariaTermoFiscalizacao tipoChaveSecundariaTermoFiscalizacao) {
		this.tipoChaveSecundariaTermoFiscalizacao = tipoChaveSecundariaTermoFiscalizacao;
	}

	public void setListaTipoChaveSecundariaTermoFiscalizacao(
			LazyObjectDataModel<TipoChaveSecundariaTermoFiscalizacao> listaTipoChaveSecundariaTermoFiscalizacao) {
		this.listaTipoChaveSecundariaTermoFiscalizacao = listaTipoChaveSecundariaTermoFiscalizacao;
	}

	public LazyObjectDataModel<TipoChaveSecundariaTermoFiscalizacao> getListaTipoChaveSecundariaTermoFiscalizacao() {
		return listaTipoChaveSecundariaTermoFiscalizacao;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TipoChaveSecundariaTermoFiscalizacaoDTO getTipoChaveSecundariaTermoFiscalizacaoDTO() {
		return tipoChaveSecundariaTermoFiscalizacaoDTO;
	}

	public void setTipoChaveSecundariaTermoFiscalizacaoDTO(
			TipoChaveSecundariaTermoFiscalizacaoDTO tipoChaveSecundariaTermoFiscalizacaoDTO) {
		this.tipoChaveSecundariaTermoFiscalizacaoDTO = tipoChaveSecundariaTermoFiscalizacaoDTO;
	}

}
