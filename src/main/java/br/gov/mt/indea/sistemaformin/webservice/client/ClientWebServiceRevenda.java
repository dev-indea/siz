package br.gov.mt.indea.sistemaformin.webservice.client;

import java.io.Serializable;
import java.net.URISyntaxException;

import javax.inject.Inject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import br.gov.mt.indea.sistemaformin.exception.ApplicationException;
import br.gov.mt.indea.sistemaformin.webservice.convert.RevendaConverter;
import br.gov.mt.indea.sistemaformin.webservice.model.Revenda;

public class ClientWebServiceRevenda implements Serializable{
	
	private static final long serialVersionUID = -9080230353371266286L;

	protected String URL_WS = "https://sistemas.indea.mt.gov.br/FronteiraWeb/ws/revenda/";
	
//	@Inject
//	@Category("WEBSERVICE::GTA")
//	private CustomLogger log;
	
	@Inject
	private RevendaConverter revendaConverter;
	
	public br.gov.mt.indea.sistemaformin.webservice.entity.Revenda getRevendaByCNPJ(String cnpj) throws ApplicationException{
		String[] resposta = null;
		try {
			System.out.println(URL_WS + "cnpj/" + cnpj.replaceAll("[^\\d]", ""));
			resposta = new WebServiceCliente().get(URL_WS + "cnpj/" + cnpj.replaceAll("[^\\d]", ""));
		} catch (URISyntaxException e) {
			//log.excecao(e.getMessage(), e);
			throw new ApplicationException("Erro ao buscar o Gta");
		}
		
		if (resposta == null || resposta[0] == null){
			return null;
		}
		
		if (resposta[0].equals("200")) {
			GsonBuilder gsonBuilder = new GsonBuilder().setDateFormat("dd/MM/yyyy");
			Gson gson = gsonBuilder.create();

			br.gov.mt.indea.sistemaformin.webservice.entity.Revenda revenda = revendaConverter.fromModelToEntity(gson.fromJson(resposta[1], Revenda.class));
			return revenda;
		} else if (resposta[0].equals("404")) {
			//log.excecao(resposta[0] + ". " + resposta[1], null);
			throw new ApplicationException("Webservice com problemas. Contate o administrador do sistema");
		} else if (resposta[0].equals("500")) {
			//log.excecao(resposta[0] + ". " + resposta[1], null);
			throw new ApplicationException("Webservice com problemas. Contate o administrador do sistema");
		} else if (resposta[0].equals("400")) {
			//log.excecao(resposta[0] + ". " + resposta[1], null);
			throw new ApplicationException("Webservice com problemas. Contate o administrador do sistema");
		} else{
			//log.excecao(resposta[0] + ". " + resposta[1], null);
			throw new ApplicationException("Webservice inativo. Tente novamente em alguns minutos. Erro: " + resposta[0]);
		} 
		
	}
	
	public br.gov.mt.indea.sistemaformin.webservice.entity.Revenda getRevendaByNome(String nome) throws ApplicationException{
		String[] resposta = null;
		try {
			System.out.println(URL_WS + "nome/" + nome);
			resposta = new WebServiceCliente().get(URL_WS + "nome/" + nome);
		} catch (URISyntaxException e) {
			//log.excecao(e.getMessage(), e);
			throw new ApplicationException("Erro ao buscar o Gta");
		}
		
		if (resposta == null || resposta[0] == null){
			return null;
		}
		
		if (resposta[0].equals("200")) {
			GsonBuilder gsonBuilder = new GsonBuilder().setDateFormat("dd/MM/yyyy");
			Gson gson = gsonBuilder.create();

			br.gov.mt.indea.sistemaformin.webservice.entity.Revenda revenda = revendaConverter.fromModelToEntity(gson.fromJson(resposta[1], Revenda.class));
			return revenda;
		} else if (resposta[0].equals("404")) {
			//log.excecao(resposta[0] + ". " + resposta[1], null);
			throw new ApplicationException("Webservice com problemas. Contate o administrador do sistema");
		} else if (resposta[0].equals("500")) {
			//log.excecao(resposta[0] + ". " + resposta[1], null);
			throw new ApplicationException("Webservice com problemas. Contate o administrador do sistema");
		} else if (resposta[0].equals("400")) {
			//log.excecao(resposta[0] + ". " + resposta[1], null);
			throw new ApplicationException("Webservice com problemas. Contate o administrador do sistema");
		} else{
			//log.excecao(resposta[0] + ". " + resposta[1], null);
			throw new ApplicationException("Webservice inativo. Tente novamente em alguns minutos. Erro: " + resposta[0]);
		} 
		
	}

}
