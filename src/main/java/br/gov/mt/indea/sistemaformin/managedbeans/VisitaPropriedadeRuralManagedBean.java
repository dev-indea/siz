package br.gov.mt.indea.sistemaformin.managedbeans;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.Past;

import org.primefaces.PrimeFaces;

import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLBeanName;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;

import br.gov.mt.indea.sistemaformin.entity.ChavePrincipalVisitaPropriedadeRural;
import br.gov.mt.indea.sistemaformin.entity.ChaveSecundariaVisitaPropriedadeRural;
import br.gov.mt.indea.sistemaformin.entity.DetalheComunicacaoAIE;
import br.gov.mt.indea.sistemaformin.entity.DetalheFormNotifica;
import br.gov.mt.indea.sistemaformin.entity.LoteEnfermidadeAbatedouroFrigorifico;
import br.gov.mt.indea.sistemaformin.entity.Municipio;
import br.gov.mt.indea.sistemaformin.entity.RegistroEntradaLASA;
import br.gov.mt.indea.sistemaformin.entity.TipoChavePrincipalVisitaPropriedadeRural;
import br.gov.mt.indea.sistemaformin.entity.TipoChaveSecundariaVisitaPropriedadeRural;
import br.gov.mt.indea.sistemaformin.entity.VisitaPropriedadeRural;
import br.gov.mt.indea.sistemaformin.entity.dto.VisitaPropriedadeRuralDTO;
import br.gov.mt.indea.sistemaformin.enums.Dominio.AtivoInativo;
import br.gov.mt.indea.sistemaformin.enums.Dominio.SimNao;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoEstabelecimento;
import br.gov.mt.indea.sistemaformin.exception.ApplicationException;
import br.gov.mt.indea.sistemaformin.exception.ApplicationRuntimeException;
import br.gov.mt.indea.sistemaformin.service.EnfermidadeAbatedouroFrigorificoService;
import br.gov.mt.indea.sistemaformin.service.FormNotificaService;
import br.gov.mt.indea.sistemaformin.service.LazyObjectDataModel;
import br.gov.mt.indea.sistemaformin.service.MunicipioService;
import br.gov.mt.indea.sistemaformin.service.PropriedadeService;
import br.gov.mt.indea.sistemaformin.service.TipoChavePrincipalVisitaPropriedadeRuralService;
import br.gov.mt.indea.sistemaformin.service.TipoChaveSecundariaVisitaPropriedadeRuralService;
import br.gov.mt.indea.sistemaformin.service.VisitaPropriedadeRuralService;
import br.gov.mt.indea.sistemaformin.util.FacesMessageUtil;
import br.gov.mt.indea.sistemaformin.webservice.client.ClientWebServiceAbatedouro;
import br.gov.mt.indea.sistemaformin.webservice.client.ClientWebServiceRecinto;
import br.gov.mt.indea.sistemaformin.webservice.client.ClientWebServiceServidor;
import br.gov.mt.indea.sistemaformin.webservice.entity.Abatedouro;
import br.gov.mt.indea.sistemaformin.webservice.entity.Produtor;
import br.gov.mt.indea.sistemaformin.webservice.entity.Propriedade;
import br.gov.mt.indea.sistemaformin.webservice.entity.Recinto;
import br.gov.mt.indea.sistemaformin.webservice.entity.Servidor;

@Named("visitaPropriedadeRuralManagedBean")
@ViewScoped
@URLBeanName("visitaPropriedadeRuralManagedBean")
@URLMappings(mappings = {
		@URLMapping(id = "pesquisarVisitaPropriedadeRural", pattern = "/visitaPropriedadeRural/pesquisar", viewId = "/pages/visitaPropriedadeRural/lista.jsf"),
		@URLMapping(id = "incluirVisitaPropriedadeRural", pattern = "/visitaPropriedadeRural/incluir", viewId = "/pages/visitaPropriedadeRural/novo.jsf"),
		@URLMapping(id = "alterarVisitaPropriedadeRural", pattern = "/visitaPropriedadeRural/alterar/#{visitaPropriedadeRuralManagedBean.id}", viewId = "/pages/visitaPropriedadeRural/novo.jsf")})
public class VisitaPropriedadeRuralManagedBean implements Serializable {

	private static final long serialVersionUID = 1760023349204758559L;
	
	private Long id;
	
	private DetalheComunicacaoAIE detalheComunicacaoAIE;
	
	private RegistroEntradaLASA registroEntradaLASA;
	
	private DetalheFormNotifica detalheFormNotifica;
	
	private LoteEnfermidadeAbatedouroFrigorifico loteEnfermidadeAbatedouroFrigorifico;
	
	private int activeIndexTelaBuscaPropriedade = 0;
	
	private boolean telaBuscaJaFoiAberta = false;

	@Inject
	private MunicipioService municipioService;
	
	@Inject
	private VisitaPropriedadeRuralService visitaPropriedadeRuralService;
	
	@Inject
	private VisitaPropriedadeRuralDTO visitaPropriedadeRuralDTO;
	
	@Inject
	private TipoChavePrincipalVisitaPropriedadeRuralService tipoChavePrincipalVisitaPropriedadeRuralService;
	
	@Inject
	private TipoChaveSecundariaVisitaPropriedadeRuralService tipoChaveSecundariaVisitaPropriedadeRuralService;
	
	@Inject
	private FormNotificaService formNotificaService;

	@Inject
	private EnfermidadeAbatedouroFrigorificoService enfermidadeAbatedouroFrigorificoService;
	
	@Inject
	private VisitaPropriedadeRural visitaPropriedadeRural;
	
	@Past(message="Data precisa ser menor ou igual a data atual")
	private Date dataDaVisita;
	
	private boolean forceUpdatePropriedade = false;
	
	@Inject
	private PropriedadeService propriedadeService;
	
	@Inject
	private ClientWebServiceServidor clientWebServiceServidor;
	
	@Inject
	private ClientWebServiceRecinto clientWebServiceRecinto;
	
	@Inject
	private ClientWebServiceAbatedouro clientWebServiceAbatedouro;
	
	private Long idPropriedade;
	
	private String nomePropriedade;

	private List<Propriedade> listaPropriedades;
	
	private String matriculaServidor;
	
	private String cpfServidor;
	
	private List<Servidor> listaServidores;
	
	private Long idRecinto;
	
	private List<Recinto> listaRecintos;
	
	private ChavePrincipalVisitaPropriedadeRural chavePrincipalVisitaPropriedadeRural;
	
	private List<TipoChaveSecundariaVisitaPropriedadeRural> tipoChaveSecundariaVisitaPropriedadeRural = new ArrayList<TipoChaveSecundariaVisitaPropriedadeRural>();
	
	private boolean editandoChavePrincipalVisitaPropriedadeRural = false;
	
	private boolean visualizandoChavePrincipalVisitaPropriedadeRural = false;
	
	private TipoEstabelecimento tipoEstabelecimento = TipoEstabelecimento.PROPRIEDADE;
	
	private Servidor servidor;
	
	private boolean editandoServidor = false;
	
	private boolean visualizandoServidor = false;

	private LazyObjectDataModel<VisitaPropriedadeRural> listaVisitaPropriedadeRural;
	
	private String cnpjAbatedouro;
	
	private List<Abatedouro> listaAbatedouros;
	
	@PostConstruct
	private void init(){
		FacesMessageUtil.addInfoContextFacesMessage("Para visitas realizadas a partir do dia 15/05/2023, deve ser utilizado a nova vers�o do SIZ dispon�vel em: ", "https://siz-v3.indea.mt.gov.br/");
		PrimeFaces current = PrimeFaces.current();
		current.executeScript("PF('modalNotificacaoSizMobile').show();");
	}
	
	public List<Municipio> getListaMunicipiosBusca(){
		return municipioService.findAllByUF(this.visitaPropriedadeRuralDTO.getUf());
	}
	
	public void buscarVisitaPropriedadeRural(){
		this.listaVisitaPropriedadeRural = new LazyObjectDataModel<VisitaPropriedadeRural>(this.visitaPropriedadeRuralService, visitaPropriedadeRuralDTO);
	}
	
	@URLAction(mappingId = "incluirVisitaPropriedadeRural", onPostback = false)
	public void novo(){
		limpar();
		
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		
		detalheComunicacaoAIE = (DetalheComunicacaoAIE) request.getSession().getAttribute("detalheComunicacaoAIE");
		
		if (detalheComunicacaoAIE != null){
			request.getSession().setAttribute("detalheComunicacaoAIE", null);
			this.visitaPropriedadeRural.setDetalheComunicacaoAIE(detalheComunicacaoAIE);
		}
		
		registroEntradaLASA = (RegistroEntradaLASA) request.getSession().getAttribute("registroEntradaLASA");
		
		if (registroEntradaLASA != null){
			request.getSession().setAttribute("registroEntradaLASA", null);
			this.visitaPropriedadeRural.setRegistroEntradaLASA(registroEntradaLASA);
		}
		
		detalheFormNotifica = (DetalheFormNotifica) request.getSession().getAttribute("detalheFormNotifica");
		
		if (detalheFormNotifica != null){
			request.getSession().setAttribute("detalheFormNotifica", null);
			
			detalheFormNotifica = formNotificaService.findDetalheFormNotificaByIdFetchAll(detalheFormNotifica.getId());
			
			this.visitaPropriedadeRural.setDetalheFormNotifica(detalheFormNotifica);
			
			if (detalheFormNotifica.getPropriedade().getCodigo() != null)
				this.visitaPropriedadeRural.setPropriedade(detalheFormNotifica.getPropriedade());
		}
		
		loteEnfermidadeAbatedouroFrigorifico = (LoteEnfermidadeAbatedouroFrigorifico) request.getSession().getAttribute("loteEnfermidadeAbatedouroFrigorifico");
		
		if (loteEnfermidadeAbatedouroFrigorifico != null){
			request.getSession().setAttribute("loteEnfermidadeAbatedouroFrigorifico", null);
			
			loteEnfermidadeAbatedouroFrigorifico = enfermidadeAbatedouroFrigorificoService.findLoteEnfermidadeAbatedouroFrigorificoByIdFetchAll(loteEnfermidadeAbatedouroFrigorifico.getId());
			
			this.visitaPropriedadeRural.setLoteEnfermidadeAbatedouroFrigorifico(loteEnfermidadeAbatedouroFrigorifico);
			
			if (loteEnfermidadeAbatedouroFrigorifico.getProprietario().getPropriedade().getCodigo() != null)
				this.visitaPropriedadeRural.setPropriedade(loteEnfermidadeAbatedouroFrigorifico.getProprietario().getPropriedade());
		}
	}
	
	@URLAction(mappingId = "pesquisarVisitaPropriedadeRural", onPostback = false)
	public void pesquisar(){
		limpar();
		
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		String numeroVisitaPropriedadeRural = (String) request.getSession().getAttribute("numeroVisitaPropriedadeRural");
		
		if (numeroVisitaPropriedadeRural != null){
			this.visitaPropriedadeRuralDTO = new VisitaPropriedadeRuralDTO();
			this.visitaPropriedadeRuralDTO.setNumero(numeroVisitaPropriedadeRural);
			this.buscarVisitaPropriedadeRural();
			
			request.getSession().setAttribute("numeroFormIN", null);
		}
	}
	
	@URLAction(mappingId = "alterarVisitaPropriedadeRural", onPostback = false)
	public void editar(){
		this.visitaPropriedadeRural = this.visitaPropriedadeRuralService.findByIdFetchAll(this.getId());
		
		if (this.visitaPropriedadeRural.getPropriedade() != null)
			this.tipoEstabelecimento = TipoEstabelecimento.PROPRIEDADE;
		else if (this.visitaPropriedadeRural.getAbatedouro() != null)
			this.tipoEstabelecimento = TipoEstabelecimento.ABATEDOURO;
		else if (this.visitaPropriedadeRural.getRecinto() != null)
			this.tipoEstabelecimento = TipoEstabelecimento.RECINTO;
		else
			throw new ApplicationRuntimeException("A Visita n�o possui propriedade, recinto ou abatedouro. Entre em contato com o administrador do sistema");
		
		if (visitaPropriedadeRural.getDataDaVisita() != null)
			this.dataDaVisita = visitaPropriedadeRural.getDataDaVisita().getTime();
	}
	
	public String adicionar() throws IOException{
		
		boolean isVisitaPropriedadeRuralOk = true;
		
		if (this.tipoEstabelecimento.equals(TipoEstabelecimento.PROPRIEDADE)){
			if (this.visitaPropriedadeRural.getPropriedade() == null){
				FacesContext context = FacesContext.getCurrentInstance();
				
				context.addMessage("cadastro:fieldPropriedade:campoPropriedade", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Propriedade: valor � obrigat�rio.", "Propriedade: valor � obrigat�rio."));
				context.addMessage("cadastro:fieldProprietario:campoProprietario", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Propriet�rio: valor � obrigat�rio.", "Propriet�rio: valor � obrigat�rio."));
				isVisitaPropriedadeRuralOk = false;
			}else {
				if (this.visitaPropriedadeRural.getPropriedade() != null){
					if (this.visitaPropriedadeRural.getProprietario() == null || this.visitaPropriedadeRural.getProprietario().equals("")){
						FacesContext context = FacesContext.getCurrentInstance();
						
						context.addMessage("cadastro:fieldProprietario:campoProprietario", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Propriet�rio: valor � obrigat�rio.", "Propriet�rio: valor � obrigat�rio."));
						isVisitaPropriedadeRuralOk = false;
					}
				}
				
				if (this.visitaPropriedadeRural.getPropriedade() != null){
					if (this.visitaPropriedadeRural.getPropriedade().getCoordenadaGeografica() == null || 
							this.visitaPropriedadeRural.getPropriedade().getCoordenadaGeografica().getLatGrau() == null ||
							this.visitaPropriedadeRural.getPropriedade().getCoordenadaGeografica().getLatMin() == null ||
							this.visitaPropriedadeRural.getPropriedade().getCoordenadaGeografica().getLatSeg() == null){
						FacesContext context = FacesContext.getCurrentInstance();
						
						context.addMessage("cadastro:fieldPropriedadeLatitude:campoPropriedadeLatitude", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Coordenadas - Latitude: valor � obrigat�rio.", "O valor deve estar corretamente preenchido no SINDESA."));
						isVisitaPropriedadeRuralOk = false;
					}
				}
				
				if (this.visitaPropriedadeRural.getPropriedade() != null){
					if (this.visitaPropriedadeRural.getPropriedade().getCoordenadaGeografica() == null || 
							this.visitaPropriedadeRural.getPropriedade().getCoordenadaGeografica().getLongGrau() == null ||
							this.visitaPropriedadeRural.getPropriedade().getCoordenadaGeografica().getLongMin() == null ||
							this.visitaPropriedadeRural.getPropriedade().getCoordenadaGeografica().getLongSeg() == null){
						FacesContext context = FacesContext.getCurrentInstance();
						
						context.addMessage("cadastro:fieldPropriedadeLongitude:campoPropriedadeLongitude", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Coordenadas - Latitude: valor � obrigat�rio.", "O valor deve estar corretamente preenchido no SINDESA."));
						isVisitaPropriedadeRuralOk = false;
					}
				}
				
			}
		} else if (this.tipoEstabelecimento.equals(TipoEstabelecimento.RECINTO)){
			if (this.visitaPropriedadeRural.getRecinto() == null){
				FacesContext context = FacesContext.getCurrentInstance();
				
				context.addMessage("cadastro:fieldRecinto:campoRecinto", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Recinto: valor � obrigat�rio.", "Recinto: valor � obrigat�rio."));
				isVisitaPropriedadeRuralOk = false;
			}
		} else {
			if (this.visitaPropriedadeRural.getAbatedouro() == null){
				FacesContext context = FacesContext.getCurrentInstance();
				
				context.addMessage("cadastro:fieldAbatedouro:campoAbatedouro", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Abatedouro: valor � obrigat�rio.", "Abatedouro: valor � obrigat�rio."));
				isVisitaPropriedadeRuralOk = false;
			}
			
			if (this.visitaPropriedadeRural.getAbatedouro() != null){
				if (this.visitaPropriedadeRural.getAbatedouro().getCoordenadaGeografica() == null || 
						this.visitaPropriedadeRural.getAbatedouro().getCoordenadaGeografica().getLatGrau() == null ||
						this.visitaPropriedadeRural.getAbatedouro().getCoordenadaGeografica().getLatMin() == null ||
						this.visitaPropriedadeRural.getAbatedouro().getCoordenadaGeografica().getLatSeg() == null){
					FacesContext context = FacesContext.getCurrentInstance();
					
					context.addMessage("cadastro:fieldAbatedouroLatitude:campoAbatedouroLatitude", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Coordenadas - Latitude: valor � obrigat�rio.", "O valor deve estar corretamente preenchido no SINDESA."));
					isVisitaPropriedadeRuralOk = false;
				}
			}
			
			if (this.visitaPropriedadeRural.getAbatedouro() != null){
				if (this.visitaPropriedadeRural.getAbatedouro().getCoordenadaGeografica() == null || 
						this.visitaPropriedadeRural.getAbatedouro().getCoordenadaGeografica().getLongGrau() == null ||
						this.visitaPropriedadeRural.getAbatedouro().getCoordenadaGeografica().getLongMin() == null ||
						this.visitaPropriedadeRural.getAbatedouro().getCoordenadaGeografica().getLongSeg() == null){
					FacesContext context = FacesContext.getCurrentInstance();
					
					context.addMessage("cadastro:fieldAbatedouroLongitude:campoAbatedouroLongitude", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Coordenadas - Longitude: valor � obrigat�rio.", "O valor deve estar corretamente preenchido no SINDESA."));
					isVisitaPropriedadeRuralOk = false;
				}
			}
		}
		
		if (this.visitaPropriedadeRural.getListChavePrincipalVisitaPropriedadeRural() == null || this.visitaPropriedadeRural.getListChavePrincipalVisitaPropriedadeRural().isEmpty()){
			FacesContext context = FacesContext.getCurrentInstance();
			
			context.addMessage("cadastro:tableProgramas", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Programas sanit�rios e atividades: deve ser adicionado ao menos um.", "Programas sanit�rios e atividades: deve ser adicionado ao menos um."));
			isVisitaPropriedadeRuralOk = false;;
		}
		
		if (!this.visitaPropriedadeRural.possuiMotivoPrincipal()){
			FacesContext context = FacesContext.getCurrentInstance();
			
			context.addMessage("cadastro:tableProgramas", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Programas sanit�rios e atividades: deve haver um motivo principal.", "Programas sanit�rios e atividades: deve haver um motivo principal."));
			isVisitaPropriedadeRuralOk = false;;
		}
		
		if (this.visitaPropriedadeRural.getListServidores() == null || this.visitaPropriedadeRural.getListServidores().isEmpty()){
			FacesContext context = FacesContext.getCurrentInstance();
			
			context.addMessage("cadastro:tableServidores", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Servidores que realizaram a visita: deve ser adicionado ao menos um.", "Servidores que realizaram a visita: deve ser adicionado ao menos um."));
			isVisitaPropriedadeRuralOk = false;;
		}
		
		if (!isVisitaPropriedadeRuralOk)
			return null;
		
		if (this.dataDaVisita!= null){
			if (this.visitaPropriedadeRural.getDataDaVisita() == null)
				this.visitaPropriedadeRural.setDataDaVisita(Calendar.getInstance());
			this.visitaPropriedadeRural.getDataDaVisita().setTime(dataDaVisita);
		}else
			this.visitaPropriedadeRural.setDataDaVisita(null);
		
		if (this.detalheComunicacaoAIE != null){
			this.visitaPropriedadeRural.setDetalheComunicacaoAIE(detalheComunicacaoAIE);
			this.detalheComunicacaoAIE.setVisitaPropriedadeRural(visitaPropriedadeRural);
		}
		
		if (this.registroEntradaLASA != null){
			this.visitaPropriedadeRural.setRegistroEntradaLASA(registroEntradaLASA);
			this.registroEntradaLASA.setVisitaPropriedadeRural(visitaPropriedadeRural);
		}
		
		if (this.detalheFormNotifica != null){
			this.visitaPropriedadeRural.setDetalheFormNotifica(detalheFormNotifica);
			this.detalheFormNotifica.setVisitaPropriedadeRural(visitaPropriedadeRural);
		}
		
		if (this.loteEnfermidadeAbatedouroFrigorifico != null){
			this.visitaPropriedadeRural.setLoteEnfermidadeAbatedouroFrigorifico(loteEnfermidadeAbatedouroFrigorifico);
			this.loteEnfermidadeAbatedouroFrigorifico.setVisitaPropriedadeRural(visitaPropriedadeRural);
		}
		
		if (visitaPropriedadeRural.getId() != null){
			this.visitaPropriedadeRuralService.saveOrUpdate(visitaPropriedadeRural);
			FacesMessageUtil.addInfoContextFacesMessage("Visita Propriedade Rural n� " + this.visitaPropriedadeRural.getNumero() + " atualizada", "");
		}else{
			this.visitaPropriedadeRural.setDataCadastro(Calendar.getInstance());
			this.visitaPropriedadeRuralService.saveOrUpdate(visitaPropriedadeRural);
			FacesMessageUtil.addInfoContextFacesMessage("Visita Propriedade Rural n� " + this.visitaPropriedadeRural.getNumero() + " adicionada", "");
		}
		
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		request.getSession().setAttribute("numeroVisitaPropriedadeRural", visitaPropriedadeRural.getNumero());
		
		limpar();
		return "pretty:pesquisarVisitaPropriedadeRural";
	}

	private void limpar() {
		this.visitaPropriedadeRural = new VisitaPropriedadeRural();
		this.dataDaVisita = null;
	}
	
	public void remover(VisitaPropriedadeRural visitaPropriedadeRural){
		this.visitaPropriedadeRuralService.delete(this.visitaPropriedadeRuralService.findByIdFetchAll(visitaPropriedadeRural.getId()));
		FacesMessageUtil.addInfoContextFacesMessage("Visita Propriedade Rural n� " + visitaPropriedadeRural.getNumero() + " exclu�da com sucesso", "");
	}
	
	public boolean isTelaBuscaPropriedadeAbertaOnLoad(){
		boolean result = false;
		
		if (telaBuscaJaFoiAberta)
			return false;
		
		if (detalheComunicacaoAIE != null){
			result = true;
			this.nomePropriedade = detalheComunicacaoAIE.getNomePropriedade();
			activeIndexTelaBuscaPropriedade = 1;
		} else if (registroEntradaLASA != null){
			
			
		} else if (detalheFormNotifica != null){
			if (detalheFormNotifica.getPropriedade().getCodigo() == null){
				result = true;
				this.nomePropriedade = detalheFormNotifica.getPropriedade().getNome();
				activeIndexTelaBuscaPropriedade = 1;
			}
		} else if (loteEnfermidadeAbatedouroFrigorifico != null){
			if (loteEnfermidadeAbatedouroFrigorifico.getProprietario().getPropriedade().getCodigo() == null){
				result = true;
				this.nomePropriedade = loteEnfermidadeAbatedouroFrigorifico.getProprietario().getPropriedade().getNome();
				activeIndexTelaBuscaPropriedade = 1;
			}
		}
		
		return result;
	}
	
	public void abrirTelaBuscaPrimeiraVez(){
		telaBuscaJaFoiAberta = true;
	}
	
	public void abrirTelaDeBuscaDePropriedade(){
		this.idPropriedade = null;
		this.nomePropriedade = null;
		this.listaPropriedades = null;
	}
	
	public void buscarPropriedadeCodigo(){
		this.listaPropriedades = null;
		
		Propriedade p;
		try {
			p = propriedadeService.findByCodigoFetchAll(idPropriedade, forceUpdatePropriedade);
			
			if (p != null){
				this.listaPropriedades = new ArrayList<Propriedade>();
				listaPropriedades.add(p);
			}
		} catch (ApplicationException e) {
			FacesMessageUtil.addErrorContextFacesMessage("Erro", e.getLocalizedMessage());
			return;
		}
		
		if (this.listaPropriedades == null || this.listaPropriedades.isEmpty())
			FacesMessageUtil.addWarnContextFacesMessage("Nenhuma propriedade foi encontrada", "");
	}
	
	public void buscarPropriedadeNome(){
		this.listaPropriedades = null;
		
		try {
			listaPropriedades = propriedadeService.findByNomeFetchAll(nomePropriedade);
		} catch (ApplicationException e) {
			FacesMessageUtil.addErrorContextFacesMessage("Erro", e.getLocalizedMessage());
			return;
		}
		
		if (this.listaPropriedades == null || this.listaPropriedades.isEmpty())
			FacesMessageUtil.addWarnContextFacesMessage("Nenhuma propriedade foi encontrada", "");
	}
	
	public void selecionarPropriedade(Propriedade propriedade){
		this.visitaPropriedadeRural.setPropriedade(propriedade);
		this.visitaPropriedadeRural.setNomePropriedade(propriedade.getNome());
		this.visitaPropriedadeRural.setCodigoPropriedade(propriedade.getCodigoPropriedade());
		
		FacesMessageUtil.addInfoContextFacesMessage("Propriedade " + this.visitaPropriedadeRural.getPropriedade().getNome() + " selecionada", "");
	}
	
	public List<Produtor> getListaProdutoresDaPropriedade(){
		HashSet<Produtor> set = new HashSet<Produtor>();
		
		if (this.visitaPropriedadeRural.getPropriedade() != null){
			if (this.visitaPropriedadeRural.getPropriedade().getProprietarios() != null)
				set.addAll(this.visitaPropriedadeRural.getPropriedade().getProprietarios());
			
			if (this.visitaPropriedadeRural.getPropriedade().getExploracaos() != null)
				for (int i = 0; i < this.visitaPropriedadeRural.getPropriedade().getExploracaos().size(); i++) {
					if (this.visitaPropriedadeRural.getPropriedade().getExploracaos().get(i).getProdutores() != null)
						set.addAll(this.visitaPropriedadeRural.getPropriedade().getExploracaos().get(i).getProdutores());
				}
		}
		
		List<Produtor> lista = new ArrayList<Produtor>(set);
		return lista;
	}
	
	public void selecionarProdutor(Produtor produtor) throws CloneNotSupportedException{
		this.visitaPropriedadeRural.setProprietario((Produtor) produtor.clone());
		
		FacesMessageUtil.addInfoContextFacesMessage("Propriet�rio " + this.visitaPropriedadeRural.getProprietario().getNome() + " selecionado", "");
	}
	
	public void abrirTelaDeBuscaDeRecinto(){
		this.idRecinto = null;;
		this.listaRecintos = null;
	}
	
	public void buscarRecintoPorId(){
		this.listaRecintos = null;
		
		try {
			Recinto recinto = clientWebServiceRecinto.getRecintoById(idRecinto);
			
			if (recinto != null){
				this.listaRecintos = new ArrayList<Recinto>();
				this.listaRecintos.add(recinto);
			}
		} catch (ApplicationException e) {
			FacesMessageUtil.addErrorContextFacesMessage("Erro", e.getLocalizedMessage());
		}
		
		if (this.listaRecintos == null || this.listaRecintos.isEmpty())
			FacesMessageUtil.addWarnContextFacesMessage("Nenhum veterin�rio foi encontrado", "");
	}
	
	public void selecionarRecinto(Recinto recinto){
		this.visitaPropriedadeRural.setRecinto(recinto);
		FacesMessageUtil.addInfoContextFacesMessage("Recinto " + this.visitaPropriedadeRural.getRecinto().getNome() + " selecionado", "");
	}
	
	public List<TipoChavePrincipalVisitaPropriedadeRural> getListaTipoChavePrincipalVisitaPropriedadeRural(){
		return this.tipoChavePrincipalVisitaPropriedadeRuralService.findAllByStatus(AtivoInativo.ATIVO);
	}
	
	public List<TipoChaveSecundariaVisitaPropriedadeRural> getListaTipoChaveSecundariaVisitaPropriedadeRural(){
		return this.tipoChaveSecundariaVisitaPropriedadeRuralService.findAllByStatus(AtivoInativo.ATIVO);
	}
	
	public List<ChavePrincipalVisitaPropriedadeRural> getListaTipoChavePrincipalVisitaPropriedadeRural_Inativos(){
		List<ChavePrincipalVisitaPropriedadeRural> listChavePrincipalVisitaPropriedadeRural = new ArrayList<ChavePrincipalVisitaPropriedadeRural>();
		
		if (this.visitaPropriedadeRural.getListChavePrincipalVisitaPropriedadeRural() != null && !this.visitaPropriedadeRural.getListChavePrincipalVisitaPropriedadeRural().isEmpty()){
			for (ChavePrincipalVisitaPropriedadeRural chavePrincipalVisitaPropriedadeRural : this.visitaPropriedadeRural.getListChavePrincipalVisitaPropriedadeRural()) {
				if (chavePrincipalVisitaPropriedadeRural.getTipoChavePrincipalVisitaPropriedadeRural().getStatus().equals(AtivoInativo.INATIVO))
					listChavePrincipalVisitaPropriedadeRural.add(chavePrincipalVisitaPropriedadeRural);
			}
		}
		
		return listChavePrincipalVisitaPropriedadeRural;
	}
	
	public List<ChaveSecundariaVisitaPropriedadeRural> getListaChaveSecundariaVisitaPropriedadeRural_Inativos(){
		List<ChaveSecundariaVisitaPropriedadeRural> listChaveSecundariaVisitaPropriedadeRural = new ArrayList<ChaveSecundariaVisitaPropriedadeRural>();
		
		if (chavePrincipalVisitaPropriedadeRural != null)
			if (chavePrincipalVisitaPropriedadeRural.getListChaveSecundariaVisitaPropriedadeRural() != null && !chavePrincipalVisitaPropriedadeRural.getListChaveSecundariaVisitaPropriedadeRural().isEmpty()){
				for (ChaveSecundariaVisitaPropriedadeRural chaveSecundariaVisitaPropriedadeRural : chavePrincipalVisitaPropriedadeRural.getListChaveSecundariaVisitaPropriedadeRural()) {
					if (chaveSecundariaVisitaPropriedadeRural.getTipoChaveSecundariaVisitaPropriedadeRural().getStatus().equals(AtivoInativo.INATIVO))
						listChaveSecundariaVisitaPropriedadeRural.add(chaveSecundariaVisitaPropriedadeRural);
				}
			}
		
		return listChaveSecundariaVisitaPropriedadeRural;
	}
	
	public void adicionarChavePrincipalVisitaPropriedadeRural(){
		if (this.visitaPropriedadeRural.getListChavePrincipalVisitaPropriedadeRural() != null){
			for (ChavePrincipalVisitaPropriedadeRural chave : this.visitaPropriedadeRural.getListChavePrincipalVisitaPropriedadeRural()) {
				if (chave.getTipoChavePrincipalVisitaPropriedadeRural().equals(this.chavePrincipalVisitaPropriedadeRural.getTipoChavePrincipalVisitaPropriedadeRural())){
					FacesMessageUtil.addWarnContextFacesMessage("Chave " + this.chavePrincipalVisitaPropriedadeRural.getTipoChavePrincipalVisitaPropriedadeRural().getNome() + " j� est� cadastrada", "");
					return;
				}
			}
		}
		
		
		this.chavePrincipalVisitaPropriedadeRural.setVisitaPropriedadeRural(this.visitaPropriedadeRural);
		
		// fazer um for e criar as chaves
		if (!editandoChavePrincipalVisitaPropriedadeRural){
			ChaveSecundariaVisitaPropriedadeRural chaveSecundariaVisitaPropriedadeRural;
			
			for (TipoChaveSecundariaVisitaPropriedadeRural tipo : this.tipoChaveSecundariaVisitaPropriedadeRural) {
				chaveSecundariaVisitaPropriedadeRural = new ChaveSecundariaVisitaPropriedadeRural();
				
				chaveSecundariaVisitaPropriedadeRural.setDataCadastro(Calendar.getInstance());
				chaveSecundariaVisitaPropriedadeRural.setChavePrincipalVisitaPropriedadeRural(this.chavePrincipalVisitaPropriedadeRural);
				chaveSecundariaVisitaPropriedadeRural.setTipoChaveSecundariaVisitaPropriedadeRural(tipo);
				
				this.chavePrincipalVisitaPropriedadeRural.getListChaveSecundariaVisitaPropriedadeRural().add(chaveSecundariaVisitaPropriedadeRural);
			}
			
			this.visitaPropriedadeRural.getListChavePrincipalVisitaPropriedadeRural().add(this.chavePrincipalVisitaPropriedadeRural);
		}else{
			
			for (int i = 0; i < this.chavePrincipalVisitaPropriedadeRural.getListChaveSecundariaVisitaPropriedadeRural().size(); i++) {
				if (!this.tipoChaveSecundariaVisitaPropriedadeRural.contains(this.chavePrincipalVisitaPropriedadeRural.getListChaveSecundariaVisitaPropriedadeRural().get(i).getTipoChaveSecundariaVisitaPropriedadeRural())){
					this.chavePrincipalVisitaPropriedadeRural.getListChaveSecundariaVisitaPropriedadeRural().remove(i);
					i--;
				}
			}
			
			boolean contains;
			ChaveSecundariaVisitaPropriedadeRural chaveSecundariaVisitaPropriedadeRural;
			
			
			// percorre B
			for (int i = 0; i < this.tipoChaveSecundariaVisitaPropriedadeRural.size(); i++) {
				contains = false;
				
				// percorre A
				for (int j = 0; j < this.chavePrincipalVisitaPropriedadeRural.getListChaveSecundariaVisitaPropriedadeRural().size(); j++) {
					// checa se algum item de B n�o est� em A 
					if (this.chavePrincipalVisitaPropriedadeRural.getListChaveSecundariaVisitaPropriedadeRural().get(j).getTipoChaveSecundariaVisitaPropriedadeRural().equals(this.tipoChaveSecundariaVisitaPropriedadeRural.get(i)))
						contains = true;
				}
				
				if (!contains){
					chaveSecundariaVisitaPropriedadeRural = new ChaveSecundariaVisitaPropriedadeRural();
					
					chaveSecundariaVisitaPropriedadeRural.setDataCadastro(Calendar.getInstance());
					chaveSecundariaVisitaPropriedadeRural.setChavePrincipalVisitaPropriedadeRural(this.chavePrincipalVisitaPropriedadeRural);
					chaveSecundariaVisitaPropriedadeRural.setTipoChaveSecundariaVisitaPropriedadeRural(this.tipoChaveSecundariaVisitaPropriedadeRural.get(i));
					
					this.chavePrincipalVisitaPropriedadeRural.getListChaveSecundariaVisitaPropriedadeRural().add(chaveSecundariaVisitaPropriedadeRural);
					
				}
			}
			
			editandoChavePrincipalVisitaPropriedadeRural = false;
		}
		
		this.chavePrincipalVisitaPropriedadeRural = new ChavePrincipalVisitaPropriedadeRural();
		this.tipoChaveSecundariaVisitaPropriedadeRural = new ArrayList<TipoChaveSecundariaVisitaPropriedadeRural>();
		FacesMessageUtil.addInfoContextFacesMessage("Atividades realizadas na propriedade incluidas com sucesso", "");
	}
	
	public void novaChavePrincipalVisitaPropriedadeRural(){
		this.chavePrincipalVisitaPropriedadeRural = new ChavePrincipalVisitaPropriedadeRural();
		this.editandoChavePrincipalVisitaPropriedadeRural = false;
		this.visualizandoChavePrincipalVisitaPropriedadeRural = false;
		this.tipoChaveSecundariaVisitaPropriedadeRural = new ArrayList<TipoChaveSecundariaVisitaPropriedadeRural>();
	}
	
	public void removerChavePrincipalVisitaPropriedadeRural(ChavePrincipalVisitaPropriedadeRural chavePrincipalVisitaPropriedadeRural){
		this.visitaPropriedadeRural.getListChavePrincipalVisitaPropriedadeRural().remove(chavePrincipalVisitaPropriedadeRural);
		
		FacesMessageUtil.addInfoContextFacesMessage("Atividades realizadas na propriedade incluidas com sucesso", "");		
	}
	
	public void editarChavePrincipalVisitaPropriedadeRural(ChavePrincipalVisitaPropriedadeRural chavePrincipalVisitaPropriedadeRural){
		this.editandoChavePrincipalVisitaPropriedadeRural = true;
		this.visualizandoChavePrincipalVisitaPropriedadeRural = false;
		this.chavePrincipalVisitaPropriedadeRural = chavePrincipalVisitaPropriedadeRural;
		
		this.tipoChaveSecundariaVisitaPropriedadeRural = new ArrayList<TipoChaveSecundariaVisitaPropriedadeRural>();
		
		for (ChaveSecundariaVisitaPropriedadeRural chave : this.chavePrincipalVisitaPropriedadeRural.getListChaveSecundariaVisitaPropriedadeRural()) {
			this.tipoChaveSecundariaVisitaPropriedadeRural.add(chave.getTipoChaveSecundariaVisitaPropriedadeRural());
		}
		
	}
	
	public void visualizarChavePrincipalVisitaPropriedadeRural(ChavePrincipalVisitaPropriedadeRural chavePrincipalVisitaPropriedadeRural){
		this.visualizandoChavePrincipalVisitaPropriedadeRural = true;
		this.editandoChavePrincipalVisitaPropriedadeRural = false;
		this.chavePrincipalVisitaPropriedadeRural = chavePrincipalVisitaPropriedadeRural;
		
		this.tipoChaveSecundariaVisitaPropriedadeRural = new ArrayList<TipoChaveSecundariaVisitaPropriedadeRural>();
		
		for (ChaveSecundariaVisitaPropriedadeRural chave : this.chavePrincipalVisitaPropriedadeRural.getListChaveSecundariaVisitaPropriedadeRural()) {
			this.tipoChaveSecundariaVisitaPropriedadeRural.add(chave.getTipoChaveSecundariaVisitaPropriedadeRural());
		}
	}
	
	public void removerChaveSecundaria(ChaveSecundariaVisitaPropriedadeRural chaveSecundariaVisitaPropriedadeRural){
		this.chavePrincipalVisitaPropriedadeRural.getListChaveSecundariaVisitaPropriedadeRural().remove(chaveSecundariaVisitaPropriedadeRural);
	}
	
	public void selecionarMotivoPrincipal(ChavePrincipalVisitaPropriedadeRural chavePrincipalVisitaPropriedadeRural){
		if (this.visitaPropriedadeRural.getListChavePrincipalVisitaPropriedadeRural() != null){
			for (ChavePrincipalVisitaPropriedadeRural chave : this.visitaPropriedadeRural.getListChavePrincipalVisitaPropriedadeRural()) {
				if (chave.getTipoChavePrincipalVisitaPropriedadeRural().equals(chavePrincipalVisitaPropriedadeRural.getTipoChavePrincipalVisitaPropriedadeRural()))
					chave.setMotivoPrincipal(SimNao.SIM);
				else
					chave.setMotivoPrincipal(SimNao.NAO);
			}
		}
	}
	
	public boolean isTipoEstabelecimentoIgualPropriedade(){
		boolean result = false;
		
		if (this.tipoEstabelecimento != null){
			result = this.tipoEstabelecimento.equals(TipoEstabelecimento.PROPRIEDADE);
			
			if (result){
				this.visitaPropriedadeRural.setRecinto(null);
				this.visitaPropriedadeRural.setAbatedouro(null);
			} else {
				this.visitaPropriedadeRural.setPropriedade(null);
			}
		}
		
		return result;
	}
	
	public boolean isTipoEstabelecimentoIgualRecinto(){
		boolean result = false;
		
		if (this.tipoEstabelecimento != null){
			result = this.tipoEstabelecimento.equals(TipoEstabelecimento.RECINTO);
			
			if (result){
				this.visitaPropriedadeRural.setPropriedade(null);
				this.visitaPropriedadeRural.setAbatedouro(null);
			} else {
				this.visitaPropriedadeRural.setRecinto(null);
			}
		}
		
		return result;
	}
	
	public boolean isTipoEstabelecimentoIgualAbatedouro(){
		boolean result = false;
		
		if (this.tipoEstabelecimento != null){
			result = this.tipoEstabelecimento.equals(TipoEstabelecimento.ABATEDOURO);
			
			if (result){
				this.visitaPropriedadeRural.setPropriedade(null);
				this.visitaPropriedadeRural.setRecinto(null);
			} else {
				this.visitaPropriedadeRural.setAbatedouro(null);
			}
		}
		
		return result;
	}
	
	public void adicionarServidor(){
		if (!editandoServidor)
			this.visitaPropriedadeRural.getListServidores().add(servidor);
		else
			editandoServidor = false;
		
		this.servidor = new Servidor();
		
		FacesMessageUtil.addInfoContextFacesMessage("Registro inclu�do com sucesso", "");
	}
	
	public void novoServidor(){
		this.servidor= new Servidor();
		this.editandoServidor = false;
		this.visualizandoServidor = false;
	}
	
	public void removerServidor(Servidor servidor){
		this.visitaPropriedadeRural.getListServidores().remove(servidor);
	}
	
	public void editarServidor(Servidor servidor){
		this.editandoServidor = true;
		this.visualizandoServidor = false;
		this.servidor = servidor;
	}
	
	public void visualizarServidor(Servidor servidor){
		this.visualizandoServidor = true;
		this.editandoServidor = false;
		this.servidor = servidor;
	}
	
	public void abrirTelaDeBuscaDeServidor(){
		this.cpfServidor = null;;
		this.matriculaServidor = null;
		this.listaServidores = null;
	}
	
	public void buscarServidorPorCpf(){
		this.listaServidores = null;
		
		Servidor s = null;
		try {
			s = clientWebServiceServidor.getServidorByCpf(cpfServidor);
			if (s != null) {
				this.listaServidores = new ArrayList<Servidor>();
				this.listaServidores.add(s);
			}
		} catch (ApplicationException e) {
			FacesMessageUtil.addErrorContextFacesMessage("Erro", e.getLocalizedMessage());
			return;
		}
		
		if (this.listaServidores == null || this.listaServidores.isEmpty())
			FacesMessageUtil.addWarnContextFacesMessage("Nenhum servidor foi encontrado", "");
	}
	
	public void buscarServidorPorMatricula(){
		this.listaServidores = null;
		
		Servidor s = null;
		try {
			s = clientWebServiceServidor.getServidorByMatricula(this.matriculaServidor);
			if (s != null) {
				this.listaServidores = new ArrayList<Servidor>();
				this.listaServidores.add(s);
			}
		} catch (ApplicationException e) {
			FacesMessageUtil.addErrorContextFacesMessage("Erro", e.getLocalizedMessage());
			return;
		}
		
		if (this.listaServidores == null || this.listaServidores.isEmpty())
			FacesMessageUtil.addWarnContextFacesMessage("Nenhum servidor foi encontrado", "");
	}
	
	public void selecionarServidor(Servidor servidor){
		this.visitaPropriedadeRural.getListServidores().add(servidor);
		FacesMessageUtil.addInfoContextFacesMessage("Servidor " + servidor.getNome() + " adicionado", "");
	}
	
	public void abrirTelaDeBuscaDeAbatedouro(){
		this.cnpjAbatedouro = null;
		this.listaAbatedouros = null;
	}
	
	public void buscarAbatedouroPorCNPJ(){
		this.listaAbatedouros = null;
		
		try {
			Abatedouro abatedouro = clientWebServiceAbatedouro.getAbatedouroByCNPJ(cnpjAbatedouro);
			if (abatedouro != null){
				this.listaAbatedouros = new ArrayList<Abatedouro>();
				this.listaAbatedouros.add(abatedouro);
			}
		} catch (ApplicationException e) {
			FacesMessageUtil.addErrorContextFacesMessage("Erro", e.getLocalizedMessage());
			return;
		}
		
		if (this.listaAbatedouros == null || this.listaAbatedouros.isEmpty())
			FacesMessageUtil.addInfoContextFacesMessage("Nenhum abatedouro foi encontrado", "");
	}
	
	public void selecionarAbatedouro(Abatedouro abatedouro){
		this.visitaPropriedadeRural.setAbatedouro(abatedouro);
		
		FacesMessageUtil.addInfoContextFacesMessage("Abatedouro " + this.visitaPropriedadeRural.getAbatedouro().getNome() + " selecionado", "");
	}
	
	public VisitaPropriedadeRural getVisitaPropriedadeRural() {
		return visitaPropriedadeRural;
	}

	public void setVisitaPropriedadeRural(
			VisitaPropriedadeRural visitaPropriedadeRural) {
		this.visitaPropriedadeRural = visitaPropriedadeRural;
	}

	public Date getDataDaVisita() {
		return dataDaVisita;
	}

	public void setDataDaVisita(Date dataDaVisita) {
		this.dataDaVisita = dataDaVisita;
	}

	public Long getIdPropriedade() {
		return idPropriedade;
	}

	public void setIdPropriedade(Long idPropriedade) {
		this.idPropriedade = idPropriedade;
	}

	public List<Propriedade> getListaPropriedades() {
		return listaPropriedades;
	}

	public void setListaPropriedades(List<Propriedade> listaPropriedades) {
		this.listaPropriedades = listaPropriedades;
	}

	public TipoChavePrincipalVisitaPropriedadeRuralService getTipoChavePrincipalVisitaPropriedadeRuralService() {
		return tipoChavePrincipalVisitaPropriedadeRuralService;
	}

	public void setTipoChavePrincipalVisitaPropriedadeRuralService(
			TipoChavePrincipalVisitaPropriedadeRuralService tipoChavePrincipalVisitaPropriedadeRuralService) {
		this.tipoChavePrincipalVisitaPropriedadeRuralService = tipoChavePrincipalVisitaPropriedadeRuralService;
	}

	public boolean isEditandoChavePrincipalVisitaPropriedadeRural() {
		return editandoChavePrincipalVisitaPropriedadeRural;
	}

	public void setEditandoChavePrincipalVisitaPropriedadeRural(
			boolean editandoChavePrincipalVisitaPropriedadeRural) {
		this.editandoChavePrincipalVisitaPropriedadeRural = editandoChavePrincipalVisitaPropriedadeRural;
	}

	public boolean isVisualizandoChavePrincipalVisitaPropriedadeRural() {
		return visualizandoChavePrincipalVisitaPropriedadeRural;
	}

	public void setVisualizandoChavePrincipalVisitaPropriedadeRural(
			boolean visualizandoChavePrincipalVisitaPropriedadeRural) {
		this.visualizandoChavePrincipalVisitaPropriedadeRural = visualizandoChavePrincipalVisitaPropriedadeRural;
	}

	public ChavePrincipalVisitaPropriedadeRural getChavePrincipalVisitaPropriedadeRural() {
		return chavePrincipalVisitaPropriedadeRural;
	}

	public void setChavePrincipalVisitaPropriedadeRural(
			ChavePrincipalVisitaPropriedadeRural chavePrincipalVisitaPropriedadeRural) {
		this.chavePrincipalVisitaPropriedadeRural = chavePrincipalVisitaPropriedadeRural;
	}

	public List<TipoChaveSecundariaVisitaPropriedadeRural> getTipoChaveSecundariaVisitaPropriedadeRural() {
		return tipoChaveSecundariaVisitaPropriedadeRural;
	}

	public void setTipoChaveSecundariaVisitaPropriedadeRural(
			List<TipoChaveSecundariaVisitaPropriedadeRural> tipoChaveSecundariaVisitaPropriedadeRural) {
		this.tipoChaveSecundariaVisitaPropriedadeRural = tipoChaveSecundariaVisitaPropriedadeRural;
	}

	public String getIdServidor() {
		return matriculaServidor;
	}

	public void setIdServidor(String matriculaServidor) {
		this.matriculaServidor = matriculaServidor;
	}

	public String getCpfServidor() {
		return cpfServidor;
	}

	public void setCpfServidor(String cpfServidor) {
		this.cpfServidor = cpfServidor;
	}

	public List<Servidor> getListaServidores() {
		return listaServidores;
	}

	public void setListaServidores(List<Servidor> listaServidores) {
		this.listaServidores = listaServidores;
	}

	public Long getIdRecinto() {
		return idRecinto;
	}

	public void setIdRecinto(Long idRecinto) {
		this.idRecinto = idRecinto;
	}

	public List<Recinto> getListaRecintos() {
		return listaRecintos;
	}

	public void setListaRecintos(List<Recinto> listaRecintos) {
		this.listaRecintos = listaRecintos;
	}
	
	public TipoEstabelecimento getTipoEstabelecimento() {
		return tipoEstabelecimento;
	}

	public void setTipoEstabelecimento(TipoEstabelecimento tipoEstabelecimento) {
		this.tipoEstabelecimento = tipoEstabelecimento;
	}
	
	public Servidor getServidor() {
		return servidor;
	}

	public void setServidor(Servidor servidor) {
		this.servidor = servidor;
	}

	public boolean isEditandoServidor() {
		return editandoServidor;
	}

	public void setEditandoServidor(boolean editandoServidor) {
		this.editandoServidor = editandoServidor;
	}

	public boolean isVisualizandoServidor() {
		return visualizandoServidor;
	}

	public void setVisualizandoServidor(boolean visualizandoServidor) {
		this.visualizandoServidor = visualizandoServidor;
	}

	public String getMatriculaServidor() {
		return matriculaServidor;
	}

	public void setMatriculaServidor(String matriculaServidor) {
		this.matriculaServidor = matriculaServidor;
	}

	public LazyObjectDataModel<VisitaPropriedadeRural> getListaVisitaPropriedadeRural() {
		return listaVisitaPropriedadeRural;
	}

	public void setListaVisitaPropriedadeRural(LazyObjectDataModel<VisitaPropriedadeRural> listaVisitaPropriedadeRural) {
		this.listaVisitaPropriedadeRural = listaVisitaPropriedadeRural;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public VisitaPropriedadeRuralDTO getVisitaPropriedadeRuralDTO() {
		return visitaPropriedadeRuralDTO;
	}

	public void setVisitaPropriedadeRuralDTO(VisitaPropriedadeRuralDTO visitaPropriedadeRuralDTO) {
		this.visitaPropriedadeRuralDTO = visitaPropriedadeRuralDTO;
	}

	public String getNomePropriedade() {
		return nomePropriedade;
	}

	public void setNomePropriedade(String nomePropriedade) {
		this.nomePropriedade = nomePropriedade;
	}

	public int getActiveIndexTelaBuscaPropriedade() {
		activeIndexTelaBuscaPropriedade = this.isTelaBuscaPropriedadeAbertaOnLoad() ? 1 : 0;
		
		return activeIndexTelaBuscaPropriedade;
	}

	public void setActiveIndexTelaBuscaPropriedade(int activeIndexTelaBuscaPropriedade) {
		this.activeIndexTelaBuscaPropriedade = activeIndexTelaBuscaPropriedade;
	}

	public String getCnpjAbatedouro() {
		return cnpjAbatedouro;
	}

	public void setCnpjAbatedouro(String cnpjAbatedouro) {
		this.cnpjAbatedouro = cnpjAbatedouro;
	}

	public List<Abatedouro> getListaAbatedouros() {
		return listaAbatedouros;
	}

	public void setListaAbatedouros(List<Abatedouro> listaAbatedouros) {
		this.listaAbatedouros = listaAbatedouros;
	}

	public boolean isForceUpdatePropriedade() {
		return forceUpdatePropriedade;
	}

	public void setForceUpdatePropriedade(boolean forceUpdatePropriedade) {
		this.forceUpdatePropriedade = forceUpdatePropriedade;
	}
	
}
