package br.gov.mt.indea.sistemaformin.webservice.convert;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import br.gov.mt.indea.sistemaformin.entity.Municipio;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoUnidade;
import br.gov.mt.indea.sistemaformin.service.MunicipioService;
import br.gov.mt.indea.sistemaformin.service.ULEService;
import br.gov.mt.indea.sistemaformin.webservice.entity.CoordenadaGeografica;
import br.gov.mt.indea.sistemaformin.webservice.entity.Endereco;
import br.gov.mt.indea.sistemaformin.webservice.entity.Exploracao;
import br.gov.mt.indea.sistemaformin.webservice.entity.Produtor;
import br.gov.mt.indea.sistemaformin.webservice.entity.Propriedade;
import br.gov.mt.indea.sistemaformin.webservice.entity.TipoLogradouro;
import br.gov.mt.indea.sistemaformin.webservice.entity.ULE;

public class PropriedadeConverter {
	
	@Inject
	private MunicipioService municipioService;
	
	@Inject
	private ULEService uleService;
	
	public Propriedade fromModelToEntity(br.gov.mt.indea.sistemaformin.webservice.model.Propriedade modelo){
		if (modelo == null)
			return null;
		
		Propriedade propriedade = new Propriedade();
		propriedade.setCodigoPropriedade(modelo.getId());
		
		propriedade.setApelido(modelo.getApelido());
		propriedade.setCodigo(modelo.getCodigo());
		propriedade.setComplemnto(modelo.getComplemnto());
		propriedade.setCpfCnpj(modelo.getCpfCnpj());
		propriedade.setEmail(modelo.getEmail());
		propriedade.setEnderecoPropriedade(modelo.getEnderecoPropriedade());
		propriedade.setNome(modelo.getNome());
		propriedade.setTipoPessoa(modelo.getTipoPessoa());
		propriedade.setViaAcesso(modelo.getViaAcesso());
		
		if (modelo.getCoordenadaGeograficas() != null && !modelo.getCoordenadaGeograficas().isEmpty()){
			CoordenadaGeografica coordenadaGeografica = new CoordenadaGeografica();
			
			coordenadaGeografica.setLatGrau(modelo.getCoordenadaGeograficas().get(0).getLatGrau());
			coordenadaGeografica.setLatMin(modelo.getCoordenadaGeograficas().get(0).getLatMin());
			coordenadaGeografica.setLatSeg(modelo.getCoordenadaGeograficas().get(0).getLatSeg());
			coordenadaGeografica.setLongGrau(modelo.getCoordenadaGeograficas().get(0).getLongGrau());
			coordenadaGeografica.setLongMin(modelo.getCoordenadaGeograficas().get(0).getLongMin());
			coordenadaGeografica.setLongSeg(modelo.getCoordenadaGeograficas().get(0).getLongSeg());
			coordenadaGeografica.setOrientacaoLatitude(modelo.getCoordenadaGeograficas().get(0).getOrientacaoLatitude());
			coordenadaGeografica.setOrientacaoLongitude(modelo.getCoordenadaGeograficas().get(0).getOrientacaoLongitude());
			coordenadaGeografica.setPessoa_id(modelo.getCoordenadaGeograficas().get(0).getPessoa_id());
			
			propriedade.setCoordenadaGeografica(coordenadaGeografica);
		}
		
		if (modelo.getEndereco() != null){
			Endereco enderecoPropriedade = new Endereco();
			
			enderecoPropriedade.setBairro(modelo.getEndereco().getBairro());
			enderecoPropriedade.setCep(modelo.getEndereco().getCep());
			enderecoPropriedade.setComplemento(modelo.getEndereco().getComplemento());
			enderecoPropriedade.setLogradouro(modelo.getEndereco().getLogradouro());
			enderecoPropriedade.setNumero(modelo.getEndereco().getNumero());
			enderecoPropriedade.setReferencia(modelo.getEndereco().getReferencia());
			enderecoPropriedade.setTelefone(modelo.getEndereco().getTelefone());
			
			if (modelo.getEndereco().getTipoLogradouro() != null){
				TipoLogradouro tipoLogradouroEnderecoPropriedade = new TipoLogradouro();
				tipoLogradouroEnderecoPropriedade.setNome(modelo.getEndereco().getTipoLogradouro().getNome());
				
				enderecoPropriedade.setTipoLogradouro(tipoLogradouroEnderecoPropriedade);
			}
			
			if (modelo.getEndereco().getMunicipio() != null)
				enderecoPropriedade.setMunicipio(this.findMunicipio(modelo.getEndereco().getMunicipio()));
			
			propriedade.setEndereco(enderecoPropriedade);
		}
		
		if (modelo.getExploracaos() != null && !modelo.getExploracaos().isEmpty()){
			List<Exploracao> listaExploracoes = new ArrayList<Exploracao>();
			Exploracao exploracaoPropriedade = null;
			
			for (br.gov.mt.indea.sistemaformin.webservice.model.Exploracao exploracaoPropriedadeModel : modelo.getExploracaos()) {
				exploracaoPropriedade = new Exploracao();
				
				exploracaoPropriedade.setAnimaisImportados(exploracaoPropriedadeModel.getAnimaisImportados());
				exploracaoPropriedade.setAreaCompartilhada(exploracaoPropriedadeModel.getAreaCompartilhada());
				exploracaoPropriedade.setAreaExploracao(exploracaoPropriedadeModel.getAreaExploracao());
				exploracaoPropriedade.setAreaUtilizada(exploracaoPropriedadeModel.getAreaUtilizada());
				exploracaoPropriedade.setCapacidadeExploracao(exploracaoPropriedadeModel.getCapacidadeExploracao());
				exploracaoPropriedade.setCodigo(exploracaoPropriedadeModel.getCodigo());
				exploracaoPropriedade.setConfinamento(exploracaoPropriedadeModel.getConfinamento());
				exploracaoPropriedade.setDataValidade(exploracaoPropriedadeModel.getDataValidade());
				exploracaoPropriedade.setEspolio(exploracaoPropriedadeModel.getEspolio());
				exploracaoPropriedade.setEstabelecimentoGta(exploracaoPropriedadeModel.getEstabelecimentoGta());
				exploracaoPropriedade.setGrauLatitude(exploracaoPropriedadeModel.getGrauLatitude());
				exploracaoPropriedade.setGrauLongitude(exploracaoPropriedadeModel.getGrauLongitude());
				exploracaoPropriedade.setIdAntigo(exploracaoPropriedadeModel.getIdAntigo());
				exploracaoPropriedade.setMinLatitude(exploracaoPropriedadeModel.getMinLatitude());
				exploracaoPropriedade.setMinLongitude(exploracaoPropriedadeModel.getMinLongitude());
				exploracaoPropriedade.setOrientacaoLatitude(exploracaoPropriedadeModel.getOrientacaoLatitude());
				exploracaoPropriedade.setOrientacaoLongitude(exploracaoPropriedadeModel.getOrientacaoLongitude());
				
				if (exploracaoPropriedadeModel.getProdutores() != null && !exploracaoPropriedadeModel.getProdutores().isEmpty()){
					List<Produtor> listaProdutoresExploracao = new ArrayList<Produtor>();
					Produtor produtorExploracao = null;
					
					for (br.gov.mt.indea.sistemaformin.webservice.model.ProdutorRural produtorExploracaoModel : exploracaoPropriedadeModel.getProdutores()){
						produtorExploracao = new Produtor();
						
						produtorExploracao.setApelido(produtorExploracaoModel.getProdutor().getApelido());
						produtorExploracao.setAssSindicatoRural(produtorExploracaoModel.getProdutor().getAssSindicatoRural());
						produtorExploracao.setCodigoProdutor(produtorExploracaoModel.getProdutor().getCodigoProdutor());
						produtorExploracao.setCoopRural(produtorExploracaoModel.getProdutor().getCoopRural());
						produtorExploracao.setCpf(produtorExploracaoModel.getProdutor().getCpfCnpj());
						produtorExploracao.setEmail(produtorExploracaoModel.getProdutor().getEmail());
						
						if (produtorExploracaoModel.getProdutor().getEndereco() != null){
							Endereco enderecoProdutorExploracao = new Endereco();
							
							enderecoProdutorExploracao.setBairro(produtorExploracaoModel.getProdutor().getEndereco().getBairro());
							enderecoProdutorExploracao.setCep(produtorExploracaoModel.getProdutor().getEndereco().getCep());
							enderecoProdutorExploracao.setComplemento(produtorExploracaoModel.getProdutor().getEndereco().getComplemento());
							enderecoProdutorExploracao.setLogradouro(produtorExploracaoModel.getProdutor().getEndereco().getLogradouro());
							enderecoProdutorExploracao.setNumero(produtorExploracaoModel.getProdutor().getEndereco().getNumero());
							enderecoProdutorExploracao.setReferencia(produtorExploracaoModel.getProdutor().getEndereco().getReferencia());
							enderecoProdutorExploracao.setTelefone(produtorExploracaoModel.getProdutor().getEndereco().getTelefone());
							
							if (produtorExploracaoModel.getProdutor().getEndereco().getTipoLogradouro() != null){
								TipoLogradouro tipoLogradouroEnderecoProdutorExploracao = new TipoLogradouro();
								tipoLogradouroEnderecoProdutorExploracao.setNome(produtorExploracaoModel.getProdutor().getEndereco().getTipoLogradouro().getNome());
								
								enderecoProdutorExploracao.setTipoLogradouro(tipoLogradouroEnderecoProdutorExploracao);
							}
							
							if (produtorExploracaoModel.getProdutor().getEndereco().getMunicipio() != null)
								enderecoProdutorExploracao.setMunicipio(this.findMunicipio(produtorExploracaoModel.getProdutor().getEndereco().getMunicipio()));
							
							produtorExploracao.setEndereco(enderecoProdutorExploracao);
						}
						
						produtorExploracao.setJornalPreferencia(produtorExploracaoModel.getProdutor().getJornalPreferencia());
						produtorExploracao.setNome(produtorExploracaoModel.getProdutor().getNome());
						
						if (produtorExploracaoModel.getProdutor().getPessoaFisica() != null){
							produtorExploracao.setDataNascimento(produtorExploracaoModel.getProdutor().getPessoaFisica().getDataNascimento());
							produtorExploracao.setEmissorRg(produtorExploracaoModel.getProdutor().getPessoaFisica().getEmissorRg());
							produtorExploracao.setMunicipioNascimento(this.findMunicipio(produtorExploracaoModel.getProdutor().getPessoaFisica().getMunicipioNascimento()));
							produtorExploracao.setNomeMae(produtorExploracaoModel.getProdutor().getPessoaFisica().getNomeMae());
							produtorExploracao.setRg(produtorExploracaoModel.getProdutor().getPessoaFisica().getRg());
							produtorExploracao.setSexo(produtorExploracaoModel.getProdutor().getPessoaFisica().getSexo());
						}
						
						produtorExploracao.setRadioPreferencia(produtorExploracaoModel.getProdutor().getRadioPreferencia());
						produtorExploracao.setTipoPessoa(produtorExploracaoModel.getProdutor().getTipoPessoa());
						produtorExploracao.setTvPreferencia(produtorExploracaoModel.getProdutor().getTvPreferencia());
						
						listaProdutoresExploracao.add(produtorExploracao);
					}
					
					exploracaoPropriedade.setProdutores(listaProdutoresExploracao);
				}
				
				exploracaoPropriedade.setPropriedade(propriedade);
				exploracaoPropriedade.setPropriedade_id(exploracaoPropriedadeModel.getPropriedade_id());
				exploracaoPropriedade.setQntPessoaResidente(exploracaoPropriedadeModel.getQntPessoaResidente());
				exploracaoPropriedade.setQntPessoaTrabalha(exploracaoPropriedadeModel.getQntPessoaTrabalha());
				exploracaoPropriedade.setSegLatitude(exploracaoPropriedadeModel.getSegLatitude());
				exploracaoPropriedade.setSegLongitude(exploracaoPropriedadeModel.getSegLongitude());
				
				listaExploracoes.add(exploracaoPropriedade);
			}
			
			propriedade.setExploracaos(listaExploracoes);
		}
		
		if (modelo.getMunicipio() != null)
			propriedade.setMunicipio(this.findMunicipio(modelo.getMunicipio()));
		
		if (modelo.getProprietarios() != null && !modelo.getProprietarios().isEmpty()){
			List<Produtor> listaProprietarios = new ArrayList<Produtor>();
			Produtor proprietarioPropriedade = null;
			
			for (br.gov.mt.indea.sistemaformin.webservice.model.ProprietarioRural proprietarioModel : modelo.getProprietarios()){
				proprietarioPropriedade = new Produtor();
				
				proprietarioPropriedade.setApelido(proprietarioModel.getProprietario().getApelido());
				proprietarioPropriedade.setAssSindicatoRural(proprietarioModel.getProprietario().getAssSindicatoRural());
				proprietarioPropriedade.setCodigoProdutor(proprietarioModel.getProprietario().getCodigoProdutor());
				proprietarioPropriedade.setCoopRural(proprietarioModel.getProprietario().getCoopRural());
				proprietarioPropriedade.setCpf(proprietarioModel.getProprietario().getCpfCnpj());
				proprietarioPropriedade.setEmail(proprietarioModel.getProprietario().getEmail());
				
				if (proprietarioModel.getProprietario().getEndereco() != null){
					Endereco enderecoProprietario = new Endereco();
					
					enderecoProprietario.setBairro(proprietarioModel.getProprietario().getEndereco().getBairro());
					enderecoProprietario.setCep(proprietarioModel.getProprietario().getEndereco().getCep());
					enderecoProprietario.setComplemento(proprietarioModel.getProprietario().getEndereco().getComplemento());
					enderecoProprietario.setLogradouro(proprietarioModel.getProprietario().getEndereco().getLogradouro());
					enderecoProprietario.setNumero(proprietarioModel.getProprietario().getEndereco().getNumero());
					enderecoProprietario.setReferencia(proprietarioModel.getProprietario().getEndereco().getReferencia());
					enderecoProprietario.setTelefone(proprietarioModel.getProprietario().getEndereco().getTelefone());
					
					if (proprietarioModel.getProprietario().getEndereco().getTipoLogradouro() != null){
						TipoLogradouro tipoLogradouroProprietario = new TipoLogradouro();
						tipoLogradouroProprietario.setNome(proprietarioModel.getProprietario().getEndereco().getTipoLogradouro().getNome());
						
						enderecoProprietario.setTipoLogradouro(tipoLogradouroProprietario);
					}
					
					if (proprietarioModel.getProprietario().getEndereco().getMunicipio() != null)
						enderecoProprietario.setMunicipio(this.findMunicipio(proprietarioModel.getProprietario().getEndereco().getMunicipio()));
					
					proprietarioPropriedade.setEndereco(enderecoProprietario);
				}
				
				proprietarioPropriedade.setJornalPreferencia(proprietarioModel.getProprietario().getJornalPreferencia());
				proprietarioPropriedade.setNome(proprietarioModel.getProprietario().getNome());
				
				if (proprietarioModel.getProprietario().getPessoaFisica() != null){
					proprietarioPropriedade.setDataNascimento(proprietarioModel.getProprietario().getPessoaFisica().getDataNascimento());
					proprietarioPropriedade.setEmissorRg(proprietarioModel.getProprietario().getPessoaFisica().getEmissorRg());
					proprietarioPropriedade.setMunicipioNascimento(this.findMunicipio(proprietarioModel.getProprietario().getPessoaFisica().getMunicipioNascimento()));
					proprietarioPropriedade.setNomeMae(proprietarioModel.getProprietario().getPessoaFisica().getNomeMae());
					proprietarioPropriedade.setRg(proprietarioModel.getProprietario().getPessoaFisica().getRg());
					proprietarioPropriedade.setSexo(proprietarioModel.getProprietario().getPessoaFisica().getSexo());
				}
				
				proprietarioPropriedade.setRadioPreferencia(proprietarioModel.getProprietario().getRadioPreferencia());
				proprietarioPropriedade.setTipoPessoa(proprietarioModel.getProprietario().getTipoPessoa());
				proprietarioPropriedade.setTvPreferencia(proprietarioModel.getProprietario().getTvPreferencia());
				
				listaProprietarios.add(proprietarioPropriedade);
			}
			
			propriedade.setProprietarios(listaProprietarios);
		}

		if (modelo.getUle() != null){
			propriedade.setUle(this.findULE(modelo.getUle()));
		}
		
		if (propriedade.getProprietarios() != null){
			for (int i = 0; i < propriedade.getProprietarios().size(); i++) {
				propriedade.getProprietarios().get(i).setCodigoProdutor(modelo.getProprietarios().get(i).getProprietario().getId());
			}
		}
		
		if (propriedade.getExploracaos() != null){
			for (int i = 0; i < propriedade.getExploracaos().size(); i++) {
				propriedade.getExploracaos().get(i).setPropriedade(propriedade);
				
				if (propriedade.getExploracaos().get(i).getProdutores() != null){
					for (int j = 0; j < propriedade.getExploracaos().get(i).getProdutores().size(); j++) {
						propriedade.getExploracaos().get(i).getProdutores().get(j).setCodigoProdutor(modelo.getExploracaos().get(i).getProdutores().get(j).getProdutor().getId());
					}
				}
			}
		}
		
		return propriedade;
	}

	private Municipio findMunicipio(br.gov.mt.indea.sistemaformin.webservice.model.Municipio municipioModel){
		if (municipioModel == null)
			return null;
		
		String codgIBGE = municipioModel.getId() + "";
		
		Municipio municipio = municipioService.findByCodgIBGE(codgIBGE.substring(0, 2), codgIBGE.substring(2, codgIBGE.length()));
		
		return municipio;
	}
	
	private ULE findULE(br.gov.mt.indea.sistemaformin.webservice.model.ULE uleModel){
		if (uleModel == null)
			return null;
		
		TipoUnidade tipoUnidade = null;
		if (uleModel .getUrs() == null)
			tipoUnidade = TipoUnidade.URS;
		else
			tipoUnidade = TipoUnidade.ULE;
		
		ULE ule = uleService.findByNome(uleModel.getNome(), tipoUnidade);
		
		if (ule == null)
			if (tipoUnidade.equals(TipoUnidade.URS)){
				tipoUnidade = TipoUnidade.ULE;
				ule = uleService.findByNome(uleModel.getNome(), tipoUnidade);
			}
		
		return ule;
	}

}
