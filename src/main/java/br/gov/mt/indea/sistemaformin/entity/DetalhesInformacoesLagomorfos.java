package br.gov.mt.indea.sistemaformin.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.envers.Audited;

@Audited
@Entity
@DiscriminatorValue("lagomorfos")
public class DetalhesInformacoesLagomorfos extends AbstractDetalhesInformacoesDeAnimais implements Cloneable{
	
	private static final long serialVersionUID = 1355044082455412390L;

	@ManyToOne
	@JoinColumn(name="id_informacoes_lagomorfos")
	private InformacoesLagomorfos informacoesLagomorfos;

	public InformacoesLagomorfos getInformacoesLagomorfos() {
		return informacoesLagomorfos;
	}

	public void setInformacoesLagomorfos(InformacoesLagomorfos informacoesLagomorfos) {
		this.informacoesLagomorfos = informacoesLagomorfos;
	}
	
	protected Object clone(InformacoesLagomorfos informacoesLagomorfos) throws CloneNotSupportedException{
		DetalhesInformacoesLagomorfos cloned = (DetalhesInformacoesLagomorfos) super.clone();
		
		cloned.setId(null);
		cloned.setInformacoesLagomorfos(informacoesLagomorfos);
		
		return cloned;
	}

}