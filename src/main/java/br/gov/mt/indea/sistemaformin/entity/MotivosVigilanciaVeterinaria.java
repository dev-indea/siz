package br.gov.mt.indea.sistemaformin.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import org.hibernate.envers.Audited;

@Audited
@Entity(name="motivo_vigilancia")
public class MotivosVigilanciaVeterinaria extends BaseEntity<Long>{

	private static final long serialVersionUID = -8168710776579649418L;

	@Id
	@SequenceGenerator(name="motivo_vigilancia_seq", sequenceName="motivo_vigilancia_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="motivo_vigilancia_seq")
	private Long id;
	
	@OneToMany(fetch=FetchType.EAGER)
	@JoinTable(name="motivo_tipo_chave_principal", joinColumns=@JoinColumn(name="id_motivo_vigilancia"), inverseJoinColumns=@JoinColumn(name="id_tipo_chave_principal"))
	private List<TipoChavePrincipalVisitaPropriedadeRural> listaTipoChavePrincipalTermoFiscalizacao = new ArrayList<TipoChavePrincipalVisitaPropriedadeRural>();
	
	@ManyToOne
	@JoinColumn(name="id_vigilancia_veterinaria")
	private VigilanciaVeterinaria vigilanciaVeterinaria;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<TipoChavePrincipalVisitaPropriedadeRural> getListaTipoChavePrincipalTermoFiscalizacao() {
		return listaTipoChavePrincipalTermoFiscalizacao;
	}

	public void setListaTipoChavePrincipalTermoFiscalizacao(List<TipoChavePrincipalVisitaPropriedadeRural> listaTipoChavePrincipalTermoFiscalizacao) {
		this.listaTipoChavePrincipalTermoFiscalizacao.clear();
		this.listaTipoChavePrincipalTermoFiscalizacao.addAll(listaTipoChavePrincipalTermoFiscalizacao);
	}

	public VigilanciaVeterinaria getVigilanciaVeterinaria() {
		return vigilanciaVeterinaria;
	}

	public void setVigilanciaVeterinaria(VigilanciaVeterinaria vigilanciaVeterinaria) {
		this.vigilanciaVeterinaria = vigilanciaVeterinaria;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MotivosVigilanciaVeterinaria other = (MotivosVigilanciaVeterinaria) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}