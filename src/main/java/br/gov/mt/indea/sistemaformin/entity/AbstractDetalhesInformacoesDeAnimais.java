package br.gov.mt.indea.sistemaformin.entity;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.SequenceGenerator;

import org.hibernate.envers.Audited;

import br.gov.mt.indea.sistemaformin.enums.Dominio.FaixaEtariaOuEspecie;

@Audited
@Entity(name="detalhe_info_animais")
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="tipo", discriminatorType=DiscriminatorType.STRING)
public abstract class AbstractDetalhesInformacoesDeAnimais extends BaseEntity<Long>{
	
	private static final long serialVersionUID = 1355044082455412390L;

	@Id
	@SequenceGenerator(name="detalhe_info_animais_seq", sequenceName="detalhe_info_animais_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="detalhe_info_animais_seq")
	private Long id;
	
	@Column(insertable=false, updatable=false)
    private String tipo;
	
	@Enumerated(EnumType.STRING)
	@Column(name="faixa_etaria")
	private FaixaEtariaOuEspecie faixaEtaria;
	
	@Column(name="qtde_macho_no_dia_da_inspecao")
	private Long quantidadeMachosNoDiaDaInspecao;
	
	@Column(name="qtde_femea_no_dia_da_inspecao")
	private Long quantidadeFemeasNoDiaDaInspecao;
	
	@Column(name="qtde_inicio_ocorrencia")
	private Long quantidadeDesdeOInicioDaOcorrencia;
	
	@Column(name="qtde_doentes_confirmados")
	private Long quantidadeDoentesConfirmados;
	
	@Column(name="qtde_doentes_provaveis")
	private Long quantidadeDoentesProvaveis;
	
	@Column(name="qtde_mortos")
	private Long quantidadeMortos;
	
	@Column(name="qtde_abatidos")
	private Long quantidadeAbatidos;
	
	@Column(name="qtde_destruidos")
	private Long quantidadeDestruidos;
	
	@Column(name="qtde_examinados")
	private Long quantidadeExaminados;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public FaixaEtariaOuEspecie getFaixaEtaria() {
		return faixaEtaria;
	}

	public void setFaixaEtaria(FaixaEtariaOuEspecie faixaEtaria) {
		this.faixaEtaria = faixaEtaria;
	}

	public Long getQuantidadeMachosNoDiaDaInspecao() {
		return quantidadeMachosNoDiaDaInspecao;
	}

	public void setQuantidadeMachosNoDiaDaInspecao(
			Long quantidadeMachosNoDiaDaInspecao) {
		this.quantidadeMachosNoDiaDaInspecao = quantidadeMachosNoDiaDaInspecao;
	}

	public Long getQuantidadeFemeasNoDiaDaInspecao() {
		return quantidadeFemeasNoDiaDaInspecao;
	}

	public void setQuantidadeFemeasNoDiaDaInspecao(
			Long quantidadeFemeasNoDiaDaInspecao) {
		this.quantidadeFemeasNoDiaDaInspecao = quantidadeFemeasNoDiaDaInspecao;
	}

	public Long getQuantidadeDesdeOInicioDaOcorrencia() {
		return quantidadeDesdeOInicioDaOcorrencia;
	}

	public void setQuantidadeDesdeOInicioDaOcorrencia(
			Long quantidadeDesdeOInicioDaOcorrencia) {
		this.quantidadeDesdeOInicioDaOcorrencia = quantidadeDesdeOInicioDaOcorrencia;
	}

	public Long getQuantidadeDoentesConfirmados() {
		return quantidadeDoentesConfirmados;
	}

	public void setQuantidadeDoentesConfirmados(Long quantidadeDoentesConfirmados) {
		this.quantidadeDoentesConfirmados = quantidadeDoentesConfirmados;
	}

	public Long getQuantidadeDoentesProvaveis() {
		return quantidadeDoentesProvaveis;
	}

	public void setQuantidadeDoentesProvaveis(Long quantidadeDoentesProvaveis) {
		this.quantidadeDoentesProvaveis = quantidadeDoentesProvaveis;
	}

	public Long getQuantidadeMortos() {
		return quantidadeMortos;
	}

	public void setQuantidadeMortos(Long quantidadeMortos) {
		this.quantidadeMortos = quantidadeMortos;
	}

	public Long getQuantidadeAbatidos() {
		return quantidadeAbatidos;
	}

	public void setQuantidadeAbatidos(Long quantidadeAbatidos) {
		this.quantidadeAbatidos = quantidadeAbatidos;
	}

	public Long getQuantidadeDestruidos() {
		return quantidadeDestruidos;
	}

	public void setQuantidadeDestruidos(Long quantidadeDestruidos) {
		this.quantidadeDestruidos = quantidadeDestruidos;
	}

	public Long getQuantidadeExaminados() {
		return quantidadeExaminados;
	}

	public void setQuantidadeExaminados(Long quantidadeExaminados) {
		this.quantidadeExaminados = quantidadeExaminados;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbstractDetalhesInformacoesDeAnimais other = (AbstractDetalhesInformacoesDeAnimais) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
