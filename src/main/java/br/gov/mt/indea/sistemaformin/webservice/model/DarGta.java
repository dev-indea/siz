package br.gov.mt.indea.sistemaformin.webservice.model;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class DarGta implements Serializable {

	private static final long serialVersionUID = 5230872316556086647L;

	private Long id;
    
    private Integer codigoReceita;
    
    private String numeroDar;
    
    private Float valor;
    
    private Date dataQuitacao;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getCodigoReceita() {
        return codigoReceita;
    }

    public void setCodigoReceita(Integer codigoReceita) {
        this.codigoReceita = codigoReceita;
    }

    public String getNumeroDar() {
        return numeroDar;
    }

    public void setNumeroDar(String numeroDar) {
        this.numeroDar = numeroDar;
    }

    public Float getValor() {
        return valor;
    }

    public void setValor(Float valor) {
        this.valor = valor;
    }

    public Date getDataQuitacao() {
        return dataQuitacao;
    }

    public void setDataQuitacao(Date dataQuitacao) {
        this.dataQuitacao = dataQuitacao;
    }

}
