package br.gov.mt.indea.sistemaformin.managedbeans;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

@Named
@ViewScoped
public class TesteMB implements Serializable{
	
	private static final long serialVersionUID = 2633041531931320771L;
	
	private String teste1;
	
	private String teste2;
	
	private String teste3;
	
	public void testar(){
		FacesContext context = FacesContext.getCurrentInstance();
		
		context.addMessage(null, new FacesMessage("Successful " + teste1));  
		context.addMessage(null, new FacesMessage("Successful " + teste2));
		context.addMessage(null, new FacesMessage("Successful " + teste3));
	}

	public String getTeste1() {
		return teste1;
	}

	public void setTeste1(String teste1) {
		this.teste1 = teste1;
	}

	public String getTeste2() {
		return teste2;
	}

	public void setTeste2(String teste2) {
		this.teste2 = teste2;
	}

	public String getTeste3() {
		return teste3;
	}

	public void setTeste3(String teste3) {
		this.teste3 = teste3;
	}
}
