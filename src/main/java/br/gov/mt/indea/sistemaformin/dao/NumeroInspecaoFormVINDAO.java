package br.gov.mt.indea.sistemaformin.dao;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.inject.Inject;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import br.gov.mt.indea.sistemaformin.entity.FormIN;
import br.gov.mt.indea.sistemaformin.entity.NumeroInspecaoFormVIN;
import br.gov.mt.indea.sistemaformin.webservice.entity.Propriedade;

@Stateless
public class NumeroInspecaoFormVINDAO extends DAO<NumeroInspecaoFormVIN>{

	@Inject
	private EntityManager em;
	
	public NumeroInspecaoFormVINDAO() {
		super(NumeroInspecaoFormVIN.class);
	}

	@Override
	public EntityManager getEntityManager() {
		return this.em;
	}
	
	public NumeroInspecaoFormVIN getNumeroInspecaoFormVINByFormINEPropriedade(FormIN formIN, Propriedade propriedade){
		NumeroInspecaoFormVIN numeroFormIN;
		
		CriteriaBuilder cb  = em.getCriteriaBuilder();
		CriteriaQuery<NumeroInspecaoFormVIN> query = cb.createQuery(NumeroInspecaoFormVIN.class);
		Root<NumeroInspecaoFormVIN> rootNumeroInspecaoFormVIN = query.from(NumeroInspecaoFormVIN.class);
		
		List<Predicate> predicados = new ArrayList<Predicate>();
		predicados.add(cb.equal(rootNumeroInspecaoFormVIN.get("numeroFormIN"), formIN.getNumero()));
		predicados.add(cb.equal(rootNumeroInspecaoFormVIN.get("idPropriedade"), propriedade.getCodigoPropriedade()));
		
		query.where(cb.and(predicados.toArray(new Predicate[]{})));
		
		try {
			numeroFormIN = em.createQuery(query).getSingleResult();
		} catch (NoResultException e) {
			numeroFormIN = null;
		}
		
		return numeroFormIN;
	}

}
