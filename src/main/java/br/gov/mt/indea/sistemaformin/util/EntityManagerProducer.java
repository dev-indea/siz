package br.gov.mt.indea.sistemaformin.util;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Dependent;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

import org.hibernate.Session;

@ApplicationScoped
public class EntityManagerProducer {
	
    @PersistenceUnit(name="primary")
    private EntityManagerFactory emf;

    @Produces
	@RequestScoped
	private EntityManager getEntityManager() {
		return emf.createEntityManager();
	}
	
	public void closeEntityManager(@Disposes EntityManager em){
		try {
			em.close();
		} catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
}
