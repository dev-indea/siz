package br.gov.mt.indea.sistemaformin.entity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

import br.gov.mt.indea.sistemaformin.enums.Dominio.SimNao;

@Audited
@Entity
@Table(name="form_sv")
public class FormSV extends BaseEntity<Long>{
	
	private static final long serialVersionUID = 6366411125208900205L;
	
	@Id
	@SequenceGenerator(name="form_sv_seq", sequenceName="form_sv_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="form_sv_seq")
	private Long id;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_usuario")
	private Usuario usuario;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_cadastro")
	private Calendar dataCadastro;
	
	@ManyToOne
	@JoinColumn(name="id_formin")
	private FormIN formIN;
	
	@ManyToOne
	@JoinColumn(name="id_formcom")
	private FormCOM formCOM;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_investigacao")
	private Calendar dataInvestigacao;
	
	@OneToMany(mappedBy="formSV", cascade={CascadeType.ALL}, orphanRemoval=true)
	private List<AvaliacaoClinicaSV> avaliacoesClinicas = new ArrayList<AvaliacaoClinicaSV>();
	
	@Enumerated(EnumType.STRING)
	@Column(name="aplica_se_identificacao_estab")
	private SimNao aplica_seIdentificacaoDeEstabelecimentos;
	
	@OneToMany(mappedBy="formSV", cascade={CascadeType.ALL}, orphanRemoval=true)
	private List<NovoEstabelecimentoParaInvestigacao> novoEstabelecimentoParaInvestigacaos = new ArrayList<NovoEstabelecimentoParaInvestigacao>();
	
	@Enumerated(EnumType.STRING)
	@Column(name="estab_possui_vet_permanente")
	private SimNao estabelecimentoPossuiAssistenciaVeterinariaPermanente;
	
	@OneToOne(cascade={CascadeType.ALL}, orphanRemoval=true)
	@JoinColumn(name="id_veterinario_assistente")
	private br.gov.mt.indea.sistemaformin.webservice.entity.Veterinario veterinarioAssistenteDoEstabelecimento;
	
	@OneToOne(cascade={CascadeType.ALL}, orphanRemoval=true)
	@JoinColumn(name="id_veterinario_atedimento")
	private br.gov.mt.indea.sistemaformin.webservice.entity.Veterinario veterinarioResponsavelPeloAtendimento;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Calendar getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Calendar dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public FormIN getFormIN() {
		return formIN;
	}

	public void setFormIN(FormIN formIN) {
		this.formIN = formIN;
	}

	public FormCOM getFormCOM() {
		return formCOM;
	}

	public void setFormCOM(FormCOM formCOM) {
		this.formCOM = formCOM;
	}

	public Calendar getDataInvestigacao() {
		return dataInvestigacao;
	}

	public void setDataInvestigacao(Calendar dataInvestigacao) {
		this.dataInvestigacao = dataInvestigacao;
	}

	public List<AvaliacaoClinicaSV> getAvaliacoesClinicas() {
		return avaliacoesClinicas;
	}

	public void setAvaliacoesClinicas(List<AvaliacaoClinicaSV> avaliacoesClinicas) {
		this.avaliacoesClinicas = avaliacoesClinicas;
	}

	public br.gov.mt.indea.sistemaformin.webservice.entity.Veterinario getVeterinarioResponsavelPeloAtendimento() {
		return veterinarioResponsavelPeloAtendimento;
	}

	public SimNao getAplica_seIdentificacaoDeEstabelecimentos() {
		return aplica_seIdentificacaoDeEstabelecimentos;
	}

	public void setAplica_seIdentificacaoDeEstabelecimentos(
			SimNao aplica_seIdentificacaoDeEstabelecimentos) {
		this.aplica_seIdentificacaoDeEstabelecimentos = aplica_seIdentificacaoDeEstabelecimentos;
	}

	public List<NovoEstabelecimentoParaInvestigacao> getNovoEstabelecimentoParaInvestigacaos() {
		return novoEstabelecimentoParaInvestigacaos;
	}

	public void setNovoEstabelecimentoParaInvestigacaos(
			List<NovoEstabelecimentoParaInvestigacao> novoEstabelecimentoParaInvestigacaos) {
		this.novoEstabelecimentoParaInvestigacaos = novoEstabelecimentoParaInvestigacaos;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	public br.gov.mt.indea.sistemaformin.webservice.entity.Veterinario getVeterinarioAssistenteDoEstabelecimento() {
		return veterinarioAssistenteDoEstabelecimento;
	}

	public void setVeterinarioAssistenteDoEstabelecimento(
			br.gov.mt.indea.sistemaformin.webservice.entity.Veterinario veterinarioAssistenteDoEstabelecimento) {
		this.veterinarioAssistenteDoEstabelecimento = veterinarioAssistenteDoEstabelecimento;
	}

	public void setVeterinarioResponsavelPeloAtendimento(
			br.gov.mt.indea.sistemaformin.webservice.entity.Veterinario veterinarioResponsavelPeloAtendimento) {
		this.veterinarioResponsavelPeloAtendimento = veterinarioResponsavelPeloAtendimento;
	}

	public SimNao getEstabelecimentoPossuiAssistenciaVeterinariaPermanente() {
		return estabelecimentoPossuiAssistenciaVeterinariaPermanente;
	}

	public void setEstabelecimentoPossuiAssistenciaVeterinariaPermanente(
			SimNao estabelecimentoPossuiAssistenciaVeterinariaPermanente) {
		this.estabelecimentoPossuiAssistenciaVeterinariaPermanente = estabelecimentoPossuiAssistenciaVeterinariaPermanente;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FormSV other = (FormSV) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}