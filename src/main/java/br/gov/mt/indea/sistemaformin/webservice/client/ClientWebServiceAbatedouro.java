package br.gov.mt.indea.sistemaformin.webservice.client;

import java.io.Serializable;
import java.net.URISyntaxException;

import javax.inject.Inject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;

import br.gov.mt.indea.sistemaformin.exception.ApplicationException;
import br.gov.mt.indea.sistemaformin.webservice.convert.AbatedouroConverter;
import br.gov.mt.indea.sistemaformin.webservice.model.Abatedouro;


public class ClientWebServiceAbatedouro implements Serializable{
	
	private static final long serialVersionUID = -6524521505872165758L;

	protected String URL_WS = "https://sistemas.indea.mt.gov.br/FronteiraWeb/ws/abatedouro/";
	
	@Inject
	private AbatedouroConverter abatedouroConverter;
	
	public br.gov.mt.indea.sistemaformin.webservice.entity.Abatedouro getAbatedouroByCNPJ(String cnpj) throws ApplicationException {

		String[] resposta = null;
		try {
			System.out.println(URL_WS + "cnpj/" + cnpj.replaceAll("[^\\d]", ""));
			resposta = new WebServiceCliente().get(URL_WS + "cnpj/" + cnpj.replaceAll("[^\\d]", ""));
		} catch (URISyntaxException e) {
			throw new ApplicationException("Erro ao buscar a abatedouro");
		}
		
		if (resposta == null || resposta[0] == null){
			return null;
		}
			
		if (resposta[0].equals("200")) {
			GsonBuilder gsonBuilder = new GsonBuilder().setDateFormat("dd/MM/yyyy");
			Gson gson = gsonBuilder.create();
			
			JsonParser parser = new JsonParser();
			JsonArray array = parser.parse(resposta[1]).getAsJsonArray();
			
			if (array.size() == 0)
				return null;
			
			br.gov.mt.indea.sistemaformin.webservice.entity.Abatedouro abatedouro = null;
			
			if (array.size() > 0)
				abatedouro = abatedouroConverter.fromModelToEntity(gson.fromJson(array.get(0), Abatedouro.class));
			
			return abatedouro;
		} else if (resposta[0].equals("404")) {
			throw new ApplicationException("Webservice com problemas. Contate o administrador do sistema");
		} else if (resposta[0].equals("500")) {
			throw new ApplicationException("Webservice com problemas. Contate o administrador do sistema");
		} else if (resposta[0].equals("400")) {
			throw new ApplicationException("Webservice com problemas. Contate o administrador do sistema");
		} else{
			throw new ApplicationException("Webservice inativo. Tente novamente em alguns minutos. Erro: " + resposta[0]);
		} 
	}
	
}
