package br.gov.mt.indea.sistemaformin.entity.dto;

import java.io.Serializable;

import br.gov.mt.indea.sistemaformin.entity.Municipio;
import br.gov.mt.indea.sistemaformin.entity.UF;

public class LaboratorioDTO implements AbstractDTO, Serializable{
	
	private static final long serialVersionUID = 6205763755360605521L;

	private UF uf = UF.MATO_GROSSO;
	
	private Municipio municipio;
	
    private String nome;
	
	private String cnpj;
	
	public boolean isNull(){
		if (uf == null && municipio == null && nome == null && cnpj == null)
			return true;
		return false;
	}
	
	public UF getUf() {
		return uf;
	}

	public void setUf(UF uf) {
		this.uf = uf;
	}

	public Municipio getMunicipio() {
		return municipio;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}

}