package br.gov.mt.indea.sistemaformin.managedbeans;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.constraints.Past;

import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLBeanName;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;

import br.gov.mt.indea.sistemaformin.entity.AvaliacaoClinicaEQ;
import br.gov.mt.indea.sistemaformin.entity.FormCOM;
import br.gov.mt.indea.sistemaformin.entity.FormEQ;
import br.gov.mt.indea.sistemaformin.entity.FormIN;
import br.gov.mt.indea.sistemaformin.entity.Municipio;
import br.gov.mt.indea.sistemaformin.entity.NovoEstabelecimentoParaInvestigacao;
import br.gov.mt.indea.sistemaformin.entity.UF;
import br.gov.mt.indea.sistemaformin.entity.VacinacaoEQ;
import br.gov.mt.indea.sistemaformin.enums.Dominio.OrigemDoAnimal;
import br.gov.mt.indea.sistemaformin.enums.Dominio.SimNao;
import br.gov.mt.indea.sistemaformin.exception.ApplicationErrorException;
import br.gov.mt.indea.sistemaformin.exception.ApplicationException;
import br.gov.mt.indea.sistemaformin.service.FormCOMService;
import br.gov.mt.indea.sistemaformin.service.FormEQService;
import br.gov.mt.indea.sistemaformin.service.FormINService;
import br.gov.mt.indea.sistemaformin.service.MunicipioService;
import br.gov.mt.indea.sistemaformin.service.PropriedadeService;
import br.gov.mt.indea.sistemaformin.util.FacesMessageUtil;
import br.gov.mt.indea.sistemaformin.webservice.client.ClientWebServiceVeterinario;
import br.gov.mt.indea.sistemaformin.webservice.entity.Endereco;
import br.gov.mt.indea.sistemaformin.webservice.entity.Propriedade;
import br.gov.mt.indea.sistemaformin.webservice.entity.Veterinario;

@Named
@ViewScoped
@URLBeanName("formEQManagedBean")
@URLMappings(mappings = {
		@URLMapping(id = "pesquisarFormEQ", pattern = "/formIN/#{formEQManagedBean.idFormIN}/formEQ/pesquisar", viewId = "/pages/formEQ/lista.jsf"),
		@URLMapping(id = "incluirFormEQ", pattern = "/formIN/#{formEQManagedBean.idFormIN}/formEQ/incluir", viewId = "/pages/formEQ/novo.jsf"),
		@URLMapping(id = "alterarFormEQ", pattern = "/formIN/#{formEQManagedBean.idFormIN}/formEQ/alterar/#{formEQManagedBean.id}", viewId = "/pages/formEQ/novo.jsf"),
		@URLMapping(id = "impressaoFormEQ", pattern = "/formIN/#{formEQManagedBean.idFormIN}/formEQ/impressao/#{formEQManagedBean.id}", viewId = "/pages/formEQ/novo.jsf")})
public class FormEQManagedBean implements Serializable {

	private static final long serialVersionUID = 1760023349204758559L;

	private Long id;
	
	private Long idFormIN;
	
	private FormIN formIN;
	
	@Inject
	private FormINService formINService;
	
	@Inject
	private FormEQService formEQService;
	
	@Inject
	private FormCOMService formCOMService;
	
	@Inject
	private MunicipioService municipioService;
	
	@Inject
	private FormEQ formEQ;
	
	private AvaliacaoClinicaEQ avaliacaoClinicaEQ = new AvaliacaoClinicaEQ();
	
	private VacinacaoEQ vacinacaoEQ = new VacinacaoEQ();
	
	private boolean editandoAvaliacao = false;
	
	private boolean visualizandoAvaliacao = false;
	
	private boolean editandoVacinacaoEQ = false;
	
	private boolean visualizandoVacinacaoEQ = false;
	
	@Past(message="Data precisa ser menor ou igual a data atual")
	private Date data;
	
	@Past(message="Data precisa ser menor ou igual a data atual")
	private Date dataVacinacao;
	
	private boolean editandoNovoEstabelecimento = false;
	
	private boolean visualizandoNovoEstabelecimento = false;
	
	private Long idPropriedade;

	private List<Propriedade> listaPropriedades;
	
	private NovoEstabelecimentoParaInvestigacao novoEstabelecimentoParaInvestigacao = new NovoEstabelecimentoParaInvestigacao();
	
	private Propriedade propriedade;
	
	private boolean forceUpdatePropriedade = false;
	
	@Inject
	private Municipio municipio;
	
	private UF uf;
	
	private String cpfVeterinario;

	private String crmvVeterinario;

	private List<Veterinario> listaVeterinarios;
	
	@Inject
	private PropriedadeService propriedadeService;
	
	@Inject
	private ClientWebServiceVeterinario clientWebServiceVeterinario;
	
	public List<Municipio> getListaMunicipios(){
		if (this.uf == null)
			return null;
		
		return municipioService.findAllByUF(this.uf);
	}
	
	@PostConstruct
	private void init(){
		
	}
	
	private void carregarFormIN(){
		this.formIN = formINService.findByIdFetchPropriedade(this.idFormIN);
	}
	
	private void limpar() {
		this.formEQ = new FormEQ();
		this.data = null;
	}
	
	@URLAction(mappingId = "incluirFormEQ", onPostback = false)
	public void novo(){
		this.limpar();
		this.carregarFormIN();
		
		this.formEQ.setFormIN(formIN);
	}
	
//	public String novo(FormCOM formCOM){
//		this.iniciarConversacao();
//		this.formEQ.setFormCOM(formCOM);
//		this.formEQ.setFormIN(formCOM.getFormIN());
//
//		return "/pages/formEQ/novo.xhtml";
//	}
	
	@URLAction(mappingId = "alterarFormEQ", onPostback = false)
	public void editar(){
		this.formEQ = formEQService.findByIdFetchAll(this.getId());
			
		if (this.formEQ.getData() != null)
			this.data = this.formEQ.getData().getTime();
	}
	
	public String adicionar() throws IOException{
		
		if (this.data != null){
			if (this.formEQ.getData() == null)
				this.formEQ.setData(Calendar.getInstance());
			this.formEQ.getData().setTime(data);
		}else
			this.formEQ.setData(null);
		
		if (this.formEQ.getVeterinarioResponsavelPeloAtendimento() == null){
			FacesContext context = FacesContext.getCurrentInstance();
			
			context.addMessage("cadastro:fieldVeterinario:campoVeterinario", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Veterin�rio: valor � obrigat�rio.", "Veterin�rio: valor � obrigat�rio."));
			return "";
		}
		
		if (formEQ.getId() != null){
			this.formEQService.saveOrUpdate(formEQ);
			FacesMessageUtil.addInfoContextFacesMessage("Form EQ atualizado", "");
		}else{
			this.formEQ.setDataCadastro(Calendar.getInstance());
			this.formEQService.saveOrUpdate(formEQ);
			FacesMessageUtil.addInfoContextFacesMessage("Form EQ adicionado", "");
		}
		
		limpar();
	    return "pretty:visaoGeralFormIN";
	}
	
	@Inject
	private VisualizadorImpressaoManagedBean visualizadorImpressaoManagedBean;
	
	@URLAction(mappingId = "impressaoFormEQ", onPostback = false)
	public void imprimir() throws ApplicationErrorException{
		this.formEQ = formEQService.findByIdFetchAll(this.getId());
		
		visualizadorImpressaoManagedBean.exibirFormEQ(formEQ);
	}
	
	public void remover(FormEQ formEQ){
		this.formEQService.delete(formEQ);
		FacesMessageUtil.addInfoContextFacesMessage("Form EQ exclu�do com sucesso", "");
	}
	
	public List<FormCOM> getListaFormCOM(){
		return formCOMService.findAllBy(this.formEQ.getFormIN());
	}
	
	public void adicionarAvaliacaoClinicaEQ(){
		this.avaliacaoClinicaEQ.setFormEQ(formEQ);
		
		if (!editandoAvaliacao)
			this.formEQ.getAvaliacoesClinicas().add(this.avaliacaoClinicaEQ);
		else
			editandoAvaliacao = false;
		
		if (this.avaliacaoClinicaEQ.getIdade() == 0)
			this.avaliacaoClinicaEQ.setIdade(null);
		
		this.avaliacaoClinicaEQ = new AvaliacaoClinicaEQ();
		FacesMessageUtil.addInfoContextFacesMessage("Avalia��o cl�nica inclu�do com sucesso", "");
	}
	
	public void novaAvaliacao(){
		this.avaliacaoClinicaEQ = new AvaliacaoClinicaEQ();
		this.editandoAvaliacao = false;
		this.visualizandoAvaliacao = false;
	}
	
	public void removerAvaliacaoClinicaEQ(AvaliacaoClinicaEQ avaliacaoClinicaEQ){
		this.formEQ.getAvaliacoesClinicas().remove(avaliacaoClinicaEQ);
		
		FacesMessageUtil.addInfoContextFacesMessage("Avalia��o cl�nica removida com sucesso", "");
	}
	
	public void editarAvaliacao(AvaliacaoClinicaEQ avaliacaoClinicaEQ){
		this.editandoAvaliacao = true;
		this.visualizandoAvaliacao = false;
		this.avaliacaoClinicaEQ = avaliacaoClinicaEQ;
	}
	
	public void visualizarAvaliacao(AvaliacaoClinicaEQ avaliacaoClinicaEQ){
		this.visualizandoAvaliacao = true;
		this.editandoAvaliacao = false;
		this.avaliacaoClinicaEQ = avaliacaoClinicaEQ;
	}
	
	public void adicionarVacinacaoEQ(){
		this.vacinacaoEQ.setAvaliacaoClinicaEQ(this.avaliacaoClinicaEQ);
		
		if (this.dataVacinacao != null){
			if (this.vacinacaoEQ.getData() == null)
				this.vacinacaoEQ.setData(Calendar.getInstance());
			this.vacinacaoEQ.getData().setTime(dataVacinacao);
		}else
			this.vacinacaoEQ.setData(null);
		this.dataVacinacao = null;
		
		if (!editandoVacinacaoEQ)
			this.avaliacaoClinicaEQ.getVacinacoesEQ().add(this.vacinacaoEQ);
		else
			editandoVacinacaoEQ = false;
		
		this.vacinacaoEQ = new VacinacaoEQ();
		this.dataVacinacao = null;
		FacesMessageUtil.addInfoContextFacesMessage("Avalia��o cl�nica inclu�da com sucesso", "");
	}
	
	public void novaVacinacaoEQ(){
		this.vacinacaoEQ = new VacinacaoEQ();
		this.editandoVacinacaoEQ = false;
		this.visualizandoVacinacaoEQ = false;
	}
	
	public void removerVacinacaoEQ(VacinacaoEQ vacinacaoEQ){
		this.avaliacaoClinicaEQ.getVacinacoesEQ().remove(vacinacaoEQ);
		
		FacesMessageUtil.addInfoContextFacesMessage("Vacina��o removida com sucesso", "");
	}
	
	public void editarVacinacaoEQ(VacinacaoEQ vacinacaoEQ){
		this.editandoVacinacaoEQ = true;
		this.visualizandoVacinacaoEQ = false;
		this.vacinacaoEQ = vacinacaoEQ;
		
		if (vacinacaoEQ.getData() != null)
			this.dataVacinacao = vacinacaoEQ.getData().getTime();
	}
	
	public void visualizarVacinacaoEQ(VacinacaoEQ vacinacaoEQ){
		this.visualizandoVacinacaoEQ = true;
		this.editandoVacinacaoEQ = false;
		this.vacinacaoEQ = vacinacaoEQ;
	}
	
	public boolean isAnimalOriundoDeOutroEstado(){
		boolean resultado = false;
		
		if (this.avaliacaoClinicaEQ.getOrigemDoAnimal() == null)
			return resultado;
		else
			resultado = this.avaliacaoClinicaEQ.getOrigemDoAnimal().equals(OrigemDoAnimal.ORIUNDO_DE_OUTRO_ESTADO);
		
		if (!resultado)
			this.avaliacaoClinicaEQ.setUfDeOrigem(null);
		
		return resultado;
	}
	
	public void adicionarNovoEstabelecimentoParaInvestigacao(){
		boolean isNovoEstabelecimentoOk = true;
		
		FacesContext context = FacesContext.getCurrentInstance();
		
		if (this.novoEstabelecimentoParaInvestigacao.getPropriedade() == null){
			context.addMessage("formModalNovoEstabelecimento:fieldPropriedade:campoPropriedade", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Propriedade: valor � obrigat�rio.", "Propriedade: valor � obrigat�rio."));
			isNovoEstabelecimentoOk = false;
		}
		
		if (!isNovoEstabelecimentoOk){
			return;
		}
		
		this.novoEstabelecimentoParaInvestigacao.setFormEQ(this.formEQ);
		
		if (!editandoNovoEstabelecimento)
			this.formEQ.getNovoEstabelecimentoParaInvestigacaos().add(novoEstabelecimentoParaInvestigacao);
		else
			editandoNovoEstabelecimento = false;
		
		this.novoEstabelecimentoParaInvestigacao = new NovoEstabelecimentoParaInvestigacao();
		FacesMessageUtil.addInfoContextFacesMessage("Estabelecimento inclu�do com sucesso", "");
	}
	
	public void novaNovoEstabelecimentoParaInvestigacao(){
		this.novoEstabelecimentoParaInvestigacao = new NovoEstabelecimentoParaInvestigacao();
		this.editandoNovoEstabelecimento = false;
		this.visualizandoNovoEstabelecimento = false;
	}
	
	public void removerNovoEstabelecimentoParaInvestigacao(NovoEstabelecimentoParaInvestigacao novoEstabelecimento){
		this.formEQ.getNovoEstabelecimentoParaInvestigacaos().remove(novoEstabelecimento);
	}
	
	public void editarNovoEstabelecimentoParaInvestigacao(NovoEstabelecimentoParaInvestigacao novoEstabelecimento){
		this.editandoNovoEstabelecimento = true;
		this.visualizandoNovoEstabelecimento = false;
		this.novoEstabelecimentoParaInvestigacao = novoEstabelecimento;
	}
	
	public void visualizarNovoEstabelecimentoParaInvestigacao(NovoEstabelecimentoParaInvestigacao novoEstabelecimento){
		this.visualizandoNovoEstabelecimento = true;
		this.editandoNovoEstabelecimento = false;
		this.novoEstabelecimentoParaInvestigacao = novoEstabelecimento;
	}
	
	public void abrirTelaDeBuscaDePropriedade(){
		this.idPropriedade = null;
		this.listaPropriedades = null;
	}
	
	public void buscarPropriedade(){
		this.listaPropriedades = null;
		
		Propriedade p;
		try {
			p = propriedadeService.findByCodigoFetchAll(idPropriedade, forceUpdatePropriedade);
			if (p != null){
				this.listaPropriedades = new ArrayList<Propriedade>();
				listaPropriedades.add(p);
			}
		} catch (ApplicationException e) {
			FacesMessageUtil.addErrorContextFacesMessage("Erro", e.getLocalizedMessage());
			return;
		}
		
		if (this.listaPropriedades == null || this.listaPropriedades.isEmpty())
			FacesMessageUtil.addWarnContextFacesMessage("Nenhuma propriedade foi encontrada", "");
	}
	
	public void selecionarPropriedade_NovoEstab(Propriedade propriedade){
		this.novoEstabelecimentoParaInvestigacao.setPropriedade(propriedade);
		FacesMessageUtil.addInfoContextFacesMessage("Propriedade " + this.novoEstabelecimentoParaInvestigacao.getPropriedade().getNome() + " selecionada", "");
	}
	
	public void abrirTelaDeCadastroDePropriedade(){
		this.propriedade = new Propriedade();
	}
	
	public void cadastrarPropriedade_NovoEstab(){
		this.propriedade.setMunicipio(this.municipio);
		this.novoEstabelecimentoParaInvestigacao.setPropriedade(this.propriedade);
		this.propriedade = null;
		this.municipio = null;
		this.uf = null;
		
		FacesMessageUtil.addInfoContextFacesMessage("Propriedade inclu�da com sucesso", "");
	}
	
	public void abrirTelaDeBuscaDeVeterinario(){
		this.cpfVeterinario = null;
		this.crmvVeterinario = null;
		this.listaVeterinarios = null;
	}
	
	public void buscarVeterinarioPorCpf(){
		this.listaVeterinarios = null;
		
		Veterinario v;
		try {
			v = clientWebServiceVeterinario.getVeterinarioByCpf(cpfVeterinario);
			
			if (v != null){
				this.listaVeterinarios = new ArrayList<Veterinario>();
				this.listaVeterinarios.add(v);
			}
		} catch (ApplicationException e) {
			FacesMessageUtil.addErrorContextFacesMessage("Erro", e.getLocalizedMessage());
			return;
		}
		
		if (this.listaVeterinarios == null || this.listaVeterinarios.isEmpty())
			FacesMessageUtil.addWarnContextFacesMessage("Nenhum veterin�rio foi encontrado", "");
	}
	
	public void buscarVeterinarioPorCrmv(){
		this.listaVeterinarios = null;
		
		Veterinario v;
		try {
			v = clientWebServiceVeterinario.getVeterinarioByCRMV(crmvVeterinario);
			
			if (v != null){
				this.listaVeterinarios = new ArrayList<Veterinario>();
				this.listaVeterinarios.add(v);
			}
		} catch (ApplicationException e) {
			FacesMessageUtil.addErrorContextFacesMessage("Erro", e.getLocalizedMessage());
			return;
		}
		
		if (this.listaVeterinarios == null || this.listaVeterinarios.isEmpty())
			FacesMessageUtil.addWarnContextFacesMessage("Nenhum veterin�rio foi encontrado", "");
	}
	
	public void selecionarVeterinario(Veterinario veterinario){
		this.formEQ.setVeterinarioResponsavelPeloAtendimento(veterinario);
		FacesMessageUtil.addInfoContextFacesMessage("Veterinario " + this.formEQ.getVeterinarioResponsavelPeloAtendimento().getNome() + " selecionado", "");
	}
	
	public boolean isEstabelecimentoPossuiAssistenciaVeterinariaPermanente(){
		boolean resultado = false;
		if (this.formEQ.getEstabelecimentoPossuiAssistenciaVeterinariaPermanente() == null)
			return resultado;
		else{
			resultado = this.formEQ.getEstabelecimentoPossuiAssistenciaVeterinariaPermanente().equals(SimNao.SIM);
		}
		
		if (resultado){
			if (this.formEQ.getVeterinarioAssistenteDoEstabelecimento() == null){
				this.formEQ.setVeterinarioAssistenteDoEstabelecimento(new Veterinario());
				this.formEQ.getVeterinarioAssistenteDoEstabelecimento().setEndereco(new Endereco());
			}
		}else
			this.formEQ.setVeterinarioAssistenteDoEstabelecimento(null);
		
		return resultado;
	}
	
	public FormEQ getFormEQ() {
		return formEQ;
	}

	public void setFormEQ(FormEQ formEQ) {
		this.formEQ = formEQ;
	}

	public AvaliacaoClinicaEQ getAvaliacaoClinicaEQ() {
		return avaliacaoClinicaEQ;
	}

	public void setAvaliacaoClinicaEQ(AvaliacaoClinicaEQ avaliacaoClinicaEQ) {
		this.avaliacaoClinicaEQ = avaliacaoClinicaEQ;
	}

	public VacinacaoEQ getVacinacaoEQ() {
		return vacinacaoEQ;
	}

	public void setVacinacaoEQ(VacinacaoEQ vacinacaoEQ) {
		this.vacinacaoEQ = vacinacaoEQ;
	}

	public boolean isVisualizandoAvaliacao() {
		return visualizandoAvaliacao;
	}
	
	public boolean isVisualizandoVacinacaoEQ() {
		return visualizandoVacinacaoEQ;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public Date getDataVacinacao() {
		return dataVacinacao;
	}

	public void setDataVacinacao(Date dataVacinacao) {
		this.dataVacinacao = dataVacinacao;
	}

	public Long getIdPropriedade() {
		return idPropriedade;
	}

	public void setIdPropriedade(Long idPropriedade) {
		this.idPropriedade = idPropriedade;
	}

	public List<Propriedade> getListaPropriedades() {
		return listaPropriedades;
	}

	public void setListaPropriedades(List<Propriedade> listaPropriedades) {
		this.listaPropriedades = listaPropriedades;
	}

	public boolean isEditandoNovoEstabelecimento() {
		return editandoNovoEstabelecimento;
	}

	public void setEditandoNovoEstabelecimento(boolean editandoNovoEstabelecimento) {
		this.editandoNovoEstabelecimento = editandoNovoEstabelecimento;
	}

	public boolean isVisualizandoNovoEstabelecimento() {
		return visualizandoNovoEstabelecimento;
	}

	public void setVisualizandoNovoEstabelecimento(
			boolean visualizandoNovoEstabelecimento) {
		this.visualizandoNovoEstabelecimento = visualizandoNovoEstabelecimento;
	}

	public NovoEstabelecimentoParaInvestigacao getNovoEstabelecimentoParaInvestigacao() {
		return novoEstabelecimentoParaInvestigacao;
	}

	public void setNovoEstabelecimentoParaInvestigacao(
			NovoEstabelecimentoParaInvestigacao novoEstabelecimentoParaInvestigacao) {
		this.novoEstabelecimentoParaInvestigacao = novoEstabelecimentoParaInvestigacao;
	}

	public Propriedade getPropriedade() {
		return propriedade;
	}

	public void setPropriedade(Propriedade propriedade) {
		this.propriedade = propriedade;
	}

	public String getCpfVeterinario() {
		return cpfVeterinario;
	}

	public void setCpfVeterinario(String cpfVeterinario) {
		this.cpfVeterinario = cpfVeterinario;
	}

	public String getCrmvVeterinario() {
		return crmvVeterinario;
	}

	public void setCrmvVeterinario(String crmvVeterinario) {
		this.crmvVeterinario = crmvVeterinario;
	}

	public List<Veterinario> getListaVeterinarios() {
		return listaVeterinarios;
	}

	public void setListaVeterinarios(List<Veterinario> listaVeterinarios) {
		this.listaVeterinarios = listaVeterinarios;
	}

	public boolean isEditandoAvaliacao() {
		return editandoAvaliacao;
	}

	public void setEditandoAvaliacao(boolean editandoAvaliacao) {
		this.editandoAvaliacao = editandoAvaliacao;
	}

	public Municipio getMunicipio() {
		return municipio;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}

	public UF getUf() {
		return uf;
	}

	public void setUf(UF uf) {
		this.uf = uf;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdFormIN() {
		return idFormIN;
	}

	public void setIdFormIN(Long idFormIN) {
		this.idFormIN = idFormIN;
	}

	public FormIN getFormIN() {
		return formIN;
	}

	public void setFormIN(FormIN formIN) {
		this.formIN = formIN;
	}

	public boolean isForceUpdatePropriedade() {
		return forceUpdatePropriedade;
	}

	public void setForceUpdatePropriedade(boolean forceUpdatePropriedade) {
		this.forceUpdatePropriedade = forceUpdatePropriedade;
	}

}
