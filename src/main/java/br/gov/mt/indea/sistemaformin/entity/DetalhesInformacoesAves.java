package br.gov.mt.indea.sistemaformin.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.envers.Audited;

@Audited
@Entity
@DiscriminatorValue("aves")
public class DetalhesInformacoesAves extends AbstractDetalhesInformacoesDeAnimais implements Cloneable{
	
	private static final long serialVersionUID = 1355044082455412390L;

	@ManyToOne
	@JoinColumn(name="id_informacoes_aves")
	private InformacoesAves informacoesAves;

	public InformacoesAves getInformacoesAves() {
		return informacoesAves;
	}

	public void setInformacoesAves(InformacoesAves informacoesAves) {
		this.informacoesAves = informacoesAves;
	}

	protected Object clone(InformacoesAves informacoesAves) throws CloneNotSupportedException{
		DetalhesInformacoesAves cloned = (DetalhesInformacoesAves) super.clone();
		
		cloned.setId(null);
		cloned.setInformacoesAves(informacoesAves);
		
		return cloned;
	}

}