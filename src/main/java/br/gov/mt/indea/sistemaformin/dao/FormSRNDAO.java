package br.gov.mt.indea.sistemaformin.dao;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.inject.Inject;


import br.gov.mt.indea.sistemaformin.entity.FormSRN;
import br.gov.mt.indea.sistemaformin.exception.ApplicationException;

@Stateless
public class FormSRNDAO extends DAO<FormSRN> {
	
	@Inject
	private EntityManager em;
	
	public FormSRNDAO() {
		super(FormSRN.class);
	}

	@Override
	public EntityManager getEntityManager() {
		return this.em;
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void add(FormSRN formSRN) throws ApplicationException{
		formSRN = this.getEntityManager().merge(formSRN);
		if (formSRN.getFormCOM() != null)
			formSRN.setFormCOM(this.getEntityManager().merge(formSRN.getFormCOM()));
		super.add(formSRN);
	}

}
