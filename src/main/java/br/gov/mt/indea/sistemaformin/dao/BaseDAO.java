package br.gov.mt.indea.sistemaformin.dao;

import javax.persistence.EntityManager;


public interface BaseDAO {

	public EntityManager getEntityManager();
}
