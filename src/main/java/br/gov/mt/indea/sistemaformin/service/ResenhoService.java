package br.gov.mt.indea.sistemaformin.service;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.mt.indea.sistemaformin.entity.Resenho;
import br.gov.mt.indea.sistemaformin.webservice.entity.Exploracao;
import br.gov.mt.indea.sistemaformin.webservice.entity.Produtor;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class ResenhoService extends PaginableService<Resenho, Long> {
	
	private static final Logger log = LoggerFactory.getLogger(ResenhoService.class);

	protected ResenhoService() {
		super(Resenho.class);
	}
	
	public Resenho findByIdFetchAll(Long id){
		Resenho resenho;
		
		StringBuilder sql = new StringBuilder();
		sql.append("  from Resenho resenho ")
		   .append("  join fetch resenho.formIN formin")
		   .append("  join fetch formin.propriedade propriedade")
		   .append("  left join fetch propriedade.coordenadaGeografica ")
		   .append("  left join fetch formin.proprietario proprietario")
		   .append("  left join fetch proprietario.endereco endprop")
		   .append("  left join fetch endprop.municipio munprop")
		   .append("  left join fetch munprop.uf")
		   .append("  join fetch propriedade.municipio ")
		   .append("  join fetch propriedade.ule ")
		   .append("  left join fetch propriedade.endereco ")
		   
		   .append("  join fetch resenho.veterinario vet")
		   .append("  join fetch vet.conselho")
		   .append("  join fetch vet.tipoEmitente")
		   .append("  left join fetch vet.ule uu")
		   .append("  left join fetch uu.urs")
		   .append("  left join fetch vet.endereco")
		   .append(" where resenho.id = :id ");
		
		Query query = getSession().createQuery(sql.toString()).setLong("id", id);
		resenho = (Resenho) query.uniqueResult();
		
		if (resenho != null){
		
			Hibernate.initialize(resenho.getFormIN().getPropriedade().getProprietarios());
			Hibernate.initialize(resenho.getFormIN().getPropriedade().getUle());
			Hibernate.initialize(resenho.getFormIN().getPropriedade().getExploracaos());
			
			for (Exploracao exploracao : resenho.getFormIN().getPropriedade().getExploracaos()){
				Hibernate.initialize(exploracao.getProdutores());
				
				for (Produtor produtor : exploracao.getProdutores()) {
					Hibernate.initialize(produtor.getMunicipioNascimento());
					Hibernate.initialize(produtor.getEndereco());
					Hibernate.initialize(produtor.getEndereco().getMunicipio());
					Hibernate.initialize(produtor.getEndereco().getMunicipio().getUf());
				}
			}
			
			for (Produtor produtor : resenho.getFormIN().getPropriedade().getProprietarios()) {
				Hibernate.initialize(produtor.getMunicipioNascimento());
				Hibernate.initialize(produtor.getEndereco());
				Hibernate.initialize(produtor.getEndereco().getMunicipio());
				Hibernate.initialize(produtor.getEndereco().getMunicipio().getUf());
			}
			
		}
		
		return resenho;
	}
	
	@Override
	public void saveOrUpdate(Resenho resenho) {
		super.saveOrUpdate(resenho);
		
		log.info("Salvando Form SN {}", resenho.getId());
	}
	
	@Override
	public void delete(Resenho resenho) {
		super.delete(resenho);
		
		log.info("Removendo Form SN {}", resenho.getId());
	}
	
	@Override
	public void validar(Resenho Resenho) {

	}

	@Override
	public void validarPersist(Resenho Resenho) {

	}

	@Override
	public void validarMerge(Resenho Resenho) {

	}

	@Override
	public void validarDelete(Resenho Resenho) {

	}

}
