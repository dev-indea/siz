package br.gov.mt.indea.sistemaformin.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;


@Audited
@Entity
@Table(name="achado_lote_enferm_abat_frig")
public class AchadosLoteEnfermidadeAbatedouroFrigorifico extends BaseEntity<Long>{

	private static final long serialVersionUID = 7668583152774414479L;

	@Id
	@SequenceGenerator(name="achado_lote_enferm_abat_frig_seq", sequenceName="achado_lote_enferm_abat_frig_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="achado_lote_enferm_abat_frig_seq")
	private Long id;
	
	@ManyToOne
	@JoinColumn(name="id_lote_enfermidade_abat_frig")
	private LoteEnfermidadeAbatedouroFrigorifico loteEnfermidadeAbatedouroFrigorifico;
	
	@OneToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="id_doenca_enf_abat_frig")
	private DoencaEnfermidadeAbatedouroFrigorifico doencaEnfermidadeAbatedouroFrigorifico;
	
	@Column(name="houve_formulario_colheita")
	private boolean houveFormularioColheita = false;
	
	@OneToMany(mappedBy="achadosLoteEnfermidadeAbatedouroFrigorifico", orphanRemoval=true, cascade={CascadeType.ALL}, fetch=FetchType.LAZY)
	private List<FormularioDeColheitaTroncoEncefalico> listaFormularioDeColheitaTroncoEncefalico = new ArrayList<FormularioDeColheitaTroncoEncefalico>();

	@Column(name="quantidade_casos")
	private Long quantidadeCasos;
	
	public AchadosLoteEnfermidadeAbatedouroFrigorifico(LoteEnfermidadeAbatedouroFrigorifico loteEnfermidadeAbatedouroFrigorifico, DoencaEnfermidadeAbatedouroFrigorifico doencaEnfermidadeAbatedouroFrigorifico, Long quantidadeCasos) {
		this.loteEnfermidadeAbatedouroFrigorifico = loteEnfermidadeAbatedouroFrigorifico;
		this.doencaEnfermidadeAbatedouroFrigorifico = doencaEnfermidadeAbatedouroFrigorifico;
		this.quantidadeCasos = quantidadeCasos;
	}
	
	public AchadosLoteEnfermidadeAbatedouroFrigorifico(){}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LoteEnfermidadeAbatedouroFrigorifico getLoteEnfermidadeAbatedouroFrigorifico() {
		return loteEnfermidadeAbatedouroFrigorifico;
	}

	public void setLoteEnfermidadeAbatedouroFrigorifico(
			LoteEnfermidadeAbatedouroFrigorifico loteEnfermidadeAbatedouroFrigorifico) {
		this.loteEnfermidadeAbatedouroFrigorifico = loteEnfermidadeAbatedouroFrigorifico;
	}

	public DoencaEnfermidadeAbatedouroFrigorifico getDoencaEnfermidadeAbatedouroFrigorifico() {
		return doencaEnfermidadeAbatedouroFrigorifico;
	}

	public void setDoencaEnfermidadeAbatedouroFrigorifico(
			DoencaEnfermidadeAbatedouroFrigorifico doencaEnfermidadeAbatedouroFrigorifico) {
		this.doencaEnfermidadeAbatedouroFrigorifico = doencaEnfermidadeAbatedouroFrigorifico;
	}

	public Long getQuantidadeCasos() {
		return quantidadeCasos;
	}

	public void setQuantidadeCasos(Long quantidadeCasos) {
		if (quantidadeCasos == null || quantidadeCasos == 0)
			this.setHouveFormularioColheita(false);
		
		this.quantidadeCasos = quantidadeCasos;
	}

	public List<FormularioDeColheitaTroncoEncefalico> getListaFormularioDeColheitaTroncoEncefalico() {
		if (this.listaFormularioDeColheitaTroncoEncefalico == null)
			this.listaFormularioDeColheitaTroncoEncefalico = new ArrayList<>();
		
		return listaFormularioDeColheitaTroncoEncefalico;
	}

	public void setListaFormularioDeColheitaTroncoEncefalico(
			List<FormularioDeColheitaTroncoEncefalico> listaFormularioDeColheitaTroncoEncefalico) {
		this.listaFormularioDeColheitaTroncoEncefalico = listaFormularioDeColheitaTroncoEncefalico;
	}

	public boolean isHouveFormularioColheita() {
		return houveFormularioColheita;
	}

	public void setHouveFormularioColheita(boolean houveFormularioColheita) {
		this.houveFormularioColheita = houveFormularioColheita;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AchadosLoteEnfermidadeAbatedouroFrigorifico other = (AchadosLoteEnfermidadeAbatedouroFrigorifico) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
