package br.gov.mt.indea.sistemaformin.webservice.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class EspecieExploracao implements Serializable {

    private static final long serialVersionUID = 7963613673347987352L;

	private EspecieExploracaoPK especieExploracaoPK;
    
    @XmlElement
    private FinalidadeExploracao finalidadeExploracao;

    public EspecieExploracao() {
    }

    public EspecieExploracaoPK getEspecieExploracaoPK() {
        return especieExploracaoPK;
    }

    public void setEspecieExploracaoPK(EspecieExploracaoPK especieExploracaoPK) {
        this.especieExploracaoPK = especieExploracaoPK;
    }

    public FinalidadeExploracao getFinalidadeExploracao() {
        return finalidadeExploracao;
    }

    public void setFinalidadeExploracao(FinalidadeExploracao finalidadeExploracao) {
        this.finalidadeExploracao = finalidadeExploracao;
    }
    
    
}
