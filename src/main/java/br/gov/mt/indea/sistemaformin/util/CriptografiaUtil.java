package br.gov.mt.indea.sistemaformin.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import br.gov.mt.indea.sistemaformin.exception.ApplicationException;

public class CriptografiaUtil {
	
	public static String criptografarComMD5 (String data) throws ApplicationException {  
        byte[] mybytes = data.getBytes();  
        MessageDigest md5 = null;
		try {
			md5 = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			throw new ApplicationException("N�o foi poss�vel encontrar o algoritmo para descriptografar a senha. Favor consultar suporte t�cnico");
		}  
        byte[] md5digest = md5.digest (mybytes);  
        return bytesToHex (md5digest);  
    }  
	
	private static String bytesToHex (byte[] b) {  
        StringBuffer sb = new StringBuffer();  
        for (int i = 0; i < b.length; ++i) {  
            sb.append ((Integer.toHexString((b[i] & 0xFF) | 0x100)).substring(1,3));  
        }  
        return sb.toString();  
    }

}
