package br.gov.mt.indea.sistemaformin.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import br.gov.mt.indea.sistemaformin.webservice.entity.Gta;

@Audited
@Entity
@Table(name="gta_enfermidade_abat_frig")
public class GtaEnfermidadeAbatedouroFrigorifico extends BaseEntity<Long>{

	private static final long serialVersionUID = 6459515682267284361L;

	@Id
	@SequenceGenerator(name="gta_enfermidade_abat_frig_seq", sequenceName="gta_enfermidade_abat_frig_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="gta_enfermidade_abat_frig_seq")
	private Long id;
	
	@ManyToOne
	@JoinColumn(name="id_lote_enfermidade_abat_frig")
	private LoteEnfermidadeAbatedouroFrigorifico loteEnfermidadeAbatedouroFrigorifico;
	
	@OneToOne(cascade={CascadeType.ALL}, fetch = FetchType.EAGER)
	@JoinColumn(name="id_gta")
	private Gta gta;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LoteEnfermidadeAbatedouroFrigorifico getLoteEnfermidadeAbatedouroFrigorifico() {
		return loteEnfermidadeAbatedouroFrigorifico;
	}

	public void setLoteEnfermidadeAbatedouroFrigorifico(
			LoteEnfermidadeAbatedouroFrigorifico loteEnfermidadeAbatedouroFrigorifico) {
		this.loteEnfermidadeAbatedouroFrigorifico = loteEnfermidadeAbatedouroFrigorifico;
	}

	public Gta getGta() {
		return gta;
	}

	public void setGta(Gta gta) {
		this.gta = gta;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((gta == null) ? 0 : gta.hashCode());
		result = prime * result + ((loteEnfermidadeAbatedouroFrigorifico == null) ? 0
				: loteEnfermidadeAbatedouroFrigorifico.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GtaEnfermidadeAbatedouroFrigorifico other = (GtaEnfermidadeAbatedouroFrigorifico) obj;
		if (gta == null) {
			if (other.gta != null)
				return false;
		} else if (!gta.equals(other.gta))
			return false;
		if (loteEnfermidadeAbatedouroFrigorifico == null) {
			if (other.loteEnfermidadeAbatedouroFrigorifico != null)
				return false;
		} else if (!loteEnfermidadeAbatedouroFrigorifico.equals(other.loteEnfermidadeAbatedouroFrigorifico))
			return false;
		return true;
	}
	
}
