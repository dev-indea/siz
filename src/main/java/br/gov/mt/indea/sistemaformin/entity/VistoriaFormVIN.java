package br.gov.mt.indea.sistemaformin.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import org.hibernate.envers.Audited;

import br.gov.mt.indea.sistemaformin.enums.Dominio.Especie;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoDeAgrupamento;

@Audited
@Entity(name="vistoria_form_vin")
public class VistoriaFormVIN extends BaseEntity<Long>{

	private static final long serialVersionUID = 3421462517675972246L;
	
	@Id
	@SequenceGenerator(name="vistoria_form_vin_seq", sequenceName="vistoria_form_vin_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="vistoria_form_vin_seq")
	private Long id;

	@ManyToOne
	@JoinColumn(name="id_formVIN")
	private FormVIN formVIN;
	
	@Enumerated(EnumType.STRING)
	private Especie especie;
	
	@Enumerated(EnumType.STRING)
	@Column(name="tipo_de_agrupamento")
	private TipoDeAgrupamento tipoDeAgrupamento;
	
	@Column(name="total_agrup_existentes")
	private Long totalAgrupamentosExistentes;
	
	@Column(name="total_animais_existentes")
	private Long totalAnimaisExistentes;
	
	@Column(name="total_agrup_vistoriados")
	private Long totalAgrupamentosVistoriados;
	
	@Column(name="total_animais_vistoriados")
	private Long totalAnimaisVistoriados;
	
	@Column(name="total_agrup_examinados")
	private Long totalAgrupamentosExaminados;
	
	@Column(name="total_animais_examinados")
	private Long totalAnimaisExaminados;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public FormVIN getFormVIN() {
		return formVIN;
	}

	public void setFormVIN(FormVIN formVIN) {
		this.formVIN = formVIN;
	}

	public Especie getEspecie() {
		return especie;
	}

	public void setEspecie(Especie especie) {
		this.especie = especie;
	}

	public TipoDeAgrupamento getTipoDeAgrupamento() {
		return tipoDeAgrupamento;
	}

	public void setTipoDeAgrupamento(TipoDeAgrupamento tipoDeAgrupamento) {
		this.tipoDeAgrupamento = tipoDeAgrupamento;
	}

	public Long getTotalAgrupamentosExistentes() {
		return totalAgrupamentosExistentes;
	}

	public void setTotalAgrupamentosExistentes(Long totalAgrupamentosExistentes) {
		this.totalAgrupamentosExistentes = totalAgrupamentosExistentes;
	}

	public Long getTotalAnimaisExistentes() {
		return totalAnimaisExistentes;
	}

	public void setTotalAnimaisExistentes(Long totalAnimaisExistentes) {
		this.totalAnimaisExistentes = totalAnimaisExistentes;
	}

	public Long getTotalAgrupamentosVistoriados() {
		return totalAgrupamentosVistoriados;
	}

	public void setTotalAgrupamentosVistoriados(Long totalAgrupamentosVistoriados) {
		this.totalAgrupamentosVistoriados = totalAgrupamentosVistoriados;
	}

	public Long getTotalAnimaisVistoriados() {
		return totalAnimaisVistoriados;
	}

	public void setTotalAnimaisVistoriados(Long totalAnimaisVistoriados) {
		this.totalAnimaisVistoriados = totalAnimaisVistoriados;
	}

	public Long getTotalAgrupamentosExaminados() {
		return totalAgrupamentosExaminados;
	}

	public void setTotalAgrupamentosExaminados(Long totalAgrupamentosExaminados) {
		this.totalAgrupamentosExaminados = totalAgrupamentosExaminados;
	}

	public Long getTotalAnimaisExaminados() {
		return totalAnimaisExaminados;
	}

	public void setTotalAnimaisExaminados(Long totalAnimaisExaminados) {
		this.totalAnimaisExaminados = totalAnimaisExaminados;
	}

}