package br.gov.mt.indea.sistemaformin.managedbeans;

import java.io.IOException;
import java.io.Serializable;
import java.util.Calendar;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLBeanName;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;

import br.gov.mt.indea.sistemaformin.entity.Laboratorio;
import br.gov.mt.indea.sistemaformin.entity.Pessoa;
import br.gov.mt.indea.sistemaformin.entity.dto.LaboratorioDTO;
import br.gov.mt.indea.sistemaformin.service.LaboratorioService;
import br.gov.mt.indea.sistemaformin.service.LazyObjectDataModel;
import br.gov.mt.indea.sistemaformin.util.FacesMessageUtil;

@Named
@ViewScoped
@URLBeanName("laboratorioManagedBean")
@URLMappings(mappings = {
		@URLMapping(id = "pesquisarLaboratorio", pattern = "/laboratorio/pesquisar", viewId = "/pages/laboratorio/lista.jsf"),
		@URLMapping(id = "incluirLaboratorio", pattern = "/laboratorio/incluir", viewId = "/pages/laboratorio/novo.jsf"),
		@URLMapping(id = "alterarLaboratorio", pattern = "/laboratorio/alterar/#{laboratorioManagedBean.id}", viewId = "/pages/laboratorio/novo.jsf")})
public class LaboratorioManagedBean extends PessoaManagedBean implements Serializable {

	private static final long serialVersionUID = 5401332308697941357L;
	
	private Long id;
	
	@Inject
	private Laboratorio laboratorio;
	
	@Inject
	private LaboratorioDTO laboratorioDTO;
	
	private LazyObjectDataModel<Laboratorio> listaLaboratorio;
	
	@Inject
	private LaboratorioService laboratorioService;
	
	@PostConstruct
	public void init(){
		super.init();
	}
	
	@Override
	public Pessoa getPessoa() {
		return this.laboratorio;
	}

	@Override
	public void setPessoa(Pessoa pessoa) {
		this.laboratorio = (Laboratorio) pessoa;
	}
	
	public void limpar(){
		this.laboratorio = new Laboratorio();
	}
	
	public void buscarLaboratorios(){
		this.listaLaboratorio = new LazyObjectDataModel<Laboratorio>(this.laboratorioService, this.laboratorioDTO);
	}
	
	@URLAction(mappingId = "incluirLaboratorio", onPostback = false)
	public void novo(){
		this.limpar();
	}
	
	@URLAction(mappingId = "pesquisarLaboratorio", onPostback = false)
	public void pesquisar(){
		limpar();
	}

	@URLAction(mappingId = "alterarLaboratorio", onPostback = false)
	public void editar(){
		this.laboratorio = laboratorioService.findByIdFetchAll(this.getId());

		setUf(this.laboratorio.getEndereco().getMunicipio().getUf());
		setCep(this.laboratorio.getEndereco().getCep());
	}
	
	public String adicionar() throws IOException{
		if (laboratorio.getDataCadastro() != null){
			this.laboratorioService.saveOrUpdate(laboratorio);
			FacesMessageUtil.addInfoContextFacesMessage("Laboratório " + laboratorio.getNome() + " atualizado", "");
		}else{
			this.laboratorio.setDataCadastro(Calendar.getInstance());
			this.laboratorioService.saveOrUpdate(laboratorio);
			FacesMessageUtil.addInfoContextFacesMessage("Laboratório " + laboratorio.getNome() + " adicionado!", "");
		}
		
		return "pretty:pesquisarLaboratorio";
	}
	
	public void remover(Laboratorio laboratorio){
		this.laboratorioService.delete(laboratorio);
		FacesMessageUtil.addInfoContextFacesMessage("Laboratório excluído com sucesso", "");
	}
	
	public Laboratorio getLaboratorio() {
		return laboratorio;
	}

	public void setLaboratorio(Laboratorio laboratorio) {
		this.laboratorio = laboratorio;
	}

	public LazyObjectDataModel<Laboratorio> getListaLaboratorio() {
		return listaLaboratorio;
	}

	public void setListaLaboratorio(LazyObjectDataModel<Laboratorio> listaLaboratorio) {
		this.listaLaboratorio = listaLaboratorio;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LaboratorioDTO getLaboratorioDTO() {
		return laboratorioDTO;
	}

	public void setLaboratorioDTO(LaboratorioDTO laboratorioDTO) {
		this.laboratorioDTO = laboratorioDTO;
	}

}
