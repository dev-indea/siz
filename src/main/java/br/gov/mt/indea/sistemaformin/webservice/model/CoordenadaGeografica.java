package br.gov.mt.indea.sistemaformin.webservice.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class CoordenadaGeografica implements Serializable {

	private static final long serialVersionUID = -2430515477730393843L;

	private Long id;
    
	private String orientacaoLongitude;
    
	private Integer pessoa_id;
    
	private String orientacaoLatitude;
    
	private Integer latGrau;
    
	private Integer latMin;

	private Float latSeg;
    
	private Integer longGrau;
   
	private Integer longMin;
     
	private Float longSeg;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrientacaoLongitude() {
        return orientacaoLongitude;
    }

    public void setOrientacaoLongitude(String orientacaoLongitude) {
        this.orientacaoLongitude = orientacaoLongitude;
    }

    public String getOrientacaoLatitude() {
        return orientacaoLatitude;
    }

    public void setOrientacaoLatitude(String orientacaoLatitude) {
        this.orientacaoLatitude = orientacaoLatitude;
    }

    public Integer getLatGrau() {
        return latGrau;
    }

    public void setLatGrau(Integer latGrau) {
        this.latGrau = latGrau;
    }

    public Integer getLatMin() {
        return latMin;
    }

    public void setLatMin(Integer latMin) {
        this.latMin = latMin;
    }

    public Float getLatSeg() {
        return latSeg;
    }

    public void setLatSeg(Float latSeg) {
        this.latSeg = latSeg;
    }

    public Integer getLongGrau() {
        return longGrau;
    }

    public void setLongGrau(Integer longGrau) {
        this.longGrau = longGrau;
    }

    public Integer getLongMin() {
        return longMin;
    }

    public void setLongMin(Integer longMin) {
        this.longMin = longMin;
    }

    public Float getLongSeg() {
        return longSeg;
    }

    public void setLongSeg(Float longSeg) {
        this.longSeg = longSeg;
    }

    public Integer getPessoa_id() {
        return pessoa_id;
    }

    public void setPessoa_id(Integer pessoa_id) {
        this.pessoa_id = pessoa_id;
    }
	
}
