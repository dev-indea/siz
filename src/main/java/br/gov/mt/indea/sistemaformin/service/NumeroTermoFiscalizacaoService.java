package br.gov.mt.indea.sistemaformin.service;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.mt.indea.sistemaformin.entity.Municipio;
import br.gov.mt.indea.sistemaformin.entity.NumeroTermoFiscalizacao;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class NumeroTermoFiscalizacaoService extends PaginableService<NumeroTermoFiscalizacao, Long> {

	private static final Logger log = LoggerFactory.getLogger(NumeroTermoFiscalizacaoService.class);
	
	protected NumeroTermoFiscalizacaoService() {
		super(NumeroTermoFiscalizacao.class);
	}
	
	public NumeroTermoFiscalizacao findByIdFetchAll(Long id){
		NumeroTermoFiscalizacao numeroTermoFiscalizacao;
		
		StringBuilder sql = new StringBuilder();
		sql.append("select numeroTermoFiscalizacao ")
		   .append("  from NumeroTermoFiscalizacao numeroTermoFiscalizacao ")
		   .append(" where numeroTermoFiscalizacao.id = :id ");
		
		Query query = getSession().createQuery(sql.toString()).setLong("id", id);
		numeroTermoFiscalizacao = (NumeroTermoFiscalizacao) query.uniqueResult();
		
		return numeroTermoFiscalizacao;
	}
	
	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	public NumeroTermoFiscalizacao getNumeroTermoFiscalizacaoByMunicipio(Municipio municipio, int ano){
		NumeroTermoFiscalizacao numeroTermoFiscalizacao;
		
		StringBuilder sql = new StringBuilder();
		sql.append("  from NumeroTermoFiscalizacao entity ")
		   .append("  join fetch entity.municipio ")
		   .append(" where entity.municipio = :municipio ")
		   .append("   and entity.ano = :ano");
		
		Query query = getSession().createQuery(sql.toString()).setParameter("municipio", municipio).setInteger("ano", ano);
		numeroTermoFiscalizacao = (NumeroTermoFiscalizacao) query.uniqueResult();

		return numeroTermoFiscalizacao;
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	public void saveOrUpdate(NumeroTermoFiscalizacao numeroTermoFiscalizacao) {
		super.saveOrUpdate(numeroTermoFiscalizacao);
		
		log.info("Salvando Numero Termo Fiscalização {}", numeroTermoFiscalizacao.getId());
	}
	
	@Override
	public void validar(NumeroTermoFiscalizacao NumeroTermoFiscalizacao) {

	}

	@Override
	public void validarPersist(NumeroTermoFiscalizacao NumeroTermoFiscalizacao) {

	}

	@Override
	public void validarMerge(NumeroTermoFiscalizacao NumeroTermoFiscalizacao) {

	}

	@Override
	public void validarDelete(NumeroTermoFiscalizacao NumeroTermoFiscalizacao) {

	}

}
