package br.gov.mt.indea.sistemaformin.webservice.client;

import java.io.Serializable;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;

import br.gov.mt.indea.sistemaformin.exception.ApplicationException;
import br.gov.mt.indea.sistemaformin.util.DataUtil;
import br.gov.mt.indea.sistemaformin.webservice.convert.GtaConverter;
import br.gov.mt.indea.sistemaformin.webservice.model.Gta;


public class ClientWebServiceGTA implements Serializable{
	
	private static final long serialVersionUID = 8889887844889533295L;

	protected String URL_WS = "https://sistemas.indea.mt.gov.br/FronteiraWeb/ws/gta/";
	
//	@Inject
//	@Category("WEBSERVICE::GTA")
//	private CustomLogger log;
	
	@Inject
	private GtaConverter gtaConverter;
	
	public br.gov.mt.indea.sistemaformin.webservice.entity.Gta getGTAByNumeroSerie(Integer numero, String serie) throws ApplicationException {

		String[] resposta = null;
		try {
			System.out.println(URL_WS + "findByNumeroSerie/" + numero + "/" + serie);
			resposta = new WebServiceCliente().get(URL_WS + "findByNumeroSerie/" + numero + "/" + serie);
		} catch (URISyntaxException e) {
//			//log.excecao(e.getMessage(), e);
			throw new ApplicationException("Erro ao buscar o Gta");
		}
		
		if (resposta == null || resposta[0] == null){
			return null;
		}
		
		if (resposta[0].equals("200")) {
			GsonBuilder gsonBuilder = new GsonBuilder().setDateFormat("dd/MM/yyyy");
			Gson gson = gsonBuilder.create();

			br.gov.mt.indea.sistemaformin.webservice.entity.Gta gta = gtaConverter.fromModelToEntity(gson.fromJson(resposta[1], Gta.class));
			return gta;
		} else if (resposta[0].equals("404")) {
//			//log.excecao(resposta[0] + ". " + resposta[1], null);
			throw new ApplicationException("Webservice com problemas. Contate o administrador do sistema");
		} else if (resposta[0].equals("500")) {
//			//log.excecao(resposta[0] + ". " + resposta[1], null);
			throw new ApplicationException("Webservice com problemas. Contate o administrador do sistema");
		} else if (resposta[0].equals("400")) {
			//log.excecao(resposta[0] + ". " + resposta[1], null);
			throw new ApplicationException("Webservice com problemas. Contate o administrador do sistema");
		} else{
//			//log.excecao(resposta[0] + ". " + resposta[1], null);
			throw new ApplicationException("Webservice inativo. Tente novamente em alguns minutos. Erro: " + resposta[0]);
		} 
	}
	
	public Gta getModelGTAByNumeroSerie(Integer numero, String serie) throws ApplicationException {

		String[] resposta = null;
		try {
			System.out.println(URL_WS + "findByNumeroSerie/" + numero + "/" + serie);
			resposta = new WebServiceCliente().get(URL_WS + "findByNumeroSerie/" + numero + "/" + serie);
		} catch (URISyntaxException e) {
//			//log.excecao(e.getMessage(), e);
			throw new ApplicationException("Erro ao buscar o Gta");
		}
		
		if (resposta == null || resposta[0] == null){
			return null;
		}
		
		if (resposta[0].equals("200")) {
			GsonBuilder gsonBuilder = new GsonBuilder().setDateFormat("dd/MM/yyyy");
			Gson gson = gsonBuilder.create();

			Gta gta = gson.fromJson(resposta[1], Gta.class);
			return gta;
		} else if (resposta[0].equals("404")) {
//			//log.excecao(resposta[0] + ". " + resposta[1], null);
			throw new ApplicationException("Webservice com problemas. Contate o administrador do sistema");
		} else if (resposta[0].equals("500")) {
//			//log.excecao(resposta[0] + ". " + resposta[1], null);
			throw new ApplicationException("Webservice com problemas. Contate o administrador do sistema");
		} else if (resposta[0].equals("400")) {
			//log.excecao(resposta[0] + ". " + resposta[1], null);
			throw new ApplicationException("Webservice com problemas. Contate o administrador do sistema");
		} else{
//			//log.excecao(resposta[0] + ". " + resposta[1], null);
			throw new ApplicationException("Webservice inativo. Tente novamente em alguns minutos. Erro: " + resposta[0]);
		} 
	}
	
	public List<br.gov.mt.indea.sistemaformin.webservice.entity.Gta> getGTAByPropriedadeDeOrigemEPeriodoDeEntrada(String codigoPropriedade, Date dataInicial, Date dataFinal) throws ApplicationException {

		String[] resposta = null;
		try {
			System.out.println(URL_WS + "findByPeriodoEntradaPropriedadeOrigem/" + codigoPropriedade + "/" + DataUtil.getDate_yyyy_MM_dd(dataInicial) + "/" + DataUtil.getDate_yyyy_MM_dd(dataFinal));
			resposta = new WebServiceCliente().get(URL_WS + "findByPeriodoEntradaPropriedadeOrigem/" + codigoPropriedade + "/" + DataUtil.getDate_yyyy_MM_dd(dataInicial) + "/" + DataUtil.getDate_yyyy_MM_dd(dataFinal));
		} catch (URISyntaxException e) {
//			//log.excecao(e.getMessage(), e);
			throw new ApplicationException("Erro ao buscar o Gta");
		}
		
		if (resposta == null || resposta[0] == null){
			return null;
		}
		
		if (resposta[0].equals("200")) {
			GsonBuilder gsonBuilder = new GsonBuilder().setDateFormat("dd/MM/yyyy");
			Gson gson = gsonBuilder.create();

			ArrayList<br.gov.mt.indea.sistemaformin.webservice.entity.Gta> lista = new ArrayList<br.gov.mt.indea.sistemaformin.webservice.entity.Gta>();
			JsonParser parser = new JsonParser();
			JsonArray array = parser.parse(resposta[1]).getAsJsonArray();

			for (int i = 0; i < array.size(); i++) {
				lista.add(gtaConverter.fromModelToEntity(gson.fromJson(array.get(i), Gta.class)));
			}
			
			return lista;
		} else if (resposta[0].equals("404")) {
//			//log.excecao(resposta[0] + ". " + resposta[1], null);
			throw new ApplicationException("Webservice com problemas. Contate o administrador do sistema");
		} else if (resposta[0].equals("500")) {
//			//log.excecao(resposta[0] + ". " + resposta[1], null);
			throw new ApplicationException("Webservice com problemas. Contate o administrador do sistema");
		} else if (resposta[0].equals("400")) {
			//log.excecao(resposta[0] + ". " + resposta[1], null);
			throw new ApplicationException("Webservice com problemas. Contate o administrador do sistema");
		} else{
//			//log.excecao(resposta[0] + ". " + resposta[1], null);
			throw new ApplicationException("Webservice inativo. Tente novamente em alguns minutos. Erro: " + resposta[0]);
		} 
	}
	
	public List<br.gov.mt.indea.sistemaformin.webservice.entity.Gta> getGTAByPropriedadeDeDestinoEPeriodoDeEntrada(String codigoPropriedade, Date dataInicial, Date dataFinal) throws ApplicationException {

		String[] resposta = null;
		try {
			System.out.println(URL_WS + "findByPeriodoEntradaPropriedadeDestino/" + codigoPropriedade + "/" + DataUtil.getDate_yyyy_MM_dd(dataInicial) + "/" + DataUtil.getDate_yyyy_MM_dd(dataFinal));
			resposta = new WebServiceCliente().get(URL_WS + "findByPeriodoEntradaPropriedadeDestino/" + codigoPropriedade + "/" + DataUtil.getDate_yyyy_MM_dd(dataInicial) + "/" + DataUtil.getDate_yyyy_MM_dd(dataFinal));
		} catch (URISyntaxException e) {
//			//log.excecao(e.getMessage(), e);
			throw new ApplicationException("Erro ao buscar o Gta");
		}
		
		if (resposta == null || resposta[0] == null){
			return null;
		}
		
		if (resposta[0].equals("200")) {
			GsonBuilder gsonBuilder = new GsonBuilder().setDateFormat("dd/MM/yyyy");
			Gson gson = gsonBuilder.create();

			ArrayList<br.gov.mt.indea.sistemaformin.webservice.entity.Gta> lista = new ArrayList<br.gov.mt.indea.sistemaformin.webservice.entity.Gta>();
			JsonParser parser = new JsonParser();
			JsonArray array = parser.parse(resposta[1]).getAsJsonArray();

			for (int i = 0; i < array.size(); i++) {
				lista.add(gtaConverter.fromModelToEntity(gson.fromJson(array.get(i), Gta.class)));
			}
			
			return lista;
		} else if (resposta[0].equals("404")) {
//			//log.excecao(resposta[0] + ". " + resposta[1], null);
			throw new ApplicationException("Webservice com problemas. Contate o administrador do sistema");
		} else if (resposta[0].equals("500")) {
//			//log.excecao(resposta[0] + ". " + resposta[1], null);
			throw new ApplicationException("Webservice com problemas. Contate o administrador do sistema");
		} else if (resposta[0].equals("400")) {
			//log.excecao(resposta[0] + ". " + resposta[1], null);
			throw new ApplicationException("Webservice com problemas. Contate o administrador do sistema");
		} else{
//			//log.excecao(resposta[0] + ". " + resposta[1], null);
			throw new ApplicationException("Webservice inativo. Tente novamente em alguns minutos. Erro: " + resposta[0]);
		} 
	}
	
	
	
	
	
	public List<Gta> getModelGTAByPeriodoEstabelecimento(String cnpjEstabelecimento, Date dataInicial, Date dataFinal) throws ApplicationException {

		String[] resposta = null;
		try {
			System.out.println(URL_WS + "findByPeriodoEstabelecimento/" + DataUtil.getDate_yyyy_MM_dd(dataInicial) + "/" + DataUtil.getDate_yyyy_MM_dd(dataFinal) + "/" + cnpjEstabelecimento);
			resposta = new WebServiceCliente().get(URL_WS + "findByPeriodoEstabelecimento/" + DataUtil.getDate_yyyy_MM_dd(dataInicial) + "/" + DataUtil.getDate_yyyy_MM_dd(dataFinal) + "/" + cnpjEstabelecimento);
		} catch (URISyntaxException e) {
			throw new ApplicationException("Erro ao buscar o Gta");
		}
		
		if (resposta == null || resposta[0] == null){
			return null;
		}
		
		if (resposta[0].equals("200")) {
			GsonBuilder gsonBuilder = new GsonBuilder().setDateFormat("dd/MM/yyyy");
			Gson gson = gsonBuilder.create();

			ArrayList<Gta> lista = new ArrayList<Gta>();
			JsonParser parser = new JsonParser();
			JsonArray array = parser.parse(resposta[1]).getAsJsonArray();

			for (int i = 0; i < array.size(); i++) {
				lista.add(gson.fromJson(array.get(i), Gta.class));
			}
			
			return lista;
		} else if (resposta[0].equals("404")) {
			throw new ApplicationException("Webservice com problemas. Contate o administrador do sistema");
		} else if (resposta[0].equals("500")) {
			throw new ApplicationException("Webservice com problemas. Contate o administrador do sistema");
		} else if (resposta[0].equals("400")) {
			throw new ApplicationException("Webservice com problemas. Contate o administrador do sistema");
		} else{
			throw new ApplicationException("Webservice inativo. Tente novamente em alguns minutos. Erro: " + resposta[0]);
		} 
	}

}
