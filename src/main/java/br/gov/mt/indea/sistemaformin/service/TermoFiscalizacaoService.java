package br.gov.mt.indea.sistemaformin.service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.transform.Transformers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.mt.indea.sistemaformin.entity.ChavePrincipalTermoFiscalizacao;
import br.gov.mt.indea.sistemaformin.entity.Municipio;
import br.gov.mt.indea.sistemaformin.entity.NumeroTermoFiscalizacao;
import br.gov.mt.indea.sistemaformin.entity.TermoFiscalizacao;
import br.gov.mt.indea.sistemaformin.entity.dto.AbstractDTO;
import br.gov.mt.indea.sistemaformin.entity.dto.TermoFiscalizacaoDTO;
import br.gov.mt.indea.sistemaformin.entity.view.RelatorioTermoFiscalizacaoGeral;
import br.gov.mt.indea.sistemaformin.exception.ApplicationException;
import br.gov.mt.indea.sistemaformin.exception.ApplicationRuntimeException;
import br.gov.mt.indea.sistemaformin.util.StringUtil;
import br.gov.mt.indea.sistemaformin.webservice.entity.ULE;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class TermoFiscalizacaoService extends PaginableService<TermoFiscalizacao, Long> {

	private static final Logger log = LoggerFactory.getLogger(TermoFiscalizacaoService.class);
	
	@Inject
	private NumeroTermoFiscalizacaoService numeroTermoFiscalizacaoService;

	protected TermoFiscalizacaoService() {
		super(TermoFiscalizacao.class);
	}
	
	public TermoFiscalizacao findByIdFetchAll(Long id){
		TermoFiscalizacao termoFiscalizacao;
		
		StringBuilder sql = new StringBuilder();
		sql.append("select termo ")
		   .append("  from TermoFiscalizacao termo ")
		   .append("  join fetch termo.revenda r")
		   .append("  left join fetch r.endereco")
		   .append("  left join fetch r.municipioNascimento")
		   .append("  left join fetch r.unidade u ")
		   .append("  left join fetch u.urs ")
		   .append("  join fetch termo.veterinario v")
		   .append("  join fetch v.conselho")
		   .append("  join fetch v.tipoEmitente")
		   .append("  left join fetch v.ule uu")
		   .append("  left join fetch uu.urs")
		   .append("  left join fetch v.endereco")
		   .append("  left join fetch v.municipioNascimento")
		   .append("  join fetch termo.listChavePrincipalTermoFiscalizacao ")
		   .append(" where termo.id = :id ");
		
		Query query = getSession().createQuery(sql.toString()).setLong("id", id);
		termoFiscalizacao = (TermoFiscalizacao) query.uniqueResult();
		
		if (termoFiscalizacao != null){
		
			if (termoFiscalizacao.getListChavePrincipalTermoFiscalizacao() != null)
				for (ChavePrincipalTermoFiscalizacao chave : termoFiscalizacao.getListChavePrincipalTermoFiscalizacao()) {
					Hibernate.initialize(chave.getListChaveSecundariaTermoFiscalizacao());
				}
			
		}
		
		return termoFiscalizacao;
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Long countAll(AbstractDTO dto) {
		StringBuilder sql = new StringBuilder();
		sql.append("select count(entity) ")
		   .append("  from " + this.getType().getSimpleName() + " as entity")
		   .append("  join entity.revenda revenda ")
		   .append("  left join revenda.endereco endereco ")
		   .append("  where 1 = 1 ");
		
		if (dto != null && !dto.isNull()){
			TermoFiscalizacaoDTO termoFiscalizacaoDTO = (TermoFiscalizacaoDTO) dto;
			
			if (termoFiscalizacaoDTO.getMunicipio() != null)
				sql.append("  and endereco.municipio = :municipio");
			if (termoFiscalizacaoDTO.getNumero() != null && !termoFiscalizacaoDTO.getNumero().equals(""))
				sql.append("  and entity.numero = :numero");
			if (termoFiscalizacaoDTO.getRevenda() != null && !termoFiscalizacaoDTO.getRevenda().equals(""))
				sql.append("  and lower(remove_acento(revenda.nome)) like :revenda");
			if (termoFiscalizacaoDTO.getData() != null){
				sql.append("  and to_char(entity.dataVisita, 'YYYY-MM-DD HH:MI:SS') between :inicio and :final");
			}
		}
		
		Query query = getSession().createQuery(sql.toString());
		
		if (dto != null && !dto.isNull()){
			TermoFiscalizacaoDTO termoFiscalizacaoDTO = (TermoFiscalizacaoDTO) dto;
			
			if (termoFiscalizacaoDTO.getMunicipio() != null)
				query.setParameter("municipio", termoFiscalizacaoDTO.getMunicipio());
			if (termoFiscalizacaoDTO.getNumero() != null && !termoFiscalizacaoDTO.getNumero().equals(""))
				query.setString("numero", termoFiscalizacaoDTO.getNumero());
			if (termoFiscalizacaoDTO.getRevenda() != null && !termoFiscalizacaoDTO.getRevenda().equals(""))
				query.setString("revenda", StringUtil.removeAcentos('%' + termoFiscalizacaoDTO.getRevenda().toLowerCase()) + '%');
			
			if (termoFiscalizacaoDTO.getData() != null){
				Calendar i = Calendar.getInstance();
				i.setTime(termoFiscalizacaoDTO.getData());
				
				StringBuilder sbI = new StringBuilder();
				sbI.append(i.get(Calendar.YEAR)).append("-")
				   .append(((i.get(Calendar.MONTH) + 1) + "").length() > 1 ? (i.get(Calendar.MONTH)+1) : "0" + (i.get(Calendar.MONTH)+1)).append("-")
				   .append((i.get(Calendar.DAY_OF_MONTH) + "").length() > 1 ? i.get(Calendar.DAY_OF_MONTH) : "0" + i.get(Calendar.DAY_OF_MONTH))
				   .append(" ")
				   .append("00").append(":")
				   .append("00").append(":")
				   .append("00");
				
				Calendar f = Calendar.getInstance();
				f.setTime(termoFiscalizacaoDTO.getData());
				
				StringBuilder sbF = new StringBuilder();
				sbF.append(f.get(Calendar.YEAR)).append("-")
				   .append(((f.get(Calendar.MONTH) + 1) + "").length() > 1 ? (f.get(Calendar.MONTH)+1) : "0" + (f.get(Calendar.MONTH)+1)).append("-")
				   .append((f.get(Calendar.DAY_OF_MONTH) + "").length() > 1 ? f.get(Calendar.DAY_OF_MONTH) : "0" + f.get(Calendar.DAY_OF_MONTH))
				   .append(" ")
				   .append("23").append(":")
				   .append("23").append(":")
				   .append("59");
				
				query.setString("inicio", sbI.toString());
				query.setString("final", sbF.toString());
			}
		}
		
		return (Long) query.uniqueResult();
		
	} 
	
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<TermoFiscalizacao> findAllComPaginacao(AbstractDTO dto, int first, int pageSize, String sortField, String sortOrder) {
		StringBuilder sql = new StringBuilder();
		sql.append("select entity ")
		   .append("  from " + this.getType().getSimpleName() + " as entity")
		   .append("  join fetch entity.revenda revenda ")
		   .append("  left join fetch revenda.endereco endereco ")
		   .append("  where 1 = 1 ");
		
		if (dto != null && !dto.isNull()){
			TermoFiscalizacaoDTO termoFiscalizacaoDTO = (TermoFiscalizacaoDTO) dto;
			
			if (termoFiscalizacaoDTO.getMunicipio() != null)
				sql.append("  and endereco.municipio = :municipio");
			if (termoFiscalizacaoDTO.getNumero() != null && !termoFiscalizacaoDTO.getNumero().equals(""))
				sql.append("  and entity.numero = :numero");
			if (termoFiscalizacaoDTO.getRevenda() != null && !termoFiscalizacaoDTO.getRevenda().equals(""))
				sql.append("  and lower(remove_acento(revenda.nome)) like :revenda");
			if (termoFiscalizacaoDTO.getData() != null){
				sql.append("  and to_char(entity.dataVisita, 'YYYY-MM-DD HH:MI:SS') between :inicio and :final");
			}
		}
		
		if (StringUtils.isNotBlank(sortField))
			sql.append(" order by entity." + sortField + " " + sortOrder);

		Query query = getSession().createQuery(sql.toString());
		
		if (dto != null && !dto.isNull()){
			TermoFiscalizacaoDTO termoFiscalizacaoDTO = (TermoFiscalizacaoDTO) dto;
			
			if (termoFiscalizacaoDTO.getMunicipio() != null)
				query.setParameter("municipio", termoFiscalizacaoDTO.getMunicipio());
			if (termoFiscalizacaoDTO.getNumero() != null && !termoFiscalizacaoDTO.getNumero().equals(""))
				query.setString("numero", termoFiscalizacaoDTO.getNumero());
			if (termoFiscalizacaoDTO.getRevenda() != null && !termoFiscalizacaoDTO.getRevenda().equals(""))
				query.setString("revenda", StringUtil.removeAcentos('%' + termoFiscalizacaoDTO.getRevenda().toLowerCase()) + '%');
			
			if (termoFiscalizacaoDTO.getData() != null){
				Calendar i = Calendar.getInstance();
				i.setTime(termoFiscalizacaoDTO.getData());
				
				StringBuilder sbI = new StringBuilder();
				sbI.append(i.get(Calendar.YEAR)).append("-")
				   .append(((i.get(Calendar.MONTH) + 1) + "").length() > 1 ? (i.get(Calendar.MONTH)+1) : "0" + (i.get(Calendar.MONTH)+1)).append("-")
				   .append((i.get(Calendar.DAY_OF_MONTH) + "").length() > 1 ? i.get(Calendar.DAY_OF_MONTH) : "0" + i.get(Calendar.DAY_OF_MONTH))
				   .append(" ")
				   .append("00").append(":")
				   .append("00").append(":")
				   .append("00");
				
				Calendar f = Calendar.getInstance();
				f.setTime(termoFiscalizacaoDTO.getData());
				
				StringBuilder sbF = new StringBuilder();
				sbF.append(f.get(Calendar.YEAR)).append("-")
				   .append(((f.get(Calendar.MONTH) + 1) + "").length() > 1 ? (f.get(Calendar.MONTH)+1) : "0" + (f.get(Calendar.MONTH)+1)).append("-")
				   .append((f.get(Calendar.DAY_OF_MONTH) + "").length() > 1 ? f.get(Calendar.DAY_OF_MONTH) : "0" + f.get(Calendar.DAY_OF_MONTH))
				   .append(" ")
				   .append("23").append(":")
				   .append("23").append(":")
				   .append("59");
				
				query.setString("inicio", sbI.toString());
				query.setString("final", sbF.toString());
			}
		}

		query.setFirstResult(first);
		query.setMaxResults(pageSize);

		List<TermoFiscalizacao> lista = query.list();
		
		return lista;
    }
	
	@Override
	public void saveOrUpdate(TermoFiscalizacao termoFiscalizacao) {
		if (termoFiscalizacao.getNumero() == null)
			try {
				termoFiscalizacao.setNumero(getProximoNumeroTermoFiscalizacao(termoFiscalizacao));
			} catch (ApplicationException e) {
				throw new ApplicationRuntimeException(e);
			}
		super.saveOrUpdate(termoFiscalizacao);
		
		log.info("Salvando Termo de Fiscalizacao {}", termoFiscalizacao.getId());
	}
	
	@Override
	public void delete(TermoFiscalizacao termoFiscalizacao) {
		super.delete(termoFiscalizacao);
		
		log.info("Removendo Termo de Fiscalizacao {}", termoFiscalizacao.getId());
	}
	
	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	private String getProximoNumeroTermoFiscalizacao(TermoFiscalizacao termoFiscalizacao) throws ApplicationException{
		StringBuilder sb = new StringBuilder();
		
		if (termoFiscalizacao.getRevenda().getEndereco().getMunicipio().getUf() == null)
			throw new ApplicationException("A UF do Termo de fiscaliza��o n�o pode ser nula.");
		else if (termoFiscalizacao.getRevenda().getEndereco().getMunicipio().getUf().getCodgIBGE() == null)
			throw new ApplicationException("A UF informada n�o possui c�digo do IBGE");
		
		if (termoFiscalizacao.getRevenda().getEndereco().getMunicipio() == null)
			throw new ApplicationException("O Munic�pio do Termo de fiscaliza��o n�o pode ser nulo.");
		else if (termoFiscalizacao.getRevenda().getEndereco().getMunicipio().getCodgIBGE() == null)
			throw new ApplicationException("O Munic�pio informada n�o possui c�digo do IBGE");
		
		sb.append(termoFiscalizacao.getRevenda().getEndereco().getMunicipio().getUf().getCodgIBGE());
		sb.append(termoFiscalizacao.getRevenda().getEndereco().getMunicipio().getCodgIBGE());
		sb.append("-");
		
		int ano = termoFiscalizacao.getDataVisita().get(java.util.Calendar.YEAR);
		
		NumeroTermoFiscalizacao numeroTermoFiscalizacao = this.getNumeroTermoFIscalizacaoByMunicipio(termoFiscalizacao.getRevenda().getEndereco().getMunicipio(), ano);
		if (numeroTermoFiscalizacao.getUltimoNumero() >= 9999)
			throw new ApplicationException("A numera��o de Termo de fiscaliza��o para o munic�pio de " + termoFiscalizacao.getRevenda().getEndereco().getMunicipio().getNome() + " j� atingiu o limite de 9999");
		
		String proximoNumero = String.format("%04d", numeroTermoFiscalizacao.getUltimoNumero());
		sb.append(proximoNumero);
	    sb.append("/");
	    sb.append(ano);
		
		return sb.toString();
	}
	
	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	private NumeroTermoFiscalizacao getNumeroTermoFIscalizacaoByMunicipio(Municipio municipio, int ano) throws ApplicationException{
		NumeroTermoFiscalizacao numeroTermoFiscalizacao = numeroTermoFiscalizacaoService.getNumeroTermoFiscalizacaoByMunicipio(municipio, ano);
		
		if (numeroTermoFiscalizacao == null){
			numeroTermoFiscalizacao = new NumeroTermoFiscalizacao();
			numeroTermoFiscalizacao.setMunicipio(municipio);
			numeroTermoFiscalizacao.setUltimoNumero(0L);
			numeroTermoFiscalizacao.setAno((long) ano);
			
			numeroTermoFiscalizacaoService.saveOrUpdate(numeroTermoFiscalizacao);
		}
		
		numeroTermoFiscalizacao.incrementNumeroTermoFiscalizacao();
		numeroTermoFiscalizacaoService.merge(numeroTermoFiscalizacao);
			
		return numeroTermoFiscalizacao;
	}
	
	@SuppressWarnings("unchecked")
	public List<Object[]> relatorio(Date dataInicial, Date dataFinal, ULE urs, ULE ule){
		StringBuilder sql = new StringBuilder();
		sql.append("select urs.nome as URS,")
		   .append("       m.nome as \"Munic�pio\", ")
		   .append("       t.numero as \"N�mero\", ")
		   .append("       to_char(t.data_visita, 'dd/mm/yyyy ') as \"Data da visita\", ")
		   .append("       to_char(t.data_cadastro, 'dd/mm/yyyy HH24:MI:SS') as \"Data cadastro\", ")
		   .append("       r.nome as \"Revenda\", ")
		   .append("       r.cpf || '' as \"CNPJ Revenda\", ")
		   .append("       r.codigo_revenda_svo as \"C�digo da revenda\", ")
		   .append("       tcp.nome as \"Motivo da visita\", ")
		   .append("       s3.apreensoes as \"Apreens�es\", ")
		   .append("       s3.autuacoes as \"Autua��es\", ")
		   .append("       s3.comercializacao as \"Comercializa��o\", ")
		   .append("       s3.comercio_ambulante as \"Com�rcio ambulante\", ")
		   .append("       s3.estocagem_e_armazenagem as \"Estocagem e armazenagem\", ")
		   .append("       s3.licenciamento as \"Licenciamento\", ")
		   .append("       s3.notificacoes as \"Notifica��es\", ")
		   .append("       s3.prazo_de_validade as \"Prazo de validade\", ")
		   .append("       s3.recebimento_de_biologicos as \"Recebimento de biol�gicos\", ")
		   .append("       s3.receituarios_especificos as \"Receitu�rios espec�ficos\", ")
		   .append("       s3.registro_produto_mapa as \"Registro do produto junto ao Mapa\", ")
		   .append("       s3.renovacao_anual_da_licenca as \"Renova��o anual da licen�a\", ")
		   .append("       s3.temperatura as \"Temperatura\", ")
		   .append("       s3.troca_de_gelo_em_pontos_especificos as \"Troca de gelo em pontos espec�ficos\", ")
		   .append("       u.nome as \"Servidor\" ")
		   .append("  from termo_fiscalizacao t, ")
		   .append("       pessoa r, ")
		   .append("       unidade ule, ")
		   .append("       unidade urs, ")
	       .append("       endereco e, ")
		   .append("       municipio m, ")
		   .append("       chave_principal_termo c ")
		   .append("  left join(select cp.id, ")
		   .append("		   MAX(case when t.nome = 'Apreens�es' then '1' else '0' end) as apreensoes, ")
		   .append("		   MAX(case when t.nome = 'Autua��es' then '1' else '0' end) as autuacoes, ")
		   .append("		   MAX(case when t.nome = 'Comercializa��o - Itendifica��o do produto e comprador' then '1' else '0' end) as comercializacao, ")
		   .append("		   MAX(case when t.nome = 'Com�rcio ambulante' then '1' else '0' end) as comercio_ambulante, ")
		   .append("		   MAX(case when t.nome = 'Estocagem e armazenagem' then '1' else '0' end) as estocagem_e_armazenagem, ")
		   .append("		   MAX(case when t.nome = 'Licenciamento' then '1' else '0' end) as licenciamento, ")
		   .append("		   MAX(case when t.nome = 'Notifica��es' then '1' else '0' end) as notificacoes, ")
		   .append("		   MAX(case when t.nome = 'Prazo de validade' then '1' else '0' end) as prazo_de_validade, ")
		   .append("		   MAX(case when t.nome = 'Recebimento de biol�gicos' then '1' else '0' end) as recebimento_de_biologicos, ")
		   .append("		   MAX(case when t.nome = 'Receitu�rios espec�ficos' then '1' else '0' end) as receituarios_especificos, ")
		   .append("		   MAX(case when t.nome = 'Registro do produto junto ao Minist�rio da Agricultura' then '1' else '0' end) as registro_produto_mapa, ")
		   .append("		   MAX(case when t.nome = 'Renova��o anual da licen�a' then '1' else '0' end) as renovacao_anual_da_licenca, ")
		   .append("		   MAX(case when t.nome = 'Temperatura' then '1' else '0' end) as temperatura, ")
		   .append("		   MAX(case when t.nome = 'Troca de gelo em pontos espec�ficos' then '1' else '0' end) as troca_de_gelo_em_pontos_especificos ")
		   .append("   	      from chave_principal_termo cp, ")
		   .append("		   chave_secundaria_termo cs, ")
		   .append("		   tipo_chave_secundaria_termo t ")
		   .append("	     where cp.id = cs.id_chave_principal_termo ")
		   .append("	       and cs.id_tipo_chave_secundaria_visit = t.id ")
		   .append("	     group by cp.id ")
		   .append("	     order by cp.id) as s3 on s3.id = c.id,  ")
		   .append("       tipo_chave_principal_termo tcp, ")
		   .append("       usuario u ")
		   .append(" where t.id_revenda = r.id ")
		   .append("   and r.id_endereco = e.id ")
		   .append("   and e.id_municipio = m.id ")
		   .append("   and c.id_principal_termo = t.id ")
		   .append("   and c.id_tipo_chave_principal_visit = tcp.id ")
		   .append("   and t.id_usuario = u.id ")
		   .append("   and r.id_unidade = ule.id ")
		   .append("   and ule.id_unidade_pai = urs.id ")
		   .append("   and to_char(t.data_visita, 'YYYY-MM-DD HH:MI:SS') between :dataInicial and :dataFinal ");
		   
		if (urs != null)
			sql.append("   AND urs.id = :idURS");
		if (ule != null)
			sql.append("   AND ule.id = :idULE");
		
		sql.append(" order by  ")
		   .append("       urs.nome, ")
		   .append("       ule.nome, ")
		   .append("       t.data_visita ");
		
		
		Query query = getSession().createSQLQuery(sql.toString());
		
		if (urs != null)
			query.setLong("idURS", urs.getId());
		if (ule != null)
			query.setLong("idULE", ule.getId());
		
		Calendar i = Calendar.getInstance();
		i.setTime(dataInicial);
		
		StringBuilder sbI = new StringBuilder();
		sbI.append(i.get(Calendar.YEAR)).append("-")
		   .append(((i.get(Calendar.MONTH) + 1) + "").length() > 1 ? (i.get(Calendar.MONTH)+1) : "0" + (i.get(Calendar.MONTH)+1)).append("-")
		   .append((i.get(Calendar.DAY_OF_MONTH) + "").length() > 1 ? i.get(Calendar.DAY_OF_MONTH) : "0" + i.get(Calendar.DAY_OF_MONTH))
		   .append(" ")
		   .append("00").append(":")
		   .append("00").append(":")
		   .append("00");
		
		Calendar f = Calendar.getInstance();
		f.setTime(dataFinal);
		
		StringBuilder sbF = new StringBuilder();
		sbF.append(f.get(Calendar.YEAR)).append("-")
		   .append(((f.get(Calendar.MONTH) + 1) + "").length() > 1 ? (f.get(Calendar.MONTH)+1) : "0" + (f.get(Calendar.MONTH)+1)).append("-")
		   .append((f.get(Calendar.DAY_OF_MONTH) + "").length() > 1 ? f.get(Calendar.DAY_OF_MONTH) : "0" + f.get(Calendar.DAY_OF_MONTH))
		   .append(" ")
		   .append("23").append(":")
		   .append("23").append(":")
		   .append("59");
		
		query.setString("dataInicial", sbI.toString());
		query.setString("dataFinal", sbF.toString());
				
				
				
		return query.list();
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<RelatorioTermoFiscalizacaoGeral> relatorioTeste(Date dataInicial, Date dataFinal){
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT * ")
		   .append("  FROM relatorio_termos_geral relatorio ")
		   .append(" WHERE relatorio.data_visita between :dataInicial and :dataFinal");
		
		
		Query query = getSession().createSQLQuery(sql.toString()); 
		
		query.setParameter("dataInicial", dataInicial);
		query.setParameter("dataFinal", dataFinal);
		
		query.setResultTransformer(Transformers.aliasToBean(RelatorioTermoFiscalizacaoGeral.class));
		
		List list = query.list();
		
		return list;
	}

	@Override
	public void validar(TermoFiscalizacao TermoFiscalizacao) {

	}

	@Override
	public void validarPersist(TermoFiscalizacao TermoFiscalizacao) {

	}

	@Override
	public void validarMerge(TermoFiscalizacao TermoFiscalizacao) {

	}

	@Override
	public void validarDelete(TermoFiscalizacao TermoFiscalizacao) {

	}

}
