package br.gov.mt.indea.sistemaformin.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import org.hibernate.envers.Audited;

@Audited
@Entity
@DiscriminatorValue("LABORATORIO")
public class Laboratorio extends Pessoa implements Cloneable {

	private static final long serialVersionUID = -4755058620808121405L;
	
}
