package br.gov.mt.indea.sistemaformin.service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.transform.Transformers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.mt.indea.sistemaformin.entity.FormIN;
import br.gov.mt.indea.sistemaformin.entity.FormVIN;
import br.gov.mt.indea.sistemaformin.entity.HistoricoFormIN;
import br.gov.mt.indea.sistemaformin.entity.Municipio;
import br.gov.mt.indea.sistemaformin.entity.NumeroFormIN;
import br.gov.mt.indea.sistemaformin.entity.dto.AbstractDTO;
import br.gov.mt.indea.sistemaformin.entity.dto.FormINDTO;
import br.gov.mt.indea.sistemaformin.entity.view.RelatorioFormINGeral;
import br.gov.mt.indea.sistemaformin.enums.Dominio.SimNao;
import br.gov.mt.indea.sistemaformin.enums.Dominio.StatusFormIN;
import br.gov.mt.indea.sistemaformin.exception.ApplicationException;
import br.gov.mt.indea.sistemaformin.exception.ApplicationRuntimeException;
import br.gov.mt.indea.sistemaformin.util.FacesMessageUtil;
import br.gov.mt.indea.sistemaformin.util.StringUtil;
import br.gov.mt.indea.sistemaformin.webservice.entity.Exploracao;
import br.gov.mt.indea.sistemaformin.webservice.entity.Produtor;
import br.gov.mt.indea.sistemaformin.webservice.entity.ULE;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class FormINService extends PaginableService<FormIN, Long> {
	
	private static final Logger log = LoggerFactory.getLogger(FormINService.class);
	
	@Inject
	private NumeroFormINService numeroFormINService;
	
	@Inject
	private HistoricoFormINService historicoFormINService;

	protected FormINService() {
		super(FormIN.class);
	}
	
	public FormIN findByIdFetchPropriedade(Long id){
		FormIN formIN;
		
		StringBuilder sql = new StringBuilder();
		sql.append("select formin ")
		   .append("  from FormIN formin ")
		   .append("  join fetch formin.uf ")
		   .append("  join fetch formin.municipio municipio ")
		   .append("  join fetch municipio.uf ")
		   .append("  join fetch formin.propriedade propriedade")
		   .append("  join fetch propriedade.municipio munprop")
		   .append("  left join fetch propriedade.endereco ")
		   .append("  left join fetch formin.proprietario proprietario")
		   .append("  left join fetch proprietario.endereco endprop")
		   .append("  left join fetch endprop.municipio munprop")
		   .append("  left join fetch munprop.uf")
		   .append("  left join fetch propriedade.coordenadaGeografica ")
		   .append("  left join fetch propriedade.exploracaos ")
		   .append("  left join fetch propriedade.ule pUle")
		   .append("  left join fetch pUle.urs")
		   .append("  join fetch formin.veterinario vet")
		   .append("  join fetch vet.conselho")
		   .append("  join fetch vet.tipoEmitente")
		   .append("  left join fetch vet.ule uu")
		   .append("  left join fetch uu.urs")
		   .append("  left join fetch vet.endereco")
		   .append("  left join fetch vet.municipioNascimento")
		   .append("  left join fetch formin.detalheComunicacaoAIE detalhe")
		   .append("  left join fetch detalhe.comunicacaoAIE")
		   .append(" where formin.id = :id");
		
		Query query = getSession().createQuery(sql.toString()).setLong("id", id);
		formIN = (FormIN) query.uniqueResult();
		
		if (formIN != null){
			if (formIN.getPropriedade() != null){
				for (Exploracao exploracao : formIN.getPropriedade().getExploracaos()){
					Hibernate.initialize(exploracao.getProdutores());
					
					for (Produtor produtor : exploracao.getProdutores()) {
						Hibernate.initialize(produtor.getMunicipioNascimento());
						Hibernate.initialize(produtor.getEndereco());
						Hibernate.initialize(produtor.getEndereco().getMunicipio());
						Hibernate.initialize(produtor.getEndereco().getMunicipio().getUf());
					}
				}
				
				Hibernate.initialize(formIN.getPropriedade().getProprietarios());
				
				for (Produtor produtor : formIN.getPropriedade().getProprietarios()) {
					Hibernate.initialize(produtor.getMunicipioNascimento());
					Hibernate.initialize(produtor.getEndereco());
					Hibernate.initialize(produtor.getEndereco().getMunicipio());
					Hibernate.initialize(produtor.getEndereco().getMunicipio().getUf());
				}
			}
			
		}
		
		return formIN;
	}
	
	public FormIN findByIdFetchAll(Long id){
		FormIN formIN;
		
		StringBuilder sql = new StringBuilder();
		sql.append("select formin ")
		   .append("  from FormIN formin ")
		   .append("  join fetch formin.uf ")
		   .append("  join fetch formin.municipio municipio ")
		   .append("  join fetch municipio.uf ")
		   .append("  join fetch formin.propriedade propriedade")
		   .append("  left join fetch propriedade.coordenadaGeografica ")
		   .append("  left join fetch propriedade.endereco ")
		   .append("  left join fetch formin.proprietario proprietario")
		   .append("  left join fetch proprietario.endereco endprop")
		   .append("  left join fetch endprop.municipio munprop")
		   .append("  left join fetch munprop.uf")
		   .append("  join fetch propriedade.municipio ")
		   .append("  join fetch propriedade.ule ")
		   
		   .append("  left join fetch formin.formINRelacionado ")
		   
		   .append("  join fetch formin.veterinario vet")
		   .append("  join fetch vet.conselho")
		   .append("  join fetch vet.tipoEmitente")
		   .append("  left join fetch vet.ule uu")
		   .append("  left join fetch uu.urs")
		   .append("  left join fetch vet.endereco")
		   .append("  left join fetch vet.municipioNascimento")
		   .append("  left join fetch formIN.visitaPropriedadeRural visita")
		   .append("  left join fetch formin.representanteEstabelecimento")
		   .append("  left join fetch visita.propriedade")
		   .append("  left join fetch visita.recinto")
		   .append("  left join fetch formin.bovinos")
		   .append("  left join fetch formin.bubalinos")
		   .append("  left join fetch formin.caprinos")
		   .append("  left join fetch formin.ovinos")
		   .append("  left join fetch formin.suinos")
		   .append("  left join fetch formin.equinos")
		   .append("  left join fetch formin.asininos")
		   .append("  left join fetch formin.muares")
		   .append("  left join fetch formin.aves")
		   .append("  left join fetch formin.abelhas")
		   .append("  left join fetch formin.lagomorfos")
		   .append("  left join fetch formin.outrosAnimais")
		   
		   .append("  left join fetch formin.detalheComunicacaoAIE detalhe")
		   .append("  left join fetch detalhe.comunicacaoAIE")
		   
		   .append(" where formin.id = :id ");
		
		Query query = getSession().createQuery(sql.toString()).setLong("id", id);
		formIN = (FormIN) query.uniqueResult();
		
		if (formIN != null){
			if (formIN.getPropriedade() != null){
				Hibernate.initialize(formIN.getPropriedade().getProprietarios());
				Hibernate.initialize(formIN.getPropriedade().getUle());
				Hibernate.initialize(formIN.getPropriedade().getExploracaos());
				
				for (Exploracao exploracao : formIN.getPropriedade().getExploracaos()){
					Hibernate.initialize(exploracao.getProdutores());
					
					for (Produtor produtor : exploracao.getProdutores()) {
						Hibernate.initialize(produtor.getMunicipioNascimento());
						Hibernate.initialize(produtor.getEndereco());
						Hibernate.initialize(produtor.getEndereco().getMunicipio());
						Hibernate.initialize(produtor.getEndereco().getMunicipio().getUf());
					}
				}
				
				for (Produtor produtor : formIN.getPropriedade().getProprietarios()) {
					Hibernate.initialize(produtor.getMunicipioNascimento());
					Hibernate.initialize(produtor.getEndereco());
					if (produtor.getEndereco() != null){
						Hibernate.initialize(produtor.getEndereco().getMunicipio());
						
						if (produtor.getEndereco().getMunicipio() != null)
							Hibernate.initialize(produtor.getEndereco().getMunicipio().getUf());
					}
				}
			}
			
			for (Produtor produtor : formIN.getPropriedade().getProprietarios()) {
				Hibernate.initialize(produtor.getMunicipioNascimento());
				Hibernate.initialize(produtor.getEndereco());
				if (produtor.getEndereco() != null){
					Hibernate.initialize(produtor.getEndereco().getMunicipio());
					
					if (produtor.getEndereco().getMunicipio() != null)
						Hibernate.initialize(produtor.getEndereco().getMunicipio().getUf());
				}
			}
			
			if (formIN.getRepresentanteEstabelecimento() != null)
				Hibernate.initialize(formIN.getRepresentanteEstabelecimento().getFuncoesNoEstabelecimento());
			
			if (formIN.getBovinos() != null){
				Hibernate.initialize(formIN.getBovinos().getDestinos());
				Hibernate.initialize(formIN.getBovinos().getDetalhes());
			}
			if (formIN.getBubalinos() != null){
				Hibernate.initialize(formIN.getBubalinos().getDestinos());
				Hibernate.initialize(formIN.getBubalinos().getDetalhes());
			}
			if (formIN.getCaprinos() != null){
				Hibernate.initialize(formIN.getCaprinos().getDestinos());
				Hibernate.initialize(formIN.getCaprinos().getDetalhes());
			}
			if (formIN.getOvinos() != null){
				Hibernate.initialize(formIN.getOvinos().getDestinos());
				Hibernate.initialize(formIN.getOvinos().getDetalhes());
			}
			if (formIN.getSuinos() != null){
				Hibernate.initialize(formIN.getSuinos().getDestinos());
				Hibernate.initialize(formIN.getSuinos().getDetalhes());
			}
			if (formIN.getEquinos() != null){
				Hibernate.initialize(formIN.getEquinos().getDestinos());
				Hibernate.initialize(formIN.getEquinos().getDetalhes());
			}
			if (formIN.getAsininos() != null){
				Hibernate.initialize(formIN.getAsininos().getDestinos());
				Hibernate.initialize(formIN.getAsininos().getDetalhes());
			}
			if (formIN.getMuares() != null){
				Hibernate.initialize(formIN.getMuares().getDestinos());
				Hibernate.initialize(formIN.getMuares().getDetalhes());
			}
			if (formIN.getAves() != null){
				Hibernate.initialize(formIN.getAves().getDestinos());
				Hibernate.initialize(formIN.getAves().getDetalhes());
				Hibernate.initialize(formIN.getAves().getOutrosTiposDeAves());
			}
			if (formIN.getAbelhas() != null){
				Hibernate.initialize(formIN.getAbelhas().getDestinos());
				Hibernate.initialize(formIN.getAbelhas().getDetalhes());
			}
			if (formIN.getLagomorfos() != null){
				Hibernate.initialize(formIN.getLagomorfos().getDestinos());
				Hibernate.initialize(formIN.getLagomorfos().getDetalhes());
			}
			if (formIN.getOutrosAnimais() != null){
				Hibernate.initialize(formIN.getOutrosAnimais().getDestinos());
				Hibernate.initialize(formIN.getOutrosAnimais().getDetalhes());
			}
			
			Hibernate.initialize(formIN.getTipoExploracaoAbelhas());
			Hibernate.initialize(formIN.getTipoExploracaoAves());
			Hibernate.initialize(formIN.getTipoExploracaoCoelhos());
			Hibernate.initialize(formIN.getTipoExploracaoEquideos());
			Hibernate.initialize(formIN.getTipoExploracaoSuinos());
			
			Hibernate.initialize(formIN.getFinalidadeExploracaoBovinosEBubalinos());
			Hibernate.initialize(formIN.getFinalidadeExploracaoCaprinos());
			Hibernate.initialize(formIN.getFinalidadeExploracaoOvinos());
			
			Hibernate.initialize(formIN.getFaseExploracaoBovinosEBubalinos());
			Hibernate.initialize(formIN.getFaseExploracaoCaprinos());
			Hibernate.initialize(formIN.getFaseExploracaoOvinos());
			
			Hibernate.initialize(formIN.getMedidas());
			Hibernate.initialize(formIN.getEspeciesAfetadas());
			Hibernate.initialize(formIN.getMedicacoes());
			Hibernate.initialize(formIN.getVacinas());
			Hibernate.initialize(formIN.getTransitoDeAnimais());
	
			Hibernate.initialize(formIN.getListaFolhasAdicionais());
			Hibernate.initialize(formIN.getListaFormAIE());
			Hibernate.initialize(formIN.getListaFormCOM());
			Hibernate.initialize(formIN.getListaFormEQ());
			Hibernate.initialize(formIN.getListaFormLAB());
			Hibernate.initialize(formIN.getListaFormMaleina());
			Hibernate.initialize(formIN.getListaFormMormo());
			Hibernate.initialize(formIN.getListaFormSH());
			Hibernate.initialize(formIN.getListaFormSN());
			Hibernate.initialize(formIN.getListaFormSRN());
			Hibernate.initialize(formIN.getListaFormSV());
			Hibernate.initialize(formIN.getListaFormVIN());
			Hibernate.initialize(formIN.getListaResenho());
			
		}
		
		return formIN;
	}
	
	public FormIN carregarHistorico(Long id){
		FormIN formIN;
		
		StringBuilder sql = new StringBuilder();
		sql.append("select formin ")
		   .append("  from FormIN formin ")
		   .append("  join fetch formin.uf ")
		   .append("  join fetch formin.municipio municipio ")
		   .append("  join fetch municipio.uf ")
		   .append("  join fetch formin.propriedade propriedade")
		   .append("  left join fetch formin.proprietario proprietario")
		   .append("  left join fetch proprietario.endereco endprop")
		   .append("  left join fetch endprop.municipio munprop")
		   .append("  left join fetch munprop.uf")
		   .append("  join fetch propriedade.municipio ")
		   .append("  join fetch propriedade.ule ule ")
		   .append("  left join fetch ule.urs ")
		   .append("  left join fetch formin.listaHistoricoFormINs ")
		   .append(" where formin.id = :id ");
		
		Query query = getSession().createQuery(sql.toString()).setLong("id", id);
		formIN = (FormIN) query.uniqueResult();
		
		return formIN;
	}
	
	public FormIN getFormINComFormulariosAnexos(Long id){
		FormIN formIN;
		
		StringBuilder sql = new StringBuilder();
		sql.append("select formin ")
		   .append("  from FormIN formin ")
		   .append("  join fetch formin.uf ")
		   .append("  join fetch formin.municipio municipio ")
		   .append("  join fetch municipio.uf ")
		   .append("  join fetch formin.propriedade propriedade")
		   .append("  left join fetch formin.proprietario proprietario")
		   .append("  left join fetch proprietario.endereco endprop")
		   .append("  left join fetch endprop.municipio munprop")
		   .append("  left join fetch munprop.uf")
		   .append("  join fetch propriedade.municipio ")
		   .append("  join fetch propriedade.ule ule ")
		   .append("  left join fetch ule.urs ")
		   .append(" where formin.id = :id ");
		
		Query query = getSession().createQuery(sql.toString()).setLong("id", id);
		formIN = (FormIN) query.uniqueResult();
		
		Hibernate.initialize(formIN.getListaFolhasAdicionais());
		Hibernate.initialize(formIN.getListaFormAIE());
		Hibernate.initialize(formIN.getListaFormCOM());
		Hibernate.initialize(formIN.getListaFormEQ());
		Hibernate.initialize(formIN.getListaFormLAB());
		Hibernate.initialize(formIN.getListaFormMaleina());
		Hibernate.initialize(formIN.getListaFormMormo());
		Hibernate.initialize(formIN.getListaFormSH());
		Hibernate.initialize(formIN.getListaFormSN());
		Hibernate.initialize(formIN.getListaFormSRN());
		Hibernate.initialize(formIN.getListaFormSV());
		Hibernate.initialize(formIN.getListaFormVIN());
		
		if (formIN.getListaFormVIN() != null)
			for (FormVIN formVIN : formIN.getListaFormVIN()) {
				Hibernate.initialize(formVIN);
				Hibernate.initialize(formVIN.getAbatedouro());
			}
		
		Hibernate.initialize(formIN.getListaResenho());
		
		return formIN;
	}
	
	public FormIN findByNumero(String numero){
		FormIN formIN;
		
		StringBuilder sql = new StringBuilder();
		sql.append("select formin ")
		   .append("  from FormIN formin ")
		   .append("  join fetch formin.uf ")
		   .append("  join fetch formin.municipio municipio ")
		   .append("  join fetch municipio.uf ")
		   .append("  join fetch formin.propriedade propriedade")
		   .append("  left join fetch propriedade.endereco ")
		   .append("  join fetch formin.proprietario proprietario")
		   .append("  left join fetch proprietario.endereco")
		   .append("  left join fetch propriedade.coordenadaGeografica ")
		   .append("  join fetch formin.veterinario vet")
		   .append("  join fetch vet.conselho")
		   .append("  join fetch vet.tipoEmitente")
		   .append("  left join fetch vet.ule uu")
		   .append("  left join fetch uu.urs")
		   .append("  left join fetch vet.endereco")
		   .append("  left join fetch vet.municipioNascimento")
		   .append(" where formin.numero = :numero")
		   .append("   and formin.statusFormIN != :status");
		
		Query query = getSession().createQuery(sql.toString()).setString("numero", numero).setParameter("status", StatusFormIN.COPIA_VALIDADA);
		formIN = (FormIN) query.uniqueResult();

		return formIN;
	}
	
	@SuppressWarnings("unchecked")
	public List<FormIN> findByNumero(String numero, String codigo){
		List<FormIN> listaFormIN;
		
		StringBuilder sql = new StringBuilder();
		sql.append("select formin ")
		   .append("  from FormIN formin ")
		   .append("  join fetch formin.uf ")
		   .append("  join fetch formin.municipio municipio ")
		   .append("  join fetch municipio.uf ")
		   .append("  join fetch formin.propriedade propriedade")
		   .append("  left join fetch propriedade.endereco ")
		   .append("  join fetch formin.proprietario proprietario")
		   .append("  left join fetch proprietario.endereco")
		   .append("  left join fetch propriedade.coordenadaGeografica ")
		   .append("  join fetch formin.veterinario vet")
		   .append("  join fetch vet.conselho")
		   .append("  join fetch vet.tipoEmitente")
		   .append("  left join fetch vet.ule uu")
		   .append("  left join fetch uu.urs")
		   .append("  left join fetch vet.endereco")
		   .append("  left join fetch vet.municipioNascimento")
		   .append(" where formin.statusFormIN != :status");
		
		if (!StringUtils.isEmpty(numero))
			sql.append("   and formin.numero = :numero ");

		if (!StringUtils.isEmpty(codigo))
			sql.append("   and propriedade.codigo = :codigo ");
		
		sql.append(" order by formin.dataDaNotificacao desc");
		
		Query query = getSession().createQuery(sql.toString()).setParameter("status", StatusFormIN.COPIA_VALIDADA);
		
		if (!StringUtils.isEmpty(numero))
			query.setString("numero", numero);

		if (!StringUtils.isEmpty(codigo))
			query.setString("codigo", codigo);
		
		listaFormIN = (List<FormIN>) query.list();

		return listaFormIN;
	}
	
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<FormIN> findAllComPaginacao(AbstractDTO dto, int first, int pageSize, String sortField, String sortOrder) {
		StringBuilder sql = new StringBuilder();
		sql.append("select entity ")
		   .append("  from " + this.getType().getSimpleName() + " as entity")
		   .append("  join fetch entity.propriedade propriedade")
		   .append("  join fetch entity.municipio ")
		   .append(" where entity.statusFormIN != 'COPIA_VALIDADA'");
		
		if (dto != null && !dto.isNull()){
			FormINDTO formINDTO = (FormINDTO) dto;
			
			if (formINDTO.getMunicipio() != null)
				sql.append("  and entity.municipio = :municipio");
			if (formINDTO.getNumeroFormIN() != null && !formINDTO.getNumeroFormIN().equals(""))
				sql.append("  and entity.numero = :numero");
			if (formINDTO.getPropriedade() != null && !formINDTO.getPropriedade().equals(""))
				sql.append("  and lower(remove_acento(propriedade.nome)) like :propriedade");
			if (formINDTO.getData() != null){
				sql.append("  and to_char(entity.dataDaNotificacao, 'YYYY-MM-DD HH:MI:SS') between :inicio and :final");
			}
		}
		
		if (StringUtils.isNotBlank(sortField))
			sql.append(" order by entity." + sortField + " " + sortOrder);

		Query query = getSession().createQuery(sql.toString());
		
		if (dto != null && !dto.isNull()){
			FormINDTO formINDTO = (FormINDTO) dto;
			
			if (formINDTO.getMunicipio() != null)
				query.setParameter("municipio", formINDTO.getMunicipio());
			if (formINDTO.getNumeroFormIN() != null && !formINDTO.getNumeroFormIN().equals(""))
				query.setString("numero", formINDTO.getNumeroFormIN());
			if (formINDTO.getPropriedade() != null && !formINDTO.getPropriedade().equals(""))
				query.setString("propriedade", StringUtil.removeAcentos('%' + formINDTO.getPropriedade().toLowerCase()) + '%');
			
			if (formINDTO.getData() != null){
				Calendar i = Calendar.getInstance();
				i.setTime(formINDTO.getData());
				
				StringBuilder sbI = new StringBuilder();
				sbI.append(i.get(Calendar.YEAR)).append("-")
				   .append(((i.get(Calendar.MONTH) + 1) + "").length() > 1 ? (i.get(Calendar.MONTH)+1) : "0" + (i.get(Calendar.MONTH)+1)).append("-")
				   .append((i.get(Calendar.DAY_OF_MONTH) + "").length() > 1 ? i.get(Calendar.DAY_OF_MONTH) : "0" + i.get(Calendar.DAY_OF_MONTH))
				   .append(" ")
				   .append("00").append(":")
				   .append("00").append(":")
				   .append("00");
				
				Calendar f = Calendar.getInstance();
				f.setTime(formINDTO.getData());
				
				StringBuilder sbF = new StringBuilder();
				sbF.append(f.get(Calendar.YEAR)).append("-")
				   .append(((f.get(Calendar.MONTH) + 1) + "").length() > 1 ? (f.get(Calendar.MONTH)+1) : "0" + (f.get(Calendar.MONTH)+1)).append("-")
				   .append((f.get(Calendar.DAY_OF_MONTH) + "").length() > 1 ? f.get(Calendar.DAY_OF_MONTH) : "0" + f.get(Calendar.DAY_OF_MONTH))
				   .append(" ")
				   .append("23").append(":")
				   .append("23").append(":")
				   .append("59");
				
				query.setString("inicio", sbI.toString());
				query.setString("final", sbF.toString());
			}
		}
		
		query.setFirstResult(first);
		query.setMaxResults(pageSize);

		List<FormIN> lista = query.list();
		
		if (lista != null)
			for (FormIN formIN : lista) {
				if (formIN.getPropriedade() != null)
					formIN.getPropriedade().getNome();
			}
		
		return lista;
    }
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Long countAll(AbstractDTO dto){
		StringBuilder sql = new StringBuilder();
		sql.append("select count(entity) ")
		   .append("  from " + this.getType().getSimpleName() + " as entity")
		   .append("  join entity.propriedade propriedade")
		   .append("  join entity.municipio ")
		   .append(" where entity.statusFormIN != 'COPIA_VALIDADA'");
		
		if (dto != null && !dto.isNull()){
			FormINDTO formINDTO = (FormINDTO) dto;
			
			if (formINDTO.getMunicipio() != null)
				sql.append("  and entity.municipio = :municipio");
			if (formINDTO.getNumeroFormIN() != null && !formINDTO.getNumeroFormIN().equals(""))
				sql.append("  and entity.numero = :numero");
			if (formINDTO.getPropriedade() != null && !formINDTO.getPropriedade().equals(""))
				sql.append("  and lower(remove_acento(propriedade.nome)) like :propriedade");
			if (formINDTO.getData() != null){
				sql.append("  and to_char(entity.dataDaNotificacao, 'YYYY-MM-DD HH:MI:SS') between :inicio and :final");
			}
		}
		
		Query query = getSession().createQuery(sql.toString());
		
		if (dto != null && !dto.isNull()){
			FormINDTO formINDTO = (FormINDTO) dto;
			
			if (formINDTO.getMunicipio() != null)
				query.setParameter("municipio", formINDTO.getMunicipio());
			if (formINDTO.getNumeroFormIN() != null && !formINDTO.getNumeroFormIN().equals(""))
				query.setString("numero", formINDTO.getNumeroFormIN());
			if (formINDTO.getPropriedade() != null && !formINDTO.getPropriedade().equals(""))
				query.setString("propriedade", StringUtil.removeAcentos('%' + formINDTO.getPropriedade().toLowerCase()) + '%');
			
			if (formINDTO.getData() != null){
				Calendar i = Calendar.getInstance();
				i.setTime(formINDTO.getData());
				
				StringBuilder sbI = new StringBuilder();
				sbI.append(i.get(Calendar.YEAR)).append("-")
				   .append(((i.get(Calendar.MONTH) + 1) + "").length() > 1 ? (i.get(Calendar.MONTH)+1) : "0" + (i.get(Calendar.MONTH)+1)).append("-")
				   .append((i.get(Calendar.DAY_OF_MONTH) + "").length() > 1 ? i.get(Calendar.DAY_OF_MONTH) : "0" + i.get(Calendar.DAY_OF_MONTH))
				   .append(" ")
				   .append("00").append(":")
				   .append("00").append(":")
				   .append("00");
				
				Calendar f = Calendar.getInstance();
				f.setTime(formINDTO.getData());
				
				StringBuilder sbF = new StringBuilder();
				sbF.append(f.get(Calendar.YEAR)).append("-")
				   .append(((f.get(Calendar.MONTH) + 1) + "").length() > 1 ? (f.get(Calendar.MONTH)+1) : "0" + (f.get(Calendar.MONTH)+1)).append("-")
				   .append((f.get(Calendar.DAY_OF_MONTH) + "").length() > 1 ? f.get(Calendar.DAY_OF_MONTH) : "0" + f.get(Calendar.DAY_OF_MONTH))
				   .append(" ")
				   .append("23").append(":")
				   .append("23").append(":")
				   .append("59");
				
				query.setString("inicio", sbI.toString());
				query.setString("final", sbF.toString());
			}
		}
    	
    	
		return (Long) query.uniqueResult();
	}
	
	public void saveOrUpdate(FormIN formIN) {
		if (formIN.getNumero() == null)
			try {
				formIN.setNumero(getProximoNumeroFormIN(formIN));
			} catch (ApplicationException e) {
				throw new ApplicationRuntimeException(e);
			}
		
		formIN = (FormIN) this.getSession().merge(formIN);
		super.saveOrUpdate(formIN);
		
		log.info("Salvando Form IN {}", formIN.getId());
	}

	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	private String getProximoNumeroFormIN(FormIN formIN) throws ApplicationException{
		StringBuilder sb = new StringBuilder();
		
		if (formIN.getUf() == null)
			throw new ApplicationException("A UF do Form IN n�o pode ser nula.");
		else if (formIN.getUf().getCodgIBGE() == null)
			throw new ApplicationException("A UF informada n�o possui c�digo do IBGE");
		
		if (formIN.getMunicipio() == null)
			throw new ApplicationException("O Munic�pio do Form IN n�o pode ser nulo.");
		else if (formIN.getMunicipio().getCodgIBGE() == null)
			throw new ApplicationException("O Munic�pio informada n�o possui c�digo do IBGE");
		
		sb.append(formIN.getUf().getCodgIBGE());
		sb.append(formIN.getMunicipio().getCodgIBGE());
		sb.append("-");
		
		NumeroFormIN numeroFormIN = this.getNumeroFornINByMunicipio(formIN.getMunicipio());
		if (numeroFormIN.getUltimoNumero() >= 9999)
			throw new ApplicationException("A numera��o de Form IN para o munic�pio de " + formIN.getMunicipio().getNome() + " j� atingiu o limite de 9999");
		
		String proximoNumero = String.format("%04d", numeroFormIN.getUltimoNumero());
		sb.append(proximoNumero);
		
		return sb.toString();
	}
	
	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	private NumeroFormIN getNumeroFornINByMunicipio(Municipio municipio) throws ApplicationException{
		NumeroFormIN numeroFormIN = numeroFormINService.getNumeroFormINByMunicipio(municipio);
		
		if (numeroFormIN == null){
			numeroFormIN = new NumeroFormIN();
			numeroFormIN.setMunicipio(municipio);
			numeroFormIN.setUltimoNumero(0L);
			
			numeroFormINService.saveOrUpdate(numeroFormIN);
		}
		
		numeroFormIN.incrementNumeroFormIN();
		numeroFormINService.merge(numeroFormIN);
		
		return numeroFormIN;
	}
	
	public void validarFormIN(FormIN formIN){
		log.info("Iniciando Valida��o do Form IN {}", formIN.getId());
		
		if (formIN.getStatusFormIN().equals(StatusFormIN.ALTERADO))
			formIN.setRetificado(SimNao.SIM);
			
		formIN.setStatusFormIN(StatusFormIN.VALIDADO);
		
		HistoricoFormIN historico 	= new HistoricoFormIN();
		historico.setFormINOriginal(formIN);
		historico.setData(Calendar.getInstance());
		
		try {
			historico.setFormINCopia((FormIN) formIN.clone());
		} catch (CloneNotSupportedException e) {
			log.error("VALIDA��O FORM IN", "Erro ao processar a valida��o do Form IN n�: " + formIN.getNumero());
			FacesMessageUtil.addErrorContextFacesMessage("Erro", e.getLocalizedMessage());
			return;
		}
		
		historicoFormINService.saveOrUpdate(historico);
		saveOrUpdate(formIN);
		
		log.info("Finalizando Valida��o do Form IN {}", formIN.getId());
	}
//	
//	@SuppressWarnings("unchecked")
//	public List<Object[]> relatorio(Date dataInicial, Date dataFinal, ULE urs, ULE ule){
//		StringBuilder sql = new StringBuilder();
//		sql.append("SELECT urs.nome AS \"URS\", ")
//		   .append("       m.nome AS \"Municipio\", ")
//		   .append("       f.numero AS \"N�mero\", ")
//		   .append("       c.latitude_grau AS \"Latitude grau\", ")
//		   .append("       c.latitude_minuto AS \"Latitude minuto\", ")
//		   .append("       c.latitude_segundo AS \"Latitude segundo\", ")
//		   .append("       c.longitude_grau AS \"Longitude \", ")
//		   .append("       c.longitude_minuto AS \"Longitude minuto\", ")
//		   .append("       c.longitude_segundo AS \"Longitude segundo\", ")
//		   .append("       f.doenca_vigilancia_sindromica AS \"Tipo notifica��o\", ")
//		   .append("	    ")
//		   .append("	   f.houve_coleta_de_amostras AS \"Houve colheita de material para envio a laborat�rio\", ")
//		   .append("        ")
//		   .append("       to_char((SELECT data_colheita ")
//		   .append("          FROM form_lab flab ")
//		   .append("         WHERE flab.id_formin = f.id ")
//		   .append("         ORDER BY data_colheita desc ")
//		   .append("         LIMIT 1), 'dd/mm/yyyy') AS \"Data de envio do material\", ")
//		   .append("         ")
//		   .append("       to_char(f.data_inicio_evento, 'dd/mm/yyyy') AS \"Data prov�vel do in�cio\", ")
//		   .append("       to_char(f.data_notificacao, 'dd/mm/yyyy HH24:MI:SS') AS \"Data da notifica��o\", ")
//		   .append("       to_char(f.data_cadastro, 'dd/mm/yyyy HH24:MI:SS') as \"Data de cadastro\", ")
//		   .append("       to_char(f.data_abertura, 'dd/mm/yyyy HH24:MI:SS') AS \"Data da visita inicial do m�dico veterin�rio\", ")
//		   .append("       f.fonte_notificacao AS \"Origem da notifica��o\", ")
//		   .append("        ")
//		   .append("       (SELECT s2.especies  ")
//		   .append("	      FROM (SELECT i.id_formin, ")
//		   .append("		               array_to_string(array_agg(distinct i.tipo), ',') as especies ")
//		   .append("		          FROM informacoes_de_animais i, ")
//		   .append("		               detalhe_info_animais d ")
//		   .append("		         WHERE (i.id = d.id_informacoes_abelhas ")
//		   .append("		           AND (d.qtde_doentes_confirmados > 0 ")
//		   .append("		            OR d.qtde_doentes_provaveis >0)) ")
//		   .append("        		     ")
//		   .append("		            OR (i.id = d.id_informacoes_outrosanimais ")
//		   .append("		           AND (d.qtde_doentes_confirmados > 0 ")
//		   .append("		            OR d.qtde_doentes_provaveis >0)) ")
//		   .append("		     ")
//		   .append("		            OR (i.id = d.id_informacoes_equinos ")
//		   .append("		           AND (d.qtde_doentes_confirmados > 0 ")
//		   .append("        		    OR d.qtde_doentes_provaveis >0)) ")
//		   .append("		     ")
//		   .append("		            OR (i.id = d.id_informacoes_aves ")
//		   .append("		           AND (d.qtde_doentes_confirmados > 0 ")
//		   .append("		            OR d.qtde_doentes_provaveis >0)) ")
//		   .append("		     ")
//		   .append("		            OR (i.id = d.id_informacoes_suinos ")
//		   .append("		           AND (d.qtde_doentes_confirmados > 0 ")
//		   .append("		            OR d.qtde_doentes_provaveis >0)) ")
//		   .append("		     ")
//		   .append("		            OR (i.id = d.id_informacoes_bubalinos ")
//		   .append("		           AND (d.qtde_doentes_confirmados > 0 ")
//		   .append("        		    OR d.qtde_doentes_provaveis >0)) ")
//		   .append("		     ")
//		   .append("		            OR (i.id = d.id_informacoes_bovinos ")
//		   .append("		           AND (d.qtde_doentes_confirmados > 0 ")
//		   .append("		            OR d.qtde_doentes_provaveis >0)) ")
//		   .append("		     ")
//		   .append("		            OR (i.id = d.id_informacoes_caprinos ")
//		   .append("		           AND (d.qtde_doentes_confirmados > 0 ")
//		   .append("		            OR d.qtde_doentes_provaveis >0)) ")
//		   .append("		     ")
//		   .append("		            OR (i.id = d.id_informacoes_lagomorfos ")
//		   .append("		           AND (d.qtde_doentes_confirmados > 0 ")
//		   .append("		            OR d.qtde_doentes_provaveis >0)) ")
//		   .append("		     ")
//		   .append("		            OR (i.id = d.id_informacoes_asininos ")
//		   .append("		           AND (d.qtde_doentes_confirmados > 0 ")
//		   .append("		            OR d.qtde_doentes_provaveis >0)) ")
//		   .append("		     ")
//		   .append("		            OR (i.id = d.id_informacoes_muares ")
//		   .append("		           AND (d.qtde_doentes_confirmados > 0 ")
//		   .append("		            OR d.qtde_doentes_provaveis >0)) ")
//		   .append("		     ")
//		   .append("		            OR (i.id = d.id_informacoes_ovinos ")
//		   .append("		           AND (d.qtde_doentes_confirmados > 0 ")
//		   .append("		            OR d.qtde_doentes_provaveis >0)) ")
//		   .append("		         GROUP BY 1) as s2 ")
//		   .append("	         WHERE s2.id_formin = f.id	) AS \"Esp�cie afetada\", ")
//		   .append(" ")
//		   .append("       pes.nome AS \"Nome do propriet�rio\", ")
//		   .append("       p.nome AS \"Nome da propriedade\", ")
//		   .append("       p.codigo_propriedade AS \"C�digo da propriedade\", ")
//		   .append("       vet.nome AS \"Nome m�dico veterin�rio respons�vel\", ")
//		   .append("       vet.numero_conselho AS \"N� CRMV\", ")
//		   .append(" ")
//		   .append("       f.diagnostico_provavel AS \"Diagn�stico prov�vel\", ")
//		   .append("       f.diagnostico_conclusivo AS \"Diagn�stico conclusivo\", ")
//		   .append(" ")
//		   .append("       (SELECT s3.testes_realizados ")
//		   .append("          FROM (SELECT r.id_formcom, ")
//		   .append("                       array_to_string(array_agg(distinct r.teste_realizado), ',') as testes_realizados ")
//		   .append("                  FROM form_com fcom, ")
//		   .append("                       resultado_de_teste r ")
//		   .append("                 WHERE r.id_formcom = fcom.id ")
//		   .append("                           AND fcom.id_formin = f.id ")
//		   .append("                   AND fcom.tipo_investigacao = 'ENCERRAMENTO' ")
//		   .append("                 GROUP BY 1) AS s3) AS \"Tipo do teste que confirmou o resultado\", ")
//		   .append(" ")
//		   .append("       to_char(fc.data_investigacao, 'dd/mm/yyyy') AS \"Data do resultado\", ")
//		   .append("       fc.diagnostico_conclusivo AS \"Diagn�stico\", ")
//		   .append(" ")
//		   .append("       case when (SELECT count(*) ")
//		   .append("       FROM form_com fcom ")
//		   .append("       WHERE fcom.id_formin = f.id ")
//		   .append("       AND fcom.tipo_investigacao = 'ENCERRAMENTO') != 0 then ")
//		   .append("       'SIM'  ")
//		   .append("       else ")
//		   .append("       fim_atendimento ")
//		   .append("       end \"Investiga��o Encerrada?\", ")
//		   .append(" ")
//		   .append("       (SELECT fcom.data_investigacao ")
//		   .append("          FROM form_com fcom ")
//		   .append("         WHERE fcom.id_formin = f.id ")
//		   .append("           AND fcom.tipo_investigacao = 'ENCERRAMENTO' ")
//		   .append("         ORDER BY data_investigacao desc ")
//		   .append("         LIMIT 1) AS \"Data do Form COM de encerramento\", ")
//		   .append("        ")
//		   .append("       (SELECT count(*) ")
//		   .append("          FROM form_com fcom ")
//		   .append("         WHERE fcom.id_formin = f.id) AS \"Quantidade Form COM\", ")
//		   .append(" ")
//		   .append("       (select sum (quantidade)  ")
//		   .append("	  from (select sum(qtde_destruidos) as quantidade ")
//		   .append("		  from form_in ff, ")
//		   .append("		       informacoes_de_animais ii, ")
//		   .append("		       detalhe_info_animais dd ")
//		   .append("		 where ff.id = ii.id_formin ")
//		   .append("		   and ii.id = dd.id_informacoes_equinos ")
//		   .append("		   and ii.tipo = 'equinos' ")
//		   .append("		   and ff.id = f.id ")
//		   .append(" ")
//		   .append("		union all ")
//		   .append(" ")
//		   .append("		select sum(qtde_novos_destruidos) as quantidade ")
//		   .append("		  from form_in ff, ")
//		   .append("		       form_com cc, ")
//		   .append("		       info_animais_form_com ii, ")
//		   .append("		       detalhe_info_animais_form_com dd ")
//		   .append("		 where ff.id = cc.id_formin ")
//		   .append("		   and cc.id = ii.id_formcom ")
//		   .append("		   and ii.id = dd.id_informacoes_equinos ")
//		   .append("		   and ii.tipo = 'equinos' ")
//		   .append("		   and ff.id = f.id) as s_equinos_destruidos) as \"Quantidade equ�deos destru�dos\" ")
//		   .append("          ")
//		   .append("  FROM form_in f ")
//		   .append(" ")
//		   .append("  LEFT JOIN form_com fc ")
//		   .append("    ON fc.id_formin = f.id ")
//		   .append("   AND fc.id = ")
//		   .append("        ( SELECT id ")
//		   .append("            FROM form_com fcc ")
//		   .append("           WHERE fcc.id_formin = f.id ")
//		   .append("           ORDER BY data_investigacao asc ")
//		   .append("           LIMIT 1 ")
//		   .append("       ), ")
//		   .append("               ")
//		   .append("       propriedade p, ")
//		   .append("       unidade ule, ")
//		   .append("       unidade urs, ")
//		   .append("       municipio m, ")
//		   .append("       coordenadageografica c, ")
//		   .append("       pessoa pes, ")
//		   .append("       pessoa vet  ")
//		   .append("               ")
//		   .append(" WHERE f.id_propriedade = p.id ")
//		   .append("   AND p.id_unidade = ule.id ")
//		   .append("   AND ule.id_unidade_pai = urs.id ")
//		   .append("   AND p.id_municipio = m.id ")
//		   .append("   AND p.id_coordenada_geografica = c.id ")
//		   .append("   AND p.id_produtor = pes.id ")
//		   .append("   AND f.id_veterinario = vet.id ")
//		   .append("   AND to_char(f.data_notificacao, 'YYYY-MM-DD HH:MI:SS') between :dataInicial and :dataFinal ")
//		   .append("   AND f.status in ('NOVO', 'VALIDADO', 'ALTERADO') ");
//		
//		if (urs != null)
//			sql.append("   AND urs.id = :idURS");
//		if (ule != null)
//			sql.append("   AND ule.id = :idULE");
//		
//		sql.append(" order by  ")
//		   .append("       urs.nome, ")
//		   .append("       ule.nome, ")
//		   .append("       f.data_notificacao ");
//		
//		Query query = getSession().createSQLQuery(sql.toString());
//		
//		if (urs != null)
//			query.setLong("idURS", urs.getId());
//		if (ule != null)
//			query.setLong("idULE", ule.getId());
//		
//		Calendar i = Calendar.getInstance();
//		i.setTime(dataInicial);
//		
//		StringBuilder sbI = new StringBuilder();
//		sbI.append(i.get(Calendar.YEAR)).append("-")
//		   .append(((i.get(Calendar.MONTH) + 1) + "").length() > 1 ? (i.get(Calendar.MONTH)+1) : "0" + (i.get(Calendar.MONTH)+1)).append("-")
//		   .append((i.get(Calendar.DAY_OF_MONTH) + "").length() > 1 ? i.get(Calendar.DAY_OF_MONTH) : "0" + i.get(Calendar.DAY_OF_MONTH))
//		   .append(" ")
//		   .append("00").append(":")
//		   .append("00").append(":")
//		   .append("00");
//		
//		Calendar f = Calendar.getInstance();
//		f.setTime(dataFinal);
//		
//		StringBuilder sbF = new StringBuilder();
//		sbF.append(f.get(Calendar.YEAR)).append("-")
//		   .append(((f.get(Calendar.MONTH) + 1) + "").length() > 1 ? (f.get(Calendar.MONTH)+1) : "0" + (f.get(Calendar.MONTH)+1)).append("-")
//		   .append((f.get(Calendar.DAY_OF_MONTH) + "").length() > 1 ? f.get(Calendar.DAY_OF_MONTH) : "0" + f.get(Calendar.DAY_OF_MONTH))
//		   .append(" ")
//		   .append("23").append(":")
//		   .append("23").append(":")
//		   .append("59");
//		
//		query.setString("dataInicial", sbI.toString());
//		query.setString("dataFinal", sbF.toString());
//				
//		return query.list();
//	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<RelatorioFormINGeral> relatorioTeste(Date dataInicial, Date dataFinal){
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT * ")
		   .append("  FROM relatorio_form_in relatorio ")
		   .append(" WHERE relatorio.data_notificacao between :dataInicial and :dataFinal");
		
		
		Query query = getSession().createSQLQuery(sql.toString()); 
		
		query.setParameter("dataInicial", dataInicial);
		query.setParameter("dataFinal", dataFinal);
		
		query.setResultTransformer(Transformers.aliasToBean(RelatorioFormINGeral.class));
		
		List list = query.list();
		
		return list;
	}
	
	@Override
	public void validar(FormIN FormIN) {

	}

	@Override
	public void validarPersist(FormIN FormIN) {

	}

	@Override
	public void validarMerge(FormIN FormIN) {

	}

	@Override
	public void validarDelete(FormIN FormIN) {

	}

}
