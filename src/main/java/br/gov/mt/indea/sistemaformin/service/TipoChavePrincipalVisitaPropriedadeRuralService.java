package br.gov.mt.indea.sistemaformin.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.mt.indea.sistemaformin.entity.TipoChavePrincipalVisitaPropriedadeRural;
import br.gov.mt.indea.sistemaformin.enums.Dominio.AtivoInativo;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class TipoChavePrincipalVisitaPropriedadeRuralService extends PaginableService<TipoChavePrincipalVisitaPropriedadeRural, Long> {

	private static final Logger log = LoggerFactory.getLogger(TipoChavePrincipalVisitaPropriedadeRuralService.class);
	
	protected TipoChavePrincipalVisitaPropriedadeRuralService() {
		super(TipoChavePrincipalVisitaPropriedadeRural.class);
	}
	
	public TipoChavePrincipalVisitaPropriedadeRural findByIdFetchAll(Long id){
		TipoChavePrincipalVisitaPropriedadeRural tipoChavePrincipalVisitaPropriedadeRural;
		
		StringBuilder sql = new StringBuilder();
		sql.append("select v ")
		   .append("  from TipoChavePrincipalVisitaPropriedadeRural v ")
		   .append(" where v.id = :id ");
		
		Query query = getSession().createQuery(sql.toString()).setLong("id", id);
		tipoChavePrincipalVisitaPropriedadeRural = (TipoChavePrincipalVisitaPropriedadeRural) query.uniqueResult();
		
		return tipoChavePrincipalVisitaPropriedadeRural;
	}
	
	@SuppressWarnings("unchecked")
	public List<TipoChavePrincipalVisitaPropriedadeRural> findAllByStatus(AtivoInativo status) {
		List<TipoChavePrincipalVisitaPropriedadeRural> tipoChavePrincipalVisitaPropriedadeRural;
		
		StringBuilder sql = new StringBuilder();
		sql.append("select v ")
		   .append("  from TipoChavePrincipalVisitaPropriedadeRural v ")
		   .append(" where v.status = :status ");
		
		Query query = getSession().createQuery(sql.toString()).setParameter("status", status);
		tipoChavePrincipalVisitaPropriedadeRural = query.list();
		
		return tipoChavePrincipalVisitaPropriedadeRural;
	}
	
	@Override
	public void saveOrUpdate(TipoChavePrincipalVisitaPropriedadeRural tipoChavePrincipalVisitaPropriedadeRural) {
		super.saveOrUpdate(tipoChavePrincipalVisitaPropriedadeRural);
		
		log.info("Salvando Tipo Chave Principal Visita Propriedade Rural {}", tipoChavePrincipalVisitaPropriedadeRural.getId());
	}
	
	@Override
	public void delete(TipoChavePrincipalVisitaPropriedadeRural tipoChavePrincipalVisitaPropriedadeRural) {
		super.delete(tipoChavePrincipalVisitaPropriedadeRural);
		
		log.info("Removendo Tipo Chave Principal Visita Propriedade Rural {}", tipoChavePrincipalVisitaPropriedadeRural.getId());
	}
	
	@Override
	public void validar(TipoChavePrincipalVisitaPropriedadeRural TipoChavePrincipalVisitaPropriedadeRural) {

	}

	@Override
	public void validarPersist(TipoChavePrincipalVisitaPropriedadeRural TipoChavePrincipalVisitaPropriedadeRural) {

	}

	@Override
	public void validarMerge(TipoChavePrincipalVisitaPropriedadeRural TipoChavePrincipalVisitaPropriedadeRural) {

	}

	@Override
	public void validarDelete(TipoChavePrincipalVisitaPropriedadeRural TipoChavePrincipalVisitaPropriedadeRural) {

	}

}
