package br.gov.mt.indea.sistemaformin.managedbeans.constants;

public class WizardNotificacaoConstants {

	public static final String TIPO_INFORMANTE = "tipoInformante";
	
	public static final String NOME_INFORMANTE = "nomeInformante";
	
	public static final String EMAIL_INFORMANTE = "emailInformante";
	
	public static final String TELEFONE_INFORMANTE = "telefoneInformante";
	
	public static final String MOTIVO_NOTIFICACAO = "motivoNotificacao";
	
	public static final String RESUMO_NOTIFICACAO = "resumoNotificacao";
	
	public static final String NOME_ESTABELECIMENTO = "nomeEstabelecimento";
	
	public static final String ENDERECO_ESTABELECIMENTO = "enderecoEstabelecimento";
	
	public static final String CONHECE_CONTATO_ESTABELECIMENTO = "conheceContatoEstabelecimento";
	
	public static final String NOME_CONTATO_ESTABELECIMENTO = "nomeContatoEstabelecimento";
	
	public static final String TELEFONE_CONTATO_ESTABELECIMENTO = "telefoneContatoEstabelecimento";
	
	public static final String TIPO_CONTATO_ESTABELECIMENTO = "tipoContatoEstabelecimento";
	
	public static final String RELACAO_NOTIFICANTE_PROPRIEDADE = "relacaoDoNotificanteComAPropriedade";
	
	public static final String RESUMO = "resumo";
	
}
