package br.gov.mt.indea.sistemaformin.dao;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.inject.Inject;


import br.gov.mt.indea.sistemaformin.entity.HistoricoFormIN;
import br.gov.mt.indea.sistemaformin.exception.ApplicationException;

@Stateless
public class HistoricoFormINDAO extends DAO<HistoricoFormIN> {
	
	@Inject
	private EntityManager em;
	
	public HistoricoFormINDAO() {
		super(HistoricoFormIN.class);
	}

	@Override
	public EntityManager getEntityManager() {
		return this.em;
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void add(HistoricoFormIN historicoFormIN) throws ApplicationException{
		historicoFormIN = this.getEntityManager().merge(historicoFormIN);
		super.add(historicoFormIN);
	}

}
