package br.gov.mt.indea.sistemaformin.service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.transform.Transformers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.mt.indea.sistemaformin.entity.InvestigacaoEpidemiologica;
import br.gov.mt.indea.sistemaformin.entity.Municipio;
import br.gov.mt.indea.sistemaformin.entity.NumeroVigilanciaVeterinaria;
import br.gov.mt.indea.sistemaformin.entity.VigilanciaVeterinaria;
import br.gov.mt.indea.sistemaformin.entity.dto.AbstractDTO;
import br.gov.mt.indea.sistemaformin.entity.dto.VigilanciaVeterinariaDTO;
import br.gov.mt.indea.sistemaformin.entity.view.RelatorioVigilanciaVeterinariaGeral;
import br.gov.mt.indea.sistemaformin.exception.ApplicationException;
import br.gov.mt.indea.sistemaformin.exception.ApplicationRuntimeException;
import br.gov.mt.indea.sistemaformin.util.StringUtil;
import br.gov.mt.indea.sistemaformin.webservice.entity.Exploracao;
import br.gov.mt.indea.sistemaformin.webservice.entity.Produtor;
import br.gov.mt.indea.sistemaformin.webservice.entity.ULE;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class VigilanciaVeterinariaService extends PaginableService<VigilanciaVeterinaria, Long> {
	
	private static final Logger log = LoggerFactory.getLogger(VigilanciaVeterinariaService.class);

	@Inject
	private NumeroVigilanciaVeterinariaService numeroVigilanciaVeterinariaService;

	protected VigilanciaVeterinariaService() {
		super(VigilanciaVeterinaria.class);
	}
	
	public VigilanciaVeterinaria findByIdFetchAll(Long id){
		VigilanciaVeterinaria vigilanciaVeterinaria;
		
		StringBuilder sql = new StringBuilder();
		sql.append("select vigilancia ")
		   .append("  from VigilanciaVeterinaria vigilancia ")
		   .append("  join fetch vigilancia.motivosVigilanciaVeterinaria ")
		   .append("  join fetch vigilancia.propriedade propriedade")
		   .append("  left join fetch propriedade.endereco ")
		   .append("  left join fetch propriedade.municipio ")
		   .append("  left join fetch vigilancia.proprietario proprietario")
		   .append("  left join fetch proprietario.endereco")
		   .append("  left join fetch propriedade.coordenadaGeografica ")
		   .append("  join fetch vigilancia.veterinario vet")
		   .append("  join fetch vet.conselho")
		   .append("  join fetch vet.tipoEmitente")
		   .append("  left join fetch vet.ule uu")
		   .append("  left join fetch uu.urs")
		   .append("  left join fetch vet.endereco")
		   .append("  left join fetch vet.municipioNascimento")
		   .append("  left join fetch vigilancia.vigilanciaAlimentosRuminantes ")
		   .append("  left join fetch vigilancia.vigilanciaBovinosEBubalinos ")
		   .append("  left join fetch vigilancia.vigilanciaSuideos ")
		   .append("  left join fetch vigilancia.vigilanciaOutrasEspecies ")
		   .append(" where vigilancia.id = :id ")
		   .append(" order by vigilancia.dataVigilancia ");
		
		Query query = getSession().createQuery(sql.toString()).setLong("id", id);
		vigilanciaVeterinaria = (VigilanciaVeterinaria) query.uniqueResult();
		
		if (vigilanciaVeterinaria != null){
		
			Hibernate.initialize(vigilanciaVeterinaria.getPropriedade().getProprietarios());
			for (Produtor produtor : vigilanciaVeterinaria.getPropriedade().getProprietarios()) {
				Hibernate.initialize(produtor.getMunicipioNascimento());
				Hibernate.initialize(produtor.getEndereco());
				Hibernate.initialize(produtor.getEndereco().getMunicipio());
				Hibernate.initialize(produtor.getEndereco().getMunicipio().getUf());
			}
			
			Hibernate.initialize(vigilanciaVeterinaria.getPropriedade().getUle());
			Hibernate.initialize(vigilanciaVeterinaria.getPropriedade().getExploracaos());
			Hibernate.initialize(vigilanciaVeterinaria.getPontosDeRisco());
			Hibernate.initialize(vigilanciaVeterinaria.getListaVigilanciaAves());
			Hibernate.initialize(vigilanciaVeterinaria.getListaInvestigacaoEpidemiologica());
			if (vigilanciaVeterinaria.getListaInvestigacaoEpidemiologica() != null)
				for (InvestigacaoEpidemiologica investigacao : vigilanciaVeterinaria.getListaInvestigacaoEpidemiologica()){
					Hibernate.initialize(investigacao.getListaDoencas());
				}
	
			if (vigilanciaVeterinaria.getVigilanciaAlimentosRuminantes() != null){
				Hibernate.initialize(vigilanciaVeterinaria.getVigilanciaAlimentosRuminantes().getTipoExploracaoOutrosRuminantes());
				Hibernate.initialize(vigilanciaVeterinaria.getVigilanciaAlimentosRuminantes().getSistemaDeCriacaoDeRuminantes2());
			}
			
			if (vigilanciaVeterinaria.getVigilanciaSuideos() != null){
				Hibernate.initialize(vigilanciaVeterinaria.getVigilanciaSuideos().getListaTipoAlimentoSuideos());
				Hibernate.initialize(vigilanciaVeterinaria.getVigilanciaSuideos().getListaTipoOrigemSuideos());
			}
			
			for (Exploracao exploracao : vigilanciaVeterinaria.getPropriedade().getExploracaos()){
				Hibernate.initialize(exploracao.getProdutores());
			}
			
		}
		
		return vigilanciaVeterinaria;
	}
	
	@Override
	public Long countAll(AbstractDTO dto) {
		StringBuilder sql = new StringBuilder();
		sql.append("select count(entity) ")
		   .append("  from " + this.getType().getSimpleName() + " as entity")
		   .append("  join entity.propriedade propriedade")
		   .append("  join propriedade.municipio ")
		   .append(" where entity.codigoVerificador is null");
		
		if (dto != null && !dto.isNull()){
			VigilanciaVeterinariaDTO vigilanciaVeterinariaDTO = (VigilanciaVeterinariaDTO) dto;
			
			if (vigilanciaVeterinariaDTO.getMunicipio() != null)
				sql.append("  and propriedade.municipio = :municipio");
			if (vigilanciaVeterinariaDTO.getNumero() != null && !vigilanciaVeterinariaDTO.getNumero().equals(""))
				sql.append("  and entity.numero = :numero");
			if (vigilanciaVeterinariaDTO.getPropriedade() != null && !vigilanciaVeterinariaDTO.getPropriedade().equals(""))
				sql.append("  and lower(remove_acento(propriedade.nome)) like :propriedade");
			if (vigilanciaVeterinariaDTO.getData() != null){
				sql.append("  and to_char(entity.dataVigilancia, 'YYYY-MM-DD HH:MI:SS') between :inicio and :final");
			}
		}
		
		Query query = getSession().createQuery(sql.toString());

		if (dto != null && !dto.isNull()){
			VigilanciaVeterinariaDTO vigilanciaVeterinariaDTO = (VigilanciaVeterinariaDTO) dto;
			
			if (vigilanciaVeterinariaDTO.getMunicipio() != null)
				query.setParameter("municipio", vigilanciaVeterinariaDTO.getMunicipio());
			if (vigilanciaVeterinariaDTO.getNumero() != null && !vigilanciaVeterinariaDTO.getNumero().equals(""))
				query.setString("numero", vigilanciaVeterinariaDTO.getNumero());
			if (vigilanciaVeterinariaDTO.getPropriedade() != null && !vigilanciaVeterinariaDTO.getPropriedade().equals(""))
				query.setString("propriedade", StringUtil.removeAcentos('%' + vigilanciaVeterinariaDTO.getPropriedade().toLowerCase()) + '%');
			
			if (vigilanciaVeterinariaDTO.getData() != null){
				Calendar i = Calendar.getInstance();
				i.setTime(vigilanciaVeterinariaDTO.getData());
				
				StringBuilder sbI = new StringBuilder();
				sbI.append(i.get(Calendar.YEAR)).append("-")
				   .append(((i.get(Calendar.MONTH) + 1) + "").length() > 1 ? (i.get(Calendar.MONTH)+1) : "0" + (i.get(Calendar.MONTH)+1)).append("-")
				   .append((i.get(Calendar.DAY_OF_MONTH) + "").length() > 1 ? i.get(Calendar.DAY_OF_MONTH) : "0" + i.get(Calendar.DAY_OF_MONTH))
				   .append(" ")
				   .append("00").append(":")
				   .append("00").append(":")
				   .append("00");
				
				Calendar f = Calendar.getInstance();
				f.setTime(vigilanciaVeterinariaDTO.getData());
				
				StringBuilder sbF = new StringBuilder();
				sbF.append(f.get(Calendar.YEAR)).append("-")
				   .append(((f.get(Calendar.MONTH) + 1) + "").length() > 1 ? (f.get(Calendar.MONTH)+1) : "0" + (f.get(Calendar.MONTH)+1)).append("-")
				   .append((f.get(Calendar.DAY_OF_MONTH) + "").length() > 1 ? f.get(Calendar.DAY_OF_MONTH) : "0" + f.get(Calendar.DAY_OF_MONTH))
				   .append(" ")
				   .append("23").append(":")
				   .append("23").append(":")
				   .append("59");
				
				query.setString("inicio", sbI.toString());
				query.setString("final", sbF.toString());
			}
		}
		
		return (Long) query.uniqueResult();
	}
	
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<VigilanciaVeterinaria> findAllComPaginacao(AbstractDTO dto, int first, int pageSize, String sortField, String sortOrder) {
		StringBuilder sql = new StringBuilder();
		sql.append("select entity ")
		   .append("  from " + this.getType().getSimpleName() + " as entity")
		   .append("  join fetch entity.propriedade propriedade")
		   .append("  join fetch propriedade.municipio ")
		   .append("  join fetch entity.veterinario ")
		   .append(" where entity.codigoVerificador is null");
		
		if (dto != null && !dto.isNull()){
			VigilanciaVeterinariaDTO vigilanciaVeterinariaDTO = (VigilanciaVeterinariaDTO) dto;
			
			if (vigilanciaVeterinariaDTO.getMunicipio() != null)
				sql.append("  and propriedade.municipio = :municipio");
			if (vigilanciaVeterinariaDTO.getNumero() != null && !vigilanciaVeterinariaDTO.getNumero().equals(""))
				sql.append("  and entity.numero = :numero");
			if (vigilanciaVeterinariaDTO.getPropriedade() != null && !vigilanciaVeterinariaDTO.getPropriedade().equals(""))
				sql.append("  and lower(remove_acento(propriedade.nome)) like :propriedade");
			if (vigilanciaVeterinariaDTO.getData() != null){
				sql.append("  and to_char(entity.dataVigilancia, 'YYYY-MM-DD HH:MI:SS') between :inicio and :final");
			}
		}
		
		if (StringUtils.isNotBlank(sortField))
			sql.append(" order by entity." + sortField + " " + sortOrder);

		Query query = getSession().createQuery(sql.toString());

		if (dto != null && !dto.isNull()){
			VigilanciaVeterinariaDTO vigilanciaVeterinariaDTO = (VigilanciaVeterinariaDTO) dto;
			
			if (vigilanciaVeterinariaDTO.getMunicipio() != null)
				query.setParameter("municipio", vigilanciaVeterinariaDTO.getMunicipio());
			if (vigilanciaVeterinariaDTO.getNumero() != null && !vigilanciaVeterinariaDTO.getNumero().equals(""))
				query.setString("numero", vigilanciaVeterinariaDTO.getNumero());
			if (vigilanciaVeterinariaDTO.getPropriedade() != null && !vigilanciaVeterinariaDTO.getPropriedade().equals(""))
				query.setString("propriedade", StringUtil.removeAcentos('%' + vigilanciaVeterinariaDTO.getPropriedade().toLowerCase()) + '%');
			
			if (vigilanciaVeterinariaDTO.getData() != null){
				Calendar i = Calendar.getInstance();
				i.setTime(vigilanciaVeterinariaDTO.getData());
				
				StringBuilder sbI = new StringBuilder();
				sbI.append(i.get(Calendar.YEAR)).append("-")
				   .append(((i.get(Calendar.MONTH) + 1) + "").length() > 1 ? (i.get(Calendar.MONTH)+1) : "0" + (i.get(Calendar.MONTH)+1)).append("-")
				   .append((i.get(Calendar.DAY_OF_MONTH) + "").length() > 1 ? i.get(Calendar.DAY_OF_MONTH) : "0" + i.get(Calendar.DAY_OF_MONTH))
				   .append(" ")
				   .append("00").append(":")
				   .append("00").append(":")
				   .append("00");
				
				Calendar f = Calendar.getInstance();
				f.setTime(vigilanciaVeterinariaDTO.getData());
				
				StringBuilder sbF = new StringBuilder();
				sbF.append(f.get(Calendar.YEAR)).append("-")
				   .append(((f.get(Calendar.MONTH) + 1) + "").length() > 1 ? (f.get(Calendar.MONTH)+1) : "0" + (f.get(Calendar.MONTH)+1)).append("-")
				   .append((f.get(Calendar.DAY_OF_MONTH) + "").length() > 1 ? f.get(Calendar.DAY_OF_MONTH) : "0" + f.get(Calendar.DAY_OF_MONTH))
				   .append(" ")
				   .append("23").append(":")
				   .append("23").append(":")
				   .append("59");
				
				query.setString("inicio", sbI.toString());
				query.setString("final", sbF.toString());
			}
		}
		
		query.setFirstResult(first);
		query.setMaxResults(pageSize);

		List<VigilanciaVeterinaria> lista = query.list();
		
		if (lista != null)
			for (VigilanciaVeterinaria vigilanciaVeterinaria : lista) {
				if (vigilanciaVeterinaria.getPropriedade() != null)
					vigilanciaVeterinaria.getPropriedade().getNome();
			}
		
		return lista;
    }
	
	@Override
	public void saveOrUpdate(VigilanciaVeterinaria vigilanciaVeterinaria) {
		if (vigilanciaVeterinaria.getNumero() == null)
			try {
				vigilanciaVeterinaria.setNumero(getProximoNumeroVigilanciaVeterinaria(vigilanciaVeterinaria));
			} catch (ApplicationException e) {
				throw new ApplicationRuntimeException(e);
			}
		
		vigilanciaVeterinaria = (VigilanciaVeterinaria) this.getSession().merge(vigilanciaVeterinaria);
		super.saveOrUpdate(vigilanciaVeterinaria);
		
		log.info("Salvando Vigilancia Veterinaria {}", vigilanciaVeterinaria.getId());
	}
	
	@Override
	public void delete(VigilanciaVeterinaria vigilanciaVeterinaria) {
		super.delete(vigilanciaVeterinaria);
		
		log.info("Removendo Vigilancia Veterinaria {}", vigilanciaVeterinaria.getId());
	}
	
	private String getProximoNumeroVigilanciaVeterinaria(VigilanciaVeterinaria vigilanciaVeterinaria) throws ApplicationException{
		StringBuilder sb = new StringBuilder();
		
		if (vigilanciaVeterinaria.getPropriedade().getMunicipio().getUf() == null)
			throw new ApplicationException("A UF da Vigil�ncia veterin�ria n�o pode ser nula.");
		else if (vigilanciaVeterinaria.getPropriedade().getMunicipio().getUf().getCodgIBGE() == null)
			throw new ApplicationException("A UF informada n�o possui c�digo do IBGE");
		
		if (vigilanciaVeterinaria.getPropriedade().getMunicipio() == null)
			throw new ApplicationException("O Munic�pio da Vigil�ncia veterin�ria n�o pode ser nulo.");
		else if (vigilanciaVeterinaria.getPropriedade().getMunicipio().getCodgIBGE() == null)
			throw new ApplicationException("O Munic�pio informada n�o possui c�digo do IBGE");
		
		sb.append(vigilanciaVeterinaria.getPropriedade().getMunicipio().getUf().getCodgIBGE());
		sb.append(vigilanciaVeterinaria.getPropriedade().getMunicipio().getCodgIBGE());
		sb.append("-");
		
		int ano = vigilanciaVeterinaria.getDataVigilancia().get(java.util.Calendar.YEAR);
		
		NumeroVigilanciaVeterinaria numeroVigilanciaVeterinaria = this.getNumeroVigilanciaVeterinariaByMunicipio(vigilanciaVeterinaria.getPropriedade().getMunicipio(), ano);
		if (numeroVigilanciaVeterinaria.getUltimoNumero() >= 9999)
			throw new ApplicationException("A numera��o de Form IN para o munic�pio de " + vigilanciaVeterinaria.getPropriedade().getMunicipio().getNome() + " j� atingiu o limite de 9999");
		
		String proximoNumero = String.format("%04d", numeroVigilanciaVeterinaria.getUltimoNumero());
		sb.append(proximoNumero);
	    sb.append("/");
	    sb.append(ano);
		
		return sb.toString();
	}
	
	private NumeroVigilanciaVeterinaria getNumeroVigilanciaVeterinariaByMunicipio(Municipio municipio, int ano) throws ApplicationException{
		NumeroVigilanciaVeterinaria numeroVigilanciaVeterinaria = numeroVigilanciaVeterinariaService.getNumeroVigilanciaVeterinariaByMunicipio(municipio, ano);
		
		if (numeroVigilanciaVeterinaria == null){
			numeroVigilanciaVeterinaria = new NumeroVigilanciaVeterinaria();
			numeroVigilanciaVeterinaria.setMunicipio(municipio);
			numeroVigilanciaVeterinaria.setUltimoNumero(0L);
			numeroVigilanciaVeterinaria.setAno((long) ano);
			
			numeroVigilanciaVeterinariaService.saveOrUpdate(numeroVigilanciaVeterinaria);
		}
		
		numeroVigilanciaVeterinaria.incrementNumeroVigilanciaVeterinaria();
		numeroVigilanciaVeterinariaService.merge(numeroVigilanciaVeterinaria);
		
		return numeroVigilanciaVeterinaria;
	}
	
	@SuppressWarnings("unchecked")
	public List<Object[]> relatorio(Date dataInicial, Date dataFinal, ULE urs, ULE ule){
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT urs.nome, ")
		   .append("       m.nome as \"Municipio\", ")
		   .append("       v.numero AS \"N�mero\", ")
		   .append("       to_char(v.data_vigilancia, 'dd/mm/yyyy') as \"Data da vigil�ncia\", ")
		   .append("       to_char(v.data_cadastro, 'dd/mm/yyyy ') as \"Data de cadastro\", ")
		   .append("       vet.nome as \"Respons�vel\", ")
		   .append("       p.nome as \"Propriedade\", ")
		   .append("       p.codigo as \"C�digo da propriedade\", ")
		   .append("       prop.nome as \"Proprietario\", ")
		   .append("       c.latitude_grau as \"Latitude - Grau\", ")
		   .append("       c.latitude_minuto as \"Latitude - Minuto\", ")
		   .append("       c.latitude_segundo as \"Latitude - Segundo\", ")
		   .append("       c.longitude_grau as \"Longitude - Grau\", ")
		   .append("       c.longitude_minuto as \"Longitude - Minuto\", ")
		   .append("       c.longitude_segundo as \"Longitude - Segundo\", ")
		   .append(" ")
		   .append("       s4.motivos as \"Principais motivos da Visita\", ")
		   .append(" ")
		   .append("       var.finalidade_exp_ruminantes || ',' || (select z1.tipo_exploracao_outros_ruminantes  ")
		   .append("						  from (select tex.id_vigilancia_bov_bub, ")
		   .append("						       array_to_string(array_agg(DISTINCT tex.id_vig_tipo_expl_outro_rum), ',') AS tipo_exploracao_outros_ruminantes ")
		   .append("						  from vig_tipo_expl_outro_rum tex ")
		   .append("						 where tex.id_vigilancia_bov_bub = var.id ")
		   .append("						 group by 1) as z1) as \"Tipo de Explora��o de ruminantes na propriedade\", ")
		   .append(" ")
		   .append("       var.sistema_criacao_ruminantes || ',' || (select z1.sistema_criacao_ruminantes  ")
		   .append("						   from (select sis.id_vigilancia_bov_bub, ")
		   .append("						        array_to_string(array_agg(DISTINCT sis.id_vigilancia_fase_expl_bov), ',') AS sistema_criacao_ruminantes ")
		   .append("						   from vigilancia_fase_expl_bov sis ")
		   .append("						  where sis.id_vigilancia_bov_bub = var.id ")
		   .append("						  group by 1) as z1) as \"Sistema de Cria��o de ruminantes\", ")
		   .append(" ")
		   .append("       var.quantidade_bovinos as \"N� total de ruminantes: (Bovinos)\", ")
		   .append("       var.quantidade_caprinos as \"N� total de ruminantes: (Caprinos)\", ")
		   .append("       var.quantidade_ovinos as \"N� total de ruminantes: (Oovinos)\", ")
		   .append("       var.outra_especie_ruminante || '-' || var.quantidade_outra_esp_rumin as \"N� total de ruminantes: (Outros)\", ")
		   .append("       var.idade_rumin_alim_com_racao as \"Idade dos ruminantes alimentados com ra��o / suplementos\", ")
		   .append("       var.quantidade_bovinos_exp as \"N� total de ruminantes expostos ao alimento ora fiscalizado (Bovinos)\", ")
		   .append("       var.quantidade_caprinos_exp as \"N� total de ruminantes expostos ao alimento ora fiscalizado (Caprinos)\", ")
		   .append("       var.quantidade_ovinos_exp as \"N� total de ruminantes expostos ao alimento ora fiscalizado (Ovinos)\", ")
		   .append("       var.tipo_alimentacao_ruminantes as \"Tipo de Alimenta��o\", ")
		   .append("       var.tipo_exp_avicola_sist_indust as \"H� cria��o av�cola em sistema industrial na propriedade\", ")
		   .append("       var.epoca_ano_suplementacao as \"�poca do ano em que acontece a suplementa��o\", ")
		   .append("       var.presenca_cama_aviario as \"Presen�a de cama de avi�rio na propriedade\", ")
		   .append("       var.observacao_cama_aviario as \"Se tiver cama de avi�rio, onde est� sendo utilizada\", ")
		   .append("       var.cama_aviario_alimentacao_rum as \"Relato de utiliza��o de cama de avi�rio na alimenta��o de ruminantes\", ")
		   .append("       var.tipo_exp_suino_sist_indust as \"H� cria��o de su�nos em sistema industrial\", ")
		   .append("       var.piscicultura_aliment_base_racao as \"Piscicultura com sistema de alimenta��o a base de ra��o\", ")
		   .append("       var.contaminacao_racaoPeixe_rumin as \"Ind�cios de contamina��o cruzada de ra��o de peixe X ra��o de ruminante\", ")
		   .append("       var.colheita_amostra_alimento_rum as \"Colheita de amostra de alimentos de ruminantes\", ")
		   .append("       var.tipo_fiscalizacao as \"Tipo da fiscaliza��o\", ")
		   .append("       var.colheita_amostra_alimento_rum as \"Houve coleta de amostra\", ")
		   .append("       var.numero_denuncia as \"N�mero da den�cia\", ")
		   .append("       var.descricao_racoes as \"Descri��o do sistema de armazenamento e elabora��o de ra��es para animais na propriedade\", ")
		   .append("       var.outras_observacoes as \"Outras observa��es\", ")
		   .append(" ")
		   .append("       s1.pontos as \"Tipos de Risco\", ")
		   .append("        ")
		   .append("       (vbb.quantidade_inspec_00_04_f + ")
		   .append("		vbb.quantidade_inspec_00_04_m + ")
		   .append("		vbb.quantidade_inspec_05_12_f + ")
		   .append("		vbb.quantidade_inspec_05_12_m + ")
		   .append("		vbb.quantidade_inspec_13_24_f + ")
		   .append("		vbb.quantidade_inspec_13_24_m + ")
		   .append("		vbb.quantidade_inspec_25_36_f + ")
		   .append("		vbb.quantidade_inspec_25_36_m + ")
		   .append("		vbb.quantidade_inspec_acima36_f + ")
		   .append("		vbb.quantidade_inspec_acima36_m) AS \"Quantidade de ruminantes inspecionados\", ")
		   .append("	(vbb.quantidade_vistoriados) AS \"Quantidade de ruminantes vistoriados\", ")
		   .append("	vbb.quantidade_leiteiras as \"Leiteras existentes\", ")
		   .append("	vbb.quantidade_reprodutoras as \"Reprodutoras existentes\", ")
		   .append("	vbb.porcentagem_natalidade as \"% natalidade\", ")
		   .append(" ")
		   .append("	(vs.quantidade_vistoriados) AS \"Quantidade de su�deos vistoriados\", ")
		   .append("	(vs.quantidade_inspec_ate_30_f + ")
		   .append("		vs.quantidade_inspec_ate_30_m + ")
		   .append("		vs.quantidade_inspec_31_60_f + ")
		   .append("		vs.quantidade_inspec_31_60_m + ")
		   .append("		vs.quantidade_inspec_61_180_f + ")
		   .append("		vs.quantidade_inspec_61_180_m + ")
		   .append("		vs.quantidade_inspec_acima180_f+  ")
		   .append("		vs.quantidade_inspec_acima180_m) AS \"Quantidade de su�deos inspecionados\", ")
		   .append("	vs.presenca_suinos_asselvajados as \"Presen�a de su�nos asselvajados\", ")
		   .append("	vs.contato_suinos_domest_e_assel as \"Contato direto dos su�nos dom�sticos com asselvajados\", ")
		   .append("	vs.prop_prox_comunidade_carente as \"Propriedade pr�xima a comunidades carentes\", ")
		   .append("	vs.prop_prox_quarentenario as \"Propriedade pr�xima a quarenten�rio de su�nos\", ")
		   .append("	vs.prop_suideos_criados_extensiv as \"Possui su�deos criados extensivamente\", ")
		   .append("	vs.prop_com_lavagem_de_terceiros as \"Fornece res�duos alimentares de terceiros aos su�nos\", ")
		   .append("	vs.dono_tem_prop_em_area_endem as \"Propriet�rio possui propriedade em outro pa�s ou em �rea end�mica para PSC\", ")
		   .append("	vs.finalidade_explocarao_suinos as \"Finalidade\", ")
		   .append("	vs.tipo_alimento_suideos as \"Alimenta��o\", ")
		   .append("	vs.tipo_origem_suideos as \"Origem dos animais\", ")
		   .append("	vs.tipo_destino_suideos as \"Destino dos animais\", ")
		   .append("	vs.tipo_destino_cadaveres as \"Destino dos cad�veres\", ")
		   .append("	vs.hipertermia as \"Sintoma hipertermia\", ")
		   .append("	vs.porc_hipertermia as \"% hipertermia\", ")
		   .append("	vs.incoordenacao_motora as \"Sintoma incoordena��o motora\", ")
		   .append("	vs.porc_incoordenacao_motora as \"% incoordena��o motora\", ")
		   .append("	vs.extremidades_cianoticas as \"Sintoma extremidades cian�ticas\", ")
		   .append("	vs.porc_extremidades_cianoticas as \"% extremidades cian�ticas\", ")
		   .append("	vs.vomitos as \"Sintoma vomitos\", ")
		   .append("	vs.porc_vomitos as \"% vomitos\", ")
		   .append("	vs.diarreia as \"Sintoma diarreia\", ")
		   .append("	vs.porc_diarreia as \"% diarreia\", ")
		   .append("	vs.anorexia as \"Sintoma anorexia\", ")
		   .append("	vs.porc_anorexia as \"% anorexia\", ")
		   .append("	vs.abortos as \"Sintoma abortos\", ")
		   .append("	vs.porc_abortos as \"% abortos\", ")
		   .append("	vs.leitoes_natimortos as \"Sintoma leitoes_natimortos\", ")
		   .append("	vs.porc_leitoes_natimortos as \"% leitoes_natimortos\", ")
		   .append("	vs.agrupamentos_nas_pocilgas as \"Sintoma agrupamentos nas pocilgas\", ")
		   .append("	vs.porc_agrupamentos_nas_pocilgas as \"% agrupamentos nas pocilgas\", ")
		   .append("	vs.hemorragias_e_petequias as \"Sintoma hemorragias e petequias\", ")
		   .append("	vs.porc_hemorragias_e_petequias as \"% hemorragias e petequias\", ")
		   .append("	vs.cresc_retardado_de_leitoes as \"Sintoma cresc retardado de leitoes\", ")
		   .append("	vs.porc_cresc_retardado_de_leitoes as \"% cresc retardado de leitoes\", ")
		   .append("	vs.mortalidade_mensal as \"Sintoma mortalidade mensal\", ")
		   .append("	vs.porc_mortalidade_mensal as \"% mortalidade mensal\", ")
		   .append("	 ")
		   .append("        s2.qtdade_aves_inspecionados as \"Aves: Quantidade total inspecionada\", ")
		   .append("        s2.qtdade_aves_vistoriados as \"Aves: Quantidade total vistoriada\", ")
		   .append(" ")
		   .append("    quantidade_ovinos_insp_f + quantidade_ovinos_insp_m AS \"Ovinos: Quantidade total inspecionada\", ")
		   .append("	quantidade_ovinos_vist AS \"Ovinos: Quantidade total vistoriada\", ")
		   .append("	quantidade_caprinos_insp_f + quantidade_caprinos_insp_m AS \"Caprinos: Quantidade total inspecionada\", ")
		   .append("	quantidade_caprinos_vist \"Caprinos: Quantidade total vistoriada\", ")
		   .append("	quantidade_equinos_insp_f + quantidade_equinos_insp_m + quantidade_asininos_insp_f + quantidade_asininos_insp_m + quantidade_muares_insp_f + quantidade_muares_insp_m AS \"Equ�deos: Quantidade total inspecionada\", ")
		   .append("	quantidade_equinos_vist + quantidade_asininos_vist + quantidade_muares_vist AS \"Equ�deos: Quantidade total vistoriada\", ")
		   .append("	quantidade_peixes_insp as \"Peixes: Quantidade total inspecionada\", ")
		   .append("	quantidade_peixes_vist as \"Peixes: Quantidade total vistoriada\", ")
		   .append(" ")
		   .append("	s3.doencas as \"Poss�veis doen�as encontradas\", ")
		   .append(" ")
		   .append("	v.historico_sugadura_morcegos as \"Hist�rico de sugaduras de morcegos\", ")
		   .append("	v.observacao as \"Observa��o\", ")
		   .append("	v.emissao_form_in as \"Houve emiss�o de Form In\" ")
		   .append("        ")
		   .append("  FROM vigilancia_veterinaria v ")
		   .append(" ")
		   .append("  LEFT OUTER JOIN vigilancia_alimentos_rumin var ON var.id = v.id_vigilancia_alim_rumin ")
		   .append(" ")
		   .append("  LEFT OUTER JOIN (SELECT pr.id_vigilancia_veterinaria, ")
		   .append("		          array_to_string(array_agg(DISTINCT pr.id_vig_vet_ponto_de_risco), ',') AS pontos ")
		   .append("		     FROM vig_vet_ponto_de_risco pr ")
		   .append("		     GROUP BY 1) AS s1 ON s1.id_vigilancia_veterinaria = v.id ")
		   .append(" ")
		   .append("  LEFT OUTER JOIN vigilancia_bov_bub vbb ON vbb.id = v.id_vigilancia_bov_bub ")
		   .append(" ")
		   .append("  LEFT OUTER JOIN vigilancia_suideos vs ON vs.id = v.id_vigilancia_suideos ")
		   .append(" ")
		   .append("  LEFT OUTER JOIN (SELECT sum(quantidade_inspecionados) AS qtdade_aves_inspecionados, ")
		   .append("			  sum(quantidade_vistoriados) AS qtdade_aves_vistoriados, ")
		   .append("			  id_vigilancia_veterinaria ")
		   .append("		     FROM vigilancia_aves ")
		   .append("		    GROUP BY 3) AS s2 ON s2.id_vigilancia_veterinaria = v.id ")
		   .append(" ")
		   .append("  LEFT OUTER JOIN vigilancia_outras_especies vout ON vout.id = v.id_vigilancia_outras ")
		   .append(" ")
		   .append("  LEFT OUTER JOIN (SELECT ie.id_vigilancia_veterinaria, ")
		   .append("		          array_to_string(array_agg(DISTINCT d.nome), ',') AS doencas ")
		   .append("		     FROM investigacao_epidemiologica ie, ")
		   .append("		          investigacao_doencas id, ")
		   .append("		          doenca d ")
		   .append("		    WHERE ie.id = id.id_investigacao ")
		   .append("		      AND d.id = id.id_doenca ")
		   .append("		    GROUP BY 1) AS s3 ON s3.id_vigilancia_veterinaria = v.id ")
		   .append("  LEFT OUTER JOIN (select vv.id, ")
		   .append("		          array_to_string(array_agg(DISTINCT tc.nome), ',') AS motivos        ")
		   .append("		     from vigilancia_veterinaria vv, ")
		   .append("		          motivo_vigilancia mv, ")
		   .append("		          motivo_tipo_chave_principal mtc, ")
		   .append("		          tipo_chave_principal_visita tc ")
		   .append("		    where mv.id = vv.id_motivos_vigilancia ")
		   .append("		      and mv.id = mtc.id_motivo_vigilancia ")
		   .append("		      and mtc.id_tipo_chave_principal = tc.id ")
		   .append("		    group by 1) AS s4 ON s4.id = v.id, ")
		   .append(" ")
		   .append(" ")
		   .append("   ")
		   .append("       pessoa vet, ")
		   .append("       propriedade p, ")
		   .append("       unidade ule, ")
		   .append("       unidade urs, ")
		   .append("       pessoa prop, ")
		   .append("       municipio m, ")
		   .append("       coordenadageografica c ")
		   .append(" WHERE v.id_veterinario = vet.id ")
		   .append("   AND v.id_propriedade = p.id ")
		   .append("   AND p.id_produtor = prop.id ")
		   .append("   AND p.id_municipio = m.id ")
		   .append("   AND p.id_unidade = ule.id ")
		   .append("   AND ule.id_unidade_pai = urs.id ")
		   .append("   AND to_char(v.data_vigilancia, 'YYYY-MM-DD HH:MI:SS') between :dataInicial and :dataFinal ")
		   .append("   AND p.id_coordenada_geografica = c.id ");
		
		if (urs != null)
			sql.append("   AND urs.id = :idURS");
		if (ule != null)
			sql.append("   AND ule.id = :idULE");
		
		sql.append(" order by  ")
		   .append("       urs.nome, ")
		   .append("       ule.nome, ")
		   .append("       v.data_vigilancia ");
		
		Query query = getSession().createSQLQuery(sql.toString());
		
		if (urs != null)
			query.setLong("idURS", urs.getId());
		if (ule != null)
			query.setLong("idULE", ule.getId());
		
		Calendar i = Calendar.getInstance();
		i.setTime(dataInicial);
		
		StringBuilder sbI = new StringBuilder();
		sbI.append(i.get(Calendar.YEAR)).append("-")
		   .append(((i.get(Calendar.MONTH) + 1) + "").length() > 1 ? (i.get(Calendar.MONTH)+1) : "0" + (i.get(Calendar.MONTH)+1)).append("-")
		   .append((i.get(Calendar.DAY_OF_MONTH) + "").length() > 1 ? i.get(Calendar.DAY_OF_MONTH) : "0" + i.get(Calendar.DAY_OF_MONTH))
		   .append(" ")
		   .append("00").append(":")
		   .append("00").append(":")
		   .append("00");
		
		Calendar f = Calendar.getInstance();
		f.setTime(dataFinal);
		
		StringBuilder sbF = new StringBuilder();
		sbF.append(f.get(Calendar.YEAR)).append("-")
		   .append(((f.get(Calendar.MONTH) + 1) + "").length() > 1 ? (f.get(Calendar.MONTH)+1) : "0" + (f.get(Calendar.MONTH)+1)).append("-")
		   .append((f.get(Calendar.DAY_OF_MONTH) + "").length() > 1 ? f.get(Calendar.DAY_OF_MONTH) : "0" + f.get(Calendar.DAY_OF_MONTH))
		   .append(" ")
		   .append("23").append(":")
		   .append("23").append(":")
		   .append("59");
		
		query.setString("dataInicial", sbI.toString());
		query.setString("dataFinal", sbF.toString());
				
				
				
		return query.list();
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<RelatorioVigilanciaVeterinariaGeral> relatorioTeste(Date dataInicial, Date dataFinal){
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT * ")
		   .append("  FROM relatorio_vigilancia_geral relatorio ")
		   .append(" WHERE relatorio.data_vigilancia between :dataInicial and :dataFinal");
		
		
		Query query = getSession().createSQLQuery(sql.toString()); 
		
		query.setParameter("dataInicial", dataInicial);
		query.setParameter("dataFinal", dataFinal);
		
		query.setResultTransformer(Transformers.aliasToBean(RelatorioVigilanciaVeterinariaGeral.class));
		
		List list = query.list();
		
		return list;
	}

	@Override
	public void validar(VigilanciaVeterinaria VigilanciaVeterinaria) {

	}

	@Override
	public void validarPersist(VigilanciaVeterinaria VigilanciaVeterinaria) {

	}

	@Override
	public void validarMerge(VigilanciaVeterinaria VigilanciaVeterinaria) {

	}

	@Override
	public void validarDelete(VigilanciaVeterinaria VigilanciaVeterinaria) {

	}

}
