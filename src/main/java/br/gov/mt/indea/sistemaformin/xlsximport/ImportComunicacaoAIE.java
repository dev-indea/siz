package br.gov.mt.indea.sistemaformin.xlsximport;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFormulaEvaluator;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import br.gov.mt.indea.sistemaformin.entity.ComunicacaoAIE;
import br.gov.mt.indea.sistemaformin.entity.DetalheComunicacaoAIE;
import br.gov.mt.indea.sistemaformin.entity.Laboratorio;
import br.gov.mt.indea.sistemaformin.entity.Municipio;
import br.gov.mt.indea.sistemaformin.entity.UF;
import br.gov.mt.indea.sistemaformin.exception.XlsxFillingException;
import br.gov.mt.indea.sistemaformin.exception.XlsxFormatException;
import br.gov.mt.indea.sistemaformin.service.UFService;

@Named
public class ImportComunicacaoAIE implements Serializable{
	
	private static final long serialVersionUID = -6241893450704636654L;

	private final String MES_ANO = "M�S/ANO";
	
	private final String LABORATORIO = "LABORAT�RIO";
	
	private final String UF = "UF";
	
	private final String MUNICIPIO = "MUNIC�PIO";
	
	private final String CODIGO_DO_IBGE = "C�DIGO DO IBGE";
	
	private final String NOME_PROPRIEDADE = "NOME DA PROPRIEDADE";
	
	private final String EQUIDEOS_NA_PROPRIEDADE = "EQUIDEOS NA PROPRIEDADE";
	
	private final String EQUINOS_POSITIVOS = "EQUINOS POSITIVOS";
	
	private final String MUARES_POSITIVOS = "MUARES POSITIVOS";
	
	private final String ASININOS_POSITIVOS = "ASININOS POSITIVOS";
	
	private final String EQUINOS_NEGATIVOS = "EQUINOS NEGATIVOS";
	
	private final String MUARES_NEGATIVOS = "MUARES NEGATIVOS";
	
	private final String ASININOS_NEGATIVOS = "ASININOS NEGATIVOS";
	
	private final String EQUINOS_EXAMINADOS = "EQUINOS EXAMINADOS";
	
	private final String MUARES_EXAMINADOS = "MUARES EXAMINADOS";
	
	private final String ASININOS_EXAMINADOS = "ASININOS EXAMINADOS";
	
	private HashMap<Long, String> cabecalho;
	
	DataFormatter formatter;
	XSSFFormulaEvaluator evaluator;
	
	@Inject
	private UFService ufService;
	
	List<ErroImportacao> listaErros;
	
	public ImportComunicacaoAIE(){
		cabecalho = new HashMap<Long, String>();
		
		cabecalho.put(0L, MES_ANO);
		cabecalho.put(1L, LABORATORIO);
		cabecalho.put(2L, UF);
		cabecalho.put(3L, MUNICIPIO);
		cabecalho.put(4L, CODIGO_DO_IBGE);
		cabecalho.put(5L, NOME_PROPRIEDADE);
		cabecalho.put(6L, EQUIDEOS_NA_PROPRIEDADE);
		cabecalho.put(7L, EQUINOS_POSITIVOS);
		cabecalho.put(8L, MUARES_POSITIVOS);
		cabecalho.put(9L, ASININOS_POSITIVOS);
		cabecalho.put(10L, EQUINOS_NEGATIVOS);
		cabecalho.put(11L, MUARES_NEGATIVOS);
		cabecalho.put(12L, ASININOS_NEGATIVOS);
		cabecalho.put(13L, EQUINOS_EXAMINADOS);
		cabecalho.put(14L, MUARES_EXAMINADOS);
		cabecalho.put(15L, ASININOS_EXAMINADOS);
	}
	
	public ComunicacaoAIE importFile(XSSFWorkbook workbook, Laboratorio laboratorio) throws XlsxFormatException, XlsxFillingException{
		XSSFSheet sheet = workbook.getSheetAt(0);
		
		formatter = new DataFormatter(true);
		evaluator = workbook.getCreationHelper().createFormulaEvaluator();
        
		try {
			validateFileStructureOk(sheet);
			
			return doImport(sheet, laboratorio);
		} catch (XlsxFormatException e) {
			throw e;
		}

	}
	
	private void validateFileStructureOk(XSSFSheet sheet) throws XlsxFormatException{
		Iterator<Row> rowIterator = sheet.iterator();
		Row firstRow = rowIterator.next();;
		
		for (Cell cell : firstRow) {
			if (!cabecalho.get(cell.getColumnIndex() + 0L).equals(cell.getStringCellValue().trim()))
				throw new XlsxFormatException("Foram encontrados erros no cabe�alho do arquivo");
		}
	}
	
	@SuppressWarnings("unused")
	private ComunicacaoAIE doImport(XSSFSheet sheet, Laboratorio laboratorioUsuario) throws XlsxFillingException{
		listaErros = new ArrayList<ErroImportacao>();
		boolean isFileOk = true;
		
		Row row = null;
		ComunicacaoAIE comunicacaoAIE = new ComunicacaoAIE();
		comunicacaoAIE.setLaboratorio(laboratorioUsuario);
		
		DetalheComunicacaoAIE detalhe = null;
		Date mesAno = null;
		Calendar mesAnoCalendar;
		String laboratorio = null;
		String uf = null;
		Municipio municipio = null;
		String codigoIbge = null;
		String nomePropriedade = null;
		Double equideosNaPropriedade = null;
		Double equinosPositivos = null;
		Double muaresPositivos = null;
		Double asininosPositivos = null;
		Double equinosNegativos = null;
		Double muaresNegativos = null;
		Double asininosNegativos = null;
		Double equinosExaminados = null;
		Double muaresExaminados = null;
		Double asininosExaminados = null;
		
		Object o = null;
		
		List<UF> listaUF = ufService.findAllFetchMunicipio();
		
		Iterator<Row> rowIterator = sheet.iterator();
		rowIterator.next();
		
		while (rowIterator.hasNext()){
			row = rowIterator.next();

			detalhe = new DetalheComunicacaoAIE();
			detalhe.setComunicacaoAIE(comunicacaoAIE);
			
			for (Cell cell : row) {
				mesAno = null;
				mesAnoCalendar = Calendar.getInstance();
				laboratorio = null;
				uf = null;
				municipio = null;
				codigoIbge = null;
				nomePropriedade = null;
				equideosNaPropriedade = null;
				equinosPositivos = null;
				muaresPositivos = null;
				asininosPositivos = null;
				equinosNegativos = null;
				muaresNegativos = null;
				asininosNegativos = null;
				equinosExaminados = null;
				muaresExaminados = null;
				asininosExaminados = null;
				o = null;
				
				if (cabecalho.get(cell.getColumnIndex() + 0L).equals(MES_ANO)){
					try {
						mesAno = (Date) this.readCellValue(cell);
						if (mesAno == null){
							isFileOk = false;
							listaErros.add(new ErroImportacao(row.getRowNum() + 1, MES_ANO, null, TipoErro.NULO));
						} else {
							mesAnoCalendar.setTime(mesAno);
							
							if (comunicacaoAIE.getDataComunicacao() == null){
								comunicacaoAIE.setDataComunicacao(mesAnoCalendar);
							}else if (!(comunicacaoAIE.getDataComunicacao().get(Calendar.MONTH) == mesAnoCalendar.get(Calendar.MONTH) &&
								comunicacaoAIE.getDataComunicacao().get(Calendar.YEAR) == mesAnoCalendar.get(Calendar.YEAR))){
								isFileOk = false;
								listaErros.add(new ErroImportacao(row.getRowNum() + 1, MES_ANO, mesAno, TipoErro.MULTIPLAS_DATAS));
							}
						}
					} catch (ClassCastException e) {
						isFileOk = false;
						listaErros.add(new ErroImportacao(row.getRowNum() + 1, MES_ANO, this.readCellValue(cell), TipoErro.FORMATO_INVALIDO));
					}
				}
				
				if (cabecalho.get(cell.getColumnIndex() + 0L).equals(LABORATORIO)){
					try {
						laboratorio = ((String) this.readCellValue(cell));
						
						if (laboratorio == null){
							isFileOk = false;
							listaErros.add(new ErroImportacao(row.getRowNum() + 1, LABORATORIO, null, TipoErro.NULO));
						} else
							if (!laboratorio.equals(laboratorioUsuario.getNome())){
								isFileOk = false;
								listaErros.add(new ErroImportacao(row.getRowNum() + 1, LABORATORIO, laboratorio, TipoErro.LABORATORIO_INVALIDO));
							}
					} catch (ClassCastException e) {
						isFileOk = false;
						listaErros.add(new ErroImportacao(row.getRowNum() + 1, LABORATORIO, this.readCellValue(cell), TipoErro.FORMATO_INVALIDO));
					}
				}
				
				if (cabecalho.get(cell.getColumnIndex() + 0L).equals(CODIGO_DO_IBGE)){
					try{
						codigoIbge = (String) this.readCellValue(cell);
						if (codigoIbge == null){
							isFileOk = false;
							listaErros.add(new ErroImportacao(row.getRowNum() + 1, CODIGO_DO_IBGE, null, TipoErro.NULO));
						} else if (cell.getCachedFormulaResultType() == Cell.CELL_TYPE_ERROR){
							isFileOk = false;
							listaErros.add(new ErroImportacao(row.getRowNum() + 1, CODIGO_DO_IBGE, formatter.formatCellValue(cell, evaluator), TipoErro.MUNICIPIO_NAO_ENCONTRADO));
						} else {
							municipio = this.getMunicipio(listaUF, codigoIbge);
							
							if (municipio == null){
								isFileOk = false;
								listaErros.add(new ErroImportacao(row.getRowNum() + 1, CODIGO_DO_IBGE, null, TipoErro.MUNICIPIO_NAO_ENCONTRADO));
							} else
								detalhe.setMunicipio(municipio);
						}
					} catch (ClassCastException e) {
						isFileOk = false;
						listaErros.add(new ErroImportacao(row.getRowNum() + 1, CODIGO_DO_IBGE, this.readCellValue(cell), TipoErro.FORMATO_INVALIDO));
					}
				}
				
				if (cabecalho.get(cell.getColumnIndex() + 0L).equals(NOME_PROPRIEDADE)){
					try {
						nomePropriedade = (String) this.readCellValue(cell);
						if (nomePropriedade == null){
							isFileOk = false;
							listaErros.add(new ErroImportacao(row.getRowNum() + 1, NOME_PROPRIEDADE, null, TipoErro.NULO));
						} else
							detalhe.setNomePropriedade(nomePropriedade);
					} catch (ClassCastException e) {
						isFileOk = false;
						listaErros.add(new ErroImportacao(row.getRowNum() + 1, NOME_PROPRIEDADE, this.readCellValue(cell), TipoErro.FORMATO_INVALIDO));
					}
				}
				
				if (cabecalho.get(cell.getColumnIndex() + 0L).equals(EQUIDEOS_NA_PROPRIEDADE)){
					try {
						equideosNaPropriedade =  (Double) this.readCellValue(cell);
						if (equideosNaPropriedade == null){
							isFileOk = false;
							listaErros.add(new ErroImportacao(row.getRowNum() + 1, EQUIDEOS_NA_PROPRIEDADE, null, TipoErro.NULO));
						} else
							detalhe.setQuantidadeEquideosNaPropriedade(equideosNaPropriedade.longValue());
					} catch (ClassCastException e) {
						o = ((String)this.readCellValue(cell));
						
						if (!o.equals("N�o Informado")){
							isFileOk = false;
							listaErros.add(new ErroImportacao(row.getRowNum() + 1, EQUIDEOS_NA_PROPRIEDADE, this.readCellValue(cell), TipoErro.FORMATO_INVALIDO));
						}
					}
				}
				
				if (cabecalho.get(cell.getColumnIndex() + 0L).equals(EQUINOS_POSITIVOS)){
					try {
						equinosPositivos =  (Double) this.readCellValue(cell);
						if (equinosPositivos == null){
							isFileOk = false;
							listaErros.add(new ErroImportacao(row.getRowNum() + 1, EQUINOS_POSITIVOS, null, TipoErro.NULO));
						} else
							detalhe.setQuantidadeEquinosPositivos(equinosPositivos.longValue());
					} catch (ClassCastException e) {
						isFileOk = false;
						listaErros.add(new ErroImportacao(row.getRowNum() + 1, EQUINOS_POSITIVOS, this.readCellValue(cell), TipoErro.FORMATO_INVALIDO));
					}
				}
				
				if (cabecalho.get(cell.getColumnIndex() + 0L).equals(MUARES_POSITIVOS)){
					try {
						muaresPositivos =  (Double) this.readCellValue(cell);
						if (muaresPositivos == null){
							isFileOk = false;
							listaErros.add(new ErroImportacao(row.getRowNum() + 1, MUARES_POSITIVOS, null, TipoErro.NULO));
						} else
							detalhe.setQuantidadeMuaresPositivos(muaresPositivos.longValue());
					} catch (ClassCastException e) {
						isFileOk = false;
						listaErros.add(new ErroImportacao(row.getRowNum() + 1, MUARES_POSITIVOS, this.readCellValue(cell), TipoErro.FORMATO_INVALIDO));
					}
				}
				
				if (cabecalho.get(cell.getColumnIndex() + 0L).equals(ASININOS_POSITIVOS)){
					try {
						asininosPositivos =  (Double) this.readCellValue(cell);
						if (asininosPositivos == null){
							isFileOk = false;
							listaErros.add(new ErroImportacao(row.getRowNum() + 1, ASININOS_POSITIVOS, null, TipoErro.NULO));
						} else
							detalhe.setQuantidadeAsininosPositivos(asininosPositivos.longValue());
					} catch (ClassCastException e) {
						isFileOk = false;
						listaErros.add(new ErroImportacao(row.getRowNum() + 1, ASININOS_POSITIVOS, this.readCellValue(cell), TipoErro.FORMATO_INVALIDO));
					}
				}
				
				if (cabecalho.get(cell.getColumnIndex() + 0L).equals(EQUINOS_NEGATIVOS)){
					try {
						equinosNegativos =  (Double) this.readCellValue(cell);
						if (equinosNegativos == null){
							isFileOk = false;
							listaErros.add(new ErroImportacao(row.getRowNum() + 1, EQUINOS_NEGATIVOS, null, TipoErro.NULO));
						} else
							detalhe.setQuantidadeEquinosNegativos(equinosNegativos.longValue());
					} catch (ClassCastException e) {
						isFileOk = false;
						listaErros.add(new ErroImportacao(row.getRowNum() + 1, EQUINOS_NEGATIVOS, this.readCellValue(cell), TipoErro.FORMATO_INVALIDO));
					}
				}
				
				if (cabecalho.get(cell.getColumnIndex() + 0L).equals(MUARES_NEGATIVOS)){
					try {
						muaresNegativos =  (Double) this.readCellValue(cell);
						if (muaresNegativos == null){
							isFileOk = false;
							listaErros.add(new ErroImportacao(row.getRowNum() + 1, MUARES_NEGATIVOS, null, TipoErro.NULO));
						} else
							detalhe.setQuantidadeMuaresNegativos(muaresNegativos.longValue());
					} catch (ClassCastException e) {
						isFileOk = false;
						listaErros.add(new ErroImportacao(row.getRowNum() + 1, MUARES_NEGATIVOS, this.readCellValue(cell), TipoErro.FORMATO_INVALIDO));
					}
				}
				
				if (cabecalho.get(cell.getColumnIndex() + 0L).equals(ASININOS_NEGATIVOS)){
					try {
						asininosNegativos =  (Double) this.readCellValue(cell);
						if (asininosNegativos == null){
							isFileOk = false;
							listaErros.add(new ErroImportacao(row.getRowNum() + 1, ASININOS_NEGATIVOS, null, TipoErro.NULO));
						} else
							detalhe.setQuantidadeAsininosNegativos(asininosNegativos.longValue());
					} catch (ClassCastException e) {
						isFileOk = false;
						listaErros.add(new ErroImportacao(row.getRowNum() + 1, ASININOS_NEGATIVOS, this.readCellValue(cell), TipoErro.FORMATO_INVALIDO));
					}
				}
				
				if (cabecalho.get(cell.getColumnIndex() + 0L).equals(EQUINOS_EXAMINADOS)){
					try {
						equinosExaminados =  (Double) this.readCellValue(cell);
						if (equinosExaminados == null){
							isFileOk = false;
							listaErros.add(new ErroImportacao(row.getRowNum() + 1, EQUINOS_EXAMINADOS, null, TipoErro.NULO));
						} else
							detalhe.setQuantidadeEquinosExaminados(equinosExaminados.longValue());
					} catch (ClassCastException e) {
						isFileOk = false;
						listaErros.add(new ErroImportacao(row.getRowNum() + 1, EQUINOS_EXAMINADOS, this.readCellValue(cell), TipoErro.FORMATO_INVALIDO));
					}
				}
				
				if (cabecalho.get(cell.getColumnIndex() + 0L).equals(MUARES_EXAMINADOS)){
					try {
						muaresExaminados =  (Double) this.readCellValue(cell);
						if (muaresExaminados == null){
							isFileOk = false;
							listaErros.add(new ErroImportacao(row.getRowNum() + 1, MUARES_EXAMINADOS, null, TipoErro.NULO));
						} else
							detalhe.setQuantidadeMuaresExaminados(muaresExaminados.longValue());
					} catch (ClassCastException e) {
						isFileOk = false;
						listaErros.add(new ErroImportacao(row.getRowNum() + 1, MUARES_EXAMINADOS, this.readCellValue(cell), TipoErro.FORMATO_INVALIDO));
					}
				}
				
				if (cabecalho.get(cell.getColumnIndex() + 0L).equals(ASININOS_EXAMINADOS)){
					try {
						asininosExaminados =  (Double) this.readCellValue(cell);
						if (asininosExaminados == null){
							isFileOk = false;
							listaErros.add(new ErroImportacao(row.getRowNum() + 1, ASININOS_EXAMINADOS, null, TipoErro.NULO));
						} else
							detalhe.setQuantidadeAsininosExaminados(asininosExaminados.longValue());
					} catch (ClassCastException e) {
						isFileOk = false;
						listaErros.add(new ErroImportacao(row.getRowNum() + 1, ASININOS_EXAMINADOS, this.readCellValue(cell), TipoErro.FORMATO_INVALIDO));
					}
				}
				
			}
			
			if (isFileOk)
				comunicacaoAIE.getListaDetalheComunicacaoAIE().add(detalhe);
			
		}
		
		if (!isFileOk){
			comunicacaoAIE = null;
			throw new XlsxFillingException("Ocorreram erros ao tentar importar o arquivo");
		}
		
		return comunicacaoAIE;
	}
	
	public Object readCellValue(Cell cell) {
		switch (cell.getCellType()) {
		case Cell.CELL_TYPE_BLANK:
			return null;
		case Cell.CELL_TYPE_BOOLEAN:
			return cell.getBooleanCellValue();
		case Cell.CELL_TYPE_ERROR:
			return String.valueOf(cell.getErrorCellValue());
		case Cell.CELL_TYPE_FORMULA:
			return readFormattedCellValue(cell);
		case Cell.CELL_TYPE_NUMERIC:
			if (DateUtil.isCellDateFormatted(cell))
				return cell.getDateCellValue();
			else
				return cell.getNumericCellValue();
		case Cell.CELL_TYPE_STRING:
			return cell.getStringCellValue();
		default:
			return "Unknown type!";
		}
	}
	
	public String readFormattedCellValue(Cell cell) {
		try {
			return formatter.formatCellValue(cell, evaluator);
		} catch (RuntimeException e) {
			return e.getMessage(); 
		}
	}
	
	private Municipio getMunicipio(List<UF> listaUF, String codigoIbge){
		String codigoIbgeUF = codigoIbge.substring(0, 2);
		String codigoIbgeMunicipio = codigoIbge.substring(2, 7);
		
		for (UF uf : listaUF) {
			if (uf.getCodgIBGE().equals(codigoIbgeUF))
				for (Municipio municipio : uf.getMunicipios()) {
					if (municipio.getCodgIBGE().equals(codigoIbgeMunicipio))
						return municipio;
				}
		}
		
		return null;
	}

	public List<ErroImportacao> getListaErros() {
		return listaErros;
	}
	
}
