package br.gov.mt.indea.sistemaformin.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;

import org.hibernate.envers.Audited;

import br.gov.mt.indea.sistemaformin.enums.Dominio.FaixaEtariaOuEspecie;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoDestinoExploracao;

@Audited
@Entity
@DiscriminatorValue("aves")
public class InformacoesAves extends AbstractInformacoesDeAnimais implements Cloneable{

	private static final long serialVersionUID = 5555757893786215468L;

	@OneToMany(mappedBy = "informacoesAves", cascade={CascadeType.ALL}, orphanRemoval=true)
	@OrderBy("id")
	private List<DetalhesInformacoesAves> detalhes = new ArrayList<DetalhesInformacoesAves>();
	
	@ElementCollection(targetClass = FaixaEtariaOuEspecie.class)
	@JoinTable(name = "info_aves_outros_tipos_aves", joinColumns = @JoinColumn(name = "id_info_aves", nullable = false))
	@Column(name = "id_info_aves_outros_tipos_aves")
	@Enumerated(EnumType.STRING)
	private List<FaixaEtariaOuEspecie> outrosTiposDeAves = new ArrayList<FaixaEtariaOuEspecie>();

	public List<DetalhesInformacoesAves> getDetalhes() {
		return detalhes;
	}

	public void setDetalhes(List<DetalhesInformacoesAves> detalhes) {
		this.detalhes = detalhes;
	}

	public List<FaixaEtariaOuEspecie> getOutrosTiposDeAves() {
		return outrosTiposDeAves;
	}

	public void setOutrosTiposDeAves(List<FaixaEtariaOuEspecie> outrosTiposDeAves) {
		this.outrosTiposDeAves = outrosTiposDeAves;
	}

	@Override
	protected List<AbstractDetalhesInformacoesDeAnimais> getDetalhesInformacoesDeAnimais() {
		List<AbstractDetalhesInformacoesDeAnimais> lista = new ArrayList<AbstractDetalhesInformacoesDeAnimais>();
		lista.addAll(this.detalhes);
		return lista;
	}
	
	protected Object clone(FormIN formIN) throws CloneNotSupportedException{
		InformacoesAves cloned = (InformacoesAves) super.clone();
		
		cloned.setId(null);
		
		if (this.getDestinos() != null){
			cloned.setDestinos(new ArrayList<TipoDestinoExploracao>());
			for (TipoDestinoExploracao destino : this.getDestinos()) {
				cloned.getDestinos().add(destino);
			}
		}
		
		if (this.detalhes != null){
			cloned.setDetalhes(new ArrayList<DetalhesInformacoesAves>());
			for (DetalhesInformacoesAves detalhe : this.detalhes) {
				cloned.getDetalhes().add((DetalhesInformacoesAves) detalhe.clone(cloned));
			}
		}
		
		if (this.outrosTiposDeAves != null){
			cloned.setOutrosTiposDeAves(new ArrayList<>());
			cloned.getOutrosTiposDeAves().addAll(this.getOutrosTiposDeAves());
		}
		
		cloned.setFormIN(formIN);
		
		return cloned;
	}

}
