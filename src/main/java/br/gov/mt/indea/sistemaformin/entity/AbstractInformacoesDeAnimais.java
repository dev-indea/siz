package br.gov.mt.indea.sistemaformin.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import org.hibernate.envers.Audited;

import br.gov.mt.indea.sistemaformin.enums.Dominio.SimNao;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoDestinoExploracao;

@Audited
@Entity(name="informacoes_de_animais")
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="tipo", discriminatorType=DiscriminatorType.STRING)
public abstract class AbstractInformacoesDeAnimais extends BaseEntity<Long> implements Cloneable {
	
	protected abstract List<AbstractDetalhesInformacoesDeAnimais> getDetalhesInformacoesDeAnimais();
	
	private static final long serialVersionUID = -3979755566569039862L;

	@Id
	@SequenceGenerator(name = "informacoes_de_animais_seq", sequenceName="informacoes_de_animais_seq", allocationSize=1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "informacoes_de_animais_seq")
	private Long id;

	@Column(insertable=false, updatable=false)
    private String tipo;
	
	@OneToOne
	@JoinColumn(name = "id_formin")
	private FormIN formIN;

	@Enumerated(EnumType.STRING)
	@Column(name = "especie_principal")
	private SimNao especiePrincipal = SimNao.NAO;
	
	@ElementCollection(targetClass = TipoDestinoExploracao.class)
	@JoinTable(name = "info_animais_destinos", joinColumns = @JoinColumn(name = "id_form_in", nullable = false))
	@Column(name = "id_info_animais_destinos")
	@Enumerated(EnumType.STRING)
	private List<TipoDestinoExploracao> destinos = new ArrayList<TipoDestinoExploracao>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public FormIN getFormIN() {
		return formIN;
	}

	public void setFormIN(FormIN formIN) {
		this.formIN = formIN;
	}

	public SimNao getEspeciePrincipal() {
		return especiePrincipal;
	}

	public void setEspeciePrincipal(SimNao especiePrincipal) {
		this.especiePrincipal = especiePrincipal;
	}

	public List<TipoDestinoExploracao> getDestinos() {
		return destinos;
	}

	public void setDestinos(List<TipoDestinoExploracao> destinos) {
		this.destinos = destinos;
	}

	/****************/
	
	public Long getTotalQuantidadeMachosNoDiaDaInspecao() {
		Long total = null;
		if (this.getDetalhesInformacoesDeAnimais() != null)
			for (AbstractDetalhesInformacoesDeAnimais detalhes : this.getDetalhesInformacoesDeAnimais()) {
				if (detalhes.getQuantidadeMachosNoDiaDaInspecao() != null){
					if (total == null)
						total = new Long(0);
					total += detalhes.getQuantidadeMachosNoDiaDaInspecao();
				}
			}

		return total;
	}
	
	public Long getTotalQuantidadeFemeasNoDiaDaInspecao() {
		Long total = null;
		if (this.getDetalhesInformacoesDeAnimais() != null)
			for (AbstractDetalhesInformacoesDeAnimais detalhes : this.getDetalhesInformacoesDeAnimais()) {
				if (detalhes.getQuantidadeFemeasNoDiaDaInspecao() != null){
					if (total == null)
						total = new Long(0);
					total += detalhes.getQuantidadeFemeasNoDiaDaInspecao();
				}
			}

		return total;
	}
	
	public Long getTotalQuantidadeDesdeOInicioDaOcorrencia() {
		Long total = null;
		if (this.getDetalhesInformacoesDeAnimais() != null)
			for (AbstractDetalhesInformacoesDeAnimais detalhes : this.getDetalhesInformacoesDeAnimais()) {
				if (detalhes.getQuantidadeDesdeOInicioDaOcorrencia() != null){
					if (total == null)
						total = new Long(0);
					total += detalhes.getQuantidadeDesdeOInicioDaOcorrencia();
				}
			}

		return total;
	}

	public Long getTotalQuantidadeDoentesConfirmados() {
		Long total = null;
		if (this.getDetalhesInformacoesDeAnimais() != null)
			for (AbstractDetalhesInformacoesDeAnimais detalhes : this.getDetalhesInformacoesDeAnimais()) {
				if (detalhes.getQuantidadeDoentesConfirmados() != null){
					if (total == null)
						total = new Long(0);
					total += detalhes.getQuantidadeDoentesConfirmados();
				}
			}

		return total;
	}
	
	public Long getTotalQuantidadeDoentesProvaveis() {
		Long total = null;
		if (this.getDetalhesInformacoesDeAnimais() != null)
			for (AbstractDetalhesInformacoesDeAnimais detalhes : this.getDetalhesInformacoesDeAnimais()) {
				if (detalhes.getQuantidadeDoentesProvaveis() != null){
					if (total == null)
						total = new Long(0);
					total += detalhes.getQuantidadeDoentesProvaveis();
				}
			}

		return total;
	}

	public Long getTotalQuantidadeMortos() {
		Long total = null;
		if (this.getDetalhesInformacoesDeAnimais() != null)
			for (AbstractDetalhesInformacoesDeAnimais detalhes : this.getDetalhesInformacoesDeAnimais()) {
				if (detalhes.getQuantidadeMortos() != null){
					if (total == null)
						total = new Long(0);
					total += detalhes.getQuantidadeMortos();
				}
			}

		return total;
	}

	public Long getTotalQuantidadeAbatidos() {
		Long total = null;
		if (this.getDetalhesInformacoesDeAnimais() != null)
			for (AbstractDetalhesInformacoesDeAnimais detalhes : this.getDetalhesInformacoesDeAnimais()) {
				if (detalhes.getQuantidadeAbatidos() != null){
					if (total == null)
						total = new Long(0);
					total += detalhes.getQuantidadeAbatidos();
				}
			}

		return total;
	}

	public Long getTotalQuantidadeDestruidos() {
		Long total = null;
		if (this.getDetalhesInformacoesDeAnimais() != null)
			for (AbstractDetalhesInformacoesDeAnimais detalhes : this.getDetalhesInformacoesDeAnimais()) {
				if (detalhes.getQuantidadeDestruidos() != null){
					if (total == null)
						total = new Long(0);
					total += detalhes.getQuantidadeDestruidos();
				}
			}

		return total;
	}

	public Long getTotalQuantidadeExaminados() {
		Long total = null;
		if (this.getDetalhesInformacoesDeAnimais() != null)
			for (AbstractDetalhesInformacoesDeAnimais detalhes : this.getDetalhesInformacoesDeAnimais()) {
				if (detalhes.getQuantidadeExaminados()!= null){
					if (total == null)
						total = new Long(0);
					total += detalhes.getQuantidadeExaminados();
				}
			}

		return total;
	}
	
	public String getAbreviaturasDestinos(){
		StringBuilder sb = new StringBuilder();
		
		for (TipoDestinoExploracao destino : this.destinos) {
			sb.append(destino.getAbreviatura()).append(", ");
		}
		
		if (sb.length() > 0)
			return sb.substring(0, sb.length() - 2);
		
		return sb.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbstractInformacoesDeAnimais other = (AbstractInformacoesDeAnimais) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
