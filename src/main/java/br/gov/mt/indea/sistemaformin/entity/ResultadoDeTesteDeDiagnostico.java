package br.gov.mt.indea.sistemaformin.entity;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

@Audited
@Entity(name="resultado_de_teste")
public class ResultadoDeTesteDeDiagnostico extends BaseEntity<Long>{
	
	private static final long serialVersionUID = -8335399002763290214L;
	
	@Id
	@SequenceGenerator(name="resultado_de_teste_seq", sequenceName="resultado_de_teste_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="resultado_de_teste_seq")
	private Long id;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_recebimento")
	private Calendar dataRecebimento;
	
	@Column(name="identificacao_laudo")
	private String identificacaoDoLaudo;
	
	private String laboratorio;
	
	@Column(name="teste_realizado")
	private String testeRealizado;
	
	private String doenca;
	
	@Column(name="quantidade_positivas")
	private Long quantidadePositivas;
	
	@Column(name="quantidade_negativas")
	private Long quantidadeNegativas;
	
	@Column(name="quantidade_inconclusivas")
	private Long quantidadeInconclusivas;
	
	@Column(name="quantidade_inadequadas")
	private Long quantidadeInadequadas;
	
	@ManyToOne
	@JoinColumn(name="id_formCOM")
	private FormCOM formCOM;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Calendar getDataRecebimento() {
		return dataRecebimento;
	}

	public void setDataRecebimento(Calendar dataRecebimento) {
		this.dataRecebimento = dataRecebimento;
	}

	public String getIdentificacaoDoLaudo() {
		return identificacaoDoLaudo;
	}

	public void setIdentificacaoDoLaudo(String identificacaoDoLaudo) {
		this.identificacaoDoLaudo = identificacaoDoLaudo;
	}

	public String getLaboratorio() {
		return laboratorio;
	}

	public void setLaboratorio(String laboratorio) {
		this.laboratorio = laboratorio;
	}

	public String getTesteRealizado() {
		return testeRealizado;
	}

	public void setTesteRealizado(String testeRealizado) {
		this.testeRealizado = testeRealizado;
	}

	public String getDoenca() {
		return doenca;
	}

	public void setDoenca(String doenca) {
		this.doenca = doenca;
	}

	public Long getQuantidadePositivas() {
		return quantidadePositivas;
	}

	public void setQuantidadePositivas(Long quantidadePositivas) {
		this.quantidadePositivas = quantidadePositivas;
	}

	public Long getQuantidadeNegativas() {
		return quantidadeNegativas;
	}

	public void setQuantidadeNegativas(Long quantidadeNegativas) {
		this.quantidadeNegativas = quantidadeNegativas;
	}

	public Long getQuantidadeInconclusivas() {
		return quantidadeInconclusivas;
	}

	public void setQuantidadeInconclusivas(Long quantidadeInconclusivas) {
		this.quantidadeInconclusivas = quantidadeInconclusivas;
	}

	public Long getQuantidadeInadequadas() {
		return quantidadeInadequadas;
	}

	public void setQuantidadeInadequadas(Long quantidadeInadequadas) {
		this.quantidadeInadequadas = quantidadeInadequadas;
	}

	public FormCOM getFormCOM() {
		return formCOM;
	}

	public void setFormCOM(FormCOM formCOM) {
		this.formCOM = formCOM;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResultadoDeTesteDeDiagnostico other = (ResultadoDeTesteDeDiagnostico) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}