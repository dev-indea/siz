package br.gov.mt.indea.sistemaformin.webservice.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ExploracaoAves implements Serializable {

    private static final long serialVersionUID = -2364455904793028382L;

	private ExploracaoAvesPK exploracaoAvesPK;
   
    private FinalidadeExploracao finalidadeExploracao;

    public ExploracaoAves() {
    }

    public FinalidadeExploracao getFinalidadeExploracao() {
        return finalidadeExploracao;
    }

    public void setFinalidadeExploracao(FinalidadeExploracao finalidadeExploracao) {
        this.finalidadeExploracao = finalidadeExploracao;
    }

    public ExploracaoAvesPK getExploracaoAvesPK() {
        return exploracaoAvesPK;
    }

    public void setExploracaoAvesPK(ExploracaoAvesPK exploracaoAvesPK) {
        this.exploracaoAvesPK = exploracaoAvesPK;
    }
}
