package br.gov.mt.indea.sistemaformin.entity.view;

import java.io.Serializable;
import java.util.Date;

public class RelatorioVisitaPropriedadeRuralGeral implements Serializable {

	private static final long serialVersionUID = 1667324840277718104L;

	private String urs;

	private String municipio;

	private String codg_ibge;

	private String numero;

	private Date data_da_visita;

	private Date data_cadastro;

	private String tipo_estabelecimento;

	private String nome;

	private String codigo_estabelecimento;

	private Integer latitude_grau;

	private Integer latitude_minuto;

	private Float latitude_segundo;

	private Integer longitude_grau;

	private Integer longitude_minuto;

	private Float longitude_segundo;

	private String chave_principal;

	private Integer abert_de_cadastro_pec_novo;

	private Integer aplicacao_de_vacina;

	private Integer apreensao;

	private Integer atual_cadastro_pec;

	private Integer autuacao;

	private Integer captura;

	private Integer coleta_de_amostra;

	private Integer controle_mov_subprod;

	private Integer desinfeccao;

	private Integer desinterdicao;

	private Integer destruicao;

	private Integer educacao_sanitaria;

	private Integer fiscalizacao_vac;

	private Integer fiscalizacao_uso_farmo;

	private Integer ingresso_e_transito_animal;

	private Integer inspecao_clinica;

	private Integer inspecao_fisica_e_sanit;

	private Integer interdicao;

	private Integer isolamento;

	private Integer laudo_de_inspecao_fisica_e_sanitaria_a_estabelevimento_avicola;

	private Integer marcacao;

	private Integer notificacao;

	private Integer sacrificio;

	private Integer saneamento;

	private Integer vistoria_de_rebanho;

	private String servidores_que_realizaram_visita;

	public String getUrs() {
		return urs;
	}

	public void setUrs(String urs) {
		this.urs = urs;
	}

	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public Date getData_da_visita() {
		return data_da_visita;
	}

	public void setData_da_visita(Date data_da_visita) {
		this.data_da_visita = data_da_visita;
	}

	public Date getData_cadastro() {
		return data_cadastro;
	}

	public void setData_cadastro(Date data_cadastro) {
		this.data_cadastro = data_cadastro;
	}

	public String getTipo_estabelecimento() {
		return tipo_estabelecimento;
	}

	public void setTipo_estabelecimento(String tipo_estabelecimento) {
		this.tipo_estabelecimento = tipo_estabelecimento;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCodigo_estabelecimento() {
		return codigo_estabelecimento;
	}

	public void setCodigo_estabelecimento(String codigo_estabelecimento) {
		this.codigo_estabelecimento = codigo_estabelecimento;
	}

	public String getChave_principal() {
		return chave_principal;
	}

	public void setChave_principal(String chave_principal) {
		this.chave_principal = chave_principal;
	}

	public Integer getEducacao_sanitaria() {
		return educacao_sanitaria;
	}

	public void setEducacao_sanitaria(Integer educacao_sanitaria) {
		this.educacao_sanitaria = educacao_sanitaria;
	}

	public Integer getApreensao() {
		return apreensao;
	}

	public void setApreensao(Integer apreensao) {
		this.apreensao = apreensao;
	}

	public Integer getSacrificio() {
		return sacrificio;
	}

	public void setSacrificio(Integer sacrificio) {
		this.sacrificio = sacrificio;
	}

	public Integer getDestruicao() {
		return destruicao;
	}

	public void setDestruicao(Integer destruicao) {
		this.destruicao = destruicao;
	}

	public Integer getCaptura() {
		return captura;
	}

	public void setCaptura(Integer captura) {
		this.captura = captura;
	}

	public Integer getDesinfeccao() {
		return desinfeccao;
	}

	public void setDesinfeccao(Integer desinfeccao) {
		this.desinfeccao = desinfeccao;
	}

	public Integer getSaneamento() {
		return saneamento;
	}

	public void setSaneamento(Integer saneamento) {
		this.saneamento = saneamento;
	}

	public Integer getMarcacao() {
		return marcacao;
	}

	public void setMarcacao(Integer marcacao) {
		this.marcacao = marcacao;
	}

	public Integer getIsolamento() {
		return isolamento;
	}

	public void setIsolamento(Integer isolamento) {
		this.isolamento = isolamento;
	}

	public Integer getInterdicao() {
		return interdicao;
	}

	public void setInterdicao(Integer interdicao) {
		this.interdicao = interdicao;
	}

	public Integer getDesinterdicao() {
		return desinterdicao;
	}

	public void setDesinterdicao(Integer desinterdicao) {
		this.desinterdicao = desinterdicao;
	}

	public Integer getNotificacao() {
		return notificacao;
	}

	public void setNotificacao(Integer notificacao) {
		this.notificacao = notificacao;
	}

	public Integer getAutuacao() {
		return autuacao;
	}

	public void setAutuacao(Integer autuacao) {
		this.autuacao = autuacao;
	}

	public Integer getLaudo_de_inspecao_fisica_e_sanitaria_a_estabelevimento_avicola() {
		return laudo_de_inspecao_fisica_e_sanitaria_a_estabelevimento_avicola;
	}

	public void setLaudo_de_inspecao_fisica_e_sanitaria_a_estabelevimento_avicola(
			Integer laudo_de_inspecao_fisica_e_sanitaria_a_estabelevimento_avicola) {
		this.laudo_de_inspecao_fisica_e_sanitaria_a_estabelevimento_avicola = laudo_de_inspecao_fisica_e_sanitaria_a_estabelevimento_avicola;
	}

	public Integer getVistoria_de_rebanho() {
		return vistoria_de_rebanho;
	}

	public void setVistoria_de_rebanho(Integer vistoria_de_rebanho) {
		this.vistoria_de_rebanho = vistoria_de_rebanho;
	}

	public Integer getAbert_de_cadastro_pec_novo() {
		return abert_de_cadastro_pec_novo;
	}

	public void setAbert_de_cadastro_pec_novo(Integer abert_de_cadastro_pec_novo) {
		this.abert_de_cadastro_pec_novo = abert_de_cadastro_pec_novo;
	}

	public Integer getAplicacao_de_vacina() {
		return aplicacao_de_vacina;
	}

	public void setAplicacao_de_vacina(Integer aplicacao_de_vacina) {
		this.aplicacao_de_vacina = aplicacao_de_vacina;
	}

	public Integer getAtual_cadastro_pec() {
		return atual_cadastro_pec;
	}

	public void setAtual_cadastro_pec(Integer atual_cadastro_pec) {
		this.atual_cadastro_pec = atual_cadastro_pec;
	}

	public Integer getColeta_de_amostra() {
		return coleta_de_amostra;
	}

	public void setColeta_de_amostra(Integer coleta_de_amostra) {
		this.coleta_de_amostra = coleta_de_amostra;
	}

	public Integer getControle_mov_subprod() {
		return controle_mov_subprod;
	}

	public void setControle_mov_subprod(Integer controle_mov_subprod) {
		this.controle_mov_subprod = controle_mov_subprod;
	}

	public Integer getFiscalizacao_vac() {
		return fiscalizacao_vac;
	}

	public void setFiscalizacao_vac(Integer fiscalizacao_vac) {
		this.fiscalizacao_vac = fiscalizacao_vac;
	}

	public Integer getFiscalizacao_uso_farmo() {
		return fiscalizacao_uso_farmo;
	}

	public void setFiscalizacao_uso_farmo(Integer fiscalizacao_uso_farmo) {
		this.fiscalizacao_uso_farmo = fiscalizacao_uso_farmo;
	}

	public Integer getIngresso_e_transito_animal() {
		return ingresso_e_transito_animal;
	}

	public void setIngresso_e_transito_animal(Integer ingresso_e_transito_animal) {
		this.ingresso_e_transito_animal = ingresso_e_transito_animal;
	}

	public Integer getInspecao_clinica() {
		return inspecao_clinica;
	}

	public void setInspecao_clinica(Integer inspecao_clinica) {
		this.inspecao_clinica = inspecao_clinica;
	}

	public Integer getInspecao_fisica_e_sanit() {
		return inspecao_fisica_e_sanit;
	}

	public void setInspecao_fisica_e_sanit(Integer inspecao_fisica_e_sanit) {
		this.inspecao_fisica_e_sanit = inspecao_fisica_e_sanit;
	}

	public String getServidores_que_realizaram_visita() {
		return servidores_que_realizaram_visita;
	}

	public void setServidores_que_realizaram_visita(String servidores_que_realizaram_visita) {
		this.servidores_que_realizaram_visita = servidores_que_realizaram_visita;
	}

	public Integer getLatitude_grau() {
		return latitude_grau;
	}

	public void setLatitude_grau(Integer latitude_grau) {
		this.latitude_grau = latitude_grau;
	}

	public Integer getLatitude_minuto() {
		return latitude_minuto;
	}

	public void setLatitude_minuto(Integer latitude_minuto) {
		this.latitude_minuto = latitude_minuto;
	}

	public Float getLatitude_segundo() {
		return latitude_segundo;
	}

	public void setLatitude_segundo(Float latitude_segundo) {
		this.latitude_segundo = latitude_segundo;
	}

	public Integer getLongitude_grau() {
		return longitude_grau;
	}

	public String getCodg_ibge() {
		return codg_ibge;
	}

	public void setCodg_ibge(String codg_ibge) {
		this.codg_ibge = codg_ibge;
	}

	public void setLongitude_grau(Integer longitude_grau) {
		this.longitude_grau = longitude_grau;
	}

	public Integer getLongitude_minuto() {
		return longitude_minuto;
	}

	public void setLongitude_minuto(Integer longitude_minuto) {
		this.longitude_minuto = longitude_minuto;
	}

	public Float getLongitude_segundo() {
		return longitude_segundo;
	}

	public void setLongitude_segundo(Float longitude_segundo) {
		this.longitude_segundo = longitude_segundo;
	}

}
