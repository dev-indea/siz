package br.gov.mt.indea.sistemaformin.webservice.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ExploracaoSuideo implements Serializable {

	private static final long serialVersionUID = -7497867622262989703L;

	private ExploracaoSuideoPK exploracaoSuideoPK;
    
    private FinalidadeExploracao finalidadeExploracao;

    public ExploracaoSuideo() {
    }

    public ExploracaoSuideoPK getExploracaoSuideoPK() {
		return exploracaoSuideoPK;
	}
	public void setExploracaoSuideoPK(ExploracaoSuideoPK exploracaoSuideoPK) {
		this.exploracaoSuideoPK = exploracaoSuideoPK;
	}

	public FinalidadeExploracao getFinalidadeExploracao() {
        return finalidadeExploracao;
    }

    public void setFinalidadeExploracao(FinalidadeExploracao finalidadeExploracao) {
        this.finalidadeExploracao = finalidadeExploracao;
    }
    
}