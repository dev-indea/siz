package br.gov.mt.indea.sistemaformin.webservice.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class EtapaEspecie implements Serializable {

	private static final long serialVersionUID = -6938996322211598022L;

	private Long id;
    
    @XmlElement(name = "especie")
	private Especie especie;
        
    @XmlElement(name = "estratificacao")
	private Estratificacao estratificacao;

    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Especie getEspecie() {
		return especie;
	}

	public void setEspecie(Especie especie) {
		this.especie = especie;
	}

	public Estratificacao getEstratificacao() {
		return estratificacao;
	}

	public void setEstratificacao(Estratificacao estratificacao) {
		this.estratificacao = estratificacao;
	}

}