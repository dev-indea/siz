package br.gov.mt.indea.sistemaformin.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import org.hibernate.envers.Audited;

import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoAlteracaoSindromeNeurologica;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoDisturbioSindromeNeurologica;


@Audited
@Entity(name="alteracao_sindrome_neur")
public class AlteracaoSindromeNeurologica extends BaseEntity<Long>{

	private static final long serialVersionUID = -8525959035675522565L;

	@Id
	@SequenceGenerator(name="alteracao_sindrome_neur_seq", sequenceName="alteracao_sindrome_neur_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="alteracao_sindrome_neur_seq")
	private Long id;
	
	@ManyToOne
	@JoinColumn(name="id_formSN")
	private FormSN formSN;
	
	@Enumerated(EnumType.STRING)
	private TipoAlteracaoSindromeNeurologica tipoAlteracaoSindromeNeurologica;
	
	@ElementCollection(targetClass = TipoDisturbioSindromeNeurologica.class)
	@JoinTable(name = "alteracao_disturbio", joinColumns = @JoinColumn(name = "id_alteracao", nullable = false))
	@Column(name = "id_alteracao_disturbio")
	@Enumerated(EnumType.STRING)
	private List<TipoDisturbioSindromeNeurologica> listaTipoDisturbioSindromeNeurologica = new ArrayList<TipoDisturbioSindromeNeurologica>();
	
	public String getListaTipoDisturbioSindromeNeurologicaAsString(){
		StringBuilder sb = new StringBuilder();
		
		if (this.listaTipoDisturbioSindromeNeurologica != null && !this.listaTipoDisturbioSindromeNeurologica.isEmpty()){
			for (TipoDisturbioSindromeNeurologica tipoDisturbioSindromeNeurologica : this.listaTipoDisturbioSindromeNeurologica) {
				sb.append(tipoDisturbioSindromeNeurologica.getDescricao()).append(", ");
			}
			
			sb = new StringBuilder(sb.toString().substring(0, sb.length()-2));
		}
		
		return sb.toString();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public FormSN getFormSN() {
		return formSN;
	}

	public void setFormSN(FormSN formSN) {
		this.formSN = formSN;
	}

	public TipoAlteracaoSindromeNeurologica getTipoAlteracaoSindromeNeurologica() {
		return tipoAlteracaoSindromeNeurologica;
	}

	public void setTipoAlteracaoSindromeNeurologica(
			TipoAlteracaoSindromeNeurologica tipoAlteracaoSindromeNeurologica) {
		this.tipoAlteracaoSindromeNeurologica = tipoAlteracaoSindromeNeurologica;
	}

	public List<TipoDisturbioSindromeNeurologica> getListaTipoDisturbioSindromeNeurologica() {
		return listaTipoDisturbioSindromeNeurologica;
	}

	public void setListaTipoDisturbioSindromeNeurologica(
			List<TipoDisturbioSindromeNeurologica> listaTipoDisturbioSindromeNeurologica) {
		this.listaTipoDisturbioSindromeNeurologica = listaTipoDisturbioSindromeNeurologica;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AlteracaoSindromeNeurologica other = (AlteracaoSindromeNeurologica) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
