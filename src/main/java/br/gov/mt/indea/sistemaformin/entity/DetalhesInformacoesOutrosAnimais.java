package br.gov.mt.indea.sistemaformin.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.envers.Audited;

@Audited
@Entity
@DiscriminatorValue("outros_animais")
public class DetalhesInformacoesOutrosAnimais extends AbstractDetalhesInformacoesDeAnimais implements Cloneable{
	
	private static final long serialVersionUID = 1355044082455412390L;

	@ManyToOne
	@JoinColumn(name="id_informacoes_outrosAnimais")
	private InformacoesOutrosAnimais informacoesOutrosAnimais;

	public InformacoesOutrosAnimais getInformacoesOutrosAnimais() {
		return informacoesOutrosAnimais;
	}

	public void setInformacoesOutrosAnimais(InformacoesOutrosAnimais informacoesOutrosAnimais) {
		this.informacoesOutrosAnimais = informacoesOutrosAnimais;
	}
	
	protected Object clone(InformacoesOutrosAnimais informacoesOutrosAnimais) throws CloneNotSupportedException{
		DetalhesInformacoesOutrosAnimais cloned = (DetalhesInformacoesOutrosAnimais) super.clone();
		
		cloned.setId(null);
		cloned.setInformacoesOutrosAnimais(informacoesOutrosAnimais);
		
		return cloned;
	}

}