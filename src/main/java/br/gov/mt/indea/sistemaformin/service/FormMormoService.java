package br.gov.mt.indea.sistemaformin.service;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.mt.indea.sistemaformin.entity.FormMormo;
import br.gov.mt.indea.sistemaformin.webservice.entity.Exploracao;
import br.gov.mt.indea.sistemaformin.webservice.entity.Produtor;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class FormMormoService extends PaginableService<FormMormo, Long> {

	private static final Logger log = LoggerFactory.getLogger(FormMormoService.class);
	
	protected FormMormoService() {
		super(FormMormo.class);
	}
	
	public FormMormo findByIdFetchAll(Long id){
		FormMormo formMormo;
		
		StringBuilder sql = new StringBuilder();
		sql.append("  from FormMormo formMormo ")
		   .append("  join fetch formMormo.formIN formin ")
		   .append("  left join fetch formMormo.formCOM ")
		   .append("  join fetch formin.propriedade propriedade ")
		   .append("  left join fetch propriedade.coordenadaGeografica ")
		   .append("  left join fetch formin.proprietario proprietario")
		   .append("  left join fetch proprietario.endereco endprop")
		   .append("  left join fetch endprop.municipio munprop")
		   .append("  left join fetch munprop.uf")
		   .append("  join fetch propriedade.municipio ")
		   .append("  join fetch propriedade.ule ")
		   .append("  left join fetch propriedade.endereco ")
		   
		   .append("  join fetch formMormo.veterinario vet")
		   .append("  join fetch vet.conselho")
		   .append("  join fetch vet.tipoEmitente")
		   .append("  left join fetch vet.ule uu")
		   .append("  left join fetch uu.urs")
		   .append("  left join fetch vet.endereco")
		   .append(" where formMormo.id = :id ");
		
		Query query = getSession().createQuery(sql.toString()).setLong("id", id);
		formMormo = (FormMormo) query.uniqueResult();
		
		if (formMormo != null){
		
			Hibernate.initialize(formMormo.getFormIN().getPropriedade().getProprietarios());
			Hibernate.initialize(formMormo.getFormIN().getPropriedade().getUle());
			Hibernate.initialize(formMormo.getFormIN().getPropriedade().getExploracaos());
			
			for (Exploracao exploracao : formMormo.getFormIN().getPropriedade().getExploracaos()){
				Hibernate.initialize(exploracao.getProdutores());
				
				for (Produtor produtor : exploracao.getProdutores()) {
					Hibernate.initialize(produtor.getMunicipioNascimento());
					Hibernate.initialize(produtor.getEndereco());
					Hibernate.initialize(produtor.getEndereco().getMunicipio());
					Hibernate.initialize(produtor.getEndereco().getMunicipio().getUf());
				}
			}
			
			for (Produtor produtor : formMormo.getFormIN().getPropriedade().getProprietarios()) {
				Hibernate.initialize(produtor.getMunicipioNascimento());
				Hibernate.initialize(produtor.getEndereco());
				Hibernate.initialize(produtor.getEndereco().getMunicipio());
				Hibernate.initialize(produtor.getEndereco().getMunicipio().getUf());
			}
			
		}
		
		return formMormo;
	}
	
	@Override
	public void saveOrUpdate(FormMormo formMormo) {
		super.saveOrUpdate(formMormo);
		
		log.info("Salvando Form Mormo {}", formMormo.getId());
	}
	
	@Override
	public void delete(FormMormo formMormo) {
		super.delete(formMormo);
		
		log.info("Removendo Form Mormo {}", formMormo.getId());
	}
	
	@Override
	public void validar(FormMormo FormMormo) {

	}

	@Override
	public void validarPersist(FormMormo FormMormo) {

	}

	@Override
	public void validarMerge(FormMormo FormMormo) {

	}

	@Override
	public void validarDelete(FormMormo FormMormo) {

	}

}
