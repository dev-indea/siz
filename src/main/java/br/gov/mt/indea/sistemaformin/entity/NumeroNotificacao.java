package br.gov.mt.indea.sistemaformin.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Audited
@Entity
@Table(name="numero_notificacao")
public class NumeroNotificacao extends BaseEntity<Long>{
	
	private static final long serialVersionUID = -3513472125122553076L;

	@Id
	@SequenceGenerator(name="numero_notificacao_seq", sequenceName="numero_notificacao_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="numero_notificacao_seq")
	private Long id;
	
	@Column(name="ultimo_numero")
	private Long ultimoNumero;
	
	@Column(name="ano")
	private Long ano;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUltimoNumero() {
		return ultimoNumero;
	}

	public void setUltimoNumero(Long ultimoNumero) {
		this.ultimoNumero = ultimoNumero;
	}

	public Long getAno() {
		return ano;
	}

	public void setAno(Long ano) {
		this.ano = ano;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NumeroNotificacao other = (NumeroNotificacao) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public void incrementNumeroNotificacao() {
		if (ultimoNumero == null)
			ultimoNumero = 0L;
		this.ultimoNumero++;
	}

}