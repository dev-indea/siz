package br.gov.mt.indea.sistemaformin.dao;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.inject.Inject;


import br.gov.mt.indea.sistemaformin.entity.FormMaleina;
import br.gov.mt.indea.sistemaformin.exception.ApplicationException;

@Stateless
public class FormMaleinaDAO extends DAO<FormMaleina> {
	
	@Inject
	private EntityManager em;
	
	public FormMaleinaDAO() {
		super(FormMaleina.class);
	}

	@Override
	public EntityManager getEntityManager() {
		return this.em;
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void add(FormMaleina formMaleina) throws ApplicationException{
		formMaleina = this.getEntityManager().merge(formMaleina);
		if (formMaleina.getFormCOM() != null)
			formMaleina.setFormCOM(this.getEntityManager().merge(formMaleina.getFormCOM()));
		super.add(formMaleina);
	}

}
