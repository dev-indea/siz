package br.gov.mt.indea.sistemaformin.webservice.model;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class PessoaFisica implements Serializable {

	private static final long serialVersionUID = -2551359623523425652L;

	private Long id;
    
    private String sexo;
   
    @XmlElement
    private GrauEscolaridade grauEscolaridade;
    
    private Municipio municipioNascimento;
   
    private Date dataNascimento;
    
    private String rg;
    
    private String emissorRg;
    
    private String nomeMae;

    public PessoaFisica() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public GrauEscolaridade getGrauEscolaridade() {
        return grauEscolaridade;
    }

    public void setGrauEscolaridade(GrauEscolaridade grauEscolaridade) {
        this.grauEscolaridade = grauEscolaridade;
    }

    public Municipio getMunicipioNascimento() {
        return municipioNascimento;
    }

    public void setMunicipioNascimento(Municipio municipioNascimento) {
        this.municipioNascimento = municipioNascimento;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    public String getEmissorRg() {
        return emissorRg;
    }

    public void setEmissorRg(String emissorRg) {
        this.emissorRg = emissorRg;
    }

    public String getNomeMae() {
        return nomeMae;
    }

    public void setNomeMae(String nomeMae) {
        this.nomeMae = nomeMae;
    }

    public PessoaFisica(String sexo, Municipio municipioNascimento, Date dataNascimento, String rg, String emissorRg, String nomeMae) {
        this.sexo = sexo;
        this.municipioNascimento = municipioNascimento;
        this.dataNascimento = dataNascimento;
        this.rg = rg;
        this.emissorRg = emissorRg;
        this.nomeMae = nomeMae;
    }
}