package br.gov.mt.indea.sistemaformin.util;

import java.io.File;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.util.Date;
import java.util.Map;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.annotation.Resource;
import javax.ejb.Asynchronous;
import javax.ejb.Singleton;
import javax.enterprise.event.Observes;
import javax.enterprise.event.TransactionPhase;
import javax.faces.context.FacesContext;
import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.apache.commons.mail.SimpleEmail;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;

import br.gov.mt.indea.sistemaformin.entity.AchadosLoteEnfermidadeAbatedouroFrigorifico;
import br.gov.mt.indea.sistemaformin.entity.LoteEnfermidadeAbatedouroFrigorifico;
import br.gov.mt.indea.sistemaformin.event.MailEvent;

@Singleton
public class Email {
	
	private static final String EMAIL_EMITENTE = "vigilanciazoossanitariamt@indea.mt.gov.br";
	
	private static final String NOME_EMITENTE = "SIZ MT";
	
	private static final String USERNAME = "vigilanciazoossanitariamt@indea.mt.gov.br";
	
	private static final String PASSWORD = "dghsd65865dsa5f76dsf5s6fd5f4s56f4ds5f4sd";
	
	@Resource(mappedName = "java:jboss/mail/Gmail")
	private Session mailSession;
	
	@Asynchronous
	public void sendAsynchronousTestMail(@Observes(during = TransactionPhase.AFTER_SUCCESS) MailEvent event) {
		try {
			MimeMessage message = new MimeMessage(mailSession);
			Address[] to = new InternetAddress[] {new InternetAddress(event.getDestinatario())};

			message.setRecipients(Message.RecipientType.TO, to);
			message.setSubject(event.getAssunto());
			message.setSentDate(new Date());
			
			BodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setContent(event.getMensagem(), "text/html; charset=utf-8");
			
			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart);
			
			messageBodyPart = new MimeBodyPart();
			
			if (event.getListaAnexo() != null && !event.getListaAnexo().isEmpty()) {
				for (Map.Entry<String, FileDataSource> entry : event.getListaAnexo().entrySet()) {
					messageBodyPart.setDataHandler(new DataHandler(entry.getValue()));
					messageBodyPart.setFileName(entry.getKey() + ".pdf");
				}
				multipart.addBodyPart(messageBodyPart);
			}
			
			message.setContent(multipart);
			Transport.send(message);
		} catch (MessagingException e) {
            throw new RuntimeException(e);
        }
	}

	@SuppressWarnings("deprecation")
	public static void send(String emailDestinatario, String nomeDestinatario, String assunto, String mensagem) throws EmailException {

		SimpleEmail email = new SimpleEmail();
		// Utilize o hostname do seu provedor de email
		email.setHostName("smtp.gmail.com");
		// Quando a porta utilizada n�o � a padr�o (gmail = 465)
		email.setSmtpPort(465);
		// Adicione os destinat�rios
		email.addTo(emailDestinatario, nomeDestinatario);
		// Configure o seu email do qual enviar�
		email.setFrom(EMAIL_EMITENTE, NOME_EMITENTE);
		// Adicione um assunto
		email.setSubject(assunto);
		// Adicione a mensagem do email
		email.setMsg(mensagem);
		email.setSSL(true);
		// Para autenticar no servidor � necess�rio chamar os dois m�todos abaixo
		email.setAuthentication(USERNAME, PASSWORD);
		email.send();
		
	}
	
	@SuppressWarnings("deprecation")
	public static void sendEmailForgotPassword(String emailDestinatario, String username, String assunto, String link) throws MalformedURLException, EmailException{
		VelocityEngine ve = new VelocityEngine();
		ve.setProperty("resource.loader", "file");
		ve.setProperty("runtime.log", FacesContext.getCurrentInstance().getExternalContext().getRealPath(File.separator) + File.separator + "logs" + File.separator + "velocity.log");
		ve.setProperty("file.resource.loader.path", FacesContext.getCurrentInstance().getExternalContext().getRealPath(File.separator) + File.separator + "resources" + File.separator + "templates" + File.separator + "email");
		ve.init();
		
		Template template = ve.getTemplate("emailRedefinirPassword.vm");
		
		HtmlEmail email = new HtmlEmail();
		
		String cidImage1 = email.embed(new File(FacesContext.getCurrentInstance().getExternalContext().getRealPath(File.separator) + File.separator + "resources" + File.separator + "img" + File.separator + "logos" + File.separator + "logo_mt.png").toURI().toURL(), "MT");
		String cidImage2 = email.embed(new File(FacesContext.getCurrentInstance().getExternalContext().getRealPath(File.separator) + File.separator + "resources" + File.separator + "img" + File.separator + "logos" + File.separator + "logo_indea.png").toURI().toURL(), "Indea");
		
		VelocityContext context = new VelocityContext();
		context.put("image1", "cid:" + cidImage1);
		context.put("image2", "cid:" + cidImage2);
		context.put("username", username);
		context.put("link", link);

		StringWriter writer = new StringWriter();
		template.merge(context, writer);
		
		email.setHostName("smtp.gmail.com");
		email.setAuthentication(USERNAME, PASSWORD);
//		email.setStartTLSEnabled(true);
		email.setFrom(EMAIL_EMITENTE, NOME_EMITENTE);
//		email.getMailSession().getProperties().put("mail.smtp.ssl.trust", "cuco.mt.gov.br");
		email.setSmtpPort(465);
		email.addTo(emailDestinatario, "");
		email.setSubject(assunto);
		email.setHtmlMsg(writer.toString());
		email.setSSL(true);
		try{
        email.send();
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	@SuppressWarnings("deprecation")
	public static void sendEmailUsuarioInativo(String emailDestinatario, String username, String assunto) throws MalformedURLException, EmailException{
		VelocityEngine ve = new VelocityEngine();
		ve.setProperty("resource.loader", "file");
		ve.setProperty("runtime.log", FacesContext.getCurrentInstance().getExternalContext().getRealPath(File.separator) + File.separator + "logs" + File.separator + "velocity.log");
		ve.setProperty("file.resource.loader.path", FacesContext.getCurrentInstance().getExternalContext().getRealPath(File.separator) + File.separator + "resources" + File.separator + "templates" + File.separator + "email");
		ve.init();
		
		Template template = ve.getTemplate("emailUsuarioInativo.vm");
		
		HtmlEmail email = new HtmlEmail();
		
		String cidImage1 = email.embed(new File(FacesContext.getCurrentInstance().getExternalContext().getRealPath(File.separator) + File.separator + "resources" + File.separator + "img" + File.separator + "logos" + File.separator + "logo_mt.png").toURI().toURL(), "MT");
		String cidImage2 = email.embed(new File(FacesContext.getCurrentInstance().getExternalContext().getRealPath(File.separator) + File.separator + "resources" + File.separator + "img" + File.separator + "logos" + File.separator + "logo_indea.png").toURI().toURL(), "Indea");
		
		VelocityContext context = new VelocityContext();
		context.put("image1", "cid:" + cidImage1);
		context.put("image2", "cid:" + cidImage2);
		context.put("username", username);

		StringWriter writer = new StringWriter();
		template.merge(context, writer);
		
		email.setHostName("smtp.gmail.com");
		email.setAuthentication(USERNAME, PASSWORD);
//		email.setStartTLSEnabled(true);
		email.setFrom(EMAIL_EMITENTE, NOME_EMITENTE);
//		email.getMailSession().getProperties().put("mail.smtp.ssl.trust", "cuco.mt.gov.br");
		email.setSmtpPort(465);
		email.addTo(emailDestinatario, "");
		email.setSubject(assunto);
		email.setHtmlMsg(writer.toString());
		email.setSSL(true);
        email.send();
	}
	
	@SuppressWarnings("deprecation")
	public static void sendEmailNotificacaoAchadosEnfermidadeAbatedouroFrigorifico(String emailDestinatario, LoteEnfermidadeAbatedouroFrigorifico loteEnfermidadeAbatedouroFrigorifico) throws MalformedURLException, EmailException {
		try {
			Thread.sleep(100000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		VelocityEngine ve = new VelocityEngine();
		ve.setProperty("resource.loader", "file");
		ve.setProperty("file.resource.loader.path", FacesContext.getCurrentInstance().getExternalContext().getRealPath(File.separator) + File.separator + "resources" + File.separator + "templates" + File.separator + "email");
		ve.init();
		
		Template template = ve.getTemplate("emailNotificacaoAchadoAbatedouroFrigorifico.vm");
		HtmlEmail email = new HtmlEmail();
		
		String cidImage1 = email.embed(new File(FacesContext.getCurrentInstance().getExternalContext().getRealPath(File.separator) + File.separator + "resources" + File.separator + "img" + File.separator + "logos" + File.separator + "logo_mt.png").toURI().toURL(), "MT");
		String cidImage2 = email.embed(new File(FacesContext.getCurrentInstance().getExternalContext().getRealPath(File.separator) + File.separator + "resources" + File.separator + "img" + File.separator + "logos" + File.separator + "logo_indea.png").toURI().toURL(), "Indea");
		
		VelocityContext context = new VelocityContext();
		context.put("image1", "cid:" + cidImage1);
		context.put("image2", "cid:" + cidImage2);
		context.put("estadoOrigem", loteEnfermidadeAbatedouroFrigorifico.getProprietario().getPropriedade().getMunicipio().getUf().getNome());

		StringWriter writer = new StringWriter();
		template.merge(context, writer);
		
		StringBuilder assunto = new StringBuilder();
		assunto.append("SIZ-Achados: ");
		
		for (AchadosLoteEnfermidadeAbatedouroFrigorifico achado	: loteEnfermidadeAbatedouroFrigorifico.getListaAchadosLoteEnfermidadeAbatedouroFrigorifico()) {
			assunto.append(achado.getDoencaEnfermidadeAbatedouroFrigorifico().getNome());
		}
		
		if (loteEnfermidadeAbatedouroFrigorifico.houveColheita())
			assunto.append(", Com colheita");
		
		assunto.append(", de ")
			   .append(loteEnfermidadeAbatedouroFrigorifico.getProprietario().getPropriedade().getMunicipio().getNome())
			   .append("/")
			   .append(loteEnfermidadeAbatedouroFrigorifico.getProprietario().getPropriedade().getMunicipio().getUf().getUf());
		
		
		
		email.setHostName("smtp.gmail.com");
		email.setAuthentication(USERNAME, PASSWORD);
//		email.setStartTLSEnabled(true);
		email.setFrom(EMAIL_EMITENTE, NOME_EMITENTE);
//		email.getMailSession().getProperties().put("mail.smtp.ssl.trust", "cuco.mt.gov.br");
		email.setSmtpPort(465);
		email.addTo(emailDestinatario, "");
		email.setSubject(assunto.toString());
		email.setHtmlMsg(writer.toString());
		email.setSSL(true);
        email.send();
	}

}
