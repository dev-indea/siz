package br.gov.mt.indea.sistemaformin.entity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

@Audited
@Entity
@Table(name="termo_fiscalizacao")
public class TermoFiscalizacao extends BaseEntity<Long>{

	private static final long serialVersionUID = 4262630873173938138L;
	
	@Id
	@SequenceGenerator(name="termo_fiscalizacao_seq", sequenceName="termo_fiscalizacao_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="termo_fiscalizacao_seq")
	private Long id;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_usuario")
	private Usuario usuario;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_cadastro")
	private Calendar dataCadastro;
	
	@Column(length=17)
	private String numero;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_visita")
	private Calendar dataVisita;
	
	@Column(length=4000)
	private String ocorrencias;
	
	@OneToOne(cascade={CascadeType.ALL}, orphanRemoval=true, fetch=FetchType.LAZY)
	@JoinColumn(name="id_revenda")
	private br.gov.mt.indea.sistemaformin.webservice.entity.Revenda revenda;
	
	@OneToOne(cascade={CascadeType.ALL}, orphanRemoval=true, fetch=FetchType.LAZY)
	@JoinColumn(name="id_veterinario")
	private br.gov.mt.indea.sistemaformin.webservice.entity.Veterinario veterinario;
	
	@OneToMany(mappedBy="termoFiscalizacao", cascade={CascadeType.ALL}, orphanRemoval=true, fetch=FetchType.LAZY)
	private List<ChavePrincipalTermoFiscalizacao> listChavePrincipalTermoFiscalizacao = new ArrayList<ChavePrincipalTermoFiscalizacao>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Calendar getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Calendar dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Calendar getDataVisita() {
		return dataVisita;
	}

	public void setDataVisita(Calendar dataVisita) {
		this.dataVisita = dataVisita;
	}

	public String getOcorrencias() {
		return ocorrencias;
	}

	public void setOcorrencias(String ocorrencias) {
		this.ocorrencias = ocorrencias;
	}

	public br.gov.mt.indea.sistemaformin.webservice.entity.Veterinario getVeterinario() {
		return veterinario;
	}

	public void setVeterinario(
			br.gov.mt.indea.sistemaformin.webservice.entity.Veterinario veterinario) {
		this.veterinario = veterinario;
	}

	public List<ChavePrincipalTermoFiscalizacao> getListChavePrincipalTermoFiscalizacao() {
		return listChavePrincipalTermoFiscalizacao;
	}

	public void setListChavePrincipalTermoFiscalizacao(
			List<ChavePrincipalTermoFiscalizacao> listChavePrincipalTermoFiscalizacao) {
		this.listChavePrincipalTermoFiscalizacao = listChavePrincipalTermoFiscalizacao;
	}

	public br.gov.mt.indea.sistemaformin.webservice.entity.Revenda getRevenda() {
		return revenda;
	}

	public void setRevenda(
			br.gov.mt.indea.sistemaformin.webservice.entity.Revenda revenda) {
		this.revenda = revenda;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TermoFiscalizacao other = (TermoFiscalizacao) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}