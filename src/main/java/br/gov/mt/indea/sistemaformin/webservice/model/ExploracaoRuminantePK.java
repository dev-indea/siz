package br.gov.mt.indea.sistemaformin.webservice.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;

public class ExploracaoRuminantePK implements Serializable {

    private static final long serialVersionUID = -5420045419641828190L;

	private Integer exploracao;

    @XmlElement
    private Especie especie;
    
    public ExploracaoRuminantePK() {
    }

    public Integer getExploracao() {
        return exploracao;
    }

    public void setExploracao(Integer exploracao) {
        this.exploracao = exploracao;
    }

    public Especie getEspecie() {
        return especie;
    }

    public void setEspecie(Especie especie) {
        this.especie = especie;
    }

    @Override
    public int hashCode() {
        return exploracao.hashCode() + especie.hashCode();
    }
    
    @Override
    public boolean equals(Object o) {
        if (o instanceof ExploracaoRuminantePK) {
            ExploracaoRuminantePK exploracaoEquideo = (ExploracaoRuminantePK) o;
            if (!exploracaoEquideo.getExploracao().equals(exploracao)) {
                return false;
            }
            if (!exploracaoEquideo.getEspecie().equals(especie)) {
                return false;
            }
            return true;
        }
        return false;
    }
    
}