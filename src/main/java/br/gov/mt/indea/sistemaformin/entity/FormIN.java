package br.gov.mt.indea.sistemaformin.entity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

import br.gov.mt.indea.sistemaformin.enums.Dominio.Especie;
import br.gov.mt.indea.sistemaformin.enums.Dominio.FaseExploracaoBovinosEBubalinos;
import br.gov.mt.indea.sistemaformin.enums.Dominio.FaseExploracaoCaprinos;
import br.gov.mt.indea.sistemaformin.enums.Dominio.FaseExploracaoOvinos;
import br.gov.mt.indea.sistemaformin.enums.Dominio.FinalidadeExploracaoBovinosEBubalinos;
import br.gov.mt.indea.sistemaformin.enums.Dominio.FinalidadeExploracaoCaprinos;
import br.gov.mt.indea.sistemaformin.enums.Dominio.FinalidadeExploracaoOvinos;
import br.gov.mt.indea.sistemaformin.enums.Dominio.FonteDaNotificacao;
import br.gov.mt.indea.sistemaformin.enums.Dominio.MedidasAdotadasNoEstabelecimento;
import br.gov.mt.indea.sistemaformin.enums.Dominio.MotivoInicialDaInvestigacao;
import br.gov.mt.indea.sistemaformin.enums.Dominio.SimNao;
import br.gov.mt.indea.sistemaformin.enums.Dominio.SimNaoSI;
import br.gov.mt.indea.sistemaformin.enums.Dominio.SistemaDeCriacaoPropriedade;
import br.gov.mt.indea.sistemaformin.enums.Dominio.StatusFormIN;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoExploracaoAbelhas;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoExploracaoAves;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoExploracaoCoelhos;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoExploracaoEquideos;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoExploracaoSuinos;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoInvestigacaoFormCOM;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoPropriedade;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoTesteLaboratorial;
import br.gov.mt.indea.sistemaformin.enums.Dominio.VigilanciaSindromica;
import br.gov.mt.indea.sistemaformin.webservice.entity.Produtor;
import br.gov.mt.indea.sistemaformin.webservice.entity.Propriedade;

@Audited
@Entity
@Table(name="form_in")
public class FormIN extends BaseEntity<Long> implements Cloneable{
	
	private static final long serialVersionUID = 8959003967936831055L;

	@Id
	@SequenceGenerator(name="form_in_seq", sequenceName="form_in_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="form_in_seq")
	private Long id;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_usuario")
	private Usuario usuario;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_cadastro")
	private Calendar dataCadastro;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_uf")
	private UF uf;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_municipio")
	private Municipio municipio;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_propriedade")
	private Propriedade propriedade;
	
	@OneToOne(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH}, fetch=FetchType.LAZY)
	@JoinColumn(name="id_proprietario")
    private Produtor proprietario;
	
	@Column(name="codigo_propriedade")
    private Long codigoPropriedade;
	
	@Column(name = "nome_propriedade")
    private String nomePropriedade;
	
	@Enumerated(EnumType.STRING)
	@Column(name="tipo_propriedade")
    private TipoPropriedade tipoPropriedade;
    
	@Enumerated(EnumType.STRING)
	@Column(name="sistema_criacao_propriedade")
	private SistemaDeCriacaoPropriedade sistemaDeCriacaoPropriedade;
    	
	@Column(length=12)
	private String numero;
	
	@Column(name = "numero_protocolo_sisbravet", length=50)
	private String numeroProtocoloSISBRAVET;
	
	@Column(name = "numero_form_in_sisbravet", length=50)
	private String numeroFormINSISBRAVET;
	
	@Column(name = "data_form_in_sisbravet", length=50)
	private Calendar dataSISBRAVET;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_visita_propriedade_rural")
	private VisitaPropriedadeRural visitaPropriedadeRural;

	@Enumerated(EnumType.STRING)
	private SimNao retificado = SimNao.NAO;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_retificacao")
	private Calendar dataRetificacao;
	
	@Column(name="justificativa_retificacao", length=4000)
	private String justificativaRetificacao;
	
	@Enumerated(EnumType.STRING)
	@Column(name="fonte_notificacao")
	private FonteDaNotificacao fonteDaNotificacao;
	
	@Enumerated(EnumType.STRING)
	@Column(name="motivo_investigacao")
	private MotivoInicialDaInvestigacao motivoInicialDaInvestigacao;
	
	@Column(name="data_notificacao")
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar dataDaNotificacao;
	
	@Column(name="descricao_notificacao", length=4000)
	private String descricaoDaNotificacao;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_formIN_relacionado")
	private FormIN formINRelacionado;
	
	@OneToOne(cascade={CascadeType.ALL}, orphanRemoval=true, fetch=FetchType.LAZY)
	@JoinColumn(name="id_pessoa")
	private RepresentanteEstabelecimento representanteEstabelecimento = new RepresentanteEstabelecimento();
	
	@Column(name="data_abertura")
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar dataAbertura;
	
	@Column(name="data_inicio_evento")
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar dataInicioEvento;
	
	@Enumerated(EnumType.STRING)
	@Column(name="fim_atendimento")
	private SimNao fimAtendimento = SimNao.NAO;
	
	@Enumerated(EnumType.STRING)
	@Column(name="vigilancia_sindromica")
	private SimNao vigilanciaSindromica = SimNao.NAO;
	
	@Enumerated(EnumType.STRING)
	@Column(name="doenca_vigilancia_sindromica")
	private VigilanciaSindromica doencaDeVigilanciaSindromica;
	
	@Column(name="diagnostico_provavel", length=300)
	private String dianosticoProvavel;
	
	@Column(name="diagnostico_conclusivo", length=300)
	private String dianosticoConclusivo;
	
	@Column(name="data_resultado_diag_lab")
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar dataResultadoDiagnosticoLaboratorial;
	
	@Column(name="identificacao_diag_lab", length=255)
	private String identificacaoDiagnosticoLaboratorial;
	
	@Enumerated(EnumType.STRING)
	@Column(name="tipo_teste_diag_lab")
	private TipoTesteLaboratorial tipoTesteLaboratorial;
	
	@Column(name="descricao_achados", length=4000)
	private String descricaoDosAchadosDaOcorrencia;
	
	@Column(length=4000)
	private String observacao;
	
	@ElementCollection(targetClass = Especie.class, fetch=FetchType.LAZY)
	@JoinTable(name = "formin_especies_afetadas", joinColumns = @JoinColumn(name = "id_form_in", nullable = false))
	@Column(name = "id_especie")
	@Enumerated(EnumType.STRING)
	private List<Especie> especiesAfetadas = new ArrayList<Especie>();
	
	
	@OneToOne(cascade={CascadeType.ALL}, orphanRemoval=true, fetch=FetchType.LAZY)
	@JoinColumn(name="id_info_bovinos")
	private InformacoesBovinos bovinos = new InformacoesBovinos();
	
	@OneToOne(cascade={CascadeType.ALL}, orphanRemoval=true, fetch=FetchType.LAZY)
	@JoinColumn(name="id_info_bubalinos")
	private InformacoesBubalinos bubalinos = new InformacoesBubalinos();
	
	@OneToOne(cascade={CascadeType.ALL}, orphanRemoval=true, fetch=FetchType.LAZY)
	@JoinColumn(name="id_info_caprinos")
	private InformacoesCaprinos caprinos = new InformacoesCaprinos();
	
	@OneToOne(cascade={CascadeType.ALL}, orphanRemoval=true, fetch=FetchType.LAZY)
	@JoinColumn(name="id_info_ovinos")
	private InformacoesOvinos ovinos = new InformacoesOvinos();
	
	@OneToOne(cascade={CascadeType.ALL}, orphanRemoval=true, fetch=FetchType.LAZY)
	@JoinColumn(name="id_info_suinos")
	private InformacoesSuinos suinos = new InformacoesSuinos();
	
	@OneToOne(cascade={CascadeType.ALL}, orphanRemoval=true, fetch=FetchType.LAZY)
	@JoinColumn(name="id_info_equinos")
	private InformacoesEquinos equinos = new InformacoesEquinos();
	
	@OneToOne(cascade={CascadeType.ALL}, orphanRemoval=true, fetch=FetchType.LAZY)
	@JoinColumn(name="id_info_asininos")
	private InformacoesAsininos asininos = new InformacoesAsininos();
	
	@OneToOne(cascade={CascadeType.ALL}, orphanRemoval=true, fetch=FetchType.LAZY)
	@JoinColumn(name="id_info_muares")
	private InformacoesMuares muares = new InformacoesMuares();
	
	@OneToOne(cascade={CascadeType.ALL}, orphanRemoval=true, fetch=FetchType.LAZY)
	@JoinColumn(name="id_info_aves")
	private InformacoesAves aves = new InformacoesAves();
	
	@OneToOne(cascade={CascadeType.ALL}, orphanRemoval=true, fetch=FetchType.LAZY)
	@JoinColumn(name="id_info_abelhas")
	private InformacoesAbelhas abelhas = new InformacoesAbelhas();
	
	@OneToOne(cascade={CascadeType.ALL}, orphanRemoval=true, fetch=FetchType.LAZY)
	@JoinColumn(name="id_info_lagomorfos")
	private InformacoesLagomorfos lagomorfos = new InformacoesLagomorfos();
	
	@OneToOne(cascade={CascadeType.ALL}, orphanRemoval=true, fetch=FetchType.LAZY)
	@JoinColumn(name="id_info_outros")
	private InformacoesOutrosAnimais outrosAnimais = new InformacoesOutrosAnimais();
	
	@Enumerated(EnumType.STRING)
	@Column(name="finalidade_exploracao_bov_bub")
	private FinalidadeExploracaoBovinosEBubalinos finalidadeExploracaoBovinosEBubalinos;
	
	@Enumerated(EnumType.STRING)
	@Column(name="fase_exploracao_bov_bub")
	private FaseExploracaoBovinosEBubalinos faseExploracaoBovinosEBubalinos;
		
	@Enumerated(EnumType.STRING)
	@Column(name="finalidade_exploracao_caprino")
	private FinalidadeExploracaoCaprinos finalidadeExploracaoCaprinos;
	
	@Enumerated(EnumType.STRING)
	@Column(name="fase_exploracao_caprinos")
	private FaseExploracaoCaprinos faseExploracaoCaprinos;
	
	@Enumerated(EnumType.STRING)
	@Column(name="finalidade_exploracao_ovinos")
	private FinalidadeExploracaoOvinos finalidadeExploracaoOvinos;
	
	@Enumerated(EnumType.STRING)
	@Column(name="fase_exploracao_ovinos")
	private FaseExploracaoOvinos faseExploracaoOvinos;
	
	@Enumerated(EnumType.STRING)
	@Column(name="tipo_exploracao_suinos")
	private TipoExploracaoSuinos tipoExploracaoSuinos;
	
	@Enumerated(EnumType.STRING)
	@Column(name="tipo_exploracao_equideos")
	private TipoExploracaoEquideos tipoExploracaoEquideos;
	
	@Enumerated(EnumType.STRING)
	@Column(name="tipo_exploracao_aves")
	private TipoExploracaoAves tipoExploracaoAves;
	
	@ElementCollection(targetClass = TipoExploracaoAbelhas.class, fetch=FetchType.LAZY)
	@JoinTable(name = "formin_tipo_expl_abelhas", joinColumns = @JoinColumn(name = "id_form_in", nullable = false))
	@Column(name = "id_formin_tipo_expl_abelhas")
	@Enumerated(EnumType.STRING)
	private List<TipoExploracaoAbelhas> tipoExploracaoAbelhas = new ArrayList<TipoExploracaoAbelhas>();
	
	@ElementCollection(targetClass = TipoExploracaoCoelhos.class, fetch=FetchType.LAZY)
	@JoinTable(name = "formin_tipo_expl_coelhos", joinColumns = @JoinColumn(name = "id_form_in", nullable = false))
	@Column(name = "id_formin_tipo_expl_coelhos")
	@Enumerated(EnumType.STRING)
	private List<TipoExploracaoCoelhos> tipoExploracaoCoelhos = new ArrayList<TipoExploracaoCoelhos>();
	
	@Enumerated(EnumType.STRING)
	@Column(name="aplica_se_medidas_adotadas")
	private SimNao aplica_seMedidasAdotadas;
	
	@ElementCollection(targetClass = MedidasAdotadasNoEstabelecimento.class, fetch=FetchType.LAZY)
	@JoinTable(name = "formin_medidas", joinColumns = @JoinColumn(name = "id_form_in", nullable = false))
	@Column(name = "id_formin_medidas")
	@Enumerated(EnumType.STRING)
	private List<MedidasAdotadasNoEstabelecimento> medidas = new ArrayList<MedidasAdotadasNoEstabelecimento>();
	
	@Column(name="provavel_origem", length=255)
	private String provavelOrigem;
	
	@Enumerated(EnumType.STRING)
	@Column(name="estabelecimento_de_turismo")
	private SimNaoSI estabelecimentoDeTurismo;
	
	@Enumerated(EnumType.STRING)
	@Column(name="utiliza_instalacoes_vizinhos")
	private SimNaoSI utilizaInstalacoesDeVizinhos;
	
	@Enumerated(EnumType.STRING)
	@Column(name="ingresso_recente_veiculos")
	private SimNaoSI ingressoRecenteDeVeiculos;
	
	@Enumerated(EnumType.STRING)
	@Column(name="animais_part_aglomeracao")
	private SimNaoSI animaisParticipamDeAglomeracao;
	
	@Enumerated(EnumType.STRING)
	@Column(name="alguem_visitou_outro_estab")
	private SimNaoSI alguemVisitouOutroEstabelecimento;
	
	@Enumerated(EnumType.STRING)
	@Column(name="recebeu_visita_outro_estab")
	private SimNaoSI recebeuVisitaDeOutroEstabelecimento;
	
	@Enumerated(EnumType.STRING)
	@Column(name="mudanca_alimentacao")
	private SimNaoSI mudancaDeAlimentacao;
	
	@Enumerated(EnumType.STRING)
	@Column(name="mao_de_obra_de_vizinhos")
	private SimNaoSI maoDeObraDeVizinhos;
	
	@Enumerated(EnumType.STRING)
	@Column(name="estab_usado_aglomeracao")
	private SimNaoSI estabelecimentoUsadoParaAglomeracaoDeAnimais;
	
	@Enumerated(EnumType.STRING)
	@Column(name="estab_proximo_area_risco")
	private SimNaoSI estabelecimentoProximoAAreasDeRisco;
	
	@Enumerated(EnumType.STRING)
	@Column(name="alguem_visitou_outro_pais")
	private SimNaoSI alguemVisitouOutroPais;
	
	@Enumerated(EnumType.STRING)
	@Column(name="historico_ingestao_planta_tox")
	private SimNaoSI historicoDeIngestaoDePlantasToxicas;
	
	@Enumerated(EnumType.STRING)
	@Column(name="houve_vacinacoes")
	private SimNaoSI houveVacinacoes;
	
	@Enumerated(EnumType.STRING)
	@Column(name="houve_medicacoes")
	private SimNaoSI houveMedicacoes;
	
	@Enumerated(EnumType.STRING)
	@Column(name="houve_transito")
	private SimNaoSI houveTransito;
	
	@OneToMany(mappedBy="formIN", cascade={CascadeType.ALL}, orphanRemoval=true, fetch=FetchType.LAZY)
	private List<VacinacaoFormIN> vacinas = new ArrayList<VacinacaoFormIN>();
	
	@OneToMany(mappedBy="formIN", cascade={CascadeType.ALL}, orphanRemoval=true, fetch=FetchType.LAZY)
	private List<Medicacao> medicacoes = new ArrayList<Medicacao>();
	
	@OneToMany(mappedBy="formIN", cascade={CascadeType.ALL}, orphanRemoval=true, fetch=FetchType.LAZY)
	private List<TransitoDeAnimais> transitoDeAnimais = new ArrayList<TransitoDeAnimais>();
	
	@Column(name="periodo_avaliacao_transito")
	private Long periodoAvalicaoDoTransito;
	
	@Enumerated(EnumType.STRING)
	@Column(name="houve_coleta_de_amostras")
	private SimNao houveColetaDeAmostras = SimNao.NAO;
	
	@OneToMany(mappedBy="formIN", orphanRemoval=true, cascade={CascadeType.REMOVE}, fetch=FetchType.LAZY)
	@OrderBy("numeroInvestigacao DESC")
	private List<FormCOM> listaFormCOM = new ArrayList<FormCOM>();
	
	@OneToMany(mappedBy="formIN", orphanRemoval=true, cascade={CascadeType.REMOVE}, fetch=FetchType.LAZY)
	@OrderBy("dataCadastro DESC")
	private List<FolhaAdicional> listaFolhasAdicionais = new ArrayList<FolhaAdicional>();
	
	@OneToMany(mappedBy="formIN", orphanRemoval=true, cascade={CascadeType.REMOVE}, fetch=FetchType.LAZY)
	@OrderBy("numeroInspecao DESC")
	private List<FormVIN> listaFormVIN = new ArrayList<FormVIN>();
	
	@OneToMany(mappedBy="formIN", orphanRemoval=true, cascade={CascadeType.REMOVE}, fetch=FetchType.LAZY)
	@OrderBy("dataColheita DESC")
	private List<FormLAB> listaFormLAB = new ArrayList<FormLAB>();
	
	@OneToMany(mappedBy="formIN", orphanRemoval=true, cascade={CascadeType.REMOVE}, fetch=FetchType.LAZY)
	@OrderBy("dataInvestigacao DESC")
	private List<FormSV> listaFormSV = new ArrayList<FormSV>();
	
	@OneToMany(mappedBy="formIN", orphanRemoval=true, cascade={CascadeType.REMOVE}, fetch=FetchType.LAZY)
	@OrderBy("dataInvestigacao DESC")
	private List<FormSH> listaFormSH = new ArrayList<FormSH>();
	
	@OneToMany(mappedBy="formIN", orphanRemoval=true, cascade={CascadeType.REMOVE}, fetch=FetchType.LAZY)
	private List<FormSN> listaFormSN = new ArrayList<FormSN>();
	
	@OneToMany(mappedBy="formIN", orphanRemoval=true, cascade={CascadeType.REMOVE}, fetch=FetchType.LAZY)
	private List<FormSRN> listaFormSRN = new ArrayList<FormSRN>();
	
	@OneToMany(mappedBy="formIN", orphanRemoval=true, cascade={CascadeType.REMOVE}, fetch=FetchType.LAZY)
	private List<FormAIE> listaFormAIE = new ArrayList<FormAIE>();
	
	@OneToMany(mappedBy="formIN", orphanRemoval=true, cascade={CascadeType.REMOVE}, fetch=FetchType.LAZY)
	private List<FormMormo> listaFormMormo = new ArrayList<FormMormo>();
	
	@OneToMany(mappedBy="formIN", orphanRemoval=true, cascade={CascadeType.REMOVE}, fetch=FetchType.LAZY)
	private List<FormMaleina> listaFormMaleina = new ArrayList<FormMaleina>();
	
	@OneToMany(mappedBy="formIN", orphanRemoval=true, cascade={CascadeType.REMOVE}, fetch=FetchType.LAZY)
	private List<FormEQ> listaFormEQ = new ArrayList<FormEQ>();
	
	@OneToMany(mappedBy="formIN", orphanRemoval=true, cascade={CascadeType.REMOVE}, fetch=FetchType.LAZY)
	private List<Resenho> listaResenho = new ArrayList<Resenho>();
	
	@OneToOne(cascade={CascadeType.ALL}, orphanRemoval=true, fetch=FetchType.LAZY)
	@JoinColumn(name="id_veterinario")
	private br.gov.mt.indea.sistemaformin.webservice.entity.Veterinario veterinario;
	
	@OneToMany(mappedBy="formINOriginal", orphanRemoval=true, cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	@OrderBy("data DESC")
	private List<HistoricoFormIN> listaHistoricoFormINs = new ArrayList<HistoricoFormIN>();
	
	@Enumerated(EnumType.STRING)
	@Column(name="status")
	private StatusFormIN statusFormIN = StatusFormIN.NOVO;
	
	@OneToOne(cascade={CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH}, orphanRemoval=false, fetch=FetchType.LAZY)
	@JoinColumn(name="id_detalhe_comunicacao_aie")
	private DetalheComunicacaoAIE detalheComunicacaoAIE;
	
	@OneToOne(cascade={CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH}, orphanRemoval=false, fetch=FetchType.LAZY)
	@JoinColumn(name="id_registro_entrada_lasa")
	private RegistroEntradaLASA registroEntradaLASA;
	
	@OneToOne(cascade={CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH}, orphanRemoval=false, fetch=FetchType.LAZY)
	@JoinColumn(name="id_detalhe_form_notifica")
	private DetalheFormNotifica detalheFormNotifica;
	
	@OneToOne(cascade={CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH}, orphanRemoval=false, fetch=FetchType.LAZY)
	@JoinColumn(name="id_lote_enf_abat_frig")
	private LoteEnfermidadeAbatedouroFrigorifico loteEnfermidadeAbatedouroFrigorifico;
	
	public boolean possuiFormComEncerramento(){
		if (this.listaFormCOM != null && !this.listaFormCOM.isEmpty()){
			for (FormCOM formCOM : listaFormCOM) {
				if (formCOM.getTipoInvestigacao().equals(TipoInvestigacaoFormCOM.ENCERRAMENTO))
					return true;
			}
		}
			
		return false;
	}
	
	public boolean isInvestigacaoEncerrada(){
		return this.fimAtendimento.equals(SimNao.SIM) || this.possuiFormComEncerramento();
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public UF getUf() {
		return uf;
	}

	public void setUf(UF uf) {
		this.uf = uf;
	}

	public Municipio getMunicipio() {
		return municipio;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public SimNao getRetificado() {
		return retificado;
	}

	public void setRetificado(SimNao retificador) {
		this.retificado = retificador;
	}

	public FonteDaNotificacao getFonteDaNotificacao() {
		return fonteDaNotificacao;
	}

	public void setFonteDaNotificacao(FonteDaNotificacao fonteDaNotificacao) {
		this.fonteDaNotificacao = fonteDaNotificacao;
	}

	public MotivoInicialDaInvestigacao getMotivoInicialDaInvestigacao() {
		return motivoInicialDaInvestigacao;
	}

	public void setMotivoInicialDaInvestigacao(MotivoInicialDaInvestigacao motivoInicialDaInvestigacao) {
		this.motivoInicialDaInvestigacao = motivoInicialDaInvestigacao;
	}

	public Calendar getDataDaNotificacao() {
		return dataDaNotificacao;
	}

	public void setDataDaNotificacao(Calendar dataDaNotificacao) {
		this.dataDaNotificacao = dataDaNotificacao;
	}

	public String getDescricaoDaNotificacao() {
		return descricaoDaNotificacao;
	}

	public void setDescricaoDaNotificacao(String descricaoDaNotificacao) {
		this.descricaoDaNotificacao = descricaoDaNotificacao;
	}

	public RepresentanteEstabelecimento getRepresentanteEstabelecimento() {
		return representanteEstabelecimento;
	}

	public void setRepresentanteEstabelecimento(
			RepresentanteEstabelecimento representanteEstabelecimento) {
		this.representanteEstabelecimento = representanteEstabelecimento;
	}

	public Calendar getDataAbertura() {
		return dataAbertura;
	}

	public void setDataAbertura(Calendar dataAbertura) {
		this.dataAbertura = dataAbertura;
	}

	public Calendar getDataInicioEvento() {
		return dataInicioEvento;
	}

	public void setDataInicioEvento(Calendar dataInicioEvento) {
		this.dataInicioEvento = dataInicioEvento;
	}

	public SimNao getFimAtendimento() {
		return fimAtendimento;
	}

	public void setFimAtendimento(SimNao fimAtendimento) {
		this.fimAtendimento = fimAtendimento;
	}

	public SimNao getVigilanciaSindromica() {
		return vigilanciaSindromica;
	}

	public void setVigilanciaSindromica(SimNao vigilanciaSindromica) {
		this.vigilanciaSindromica = vigilanciaSindromica;
	}

	public VigilanciaSindromica getDoencaDeVigilanciaSindromica() {
		return doencaDeVigilanciaSindromica;
	}

	public void setDoencaDeVigilanciaSindromica(VigilanciaSindromica doencaDeVigilanciaSindromica) {
		this.doencaDeVigilanciaSindromica = doencaDeVigilanciaSindromica;
	}

	public String getDianosticoProvavel() {
		return dianosticoProvavel;
	}

	public void setDianosticoProvavel(String dianosticoProvavel) {
		this.dianosticoProvavel = dianosticoProvavel;
	}

	public String getDianosticoConclusivo() {
		return dianosticoConclusivo;
	}

	public void setDianosticoConclusivo(String dianosticoConclusivo) {
		this.dianosticoConclusivo = dianosticoConclusivo;
	}

	public String getDescricaoDosAchadosDaOcorrencia() {
		return descricaoDosAchadosDaOcorrencia;
	}

	public void setDescricaoDosAchadosDaOcorrencia(
			String descricaoDosAchadosDaOcorrencia) {
		this.descricaoDosAchadosDaOcorrencia = descricaoDosAchadosDaOcorrencia;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public FormIN getFormINRelacionado() {
		return formINRelacionado;
	}

	public void setFormINRelacionado(FormIN formINRelacionado) {
		this.formINRelacionado = formINRelacionado;
	}
	
	public InformacoesBovinos getBovinos() {
		return bovinos;
	}

	public void setBovinos(InformacoesBovinos bovinos) {
		this.bovinos = bovinos;
	}

	public InformacoesBubalinos getBubalinos() {
		return bubalinos;
	}

	public void setBubalinos(InformacoesBubalinos bubalinos) {
		this.bubalinos = bubalinos;
	}

	public InformacoesCaprinos getCaprinos() {
		return caprinos;
	}

	public void setCaprinos(InformacoesCaprinos caprinos) {
		this.caprinos = caprinos;
	}
	public InformacoesOvinos getOvinos() {
		return ovinos;
	}

	public void setOvinos(InformacoesOvinos ovinos) {
		this.ovinos = ovinos;
	}

	public InformacoesSuinos getSuinos() {
		return suinos;
	}

	public void setSuinos(InformacoesSuinos suinos) {
		this.suinos = suinos;
	}
	
	public InformacoesEquinos getEquinos() {
		return equinos;
	}

	public void setEquinos(InformacoesEquinos equinos) {
		this.equinos = equinos;
	}

	public InformacoesAsininos getAsininos() {
		return asininos;
	}

	public void setAsininos(InformacoesAsininos asininos) {
		this.asininos = asininos;
	}

	public InformacoesMuares getMuares() {
		return muares;
	}

	public void setMuares(InformacoesMuares muares) {
		this.muares = muares;
	}

	public InformacoesAves getAves() {
		return aves;
	}

	public void setAves(InformacoesAves aves) {
		this.aves = aves;
	}

	public InformacoesAbelhas getAbelhas() {
		return abelhas;
	}

	public void setAbelhas(InformacoesAbelhas abelhas) {
		this.abelhas = abelhas;
	}

	public InformacoesLagomorfos getLagomorfos() {
		return lagomorfos;
	}

	public void setLagomorfos(InformacoesLagomorfos lagomorfos) {
		this.lagomorfos = lagomorfos;
	}

	public InformacoesOutrosAnimais getOutrosAnimais() {
		return outrosAnimais;
	}

	public void setOutrosAnimais(InformacoesOutrosAnimais outrosAnimais) {
		this.outrosAnimais = outrosAnimais;
	}	

	public FinalidadeExploracaoBovinosEBubalinos getFinalidadeExploracaoBovinosEBubalinos() {
		return finalidadeExploracaoBovinosEBubalinos;
	}

	public void setFinalidadeExploracaoBovinosEBubalinos(
			FinalidadeExploracaoBovinosEBubalinos finalidadeExploracaoBovinosEBubalinos) {
		this.finalidadeExploracaoBovinosEBubalinos = finalidadeExploracaoBovinosEBubalinos;
	}

	public FaseExploracaoBovinosEBubalinos getFaseExploracaoBovinosEBubalinos() {
		return faseExploracaoBovinosEBubalinos;
	}

	public void setFaseExploracaoBovinosEBubalinos(
			FaseExploracaoBovinosEBubalinos faseExploracaoBovinosEBubalinos) {
		this.faseExploracaoBovinosEBubalinos = faseExploracaoBovinosEBubalinos;
	}

	public FinalidadeExploracaoCaprinos getFinalidadeExploracaoCaprinos() {
		return finalidadeExploracaoCaprinos;
	}

	public void setFinalidadeExploracaoCaprinos(
			FinalidadeExploracaoCaprinos finalidadeExploracaoCaprinos) {
		this.finalidadeExploracaoCaprinos = finalidadeExploracaoCaprinos;
	}

	public FaseExploracaoCaprinos getFaseExploracaoCaprinos() {
		return faseExploracaoCaprinos;
	}

	public void setFaseExploracaoCaprinos(
			FaseExploracaoCaprinos faseExploracaoCaprinos) {
		this.faseExploracaoCaprinos = faseExploracaoCaprinos;
	}

	public FinalidadeExploracaoOvinos getFinalidadeExploracaoOvinos() {
		return finalidadeExploracaoOvinos;
	}

	public void setFinalidadeExploracaoOvinos(
			FinalidadeExploracaoOvinos finalidadeExploracaoOvinos) {
		this.finalidadeExploracaoOvinos = finalidadeExploracaoOvinos;
	}

	public FaseExploracaoOvinos getFaseExploracaoOvinos() {
		return faseExploracaoOvinos;
	}

	public void setFaseExploracaoOvinos(FaseExploracaoOvinos faseExploracaoOvinos) {
		this.faseExploracaoOvinos = faseExploracaoOvinos;
	}

	public TipoExploracaoSuinos getTipoExploracaoSuinos() {
		return tipoExploracaoSuinos;
	}

	public void setTipoExploracaoSuinos(TipoExploracaoSuinos tipoExploracaoSuinos) {
		this.tipoExploracaoSuinos = tipoExploracaoSuinos;
	}

	public TipoExploracaoEquideos getTipoExploracaoEquideos() {
		return tipoExploracaoEquideos;
	}

	public void setTipoExploracaoEquideos(
			TipoExploracaoEquideos tipoExploracaoEquideos) {
		this.tipoExploracaoEquideos = tipoExploracaoEquideos;
	}

	public TipoExploracaoAves getTipoExploracaoAves() {
		return tipoExploracaoAves;
	}

	public void setTipoExploracaoAves(TipoExploracaoAves tipoExploracaoAves) {
		this.tipoExploracaoAves = tipoExploracaoAves;
	}

	public List<TipoExploracaoAbelhas> getTipoExploracaoAbelhas() {
		return tipoExploracaoAbelhas;
	}

	public void setTipoExploracaoAbelhas(
			List<TipoExploracaoAbelhas> tipoExploracaoAbelhas) {
		this.tipoExploracaoAbelhas = tipoExploracaoAbelhas;
	}

	public List<TipoExploracaoCoelhos> getTipoExploracaoCoelhos() {
		return tipoExploracaoCoelhos;
	}

	public void setTipoExploracaoCoelhos(
			List<TipoExploracaoCoelhos> tipoExploracaoCoelhos) {
		this.tipoExploracaoCoelhos = tipoExploracaoCoelhos;
	}

	public List<MedidasAdotadasNoEstabelecimento> getMedidas() {
		return medidas;
	}

	public void setMedidas(List<MedidasAdotadasNoEstabelecimento> medidas2) {
		this.medidas = medidas2;
	}

	public String getProvavelOrigem() {
		return provavelOrigem;
	}

	public void setProvavelOrigem(String provavelOrigem) {
		this.provavelOrigem = provavelOrigem;
	}

	public SimNaoSI getEstabelecimentoDeTurismo() {
		return estabelecimentoDeTurismo;
	}

	public void setEstabelecimentoDeTurismo(SimNaoSI estabelecimentoDeTurismo) {
		this.estabelecimentoDeTurismo = estabelecimentoDeTurismo;
	}

	public SimNaoSI getUtilizaInstalacoesDeVizinhos() {
		return utilizaInstalacoesDeVizinhos;
	}

	public void setUtilizaInstalacoesDeVizinhos(
			SimNaoSI utilizaInstalacoesDeVizinhos) {
		this.utilizaInstalacoesDeVizinhos = utilizaInstalacoesDeVizinhos;
	}

	public SimNaoSI getIngressoRecenteDeVeiculos() {
		return ingressoRecenteDeVeiculos;
	}

	public void setIngressoRecenteDeVeiculos(SimNaoSI ingressoRecenteDeVeiculos) {
		this.ingressoRecenteDeVeiculos = ingressoRecenteDeVeiculos;
	}

	public SimNaoSI getAnimaisParticipamDeAglomeracao() {
		return animaisParticipamDeAglomeracao;
	}

	public void setAnimaisParticipamDeAglomeracao(
			SimNaoSI animaisParticipamDeAglomeracao) {
		this.animaisParticipamDeAglomeracao = animaisParticipamDeAglomeracao;
	}

	public SimNaoSI getAlguemVisitouOutroEstabelecimento() {
		return alguemVisitouOutroEstabelecimento;
	}

	public void setAlguemVisitouOutroEstabelecimento(
			SimNaoSI alguemVisitouOutroEstabelecimento) {
		this.alguemVisitouOutroEstabelecimento = alguemVisitouOutroEstabelecimento;
	}

	public SimNaoSI getRecebeuVisitaDeOutroEstabelecimento() {
		return recebeuVisitaDeOutroEstabelecimento;
	}

	public void setRecebeuVisitaDeOutroEstabelecimento(
			SimNaoSI recebeuVisitaDeOutroEstabelecimento) {
		this.recebeuVisitaDeOutroEstabelecimento = recebeuVisitaDeOutroEstabelecimento;
	}

	public SimNaoSI getMudancaDeAlimentacao() {
		return mudancaDeAlimentacao;
	}

	public void setMudancaDeAlimentacao(SimNaoSI mudancaDeAlimentacao) {
		this.mudancaDeAlimentacao = mudancaDeAlimentacao;
	}

	public SimNaoSI getMaoDeObraDeVizinhos() {
		return maoDeObraDeVizinhos;
	}

	public void setMaoDeObraDeVizinhos(SimNaoSI maoDeObraDeVizinhos) {
		this.maoDeObraDeVizinhos = maoDeObraDeVizinhos;
	}

	public SimNaoSI getEstabelecimentoUsadoParaAglomeracaoDeAnimais() {
		return estabelecimentoUsadoParaAglomeracaoDeAnimais;
	}

	public void setEstabelecimentoUsadoParaAglomeracaoDeAnimais(
			SimNaoSI estabelecimentoUsadoParaAglomeracaoDeAnimais) {
		this.estabelecimentoUsadoParaAglomeracaoDeAnimais = estabelecimentoUsadoParaAglomeracaoDeAnimais;
	}

	public SimNaoSI getEstabelecimentoProximoAAreasDeRisco() {
		return estabelecimentoProximoAAreasDeRisco;
	}

	public void setEstabelecimentoProximoAAreasDeRisco(
			SimNaoSI estabelecimentoProximoAAreasDeRisco) {
		this.estabelecimentoProximoAAreasDeRisco = estabelecimentoProximoAAreasDeRisco;
	}

	public SimNaoSI getAlguemVisitouOutroPais() {
		return alguemVisitouOutroPais;
	}

	public void setAlguemVisitouOutroPais(SimNaoSI alguemVisitouOutroPais) {
		this.alguemVisitouOutroPais = alguemVisitouOutroPais;
	}

	public SimNaoSI getHistoricoDeIngestaoDePlantasToxicas() {
		return historicoDeIngestaoDePlantasToxicas;
	}

	public void setHistoricoDeIngestaoDePlantasToxicas(
			SimNaoSI historicoDeIngestaoDePlantasToxicas) {
		this.historicoDeIngestaoDePlantasToxicas = historicoDeIngestaoDePlantasToxicas;
	}

	public List<VacinacaoFormIN> getVacinas() {
		return vacinas;
	}

	public void setVacinas(List<VacinacaoFormIN> listaVacinas) {
		this.vacinas = listaVacinas;
	}

	public List<Medicacao> getMedicacoes() {
		return medicacoes;
	}

	public void setMedicacoes(List<Medicacao> medicacoes) {
		this.medicacoes = medicacoes;
	}

	public List<TransitoDeAnimais> getTransitoDeAnimais() {
		Collections.sort(this.transitoDeAnimais);
		return transitoDeAnimais;
	}

	public void setTransitoDeAnimais(List<TransitoDeAnimais> transitoDeAnimais) {
		this.transitoDeAnimais = transitoDeAnimais;
	}

	public Calendar getDataRetificacao() {
		return dataRetificacao;
	}

	public void setDataRetificacao(Calendar dataRetificacao) {
		this.dataRetificacao = dataRetificacao;
	}

	public String getJustificativaRetificacao() {
		return justificativaRetificacao;
	}

	public void setJustificativaRetificacao(String justificativaRetificacao) {
		this.justificativaRetificacao = justificativaRetificacao;
	}

	public Long getPeriodoAvalicaoDoTransito() {
		return periodoAvalicaoDoTransito;
	}

	public void setPeriodoAvalicaoDoTransito(Long periodoAvalicaoDoTransito) {
		this.periodoAvalicaoDoTransito = periodoAvalicaoDoTransito;
	}

	public SimNao getHouveColetaDeAmostras() {
		return houveColetaDeAmostras;
	}

	public void setHouveColetaDeAmostras(SimNao houveColetaDeAmostras) {
		this.houveColetaDeAmostras = houveColetaDeAmostras;
	}

	public List<FormCOM> getListaFormCOM() {
		return listaFormCOM;
	}

	public void setListaFormCOM(List<FormCOM> listaFormCOM) {
		this.listaFormCOM = listaFormCOM;
	}

	public Calendar getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Calendar dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public List<FolhaAdicional> getListaFolhasAdicionais() {
		return listaFolhasAdicionais;
	}

	public void setListaFolhasAdicionais(List<FolhaAdicional> listaFolhasAdicionais) {
		this.listaFolhasAdicionais = listaFolhasAdicionais;
	}

	public List<FormVIN> getListaFormVIN() {
		return listaFormVIN;
	}

	public void setListaFormVIN(List<FormVIN> listaFormVIN) {
		this.listaFormVIN = listaFormVIN;
	}

	public List<FormLAB> getListaFormLAB() {
		return listaFormLAB;
	}

	public void setListaFormLAB(List<FormLAB> listaFormLAB) {
		this.listaFormLAB = listaFormLAB;
	}

	public List<FormSV> getListaFormSV() {
		return listaFormSV;
	}

	public void setListaFormSV(List<FormSV> listaFormSV) {
		this.listaFormSV = listaFormSV;
	}

	public List<FormSH> getListaFormSH() {
		return listaFormSH;
	}

	public void setListaFormSH(List<FormSH> listaFormSH) {
		this.listaFormSH = listaFormSH;
	}

	public List<FormSN> getListaFormSN() {
		return listaFormSN;
	}

	public void setListaFormSN(List<FormSN> listaFormSN) {
		this.listaFormSN = listaFormSN;
	}

	public List<FormSRN> getListaFormSRN() {
		return listaFormSRN;
	}

	public void setListaFormSRN(List<FormSRN> listaFormSRN) {
		this.listaFormSRN = listaFormSRN;
	}

	public List<FormAIE> getListaFormAIE() {
		return listaFormAIE;
	}

	public void setListaFormAIE(List<FormAIE> listaFormAIE) {
		this.listaFormAIE = listaFormAIE;
	}

	public List<FormMormo> getListaFormMormo() {
		return listaFormMormo;
	}

	public void setListaFormMormo(List<FormMormo> listaFormMormo) {
		this.listaFormMormo = listaFormMormo;
	}

	public List<FormMaleina> getListaFormMaleina() {
		return listaFormMaleina;
	}

	public void setListaFormMaleina(List<FormMaleina> listaFormMaleina) {
		this.listaFormMaleina = listaFormMaleina;
	}

	public List<FormEQ> getListaFormEQ() {
		return listaFormEQ;
	}

	public void setListaFormEQ(List<FormEQ> listaFormEQ) {
		this.listaFormEQ = listaFormEQ;
	}

	public SimNao getAplica_seMedidasAdotadas() {
		return aplica_seMedidasAdotadas;
	}

	public void setAplica_seMedidasAdotadas(SimNao aplica_seMedidasAdotadas) {
		this.aplica_seMedidasAdotadas = aplica_seMedidasAdotadas;
	}

	public List<Resenho> getListaResenho() {
		return listaResenho;
	}

	public void setListaResenho(List<Resenho> listaResenho) {
		this.listaResenho = listaResenho;
	}

	public Propriedade getPropriedade() {
		return propriedade;
	}

	public void setPropriedade(Propriedade propriedade) {
		this.propriedade = propriedade;
	}

	public br.gov.mt.indea.sistemaformin.webservice.entity.Veterinario getVeterinario() {
		return veterinario;
	}

	public void setVeterinario(br.gov.mt.indea.sistemaformin.webservice.entity.Veterinario veterinario) {
		this.veterinario = veterinario;
	}

	public TipoPropriedade getTipoPropriedade() {
		return tipoPropriedade;
	}

	public void setTipoPropriedade(TipoPropriedade tipoPropriedade) {
		this.tipoPropriedade = tipoPropriedade;
	}

	public SistemaDeCriacaoPropriedade getSistemaDeCriacaoPropriedade() {
		return sistemaDeCriacaoPropriedade;
	}

	public void setSistemaDeCriacaoPropriedade(
			SistemaDeCriacaoPropriedade sistemaDeCriacaoPropriedade) {
		this.sistemaDeCriacaoPropriedade = sistemaDeCriacaoPropriedade;
	}

	public VisitaPropriedadeRural getVisitaPropriedadeRural() {
		return visitaPropriedadeRural;
	}

	public void setVisitaPropriedadeRural(
			VisitaPropriedadeRural visitaPropriedadeRural) {
		this.visitaPropriedadeRural = visitaPropriedadeRural;
	}

	public List<HistoricoFormIN> getListaHistoricoFormINs() {
		if (this.listaHistoricoFormINs == null)
			this.listaHistoricoFormINs = new ArrayList<HistoricoFormIN>();
		return listaHistoricoFormINs;
	}

	public void setListaHistoricoFormINs(List<HistoricoFormIN> listaHistoricoFormINs) {
		this.listaHistoricoFormINs = listaHistoricoFormINs;
	}

	public StatusFormIN getStatusFormIN() {
		return statusFormIN;
	}

	public void setStatusFormIN(StatusFormIN statusFormIN) {
		this.statusFormIN = statusFormIN;
	}

	public SimNaoSI getHouveVacinacoes() {
		return houveVacinacoes;
	}

	public void setHouveVacinacoes(SimNaoSI houveVacinacoes) {
		this.houveVacinacoes = houveVacinacoes;
	}

	public SimNaoSI getHouveMedicacoes() {
		return houveMedicacoes;
	}

	public void setHouveMedicacoes(SimNaoSI houveMedicacoes) {
		this.houveMedicacoes = houveMedicacoes;
	}

	public SimNaoSI getHouveTransito() {
		return houveTransito;
	}

	public void setHouveTransito(SimNaoSI houveTransito) {
		this.houveTransito = houveTransito;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Calendar getDataResultadoDiagnosticoLaboratorial() {
		return dataResultadoDiagnosticoLaboratorial;
	}

	public void setDataResultadoDiagnosticoLaboratorial(
			Calendar dataResultadoDiagnosticoLaboratorial) {
		this.dataResultadoDiagnosticoLaboratorial = dataResultadoDiagnosticoLaboratorial;
	}

	public String getIdentificacaoDiagnosticoLaboratorial() {
		return identificacaoDiagnosticoLaboratorial;
	}

	public void setIdentificacaoDiagnosticoLaboratorial(
			String identificacaoDiagnosticoLaboratorial) {
		this.identificacaoDiagnosticoLaboratorial = identificacaoDiagnosticoLaboratorial;
	}

	public TipoTesteLaboratorial getTipoTesteLaboratorial() {
		return tipoTesteLaboratorial;
	}

	public void setTipoTesteLaboratorial(TipoTesteLaboratorial tipoTesteLaboratorial) {
		this.tipoTesteLaboratorial = tipoTesteLaboratorial;
	}

	public List<Especie> getEspeciesAfetadas() {
		return especiesAfetadas;
	}

	public void setEspeciesAfetadas(List<Especie> especiesAfetadas) {
		this.especiesAfetadas = especiesAfetadas;
	}

	public DetalheComunicacaoAIE getDetalheComunicacaoAIE() {
		return detalheComunicacaoAIE;
	}

	public void setDetalheComunicacaoAIE(DetalheComunicacaoAIE detalheComunicacaoAIE) {
		this.detalheComunicacaoAIE = detalheComunicacaoAIE;
	}

	public RegistroEntradaLASA getRegistroEntradaLASA() {
		return registroEntradaLASA;
	}

	public void setRegistroEntradaLASA(RegistroEntradaLASA registroEntradaLASA) {
		this.registroEntradaLASA = registroEntradaLASA;
	}
	
	public DetalheFormNotifica getDetalheFormNotifica() {
		return detalheFormNotifica;
	}

	public void setDetalheFormNotifica(DetalheFormNotifica detalheFormNotifica) {
		this.detalheFormNotifica = detalheFormNotifica;
	}

	public LoteEnfermidadeAbatedouroFrigorifico getLoteEnfermidadeAbatedouroFrigorifico() {
		return loteEnfermidadeAbatedouroFrigorifico;
	}

	public void setLoteEnfermidadeAbatedouroFrigorifico(
			LoteEnfermidadeAbatedouroFrigorifico loteEnfermidadeAbatedouroFrigorifico) {
		this.loteEnfermidadeAbatedouroFrigorifico = loteEnfermidadeAbatedouroFrigorifico;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FormIN other = (FormIN) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public Object clone() throws CloneNotSupportedException{
		FormIN cloned = (FormIN) super.clone();
		
		cloned.setId(null);
		cloned.setStatusFormIN(StatusFormIN.COPIA_VALIDADA);
		cloned.setListaFolhasAdicionais(null);
		cloned.setListaFormAIE(null);
		cloned.setListaFormCOM(null);
		cloned.setListaFormEQ(null);
		cloned.setListaFormLAB(null);
		cloned.setListaFormMaleina(null);
		cloned.setListaFormMormo(null);
		cloned.setListaFormSH(null);
		cloned.setListaFormSN(null);
		cloned.setListaFormSRN(null);
		cloned.setListaFormSV(null);
		cloned.setListaFormVIN(null);
		cloned.setListaResenho(null);
		cloned.setListaHistoricoFormINs(null);
		
		if (this.abelhas != null)
			cloned.setAbelhas((InformacoesAbelhas) this.getAbelhas().clone(cloned));
		if (this.asininos != null)
			cloned.setAsininos((InformacoesAsininos) this.getAsininos().clone(cloned));
		if (this.aves != null)
			cloned.setAves((InformacoesAves) this.getAves().clone(cloned));
		if (this.bovinos != null)
			cloned.setBovinos((InformacoesBovinos) this.getBovinos().clone(cloned));
		if (this.bubalinos != null)
			cloned.setBubalinos((InformacoesBubalinos) this.getBubalinos().clone(cloned));
		if (this.caprinos != null)
			cloned.setCaprinos((InformacoesCaprinos) this.getCaprinos().clone(cloned));
		if (this.equinos != null)
			cloned.setEquinos((InformacoesEquinos) this.getEquinos().clone(cloned));
		if (this.lagomorfos != null)
			cloned.setLagomorfos((InformacoesLagomorfos) this.getLagomorfos().clone(cloned));
		
		if (this.medicacoes != null){
			cloned.setMedicacoes(new ArrayList<Medicacao>());
			for (Medicacao medicacao : this.medicacoes) {
				cloned.getMedicacoes().add((Medicacao) medicacao.clone(cloned));
			}
		}
		
		if (this.medidas != null){
			cloned.setMedidas(new ArrayList<MedidasAdotadasNoEstabelecimento>());
			for (MedidasAdotadasNoEstabelecimento medida : this.medidas) {
				cloned.getMedidas().add(medida);
			}
		}
		
		if (this.especiesAfetadas != null){
			cloned.setEspeciesAfetadas(new ArrayList<Especie>());
			for (Especie especie : this.especiesAfetadas) {
				cloned.getEspeciesAfetadas().add(especie);
			}
		}
		
		if (this.muares != null)
			cloned.setMuares((InformacoesMuares) this.getMuares().clone(cloned));
		if (this.outrosAnimais != null)
			cloned.setOutrosAnimais((InformacoesOutrosAnimais) this.getOutrosAnimais().clone(cloned));
		if (this.ovinos != null)
			cloned.setOvinos((InformacoesOvinos) this.getOvinos().clone(cloned));
		if (this.propriedade != null)
			cloned.setPropriedade(this.getPropriedade());
		if (this.proprietario != null)
			cloned.setProprietario((Produtor) this.getProprietario().clone());
		if (this.representanteEstabelecimento != null)
			cloned.setRepresentanteEstabelecimento((RepresentanteEstabelecimento) this.getRepresentanteEstabelecimento().clone());
		if (this.suinos != null)
			cloned.setSuinos((InformacoesSuinos) this.getSuinos().clone(cloned));
		
		if (this.tipoExploracaoAbelhas != null){
			cloned.setTipoExploracaoAbelhas(new ArrayList<TipoExploracaoAbelhas>());
			for (TipoExploracaoAbelhas tipoExploracao : this.tipoExploracaoAbelhas) {
				cloned.getTipoExploracaoAbelhas().add(tipoExploracao);
			}
		}
		
		if (this.tipoExploracaoCoelhos != null){
			cloned.setTipoExploracaoCoelhos(new ArrayList<TipoExploracaoCoelhos>());
			for (TipoExploracaoCoelhos tipoExploracao : this.tipoExploracaoCoelhos) {
				cloned.getTipoExploracaoCoelhos().add(tipoExploracao);
			}
		}
		
		if (this.transitoDeAnimais != null){
			cloned.setTransitoDeAnimais(new ArrayList<TransitoDeAnimais>());
			for (TransitoDeAnimais transito : this.transitoDeAnimais) {
				cloned.getTransitoDeAnimais().add((TransitoDeAnimais) transito.clone(cloned));
			}
		}
		
		if (this.vacinas != null){
			cloned.setVacinas(new ArrayList<VacinacaoFormIN>());
			for (VacinacaoFormIN vacina : this.vacinas) {
				cloned.getVacinas().add((VacinacaoFormIN) vacina.clone(cloned));
			}
		}
		
		if (this.veterinario != null)
			cloned.setVeterinario((br.gov.mt.indea.sistemaformin.webservice.entity.Veterinario) this.getVeterinario().clone());
		
		return cloned;
	}

	public Produtor getProprietario() {
		return proprietario;
	}

	public void setProprietario(Produtor proprietario) {
		this.proprietario = proprietario;
	}

	public Long getCodigoPropriedade() {
		return codigoPropriedade;
	}

	public void setCodigoPropriedade(Long codigoPropriedade) {
		this.codigoPropriedade = codigoPropriedade;
	}

	public String getNomePropriedade() {
		return nomePropriedade;
	}

	public void setNomePropriedade(String nomePropriedade) {
		this.nomePropriedade = nomePropriedade;
	}

	public String getNumeroProtocoloSISBRAVET() {
		return numeroProtocoloSISBRAVET;
	}

	public void setNumeroProtocoloSISBRAVET(String numeroProtocoloSISBRAVET) {
		this.numeroProtocoloSISBRAVET = numeroProtocoloSISBRAVET;
	}

	public String getNumeroFormINSISBRAVET() {
		return numeroFormINSISBRAVET;
	}

	public void setNumeroFormINSISBRAVET(String numeroFormINSISBRAVET) {
		this.numeroFormINSISBRAVET = numeroFormINSISBRAVET;
	}

	public Calendar getDataSISBRAVET() {
		return dataSISBRAVET;
	}

	public void setDataSISBRAVET(Calendar dataSISBRAVET) {
		this.dataSISBRAVET = dataSISBRAVET;
	}
	
}
