package br.gov.mt.indea.sistemaformin.entity;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

@Audited
@Entity(name="vacinacao_eq")
public class VacinacaoEQ extends BaseEntity<Long>{

	private static final long serialVersionUID = 3572099774562833247L;

	@Id
	@SequenceGenerator(name="vacinacao_eq_seq", sequenceName="vacinacao_eq_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="vacinacao_eq_seq")
	private Long id;
	
	@ManyToOne
	@JoinColumn(name="id_avaliacao_clinica_eq")
	private AvaliacaoClinicaEQ avaliacaoClinicaEQ;
	
	@Column(name="id_amostra")
	private Long idAmostra;
	
	private String doenca;
	
	@Column(name="data_notificacao")
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar data;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public AvaliacaoClinicaEQ getAvaliacaoClinicaEQ() {
		return avaliacaoClinicaEQ;
	}

	public void setAvaliacaoClinicaEQ(AvaliacaoClinicaEQ avaliacaoClinicaEQ) {
		this.avaliacaoClinicaEQ = avaliacaoClinicaEQ;
	}

	public Long getIdAmostra() {
		return idAmostra;
	}

	public void setIdAmostra(Long idAmostra) {
		this.idAmostra = idAmostra;
	}

	public String getDoenca() {
		return doenca;
	}

	public void setDoenca(String doenca) {
		this.doenca = doenca;
	}

	public Calendar getData() {
		return data;
	}

	public void setData(Calendar data) {
		this.data = data;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VacinacaoEQ other = (VacinacaoEQ) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}