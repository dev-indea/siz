package br.gov.mt.indea.sistemaformin.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;

import br.gov.mt.indea.sistemaformin.seguranca.JsfAccessDeniedHandler;
import br.gov.mt.indea.sistemaformin.seguranca.JsfLoginUrlAuthenticationEntryPoint;
import br.gov.mt.indea.sistemaformin.seguranca.JsfRedirectStrategy;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter{

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		JsfLoginUrlAuthenticationEntryPoint jsfLoginEntry = new JsfLoginUrlAuthenticationEntryPoint();
        jsfLoginEntry.setLoginFormUrl("/login");
        jsfLoginEntry.setRedirectStrategy(new JsfRedirectStrategy());

        JsfAccessDeniedHandler jsfDeniedEntry = new JsfAccessDeniedHandler();
        jsfDeniedEntry.setLoginPath("/acessoNegado.jsf");
        jsfDeniedEntry.setContextRelative(true);
		
		http.headers().contentTypeOptions().disable();
		http.csrf().disable();

        http.exceptionHandling()
				.accessDeniedPage("/acessoNegado.jsf")
				.authenticationEntryPoint(jsfLoginEntry)
		        .accessDeniedHandler(jsfDeniedEntry)
			.and().authorizeRequests()
        		.antMatchers("/javax.faces.resource/**",
        					 "/",
        					 "/recuperarSenha", 
        					 "/redefinirSenha/**",
        					 "/resources/**",
        					 "/resources/templates/**",
        					 "/resources/img/logos/**",
        					 "/404",
        					 "/erro.jsf",
        					 "/notificacao",
        					 "/notificacao/**")
        		.permitAll()
        	.and()
        		.authorizeRequests()
        		.antMatchers("/formIN/**")
        		.hasAuthority("formIN")
    		.and()
        		.authorizeRequests()
        		.antMatchers("/termoFiscalizacao/**")
        		.hasAuthority("termoFiscalizacao")
    		.and()
        		.authorizeRequests()
        		.antMatchers("/tipoChavePrincipalTermoFiscalizacao/**")
        		.hasAuthority("tipoChavePrincipalTermoFiscalizacao")
    		.and()
        		.authorizeRequests()
        		.antMatchers("/tipoChavePrincipalVisitaPropriedadeRural/**")
        		.hasAuthority("tipoChavePrincipalVisitaPropriedadeRural")
    		.and()
        		.authorizeRequests()
        		.antMatchers("/tipoChaveSecundariaTermoFiscalizacao/**")
        		.hasAuthority("tipoChaveSecundariaTermoFiscalizacao")
    		.and()
        		.authorizeRequests()
        		.antMatchers("/tipoChaveSecundariaVisitaPropriedadeRural/**")
        		.hasAuthority("tipoChaveSecundariaVisitaPropriedadeRural")
    		.and()
        		.authorizeRequests()
        		.antMatchers("/vigilanciaVeterinaria/**")
        		.hasAuthority("vigilanciaVeterinaria")
    		.and()
        		.authorizeRequests()
        		.antMatchers("/visitaPropriedadeRural/**")
        		.hasAuthority("visitaPropriedadeRural")
    		.and()
        		.authorizeRequests()
        		.antMatchers("/usuario/**",
        					 "/grupo/**",
        					 "/permissao/**")
        		.hasAuthority("admin")
    		
    		.and()
    			.authorizeRequests()
                .anyRequest()
                .authenticated()
    		.and()
    			.formLogin()
            	.loginPage("/login")
            	.successHandler(new SIZAuthenticationSuccessHandlerImpl())
            	.failureUrl("/login?failed=true") 
            	.usernameParameter("campoUsuario:username")
            	.passwordParameter("campoPassword:password")
            	.permitAll()
        	.and()
        		.logout()
        		.logoutSuccessUrl("/login.jsf");
		
	}
	
	@Bean
	public UserDetailsService userDetailsService() {
		return new SIZUserDetailsService();
	}
	
	@Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth)
            throws Exception {
        auth.userDetailsService(userDetailsService()).passwordEncoder(new Md5PasswordEncoder());
    }
	
	@Bean
	public Md5PasswordEncoder passwordEncoder(){
		return new Md5PasswordEncoder();
	}

}
