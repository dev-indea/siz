package br.gov.mt.indea.sistemaformin.dao;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import br.gov.mt.indea.sistemaformin.entity.FolhaAdicional;
import br.gov.mt.indea.sistemaformin.exception.ApplicationException;

@Stateless
public class FolhaAdicionalDAO extends DAO<FolhaAdicional> {
	
	@Inject
	private EntityManager em;
	
	public FolhaAdicionalDAO() {
		super(FolhaAdicional.class);
	}

	@Override
	public EntityManager getEntityManager() {
		return this.em;
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void add(FolhaAdicional folhaAdicional) throws ApplicationException{
		folhaAdicional = this.getEntityManager().merge(folhaAdicional);
		if (folhaAdicional.getFormCOM() != null)
			folhaAdicional.setFormCOM(this.getEntityManager().merge(folhaAdicional.getFormCOM()));
		super.add(folhaAdicional);
	}

}
