package br.gov.mt.indea.sistemaformin.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.envers.Audited;

@Audited
@Entity
@DiscriminatorValue("muares")
public class DetalhesInformacoesMuares extends AbstractDetalhesInformacoesDeAnimais implements Cloneable{
	
	private static final long serialVersionUID = 1355044082455412390L;

	@ManyToOne
	@JoinColumn(name="id_informacoes_muares")
	private InformacoesMuares informacoesMuares;

	public InformacoesMuares getInformacoesMuares() {
		return informacoesMuares;
	}

	public void setInformacoesMuares(InformacoesMuares informacoesMuares) {
		this.informacoesMuares = informacoesMuares;
	}
	
	protected Object clone(InformacoesMuares informacoesMuares) throws CloneNotSupportedException{
		DetalhesInformacoesMuares cloned = (DetalhesInformacoesMuares) super.clone();
		
		cloned.setId(null);
		cloned.setInformacoesMuares(informacoesMuares);
		
		return cloned;
	}

}