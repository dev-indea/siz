package br.gov.mt.indea.sistemaformin.service;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.mt.indea.sistemaformin.entity.Municipio;
import br.gov.mt.indea.sistemaformin.entity.NumeroVisitaPropriedadeRural;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class NumeroVisitaPropriedadeRuralService extends PaginableService<NumeroVisitaPropriedadeRural, Long> {

	private static final Logger log = LoggerFactory.getLogger(NumeroVisitaPropriedadeRuralService.class);
	
	protected NumeroVisitaPropriedadeRuralService() {
		super(NumeroVisitaPropriedadeRural.class);
	}
	
	public NumeroVisitaPropriedadeRural findByIdFetchAll(Long id){
		NumeroVisitaPropriedadeRural numeroVisitaPropriedadeRural;
		
		StringBuilder sql = new StringBuilder();
		sql.append("select numeroVisitaPropriedadeRural ")
		   .append("  from NumeroVisitaPropriedadeRural numeroVisitaPropriedadeRural ")
		   .append(" where numeroVisitaPropriedadeRural.id = :id ");
		
		Query query = getSession().createQuery(sql.toString()).setLong("id", id);
		numeroVisitaPropriedadeRural = (NumeroVisitaPropriedadeRural) query.uniqueResult();
		
		return numeroVisitaPropriedadeRural;
	}
	
	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	public NumeroVisitaPropriedadeRural getNumeroVisitaPropriedadeRuralByMunicipio(Municipio municipio, int ano){
		NumeroVisitaPropriedadeRural numeroVisitaPropriedadeRural;
		
		StringBuilder sql = new StringBuilder();
		sql.append("  from NumeroVisitaPropriedadeRural entity ")
		   .append("  join fetch entity.municipio ")
		   .append(" where entity.municipio = :municipio ")
		   .append("   and entity.ano = :ano");
		
		Query query = getSession().createQuery(sql.toString()).setParameter("municipio", municipio).setInteger("ano", ano);
		numeroVisitaPropriedadeRural = (NumeroVisitaPropriedadeRural) query.uniqueResult();

		return numeroVisitaPropriedadeRural;
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	public void saveOrUpdate(NumeroVisitaPropriedadeRural numeroVisitaPropriedadeRural) {
		super.saveOrUpdate(numeroVisitaPropriedadeRural);
		
		log.info("Salvando Numero Visita a Propriedade Rural {}", numeroVisitaPropriedadeRural.getId());
	}
	
	@Override
	public void validar(NumeroVisitaPropriedadeRural NumeroVisitaPropriedadeRural) {

	}

	@Override
	public void validarPersist(NumeroVisitaPropriedadeRural NumeroVisitaPropriedadeRural) {

	}

	@Override
	public void validarMerge(NumeroVisitaPropriedadeRural NumeroVisitaPropriedadeRural) {

	}

	@Override
	public void validarDelete(NumeroVisitaPropriedadeRural NumeroVisitaPropriedadeRural) {

	}

}
