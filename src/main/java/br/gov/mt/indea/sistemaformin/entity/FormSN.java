package br.gov.mt.indea.sistemaformin.entity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

import br.gov.mt.indea.sistemaformin.enums.Dominio.CategoriaDoAnimalSubmetidoAVigilancia;
import br.gov.mt.indea.sistemaformin.enums.Dominio.Especie;
import br.gov.mt.indea.sistemaformin.enums.Dominio.FinalidadeExploracaoBovinosEBubalinos;
import br.gov.mt.indea.sistemaformin.enums.Dominio.FonteDaNotificacao;
import br.gov.mt.indea.sistemaformin.enums.Dominio.LocalDeColhimentoDeAmostra;
import br.gov.mt.indea.sistemaformin.enums.Dominio.MeioConservacaoAmostra;
import br.gov.mt.indea.sistemaformin.enums.Dominio.MetodoParaEstipularIdade;
import br.gov.mt.indea.sistemaformin.enums.Dominio.Sexo;
import br.gov.mt.indea.sistemaformin.enums.Dominio.SimNao;
import br.gov.mt.indea.sistemaformin.enums.Dominio.SimNaoSI;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoAlteracaoSindromeNeurologica;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoAmostra;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoDisturbioSindromeNeurologica;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoPeriodo;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoResponsavelColheitaDeAmostra;
import br.gov.mt.indea.sistemaformin.util.DataUtil;
import br.gov.mt.indea.sistemaformin.webservice.entity.Propriedade;

@Audited
@Entity
@Table(name="form_sn")
public class FormSN extends BaseEntity<Long>{

	private static final long serialVersionUID = -1914831278498095484L;

	@Id
	@SequenceGenerator(name="form_sn_seq", sequenceName="form_sn_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="form_sn_seq")
	private Long id;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_usuario")
	private Usuario usuario;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_cadastro")
	private Calendar dataCadastro;
	
	@ManyToOne
	@JoinColumn(name="id_formin")
	private FormIN formIN;
	
	@ManyToOne
	@JoinColumn(name="id_formcom")
	private FormCOM formCOM;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_propriedade")
	private Propriedade propriedade;
	
	@Enumerated(EnumType.STRING)
	private Especie especie;
	
	@Column(name="pais_de_origem_do_animal", length=255)
	private String paisDeOrigemDoAnimal;
	
	@Column(name="especie_animal_silvestre", length=255)
	private String especieDoAnimalSilvestre;
	
	@Enumerated(EnumType.STRING)
	@Column(name="local_colhimento_de_amostra")
	private LocalDeColhimentoDeAmostra localDeColhimentoDeAmostra;
	
	@Column(name="outro_local_colhimento_amostra", length=255)
	private String outroLocalDeColhimentoDeAmostra;
	
	@Column(name="identificacaoAnimal", length=255)
	private String identificacaoDoAnimal;
	
	private Double idade;
	
	@Enumerated(EnumType.STRING)
	private TipoPeriodo tipoPeriodo;
	
	@Column(length=255)
	private String raca;
	
	@Enumerated(EnumType.STRING)
	private FinalidadeExploracaoBovinosEBubalinos finalidadeExploracaoBovinosEBubalinos;
	
	private Sexo sexo;
	
	@ElementCollection(targetClass = MetodoParaEstipularIdade.class)
	@JoinTable(name = "formsn_metodos", joinColumns = @JoinColumn(name = "id_formsn", nullable = false))
	@Column(name = "id_formsn_metodos")
	@Enumerated(EnumType.STRING)
	private List<MetodoParaEstipularIdade> metodosParaEstipularIdade = new ArrayList<MetodoParaEstipularIdade>();
	
	@Column(name="quantidade_animais_no_rebanho")
	private Long quantidadeAnimaisNoRebanho;
	
	@Column(name="quantidade_animais_doentes")
	private Long quantidadeAnimaisDoentes;
	
	@Column(name="quantidade_animais_mortos")
	private Long quantidadeAnimaisMortos;
	
	@Enumerated(EnumType.STRING)
	@Column(name="havia_outras_especies_afetadas")
	private SimNao haviaOutrasEspeciesAfetadas;
	
	@Column(name="outras_especies_afetadas", length=255)
	private String outrasEspeciesAfetadas;
	
	@Enumerated(EnumType.STRING)
	@Column(name="vacinado_raiva")
	private SimNao vacinadoRaiva;
	
	@Column(name="data_vacina_raiva")
	private Calendar dataVacinaRaiva;
	
	@Enumerated(EnumType.STRING)
	@Column(name="vacinado_clostridiose")
	private SimNao vacinadoClostridiose;
	
	@Column(name="data_vacina_clostridiose")
	private Calendar dataVacinaClostridiose;
	
	@Enumerated(EnumType.STRING)
	@Column(name="vacinado_cinomose")
	private SimNao vacinadoCinomose;
	
	@Column(name="data_vacina_cinomose")
	private Calendar dataVacinaCinomose;
	
	@Enumerated(EnumType.STRING)
	@Column(name="vacinado_leptospirose")
	private SimNao vacinadoLeptospirose;
	
	@Column(name="data_vacina_leptospirose")
	private Calendar dataVacinaLeptospirose;
	
	@Enumerated(EnumType.STRING)
	@Column(name="vacinado_botulismo")
	private SimNao vacinadoBotulismo;
	
	@Column(name="data_vacina_botulismo")
	private Calendar dataVacinaBotulismo;
	
	@Enumerated(EnumType.STRING)
	@Column(name="vacinado_encefalomielite")
	private SimNao vacinadoEncefalomielite;
	
	@Column(name="data_vacina_encefalomielite")
	private Calendar dataVacinaEncefalomielite;
	
	@Column(name="outra_vacina")
	private String outraVacina;
	
	@Column(name="data_outra_vacina")
	private Calendar dataOutraVacina;
	
	@OneToMany(mappedBy="formSN", cascade={CascadeType.ALL}, orphanRemoval=true, fetch=FetchType.LAZY)
	private List<VacinacaoFormSN> outrasVacinas = new ArrayList<VacinacaoFormSN>();
	
	@Enumerated(EnumType.STRING)
	@Column(name="fonte_da_notificacao")
	private FonteDaNotificacao fonteDaNotificacao;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_notificacao")
	private Calendar dataNotificacao;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_primeira_visita")
	private Calendar dataPrimeiraVisita;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_inicio_doenca")
	private Calendar dataProvavelInicioDaDoenca;
	
	@Enumerated(EnumType.STRING)
	@Column(name="categoria_do_animal")
	private CategoriaDoAnimalSubmetidoAVigilancia categoriaDoAnimalSubmetidoAVigilancia;
	
	@Column(name="qtdade_dias_disturbio_neuro")
	private Long quantidadeDiasDisturbioNeurologico;
	
	@Column(name="qtdade_dias_doenca_cronica")
	private Long quantidadeDiasDoencaCronica;
	
	@OneToMany(mappedBy="formSN", cascade={CascadeType.ALL}, orphanRemoval=true, fetch=FetchType.LAZY)
	private List<AlteracaoSindromeNeurologica> listaAlteracoesSindromeNeurologica = new ArrayList<AlteracaoSindromeNeurologica>();
	
	@Enumerated(EnumType.STRING)
	private SimNao eutanasiado;
	
	@Enumerated(EnumType.STRING)
	@Column(name="ha_animais_que_se_recuperaram")
	private SimNaoSI haAnimaisQueSerecuperaramDosSinaisClinicos;
	
	@Column(name="percent_animais_recuperados")
	private Long percentualDeAnimaisRecuperados;
	
	@Enumerated(EnumType.STRING)
	@Column(name="contato_de_pessoas_com_animais")
	private SimNaoSI contatoDiretoDePessoasComAnimaisSuspeitos;
	
	@ElementCollection(targetClass = TipoAmostra.class)
	@JoinTable(name = "formsn_tipo_amostra", joinColumns = @JoinColumn(name = "id_formsn", nullable = false))
	@Column(name = "id_formsn_tipo_amostra")
	@Enumerated(EnumType.STRING)
	private List<TipoAmostra> tipoAmostra = new ArrayList<TipoAmostra>();
	
	@Column(name="outro_tipo_amostra", length=255)
	private String outroTipoDeAmostra;
	
	@Column(name="data_da_morte")
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar dataDaMorte;
	
	@Column(name="data_da_colheita")
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar dataDaColheita;
	
	@Column(name="tempo_entre_colheita_e_conserv", length=255)
	private String tempoEntreColheitaEConservacao;
	
	@ElementCollection(targetClass = MeioConservacaoAmostra.class)
	@JoinTable(name = "formsn_meio_conservacao", joinColumns = @JoinColumn(name = "id_formsn", nullable = false))
	@Column(name = "id_formsn_meio_conservacao")
	@Enumerated(EnumType.STRING)
	private List<MeioConservacaoAmostra> meioConservacaoAmostra = new ArrayList<MeioConservacaoAmostra>();
	
	@Column(length=570)
	private String observacoes;
	
	@Enumerated(EnumType.STRING)
	private TipoResponsavelColheitaDeAmostra tipoResponsavelColheitaDeAmostra;
	
	@OneToOne(cascade={CascadeType.ALL}, orphanRemoval=true)
	@JoinColumn(name="id_veterinario_colheita")
	private br.gov.mt.indea.sistemaformin.webservice.entity.Veterinario veterinarioColheita;
	
	public Long getId() {
		return id;
	}
	
	public String getOutrasVacinasAsString(){
		StringBuilder sb = new StringBuilder();
		if (this.outrasVacinas != null && !this.outrasVacinas.isEmpty()){
			for (VacinacaoFormSN vacina : outrasVacinas) {
				sb.append(vacina.getDoenca()).append(", ").append(DataUtil.getCalendar_dd_MM_yyyy(vacina.getData())).append(". ");
			}
		}
			
		return sb.toString();
	}
	
	public List<TipoAlteracaoSindromeNeurologica> getListaTipoAlteracaoSindromeNeurologica(){
		List<TipoAlteracaoSindromeNeurologica> lista = new ArrayList<TipoAlteracaoSindromeNeurologica>();
		
		if (this.listaAlteracoesSindromeNeurologica != null && !this.listaAlteracoesSindromeNeurologica.isEmpty()){
			for (AlteracaoSindromeNeurologica alteracaoSindromeNeurologica : listaAlteracoesSindromeNeurologica) {
				lista.add(alteracaoSindromeNeurologica.getTipoAlteracaoSindromeNeurologica());
			}
		}
		
		return lista;
	}
	
	public List<TipoDisturbioSindromeNeurologica> getListaTipoDisturbioSindromeNeurologica_NeurologicaOuDeSensibilidade(){
		if (this.listaAlteracoesSindromeNeurologica != null && !this.listaAlteracoesSindromeNeurologica.isEmpty()){
			for (AlteracaoSindromeNeurologica alteracao : listaAlteracoesSindromeNeurologica) {
				if (alteracao.getTipoAlteracaoSindromeNeurologica().equals(TipoAlteracaoSindromeNeurologica.NEUROLOGICA_OU_DE_SENSIBILIDADE))
					return alteracao.getListaTipoDisturbioSindromeNeurologica();
			}
		}
		
		return null;
	}
	
	public List<TipoDisturbioSindromeNeurologica> getListaTipoDisturbioSindromeNeurologica_PosturaOuLocomocao(){
		if (this.listaAlteracoesSindromeNeurologica != null && !this.listaAlteracoesSindromeNeurologica.isEmpty()){
			for (AlteracaoSindromeNeurologica alteracao : listaAlteracoesSindromeNeurologica) {
				if (alteracao.getTipoAlteracaoSindromeNeurologica().equals(TipoAlteracaoSindromeNeurologica.POSTURA_OU_LOCOMOCAO))
					return alteracao.getListaTipoDisturbioSindromeNeurologica();
			}
		}
		
		return null;
	}
	
	public List<TipoDisturbioSindromeNeurologica> getListaTipoDisturbioSindromeNeurologica_Comportamental(){
		if (this.listaAlteracoesSindromeNeurologica != null && !this.listaAlteracoesSindromeNeurologica.isEmpty()){
			for (AlteracaoSindromeNeurologica alteracao : listaAlteracoesSindromeNeurologica) {
				if (alteracao.getTipoAlteracaoSindromeNeurologica().equals(TipoAlteracaoSindromeNeurologica.COMPORTAMENTAL))
					return alteracao.getListaTipoDisturbioSindromeNeurologica();
			}
		}
		
		return null;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Calendar getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Calendar dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public FormIN getFormIN() {
		return formIN;
	}

	public void setFormIN(FormIN formIN) {
		this.formIN = formIN;
	}

	public FormCOM getFormCOM() {
		return formCOM;
	}

	public void setFormCOM(FormCOM formCOM) {
		this.formCOM = formCOM;
	}

	public Especie getEspecie() {
		return especie;
	}

	public void setEspecie(Especie especie) {
		this.especie = especie;
	}

	public String getPaisDeOrigemDoAnimal() {
		return paisDeOrigemDoAnimal;
	}

	public void setPaisDeOrigemDoAnimal(String paisDeOrigemDoAnimal) {
		this.paisDeOrigemDoAnimal = paisDeOrigemDoAnimal;
	}

	public String getEspecieDoAnimalSilvestre() {
		return especieDoAnimalSilvestre;
	}

	public void setEspecieDoAnimalSilvestre(String especieDoAnimalSilvestre) {
		this.especieDoAnimalSilvestre = especieDoAnimalSilvestre;
	}

	public LocalDeColhimentoDeAmostra getLocalDeColhimentoDeAmostra() {
		return localDeColhimentoDeAmostra;
	}

	public void setLocalDeColhimentoDeAmostra(
			LocalDeColhimentoDeAmostra localDeColhimentoDeAmostra) {
		this.localDeColhimentoDeAmostra = localDeColhimentoDeAmostra;
	}

	public String getOutroLocalDeColhimentoDeAmostra() {
		return outroLocalDeColhimentoDeAmostra;
	}

	public void setOutroLocalDeColhimentoDeAmostra(
			String outroLocalDeColhimentoDeAmostra) {
		this.outroLocalDeColhimentoDeAmostra = outroLocalDeColhimentoDeAmostra;
	}

	public String getIdentificacaoDoAnimal() {
		return identificacaoDoAnimal;
	}

	public void setIdentificacaoDoAnimal(String identificacaoDoAnimal) {
		this.identificacaoDoAnimal = identificacaoDoAnimal;
	}

	public Double getIdade() {
		return idade;
	}

	public void setIdade(Double idade) {
		this.idade = idade;
	}

	public TipoPeriodo getTipoPeriodo() {
		return tipoPeriodo;
	}

	public void setTipoPeriodo(TipoPeriodo tipoPeriodo) {
		this.tipoPeriodo = tipoPeriodo;
	}

	public String getRaca() {
		return raca;
	}

	public void setRaca(String raca) {
		this.raca = raca;
	}

	public Sexo getSexo() {
		return sexo;
	}

	public void setSexo(Sexo sexo) {
		this.sexo = sexo;
	}

	public List<MetodoParaEstipularIdade> getMetodosParaEstipularIdade() {
		return metodosParaEstipularIdade;
	}

	public void setMetodosParaEstipularIdade(
			List<MetodoParaEstipularIdade> metodosParaEstipularIdade) {
		this.metodosParaEstipularIdade = metodosParaEstipularIdade;
	}

	public Long getQuantidadeAnimaisNoRebanho() {
		return quantidadeAnimaisNoRebanho;
	}

	public void setQuantidadeAnimaisNoRebanho(Long quantidadeAnimaisNoRebanho) {
		this.quantidadeAnimaisNoRebanho = quantidadeAnimaisNoRebanho;
	}

	public Long getQuantidadeAnimaisDoentes() {
		return quantidadeAnimaisDoentes;
	}

	public void setQuantidadeAnimaisDoentes(Long quantidadeAnimaisDoentes) {
		this.quantidadeAnimaisDoentes = quantidadeAnimaisDoentes;
	}

	public Long getQuantidadeAnimaisMortos() {
		return quantidadeAnimaisMortos;
	}

	public void setQuantidadeAnimaisMortos(Long quantidadeAnimaisMortos) {
		this.quantidadeAnimaisMortos = quantidadeAnimaisMortos;
	}

	public SimNao getHaviaOutrasEspeciesAfetadas() {
		return haviaOutrasEspeciesAfetadas;
	}

	public void setHaviaOutrasEspeciesAfetadas(SimNao haviaOutrasEspeciesAfetadas) {
		this.haviaOutrasEspeciesAfetadas = haviaOutrasEspeciesAfetadas;
	}

	public String getOutrasEspeciesAfetadas() {
		return outrasEspeciesAfetadas;
	}

	public void setOutrasEspeciesAfetadas(String outrasEspeciesAfetadas) {
		this.outrasEspeciesAfetadas = outrasEspeciesAfetadas;
	}

	public FonteDaNotificacao getFonteDaNotificacao() {
		return fonteDaNotificacao;
	}

	public void setFonteDaNotificacao(FonteDaNotificacao fonteDaNotificacao) {
		this.fonteDaNotificacao = fonteDaNotificacao;
	}

	public Calendar getDataNotificacao() {
		return dataNotificacao;
	}

	public void setDataNotificacao(Calendar dataNotificacao) {
		this.dataNotificacao = dataNotificacao;
	}

	public Calendar getDataPrimeiraVisita() {
		return dataPrimeiraVisita;
	}

	public void setDataPrimeiraVisita(Calendar dataPrimeiraVisita) {
		this.dataPrimeiraVisita = dataPrimeiraVisita;
	}

	public Calendar getDataProvavelInicioDaDoenca() {
		return dataProvavelInicioDaDoenca;
	}

	public void setDataProvavelInicioDaDoenca(Calendar dataProvavelInicioDaDoenca) {
		this.dataProvavelInicioDaDoenca = dataProvavelInicioDaDoenca;
	}

	public CategoriaDoAnimalSubmetidoAVigilancia getCategoriaDoAnimalSubmetidoAVigilancia() {
		return categoriaDoAnimalSubmetidoAVigilancia;
	}

	public void setCategoriaDoAnimalSubmetidoAVigilancia(
			CategoriaDoAnimalSubmetidoAVigilancia categoriaDoAnimalSubmetidoAVigilancia) {
		this.categoriaDoAnimalSubmetidoAVigilancia = categoriaDoAnimalSubmetidoAVigilancia;
	}

	public SimNao getEutanasiado() {
		return eutanasiado;
	}

	public void setEutanasiado(SimNao eutanasiado) {
		this.eutanasiado = eutanasiado;
	}

	public SimNaoSI getHaAnimaisQueSerecuperaramDosSinaisClinicos() {
		return haAnimaisQueSerecuperaramDosSinaisClinicos;
	}

	public void setHaAnimaisQueSerecuperaramDosSinaisClinicos(
			SimNaoSI haAnimaisQueSerecuperaramDosSinaisClinicos) {
		this.haAnimaisQueSerecuperaramDosSinaisClinicos = haAnimaisQueSerecuperaramDosSinaisClinicos;
	}

	public Long getPercentualDeAnimaisRecuperados() {
		return percentualDeAnimaisRecuperados;
	}

	public void setPercentualDeAnimaisRecuperados(
			Long percentualDeAnimaisRecuperados) {
		this.percentualDeAnimaisRecuperados = percentualDeAnimaisRecuperados;
	}

	public SimNaoSI getContatoDiretoDePessoasComAnimaisSuspeitos() {
		return contatoDiretoDePessoasComAnimaisSuspeitos;
	}

	public void setContatoDiretoDePessoasComAnimaisSuspeitos(
			SimNaoSI contatoDiretoDePessoasComAnimaisSuspeitos) {
		this.contatoDiretoDePessoasComAnimaisSuspeitos = contatoDiretoDePessoasComAnimaisSuspeitos;
	}

	public List<TipoAmostra> getTipoAmostra() {
		return tipoAmostra;
	}

	public void setTipoAmostra(List<TipoAmostra> tipoAmostra) {
		this.tipoAmostra = tipoAmostra;
	}

	public String getOutroTipoDeAmostra() {
		return outroTipoDeAmostra;
	}

	public void setOutroTipoDeAmostra(String outroTipoDeAmostra) {
		this.outroTipoDeAmostra = outroTipoDeAmostra;
	}

	public Calendar getDataDaMorte() {
		return dataDaMorte;
	}

	public void setDataDaMorte(Calendar dataDaMorte) {
		this.dataDaMorte = dataDaMorte;
	}

	public Calendar getDataDaColheita() {
		return dataDaColheita;
	}

	public void setDataDaColheita(Calendar dataDaColheita) {
		this.dataDaColheita = dataDaColheita;
	}

	public String getTempoEntreColheitaEConservacao() {
		return tempoEntreColheitaEConservacao;
	}

	public void setTempoEntreColheitaEConservacao(
			String tempoEntreColheitaEConservacao) {
		this.tempoEntreColheitaEConservacao = tempoEntreColheitaEConservacao;
	}

	public List<MeioConservacaoAmostra> getMeioConservacaoAmostra() {
		return meioConservacaoAmostra;
	}

	public void setMeioConservacaoAmostra(
			List<MeioConservacaoAmostra> meioConservacaoAmostra) {
		this.meioConservacaoAmostra = meioConservacaoAmostra;
	}

	public SimNao getVacinadoRaiva() {
		return vacinadoRaiva;
	}

	public void setVacinadoRaiva(SimNao vacinadoRaiva) {
		this.vacinadoRaiva = vacinadoRaiva;
	}

	public Calendar getDataVacinaRaiva() {
		return dataVacinaRaiva;
	}

	public void setDataVacinaRaiva(Calendar dataVacinaRaiva) {
		this.dataVacinaRaiva = dataVacinaRaiva;
	}

	public SimNao getVacinadoClostridiose() {
		return vacinadoClostridiose;
	}

	public void setVacinadoClostridiose(SimNao vacinadoClostridiose) {
		this.vacinadoClostridiose = vacinadoClostridiose;
	}

	public Calendar getDataVacinaClostridiose() {
		return dataVacinaClostridiose;
	}

	public void setDataVacinaClostridiose(Calendar dataVacinaClostridiose) {
		this.dataVacinaClostridiose = dataVacinaClostridiose;
	}

	public SimNao getVacinadoCinomose() {
		return vacinadoCinomose;
	}

	public void setVacinadoCinomose(SimNao vacinadoCinomose) {
		this.vacinadoCinomose = vacinadoCinomose;
	}

	public Calendar getDataVacinaCinomose() {
		return dataVacinaCinomose;
	}

	public void setDataVacinaCinomose(Calendar dataVacinaCinomose) {
		this.dataVacinaCinomose = dataVacinaCinomose;
	}

	public SimNao getVacinadoLeptospirose() {
		return vacinadoLeptospirose;
	}

	public void setVacinadoLeptospirose(SimNao vacinadoLeptospirose) {
		this.vacinadoLeptospirose = vacinadoLeptospirose;
	}

	public Calendar getDataVacinaLeptospirose() {
		return dataVacinaLeptospirose;
	}

	public void setDataVacinaLeptospirose(Calendar dataVacinaLeptospirose) {
		this.dataVacinaLeptospirose = dataVacinaLeptospirose;
	}

	public SimNao getVacinadoBotulismo() {
		return vacinadoBotulismo;
	}

	public void setVacinadoBotulismo(SimNao vacinadoBotulismo) {
		this.vacinadoBotulismo = vacinadoBotulismo;
	}

	public Calendar getDataVacinaBotulismo() {
		return dataVacinaBotulismo;
	}

	public void setDataVacinaBotulismo(Calendar dataVacinaBotulismo) {
		this.dataVacinaBotulismo = dataVacinaBotulismo;
	}

	public SimNao getVacinadoEncefalomielite() {
		return vacinadoEncefalomielite;
	}

	public void setVacinadoEncefalomielite(SimNao vacinadoEncefalomielite) {
		this.vacinadoEncefalomielite = vacinadoEncefalomielite;
	}

	public Calendar getDataVacinaEncefalomielite() {
		return dataVacinaEncefalomielite;
	}

	public void setDataVacinaEncefalomielite(Calendar dataVacinaEncefalomielite) {
		this.dataVacinaEncefalomielite = dataVacinaEncefalomielite;
	}

	public String getOutraVacina() {
		return outraVacina;
	}

	public void setOutraVacina(String outraVacina) {
		this.outraVacina = outraVacina;
	}

	public Calendar getDataOutraVacina() {
		return dataOutraVacina;
	}

	public void setDataOutraVacina(Calendar dataOutraVacina) {
		this.dataOutraVacina = dataOutraVacina;
	}

	public String getObservacoes() {
		return observacoes;
	}

	public void setObservacoes(String observacoes) {
		this.observacoes = observacoes;
	}

	public br.gov.mt.indea.sistemaformin.webservice.entity.Veterinario getVeterinarioColheita() {
		return veterinarioColheita;
	}

	public void setVeterinarioColheita(br.gov.mt.indea.sistemaformin.webservice.entity.Veterinario veterinarioColheita) {
		this.veterinarioColheita = veterinarioColheita;
	}

	public List<VacinacaoFormSN> getOutrasVacinas() {
		return outrasVacinas;
	}

	public void setOutrasVacinas(List<VacinacaoFormSN> outrasVacinas) {
		this.outrasVacinas = outrasVacinas;
	}

	public Propriedade getPropriedade() {
		return propriedade;
	}

	public void setPropriedade(Propriedade propriedade) {
		this.propriedade = propriedade;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public TipoResponsavelColheitaDeAmostra getTipoResponsavelColheitaDeAmostra() {
		return tipoResponsavelColheitaDeAmostra;
	}

	public void setTipoResponsavelColheitaDeAmostra(
			TipoResponsavelColheitaDeAmostra tipoResponsavelColheitaDeAmostra) {
		this.tipoResponsavelColheitaDeAmostra = tipoResponsavelColheitaDeAmostra;
	}

	public Long getQuantidadeDiasDisturbioNeurologico() {
		return quantidadeDiasDisturbioNeurologico;
	}

	public void setQuantidadeDiasDisturbioNeurologico(
			Long quantidadeDiasDisturbioNeurologico) {
		this.quantidadeDiasDisturbioNeurologico = quantidadeDiasDisturbioNeurologico;
	}

	public Long getQuantidadeDiasDoencaCronica() {
		return quantidadeDiasDoencaCronica;
	}

	public void setQuantidadeDiasDoencaCronica(Long quantidadeDiasDoencaCronica) {
		this.quantidadeDiasDoencaCronica = quantidadeDiasDoencaCronica;
	}

	public List<AlteracaoSindromeNeurologica> getListaAlteracoesSindromeNeurologica() {
		return listaAlteracoesSindromeNeurologica;
	}

	public void setListaAlteracoesSindromeNeurologica(
			List<AlteracaoSindromeNeurologica> listaAlteracoesSindromeNeurologica) {
		this.listaAlteracoesSindromeNeurologica = listaAlteracoesSindromeNeurologica;
	}

	public FinalidadeExploracaoBovinosEBubalinos getFinalidadeExploracaoBovinosEBubalinos() {
		return finalidadeExploracaoBovinosEBubalinos;
	}

	public void setFinalidadeExploracaoBovinosEBubalinos(
			FinalidadeExploracaoBovinosEBubalinos finalidadeExploracaoBovinosEBubalinos) {
		this.finalidadeExploracaoBovinosEBubalinos = finalidadeExploracaoBovinosEBubalinos;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FormSN other = (FormSN) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}