package br.gov.mt.indea.sistemaformin.managedbeans;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.constraints.Past;

import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLBeanName;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;

import br.gov.mt.indea.sistemaformin.entity.FormCOM;
import br.gov.mt.indea.sistemaformin.entity.FormIN;
import br.gov.mt.indea.sistemaformin.entity.FormSRN;
import br.gov.mt.indea.sistemaformin.entity.Municipio;
import br.gov.mt.indea.sistemaformin.enums.Dominio.ResultadoNecropsiaDeAves;
import br.gov.mt.indea.sistemaformin.enums.Dominio.SimNao;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoAlimentoAves;
import br.gov.mt.indea.sistemaformin.exception.ApplicationErrorException;
import br.gov.mt.indea.sistemaformin.exception.ApplicationException;
import br.gov.mt.indea.sistemaformin.service.FormCOMService;
import br.gov.mt.indea.sistemaformin.service.FormINService;
import br.gov.mt.indea.sistemaformin.service.FormSRNService;
import br.gov.mt.indea.sistemaformin.service.MunicipioService;
import br.gov.mt.indea.sistemaformin.util.FacesMessageUtil;
import br.gov.mt.indea.sistemaformin.webservice.client.ClientWebServiceVeterinario;
import br.gov.mt.indea.sistemaformin.webservice.entity.Veterinario;

@Named
@ViewScoped
@URLBeanName("formSRNManagedBean")
@URLMappings(mappings = {
		@URLMapping(id = "pesquisarFormSRN", pattern = "/formIN/#{formSRNManagedBean.idFormIN}/formSRN/pesquisar", viewId = "/pages/formSRN/lista.jsf"),
		@URLMapping(id = "incluirFormSRN", pattern = "/formIN/#{formSRNManagedBean.idFormIN}/formSRN/incluir", viewId = "/pages/formSRN/novo.jsf"),
		@URLMapping(id = "alterarFormSRN", pattern = "/formIN/#{formSRNManagedBean.idFormIN}/formSRN/alterar/#{formSRNManagedBean.id}", viewId = "/pages/formSRN/novo.jsf"),
		@URLMapping(id = "impressaoFormSRN", pattern = "/formIN/#{formSRNManagedBean.idFormIN}/formSRN/impressao/#{formSRNManagedBean.id}", viewId = "/pages/impressao.jsf")})
public class FormSRNManagedBean implements Serializable {

	private static final long serialVersionUID = 1760023349204758559L;

	private Long id;
	
	private Long idFormIN;
	
	private FormIN formIN;
	
	@Inject
	private FormINService formINService;

	@Inject
	private MunicipioService municipioService;
	
	@Inject
	private FormSRNService formSRNService;
	
	@Inject
	private FormCOMService formCOMService;
	
	@Inject
	private FormSRN formSRN;
	
	@Past(message="Data precisa ser menor ou igual a data atual")
	private Date dataAtendimento;
	
	private String cpfVeterinario;

	private String crmvVeterinario;

	private List<Veterinario> listaVeterinarios;
	
	@Inject
	private ClientWebServiceVeterinario clientWebServiceVeterinario;
	
	public List<Municipio> getListaMunicipios_GranjaOuLocalDeOrigemDosAnimais(){
		if (this.formSRN.getUfGranjaOuLocalDeOrigemDosAnimais() == null)
			return null;
		
		return municipioService.findAllByUF(this.formSRN.getUfGranjaOuLocalDeOrigemDosAnimais());
	}
	
	public List<Municipio> getListaMunicipios_IncubatorioDeOrigem(){
		if (this.formSRN.getUfIncubatorioDeOrigem() == null)
			return null;
		
		return municipioService.findAllByUF(this.formSRN.getUfIncubatorioDeOrigem());
	}
	
	@PostConstruct
	private void init(){
		
	}
	
	private void carregarFormIN(){
		this.formIN = formINService.findByIdFetchPropriedade(this.idFormIN);
	}
	
	private void limpar() {
		this.formSRN = new FormSRN();
		this.dataAtendimento = null;
	}
	
	@URLAction(mappingId = "incluirFormSRN", onPostback = false)
	public void novo(){
		this.limpar();
		this.carregarFormIN();
		
		this.formSRN.setFormIN(formIN);
	}
	
//	public String novo(FormCOM formCOM){
//		this.iniciarConversacao();
//		this.formSRN.setFormCOM(formCOM);
//		this.formSRN.setFormIN(formCOM.getFormIN());
//
//		return "/pages/formSRN/novo.xhtml";
//	}
	
	@URLAction(mappingId = "alterarFormSRN", onPostback = false)
	public void editar(){
		this.formSRN = formSRNService.findByIdFetchAll(this.getId());
		
		if (this.formSRN.getDataAtendimento() != null)
			this.dataAtendimento = this.formSRN.getDataAtendimento().getTime();
	}
	
	@Inject
	private VisualizadorImpressaoManagedBean visualizadorImpressaoManagedBean;
	
	@URLAction(mappingId = "impressaoFormSRN", onPostback = false)
	public void imprimir() throws ApplicationErrorException{
		this.formSRN = formSRNService.findByIdFetchAll(this.getId());
		
		visualizadorImpressaoManagedBean.exibirFormSRN(formSRN);
	}
	
	public void remover(FormSRN formSRN){
		this.formSRNService.delete(formSRN);
		FacesMessageUtil.addInfoContextFacesMessage("Form SRN exclu�do com sucesso", "");
	}
	
	public String adicionar() throws IOException{
		
		if (this.dataAtendimento != null){
			if (this.formSRN.getDataAtendimento() == null)
				this.formSRN.setDataAtendimento(Calendar.getInstance());
			this.formSRN.getDataAtendimento().setTime(dataAtendimento);
		}else
			this.formSRN.setDataAtendimento(null);

		if (this.formSRN.getVeterinario() == null){
			FacesContext context = FacesContext.getCurrentInstance();
			
			context.addMessage("cadastro:fieldVeterinario:campoVeterinario", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Veterin�rio: valor � obrigat�rio.", "Veterin�rio: valor � obrigat�rio."));
			return "";
		}
		
		if (formSRN.getId() != null){
			this.formSRNService.saveOrUpdate(formSRN);
			FacesMessageUtil.addInfoContextFacesMessage("Form SRN atualizado", "");
		}else{
			this.formSRN.setDataCadastro(Calendar.getInstance());
			this.formSRNService.saveOrUpdate(formSRN);
			FacesMessageUtil.addInfoContextFacesMessage("Form SRN adicionado", "");
		}
		
		limpar();
	    return "pretty:visaoGeralFormIN";
	}
	
	public List<FormCOM> getListaFormCOM(){
		return formCOMService.findAllBy(this.formSRN.getFormIN());
	}
	
	public boolean isOutroTipoDeAlimento(){
		boolean resultado = false;
		if (this.formSRN.getTipoAlimento() == null)
			return resultado;
		else
			resultado = this.formSRN.getTipoAlimento().contains(TipoAlimentoAves.OUTRO);
		
		if (!resultado)
			this.formSRN.setOutroAlimento(null);
		
		return resultado;
	}
	
	public boolean isAlimentoPassaPorTratamento(){
		boolean resultado = false;
		if (this.formSRN.getAlimentoPassaPorTratamento() == null)
			return resultado;
		else
			resultado = this.formSRN.getAlimentoPassaPorTratamento().equals(SimNao.SIM);
		
		if (!resultado)
			this.formSRN.setTratamentoAlimento(null);
		
		return resultado;
	}
	
	public boolean isAguaPassaPorTratamento(){
		boolean resultado = false;
		if (this.formSRN.getAguaPassaPorTratamento() == null)
			return resultado;
		else
			resultado = this.formSRN.getAguaPassaPorTratamento().equals(SimNao.SIM);
		
		if (!resultado)
			this.formSRN.setTratamentoAgua(null);
		
		return resultado;
	}
	
	public boolean isCamaDoLoteReutilizada(){
		boolean resultado = false;
		if (this.formSRN.getCamaReutilizada() == null)
			return resultado;
		else
			resultado = this.formSRN.getCamaReutilizada().equals(SimNao.SIM);
		
		return resultado;
	}
	
	public boolean isCamaPassaPorTratamento(){
		boolean resultado = false;
		if (this.formSRN.getCamaPassaPorTratamento() == null)
			return resultado;
		else
			resultado = this.formSRN.getCamaPassaPorTratamento().equals(SimNao.SIM);
		
		if (!resultado)
			this.formSRN.setTratamentoCama(null);
		
		return resultado;
	}
	
	public boolean isVizinhosPossuemAves(){
		boolean resultado = false;
		if (this.formSRN.getVizinhosPossuemAves() == null)
			return resultado;
		else
			resultado = this.formSRN.getVizinhosPossuemAves().equals(SimNao.SIM);
		
		if (!resultado)
			this.formSRN.setTiposDeAvesDosVizinhos(null);
		
		return resultado;
	}
	
	public boolean haHistoricoDeAltaMortalidadeNoEstabelecimento(){
		boolean resultado = false;
		if (this.formSRN.getHaHistoricoDeAltaMortalidadeNoEstabelecimento() == null)
			return resultado;
		else
			resultado = this.formSRN.getHaHistoricoDeAltaMortalidadeNoEstabelecimento().equals(SimNao.SIM);
		
		if (!resultado)
			this.formSRN.setQuandoHouveAltaMortalidadeNoEstabelecimento(null);
		
		return resultado;
	}
	
	public boolean haHistoricoDeAltaMortalidadeNaRegiaoDeAvesComSinaisClinicosRelacionadosASuspeita(){
		boolean resultado = false;
		if (this.formSRN.getHaHistoricoDeAltaMortalidadeNaRegiaoDeAvesComSinaisClinicosRelacionadosASuspeita() == null)
			return resultado;
		else
			resultado = this.formSRN.getHaHistoricoDeAltaMortalidadeNaRegiaoDeAvesComSinaisClinicosRelacionadosASuspeita().equals(SimNao.SIM);
		
		if (!resultado)
			this.formSRN.setQuandoHouveAltaMortalidadeNaRegiao(null);
		
		return resultado;
	}
	
	public boolean isEstabelecimentoPossuiAssistenciaVeterinariaPermanente(){
		boolean resultado = false;
		if (this.formSRN.getEstabelecimentoPossuiAssistenciaVeterinariaPermanente() == null)
			return resultado;
		else
			resultado = this.formSRN.getEstabelecimentoPossuiAssistenciaVeterinariaPermanente().equals(SimNao.SIM);
		
		if (!resultado){
			this.formSRN.setVeterinarioVisitouAsAves(null);
			this.formSRN.setDiagnosticoPresuntivo(null);
			this.formSRN.setHaLaudoDasAves(null);
			this.formSRN.setDiagnostico(null);
			this.formSRN.setFoiTomadaAlgumaAcao(null);
			this.formSRN.setAcaoTomada(null);
			this.formSRN.setHouveReducaoDaMortalidadeAposAAcao(null);
		}
		
		return resultado;
	}
	
	public boolean veterinarioVisitouAsAves(){
		boolean resultado = false;
		if (this.formSRN.getVeterinarioVisitouAsAves() == null)
			return resultado;
		else
			resultado = this.formSRN.getVeterinarioVisitouAsAves().equals(SimNao.SIM);
		
		if (!resultado)
			this.formSRN.setDiagnosticoPresuntivo(null);
		
		return resultado;
	}
	
	public boolean haLaudoDeDiagnostico(){
		boolean resultado = false;
		if (this.formSRN.getHaLaudoDasAves() == null)
			return resultado;
		else
			resultado = this.formSRN.getHaLaudoDasAves().equals(SimNao.SIM);
		
		if (!resultado)
			this.formSRN.setDiagnostico(null);
		
		return resultado;
	}
	
	public boolean foiTomadaAlgumaAcao(){
		boolean resultado = false;
		if (this.formSRN.getFoiTomadaAlgumaAcao() == null)
			return resultado;
		else
			resultado = this.formSRN.getFoiTomadaAlgumaAcao().equals(SimNao.SIM);
		
		if (!resultado)
			this.formSRN.setAcaoTomada(null);
		
		return resultado;
	}
	
	public boolean houveAltaMortalidadeRepentina(){
		boolean resultado = false;
		if (this.formSRN.getHouveAltaMortalidadeRepentina() == null)
			return resultado;
		else
			resultado = this.formSRN.getHouveAltaMortalidadeRepentina().equals(SimNao.SIM);
		
		if (!resultado)
			this.formSRN.setPercentualMortalidade(null);
		
		return resultado;
	}
	
	public boolean houveQuedaDeConsumoDeAlimentacao(){
		boolean resultado = false;
		if (this.formSRN.getHouveQuedaDeConsumoDeAlimentacao() == null)
			return resultado;
		else
			resultado = this.formSRN.getHouveQuedaDeConsumoDeAlimentacao().equals(SimNao.SIM);
		
		if (!resultado)
			this.formSRN.setPercentualQuedaDeConsumoDeAlimentacao(null);
		
		return resultado;
	}
	
	public boolean houveQuedaDePostura(){
		boolean resultado = false;
		if (this.formSRN.getHouveQuedaDePostura() == null)
			return resultado;
		else
			resultado = this.formSRN.getHouveQuedaDePostura().equals(SimNao.SIM);
		
		if (!resultado)
			this.formSRN.setPercentualQuedaDePostura(null);
		
		return resultado;
	}
	
	public boolean houveQuedaDeConsumoDeAgua(){
		boolean resultado = false;
		if (this.formSRN.getHouveQuedaDeConsumoDeAgua() == null)
			return resultado;
		else
			resultado = this.formSRN.getHouveQuedaDeConsumoDeAgua().equals(SimNao.SIM);
		
		if (!resultado)
			this.formSRN.setPercentualQuedaDeConsumoDeAgua(null);
		
		return resultado;
	}
	
	public boolean isResultadoNecropsiaDoSistemaRespiratorioIgualAExsudatoTraqueal(){
		boolean resultado = false;
		if (this.formSRN.getNecropsiaSistemaRespiratorio() == null)
			return resultado;
		else
			resultado = this.formSRN.getNecropsiaSistemaRespiratorio().contains(ResultadoNecropsiaDeAves.EXSUDATO_TRAQUEAL);
		
		if (!resultado)
			this.formSRN.setTipoExsudatoTraqueal(null);
		
		return resultado;
	}
	
	public boolean isResultadoNecropsiaDoSistemaUrinarioIgualAOrgaosComCongestao(){
		boolean resultado = false;
		if (this.formSRN.getNecropsiaSistemaUrinario() == null)
			return resultado;
		else
			resultado = this.formSRN.getNecropsiaSistemaUrinario().contains(ResultadoNecropsiaDeAves.ORGAOS_COM_CONGESTAO_SIST_URINARIO);
		
		if (!resultado)
			this.formSRN.setOrgaosComCongestaoNoSistemaUrinario(null);
		
		return resultado;
	}
	
	public boolean isResultadoNecropsiaDoSistemaCirculatorioIgualAOrgaosComCongestao(){
		boolean resultado = false;
		if (this.formSRN.getNecropsiaSistemaCirculatorio() == null)
			return resultado;
		else
			resultado = this.formSRN.getNecropsiaSistemaCirculatorio().contains(ResultadoNecropsiaDeAves.ORGAOS_COM_CONGESTAO_SIST_CIRCULATORIO);
		
		if (!resultado)
			this.formSRN.setOrgaosComCongestaoNoSistemaCirculatorio(null);
		
		return resultado;
	}
	
	public boolean isResultadoNecropsiaDoSistemaDigestivo(){
		boolean resultado = false;
		if (this.formSRN.getNecropsiaSistemaDigestivo() == null)
			return resultado;
		else
			resultado = this.formSRN.getNecropsiaSistemaDigestivo().contains(ResultadoNecropsiaDeAves.ORGAOS_COM_CONGESTAO_SIST_DIGESTIVO);
		
		if (!resultado)
			this.formSRN.setOrgaosComCongestaoNoSistemaDigestivo(null);
		
		return resultado;
	}
	
	public void abrirTelaDeBuscaDeVeterinario(){
		this.cpfVeterinario = null;
		this.crmvVeterinario = null;
		this.listaVeterinarios = null;
	}
	
	public void buscarVeterinarioPorCpf(){
		this.listaVeterinarios = null;
		
		Veterinario v;
		try {
			v = clientWebServiceVeterinario.getVeterinarioByCpf(cpfVeterinario);
			
			if (v != null){
				this.listaVeterinarios = new ArrayList<Veterinario>();
				this.listaVeterinarios.add(v);
			}
		} catch (ApplicationException e) {
			FacesMessageUtil.addErrorContextFacesMessage("Erro", e.getLocalizedMessage());
			return;
		}
		
		if (this.listaVeterinarios == null || this.listaVeterinarios.isEmpty())
			FacesMessageUtil.addWarnContextFacesMessage("Nenhum veterin�rio foi encontrado", "");
	}
	
	public void buscarVeterinarioPorCrmv(){
		this.listaVeterinarios = null;
		
		Veterinario v;
		try {
			v = clientWebServiceVeterinario.getVeterinarioByCRMV(crmvVeterinario);
			
			if (v != null){
				this.listaVeterinarios = new ArrayList<Veterinario>();
				this.listaVeterinarios.add(v);
			}
		} catch (ApplicationException e) {
			FacesMessageUtil.addErrorContextFacesMessage("Erro", e.getLocalizedMessage());
			return;
		}
		
		if (this.listaVeterinarios == null || this.listaVeterinarios.isEmpty())
			FacesMessageUtil.addWarnContextFacesMessage("Nenhum veterin�rio foi encontrado", "");
	}
	
	public void selecionarVeterinario(Veterinario veterinario){
		this.formSRN.setVeterinario(veterinario);
		FacesMessageUtil.addInfoContextFacesMessage("Veterinario " + this.formSRN.getVeterinario().getNome() + " selecionado", "");
	}
	
	public FormSRN getFormSRN() {
		return formSRN;
	}

	public void setFormSRN(FormSRN formSRN) {
		this.formSRN = formSRN;
	}

	public Date getDataAtendimento() {
		return dataAtendimento;
	}

	public void setDataAtendimento(Date dataAtendimento) {
		this.dataAtendimento = dataAtendimento;
	}

	public String getCpfVeterinario() {
		return cpfVeterinario;
	}

	public void setCpfVeterinario(String cpfVeterinario) {
		this.cpfVeterinario = cpfVeterinario;
	}

	public String getCrmvVeterinario() {
		return crmvVeterinario;
	}

	public void setCrmvVeterinario(String crmvVeterinario) {
		this.crmvVeterinario = crmvVeterinario;
	}

	public List<Veterinario> getListaVeterinarios() {
		return listaVeterinarios;
	}

	public void setListaVeterinarios(List<Veterinario> listaVeterinarios) {
		this.listaVeterinarios = listaVeterinarios;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdFormIN() {
		return idFormIN;
	}

	public void setIdFormIN(Long idFormIN) {
		this.idFormIN = idFormIN;
	}

	public FormIN getFormIN() {
		return formIN;
	}

	public void setFormIN(FormIN formIN) {
		this.formIN = formIN;
	}

}
