package br.gov.mt.indea.sistemaformin.webservice.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;

public class ExploracaoSuideoPK implements Serializable {

	private static final long serialVersionUID = -8319965237403920525L;

	private Integer exploracao;

    @XmlElement
    private Especie especie;
    
    public ExploracaoSuideoPK() {
    }

    public Integer getExploracao() {
        return exploracao;
    }

    public void setExploracao(Integer exploracao) {
        this.exploracao = exploracao;
    }

    public Especie getEspecie() {
        return especie;
    }

    public void setEspecie(Especie especie) {
        this.especie = especie;
    }

    @Override
    public int hashCode() {
        return exploracao.hashCode() + especie.hashCode();
    }
    
    @Override
    public boolean equals(Object o) {
        if (o instanceof ExploracaoSuideoPK) {
            ExploracaoSuideoPK exploracaoEquideo = (ExploracaoSuideoPK) o;
            if (!exploracaoEquideo.getExploracao().equals(exploracao)) {
                return false;
            }
            if (!exploracaoEquideo.getEspecie().equals(especie)) {
                return false;
            }
            return true;
        }
        return false;
    }
    
}