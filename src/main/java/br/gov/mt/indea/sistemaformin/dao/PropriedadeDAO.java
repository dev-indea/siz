package br.gov.mt.indea.sistemaformin.dao;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.inject.Inject;


import br.gov.mt.indea.sistemaformin.webservice.entity.Propriedade;

@Stateless
public class PropriedadeDAO extends DAO<Propriedade>{

	@Inject
	private EntityManager em;
	
	public PropriedadeDAO() {
		super(Propriedade.class);
	}

	@Override
	public EntityManager getEntityManager() {
		return this.em;
	}
	
	

}
