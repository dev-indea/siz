package br.gov.mt.indea.sistemaformin.service;

import java.util.Calendar;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.mt.indea.sistemaformin.entity.DetalheFormNotifica;
import br.gov.mt.indea.sistemaformin.entity.FormNotifica;
import br.gov.mt.indea.sistemaformin.entity.Municipio;
import br.gov.mt.indea.sistemaformin.entity.dto.AbstractDTO;
import br.gov.mt.indea.sistemaformin.entity.dto.FormNotificaDTO;
import br.gov.mt.indea.sistemaformin.enums.Dominio.SimNao;
import br.gov.mt.indea.sistemaformin.util.StringUtil;
import br.gov.mt.indea.sistemaformin.webservice.entity.Exploracao;
import br.gov.mt.indea.sistemaformin.webservice.entity.Produtor;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class FormNotificaService extends PaginableService<FormNotifica, Long> {
	
	private static final Logger log = LoggerFactory.getLogger(FormNotificaService.class);

	protected FormNotificaService() {
		super(FormNotifica.class);
	}
	
	public FormNotifica findByIdFetchAll(Long id){
		FormNotifica formNotifica;
		
		StringBuilder sql = new StringBuilder();
		sql.append("  from FormNotifica formNotifica ")
		   .append("  left join fetch formNotifica.notificante notificante")
		   .append("  left join fetch notificante.pessoa")
		   .append("  left join fetch notificante.instituicao instituicao")
		   .append("  left join fetch instituicao.endereco enderecoInst")
		   .append("  left join fetch enderecoInst.municipio municipioInst")
		   .append("  left join fetch municipioInst.uf")
		   .append("  left join fetch formNotifica.listaDetalheFormNotifica listaDetalheFormNotifica")
		   .append("  left join fetch formNotifica.suspeitaDoencaAnimal suspeita")
		   .append(" where formNotifica.id = :id ");
		
		Query query = getSession().createQuery(sql.toString()).setLong("id", id);
		formNotifica = (FormNotifica) query.uniqueResult();
		
		if (formNotifica != null){
			if (formNotifica.getSuspeitaDoencaAnimal() != null){
				Hibernate.initialize(formNotifica.getSuspeitaDoencaAnimal().getSuspeitaEspecies());
				Hibernate.initialize(formNotifica.getSuspeitaDoencaAnimal().getTestesLaboratoriais());
			}
			
			if (formNotifica.getListaDetalheFormNotifica() != null && !formNotifica.getListaDetalheFormNotifica().isEmpty()){
				for (DetalheFormNotifica detalhe : formNotifica.getListaDetalheFormNotifica()) {
					Hibernate.initialize(detalhe.getPropriedade().getProprietarios());
					Hibernate.initialize(detalhe.getPropriedade().getExploracaos());
					Hibernate.initialize(detalhe.getPropriedade().getEndereco());
					
					if (detalhe.getPropriedade().getExploracaos() != null && !detalhe.getPropriedade().getExploracaos().isEmpty())
						for (Exploracao	exploracao : detalhe.getPropriedade().getExploracaos()) {
							Hibernate.initialize(exploracao.getProdutores());
						}
				}
			}
			
		}
		
		return formNotifica;
	}
	
	@SuppressWarnings("unchecked")
	public List<DetalheFormNotifica> findAll(Municipio municipio){
		if (municipio == null)
			return null;
		
		List<DetalheFormNotifica> listaDetalheFormNotifica;
		
		StringBuilder sql = new StringBuilder();
		sql.append("  from DetalheFormNotifica detalheFormNotifica ")
		   .append("  left join fetch detalheFormNotifica.formNotifica formNotifica")
		   .append("  left join fetch formNotifica.notificante")
		   .append("  left join fetch detalheFormNotifica.propriedade propriedade")
		   .append("  left join fetch propriedade.municipio municipio")
		   .append("  left join fetch detalheFormNotifica.formIN formin")
		   .append("  left join fetch formin.propriedade")
		   .append("  left join fetch detalheFormNotifica.visitaPropriedadeRural visitaPropriedadeRural")
		   .append("  left join fetch visitaPropriedadeRural.propriedade propriedade")
		   .append("  left join fetch visitaPropriedadeRural.recinto recinto")
		   .append("  left join fetch propriedade.municipio")
		   .append("  left join fetch recinto.municipio")
		   .append(" where municipio.id = :id ");
		
		Query query = getSession().createQuery(sql.toString()).setLong("id", municipio.getId());
		listaDetalheFormNotifica = (List<DetalheFormNotifica>) query.list();
		
		if (listaDetalheFormNotifica != null){
			
			
		}
		
		return listaDetalheFormNotifica;
	}
	
	@Override
	public void saveOrUpdate(FormNotifica formNotifica) {
		formNotifica = (FormNotifica) this.getSession().merge(formNotifica);
		super.saveOrUpdate(formNotifica);
		
		log.info("Salvando Form Notifica {}", formNotifica.getId());
	}
	
	public void saveOrUpdate(DetalheFormNotifica detalheFormNotifica) {
		FormNotifica formNotifica = detalheFormNotifica.getFormNotifica(); 
		formNotifica = (FormNotifica) this.getSession().merge(formNotifica);
		
		detalheFormNotifica = (DetalheFormNotifica) this.getSession().merge(detalheFormNotifica);
		super.saveOrUpdate(formNotifica);
		
		log.info("Salvando Form Notifica {}", formNotifica.getId());
	}
	
	@Override
	public void delete(FormNotifica formNotifica) {
		super.delete(formNotifica);
		
		log.info("Removendo Form Notifica {}", formNotifica.getId());
	}
	
	public DetalheFormNotifica findDetalheFormNotificaByIdFetchAll(Long id) {
		DetalheFormNotifica detalheFormNotifica;
		
		StringBuilder sql = new StringBuilder();
		sql.append("  from DetalheFormNotifica detalheFormNotifica ")
		   .append("  left join fetch detalheFormNotifica.formNotifica formNotifica")
		   .append("  left join fetch formNotifica.notificante")
		   .append("  left join fetch detalheFormNotifica.propriedade propriedade")
		   .append("  left join fetch propriedade.municipio municipio")
		   .append("  left join fetch propriedade.proprietarios proprietarios")
		   .append("  left join fetch proprietarios.endereco ")
		   .append("  left join fetch propriedade.ule ")
		   .append("  left join fetch propriedade.endereco ")
		   .append(" where detalheFormNotifica.id = :id ");
		
		Query query = getSession().createQuery(sql.toString()).setLong("id", id);
		detalheFormNotifica = (DetalheFormNotifica) query.uniqueResult();
		
		if (detalheFormNotifica != null){
			Hibernate.initialize(detalheFormNotifica.getPropriedade().getExploracaos());
			
			for (Exploracao exploracao : detalheFormNotifica.getPropriedade().getExploracaos()){
				Hibernate.initialize(exploracao.getProdutores());
				
				for (Produtor produtor : exploracao.getProdutores()) {
					Hibernate.initialize(produtor.getMunicipioNascimento());
					Hibernate.initialize(produtor.getEndereco());
					Hibernate.initialize(produtor.getEndereco().getMunicipio());
					Hibernate.initialize(produtor.getEndereco().getMunicipio().getUf());
				}
			}
			
		}
		
		return detalheFormNotifica;
	}
	
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<FormNotifica> findAllComPaginacao(AbstractDTO dto, int first, int pageSize, String sortField, String sortOrder) {
		StringBuilder sql = new StringBuilder();
		sql.append("select formNotifica ")
		   .append("  from FormNotifica formNotifica ")
		   .append("  left join fetch formNotifica.notificante notificante")
		   .append("  left join fetch formNotifica.listaDetalheFormNotifica listaDetalheFormNotifica")
		   .append("  left join fetch listaDetalheFormNotifica.propriedade propriedade")
		   .append(" where 1 = 1");
		
		if (dto != null && !dto.isNull()){
			FormNotificaDTO formNotificaDTO = (FormNotificaDTO) dto;
			
			if (formNotificaDTO.getId() != null)
				sql.append("  and formNotifica.id = :id");
			else { 
				if (formNotificaDTO.getDataDaNotificacao() != null)
					sql.append("  and to_char(formNotifica.dataCadastro, 'YYYY-MM-DD HH:MI:SS') between :inicio and :final");
				if (formNotificaDTO.getMunicipio() != null)
					sql.append("  and propriedade.municipio = :municipio");
				if (formNotificaDTO.getAnonimo() != null)
					if (formNotificaDTO.getAnonimo().equals(SimNao.SIM))
						sql.append("  and notificante is null");
					else
						sql.append("  and notificante is not null");
				if (formNotificaDTO.getNomePropriedade() != null && !formNotificaDTO.getNomePropriedade().equals(""))
					sql.append("  and lower(remove_acento(propriedade.nome)) like :nomePropriedade");
				if (formNotificaDTO.getCodigoPropriedade() != null && !formNotificaDTO.getCodigoPropriedade().equals(""))
					sql.append("  and propriedade.codigoPropriedade = :codigoPropriedade");
			}
		}
		
		if (StringUtils.isNotBlank(sortField))
			sql.append(" order by formNotifica." + sortField + " " + sortOrder);

		Query query = getSession().createQuery(sql.toString());
		
		if (dto != null && !dto.isNull()){
			FormNotificaDTO formNotificaDTO = (FormNotificaDTO) dto;
			
			if (formNotificaDTO.getId() != null)
				query.setParameter("id", formNotificaDTO.getId());
			else {
				if (formNotificaDTO.getMunicipio() != null)
					query.setParameter("municipio", formNotificaDTO.getMunicipio());
				if (formNotificaDTO.getNomePropriedade() != null && !formNotificaDTO.getNomePropriedade().equals(""))
					query.setString("nomePropriedade", StringUtil.removeAcentos('%' + formNotificaDTO.getNomePropriedade().toLowerCase()) + '%');
				if (formNotificaDTO.getCodigoPropriedade() != null && !formNotificaDTO.getCodigoPropriedade().equals(""))
					query.setLong("codigoPropriedade", formNotificaDTO.getCodigoPropriedade());
				
				if (formNotificaDTO.getDataDaNotificacao() != null){
					Calendar i = Calendar.getInstance();
					i.setTime(formNotificaDTO.getDataDaNotificacao());
					
					StringBuilder sbI = new StringBuilder();
					sbI.append(i.get(Calendar.YEAR)).append("-")
					   .append(((i.get(Calendar.MONTH) + 1) + "").length() > 1 ? (i.get(Calendar.MONTH)+1) : "0" + (i.get(Calendar.MONTH)+1)).append("-")
					   .append((i.get(Calendar.DAY_OF_MONTH) + "").length() > 1 ? i.get(Calendar.DAY_OF_MONTH) : "0" + i.get(Calendar.DAY_OF_MONTH))
					   .append(" ")
					   .append("00").append(":")
					   .append("00").append(":")
					   .append("00");
					
					Calendar f = Calendar.getInstance();
					f.setTime(formNotificaDTO.getDataDaNotificacao());
					
					StringBuilder sbF = new StringBuilder();
					sbF.append(f.get(Calendar.YEAR)).append("-")
					   .append(((f.get(Calendar.MONTH) + 1) + "").length() > 1 ? (f.get(Calendar.MONTH)+1) : "0" + (f.get(Calendar.MONTH)+1)).append("-")
					   .append((f.get(Calendar.DAY_OF_MONTH) + "").length() > 1 ? f.get(Calendar.DAY_OF_MONTH) : "0" + f.get(Calendar.DAY_OF_MONTH))
					   .append(" ")
					   .append("23").append(":")
					   .append("23").append(":")
					   .append("59");
					
					query.setString("inicio", sbI.toString());
					query.setString("final", sbF.toString());
				}
			}
		}
		
		query.setFirstResult(first);
		query.setMaxResults(pageSize);

		List<FormNotifica> lista = query.list();
		
		if (dto != null && !dto.isNull()){
			FormNotificaDTO formNotificaDTO = (FormNotificaDTO) dto;
			formNotificaDTO.setId(null);
		}
		
		return lista;
    }
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Long countAll(AbstractDTO dto){
		StringBuilder sql = new StringBuilder();
		sql.append("select count(formNotifica) ")
		.append("  from FormNotifica formNotifica ")
		   .append("  left join formNotifica.notificante notificante")
		   .append("  left join formNotifica.listaDetalheFormNotifica listaDetalheFormNotifica")
		   .append("  left join listaDetalheFormNotifica.propriedade propriedade")
		   .append(" where 1 = 1");
		
		if (dto != null && !dto.isNull()){
			FormNotificaDTO formNotificaDTO = (FormNotificaDTO) dto;
			
			if (formNotificaDTO.getId() != null)
				sql.append("  and formNotifica.id = :id");
			else {
				if (formNotificaDTO.getDataDaNotificacao() != null)
					sql.append("  and to_char(formNotifica.dataCadastro, 'YYYY-MM-DD HH:MI:SS') between :inicio and :final");
				if (formNotificaDTO.getMunicipio() != null)
					sql.append("  and propriedade.municipio = :municipio");
				if (formNotificaDTO.getAnonimo() != null)
					if (formNotificaDTO.getAnonimo().equals(SimNao.SIM))
						sql.append("  and notificante is null");
					else
						sql.append("  and notificante is not null");
				if (formNotificaDTO.getNomePropriedade() != null && !formNotificaDTO.getNomePropriedade().equals(""))
					sql.append("  and lower(remove_acento(propriedade.nome)) like :nomePropriedade");
				if (formNotificaDTO.getCodigoPropriedade() != null && !formNotificaDTO.getCodigoPropriedade().equals(""))
					sql.append("  and propriedade.codigoPropriedade = :codigoPropriedade");
			}
		}
		
		Query query = getSession().createQuery(sql.toString());
		
		if (dto != null && !dto.isNull()){
			FormNotificaDTO formNotificaDTO = (FormNotificaDTO) dto;
			
			if (formNotificaDTO.getId() != null)
				query.setParameter("id", formNotificaDTO.getId());
			else {
					if (formNotificaDTO.getMunicipio() != null)
					query.setParameter("municipio", formNotificaDTO.getMunicipio());
				if (formNotificaDTO.getNomePropriedade() != null && !formNotificaDTO.getNomePropriedade().equals(""))
					query.setString("nomePropriedade", StringUtil.removeAcentos('%' + formNotificaDTO.getNomePropriedade().toLowerCase()) + '%');
				if (formNotificaDTO.getCodigoPropriedade() != null && !formNotificaDTO.getCodigoPropriedade().equals(""))
					query.setLong("codigoPropriedade", formNotificaDTO.getCodigoPropriedade());
				
				if (formNotificaDTO.getDataDaNotificacao() != null){
					Calendar i = Calendar.getInstance();
					i.setTime(formNotificaDTO.getDataDaNotificacao());
					
					StringBuilder sbI = new StringBuilder();
					sbI.append(i.get(Calendar.YEAR)).append("-")
					   .append(((i.get(Calendar.MONTH) + 1) + "").length() > 1 ? (i.get(Calendar.MONTH)+1) : "0" + (i.get(Calendar.MONTH)+1)).append("-")
					   .append((i.get(Calendar.DAY_OF_MONTH) + "").length() > 1 ? i.get(Calendar.DAY_OF_MONTH) : "0" + i.get(Calendar.DAY_OF_MONTH))
					   .append(" ")
					   .append("00").append(":")
					   .append("00").append(":")
					   .append("00");
					
					Calendar f = Calendar.getInstance();
					f.setTime(formNotificaDTO.getDataDaNotificacao());
					
					StringBuilder sbF = new StringBuilder();
					sbF.append(f.get(Calendar.YEAR)).append("-")
					   .append(((f.get(Calendar.MONTH) + 1) + "").length() > 1 ? (f.get(Calendar.MONTH)+1) : "0" + (f.get(Calendar.MONTH)+1)).append("-")
					   .append((f.get(Calendar.DAY_OF_MONTH) + "").length() > 1 ? f.get(Calendar.DAY_OF_MONTH) : "0" + f.get(Calendar.DAY_OF_MONTH))
					   .append(" ")
					   .append("23").append(":")
					   .append("23").append(":")
					   .append("59");
					
					query.setString("inicio", sbI.toString());
					query.setString("final", sbF.toString());
				}
			}
		}
    	
		return (Long) query.uniqueResult();
	}
	
	@Override
	public void validar(FormNotifica FormNotifica) {

	}

	@Override
	public void validarPersist(FormNotifica FormNotifica) {

	}

	@Override
	public void validarMerge(FormNotifica FormNotifica) {

	}

	@Override
	public void validarDelete(FormNotifica FormNotifica) {

	}

}
