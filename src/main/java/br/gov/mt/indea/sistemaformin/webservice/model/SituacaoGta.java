package br.gov.mt.indea.sistemaformin.webservice.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class SituacaoGta implements Serializable {

	private static final long serialVersionUID = -6354606077441731350L;

	private Long idTable;
	
    private String id;
    
    private String nome;
    
    private String codigoPga;

    public SituacaoGta() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public SituacaoGta(String id, String nome, String codigoPga) {
        this.id = id;
        this.nome = nome;
        this.codigoPga = codigoPga;
    }

    public String getCodigoPga() {
        return codigoPga;
    }

    public void setCodigoPga(String codigoPga) {
        this.codigoPga = codigoPga;
    }

	public Long getIdTable() {
		return idTable;
	}

	public void setIdTable(Long idTable) {
		this.idTable = idTable;
	}
}