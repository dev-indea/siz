package br.gov.mt.indea.sistemaformin.webservice.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class PessoaDestinoGta implements Serializable {

	private static final long serialVersionUID = -6717339493604505314L;

	private Long id;
    
    @XmlElement
    private Pessoa produtor;
    
    @XmlElement
    private Pessoa estabelecimento;
    
    @XmlElement
    private Exploracao exploracao;
    
    @XmlElement
    private AglomeracaoSimple aglomeracao;
    
    @XmlElement
    private Municipio municipio;
    
    private String cnpjCpf;
    
    private String codigoEstabelecimento;
    
    private String nomeProdutor;
    
    private String nomeEstabelecimento;
    
    private String codigoExploracao;
    
    private String codigoAglomeracao;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Pessoa getProdutor() {
        return produtor;
    }

    public void setProdutor(Pessoa produtor) {
        this.produtor = produtor;
    }

    public Pessoa getEstabelecimento() {
        return estabelecimento;
    }

    public void setEstabelecimento(Pessoa estabelecimento) {
        this.estabelecimento = estabelecimento;
    }

    public AglomeracaoSimple getAglomeracao() {
        return aglomeracao;
    }

    public void setAglomeracao(AglomeracaoSimple aglomeracao) {
        this.aglomeracao = aglomeracao;
    }

    public Exploracao getExploracao() {
        return exploracao;
    }

    public void setExploracao(Exploracao exploracao) {
        this.exploracao = exploracao;
    }

    public Municipio getMunicipio() {
        return municipio;
    }

    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }

    public String getCnpjCpf() {
        return cnpjCpf;
    }

    public void setCnpjCpf(String cnpjCpf) {
        this.cnpjCpf = cnpjCpf;
    }

    public String getCodigoEstabelecimento() {
        return codigoEstabelecimento;
    }

    public void setCodigoEstabelecimento(String codigoEstabelecimento) {
        this.codigoEstabelecimento = codigoEstabelecimento;
    }

    public String getNomeProdutor() {
        return nomeProdutor;
    }

    public void setNomeProdutor(String nomeProdutor) {
        this.nomeProdutor = nomeProdutor;
    }

    public String getNomeEstabelecimento() {
        return nomeEstabelecimento;
    }

    public void setNomeEstabelecimento(String nomeEstabelecimento) {
        this.nomeEstabelecimento = nomeEstabelecimento;
    }

    public String getCodigoExploracao() {
        return codigoExploracao;
    }

    public void setCodigoExploracao(String codigoExploracao) {
        this.codigoExploracao = codigoExploracao;
    }

    public String getCodigoAglomeracao() {
        return codigoAglomeracao;
    }

    public void setCodigoAglomeracao(String codigoAglomeracao) {
        this.codigoAglomeracao = codigoAglomeracao;
    }
}