package br.gov.mt.indea.sistemaformin.entity;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

@Audited
@Entity(name="chave_secundaria_termo")
public class ChaveSecundariaTermoFiscalizacao extends BaseEntity<Long>{

	private static final long serialVersionUID = 2357402340404570791L;

	@Id
	@SequenceGenerator(name="chave_secundaria_termo_seq", sequenceName="chave_secundaria_termo_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="chave_secundaria_termo_seq")
	private Long id;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_cadastro")
	private Calendar dataCadastro;
	
	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="id_tipo_chave_secundaria_visit")
	private TipoChaveSecundariaTermoFiscalizacao tipoChaveSecundariaTermoFiscalizacao;
	
	@ManyToOne
	@JoinColumn(name="id_chave_principal_termo")
	private ChavePrincipalTermoFiscalizacao chavePrincipalTermoFiscalizacao;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Calendar getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Calendar dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public TipoChaveSecundariaTermoFiscalizacao getTipoChaveSecundariaTermoFiscalizacao() {
		return tipoChaveSecundariaTermoFiscalizacao;
	}

	public void setTipoChaveSecundariaTermoFiscalizacao(
			TipoChaveSecundariaTermoFiscalizacao tipoChaveSecundariaTermoFiscalizacao) {
		this.tipoChaveSecundariaTermoFiscalizacao = tipoChaveSecundariaTermoFiscalizacao;
	}

	public ChavePrincipalTermoFiscalizacao getChavePrincipalTermoFiscalizacao() {
		return chavePrincipalTermoFiscalizacao;
	}

	public void setChavePrincipalTermoFiscalizacao(
			ChavePrincipalTermoFiscalizacao chavePrincipalTermoFiscalizacao) {
		this.chavePrincipalTermoFiscalizacao = chavePrincipalTermoFiscalizacao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((tipoChaveSecundariaTermoFiscalizacao == null) ? 0
						: tipoChaveSecundariaTermoFiscalizacao.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ChaveSecundariaTermoFiscalizacao other = (ChaveSecundariaTermoFiscalizacao) obj;
		if (tipoChaveSecundariaTermoFiscalizacao == null) {
			if (other.tipoChaveSecundariaTermoFiscalizacao != null)
				return false;
		} else if (!tipoChaveSecundariaTermoFiscalizacao
				.equals(other.tipoChaveSecundariaTermoFiscalizacao))
			return false;
		return true;
	}

}