package br.gov.mt.indea.sistemaformin.dao;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import br.gov.mt.indea.sistemaformin.entity.Municipio;
import br.gov.mt.indea.sistemaformin.entity.NumeroVisitaPropriedadeRural;

@Stateless
public class NumeroVisitaPropriedadeRuralDAO extends DAO<NumeroVisitaPropriedadeRural>{

	@Inject
	private EntityManager em;
	
	public NumeroVisitaPropriedadeRuralDAO() {
		super(NumeroVisitaPropriedadeRural.class);
	}

	@Override
	public EntityManager getEntityManager() {
		return this.em;
	}
	
	public NumeroVisitaPropriedadeRural getNumeroVisitaPropriedadeRuralByMunicipio(Municipio municipio, int ano){
		NumeroVisitaPropriedadeRural numeroVisitaPropriedadeRural;
		
		CriteriaBuilder cb  = em.getCriteriaBuilder();
		CriteriaQuery<NumeroVisitaPropriedadeRural> criteria = cb.createQuery(NumeroVisitaPropriedadeRural.class);
		Root<NumeroVisitaPropriedadeRural> rootNumeroVisitaPropriedadeRural = criteria.from(NumeroVisitaPropriedadeRural.class);
		
		List<Predicate> predicados = new ArrayList<Predicate>();
		
		predicados.add(cb.equal(rootNumeroVisitaPropriedadeRural.get("municipio"), municipio));
		predicados.add(cb.equal(rootNumeroVisitaPropriedadeRural.get("ano"), ano));
		
		criteria.where(cb.and(predicados.toArray(new Predicate[]{})));
		
		try {
			numeroVisitaPropriedadeRural = em.createQuery(criteria).getSingleResult();
		} catch (NoResultException e) {
			numeroVisitaPropriedadeRural = null;
		}

		return numeroVisitaPropriedadeRural;
	}

}
