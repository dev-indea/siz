package br.gov.mt.indea.sistemaformin.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;

import br.gov.mt.indea.sistemaformin.entity.Municipio;
import br.gov.mt.indea.sistemaformin.entity.RegistroEntradaLASA;
import br.gov.mt.indea.sistemaformin.entity.dto.AbstractDTO;
import br.gov.mt.indea.sistemaformin.entity.dto.RegistroEntradaLASADTO;

@Stateless
public class RegistroEntradaLASAService extends PaginableService<RegistroEntradaLASA, Long>{

	protected RegistroEntradaLASAService() {
		super(RegistroEntradaLASA.class);
	}
	
	@SuppressWarnings("unchecked")
	public List<RegistroEntradaLASA> findAll(Municipio municipio){
		if (municipio == null)
			return null;
		
		List<RegistroEntradaLASA> comunicacaoAIE;
		
		StringBuilder sql = new StringBuilder();
		sql.append("  from RegistroEntradaLASA detalhe ")
		   .append("  left join fetch detalhe.municipio municipio")
		   .append(" where municipio.id = :id ");
		
		Query query = getSession().createQuery(sql.toString()).setLong("id", municipio.getId());
		comunicacaoAIE = (List<RegistroEntradaLASA>) query.list();
		
		return comunicacaoAIE;
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public Long countAll(AbstractDTO dto) {
		StringBuilder sql = new StringBuilder();
		sql.append("select count(registroEntradaLASA) ")
		   .append("  from " + this.getType().getSimpleName() + " as registroEntradaLASA")
		   .append("  join registroEntradaLASA.municipio ")
		   .append("  where 1 = 1 ");
		
		if (dto != null && !dto.isNull()){
			RegistroEntradaLASADTO registroEntradaLASADTO = (RegistroEntradaLASADTO) dto;
			
			if (registroEntradaLASADTO.getMunicipio() != null)
				sql.append("  and registroEntradaLASA.municipio = :municipio");
			if (registroEntradaLASADTO.getNumero() != null)
				sql.append("  and registroEntradaLASA.numero = :numero");
		}
		
		Query query = getSession().createQuery(sql.toString());
		
		if (dto != null && !dto.isNull()){
			RegistroEntradaLASADTO registroEntradaLASADTO = (RegistroEntradaLASADTO) dto;
			
			if (registroEntradaLASADTO.getMunicipio() != null)
				query.setParameter("municipio", registroEntradaLASADTO.getMunicipio());
			
			if (registroEntradaLASADTO.getNumero() != null)
				query.setLong("numero", registroEntradaLASADTO.getNumero());
		}

		return (Long) query.uniqueResult();
    }
	
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<RegistroEntradaLASA> findAllComPaginacao(AbstractDTO dto, int first, int pageSize, String sortField, String sortOrder) {
		StringBuilder sql = new StringBuilder();
		sql.append("select registroEntradaLASA")
		   .append("  from " + this.getType().getSimpleName() + " as registroEntradaLASA")
		   .append("  join fetch registroEntradaLASA.municipio ")
		   .append("  where 1 = 1 ");
		
		if (dto != null && !dto.isNull()){
			RegistroEntradaLASADTO registroEntradaLASADTO = (RegistroEntradaLASADTO) dto;
			
			if (registroEntradaLASADTO.getMunicipio() != null)
				sql.append("  and registroEntradaLASA.municipio = :municipio");
			if (registroEntradaLASADTO.getNumero() != null)
				sql.append("  and registroEntradaLASA.numero = :numero");
		}
		
		if (StringUtils.isNotBlank(sortField))
			sql.append(" order by registroEntradaLASA." + sortField + " " + sortOrder);

		Query query = getSession().createQuery(sql.toString());
		
		if (dto != null && !dto.isNull()){
			RegistroEntradaLASADTO registroEntradaLASADTO = (RegistroEntradaLASADTO) dto;
			
			if (registroEntradaLASADTO.getMunicipio() != null)
				query.setParameter("municipio", registroEntradaLASADTO.getMunicipio());
			
			if (registroEntradaLASADTO.getNumero() != null)
				query.setLong("numero", registroEntradaLASADTO.getNumero());
		}

		query.setFirstResult(first);
		query.setMaxResults(pageSize);

		List<RegistroEntradaLASA> lista = query.list();
		
		return lista;
    }

	@Override
	public void validar(RegistroEntradaLASA model) {
		
	}

	@Override
	public void validarPersist(RegistroEntradaLASA model) {
		
	}

	@Override
	public void validarMerge(RegistroEntradaLASA model) {
		
	}

	@Override
	public void validarDelete(RegistroEntradaLASA model) {
		
	}

}
