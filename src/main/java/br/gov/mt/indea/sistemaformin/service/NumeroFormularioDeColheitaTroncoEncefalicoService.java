package br.gov.mt.indea.sistemaformin.service;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.mt.indea.sistemaformin.entity.NumeroFormularioDeColheitaTroncoEncefalico;
import br.gov.mt.indea.sistemaformin.entity.ServicoDeInspecao;
import br.gov.mt.indea.sistemaformin.entity.UF;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class NumeroFormularioDeColheitaTroncoEncefalicoService extends PaginableService<NumeroFormularioDeColheitaTroncoEncefalico, Long> {

	private static final Logger log = LoggerFactory.getLogger(NumeroFormularioDeColheitaTroncoEncefalicoService.class);
	
	protected NumeroFormularioDeColheitaTroncoEncefalicoService() {
		super(NumeroFormularioDeColheitaTroncoEncefalico.class);
	}
	
	public NumeroFormularioDeColheitaTroncoEncefalico findByIdFetchAll(Long id){
		NumeroFormularioDeColheitaTroncoEncefalico numeroFormularioDeColheitaTroncoEncefalico;
		
		StringBuilder sql = new StringBuilder();
		sql.append("select numeroFormularioDeColheitaTroncoEncefalico ")
		   .append("  from NumeroFormularioDeColheitaTroncoEncefalico numeroFormularioDeColheitaTroncoEncefalico ")
		   .append(" where numeroFormularioDeColheitaTroncoEncefalico.id = :id ");
		
		Query query = getSession().createQuery(sql.toString()).setLong("id", id);
		numeroFormularioDeColheitaTroncoEncefalico = (NumeroFormularioDeColheitaTroncoEncefalico) query.uniqueResult();
		
		return numeroFormularioDeColheitaTroncoEncefalico;
	}
	
	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	public NumeroFormularioDeColheitaTroncoEncefalico getNumeroFormularioDeColheitaTroncoEncefalicoByMunicipio(UF uf, ServicoDeInspecao servicoDeInspecao, int ano){
		NumeroFormularioDeColheitaTroncoEncefalico numeroFormularioDeColheitaTroncoEncefalico;
		
		StringBuilder sql = new StringBuilder();
		sql.append("  from NumeroFormularioDeColheitaTroncoEncefalico numeroFormularioDeColheitaTroncoEncefalico ")
		   .append(" where numeroFormularioDeColheitaTroncoEncefalico.uf = :uf ")
		   .append("   and numeroFormularioDeColheitaTroncoEncefalico.ano = :ano")
		   .append("   and numeroFormularioDeColheitaTroncoEncefalico.servicoDeInspecao = :servicoDeInspecao");
		
		Query query = getSession().createQuery(sql.toString()).setParameter("uf", uf).setParameter("servicoDeInspecao", servicoDeInspecao).setInteger("ano", ano);
		numeroFormularioDeColheitaTroncoEncefalico = (NumeroFormularioDeColheitaTroncoEncefalico) query.uniqueResult();

		return numeroFormularioDeColheitaTroncoEncefalico;
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	public void saveOrUpdate(NumeroFormularioDeColheitaTroncoEncefalico numeroFormularioDeColheitaTroncoEncefalico) {
		super.saveOrUpdate(numeroFormularioDeColheitaTroncoEncefalico);
		
		log.info("Salvando Numero Formulário de Colheita de Tronco Encefálico {}", numeroFormularioDeColheitaTroncoEncefalico.getId());
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void initialize(NumeroFormularioDeColheitaTroncoEncefalico numeroFormularioDeColheitaTroncoEncefalico) {
		super.saveOrUpdate(numeroFormularioDeColheitaTroncoEncefalico);
		
		log.info("Salvando Numero Formulário de Colheita de Tronco Encefálico {}", numeroFormularioDeColheitaTroncoEncefalico.getId());
	}
	
	@Override
	public void validar(NumeroFormularioDeColheitaTroncoEncefalico NumeroFormularioDeColheitaTroncoEncefalico) {

	}

	@Override
	public void validarPersist(NumeroFormularioDeColheitaTroncoEncefalico NumeroFormularioDeColheitaTroncoEncefalico) {

	}

	@Override
	public void validarMerge(NumeroFormularioDeColheitaTroncoEncefalico NumeroFormularioDeColheitaTroncoEncefalico) {

	}

	@Override
	public void validarDelete(NumeroFormularioDeColheitaTroncoEncefalico NumeroFormularioDeColheitaTroncoEncefalico) {

	}

}
