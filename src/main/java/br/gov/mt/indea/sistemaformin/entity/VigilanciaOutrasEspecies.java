package br.gov.mt.indea.sistemaformin.entity;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

@Audited
@Entity(name="vigilancia_outras_especies")
public class VigilanciaOutrasEspecies extends BaseEntity<Long>{
	
	private static final long serialVersionUID = -361785479572430083L;

	@Id
	@SequenceGenerator(name="vigilancia_outras_especies_seq", sequenceName="vigilancia_outras_especies_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="vigilancia_outras_especies_seq")
	private Long id;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_cadastro")
	private Calendar dataCadastro;
	
	@Column(name="quantidade_ovinos_insp_f")
	private Integer quantidadeOvinosInspecionados_F;
	
	@Column(name="quantidade_ovinos_insp_m")
	private Integer quantidadeOvinosInspecionados_M;
	
	@Column(name="quantidade_caprinos_insp_f")
	private Integer quantidadeCaprinosInspecionados_F;
	
	@Column(name="quantidade_caprinos_insp_m")
	private Integer quantidadeCaprinosInspecionados_M;
	
	@Column(name="quantidade_equinos_insp_f")
	private Integer quantidadeEquinosInspecionados_F;
	
	@Column(name="quantidade_equinos_insp_m")
	private Integer quantidadeEquinosInspecionados_M;
	
	@Column(name="quantidade_muares_insp_f")
	private Integer quantidadeMuaresInspecionados_F;
	
	@Column(name="quantidade_muares_insp_m")
	private Integer quantidadeMuaresInspecionados_M;
	
	@Column(name="quantidade_asininos_insp_f")
	private Integer quantidadeAsininosInspecionados_F;
	
	@Column(name="quantidade_asininos_insp_m")
	private Integer quantidadeAsininosInspecionados_M;
	
	@Column(name="quantidade_peixes_insp")
	private Integer quantidadePeixesInspecionados;
	
	@Column(name="quantidade_ovinos_vist")
	private Integer quantidadeOvinosVistoriados;
	
	@Column(name="quantidade_caprinos_vist")
	private Integer quantidadeCaprinosVistoriados;
	
	@Column(name="quantidade_equinos_vist")
	private Integer quantidadeEquinosVistoriados;
	
	@Column(name="quantidade_muares_vist")
	private Integer quantidadeMuaresVistoriados;
	@Column(name="quantidade_asininos_vist")
	private Integer quantidadeAsininosVistoriados;
	
	@Column(name="quantidade_peixes_vist")
	private Integer quantidadePeixesVistoriados;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Calendar getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Calendar dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Integer getQuantidadeOvinosInspecionados_F() {
		return quantidadeOvinosInspecionados_F;
	}

	public void setQuantidadeOvinosInspecionados_F(
			Integer quantidadeOvinosInspecionados_F) {
		this.quantidadeOvinosInspecionados_F = quantidadeOvinosInspecionados_F;
	}

	public Integer getQuantidadeOvinosInspecionados_M() {
		return quantidadeOvinosInspecionados_M;
	}

	public void setQuantidadeOvinosInspecionados_M(
			Integer quantidadeOvinosInspecionados_M) {
		this.quantidadeOvinosInspecionados_M = quantidadeOvinosInspecionados_M;
	}

	public Integer getQuantidadeCaprinosInspecionados_F() {
		return quantidadeCaprinosInspecionados_F;
	}

	public void setQuantidadeCaprinosInspecionados_F(
			Integer quantidadeCaprinosInspecionados_F) {
		this.quantidadeCaprinosInspecionados_F = quantidadeCaprinosInspecionados_F;
	}

	public Integer getQuantidadeCaprinosInspecionados_M() {
		return quantidadeCaprinosInspecionados_M;
	}

	public void setQuantidadeCaprinosInspecionados_M(
			Integer quantidadeCaprinosInspecionados_M) {
		this.quantidadeCaprinosInspecionados_M = quantidadeCaprinosInspecionados_M;
	}

	public Integer getQuantidadeEquinosInspecionados_F() {
		return quantidadeEquinosInspecionados_F;
	}

	public void setQuantidadeEquinosInspecionados_F(
			Integer quantidadeEquinosInspecionados_F) {
		this.quantidadeEquinosInspecionados_F = quantidadeEquinosInspecionados_F;
	}

	public Integer getQuantidadeEquinosInspecionados_M() {
		return quantidadeEquinosInspecionados_M;
	}

	public void setQuantidadeEquinosInspecionados_M(
			Integer quantidadeEquinosInspecionados_M) {
		this.quantidadeEquinosInspecionados_M = quantidadeEquinosInspecionados_M;
	}

	public Integer getQuantidadeMuaresInspecionados_F() {
		return quantidadeMuaresInspecionados_F;
	}

	public void setQuantidadeMuaresInspecionados_F(
			Integer quantidadeMuaresInspecionados_F) {
		this.quantidadeMuaresInspecionados_F = quantidadeMuaresInspecionados_F;
	}

	public Integer getQuantidadeMuaresInspecionados_M() {
		return quantidadeMuaresInspecionados_M;
	}

	public void setQuantidadeMuaresInspecionados_M(
			Integer quantidadeMuaresInspecionados_M) {
		this.quantidadeMuaresInspecionados_M = quantidadeMuaresInspecionados_M;
	}

	public Integer getQuantidadeAsininosInspecionados_F() {
		return quantidadeAsininosInspecionados_F;
	}

	public void setQuantidadeAsininosInspecionados_F(
			Integer quantidadeAsininosInspecionados_F) {
		this.quantidadeAsininosInspecionados_F = quantidadeAsininosInspecionados_F;
	}

	public Integer getQuantidadeAsininosInspecionados_M() {
		return quantidadeAsininosInspecionados_M;
	}

	public void setQuantidadeAsininosInspecionados_M(
			Integer quantidadeAsininosInspecionados_M) {
		this.quantidadeAsininosInspecionados_M = quantidadeAsininosInspecionados_M;
	}

	public Integer getQuantidadePeixesInspecionados() {
		return quantidadePeixesInspecionados;
	}

	public void setQuantidadePeixesInspecionados(
			Integer quantidadePeixesInspecionados) {
		this.quantidadePeixesInspecionados = quantidadePeixesInspecionados;
	}

	public Integer getQuantidadeOvinosVistoriados() {
		return quantidadeOvinosVistoriados;
	}

	public void setQuantidadeOvinosVistoriados(Integer quantidadeOvinosVistoriados) {
		this.quantidadeOvinosVistoriados = quantidadeOvinosVistoriados;
	}

	public Integer getQuantidadeCaprinosVistoriados() {
		return quantidadeCaprinosVistoriados;
	}

	public void setQuantidadeCaprinosVistoriados(
			Integer quantidadeCaprinosVistoriados) {
		this.quantidadeCaprinosVistoriados = quantidadeCaprinosVistoriados;
	}

	public Integer getQuantidadeEquinosVistoriados() {
		return quantidadeEquinosVistoriados;
	}

	public void setQuantidadeEquinosVistoriados(Integer quantidadeEquinosVistoriados) {
		this.quantidadeEquinosVistoriados = quantidadeEquinosVistoriados;
	}

	public Integer getQuantidadeMuaresVistoriados() {
		return quantidadeMuaresVistoriados;
	}

	public void setQuantidadeMuaresVistoriados(Integer quantidadeMuaresVistoriados) {
		this.quantidadeMuaresVistoriados = quantidadeMuaresVistoriados;
	}

	public Integer getQuantidadeAsininosVistoriados() {
		return quantidadeAsininosVistoriados;
	}

	public void setQuantidadeAsininosVistoriados(
			Integer quantidadeAsininosVistoriados) {
		this.quantidadeAsininosVistoriados = quantidadeAsininosVistoriados;
	}

	public Integer getQuantidadePeixesVistoriados() {
		return quantidadePeixesVistoriados;
	}

	public void setQuantidadePeixesVistoriados(Integer quantidadePeixesVistoriados) {
		this.quantidadePeixesVistoriados = quantidadePeixesVistoriados;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VigilanciaOutrasEspecies other = (VigilanciaOutrasEspecies) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}