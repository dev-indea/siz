package br.gov.mt.indea.sistemaformin.managedbeans;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.Past;

import org.primefaces.PrimeFaces;
import org.primefaces.context.RequestContext;

import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLBeanName;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;

import br.gov.mt.indea.sistemaformin.entity.Doenca;
import br.gov.mt.indea.sistemaformin.entity.InvestigacaoEpidemiologica;
import br.gov.mt.indea.sistemaformin.entity.Municipio;
import br.gov.mt.indea.sistemaformin.entity.TipoChavePrincipalVisitaPropriedadeRural;
import br.gov.mt.indea.sistemaformin.entity.VigilanciaAlimentosRuminantes;
import br.gov.mt.indea.sistemaformin.entity.VigilanciaAves;
import br.gov.mt.indea.sistemaformin.entity.VigilanciaBovinosEBubalinos;
import br.gov.mt.indea.sistemaformin.entity.VigilanciaOutrasEspecies;
import br.gov.mt.indea.sistemaformin.entity.VigilanciaSuideos;
import br.gov.mt.indea.sistemaformin.entity.VigilanciaVeterinaria;
import br.gov.mt.indea.sistemaformin.entity.dto.VigilanciaVeterinariaDTO;
import br.gov.mt.indea.sistemaformin.enums.Dominio.AtivoInativo;
import br.gov.mt.indea.sistemaformin.enums.Dominio.PontosDeRisco;
import br.gov.mt.indea.sistemaformin.enums.Dominio.SimNao;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoAlimentoSuideos;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoDestinoSuideos;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoOrigemSuideos;
import br.gov.mt.indea.sistemaformin.exception.ApplicationException;
import br.gov.mt.indea.sistemaformin.service.LazyObjectDataModel;
import br.gov.mt.indea.sistemaformin.service.MunicipioService;
import br.gov.mt.indea.sistemaformin.service.PropriedadeService;
import br.gov.mt.indea.sistemaformin.service.TipoChavePrincipalVisitaPropriedadeRuralService;
import br.gov.mt.indea.sistemaformin.service.VigilanciaVeterinariaService;
import br.gov.mt.indea.sistemaformin.util.FacesMessageUtil;
import br.gov.mt.indea.sistemaformin.webservice.client.ClientWebServiceVeterinario;
import br.gov.mt.indea.sistemaformin.webservice.entity.Produtor;
import br.gov.mt.indea.sistemaformin.webservice.entity.Propriedade;
import br.gov.mt.indea.sistemaformin.webservice.entity.Veterinario;

@Named
@ViewScoped
@URLBeanName("vigilanciaVeterinariaManagedBean")
@URLMappings(mappings = {
		@URLMapping(id = "pesquisarVigilanciaVeterinaria", pattern = "/vigilanciaVeterinaria/pesquisar", viewId = "/pages/vigilanciaVeterinaria/lista.jsf"),
		@URLMapping(id = "incluirVigilanciaVeterinaria", pattern = "/vigilanciaVeterinaria/incluir", viewId = "/pages/vigilanciaVeterinaria/novo.jsf"),
		@URLMapping(id = "alterarVigilanciaVeterinaria", pattern = "/vigilanciaVeterinaria/alterar/#{vigilanciaVeterinariaManagedBean.id}", viewId = "/pages/vigilanciaVeterinaria/novo.jsf")})
public class VigilanciaVeterinariaManagedBean implements Serializable {

	private static final long serialVersionUID = 1760023349204758559L;

	private Long id;
	
	@Inject
	private VigilanciaVeterinariaService vigilanciaVeterinariaService;
	
	@Inject
	private TipoChavePrincipalVisitaPropriedadeRuralService tipoChavePrincipalVisitaPropriedadeRuralService;
	
	@Inject
	private MunicipioService municipioService;
	
	@Inject
	private VigilanciaVeterinaria vigilanciaVeterinaria;
	
	@Inject
	private VigilanciaVeterinariaDTO vigilanciaVeterinariaDTO;
	
	@Past(message="Data precisa ser menor ou igual a data atual")
	private Date dataVigilancia;
	
	private String cpfVeterinario;

	private String crmvVeterinario;

	private List<Veterinario> listaVeterinarios;
	
	@Inject
	private ClientWebServiceVeterinario clientWebServiceVeterinario;
	
	private boolean forceUpdatePropriedade = false;
	
	@Inject
	private PropriedadeService propriedadeService;
	
	private TipoChavePrincipalVisitaPropriedadeRural tipoChavePrincipalVisitaPropriedadeRural;
	
	private Long idPropriedade;

	private List<Propriedade> listaPropriedades;
	
	private VigilanciaAves vigilanciaAves;
	
	private boolean editandoVigilanciaAves = false;
	
	private boolean visualizandoVigilanciaAves = false;
	
	private InvestigacaoEpidemiologica investigacaoEpidemiologica;
	
	private boolean editandoInvestigacaoEpidemiologica = false;
	
	private boolean visualizandoInvestigacaoEpidemiologica = false;
	
	private Doenca doenca;
	
	private boolean editandoDoenca = false;
	
	private boolean visualizandoDoenca = false;
	
	private SimNao pontoDeRiscoProximoAPropriedade;
	
	private SimNao investigacaoDeAlimentosFornecidosARuminantes;
	
	private SimNao investigacaoBovinosEBubalinos;
	
	private SimNao inspecaoBovinosEBubalinos;
	
	private SimNao investigacaoSuideos;
	
	private SimNao inspecaoSuideos;
	
	private SimNao investigacaoOutrasEspecies;
	
	private SimNao inspecaoOutrasEspecies;
	
	@Past(message="Data precisa ser menor ou igual a data atual")
	private Date dataUltimoCasoDoencaDeInvestigacao;
	
	private LazyObjectDataModel<VigilanciaVeterinaria> listaVigilanciaVeterinaria;
	
	@PostConstruct
	private void init(){
		FacesMessageUtil.addInfoContextFacesMessage("Para vigil�ncias realizadas a partir do dia 15/05/2023, deve ser utilizado a nova vers�o do SIZ dispon�vel em: ", "https://siz-v3.indea.mt.gov.br/");
		PrimeFaces current = PrimeFaces.current();
		current.executeScript("PF('modalNotificacaoSizMobile').show();");
	}
	
	public List<Municipio> getListaMunicipiosBusca(){
		return municipioService.findAllByUF(this.vigilanciaVeterinariaDTO.getUf());
	}
	
	public void buscarVigilanciaVeterinaria(){
		this.listaVigilanciaVeterinaria = new LazyObjectDataModel<VigilanciaVeterinaria>(this.vigilanciaVeterinariaService, this.vigilanciaVeterinariaDTO);
	}
	
	private void limpar() {
		this.vigilanciaVeterinaria = new VigilanciaVeterinaria();
		
		this.investigacaoBovinosEBubalinos = null;
		this.investigacaoDeAlimentosFornecidosARuminantes = null;
		this.investigacaoOutrasEspecies = null;
		this.investigacaoSuideos = null;
		this.pontoDeRiscoProximoAPropriedade = null;
	}
	
	@URLAction(mappingId = "incluirVigilanciaVeterinaria", onPostback = false)
	public void novo(){
		limpar();
	}
	
	@URLAction(mappingId = "pesquisarVigilanciaVeterinaria", onPostback = false)
	public void pesquisar(){
		limpar();
		
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		String numeroVigilanciaVeterinaria = (String) request.getSession().getAttribute("numeroVigilanciaVeterinaria");
		
		if (numeroVigilanciaVeterinaria != null){
			this.vigilanciaVeterinariaDTO = new VigilanciaVeterinariaDTO();
			this.vigilanciaVeterinariaDTO.setNumero(numeroVigilanciaVeterinaria);
			this.buscarVigilanciaVeterinaria();
			
			request.getSession().setAttribute("numeroVigilanciaVeterinaria", null);
		}
	}

	@URLAction(mappingId = "alterarVigilanciaVeterinaria", onPostback = false)
	public void editar(){
		this.vigilanciaVeterinaria = vigilanciaVeterinariaService.findByIdFetchAll(this.getId());
		
		if (vigilanciaVeterinaria.getDataVigilancia() != null)
			this.dataVigilancia = vigilanciaVeterinaria.getDataVigilancia().getTime();
		
		if (this.vigilanciaVeterinaria.getPontosDeRisco() != null){
			if (!this.vigilanciaVeterinaria.getPontosDeRisco().isEmpty())
				this.pontoDeRiscoProximoAPropriedade = SimNao.SIM;
			else
				this.pontoDeRiscoProximoAPropriedade = SimNao.NAO;
		}else
			this.pontoDeRiscoProximoAPropriedade = SimNao.NAO;
		
		if (this.vigilanciaVeterinaria.getVigilanciaBovinosEBubalinos() != null){
			this.investigacaoBovinosEBubalinos = SimNao.SIM;
			
			if ((this.vigilanciaVeterinaria.getVigilanciaBovinosEBubalinos().getQuantidadeInspecionados00_04_F() == null) &&
				
				(this.vigilanciaVeterinaria.getVigilanciaBovinosEBubalinos().getQuantidadeInspecionados00_04_M() == null) &&
				
				(this.vigilanciaVeterinaria.getVigilanciaBovinosEBubalinos().getQuantidadeInspecionados05_12_F() == null) &&
				
				(this.vigilanciaVeterinaria.getVigilanciaBovinosEBubalinos().getQuantidadeInspecionados05_12_M() == null) &&
				
				(this.vigilanciaVeterinaria.getVigilanciaBovinosEBubalinos().getQuantidadeInspecionados13_24_F() == null) &&
				
				(this.vigilanciaVeterinaria.getVigilanciaBovinosEBubalinos().getQuantidadeInspecionados13_24_M() == null) &&
				
				(this.vigilanciaVeterinaria.getVigilanciaBovinosEBubalinos().getQuantidadeInspecionados25_36_F() == null) &&
				
				(this.vigilanciaVeterinaria.getVigilanciaBovinosEBubalinos().getQuantidadeInspecionados25_36_M() == null) &&
				
				(this.vigilanciaVeterinaria.getVigilanciaBovinosEBubalinos().getQuantidadeInspecionadosAcima36_F() == null) &&
				
				(this.vigilanciaVeterinaria.getVigilanciaBovinosEBubalinos().getQuantidadeInspecionadosAcima36_M() == null)){
				
				this.inspecaoBovinosEBubalinos = SimNao.NAO;
			} else
				this.inspecaoBovinosEBubalinos = SimNao.SIM;
		} else
			this.investigacaoBovinosEBubalinos = SimNao.NAO;
		
		if (this.vigilanciaVeterinaria.getVigilanciaAlimentosRuminantes() != null)
			this.investigacaoDeAlimentosFornecidosARuminantes = SimNao.SIM;
		else
			this.investigacaoDeAlimentosFornecidosARuminantes = SimNao.NAO;
		
		if (this.vigilanciaVeterinaria.getVigilanciaOutrasEspecies() != null){
			this.investigacaoOutrasEspecies = SimNao.SIM;
			
			if ((this.vigilanciaVeterinaria.getVigilanciaOutrasEspecies().getQuantidadeAsininosInspecionados_F() == null) &&
				
				(this.vigilanciaVeterinaria.getVigilanciaOutrasEspecies().getQuantidadeAsininosInspecionados_M() == null) &&
				
				(this.vigilanciaVeterinaria.getVigilanciaOutrasEspecies().getQuantidadeCaprinosInspecionados_F() == null) &&
				
				(this.vigilanciaVeterinaria.getVigilanciaOutrasEspecies().getQuantidadeCaprinosInspecionados_M() == null) &&
				
				(this.vigilanciaVeterinaria.getVigilanciaOutrasEspecies().getQuantidadeEquinosInspecionados_F() == null) &&
				
				(this.vigilanciaVeterinaria.getVigilanciaOutrasEspecies().getQuantidadeEquinosInspecionados_M() == null) &&
				
				(this.vigilanciaVeterinaria.getVigilanciaOutrasEspecies().getQuantidadeMuaresInspecionados_F() == null) &&
				
				(this.vigilanciaVeterinaria.getVigilanciaOutrasEspecies().getQuantidadeMuaresInspecionados_M() == null) &&
				
				(this.vigilanciaVeterinaria.getVigilanciaOutrasEspecies().getQuantidadeOvinosInspecionados_F() == null) &&
				
				(this.vigilanciaVeterinaria.getVigilanciaOutrasEspecies().getQuantidadeOvinosInspecionados_F() == null) &&
				
				(this.vigilanciaVeterinaria.getVigilanciaOutrasEspecies().getQuantidadePeixesInspecionados() == null)){
					
					this.inspecaoOutrasEspecies = SimNao.NAO;
				} else
					this.inspecaoOutrasEspecies = SimNao.SIM;
		} else
			this.investigacaoOutrasEspecies = SimNao.NAO;
		
		if (this.vigilanciaVeterinaria.getVigilanciaSuideos() != null){
			this.investigacaoSuideos = SimNao.SIM;
			
			if ((this.vigilanciaVeterinaria.getVigilanciaSuideos().getQuantidadeInspecionadosAte_30_F() == null) &&
				
				(this.vigilanciaVeterinaria.getVigilanciaSuideos().getQuantidadeInspecionadosAte_30_M() == null) &&
				
				(this.vigilanciaVeterinaria.getVigilanciaSuideos().getQuantidadeInspecionados31_60_F() == null) &&
				
				(this.vigilanciaVeterinaria.getVigilanciaSuideos().getQuantidadeInspecionados31_60_M() == null) &&
				
				(this.vigilanciaVeterinaria.getVigilanciaSuideos().getQuantidadeInspecionados61_180_F() == null) &&
				
				(this.vigilanciaVeterinaria.getVigilanciaSuideos().getQuantidadeInspecionados61_180_M() == null) &&
				
				(this.vigilanciaVeterinaria.getVigilanciaSuideos().getQuantidadeInspecionadosAcima180_F() == null) &&
				
				(this.vigilanciaVeterinaria.getVigilanciaSuideos().getQuantidadeInspecionadosAcima180_M() == null)){
				
				this.inspecaoSuideos = SimNao.NAO;
			} else
				this.inspecaoSuideos = SimNao.SIM;
		}else
			this.investigacaoSuideos = SimNao.NAO;
	}
	
	public String adicionar() throws IOException{
		boolean isVigilanciaVeterinariaOK = true;
		
		if (this.vigilanciaVeterinaria.getPropriedade() == null){
			FacesContext context = FacesContext.getCurrentInstance();
			
			context.addMessage("cadastro:fieldPropriedade:campoPropriedade", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Propriedade: valor � obrigat�rio.", "Propriedade: valor � obrigat�rio."));
			context.addMessage("cadastro:fieldProprietario:campoProprietario", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Propriet�rio: valor � obrigat�rio.", "Propriet�rio: valor � obrigat�rio."));
			isVigilanciaVeterinariaOK = false;
		}else {
			if (this.vigilanciaVeterinaria.getPropriedade() != null){
				if (this.vigilanciaVeterinaria.getProprietario() == null || this.vigilanciaVeterinaria.getProprietario().equals("")){
					FacesContext context = FacesContext.getCurrentInstance();
					
					context.addMessage("cadastro:fieldProprietario:campoProprietario", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Propriet�rio: valor � obrigat�rio.", "Propriet�rio: valor � obrigat�rio."));
					isVigilanciaVeterinariaOK = false;
				}
			}
			
			if (this.vigilanciaVeterinaria.getPropriedade() != null){
				if (this.vigilanciaVeterinaria.getPropriedade().getCoordenadaGeografica() == null || 
						this.vigilanciaVeterinaria.getPropriedade().getCoordenadaGeografica().getLatGrau() == null ||
						this.vigilanciaVeterinaria.getPropriedade().getCoordenadaGeografica().getLatMin() == null ||
						this.vigilanciaVeterinaria.getPropriedade().getCoordenadaGeografica().getLatSeg() == null){
					FacesContext context = FacesContext.getCurrentInstance();
					
					context.addMessage("cadastro:fieldPropriedadeLatitude:campoPropriedadeLatitude", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Coordenadas - Latitude: valor � obrigat�rio.", "O valor deve estar corretamente preenchido no SINDESA."));
					isVigilanciaVeterinariaOK = false;
				}
			}
			
			if (this.vigilanciaVeterinaria.getPropriedade() != null){
				if (this.vigilanciaVeterinaria.getPropriedade().getCoordenadaGeografica() == null || 
						this.vigilanciaVeterinaria.getPropriedade().getCoordenadaGeografica().getLongGrau() == null ||
						this.vigilanciaVeterinaria.getPropriedade().getCoordenadaGeografica().getLongMin() == null ||
						this.vigilanciaVeterinaria.getPropriedade().getCoordenadaGeografica().getLongSeg() == null){
					FacesContext context = FacesContext.getCurrentInstance();
					
					context.addMessage("cadastro:fieldPropriedadeLongitude:campoPropriedadeLongitude", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Coordenadas - Latitude: valor � obrigat�rio.", "O valor deve estar corretamente preenchido no SINDESA."));
					isVigilanciaVeterinariaOK = false;
				}
			}
		}
		
		if (this.vigilanciaVeterinaria.getVeterinario() == null){
			FacesContext context = FacesContext.getCurrentInstance();
			
			context.addMessage("cadastro:fieldVeterinario:campoVeterinario", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Veterin�rio: valor � obrigat�rio.", "Veterin�rio: valor � obrigat�rio."));
			isVigilanciaVeterinariaOK = false;
		}
		
		if (this.vigilanciaVeterinaria.getMotivosVigilanciaVeterinaria() == null || 
			this.vigilanciaVeterinaria.getMotivosVigilanciaVeterinaria().getListaTipoChavePrincipalTermoFiscalizacao() == null ||
			this.vigilanciaVeterinaria.getMotivosVigilanciaVeterinaria().getListaTipoChavePrincipalTermoFiscalizacao().isEmpty()){
			
			FacesContext context = FacesContext.getCurrentInstance();
			
			context.addMessage("cadastro:fieldPrincipalMotivo:campoPrincipalMotivo", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Principais motivo da visita: valor � obrigat�rio.", "Principais motivo da visita: valor � obrigat�rio."));
			isVigilanciaVeterinariaOK = false;
		}
		
		if (!isVigilanciaVeterinariaOK)
			return "";
		
		if (this.dataVigilancia!= null){
			if (this.vigilanciaVeterinaria.getDataVigilancia() == null)
				this.vigilanciaVeterinaria.setDataVigilancia(Calendar.getInstance());
			this.vigilanciaVeterinaria.getDataVigilancia().setTime(dataVigilancia);
		}else
			this.vigilanciaVeterinaria.setDataVigilancia(null);
		
		if (vigilanciaVeterinaria.getId() != null){
			this.vigilanciaVeterinariaService.saveOrUpdate(vigilanciaVeterinaria);

			FacesMessageUtil.addInfoContextFacesMessage("Vigilancia veterinaria n� " + this.vigilanciaVeterinaria.getNumero() + " atualizada", "");
		}else{
			this.vigilanciaVeterinaria.setDataCadastro(Calendar.getInstance());
			this.vigilanciaVeterinariaService.saveOrUpdate(vigilanciaVeterinaria);

			FacesMessageUtil.addInfoContextFacesMessage("Vigilancia veterinaria n� " + this.vigilanciaVeterinaria.getNumero() + " adicionada", "");
		}
		
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		request.getSession().setAttribute("numeroVigilanciaVeterinaria", vigilanciaVeterinaria.getNumero());
		
		this.limpar();
		return "pretty:pesquisarVigilanciaVeterinaria";
	}
	
	public void remover(VigilanciaVeterinaria vigilanciaVeterinaria){
		this.vigilanciaVeterinariaService.delete(vigilanciaVeterinaria);
		FacesMessageUtil.addInfoContextFacesMessage("Vigilancia veterinaria n� " + vigilanciaVeterinaria.getNumero() + " exclu�da com sucesso", "");
	}
	
	public List<TipoChavePrincipalVisitaPropriedadeRural> getListaTipoChavePrincipalVisitaPropriedadeRural(){
		return this.tipoChavePrincipalVisitaPropriedadeRuralService.findAllByStatus(AtivoInativo.ATIVO);
	}
	
	public void abrirTelaDeBuscaDePropriedade(){
		this.idPropriedade = null;
		this.listaPropriedades = null;
	}
	
	public void buscarPropriedade(){
		this.listaPropriedades = null;
		
		Propriedade p;
		try {
			p = propriedadeService.findByCodigoFetchAll(idPropriedade, forceUpdatePropriedade);
			
			if (p != null){
				this.listaPropriedades = new ArrayList<Propriedade>();
				listaPropriedades.add(p);
			}
		} catch (ApplicationException e) {
			FacesMessageUtil.addErrorContextFacesMessage("Erro", e.getLocalizedMessage());
			return;
		}
		
		if (this.listaPropriedades == null || this.listaPropriedades.isEmpty())
			FacesMessageUtil.addWarnContextFacesMessage("Nenhuma propriedade foi encontrada", "");
	}
	
	public void selecionarPropriedade(Propriedade propriedade){
		this.vigilanciaVeterinaria.setPropriedade(propriedade);
		this.vigilanciaVeterinaria.setNomePropriedade(propriedade.getNome());
		this.vigilanciaVeterinaria.setCodigoPropriedade(propriedade.getCodigoPropriedade());
		FacesMessageUtil.addInfoContextFacesMessage("Propriedade " + this.vigilanciaVeterinaria.getPropriedade().getNome() + " selecionada", "");
	}
	
	public List<Produtor> getListaProdutoresDaPropriedade(){
		HashSet<Produtor> set = new HashSet<Produtor>();
		
		if (this.vigilanciaVeterinaria.getPropriedade() != null){
			if (this.vigilanciaVeterinaria.getPropriedade().getProprietarios() != null)
				set.addAll(this.vigilanciaVeterinaria.getPropriedade().getProprietarios());
			
			if (this.vigilanciaVeterinaria.getPropriedade().getExploracaos() != null)
				for (int i = 0; i < this.vigilanciaVeterinaria.getPropriedade().getExploracaos().size(); i++) {
					if (this.vigilanciaVeterinaria.getPropriedade().getExploracaos().get(i).getProdutores() != null)
						set.addAll(this.vigilanciaVeterinaria.getPropriedade().getExploracaos().get(i).getProdutores());
				}
		}
		
		List<Produtor> lista = new ArrayList<Produtor>(set);
		return lista;
	}
	
	public void selecionarProdutor(Produtor produtor) throws CloneNotSupportedException{
		this.vigilanciaVeterinaria.setProprietario((Produtor) produtor.clone());
		
		FacesMessageUtil.addInfoContextFacesMessage("Propriet�rio " + this.vigilanciaVeterinaria.getProprietario().getNome() + " selecionado", "");
	}
	
	public void abrirTelaDeBuscaDeVeterinario(){
		this.cpfVeterinario = null;
		this.crmvVeterinario = null;
		this.listaVeterinarios = null;
	}
	
	public void buscarVeterinarioPorCpf(){
		this.listaVeterinarios = null;
		
		Veterinario v;
		try {
			v = clientWebServiceVeterinario.getVeterinarioByCpf(cpfVeterinario);
			
			if (v != null){
				this.listaVeterinarios = new ArrayList<Veterinario>();
				this.listaVeterinarios.add(v);
			}
		} catch (ApplicationException e) {
			FacesMessageUtil.addErrorContextFacesMessage("Erro", e.getLocalizedMessage());
			return;
		}
		
		if (this.listaVeterinarios == null || this.listaVeterinarios.isEmpty())
			FacesMessageUtil.addWarnContextFacesMessage("Nenhum veterin�rio foi encontrado", "");
	}
	
	public void buscarVeterinarioPorCrmv(){
		this.listaVeterinarios = null;
		
		Veterinario v;
		try {
			v = clientWebServiceVeterinario.getVeterinarioByCRMV(crmvVeterinario);
			
			if (v != null){
				this.listaVeterinarios = new ArrayList<Veterinario>();
				this.listaVeterinarios.add(v);
			}
		} catch (ApplicationException e) {
			FacesMessageUtil.addErrorContextFacesMessage("Erro", e.getLocalizedMessage());
			return;
		}
		
		if (this.listaVeterinarios == null || this.listaVeterinarios.isEmpty())
			FacesMessageUtil.addWarnContextFacesMessage("Nenhum veterin�rio foi encontrado", "");
	}
	
	public void selecionarVeterinario(Veterinario veterinario){
		this.vigilanciaVeterinaria.setVeterinario(veterinario);
		FacesMessageUtil.addInfoContextFacesMessage("Veterinario " + this.vigilanciaVeterinaria.getVeterinario().getNome() + " selecionado", "");
	}
	
	public void adicionarVigilanciaAves(){
		this.vigilanciaAves.setVigilanciaVeterinaria(vigilanciaVeterinaria);

		if (!editandoVigilanciaAves)
			this.vigilanciaVeterinaria.getListaVigilanciaAves().add(vigilanciaAves);
		else
			editandoVigilanciaAves = false;
		
		this.vigilanciaAves = new VigilanciaAves();
		
		FacesMessageUtil.addInfoContextFacesMessage("Registro inclu�do com sucesso", "");
	}
	
	public void novaVigilanciaAves(){
		this.vigilanciaAves= new VigilanciaAves();
		this.editandoVigilanciaAves = false;
		this.visualizandoVigilanciaAves = false;
	}
	
	public void removerVigilanciaAves(VigilanciaAves vigilanciaAves){
		this.vigilanciaVeterinaria.getListaVigilanciaAves().remove(vigilanciaAves);
	}
	
	public void editarVigilanciaAves(VigilanciaAves vigilanciaAves){
		this.editandoVigilanciaAves = true;
		this.visualizandoVigilanciaAves = false;
		this.vigilanciaAves = vigilanciaAves;
	}
	
	public void visualizarVigilanciaAves(VigilanciaAves vigilanciaAves){
		this.visualizandoVigilanciaAves = true;
		this.editandoVigilanciaAves = false;
		this.vigilanciaAves = vigilanciaAves;
	}
	
	public void adicionarInvestigacaoEpidemiologica(){
		this.investigacaoEpidemiologica.setVigilanciaVeterinaria(vigilanciaVeterinaria);

		if (this.dataUltimoCasoDoencaDeInvestigacao!= null){
			if (this.investigacaoEpidemiologica.getData() == null)
				this.investigacaoEpidemiologica.setData(Calendar.getInstance());
			this.investigacaoEpidemiologica.getData().setTime(dataUltimoCasoDoencaDeInvestigacao);
		}else
			this.investigacaoEpidemiologica.setData(null);
		
		if (!editandoInvestigacaoEpidemiologica)
			this.vigilanciaVeterinaria.getListaInvestigacaoEpidemiologica().add(investigacaoEpidemiologica);
		else
			editandoInvestigacaoEpidemiologica = false;
		
		this.investigacaoEpidemiologica = new InvestigacaoEpidemiologica();
		
		FacesMessageUtil.addInfoContextFacesMessage("Investiga��o inclu�da com sucesso", "");
	}
	
	public void novaInvestigacaoEpidemiologica(){
		this.investigacaoEpidemiologica= new InvestigacaoEpidemiologica();
		this.editandoInvestigacaoEpidemiologica = false;
		this.visualizandoInvestigacaoEpidemiologica = false;
	}
	
	public void removerInvestigacaoEpidemiologica(InvestigacaoEpidemiologica investigacaoEpidemiologica){
		this.vigilanciaVeterinaria.getListaInvestigacaoEpidemiologica().remove(investigacaoEpidemiologica);
	}
	
	public void editarInvestigacaoEpidemiologica(InvestigacaoEpidemiologica investigacaoEpidemiologica){
		this.editandoInvestigacaoEpidemiologica = true;
		this.visualizandoInvestigacaoEpidemiologica = false;
		this.investigacaoEpidemiologica = investigacaoEpidemiologica;
		
		if (this.investigacaoEpidemiologica.getData() != null)
			this.dataUltimoCasoDoencaDeInvestigacao = this.investigacaoEpidemiologica.getData().getTime();
	}
	
	public void visualizarInvestigacaoEpidemiologica(InvestigacaoEpidemiologica investigacaoEpidemiologica){
		this.visualizandoInvestigacaoEpidemiologica = true;
		this.editandoInvestigacaoEpidemiologica = false;
		this.investigacaoEpidemiologica = investigacaoEpidemiologica;
	}
	
	public void adicionarDoenca(){
		if (!editandoDoenca)
			this.investigacaoEpidemiologica.getListaDoencas().add(doenca);
		else
			editandoDoenca = false;
		
		this.doenca = new Doenca();
		
		FacesMessageUtil.addInfoContextFacesMessage("Investiga��o inclu�da com sucesso", "");
	}
	
	public void novaDoenca(){
		this.doenca= new Doenca();
		this.editandoDoenca = false;
		this.visualizandoDoenca = false;
	}
	
	public void removerDoenca(Doenca doenca){
		this.investigacaoEpidemiologica.getListaDoencas().remove(doenca);
	}
	
	public void editarDoenca(Doenca doenca){
		this.editandoDoenca = true;
		this.visualizandoDoenca = false;
		this.doenca = doenca;
	}
	
	public void visualizarDoenca(Doenca doenca){
		this.visualizandoDoenca = true;
		this.editandoDoenca = false;
		this.doenca = doenca;
	}
	
	public boolean haPontoDeRiscoProximoAPropriedade(){
		boolean resultado = false;
		
		if (this.pontoDeRiscoProximoAPropriedade == null)
			return false;
		
		resultado = this.pontoDeRiscoProximoAPropriedade.equals(SimNao.SIM);;
		
		if (!resultado){
			this.vigilanciaVeterinaria.setPontosDeRisco(null);
		}else{
			if (this.vigilanciaVeterinaria.getPontosDeRisco() == null)
				this.vigilanciaVeterinaria.setPontosDeRisco(new ArrayList<PontosDeRisco>());
			
		}
		
		return resultado;
	}
	
	public boolean isOutroTipoAlimentacaoSuideos(){
		boolean resultado = false;
		if (this.vigilanciaVeterinaria.getVigilanciaSuideos().getListaTipoAlimentoSuideos() == null){
			this.vigilanciaVeterinaria.getVigilanciaSuideos().setOutraAlimentacaoSuideos(null);
			return resultado;
		}else
			resultado = this.vigilanciaVeterinaria.getVigilanciaSuideos().getListaTipoAlimentoSuideos().contains(TipoAlimentoSuideos.OUTRO);
		
		if (!resultado)
			this.vigilanciaVeterinaria.getVigilanciaSuideos().setOutraAlimentacaoSuideos(null);
		
		return resultado;
	}
	
	public boolean isOutroTipoOrigemSuideos(){
		boolean resultado = false;
		if (this.vigilanciaVeterinaria.getVigilanciaSuideos().getListaTipoOrigemSuideos() == null){
			this.vigilanciaVeterinaria.getVigilanciaSuideos().setOutraOrigemSuideos(null);
			return resultado;
		}else
			resultado = this.vigilanciaVeterinaria.getVigilanciaSuideos().getListaTipoOrigemSuideos().contains(TipoOrigemSuideos.OUTRO);
		
		if (!resultado)
			this.vigilanciaVeterinaria.getVigilanciaSuideos().setOutraOrigemSuideos(null);
		
		return resultado;
	}
	
	public boolean isOutroTipoDestinoSuideos(){
		boolean resultado = false;
		if (this.vigilanciaVeterinaria.getVigilanciaSuideos().getTipoDestinoSuideos() == null)
			return resultado;
		else
			resultado = this.vigilanciaVeterinaria.getVigilanciaSuideos().getTipoDestinoSuideos().equals(TipoDestinoSuideos.OUTRO);
		
		if (!resultado)
			this.vigilanciaVeterinaria.getVigilanciaSuideos().setOutroDestinoSuideos(null);
		
		return resultado;
	}
	
	public boolean isHistoricoDeSugadurasDeMorcegosNaPropriedade(){
		boolean resultado = false;
		if (this.vigilanciaVeterinaria.getHistoricoDeSugadurasDeMorcegos() == null)
			return resultado;
		else
			resultado = this.vigilanciaVeterinaria.getHistoricoDeSugadurasDeMorcegos().equals(SimNao.SIM);
		
		if (!resultado){
			this.vigilanciaVeterinaria.setQuantidadeSugadurasEmAves(null);
			this.vigilanciaVeterinaria.setQuantidadeSugadurasEmBovinos(null);
			this.vigilanciaVeterinaria.setQuantidadeSugadurasEmBubalinos(null);
			this.vigilanciaVeterinaria.setQuantidadeSugadurasEmCaprinos(null);
			this.vigilanciaVeterinaria.setQuantidadeSugadurasEmEquideos(null);
			this.vigilanciaVeterinaria.setQuantidadeSugadurasEmSuideos(null);
		}
		
		return resultado;
	}
	
	public boolean isMotivoVigilanciaIgualAPrevencaoEControleDasEncefalopatiasEspongiformesTransmissiveis(){
		if (this.vigilanciaVeterinaria.getMotivosVigilanciaVeterinaria() == null)
			return false;
		
		if (this.vigilanciaVeterinaria.getMotivosVigilanciaVeterinaria().getListaTipoChavePrincipalTermoFiscalizacao() == null || this.vigilanciaVeterinaria.getMotivosVigilanciaVeterinaria().getListaTipoChavePrincipalTermoFiscalizacao().isEmpty())
			return false;
		
		for (TipoChavePrincipalVisitaPropriedadeRural tipoChave : (this.vigilanciaVeterinaria.getMotivosVigilanciaVeterinaria().getListaTipoChavePrincipalTermoFiscalizacao())) {
			if (tipoChave.getNome().equals("PREVEN��O E CONTROLE DAS ENCEFALOPATIAS ESPONGIFORMES TRANSMISS�VEIS")){
				this.investigacaoDeAlimentosFornecidosARuminantes = SimNao.SIM;
				return true;
			}
		}
		
		return false;
	}
	
	public boolean isMotivoVigilanciaIgualASanidadeSuideos(){
		if (this.vigilanciaVeterinaria.getMotivosVigilanciaVeterinaria() == null)
			return false;
		
		if (this.vigilanciaVeterinaria.getMotivosVigilanciaVeterinaria().getListaTipoChavePrincipalTermoFiscalizacao() == null || this.vigilanciaVeterinaria.getMotivosVigilanciaVeterinaria().getListaTipoChavePrincipalTermoFiscalizacao().isEmpty())
			return false;
		
		for (TipoChavePrincipalVisitaPropriedadeRural tipoChave : (this.vigilanciaVeterinaria.getMotivosVigilanciaVeterinaria().getListaTipoChavePrincipalTermoFiscalizacao())) {
			if (tipoChave.getNome().equals("SANIDADE SU�DEA")){
				this.investigacaoSuideos = SimNao.SIM;
				return true;
			} 
		}
		
		return false;
	}
	
	public void changeMotivoVigilancia(AjaxBehaviorEvent e){
		if (this.vigilanciaVeterinaria.getMotivosVigilanciaVeterinaria() != null)
		
			if (this.vigilanciaVeterinaria.getMotivosVigilanciaVeterinaria().getListaTipoChavePrincipalTermoFiscalizacao() != null && !this.vigilanciaVeterinaria.getMotivosVigilanciaVeterinaria().getListaTipoChavePrincipalTermoFiscalizacao().isEmpty())
		
				for (TipoChavePrincipalVisitaPropriedadeRural tipoChave : (this.vigilanciaVeterinaria.getMotivosVigilanciaVeterinaria().getListaTipoChavePrincipalTermoFiscalizacao())) {
					if (tipoChave.getNome().equals("PREVEN��O E CONTROLE DAS ENCEFALOPATIAS ESPONGIFORMES TRANSMISS�VEIS")){
						this.investigacaoDeAlimentosFornecidosARuminantes = SimNao.SIM;
						
						if (this.vigilanciaVeterinaria.getVigilanciaAlimentosRuminantes() == null)
							RequestContext.getCurrentInstance().update("cadastro:panelExternoInvestigacaoAlimentosRuminantes");
					}
					
					if (tipoChave.getNome().equals("SANIDADE SU�DEA")){
						this.investigacaoSuideos = SimNao.SIM;
						
						if (this.vigilanciaVeterinaria.getVigilanciaSuideos() == null)
							RequestContext.getCurrentInstance().update("cadastro:panelExternoInvestigacaoSuideos");
					}
				}
	}
	
	public boolean houveInvestigacaoDeAlimentosFornecidosARuminantes(){
		boolean resultado = false;
		
		if (this.investigacaoDeAlimentosFornecidosARuminantes == null)
			return false;
		
		resultado = this.investigacaoDeAlimentosFornecidosARuminantes.equals(SimNao.SIM);;
		
		if (!resultado){
			this.vigilanciaVeterinaria.setVigilanciaAlimentosRuminantes(null);
		}else{
			if (this.vigilanciaVeterinaria.getVigilanciaAlimentosRuminantes() == null)
				this.vigilanciaVeterinaria.setVigilanciaAlimentosRuminantes(new VigilanciaAlimentosRuminantes());
			
			//TODO this.vigilanciaVeterinaria.set criacao avicola em sistema industrial
			
		}
		
		return resultado;
	}
	
	public boolean houveInvestigacaoBovinosEBubalinos(){
		boolean resultado = false;
		
		if (this.investigacaoBovinosEBubalinos == null)
			return false;
		
		resultado = this.investigacaoBovinosEBubalinos.equals(SimNao.SIM);;
		
		if (!resultado){
			this.vigilanciaVeterinaria.setVigilanciaBovinosEBubalinos(null);
		}else{
			if (this.vigilanciaVeterinaria.getVigilanciaBovinosEBubalinos() == null)
				this.vigilanciaVeterinaria.setVigilanciaBovinosEBubalinos(new VigilanciaBovinosEBubalinos());
		}	
		
		return resultado;
	}
	
	public boolean houveInspecaoBovinosEBubalinos(){
		boolean resultado = false;
		
		if (this.inspecaoBovinosEBubalinos == null)
			return false;
		
		resultado = this.inspecaoBovinosEBubalinos.equals(SimNao.SIM);;
		
		if (!resultado){
			if (this.vigilanciaVeterinaria.getVigilanciaBovinosEBubalinos() != null){
				this.vigilanciaVeterinaria.getVigilanciaBovinosEBubalinos().setQuantidadeInspecionados00_04_F(null);
				this.vigilanciaVeterinaria.getVigilanciaBovinosEBubalinos().setQuantidadeInspecionados00_04_M(null);
				this.vigilanciaVeterinaria.getVigilanciaBovinosEBubalinos().setQuantidadeInspecionados05_12_F(null);
				this.vigilanciaVeterinaria.getVigilanciaBovinosEBubalinos().setQuantidadeInspecionados05_12_M(null);
				this.vigilanciaVeterinaria.getVigilanciaBovinosEBubalinos().setQuantidadeInspecionados13_24_F(null);
				this.vigilanciaVeterinaria.getVigilanciaBovinosEBubalinos().setQuantidadeInspecionados13_24_M(null);
				this.vigilanciaVeterinaria.getVigilanciaBovinosEBubalinos().setQuantidadeInspecionados25_36_F(null);
				this.vigilanciaVeterinaria.getVigilanciaBovinosEBubalinos().setQuantidadeInspecionados25_36_M(null);
				this.vigilanciaVeterinaria.getVigilanciaBovinosEBubalinos().setQuantidadeInspecionadosAcima36_F(null);
				this.vigilanciaVeterinaria.getVigilanciaBovinosEBubalinos().setQuantidadeInspecionadosAcima36_M(null);
			}
		}
		
		return resultado;
	}
	
	public boolean houveInvestigacaoSuideos(){
		boolean resultado = false;
		
		if (this.investigacaoSuideos == null)
			return false;
		
		resultado = this.investigacaoSuideos.equals(SimNao.SIM);;
		
		if (!resultado){
			this.vigilanciaVeterinaria.setVigilanciaSuideos(null);
		}else{
			if (this.vigilanciaVeterinaria.getVigilanciaSuideos() == null)
				this.vigilanciaVeterinaria.setVigilanciaSuideos(new VigilanciaSuideos());
		}	
		
		return resultado;
	}
	
	public boolean houveInspecaoSuideos(){
		boolean resultado = false;
		
		if (this.inspecaoSuideos == null)
			return false;
		
		resultado = this.inspecaoSuideos.equals(SimNao.SIM);;
		
		if (!resultado){
			if (this.vigilanciaVeterinaria.getVigilanciaSuideos() != null){
				this.vigilanciaVeterinaria.getVigilanciaSuideos().setQuantidadeInspecionadosAte_30_F(null);
				this.vigilanciaVeterinaria.getVigilanciaSuideos().setQuantidadeInspecionadosAte_30_M(null);
				this.vigilanciaVeterinaria.getVigilanciaSuideos().setQuantidadeInspecionados31_60_F(null);
				this.vigilanciaVeterinaria.getVigilanciaSuideos().setQuantidadeInspecionados31_60_M(null);
				this.vigilanciaVeterinaria.getVigilanciaSuideos().setQuantidadeInspecionados61_180_F(null);
				this.vigilanciaVeterinaria.getVigilanciaSuideos().setQuantidadeInspecionados61_180_M(null);
				this.vigilanciaVeterinaria.getVigilanciaSuideos().setQuantidadeInspecionadosAcima180_F(null);
				this.vigilanciaVeterinaria.getVigilanciaSuideos().setQuantidadeInspecionadosAcima180_M(null);
			}
		}
		
		return resultado;
	}
	
	public boolean houveInvestigacaoOutrasEspecies(){
		boolean resultado = false;
		
		if (this.investigacaoOutrasEspecies == null)
			return false;
		
		resultado = this.investigacaoOutrasEspecies.equals(SimNao.SIM);;
		
		if (!resultado){
			this.vigilanciaVeterinaria.setVigilanciaOutrasEspecies(null);
		}else{
			if (this.vigilanciaVeterinaria.getVigilanciaOutrasEspecies() == null)
				this.vigilanciaVeterinaria.setVigilanciaOutrasEspecies(new VigilanciaOutrasEspecies());
		}	
		
		return resultado;
	}
	
	public boolean houveInspecaoOutrasEspecies(){
		boolean resultado = false;
		
		if (this.inspecaoOutrasEspecies == null)
			return false;
		
		resultado = this.inspecaoOutrasEspecies.equals(SimNao.SIM);;
		
		if (!resultado){
			if (this.vigilanciaVeterinaria.getVigilanciaOutrasEspecies() != null){
				this.vigilanciaVeterinaria.getVigilanciaOutrasEspecies().setQuantidadeAsininosInspecionados_F(null);
				this.vigilanciaVeterinaria.getVigilanciaOutrasEspecies().setQuantidadeAsininosInspecionados_M(null);
				this.vigilanciaVeterinaria.getVigilanciaOutrasEspecies().setQuantidadeCaprinosInspecionados_F(null);
				this.vigilanciaVeterinaria.getVigilanciaOutrasEspecies().setQuantidadeCaprinosInspecionados_M(null);
				this.vigilanciaVeterinaria.getVigilanciaOutrasEspecies().setQuantidadeEquinosInspecionados_F(null);
				this.vigilanciaVeterinaria.getVigilanciaOutrasEspecies().setQuantidadeEquinosInspecionados_M(null);
				this.vigilanciaVeterinaria.getVigilanciaOutrasEspecies().setQuantidadeMuaresInspecionados_F(null);
				this.vigilanciaVeterinaria.getVigilanciaOutrasEspecies().setQuantidadeMuaresInspecionados_M(null);
				this.vigilanciaVeterinaria.getVigilanciaOutrasEspecies().setQuantidadeOvinosInspecionados_F(null);
				this.vigilanciaVeterinaria.getVigilanciaOutrasEspecies().setQuantidadeOvinosInspecionados_M(null);
				this.vigilanciaVeterinaria.getVigilanciaOutrasEspecies().setQuantidadePeixesInspecionados(null);
			}
		}
		
		return resultado;
	}
	
	public VigilanciaVeterinaria getVigilanciaVeterinaria() {
		return vigilanciaVeterinaria;
	}

	public void setVigilanciaVeterinaria(VigilanciaVeterinaria vigilanciaVeterinaria) {
		this.vigilanciaVeterinaria = vigilanciaVeterinaria;
	}

	public String getCpfVeterinario() {
		return cpfVeterinario;
	}

	public void setCpfVeterinario(String cpfVeterinario) {
		this.cpfVeterinario = cpfVeterinario;
	}

	public String getCrmvVeterinario() {
		return crmvVeterinario;
	}

	public void setCrmvVeterinario(String crmvVeterinario) {
		this.crmvVeterinario = crmvVeterinario;
	}

	public List<Veterinario> getListaVeterinarios() {
		return listaVeterinarios;
	}

	public void setListaVeterinarios(List<Veterinario> listaVeterinarios) {
		this.listaVeterinarios = listaVeterinarios;
	}

	public Date getDataVigilancia() {
		return dataVigilancia;
	}

	public void setDataVigilancia(Date dataVigilancia) {
		this.dataVigilancia = dataVigilancia;
	}

	public TipoChavePrincipalVisitaPropriedadeRural getTipoChavePrincipalVisitaPropriedadeRural() {
		return tipoChavePrincipalVisitaPropriedadeRural;
	}

	public void setTipoChavePrincipalVisitaPropriedadeRural(
			TipoChavePrincipalVisitaPropriedadeRural tipoChavePrincipalVisitaPropriedadeRural) {
		this.tipoChavePrincipalVisitaPropriedadeRural = tipoChavePrincipalVisitaPropriedadeRural;
	}

	public Long getIdPropriedade() {
		return idPropriedade;
	}

	public void setIdPropriedade(Long idPropriedade) {
		this.idPropriedade = idPropriedade;
	}

	public List<Propriedade> getListaPropriedades() {
		return listaPropriedades;
	}

	public void setListaPropriedades(List<Propriedade> listaPropriedades) {
		this.listaPropriedades = listaPropriedades;
	}

	public VigilanciaAves getVigilanciaAves() {
		return vigilanciaAves;
	}

	public void setVigilanciaAves(VigilanciaAves vigilanciaAves) {
		this.vigilanciaAves = vigilanciaAves;
	}

	public boolean isEditandoVigilanciaAves() {
		return editandoVigilanciaAves;
	}

	public void setEditandoVigilanciaAves(boolean editandoVigilanciaAves) {
		this.editandoVigilanciaAves = editandoVigilanciaAves;
	}

	public boolean isVisualizandoVigilanciaAves() {
		return visualizandoVigilanciaAves;
	}

	public void setVisualizandoVigilanciaAves(boolean visualizandoVigilanciaAves) {
		this.visualizandoVigilanciaAves = visualizandoVigilanciaAves;
	}

	public InvestigacaoEpidemiologica getInvestigacaoEpidemiologica() {
		return investigacaoEpidemiologica;
	}

	public void setInvestigacaoEpidemiologica(
			InvestigacaoEpidemiologica investigacaoEpidemiologica) {
		this.investigacaoEpidemiologica = investigacaoEpidemiologica;
	}

	public boolean isEditandoInvestigacaoEpidemiologica() {
		return editandoInvestigacaoEpidemiologica;
	}

	public void setEditandoInvestigacaoEpidemiologica(
			boolean editandoInvestigacaoEpidemiologica) {
		this.editandoInvestigacaoEpidemiologica = editandoInvestigacaoEpidemiologica;
	}

	public boolean isVisualizandoInvestigacaoEpidemiologica() {
		return visualizandoInvestigacaoEpidemiologica;
	}

	public void setVisualizandoInvestigacaoEpidemiologica(
			boolean visualizandoInvestigacaoEpidemiologica) {
		this.visualizandoInvestigacaoEpidemiologica = visualizandoInvestigacaoEpidemiologica;
	}

	public Date getDataUltimoCasoDoencaDeInvestigacao() {
		return dataUltimoCasoDoencaDeInvestigacao;
	}

	public void setDataUltimoCasoDoencaDeInvestigacao(Date dataUltimoCasoDoencaDeInvestigacao) {
		this.dataUltimoCasoDoencaDeInvestigacao = dataUltimoCasoDoencaDeInvestigacao;
	}

	public Doenca getDoenca() {
		return doenca;
	}

	public void setDoenca(Doenca doenca) {
		this.doenca = doenca;
	}

	public boolean isEditandoDoenca() {
		return editandoDoenca;
	}

	public void setEditandoDoenca(boolean editandoDoenca) {
		this.editandoDoenca = editandoDoenca;
	}

	public boolean isVisualizandoDoenca() {
		return visualizandoDoenca;
	}

	public void setVisualizandoDoenca(boolean visualizandoDoenca) {
		this.visualizandoDoenca = visualizandoDoenca;
	}

	public SimNao getInvestigacaoDeAlimentosFornecidosARuminantes() {
		return investigacaoDeAlimentosFornecidosARuminantes;
	}

	public void setInvestigacaoDeAlimentosFornecidosARuminantes(
			SimNao investigacaoDeAlimentosFornecidosARuminantes) {
		this.investigacaoDeAlimentosFornecidosARuminantes = investigacaoDeAlimentosFornecidosARuminantes;
	}

	public SimNao getInvestigacaoBovinosEBubalinos() {
		return investigacaoBovinosEBubalinos;
	}

	public void setInvestigacaoBovinosEBubalinos(
			SimNao investigacaoBovinosEBubalinos) {
		this.investigacaoBovinosEBubalinos = investigacaoBovinosEBubalinos;
	}

	public SimNao getInvestigacaoSuideos() {
		return investigacaoSuideos;
	}

	public void setInvestigacaoSuideos(SimNao investigacaoSuideos) {
		this.investigacaoSuideos = investigacaoSuideos;
	}

	public SimNao getInvestigacaoOutrasEspecies() {
		return investigacaoOutrasEspecies;
	}

	public void setInvestigacaoOutrasEspecies(SimNao investigacaoOutrasEspecies) {
		this.investigacaoOutrasEspecies = investigacaoOutrasEspecies;
	}

	public SimNao getPontoDeRiscoProximoAPropriedade() {
		return pontoDeRiscoProximoAPropriedade;
	}

	public void setPontoDeRiscoProximoAPropriedade(
			SimNao pontoDeRiscoProximoAPropriedade) {
		this.pontoDeRiscoProximoAPropriedade = pontoDeRiscoProximoAPropriedade;
	}

	public LazyObjectDataModel<VigilanciaVeterinaria> getListaVigilanciaVeterinaria() {
		return listaVigilanciaVeterinaria;
	}

	public void setListaVigilanciaVeterinaria(LazyObjectDataModel<VigilanciaVeterinaria> listaVigilanciaVeterinaria) {
		this.listaVigilanciaVeterinaria = listaVigilanciaVeterinaria;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public VigilanciaVeterinariaDTO getVigilanciaVeterinariaDTO() {
		return vigilanciaVeterinariaDTO;
	}

	public void setVigilanciaVeterinariaDTO(VigilanciaVeterinariaDTO vigilanciaVeterinariaDTO) {
		this.vigilanciaVeterinariaDTO = vigilanciaVeterinariaDTO;
	}

	public SimNao getInspecaoBovinosEBubalinos() {
		return inspecaoBovinosEBubalinos;
	}

	public void setInspecaoBovinosEBubalinos(SimNao inspecaoBovinosEBubalinos) {
		this.inspecaoBovinosEBubalinos = inspecaoBovinosEBubalinos;
	}

	public SimNao getInspecaoSuideos() {
		return inspecaoSuideos;
	}

	public void setInspecaoSuideos(SimNao inspecaoSuideos) {
		this.inspecaoSuideos = inspecaoSuideos;
	}

	public SimNao getInspecaoOutrasEspecies() {
		return inspecaoOutrasEspecies;
	}

	public void setInspecaoOutrasEspecies(SimNao inspecaoOutrasEspecies) {
		this.inspecaoOutrasEspecies = inspecaoOutrasEspecies;
	}

	public boolean isForceUpdatePropriedade() {
		return forceUpdatePropriedade;
	}

	public void setForceUpdatePropriedade(boolean forceUpdatePropriedade) {
		this.forceUpdatePropriedade = forceUpdatePropriedade;
	}

}
