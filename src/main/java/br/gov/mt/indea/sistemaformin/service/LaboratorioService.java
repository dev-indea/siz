package br.gov.mt.indea.sistemaformin.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.mt.indea.sistemaformin.entity.Laboratorio;
import br.gov.mt.indea.sistemaformin.util.StringUtil;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class LaboratorioService extends PaginableService<Laboratorio, Long> {
	
	private static final Logger log = LoggerFactory.getLogger(LaboratorioService.class);

	protected LaboratorioService() {
		super(Laboratorio.class);
	}
	
	public Laboratorio findByIdFetchAll(Long id){
		Laboratorio laboratorio;
		
		StringBuilder sql = new StringBuilder();
		sql.append("  from Laboratorio laboratorio ")
		   .append("  join fetch laboratorio.endereco endereco")
		   .append(" where laboratorio.id = :id ");
		
		Query query = getSession().createQuery(sql.toString()).setLong("id", id);
		laboratorio = (Laboratorio) query.uniqueResult();
		
		return laboratorio;
	}
	
	@SuppressWarnings("unchecked")
	public List<Laboratorio> findAll(String nome){
		StringBuilder sql = new StringBuilder();
		sql.append("  from Laboratorio laboratorio ")
		   .append("  join fetch laboratorio.endereco endereco")
		   .append(" where lower(remove_acento(laboratorio.nome)) like :nome ")
		   .append(" order by laboratorio.nome");
		
		Query query = getSession().createQuery(sql.toString()).setString("nome", '%' + StringUtil.removeAcentos(nome.toLowerCase() + '%'));
		
		return query.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Laboratorio> findAll(){
		StringBuilder sql = new StringBuilder();
		sql.append("  from Laboratorio laboratorio ")
		   .append("  join fetch laboratorio.endereco endereco")
		   .append(" order by laboratorio.nome");
		
		Query query = getSession().createQuery(sql.toString());
		
		return query.list();
	}
	
	@Override
	public void saveOrUpdate(Laboratorio laboratorio) {
		super.saveOrUpdate(laboratorio);
		
		log.info("Salvando Laboratorio {}", laboratorio.getId());
	}
	
	@Override
	public void delete(Laboratorio laboratorio) {
		super.delete(laboratorio);
		
		log.info("Removendo Laboratorio {}", laboratorio.getId());
	}
	
	@Override
	public void validar(Laboratorio Laboratorio) {

	}

	@Override
	public void validarPersist(Laboratorio Laboratorio) {

	}

	@Override
	public void validarMerge(Laboratorio Laboratorio) {

	}

	@Override
	public void validarDelete(Laboratorio Laboratorio) {

	}

}
