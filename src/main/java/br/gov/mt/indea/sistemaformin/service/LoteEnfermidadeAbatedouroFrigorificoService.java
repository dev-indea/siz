package br.gov.mt.indea.sistemaformin.service;

import java.util.Calendar;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Hibernate;
import org.hibernate.Query;

import br.gov.mt.indea.sistemaformin.entity.LoteEnfermidadeAbatedouroFrigorifico;
import br.gov.mt.indea.sistemaformin.entity.dto.AbstractDTO;
import br.gov.mt.indea.sistemaformin.entity.dto.EnfermidadeAbatedouroFrigorificoDTO;
import br.gov.mt.indea.sistemaformin.util.StringUtil;

@Stateless
public class LoteEnfermidadeAbatedouroFrigorificoService extends PaginableService<LoteEnfermidadeAbatedouroFrigorifico, Long>{

	protected LoteEnfermidadeAbatedouroFrigorificoService() {
		super(LoteEnfermidadeAbatedouroFrigorifico.class);
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public Long countAll(AbstractDTO dto) {
		StringBuilder sql = new StringBuilder();
		sql.append("select count(enfermidadeAbatedouroFrigorifico) ")
		   .append("  from EnfermidadeAbatedouroFrigorifico as enfermidadeAbatedouroFrigorifico")
		   .append("  left join enfermidadeAbatedouroFrigorifico.listaLoteEnfermidadeAbatedouroFrigorifico listaLoteEnfermidadeAbatedouroFrigorifico ")
		   .append("  left join listaLoteEnfermidadeAbatedouroFrigorifico.proprietario proprietario ")
		   .append("  left join proprietario.propriedade propriedade")
		   .append("  where 1 = 1 ");
		
		if (dto != null && !dto.isNull()){
			EnfermidadeAbatedouroFrigorificoDTO enfermidadeAbatedouroFrigorificoDTO = (EnfermidadeAbatedouroFrigorificoDTO) dto;
			if (enfermidadeAbatedouroFrigorificoDTO.getId() != null)
				sql.append("  and enfermidadeAbatedouroFrigorifico.id = :id");
			else {
				if (enfermidadeAbatedouroFrigorificoDTO.getDataDoAbate() != null)
					sql.append("  and to_char(enfermidadeAbatedouroFrigorifico.dataAbate, 'YYYY-MM-DD HH:MI:SS') between :inicio and :final");
				if (enfermidadeAbatedouroFrigorificoDTO.getMunicipio() != null)
					sql.append("  and propriedade.municipio = :municipio");
				if (enfermidadeAbatedouroFrigorificoDTO.getNomePropriedade() != null && !enfermidadeAbatedouroFrigorificoDTO.getNomePropriedade().equals(""))
					sql.append("  and lower(remove_acento(propriedade.nome)) like :nomePropriedade");
				if (enfermidadeAbatedouroFrigorificoDTO.getCodigoPropriedade() != null && !enfermidadeAbatedouroFrigorificoDTO.getCodigoPropriedade().equals(""))
					sql.append("  and propriedade.codigoPropriedade = :codigoPropriedade");
				if (enfermidadeAbatedouroFrigorificoDTO.getServicoDeInspecao() != null)
					sql.append("  and enfermidadeAbatedouroFrigorifico.servicoDeInspecao = :servicoDeInspecao");
			}
		}
		
		Query query = getSession().createQuery(sql.toString());
		
		if (dto != null && !dto.isNull()){
			EnfermidadeAbatedouroFrigorificoDTO enfermidadeAbatedouroFrigorificoDTO = (EnfermidadeAbatedouroFrigorificoDTO) dto;
			
			if (enfermidadeAbatedouroFrigorificoDTO.getId() != null)
				query.setParameter("id", enfermidadeAbatedouroFrigorificoDTO.getId());
			else {
				if (enfermidadeAbatedouroFrigorificoDTO.getMunicipio() != null)
					query.setParameter("municipio", enfermidadeAbatedouroFrigorificoDTO.getMunicipio());
				if (enfermidadeAbatedouroFrigorificoDTO.getNomePropriedade() != null && !enfermidadeAbatedouroFrigorificoDTO.getNomePropriedade().equals(""))
					query.setString("nomePropriedade", StringUtil.removeAcentos('%' + enfermidadeAbatedouroFrigorificoDTO.getNomePropriedade().toLowerCase()) + '%');
				if (enfermidadeAbatedouroFrigorificoDTO.getCodigoPropriedade() != null && !enfermidadeAbatedouroFrigorificoDTO.getCodigoPropriedade().equals(""))
					query.setLong("codigoPropriedade", enfermidadeAbatedouroFrigorificoDTO.getCodigoPropriedade());
				if (enfermidadeAbatedouroFrigorificoDTO.getServicoDeInspecao() != null)
					query.setParameter("servicoDeInspecao", enfermidadeAbatedouroFrigorificoDTO.getServicoDeInspecao());
				
				if (enfermidadeAbatedouroFrigorificoDTO.getDataDoAbate() != null){
					Calendar i = Calendar.getInstance();
					i.setTime(enfermidadeAbatedouroFrigorificoDTO.getDataDoAbate());
					
					StringBuilder sbI = new StringBuilder();
					sbI.append(i.get(Calendar.YEAR)).append("-")
					   .append(((i.get(Calendar.MONTH) + 1) + "").length() > 1 ? (i.get(Calendar.MONTH)+1) : "0" + (i.get(Calendar.MONTH)+1)).append("-")
					   .append((i.get(Calendar.DAY_OF_MONTH) + "").length() > 1 ? i.get(Calendar.DAY_OF_MONTH) : "0" + i.get(Calendar.DAY_OF_MONTH))
					   .append(" ")
					   .append("00").append(":")
					   .append("00").append(":")
					   .append("00");
					
					Calendar f = Calendar.getInstance();
					f.setTime(enfermidadeAbatedouroFrigorificoDTO.getDataDoAbate());
					
					StringBuilder sbF = new StringBuilder();
					sbF.append(f.get(Calendar.YEAR)).append("-")
					   .append(((f.get(Calendar.MONTH) + 1) + "").length() > 1 ? (f.get(Calendar.MONTH)+1) : "0" + (f.get(Calendar.MONTH)+1)).append("-")
					   .append((f.get(Calendar.DAY_OF_MONTH) + "").length() > 1 ? f.get(Calendar.DAY_OF_MONTH) : "0" + f.get(Calendar.DAY_OF_MONTH))
					   .append(" ")
					   .append("23").append(":")
					   .append("23").append(":")
					   .append("59");
					
					query.setString("inicio", sbI.toString());
					query.setString("final", sbF.toString());
				}
			}
		}

		return (Long) query.uniqueResult();
    }
	
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<LoteEnfermidadeAbatedouroFrigorifico> findAllComPaginacao(AbstractDTO dto, int first, int pageSize, String sortField, String sortOrder) {
		StringBuilder sql = new StringBuilder();
		sql.append("select distinct loteEnfermidadeAbatedouroFrigorifico")
		   .append("  from LoteEnfermidadeAbatedouroFrigorifico as loteEnfermidadeAbatedouroFrigorifico")
		   .append("  left join fetch loteEnfermidadeAbatedouroFrigorifico.enfermidadeAbatedouroFrigorifico enfermidadeAbatedouroFrigorifico ")
		   .append("  left join fetch loteEnfermidadeAbatedouroFrigorifico.proprietario proprietario ")
		   .append("  left join fetch proprietario.propriedade propriedade")
		   .append("  where 1 = 1 ");
		
		if (dto != null && !dto.isNull()){
			EnfermidadeAbatedouroFrigorificoDTO enfermidadeAbatedouroFrigorificoDTO = (EnfermidadeAbatedouroFrigorificoDTO) dto;
			if (enfermidadeAbatedouroFrigorificoDTO.getId() != null)
				sql.append("  and enfermidadeAbatedouroFrigorifico.id = :id");
			else {
				if (enfermidadeAbatedouroFrigorificoDTO.getDataDoAbate() != null)
					sql.append("  and to_char(enfermidadeAbatedouroFrigorifico.dataAbate, 'YYYY-MM-DD HH:MI:SS') between :inicio and :final");
				if (enfermidadeAbatedouroFrigorificoDTO.getMunicipio() != null)
					sql.append("  and propriedade.municipio = :municipio");
				if (enfermidadeAbatedouroFrigorificoDTO.getNomePropriedade() != null && !enfermidadeAbatedouroFrigorificoDTO.getNomePropriedade().equals(""))
					sql.append("  and lower(remove_acento(propriedade.nome)) like :nomePropriedade");
				if (enfermidadeAbatedouroFrigorificoDTO.getCodigoPropriedade() != null && !enfermidadeAbatedouroFrigorificoDTO.getCodigoPropriedade().equals(""))
					sql.append("  and propriedade.codigoPropriedade = :codigoPropriedade");
				if (enfermidadeAbatedouroFrigorificoDTO.getServicoDeInspecao() != null)
					sql.append("  and enfermidadeAbatedouroFrigorifico.servicoDeInspecao = :servicoDeInspecao");
			}
		}
		
		if (StringUtils.isNotBlank(sortField))
			sql.append(" order by enfermidadeAbatedouroFrigorifico." + sortField + " " + sortOrder);

		Query query = getSession().createQuery(sql.toString());
		
		if (dto != null && !dto.isNull()){
			EnfermidadeAbatedouroFrigorificoDTO enfermidadeAbatedouroFrigorificoDTO = (EnfermidadeAbatedouroFrigorificoDTO) dto;
			
			if (enfermidadeAbatedouroFrigorificoDTO.getId() != null)
				query.setParameter("id", enfermidadeAbatedouroFrigorificoDTO.getId());
			else {
				if (enfermidadeAbatedouroFrigorificoDTO.getMunicipio() != null)
					query.setParameter("municipio", enfermidadeAbatedouroFrigorificoDTO.getMunicipio());
				if (enfermidadeAbatedouroFrigorificoDTO.getNomePropriedade() != null && !enfermidadeAbatedouroFrigorificoDTO.getNomePropriedade().equals(""))
					query.setString("nomePropriedade", StringUtil.removeAcentos('%' + enfermidadeAbatedouroFrigorificoDTO.getNomePropriedade().toLowerCase()) + '%');
				if (enfermidadeAbatedouroFrigorificoDTO.getCodigoPropriedade() != null && !enfermidadeAbatedouroFrigorificoDTO.getCodigoPropriedade().equals(""))
					query.setLong("codigoPropriedade", enfermidadeAbatedouroFrigorificoDTO.getCodigoPropriedade());
				if (enfermidadeAbatedouroFrigorificoDTO.getServicoDeInspecao() != null)
					query.setParameter("servicoDeInspecao", enfermidadeAbatedouroFrigorificoDTO.getServicoDeInspecao());
				
				if (enfermidadeAbatedouroFrigorificoDTO.getDataDoAbate() != null){
					Calendar i = Calendar.getInstance();
					i.setTime(enfermidadeAbatedouroFrigorificoDTO.getDataDoAbate());
					
					StringBuilder sbI = new StringBuilder();
					sbI.append(i.get(Calendar.YEAR)).append("-")
					   .append(((i.get(Calendar.MONTH) + 1) + "").length() > 1 ? (i.get(Calendar.MONTH)+1) : "0" + (i.get(Calendar.MONTH)+1)).append("-")
					   .append((i.get(Calendar.DAY_OF_MONTH) + "").length() > 1 ? i.get(Calendar.DAY_OF_MONTH) : "0" + i.get(Calendar.DAY_OF_MONTH))
					   .append(" ")
					   .append("00").append(":")
					   .append("00").append(":")
					   .append("00");
					
					Calendar f = Calendar.getInstance();
					f.setTime(enfermidadeAbatedouroFrigorificoDTO.getDataDoAbate());
					
					StringBuilder sbF = new StringBuilder();
					sbF.append(f.get(Calendar.YEAR)).append("-")
					   .append(((f.get(Calendar.MONTH) + 1) + "").length() > 1 ? (f.get(Calendar.MONTH)+1) : "0" + (f.get(Calendar.MONTH)+1)).append("-")
					   .append((f.get(Calendar.DAY_OF_MONTH) + "").length() > 1 ? f.get(Calendar.DAY_OF_MONTH) : "0" + f.get(Calendar.DAY_OF_MONTH))
					   .append(" ")
					   .append("23").append(":")
					   .append("23").append(":")
					   .append("59");
					
					query.setString("inicio", sbI.toString());
					query.setString("final", sbF.toString());
				}
			}
		}

		query.setFirstResult(first);
		query.setMaxResults(pageSize);

		List<LoteEnfermidadeAbatedouroFrigorifico> lista = query.list();
		
		if (lista != null && !lista.isEmpty()){
			for (LoteEnfermidadeAbatedouroFrigorifico loteEnfermidadeAbatedouroFrigorifico : lista) {
				Hibernate.initialize(loteEnfermidadeAbatedouroFrigorifico.getListaGtaEnfermidadeAbatedouroFrigorifico());
				Hibernate.initialize(loteEnfermidadeAbatedouroFrigorifico.getListaAchadosLoteEnfermidadeAbatedouroFrigorifico());
			}
		}
		
		if (dto != null && !dto.isNull()){
			EnfermidadeAbatedouroFrigorificoDTO enfermidadeAbatedouroFrigorificoDTO = (EnfermidadeAbatedouroFrigorificoDTO) dto;
			enfermidadeAbatedouroFrigorificoDTO.setId(null);
		}
		
		return lista;
    }
	
	@Override
	public void validar(LoteEnfermidadeAbatedouroFrigorifico model) {
		
	}

	@Override
	public void validarPersist(LoteEnfermidadeAbatedouroFrigorifico model) {
		
	}

	@Override
	public void validarMerge(LoteEnfermidadeAbatedouroFrigorifico model) {
		
	}

	@Override
	public void validarDelete(LoteEnfermidadeAbatedouroFrigorifico model) {
		
	}

}
