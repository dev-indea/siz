package br.gov.mt.indea.sistemaformin.entity;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

import br.gov.mt.indea.sistemaformin.enums.Dominio.AtivoInativo;

@Audited
@Entity
@Table(name="tipo_chave_secundaria_termo")
public class TipoChaveSecundariaTermoFiscalizacao extends BaseEntity<Long>{

	private static final long serialVersionUID = 3458588875415434425L;

	@Id
	@SequenceGenerator(name="tipo_chave_secund_termo_seq", sequenceName="tipo_chave_secund_termo_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="tipo_chave_secund_termo_seq")
	private Long id;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_usuario")
	private Usuario usuario;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_cadastro")
	private Calendar dataCadastro;
	
	private String nome;
	
	@Enumerated(EnumType.STRING)
    private AtivoInativo status;
	
	@ManyToOne
	@JoinColumn(name="id_tipo_chave_principal_termo")
	private TipoChavePrincipalTermoFiscalizacao tipoChavePrincipalTermoFiscalizacao;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Calendar getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Calendar dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public AtivoInativo getStatus() {
		return status;
	}

	public void setStatus(AtivoInativo status) {
		this.status = status;
	}

	public TipoChavePrincipalTermoFiscalizacao getTipoChavePrincipalTermoFiscalizacao() {
		return tipoChavePrincipalTermoFiscalizacao;
	}

	public void setTipoChavePrincipalTermoFiscalizacao(
			TipoChavePrincipalTermoFiscalizacao tipoChavePrincipalTermoFiscalizacao) {
		this.tipoChavePrincipalTermoFiscalizacao = tipoChavePrincipalTermoFiscalizacao;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoChaveSecundariaTermoFiscalizacao other = (TipoChaveSecundariaTermoFiscalizacao) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}