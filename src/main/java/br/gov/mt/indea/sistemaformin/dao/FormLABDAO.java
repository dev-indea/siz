package br.gov.mt.indea.sistemaformin.dao;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.inject.Inject;


import br.gov.mt.indea.sistemaformin.entity.FormLAB;
import br.gov.mt.indea.sistemaformin.exception.ApplicationException;

@Stateless
public class FormLABDAO extends DAO<FormLAB> {
	
	@Inject
	private EntityManager em;
	
	public FormLABDAO() {
		super(FormLAB.class);
	}

	@Override
	public EntityManager getEntityManager() {
		return this.em;
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void add(FormLAB formLAB) throws ApplicationException{
		formLAB = this.getEntityManager().merge(formLAB);
		if (formLAB.getFormCOM() != null)
			formLAB.setFormCOM(this.getEntityManager().merge(formLAB.getFormCOM()));
		super.add(formLAB);
	}

}
