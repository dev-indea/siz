package br.gov.mt.indea.sistemaformin.entity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

import br.gov.mt.indea.sistemaformin.enums.Dominio.ResultadoNecropsiaDeAves;
import br.gov.mt.indea.sistemaformin.enums.Dominio.SimNao;
import br.gov.mt.indea.sistemaformin.enums.Dominio.SinaisClinicosSindromeNervosaERespiratoriaDasAves;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoAlimentoAves;

@Audited
@Entity
@Table(name="form_srn")
public class FormSRN extends BaseEntity<Long>{
	
	private static final long serialVersionUID = -8212634293780967219L;

	@Id
	@SequenceGenerator(name="form_srn_seq", sequenceName="form_srn_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="form_srn_seq")
	private Long id;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_usuario")
	private Usuario usuario;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_cadastro")
	private Calendar dataCadastro;
	
	@ManyToOne
	@JoinColumn(name="id_formin")
	private FormIN formIN;
	
	@ManyToOne
	@JoinColumn(name="id_formcom")
	private FormCOM formCOM;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_atendimento")
	private Calendar dataAtendimento;
	
	@Column(name="integradora_ou_cooperativa", length=255)
    private String empresaIntegradoraOuCooperativa;
	
	@Column(name="id_nucleo_ou_lote_envolvido", length=255)
    private String idNucleoOuLoteEnvolvido;
	
	@Column(name="idade_nucleo_ou_lote", length=255)
    private String idadeNucleoOuLoteEnvolvido;
    
    @Column(name="granja_ou_local_de_origem", length=255)
    private String granjaOuLocalDeOrigemDasAves;
    
    @ManyToOne
	@JoinColumn(name="id_uf_granja_ou_local")
	private UF ufGranjaOuLocalDeOrigemDosAnimais = UF.MATO_GROSSO;
	
	@ManyToOne
	@JoinColumn(name="id_municipio_granja_ou_local")
    private Municipio municipioGranjaOuLocalDeOrigemDosAnimais;
    
    @Column(name="incubatorio_de_origem", length=255)
    private String incubatorioDeOrigem;
    
    @ManyToOne
	@JoinColumn(name="id_uf_incubatorio_origem")
	private UF ufIncubatorioDeOrigem = UF.MATO_GROSSO;
	
	@ManyToOne
	@JoinColumn(name="id_municipio_incubatorio_origem")
    private Municipio municipioIncubatorioDeOrigem;
	
	@ElementCollection(targetClass = TipoAlimentoAves.class)
	@JoinTable(name = "formsrn_tipo_alimento", joinColumns = @JoinColumn(name = "id_formsrn", nullable = false))
	@Column(name = "id_formsrn_tipo_alimento")
	@Enumerated(EnumType.STRING)
	private List<TipoAlimentoAves> tipoAlimento = new ArrayList<TipoAlimentoAves>();
	
	@Column(name="outro_alimento", length=255)
	private String outroAlimento;
	
	@Enumerated(EnumType.STRING)
	@Column(name="alimento_tratado")
	private SimNao alimentoPassaPorTratamento;
	
	@Column(name="tratamento_alimento", length=255)
	private String tratamentoAlimento;
	
	@Enumerated(EnumType.STRING)
	@Column(name="agua_tratada")
	private SimNao aguaPassaPorTratamento;
	
	@Column(name="tratamento_agua", length=255)
	private String tratamentoAgua;
	
	@Enumerated(EnumType.STRING)
	@Column(name="cama_reutilizada")
	private SimNao camaReutilizada;

	@Enumerated(EnumType.STRING)
	@Column(name="cama_tratada")
	private SimNao camaPassaPorTratamento;
	
	@Column(name="tratamento_cama", length=255)
	private String tratamentoCama;
	
	@Enumerated(EnumType.STRING)
	@Column(name="vizinhos_possuem_aves")
	private SimNao vizinhosPossuemAves;
	
	@Column(name="tipos_aves_dos_vizinhos")
	private String tiposDeAvesDosVizinhos;
	
	@Enumerated(EnumType.STRING)
	@Column(name="historico_mortalidade_estab")
	private SimNao haHistoricoDeAltaMortalidadeNoEstabelecimento;
	
	@Column(name="quando_houve_mort_estab", length=255)
	private String quandoHouveAltaMortalidadeNoEstabelecimento;
	
	@Enumerated(EnumType.STRING)
	@Column(name="historico_mortalidade_regiao")
	private SimNao haHistoricoDeAltaMortalidadeNaRegiaoDeAvesComSinaisClinicosRelacionadosASuspeita;
	
	@Column(name="quando_houve_mort_regiao", length=255)
	private String quandoHouveAltaMortalidadeNaRegiao;
	
	@Column(name="motivo_mortalidade", length=255)
	private String motivoMortalidade;
	
	@Enumerated(EnumType.STRING)
	@Column(name="estab_possui_vet_permanente")
	private SimNao estabelecimentoPossuiAssistenciaVeterinariaPermanente;
	
	@Enumerated(EnumType.STRING)
	@Column(name="veterinario_visitou_as_aves")
	private SimNao veterinarioVisitouAsAves;
	
	@Column(name="diagnostico_presuntivo")
	private String diagnosticoPresuntivo;
	
	@Enumerated(EnumType.STRING)
	@Column(name="ha_laudo_das_aves")
	private SimNao haLaudoDasAves;
	
	@Column(length=255)
	private String diagnostico;
	
	@Enumerated(EnumType.STRING)
	@Column(name="foi_tomada_alguma_acao")
	private SimNao foiTomadaAlgumaAcao;
	
	@Column(name="acao_tomada", length=255)
	private String acaoTomada;
	
	@Enumerated(EnumType.STRING)
	@Column(name="hovue_reducao_de_mortes")
	private SimNao houveReducaoDaMortalidadeAposAAcao;
	
	@Enumerated(EnumType.STRING)
	@Column(name="houve_mort_repentina")
	private SimNao houveAltaMortalidadeRepentina;
	
	@Column(name="percentual_mortalidade")
	private Long percentualMortalidade;
	
	@Enumerated(EnumType.STRING)
	@Column(name="houve_queda_alimentacao")
	private SimNao houveQuedaDeConsumoDeAlimentacao;
	
	@Column(name="percentual_queda_alimentacao")
	private Long percentualQuedaDeConsumoDeAlimentacao;
	
	@Enumerated(EnumType.STRING)
	@Column(name="houve_queda_de_postura")
	private SimNao houveQuedaDePostura;
	
	@Column(name="percentual_queda_de_postura")
	private Long percentualQuedaDePostura;
	
	@Enumerated(EnumType.STRING)
	@Column(name="houve_queda_consumo_agua")
	private SimNao houveQuedaDeConsumoDeAgua;
	
	@Column(name="percent_queda_consumo_agua")
	private Long percentualQuedaDeConsumoDeAgua;
	
	@ElementCollection(targetClass = SinaisClinicosSindromeNervosaERespiratoriaDasAves.class)
	@JoinTable(name = "formsrn_sinais_estado_geral", joinColumns = @JoinColumn(name = "id_formsrn", nullable = false))
	@Column(name = "id_formsrn_sinais_estado_geral")
	@Enumerated(EnumType.STRING)
	private List<SinaisClinicosSindromeNervosaERespiratoriaDasAves> sinaisClinicosEstadoGeral = new ArrayList<SinaisClinicosSindromeNervosaERespiratoriaDasAves>();
	
	@ElementCollection(targetClass = SinaisClinicosSindromeNervosaERespiratoriaDasAves.class)
	@JoinTable(name = "formsrn_sinais_sist_respiratorio", joinColumns = @JoinColumn(name = "id_formsrn", nullable = false))
	@Column(name = "id_formsrn_sinais_sist_respiratorio")
	@Enumerated(EnumType.STRING)
	private List<SinaisClinicosSindromeNervosaERespiratoriaDasAves> sinaisClinicosSistemaRespiratorio = new ArrayList<SinaisClinicosSindromeNervosaERespiratoriaDasAves>();
	
	@ElementCollection(targetClass = SinaisClinicosSindromeNervosaERespiratoriaDasAves.class)
	@JoinTable(name = "formsrn_sinais_sist_nervoso", joinColumns = @JoinColumn(name = "id_formsrn", nullable = false))
	@Column(name = "id_formsrn_sinais_sist_nervoso")
	@Enumerated(EnumType.STRING)
	private List<SinaisClinicosSindromeNervosaERespiratoriaDasAves> sinaisClinicosSistemaNervoso = new ArrayList<SinaisClinicosSindromeNervosaERespiratoriaDasAves>();
	
	@ElementCollection(targetClass = SinaisClinicosSindromeNervosaERespiratoriaDasAves.class)
	@JoinTable(name = "formsrn_sinais_sist_digestorio", joinColumns = @JoinColumn(name = "id_formsrn", nullable = false))
	@Column(name = "id_formsrn_sinais_sist_digestorio")
	@Enumerated(EnumType.STRING)
	private List<SinaisClinicosSindromeNervosaERespiratoriaDasAves> sinaisClinicosSistemaDigestorio = new ArrayList<SinaisClinicosSindromeNervosaERespiratoriaDasAves>();
	
	@ElementCollection(targetClass = SinaisClinicosSindromeNervosaERespiratoriaDasAves.class)
	@JoinTable(name = "formsrn_sinais_sist_circulatorio", joinColumns = @JoinColumn(name = "id_formsrn", nullable = false))
	@Column(name = "id_formsrn_sinais_sist_circulatorio")
	@Enumerated(EnumType.STRING)
	private List<SinaisClinicosSindromeNervosaERespiratoriaDasAves> sinaisClinicosSistemaCirculatorio = new ArrayList<SinaisClinicosSindromeNervosaERespiratoriaDasAves>();
	
	@Column(name="qtdade_aves_necropsiadas")
	private Long quantidadeAvesNecropsiadas;
	
	@Column(name="qtdade_aves_com_sinais_clinicos")
	private Long quantidadeAvesComSinaisClinicos;
	
	@Column(name="qtdade_aves_mortas")
	private Long quantidadeAvesMortas;
	
	@ElementCollection(targetClass = ResultadoNecropsiaDeAves.class)
	@JoinTable(name = "formsrn_necrop_estado_geral", joinColumns = @JoinColumn(name = "id_formsrn", nullable = false))
	@Column(name = "id_formsrn_necrop_estado_geral")
	@Enumerated(EnumType.STRING)
	private List<ResultadoNecropsiaDeAves> necropsiaEstadoGeral = new ArrayList<ResultadoNecropsiaDeAves>();
	
	@ElementCollection(targetClass = ResultadoNecropsiaDeAves.class)
	@JoinTable(name = "formsrn_necrop_sist_resp", joinColumns = @JoinColumn(name = "id_formsrn", nullable = false))
	@Column(name = "id_formsrn_necrop_sist_resp")
	@Enumerated(EnumType.STRING)
	private List<ResultadoNecropsiaDeAves> necropsiaSistemaRespiratorio = new ArrayList<ResultadoNecropsiaDeAves>();
	
	@ElementCollection(targetClass = ResultadoNecropsiaDeAves.class)
	@JoinTable(name = "formsrn_necrop_sist_urin", joinColumns = @JoinColumn(name = "id_formsrn", nullable = false))
	@Column(name = "id_formsrn_necrop_sist_urin")
	@Enumerated(EnumType.STRING)
	private List<ResultadoNecropsiaDeAves> necropsiaSistemaUrinario = new ArrayList<ResultadoNecropsiaDeAves>();
	
	@ElementCollection(targetClass = ResultadoNecropsiaDeAves.class)
	@JoinTable(name = "formsrn_necrop_sist_circ", joinColumns = @JoinColumn(name = "id_formsrn", nullable = false))
	@Column(name = "id_formsrn_necrop_sist_circ")
	@Enumerated(EnumType.STRING)
	private List<ResultadoNecropsiaDeAves> necropsiaSistemaCirculatorio = new ArrayList<ResultadoNecropsiaDeAves>();
	
	@ElementCollection(targetClass = ResultadoNecropsiaDeAves.class)
	@JoinTable(name = "formsrn_necrop_sist_digestivo", joinColumns = @JoinColumn(name = "id_formsrn", nullable = false))
	@Column(name = "id_formsrn_necrop_sist_digestivo")
	@Enumerated(EnumType.STRING)
	private List<ResultadoNecropsiaDeAves> necropsiaSistemaDigestivo = new ArrayList<ResultadoNecropsiaDeAves>();
	
	@ElementCollection(targetClass = ResultadoNecropsiaDeAves.class)
	@JoinTable(name = "formsrn_necrop_sist_nervoso", joinColumns = @JoinColumn(name = "id_formsrn", nullable = false))
	@Column(name = "id_formsrn_necrop_sist_nervoso")
	@Enumerated(EnumType.STRING)
	private List<ResultadoNecropsiaDeAves> necropsiaSistemaNervoso = new ArrayList<ResultadoNecropsiaDeAves>();
	
	@Column(name="tipo_exsudato_traqueal", length=255)
	private String tipoExsudatoTraqueal;
	
	@Column(name="orgaos_c_congest_sist_urinario", length=255)
	private String orgaosComCongestaoNoSistemaUrinario;
	
	@Column(name="orgaos_c_congest_sist_circulat", length=255)
	private String orgaosComCongestaoNoSistemaCirculatorio;
	
	@Column(name="orgaos_c_congest_sist_digestiv", length=255)
	private String orgaosComCongestaoNoSistemaDigestivo;
	
	@Column(name="informacoes_adicionais", length=1000)
	private String informacoesAdicionais;
	
	@OneToOne(cascade={CascadeType.ALL}, orphanRemoval=true)
	@JoinColumn(name="id_veterinario")
	private br.gov.mt.indea.sistemaformin.webservice.entity.Veterinario veterinario;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Calendar getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Calendar dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public FormIN getFormIN() {
		return formIN;
	}

	public void setFormIN(FormIN formIN) {
		this.formIN = formIN;
	}

	public FormCOM getFormCOM() {
		return formCOM;
	}

	public void setFormCOM(FormCOM formCOM) {
		this.formCOM = formCOM;
	}

	public Calendar getDataAtendimento() {
		return dataAtendimento;
	}

	public void setDataAtendimento(Calendar dataAtendimento) {
		this.dataAtendimento = dataAtendimento;
	}

	public List<TipoAlimentoAves> getTipoAlimento() {
		return tipoAlimento;
	}

	public void setTipoAlimento(List<TipoAlimentoAves> tipoALimento) {
		this.tipoAlimento = tipoALimento;
	}

	public String getOutroAlimento() {
		return outroAlimento;
	}

	public void setOutroAlimento(String outroAlimento) {
		this.outroAlimento = outroAlimento;
	}

	public SimNao getAlimentoPassaPorTratamento() {
		return alimentoPassaPorTratamento;
	}

	public void setAlimentoPassaPorTratamento(SimNao alimentoPassaPorTratamento) {
		this.alimentoPassaPorTratamento = alimentoPassaPorTratamento;
	}

	public String getTratamentoAlimento() {
		return tratamentoAlimento;
	}

	public void setTratamentoAlimento(String tratamentoAlimento) {
		this.tratamentoAlimento = tratamentoAlimento;
	}

	public SimNao getAguaPassaPorTratamento() {
		return aguaPassaPorTratamento;
	}

	public void setAguaPassaPorTratamento(SimNao aguaPassaPorTratamento) {
		this.aguaPassaPorTratamento = aguaPassaPorTratamento;
	}

	public String getTratamentoAgua() {
		return tratamentoAgua;
	}

	public void setTratamentoAgua(String tratamentoAgua) {
		this.tratamentoAgua = tratamentoAgua;
	}

	public SimNao getCamaReutilizada() {
		return camaReutilizada;
	}

	public void setCamaReutilizada(SimNao camaReutilizada) {
		this.camaReutilizada = camaReutilizada;
	}

	public SimNao getCamaPassaPorTratamento() {
		return camaPassaPorTratamento;
	}

	public void setCamaPassaPorTratamento(SimNao camaPassaPorTratamento) {
		this.camaPassaPorTratamento = camaPassaPorTratamento;
	}

	public String getTratamentoCama() {
		return tratamentoCama;
	}

	public void setTratamentoCama(String tratamentoCama) {
		this.tratamentoCama = tratamentoCama;
	}

	public SimNao getVizinhosPossuemAves() {
		return vizinhosPossuemAves;
	}

	public void setVizinhosPossuemAves(SimNao vizinhosPossuemAves) {
		this.vizinhosPossuemAves = vizinhosPossuemAves;
	}

	public String getTiposDeAvesDosVizinhos() {
		return tiposDeAvesDosVizinhos;
	}

	public void setTiposDeAvesDosVizinhos(String tiposDeAvesDosVizinhos) {
		this.tiposDeAvesDosVizinhos = tiposDeAvesDosVizinhos;
	}

	public SimNao getHaHistoricoDeAltaMortalidadeNoEstabelecimento() {
		return haHistoricoDeAltaMortalidadeNoEstabelecimento;
	}

	public void setHaHistoricoDeAltaMortalidadeNoEstabelecimento(
			SimNao haHistoricoDeAltaMortalidadeNoEstabelecimento) {
		this.haHistoricoDeAltaMortalidadeNoEstabelecimento = haHistoricoDeAltaMortalidadeNoEstabelecimento;
	}

	public String getQuandoHouveAltaMortalidadeNoEstabelecimento() {
		return quandoHouveAltaMortalidadeNoEstabelecimento;
	}

	public void setQuandoHouveAltaMortalidadeNoEstabelecimento(
			String quandoHouveAltaMortalidadeNoEstabelecimento) {
		this.quandoHouveAltaMortalidadeNoEstabelecimento = quandoHouveAltaMortalidadeNoEstabelecimento;
	}

	public SimNao getHaHistoricoDeAltaMortalidadeNaRegiaoDeAvesComSinaisClinicosRelacionadosASuspeita() {
		return haHistoricoDeAltaMortalidadeNaRegiaoDeAvesComSinaisClinicosRelacionadosASuspeita;
	}

	public void setHaHistoricoDeAltaMortalidadeNaRegiaoDeAvesComSinaisClinicosRelacionadosASuspeita(
			SimNao haHistoricoDeAltaMortalidadeNaRegiaoDeAvesComSinaisClinicosRelacionadosASuspeita) {
		this.haHistoricoDeAltaMortalidadeNaRegiaoDeAvesComSinaisClinicosRelacionadosASuspeita = haHistoricoDeAltaMortalidadeNaRegiaoDeAvesComSinaisClinicosRelacionadosASuspeita;
	}

	public String getQuandoHouveAltaMortalidadeNaRegiao() {
		return quandoHouveAltaMortalidadeNaRegiao;
	}

	public void setQuandoHouveAltaMortalidadeNaRegiao(
			String quandoHouveAltaMortalidadeNaRegiao) {
		this.quandoHouveAltaMortalidadeNaRegiao = quandoHouveAltaMortalidadeNaRegiao;
	}

	public String getMotivoMortalidade() {
		return motivoMortalidade;
	}

	public void setMotivoMortalidade(String motivoMortalidade) {
		this.motivoMortalidade = motivoMortalidade;
	}

	public SimNao getVeterinarioVisitouAsAves() {
		return veterinarioVisitouAsAves;
	}

	public void setVeterinarioVisitouAsAves(SimNao veterinarioVisitouAsAves) {
		this.veterinarioVisitouAsAves = veterinarioVisitouAsAves;
	}

	public String getDiagnosticoPresuntivo() {
		return diagnosticoPresuntivo;
	}

	public void setDiagnosticoPresuntivo(String diagnosticoPresuntivo) {
		this.diagnosticoPresuntivo = diagnosticoPresuntivo;
	}

	public SimNao getHaLaudoDasAves() {
		return haLaudoDasAves;
	}

	public void setHaLaudoDasAves(SimNao haLaudoDasAves) {
		this.haLaudoDasAves = haLaudoDasAves;
	}

	public String getDiagnostico() {
		return diagnostico;
	}

	public void setDiagnostico(String diagnostico) {
		this.diagnostico = diagnostico;
	}

	public SimNao getFoiTomadaAlgumaAcao() {
		return foiTomadaAlgumaAcao;
	}

	public void setFoiTomadaAlgumaAcao(SimNao foiTomadaAlgumaAcao) {
		this.foiTomadaAlgumaAcao = foiTomadaAlgumaAcao;
	}

	public String getAcaoTomada() {
		return acaoTomada;
	}

	public void setAcaoTomada(String acaoTomada) {
		this.acaoTomada = acaoTomada;
	}

	public SimNao getHouveReducaoDaMortalidadeAposAAcao() {
		return houveReducaoDaMortalidadeAposAAcao;
	}

	public void setHouveReducaoDaMortalidadeAposAAcao(
			SimNao houveReducaoDaMortalidadeAposAAcao) {
		this.houveReducaoDaMortalidadeAposAAcao = houveReducaoDaMortalidadeAposAAcao;
	}

	public SimNao getHouveAltaMortalidadeRepentina() {
		return houveAltaMortalidadeRepentina;
	}

	public void setHouveAltaMortalidadeRepentina(
			SimNao houveAltaMortalidadeRepentina) {
		this.houveAltaMortalidadeRepentina = houveAltaMortalidadeRepentina;
	}

	public Long getPercentualMortalidade() {
		return percentualMortalidade;
	}

	public void setPercentualMortalidade(Long percentualMortalidade) {
		this.percentualMortalidade = percentualMortalidade;
	}

	public SimNao getHouveQuedaDeConsumoDeAlimentacao() {
		return houveQuedaDeConsumoDeAlimentacao;
	}

	public void setHouveQuedaDeConsumoDeAlimentacao(SimNao houveQuedaDeConsumoDeAlimentacao) {
		this.houveQuedaDeConsumoDeAlimentacao = houveQuedaDeConsumoDeAlimentacao;
	}

	public Long getPercentualQuedaDeConsumoDeAlimentacao() {
		return percentualQuedaDeConsumoDeAlimentacao;
	}

	public void setPercentualQuedaDeConsumoDeAlimentacao(Long percentualQuedaDeConsumoDeAlimentacao) {
		this.percentualQuedaDeConsumoDeAlimentacao = percentualQuedaDeConsumoDeAlimentacao;
	}

	public SimNao getHouveQuedaDePostura() {
		return houveQuedaDePostura;
	}

	public void setHouveQuedaDePostura(SimNao houveQuedaDePostura) {
		this.houveQuedaDePostura = houveQuedaDePostura;
	}

	public Long getPercentualQuedaDePostura() {
		return percentualQuedaDePostura;
	}

	public void setPercentualQuedaDePostura(Long percentualQuedaDePostura) {
		this.percentualQuedaDePostura = percentualQuedaDePostura;
	}

	public SimNao getHouveQuedaDeConsumoDeAgua() {
		return houveQuedaDeConsumoDeAgua;
	}

	public void setHouveQuedaDeConsumoDeAgua(SimNao houveQuedaDeConsumoDeAgua) {
		this.houveQuedaDeConsumoDeAgua = houveQuedaDeConsumoDeAgua;
	}

	public Long getPercentualQuedaDeConsumoDeAgua() {
		return percentualQuedaDeConsumoDeAgua;
	}

	public void setPercentualQuedaDeConsumoDeAgua(
			Long percentualQuedaDeConsumoDeAgua) {
		this.percentualQuedaDeConsumoDeAgua = percentualQuedaDeConsumoDeAgua;
	}

	public List<SinaisClinicosSindromeNervosaERespiratoriaDasAves> getSinaisClinicosEstadoGeral() {
		return sinaisClinicosEstadoGeral;
	}

	public void setSinaisClinicosEstadoGeral(
			List<SinaisClinicosSindromeNervosaERespiratoriaDasAves> sinaisClinicosEstadoGeral) {
		this.sinaisClinicosEstadoGeral = sinaisClinicosEstadoGeral;
	}

	public List<SinaisClinicosSindromeNervosaERespiratoriaDasAves> getSinaisClinicosSistemaRespiratorio() {
		return sinaisClinicosSistemaRespiratorio;
	}

	public void setSinaisClinicosSistemaRespiratorio(
			List<SinaisClinicosSindromeNervosaERespiratoriaDasAves> sinaisClinicosSistemaRespiratorio) {
		this.sinaisClinicosSistemaRespiratorio = sinaisClinicosSistemaRespiratorio;
	}

	public List<SinaisClinicosSindromeNervosaERespiratoriaDasAves> getSinaisClinicosSistemaNervoso() {
		return sinaisClinicosSistemaNervoso;
	}

	public void setSinaisClinicosSistemaNervoso(
			List<SinaisClinicosSindromeNervosaERespiratoriaDasAves> sinaisClinicosSistemaNervoso) {
		this.sinaisClinicosSistemaNervoso = sinaisClinicosSistemaNervoso;
	}

	public List<SinaisClinicosSindromeNervosaERespiratoriaDasAves> getSinaisClinicosSistemaDigestorio() {
		return sinaisClinicosSistemaDigestorio;
	}

	public void setSinaisClinicosSistemaDigestorio(
			List<SinaisClinicosSindromeNervosaERespiratoriaDasAves> sinaisClinicosSistemaDigestorio) {
		this.sinaisClinicosSistemaDigestorio = sinaisClinicosSistemaDigestorio;
	}

	public List<SinaisClinicosSindromeNervosaERespiratoriaDasAves> getSinaisClinicosSistemaCirculatorio() {
		return sinaisClinicosSistemaCirculatorio;
	}

	public void setSinaisClinicosSistemaCirculatorio(
			List<SinaisClinicosSindromeNervosaERespiratoriaDasAves> sinaisClinicosSistemaCirculatorio) {
		this.sinaisClinicosSistemaCirculatorio = sinaisClinicosSistemaCirculatorio;
	}

	public Long getQuantidadeAvesNecropsiadas() {
		return quantidadeAvesNecropsiadas;
	}

	public void setQuantidadeAvesNecropsiadas(Long quantidadeAvesNecropsiadas) {
		this.quantidadeAvesNecropsiadas = quantidadeAvesNecropsiadas;
	}

	public Long getQuantidadeAvesComSinaisClinicos() {
		return quantidadeAvesComSinaisClinicos;
	}

	public void setQuantidadeAvesComSinaisClinicos(
			Long quantidadeAvesComSinaisClinicos) {
		this.quantidadeAvesComSinaisClinicos = quantidadeAvesComSinaisClinicos;
	}

	public Long getQuantidadeAvesMortas() {
		return quantidadeAvesMortas;
	}

	public void setQuantidadeAvesMortas(Long quantidadeAvesMortas) {
		this.quantidadeAvesMortas = quantidadeAvesMortas;
	}

	public List<ResultadoNecropsiaDeAves> getNecropsiaEstadoGeral() {
		return necropsiaEstadoGeral;
	}

	public void setNecropsiaEstadoGeral(
			List<ResultadoNecropsiaDeAves> necropsiaEstadoGeral) {
		this.necropsiaEstadoGeral = necropsiaEstadoGeral;
	}

	public List<ResultadoNecropsiaDeAves> getNecropsiaSistemaRespiratorio() {
		return necropsiaSistemaRespiratorio;
	}

	public void setNecropsiaSistemaRespiratorio(
			List<ResultadoNecropsiaDeAves> necropsiaSistemaRespiratorio) {
		this.necropsiaSistemaRespiratorio = necropsiaSistemaRespiratorio;
	}

	public List<ResultadoNecropsiaDeAves> getNecropsiaSistemaUrinario() {
		return necropsiaSistemaUrinario;
	}

	public void setNecropsiaSistemaUrinario(
			List<ResultadoNecropsiaDeAves> necropsiaSistemaUrinario) {
		this.necropsiaSistemaUrinario = necropsiaSistemaUrinario;
	}

	public List<ResultadoNecropsiaDeAves> getNecropsiaSistemaCirculatorio() {
		return necropsiaSistemaCirculatorio;
	}

	public void setNecropsiaSistemaCirculatorio(
			List<ResultadoNecropsiaDeAves> necropsiaSistemaCirculatorio) {
		this.necropsiaSistemaCirculatorio = necropsiaSistemaCirculatorio;
	}

	public List<ResultadoNecropsiaDeAves> getNecropsiaSistemaNervoso() {
		return necropsiaSistemaNervoso;
	}

	public void setNecropsiaSistemaNervoso(
			List<ResultadoNecropsiaDeAves> necropsiaSistemaNervoso) {
		this.necropsiaSistemaNervoso = necropsiaSistemaNervoso;
	}

	public String getTipoExsudatoTraqueal() {
		return tipoExsudatoTraqueal;
	}

	public void setTipoExsudatoTraqueal(String tipoExsudatoTraqueal) {
		this.tipoExsudatoTraqueal = tipoExsudatoTraqueal;
	}

	public String getOrgaosComCongestaoNoSistemaUrinario() {
		return orgaosComCongestaoNoSistemaUrinario;
	}

	public void setOrgaosComCongestaoNoSistemaUrinario(
			String orgaosComCongestaoNoSistemaUrinario) {
		this.orgaosComCongestaoNoSistemaUrinario = orgaosComCongestaoNoSistemaUrinario;
	}

	public String getOrgaosComCongestaoNoSistemaCirculatorio() {
		return orgaosComCongestaoNoSistemaCirculatorio;
	}

	public void setOrgaosComCongestaoNoSistemaCirculatorio(
			String orgaosComCongestaoNoSistemaCirculatorio) {
		this.orgaosComCongestaoNoSistemaCirculatorio = orgaosComCongestaoNoSistemaCirculatorio;
	}

	public String getInformacoesAdicionais() {
		return informacoesAdicionais;
	}

	public void setInformacoesAdicionais(String informacoesAdicionais) {
		this.informacoesAdicionais = informacoesAdicionais;
	}

	public List<ResultadoNecropsiaDeAves> getNecropsiaSistemaDigestivo() {
		return necropsiaSistemaDigestivo;
	}

	public void setNecropsiaSistemaDigestivo(
			List<ResultadoNecropsiaDeAves> necropsiaSistemaDigestivo) {
		this.necropsiaSistemaDigestivo = necropsiaSistemaDigestivo;
	}

	public String getOrgaosComCongestaoNoSistemaDigestivo() {
		return orgaosComCongestaoNoSistemaDigestivo;
	}

	public void setOrgaosComCongestaoNoSistemaDigestivo(
			String orgaosComCongestaoNoSistemaDigestivo) {
		this.orgaosComCongestaoNoSistemaDigestivo = orgaosComCongestaoNoSistemaDigestivo;
	}

	public SimNao getEstabelecimentoPossuiAssistenciaVeterinariaPermanente() {
		return estabelecimentoPossuiAssistenciaVeterinariaPermanente;
	}

	public void setEstabelecimentoPossuiAssistenciaVeterinariaPermanente(
			SimNao estabelecimentoPossuiAssistenciaVeterinariaPermanente) {
		this.estabelecimentoPossuiAssistenciaVeterinariaPermanente = estabelecimentoPossuiAssistenciaVeterinariaPermanente;
	}

	public br.gov.mt.indea.sistemaformin.webservice.entity.Veterinario getVeterinario() {
		return veterinario;
	}

	public void setVeterinario(br.gov.mt.indea.sistemaformin.webservice.entity.Veterinario veterinario) {
		this.veterinario = veterinario;
	}

	public String getEmpresaIntegradoraOuCooperativa() {
		return empresaIntegradoraOuCooperativa;
	}

	public void setEmpresaIntegradoraOuCooperativa(
			String empresaIntegradoraOuCooperativa) {
		this.empresaIntegradoraOuCooperativa = empresaIntegradoraOuCooperativa;
	}

	public String getIdNucleoOuLoteEnvolvido() {
		return idNucleoOuLoteEnvolvido;
	}

	public void setIdNucleoOuLoteEnvolvido(String idNucleoOuLoteEnvolvido) {
		this.idNucleoOuLoteEnvolvido = idNucleoOuLoteEnvolvido;
	}

	public String getIdadeNucleoOuLoteEnvolvido() {
		return idadeNucleoOuLoteEnvolvido;
	}

	public void setIdadeNucleoOuLoteEnvolvido(String idadeNucleoOuLoteEnvolvido) {
		this.idadeNucleoOuLoteEnvolvido = idadeNucleoOuLoteEnvolvido;
	}

	public String getGranjaOuLocalDeOrigemDasAves() {
		return granjaOuLocalDeOrigemDasAves;
	}

	public void setGranjaOuLocalDeOrigemDasAves(
			String granjaOuLocalDeOrigemDasAves) {
		this.granjaOuLocalDeOrigemDasAves = granjaOuLocalDeOrigemDasAves;
	}

	public Municipio getMunicipioGranjaOuLocalDeOrigemDosAnimais() {
		return municipioGranjaOuLocalDeOrigemDosAnimais;
	}

	public void setMunicipioGranjaOuLocalDeOrigemDosAnimais(
			Municipio municipioGranjaOuLocalDeOrigemDosAnimais) {
		this.municipioGranjaOuLocalDeOrigemDosAnimais = municipioGranjaOuLocalDeOrigemDosAnimais;
	}

	public String getIncubatorioDeOrigem() {
		return incubatorioDeOrigem;
	}

	public void setIncubatorioDeOrigem(String incubatorioDeOrigem) {
		this.incubatorioDeOrigem = incubatorioDeOrigem;
	}

	public Municipio getMunicipioIncubatorioDeOrigem() {
		return municipioIncubatorioDeOrigem;
	}

	public void setMunicipioIncubatorioDeOrigem(
			Municipio municipioIncubatorioDeOrigem) {
		this.municipioIncubatorioDeOrigem = municipioIncubatorioDeOrigem;
	}

	public UF getUfGranjaOuLocalDeOrigemDosAnimais() {
		return ufGranjaOuLocalDeOrigemDosAnimais;
	}

	public void setUfGranjaOuLocalDeOrigemDosAnimais(
			UF ufGranjaOuLocalDeOrigemDosAnimais) {
		this.ufGranjaOuLocalDeOrigemDosAnimais = ufGranjaOuLocalDeOrigemDosAnimais;
	}

	public UF getUfIncubatorioDeOrigem() {
		return ufIncubatorioDeOrigem;
	}

	public void setUfIncubatorioDeOrigem(UF ufIncubatorioDeOrigem) {
		this.ufIncubatorioDeOrigem = ufIncubatorioDeOrigem;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FormSRN other = (FormSRN) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}