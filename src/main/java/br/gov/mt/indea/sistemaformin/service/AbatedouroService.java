package br.gov.mt.indea.sistemaformin.service;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.hibernate.Query;

import br.gov.mt.indea.sistemaformin.exception.NotSupportedRuntimeException;
import br.gov.mt.indea.sistemaformin.webservice.entity.Abatedouro;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class AbatedouroService extends PaginableService<Abatedouro, Long> {
	
	protected AbatedouroService() {
		super(Abatedouro.class);
	}
	
	public Abatedouro findByCnpj(String cnpj){
		Abatedouro abatedouro;
		
		StringBuilder sql = new StringBuilder();
		sql.append("select abatedouro")
		   .append("  from Abatedouro abatedouro ")
		   .append("  join fetch abatedouro.municipio")
		   .append("  left join fetch abatedouro.coordenadaGeografica")
		   .append(" where abatedouro.cnpj = :cnpj ");
		
		Query query = getSession().createQuery(sql.toString()).setString("cnpj", cnpj);
		abatedouro = (Abatedouro) query.uniqueResult();
		
		return abatedouro;
	}
	
	@Override
	public void saveOrUpdate(Abatedouro abatedouro) {
		throw new NotSupportedRuntimeException("A��o n�o permitida");
	}
	
	@Override
	public void delete(Abatedouro abatedouro) {
		throw new NotSupportedRuntimeException("A��o n�o permitida");
	}
	
	@Override
	public void persist(Abatedouro model) {
		throw new NotSupportedRuntimeException("A��o n�o permitida");
	}
	
	@Override
	public void validar(Abatedouro Abatedouro) {

	}

	@Override
	public void validarPersist(Abatedouro Abatedouro) {

	}

	@Override
	public void validarMerge(Abatedouro Abatedouro) {

	}

	@Override
	public void validarDelete(Abatedouro Abatedouro) {

	}

}
