package br.gov.mt.indea.sistemaformin.managedbeans;

import java.io.Serializable;
import java.util.Calendar;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.PrimeFaces;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FlowEvent;
import org.springframework.util.StringUtils;

import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLBeanName;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;

import br.gov.mt.indea.sistemaformin.entity.Municipio;
import br.gov.mt.indea.sistemaformin.entity.Notificacao;
import br.gov.mt.indea.sistemaformin.entity.Notificante;
import br.gov.mt.indea.sistemaformin.entity.UF;
import br.gov.mt.indea.sistemaformin.enums.Dominio.SimNao;
import br.gov.mt.indea.sistemaformin.managedbeans.constants.WizardNotificacaoConstants;
import br.gov.mt.indea.sistemaformin.service.MunicipioService;
import br.gov.mt.indea.sistemaformin.service.NotificacaoService;
import br.gov.mt.indea.sistemaformin.util.FacesMessageUtil;
import br.gov.mt.indea.sistemaformin.webservice.client.CepService;
import br.gov.mt.indea.sistemaformin.webservice.entity.Endereco;


@ViewScoped
@Named("notificacaoManagedBean")
@URLBeanName("notificacaoManagedBean")
@URLMappings(mappings = {
		@URLMapping(id = "notificacao", pattern = "/notificacao", viewId = "/externo/notificacao/inicio.jsf"),
		@URLMapping(id = "novaNotificacao", pattern = "/notificacao/novo", viewId = "/externo/notificacao/novo.jsf"),
		@URLMapping(id = "consultarNotificacao", pattern = "/notificacao/consultar", viewId = "/externo/notificacao/consultar.jsf")})
public class NotificacaoManagedBean implements Serializable {

	public static final long serialVersionUID = 1992610136330703837L;

	@Inject
	Notificacao notificacao;
	
	private UF uf = UF.MATO_GROSSO;
	
	@Inject
	CepService cepService;
	
	@Inject
	MunicipioService municipioService;
	
	@Inject
	NotificacaoService notificacaoService;
	
	private SimNao notificanteQuerSeIdentificar;
	
	private SimNao contato;
	
	private boolean firstPage = true;
	
	private boolean lastPage = false;
	
	private boolean saved = false;
	
	private boolean telaInicial = true;

	private List<Municipio> listaMunicipios;
	
	private String numeroBusca;
	
	private String codigoSegurancaBusca;
	
	@PostConstruct
	public void init() {
		
	}
	
	@URLAction(mappingId = "novaNotificacao", onPostback = false)
	public void novaNotificacao() {
		listaMunicipios = municipioService.findAllByUF(this.uf);
	}
	
	@URLAction(mappingId = "consultarNotificacao", onPostback = false)
	public void consultarNotificacao() {

	}
	
	public void buscar() {
		this.notificacao = notificacaoService.find(numeroBusca, codigoSegurancaBusca);
		
		if (this.notificacao == null) {
			FacesMessageUtil.addErrorContextFacesMessage("N�o encontramos nenhum alerta.", "Verifique os dados informados");
		}
	}
	
	public String onFlowProcess(FlowEvent event) {
		RequestContext context = RequestContext.getCurrentInstance();
		context.update("pgrowl");
		
		String nextStep = event.getNewStep();
		
		if (event.getOldStep().equals(WizardNotificacaoConstants.TIPO_INFORMANTE)) {
			this.selectNotificante();
			this.firstPage = false;
		}
		
		// Se o notificante � an�nimo, pula pra localiza��o
		if (nextStep.equals(WizardNotificacaoConstants.NOME_INFORMANTE)){
			if (this.notificanteQuerSeIdentificar.equals(SimNao.NAO)){
				return WizardNotificacaoConstants.MOTIVO_NOTIFICACAO;
			}
		} 
		
		//Se est� no nome do notificante e volta, firstPage volta a ser true
		if (event.getOldStep().equals(WizardNotificacaoConstants.NOME_INFORMANTE) && event.getNewStep().equals(WizardNotificacaoConstants.TIPO_INFORMANTE)) {
			this.firstPage = true;
		}
		
		// Se o notificante � an�nimo, e o usu�rio volta de localiza��o, pula pro notificante
		if (nextStep.equals(WizardNotificacaoConstants.TELEFONE_INFORMANTE) && event.getOldStep().equals(WizardNotificacaoConstants.MOTIVO_NOTIFICACAO) && this.notificanteQuerSeIdentificar.equals(SimNao.NAO)) {
			this.firstPage = true;
			return WizardNotificacaoConstants.TIPO_INFORMANTE;
		}
		
		// Se o endere�o n�o possui logradouro ou refer�ncia, o sistema n�o permite continuar
		if (event.getOldStep().equals(WizardNotificacaoConstants.ENDERECO_ESTABELECIMENTO) && event.getNewStep().equals(WizardNotificacaoConstants.NOME_CONTATO_ESTABELECIMENTO)) {
			if (StringUtils.isEmpty(this.notificacao.getEndereco().getLogradouro()) && StringUtils.isEmpty(this.notificacao.getEndereco().getReferencia())) {
				FacesMessageUtil.addErrorContextFacesMessage("Adicione pelo menos uma imforma��o sobre o endere�o da localidade", "Adicione pelo menos uma imforma��o sobre o endere�o da localidade");
				return event.getOldStep();
			}
		}
		
		// Se o usu�rio n�o sabe o contato, pula pro motivo da notifica��o 
		if (nextStep.equals(WizardNotificacaoConstants.NOME_CONTATO_ESTABELECIMENTO)){
			if (this.contato.equals(SimNao.NAO)){
				this.lastPage = true;
				return WizardNotificacaoConstants.RESUMO;
			}
		} 
		
		// Se o usu�rio n�o sabe o contato, e o usu�rio volta do motivo, pula pro contato
		if (nextStep.equals(WizardNotificacaoConstants.TIPO_CONTATO_ESTABELECIMENTO) && event.getOldStep().equals(WizardNotificacaoConstants.RESUMO) && this.contato.equals(SimNao.NAO)) {
			this.lastPage = false;
			return WizardNotificacaoConstants.CONHECE_CONTATO_ESTABELECIMENTO;
		}
		
		if (event.getNewStep().equals(WizardNotificacaoConstants.RESUMO))
			this.lastPage = true;
		
		if (event.getOldStep().equals(WizardNotificacaoConstants.RESUMO))
			this.lastPage = false;
		
		return nextStep;
    }
	
	public void selectNotificante() {
		if (this.notificacao != null)
			if (this.notificanteQuerSeIdentificar != null)
				if (this.notificanteQuerSeIdentificar.equals(SimNao.SIM)) {
					this.notificacao.setNotificante(new Notificante());
				} else {
					this.notificacao.setNotificante(null);
				}
	}
	
	public void buscarEndereco() {
		Endereco endereco = cepService.buscarCep(this.notificacao.getEndereco().getCep());
		if (endereco == null) {
			FacesMessageUtil.addContextFacesMessage(FacesMessage.SEVERITY_WARN, "N�o foi poss�vel encontrar o enderec�o deste CEP", "Pro favor continue com o preenchimento manualmente");
		}else {
			this.notificacao.setEndereco(endereco);
			
			PrimeFaces.current().resetInputs("panelEndereco");
		}
	}
	
	public String save() {
		this.notificacao.setDataCadastro(Calendar.getInstance());
		notificacaoService.saveOrUpdate(this.notificacao);
		
		this.saved = true;
		this.firstPage = false;
		this.lastPage = false;
		
		return "";
	}

	public boolean isFirstPage() {
		return firstPage;
	}

	public void setFirstPage(boolean firstPage) {
		this.firstPage = firstPage;
	}

	public boolean isLastPage() {
		return lastPage;
	}

	public void setLastPage(boolean lastPage) {
		this.lastPage = lastPage;
	}

	public boolean isSaved() {
		return saved;
	}

	public void setSaved(boolean saved) {
		this.saved = saved;
	}

	public Notificacao getNotificacao() {
		return notificacao;
	}

	public void setNotificacao(Notificacao notificacao) {
		this.notificacao = notificacao;
	}

	public SimNao getAnonimo() {
		return notificanteQuerSeIdentificar;
	}

	public void setAnonimo(SimNao anonimo) {
		this.notificanteQuerSeIdentificar = anonimo;
	}

	public List<Municipio> getListaMunicipios() {
		return listaMunicipios;
	}

	public void setListaMunicipios(List<Municipio> listaMunicipios) {
		this.listaMunicipios = listaMunicipios;
	}

	public SimNao getContato() {
		return contato;
	}

	public void setContato(SimNao contato) {
		this.contato = contato;
	}

	public boolean isTelaInicial() {
		return telaInicial;
	}

	public void setTelaInicial(boolean telaInicial) {
		this.telaInicial = telaInicial;
	}

	public String getNumeroBusca() {
		return numeroBusca;
	}

	public void setNumeroBusca(String numeroBusca) {
		this.numeroBusca = numeroBusca;
	}

	public String getCodigoSegurancaBusca() {
		return codigoSegurancaBusca;
	}

	public void setCodigoSegurancaBusca(String codigoSegurancaBusca) {
		this.codigoSegurancaBusca = codigoSegurancaBusca;
	}
	
}
