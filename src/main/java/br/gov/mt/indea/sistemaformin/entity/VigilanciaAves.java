package br.gov.mt.indea.sistemaformin.entity;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

import br.gov.mt.indea.sistemaformin.enums.Dominio.Especie;

@Audited
@Entity(name="vigilancia_aves")
public class VigilanciaAves extends BaseEntity<Long>{
	
	private static final long serialVersionUID = -361785479572430083L;

	@Id
	@SequenceGenerator(name="vigilancia_aves_seq", sequenceName="vigilancia_aves_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="vigilancia_aves_seq")
	private Long id;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_cadastro")
	private Calendar dataCadastro;
	
	@ManyToOne
	@JoinColumn(name="id_vigilancia_veterinaria")
	private VigilanciaVeterinaria vigilanciaVeterinaria;
	
	@Enumerated(EnumType.STRING)
	@Column(name="especie")
	private Especie especie;
	
	@Column(name="quantidade_inspecionados")
	private Integer quantidadeInspecionados;
	
	@Column(name="quantidade_vistoriados")
	private Integer quantidadeVistoriados;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Calendar getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Calendar dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Especie getEspecie() {
		return especie;
	}

	public void setEspecie(Especie especie) {
		this.especie = especie;
	}

	public Integer getQuantidadeInspecionados() {
		return quantidadeInspecionados;
	}

	public void setQuantidadeInspecionados(Integer quantidadeInspecionados) {
		this.quantidadeInspecionados = quantidadeInspecionados;
	}

	public Integer getQuantidadeVistoriados() {
		return quantidadeVistoriados;
	}

	public void setQuantidadeVistoriados(Integer quantidadeVistoriados) {
		this.quantidadeVistoriados = quantidadeVistoriados;
	}

	public VigilanciaVeterinaria getVigilanciaVeterinaria() {
		return vigilanciaVeterinaria;
	}

	public void setVigilanciaVeterinaria(VigilanciaVeterinaria vigilanciaVeterinaria) {
		this.vigilanciaVeterinaria = vigilanciaVeterinaria;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VigilanciaAves other = (VigilanciaAves) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}