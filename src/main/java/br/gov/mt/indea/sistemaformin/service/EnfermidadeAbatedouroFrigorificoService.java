package br.gov.mt.indea.sistemaformin.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.activation.FileDataSource;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.event.Event;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.hibernate.Hibernate;
import org.hibernate.LazyInitializationException;
import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.mt.indea.sistemaformin.entity.AchadosLoteEnfermidadeAbatedouroFrigorifico;
import br.gov.mt.indea.sistemaformin.entity.EnfermidadeAbatedouroFrigorifico;
import br.gov.mt.indea.sistemaformin.entity.FormularioDeColheitaTroncoEncefalico;
import br.gov.mt.indea.sistemaformin.entity.GtaEnfermidadeAbatedouroFrigorifico;
import br.gov.mt.indea.sistemaformin.entity.LoteEnfermidadeAbatedouroFrigorifico;
import br.gov.mt.indea.sistemaformin.entity.Municipio;
import br.gov.mt.indea.sistemaformin.entity.NumeroFormularioDeColheitaTroncoEncefalico;
import br.gov.mt.indea.sistemaformin.entity.ServicoDeInspecao;
import br.gov.mt.indea.sistemaformin.entity.UF;
import br.gov.mt.indea.sistemaformin.entity.dto.AbstractDTO;
import br.gov.mt.indea.sistemaformin.entity.dto.EnfermidadeAbatedouroFrigorificoDTO;
import br.gov.mt.indea.sistemaformin.event.MailEvent;
import br.gov.mt.indea.sistemaformin.exception.ApplicationException;
import br.gov.mt.indea.sistemaformin.exception.ApplicationRuntimeException;
import br.gov.mt.indea.sistemaformin.util.GeradorDeRelatorio;
import br.gov.mt.indea.sistemaformin.util.StringUtil;
import net.sf.jasperreports.engine.JRException;

@Stateless
public class EnfermidadeAbatedouroFrigorificoService extends PaginableService<EnfermidadeAbatedouroFrigorifico, Long>{

	private static final Logger log = LoggerFactory.getLogger(EnfermidadeAbatedouroFrigorificoService.class);
	
	@Inject
	private NumeroFormularioDeColheitaTroncoEncefalicoService numeroFormularioDeColheitaTroncoEncefalicoService;
	
	@Inject
	private Event<MailEvent> eventProducer;
	
	@Inject
	ParametrosSIZService parametrosSIZService;
	
	@Inject
	PropriedadeService propriedadeService;
	
	protected EnfermidadeAbatedouroFrigorificoService() {
		super(EnfermidadeAbatedouroFrigorifico.class);
	}
	
	public EnfermidadeAbatedouroFrigorifico findByIdFetchAll(Long id){
		EnfermidadeAbatedouroFrigorifico enfermidadeAbatedouroFrigorifico;
		
		StringBuilder sql = new StringBuilder();
		sql.append("select enfermidadeAbatedouroFrigorifico ")
		   .append("  from EnfermidadeAbatedouroFrigorifico enfermidadeAbatedouroFrigorifico ")
		   .append("  join fetch enfermidadeAbatedouroFrigorifico.servicoDeInspecao servicoDeInspecao ")
		   .append("  join fetch servicoDeInspecao.abatedouro abatedouro ")
		   .append("  left join fetch abatedouro.endereco endereco ")
		   .append("  left join fetch enfermidadeAbatedouroFrigorifico.listaLoteEnfermidadeAbatedouroFrigorifico listaLoteEnfermidadeAbatedouroFrigorifico ")
		   .append("  left join fetch listaLoteEnfermidadeAbatedouroFrigorifico.proprietario proprietario ")
		   .append("  left join fetch proprietario.propriedade propriedade ")
		   .append(" where enfermidadeAbatedouroFrigorifico.id = :id ")
		   .append(" order by enfermidadeAbatedouroFrigorifico.dataAbate ");
		
		Query query = getSession().createQuery(sql.toString()).setLong("id", id);
		enfermidadeAbatedouroFrigorifico = (EnfermidadeAbatedouroFrigorifico) query.uniqueResult();
		
		if (enfermidadeAbatedouroFrigorifico != null){
			for (LoteEnfermidadeAbatedouroFrigorifico loteEnfermidadeAbatedouroFrigorifico : enfermidadeAbatedouroFrigorifico.getListaLoteEnfermidadeAbatedouroFrigorifico()) {
				Hibernate.initialize(loteEnfermidadeAbatedouroFrigorifico.getListaGtaEnfermidadeAbatedouroFrigorifico());
				for (GtaEnfermidadeAbatedouroFrigorifico gta : loteEnfermidadeAbatedouroFrigorifico.getListaGtaEnfermidadeAbatedouroFrigorifico()) {
					Hibernate.initialize(gta.getGta().getPessoaOrigemGta());
				}
				
				Hibernate.initialize(loteEnfermidadeAbatedouroFrigorifico.getListaAchadosLoteEnfermidadeAbatedouroFrigorifico());
				
				for (AchadosLoteEnfermidadeAbatedouroFrigorifico achado : loteEnfermidadeAbatedouroFrigorifico.getListaAchadosLoteEnfermidadeAbatedouroFrigorifico()) {
					Hibernate.initialize(achado.getListaFormularioDeColheitaTroncoEncefalico());
					
					for (FormularioDeColheitaTroncoEncefalico formulario : achado.getListaFormularioDeColheitaTroncoEncefalico()) {
						Hibernate.initialize(formulario.getListaMotivacaoParaAbateDeEmergenciaEmFrigorifico());
						Hibernate.initialize(formulario.getListaSinaisClinicosNervososEncefalopatoaEspongiformeTransmissivel());
					}
				}
				
				for (GtaEnfermidadeAbatedouroFrigorifico gta : loteEnfermidadeAbatedouroFrigorifico.getListaGtaEnfermidadeAbatedouroFrigorifico()) {
					Hibernate.initialize(gta.getGta().getEstratificacoes());
				}
			}
			
			
		}
		
		return enfermidadeAbatedouroFrigorifico;
	}
	
	public EnfermidadeAbatedouroFrigorifico findByDataAbateFetchAll(Date dataAbate, ServicoDeInspecao servicoDeInspecao){
		EnfermidadeAbatedouroFrigorifico enfermidadeAbatedouroFrigorifico;
		
		StringBuilder sql = new StringBuilder();
		sql.append("select enfermidadeAbatedouroFrigorifico ")
		   .append("  from EnfermidadeAbatedouroFrigorifico enfermidadeAbatedouroFrigorifico ")
		   .append("  join fetch enfermidadeAbatedouroFrigorifico.servicoDeInspecao servicoDeInspecao ")
		   .append("  join fetch servicoDeInspecao.abatedouro abatedouro ")
		   .append("  left join fetch abatedouro.endereco endereco ")
		   .append("  left join fetch enfermidadeAbatedouroFrigorifico.listaLoteEnfermidadeAbatedouroFrigorifico listaLoteEnfermidadeAbatedouroFrigorifico ")
		   .append("  left join fetch listaLoteEnfermidadeAbatedouroFrigorifico.proprietario proprietario ")
		   .append("  left join fetch proprietario.propriedade propriedade ")
		   .append(" where enfermidadeAbatedouroFrigorifico.dataAbate = :dataAbate ")
		   .append("   and enfermidadeAbatedouroFrigorifico.servicoDeInspecao = :servicoDeInspecao ")
		   .append(" order by enfermidadeAbatedouroFrigorifico.dataAbate ");
		
		Query query = getSession().createQuery(sql.toString()).setDate("dataAbate", dataAbate).setParameter("servicoDeInspecao", servicoDeInspecao);
		enfermidadeAbatedouroFrigorifico = (EnfermidadeAbatedouroFrigorifico) query.uniqueResult();
		
		if (enfermidadeAbatedouroFrigorifico != null){
			for (LoteEnfermidadeAbatedouroFrigorifico loteEnfermidadeAbatedouroFrigorifico : enfermidadeAbatedouroFrigorifico.getListaLoteEnfermidadeAbatedouroFrigorifico()) {
				Hibernate.initialize(loteEnfermidadeAbatedouroFrigorifico.getListaGtaEnfermidadeAbatedouroFrigorifico());
				for (GtaEnfermidadeAbatedouroFrigorifico gta : loteEnfermidadeAbatedouroFrigorifico.getListaGtaEnfermidadeAbatedouroFrigorifico()) {
					Hibernate.initialize(gta.getGta().getPessoaOrigemGta());
				}
				
				Hibernate.initialize(loteEnfermidadeAbatedouroFrigorifico.getListaAchadosLoteEnfermidadeAbatedouroFrigorifico());
				
				for (AchadosLoteEnfermidadeAbatedouroFrigorifico achado : loteEnfermidadeAbatedouroFrigorifico.getListaAchadosLoteEnfermidadeAbatedouroFrigorifico()) {
					Hibernate.initialize(achado.getListaFormularioDeColheitaTroncoEncefalico());
					
					for (FormularioDeColheitaTroncoEncefalico formulario : achado.getListaFormularioDeColheitaTroncoEncefalico()) {
						Hibernate.initialize(formulario.getListaMotivacaoParaAbateDeEmergenciaEmFrigorifico());
						Hibernate.initialize(formulario.getListaSinaisClinicosNervososEncefalopatoaEspongiformeTransmissivel());
					}
				}
				
				for (GtaEnfermidadeAbatedouroFrigorifico gta : loteEnfermidadeAbatedouroFrigorifico.getListaGtaEnfermidadeAbatedouroFrigorifico()) {
					Hibernate.initialize(gta.getGta().getEstratificacoes());
				}
			}
			
			
		}
		
		return enfermidadeAbatedouroFrigorifico;
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public Long countAll(AbstractDTO dto) {
		StringBuilder sql = new StringBuilder();
		sql.append("select count(enfermidadeAbatedouroFrigorifico) ")
		   .append("  from " + this.getType().getSimpleName() + " as enfermidadeAbatedouroFrigorifico")
		   .append("  left join enfermidadeAbatedouroFrigorifico.listaLoteEnfermidadeAbatedouroFrigorifico listaLoteEnfermidadeAbatedouroFrigorifico ")
		   .append("  left join listaLoteEnfermidadeAbatedouroFrigorifico.proprietario proprietario ")
		   .append("  left join proprietario.propriedade propriedade")
		   .append("  where 1 = 1 ");
		
		if (dto != null && !dto.isNull()){
			EnfermidadeAbatedouroFrigorificoDTO enfermidadeAbatedouroFrigorificoDTO = (EnfermidadeAbatedouroFrigorificoDTO) dto;
			if (enfermidadeAbatedouroFrigorificoDTO.getId() != null)
				sql.append("  and enfermidadeAbatedouroFrigorifico.id = :id");
			else {
				if (enfermidadeAbatedouroFrigorificoDTO.getDataDoAbate() != null)
					sql.append("  and to_char(enfermidadeAbatedouroFrigorifico.dataAbate, 'YYYY-MM-DD HH:MI:SS') between :inicio and :final");
				if (enfermidadeAbatedouroFrigorificoDTO.getMunicipio() != null)
					sql.append("  and propriedade.municipio = :municipio");
				if (enfermidadeAbatedouroFrigorificoDTO.getNomePropriedade() != null && !enfermidadeAbatedouroFrigorificoDTO.getNomePropriedade().equals(""))
					sql.append("  and lower(remove_acento(propriedade.nome)) like :nomePropriedade");
				if (enfermidadeAbatedouroFrigorificoDTO.getCodigoPropriedade() != null && !enfermidadeAbatedouroFrigorificoDTO.getCodigoPropriedade().equals(""))
					sql.append("  and propriedade.codigoPropriedade = :codigoPropriedade");
				if (enfermidadeAbatedouroFrigorificoDTO.getServicoDeInspecao() != null)
					sql.append("  and enfermidadeAbatedouroFrigorifico.servicoDeInspecao = :servicoDeInspecao");
			}
		}
		
		Query query = getSession().createQuery(sql.toString());
		
		if (dto != null && !dto.isNull()){
			EnfermidadeAbatedouroFrigorificoDTO enfermidadeAbatedouroFrigorificoDTO = (EnfermidadeAbatedouroFrigorificoDTO) dto;
			
			if (enfermidadeAbatedouroFrigorificoDTO.getId() != null)
				query.setParameter("id", enfermidadeAbatedouroFrigorificoDTO.getId());
			else {
				if (enfermidadeAbatedouroFrigorificoDTO.getMunicipio() != null)
					query.setParameter("municipio", enfermidadeAbatedouroFrigorificoDTO.getMunicipio());
				if (enfermidadeAbatedouroFrigorificoDTO.getNomePropriedade() != null && !enfermidadeAbatedouroFrigorificoDTO.getNomePropriedade().equals(""))
					query.setString("nomePropriedade", StringUtil.removeAcentos('%' + enfermidadeAbatedouroFrigorificoDTO.getNomePropriedade().toLowerCase()) + '%');
				if (enfermidadeAbatedouroFrigorificoDTO.getCodigoPropriedade() != null && !enfermidadeAbatedouroFrigorificoDTO.getCodigoPropriedade().equals(""))
					query.setLong("codigoPropriedade", enfermidadeAbatedouroFrigorificoDTO.getCodigoPropriedade());
				if (enfermidadeAbatedouroFrigorificoDTO.getServicoDeInspecao() != null)
					query.setParameter("servicoDeInspecao", enfermidadeAbatedouroFrigorificoDTO.getServicoDeInspecao());
				
				if (enfermidadeAbatedouroFrigorificoDTO.getDataDoAbate() != null){
					Calendar i = Calendar.getInstance();
					i.setTime(enfermidadeAbatedouroFrigorificoDTO.getDataDoAbate());
					
					StringBuilder sbI = new StringBuilder();
					sbI.append(i.get(Calendar.YEAR)).append("-")
					   .append(((i.get(Calendar.MONTH) + 1) + "").length() > 1 ? (i.get(Calendar.MONTH)+1) : "0" + (i.get(Calendar.MONTH)+1)).append("-")
					   .append((i.get(Calendar.DAY_OF_MONTH) + "").length() > 1 ? i.get(Calendar.DAY_OF_MONTH) : "0" + i.get(Calendar.DAY_OF_MONTH))
					   .append(" ")
					   .append("00").append(":")
					   .append("00").append(":")
					   .append("00");
					
					Calendar f = Calendar.getInstance();
					f.setTime(enfermidadeAbatedouroFrigorificoDTO.getDataDoAbate());
					
					StringBuilder sbF = new StringBuilder();
					sbF.append(f.get(Calendar.YEAR)).append("-")
					   .append(((f.get(Calendar.MONTH) + 1) + "").length() > 1 ? (f.get(Calendar.MONTH)+1) : "0" + (f.get(Calendar.MONTH)+1)).append("-")
					   .append((f.get(Calendar.DAY_OF_MONTH) + "").length() > 1 ? f.get(Calendar.DAY_OF_MONTH) : "0" + f.get(Calendar.DAY_OF_MONTH))
					   .append(" ")
					   .append("23").append(":")
					   .append("23").append(":")
					   .append("59");
					
					query.setString("inicio", sbI.toString());
					query.setString("final", sbF.toString());
				}
			}
		}

		return (Long) query.uniqueResult();
    }
	
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<EnfermidadeAbatedouroFrigorifico> findAllComPaginacao(AbstractDTO dto, int first, int pageSize, String sortField, String sortOrder) {
		StringBuilder sql = new StringBuilder();
		sql.append("select enfermidadeAbatedouroFrigorifico")
		   .append("  from " + this.getType().getSimpleName() + " as enfermidadeAbatedouroFrigorifico")
		   .append("  left join fetch enfermidadeAbatedouroFrigorifico.listaLoteEnfermidadeAbatedouroFrigorifico listaLoteEnfermidadeAbatedouroFrigorifico ")
		   .append("  left join fetch listaLoteEnfermidadeAbatedouroFrigorifico.proprietario proprietario ")
		   .append("  left join fetch proprietario.propriedade propriedade")
		   .append("  where 1 = 1 ");
		
		if (dto != null && !dto.isNull()){
			EnfermidadeAbatedouroFrigorificoDTO enfermidadeAbatedouroFrigorificoDTO = (EnfermidadeAbatedouroFrigorificoDTO) dto;
			if (enfermidadeAbatedouroFrigorificoDTO.getId() != null)
				sql.append("  and enfermidadeAbatedouroFrigorifico.id = :id");
			else {
				if (enfermidadeAbatedouroFrigorificoDTO.getDataDoAbate() != null)
					sql.append("  and to_char(enfermidadeAbatedouroFrigorifico.dataAbate, 'YYYY-MM-DD HH:MI:SS') between :inicio and :final");
				if (enfermidadeAbatedouroFrigorificoDTO.getMunicipio() != null)
					sql.append("  and propriedade.municipio = :municipio");
				if (enfermidadeAbatedouroFrigorificoDTO.getNomePropriedade() != null && !enfermidadeAbatedouroFrigorificoDTO.getNomePropriedade().equals(""))
					sql.append("  and lower(remove_acento(propriedade.nome)) like :nomePropriedade");
				if (enfermidadeAbatedouroFrigorificoDTO.getCodigoPropriedade() != null && !enfermidadeAbatedouroFrigorificoDTO.getCodigoPropriedade().equals(""))
					sql.append("  and propriedade.codigoPropriedade = :codigoPropriedade");
				if (enfermidadeAbatedouroFrigorificoDTO.getServicoDeInspecao() != null)
					sql.append("  and enfermidadeAbatedouroFrigorifico.servicoDeInspecao = :servicoDeInspecao");
			}
		}
		
		if (StringUtils.isNotBlank(sortField))
			sql.append(" order by enfermidadeAbatedouroFrigorifico." + sortField + " " + sortOrder);

		Query query = getSession().createQuery(sql.toString());
		
		if (dto != null && !dto.isNull()){
			EnfermidadeAbatedouroFrigorificoDTO enfermidadeAbatedouroFrigorificoDTO = (EnfermidadeAbatedouroFrigorificoDTO) dto;
			
			if (enfermidadeAbatedouroFrigorificoDTO.getId() != null)
				query.setParameter("id", enfermidadeAbatedouroFrigorificoDTO.getId());
			else {
				if (enfermidadeAbatedouroFrigorificoDTO.getMunicipio() != null)
					query.setParameter("municipio", enfermidadeAbatedouroFrigorificoDTO.getMunicipio());
				if (enfermidadeAbatedouroFrigorificoDTO.getNomePropriedade() != null && !enfermidadeAbatedouroFrigorificoDTO.getNomePropriedade().equals(""))
					query.setString("nomePropriedade", StringUtil.removeAcentos('%' + enfermidadeAbatedouroFrigorificoDTO.getNomePropriedade().toLowerCase()) + '%');
				if (enfermidadeAbatedouroFrigorificoDTO.getCodigoPropriedade() != null && !enfermidadeAbatedouroFrigorificoDTO.getCodigoPropriedade().equals(""))
					query.setLong("codigoPropriedade", enfermidadeAbatedouroFrigorificoDTO.getCodigoPropriedade());
				if (enfermidadeAbatedouroFrigorificoDTO.getServicoDeInspecao() != null)
					query.setParameter("servicoDeInspecao", enfermidadeAbatedouroFrigorificoDTO.getServicoDeInspecao());
				
				if (enfermidadeAbatedouroFrigorificoDTO.getDataDoAbate() != null){
					Calendar i = Calendar.getInstance();
					i.setTime(enfermidadeAbatedouroFrigorificoDTO.getDataDoAbate());
					
					StringBuilder sbI = new StringBuilder();
					sbI.append(i.get(Calendar.YEAR)).append("-")
					   .append(((i.get(Calendar.MONTH) + 1) + "").length() > 1 ? (i.get(Calendar.MONTH)+1) : "0" + (i.get(Calendar.MONTH)+1)).append("-")
					   .append((i.get(Calendar.DAY_OF_MONTH) + "").length() > 1 ? i.get(Calendar.DAY_OF_MONTH) : "0" + i.get(Calendar.DAY_OF_MONTH))
					   .append(" ")
					   .append("00").append(":")
					   .append("00").append(":")
					   .append("00");
					
					Calendar f = Calendar.getInstance();
					f.setTime(enfermidadeAbatedouroFrigorificoDTO.getDataDoAbate());
					
					StringBuilder sbF = new StringBuilder();
					sbF.append(f.get(Calendar.YEAR)).append("-")
					   .append(((f.get(Calendar.MONTH) + 1) + "").length() > 1 ? (f.get(Calendar.MONTH)+1) : "0" + (f.get(Calendar.MONTH)+1)).append("-")
					   .append((f.get(Calendar.DAY_OF_MONTH) + "").length() > 1 ? f.get(Calendar.DAY_OF_MONTH) : "0" + f.get(Calendar.DAY_OF_MONTH))
					   .append(" ")
					   .append("23").append(":")
					   .append("23").append(":")
					   .append("59");
					
					query.setString("inicio", sbI.toString());
					query.setString("final", sbF.toString());
				}
			}
		}

		query.setFirstResult(first);
		query.setMaxResults(pageSize);

		List<EnfermidadeAbatedouroFrigorifico> lista = query.list();
		
		if (dto != null && !dto.isNull()){
			EnfermidadeAbatedouroFrigorificoDTO enfermidadeAbatedouroFrigorificoDTO = (EnfermidadeAbatedouroFrigorificoDTO) dto;
			enfermidadeAbatedouroFrigorificoDTO.setId(null);
		}
		
		return lista;
    }
	
	public void saveOrUpdate(EnfermidadeAbatedouroFrigorifico enfermidadeAbatedouroFrigorifico) {
		if (enfermidadeAbatedouroFrigorifico.getListaLoteEnfermidadeAbatedouroFrigorifico() != null)
			for (LoteEnfermidadeAbatedouroFrigorifico lote : enfermidadeAbatedouroFrigorifico.getListaLoteEnfermidadeAbatedouroFrigorifico()) {
				if (lote.getListaAchadosLoteEnfermidadeAbatedouroFrigorifico() != null)
					for (AchadosLoteEnfermidadeAbatedouroFrigorifico achados : lote.getListaAchadosLoteEnfermidadeAbatedouroFrigorifico()) {
						if (achados.getListaFormularioDeColheitaTroncoEncefalico() != null)
							for (FormularioDeColheitaTroncoEncefalico formulario : achados.getListaFormularioDeColheitaTroncoEncefalico()) {
								if (formulario.getNumero() == null)
									try {
										formulario.setNumero(getProximoNumeroFormularioDeColheitaTroncoEncefalico(formulario));
									} catch (ApplicationException e) {
										throw new ApplicationRuntimeException(e);
									}
							}
					}
			}
		
//		if (enfermidadeAbatedouroFrigorifico.getListaLoteEnfermidadeAbatedouroFrigorifico() != null)
//			for (LoteEnfermidadeAbatedouroFrigorifico lote : enfermidadeAbatedouroFrigorifico.getListaLoteEnfermidadeAbatedouroFrigorifico()) {
//				if (lote.getListaGtaEnfermidadeAbatedouroFrigorifico() != null) {
//					for (GtaEnfermidadeAbatedouroFrigorifico gta : lote.getListaGtaEnfermidadeAbatedouroFrigorifico()) {
//						if (gta.getGta().getPessoaOrigemGta().getPropriedade().getId() == null)
//							propriedadeService.saveOrUpdate(gta.getGta().getPessoaOrigemGta().getPropriedade());
//					}
//				}
//			}
		
		
		enfermidadeAbatedouroFrigorifico = (EnfermidadeAbatedouroFrigorifico) this.getSession().merge(enfermidadeAbatedouroFrigorifico);
//		super.saveOrUpdate(enfermidadeAbatedouroFrigorifico);
		
		for (LoteEnfermidadeAbatedouroFrigorifico lote : enfermidadeAbatedouroFrigorifico.getListaLoteEnfermidadeAbatedouroFrigorifico()) {
			if (lote.getProprietario().isPessoaOrigemDeOutroEstado())
				for (AchadosLoteEnfermidadeAbatedouroFrigorifico achado : lote.getListaAchadosLoteEnfermidadeAbatedouroFrigorifico()) {
					try {
						this.sendEmail(achado);
					} catch (MalformedURLException e) {
						e.printStackTrace();
					} catch (EmailException e) {
						e.printStackTrace();
					} catch (JRException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
		}
		
		log.info("Salvando Form IN {}", enfermidadeAbatedouroFrigorifico.getId());
	}
	
	private void sendEmail(AchadosLoteEnfermidadeAbatedouroFrigorifico achado) throws EmailException, JRException, IOException {
		MailEvent event = new MailEvent();
		
		StringBuilder assunto = new StringBuilder();
		assunto.append("SIZ-Achados: ");
		
		assunto.append(achado.getDoencaEnfermidadeAbatedouroFrigorifico().getNome());
		
		if (achado.getLoteEnfermidadeAbatedouroFrigorifico().houveColheita())
			assunto.append(", Com colheita");
		
		assunto.append(", de ")
			   .append(achado.getLoteEnfermidadeAbatedouroFrigorifico().getProprietario().getPropriedade().getMunicipio().getNome())
			   .append("/")
			   .append(achado.getLoteEnfermidadeAbatedouroFrigorifico().getProprietario().getPropriedade().getMunicipio().getUf().getUf());
		
		
		String emailSFA = parametrosSIZService.getInstance().getEmailSFA();
		event.setDestinatario(emailSFA);
		event.setAssunto(assunto.toString());
		event.setMensagem(this.getMensagemEmail(achado));
		
		if (achado.getListaFormularioDeColheitaTroncoEncefalico() != null && !achado.getListaFormularioDeColheitaTroncoEncefalico().isEmpty())
			event.setListaAnexo(this.getListaAnexo(achado));
		
		eventProducer.fire(event);
	}
	
	private Map<String, FileDataSource> getListaAnexo(AchadosLoteEnfermidadeAbatedouroFrigorifico achado) throws JRException, IOException {
		List<FormularioDeColheitaTroncoEncefalico> lista;
		Map<String, FileDataSource> listaAnexo = new HashMap<>();
		byte[] byteArrayRelatorio;
		FileDataSource fileDataSource;
		
		for (FormularioDeColheitaTroncoEncefalico formularioDeColheitaTroncoEncefalico : achado.getListaFormularioDeColheitaTroncoEncefalico()) {
			lista = new ArrayList<>();
			lista.add(formularioDeColheitaTroncoEncefalico);
			byteArrayRelatorio = GeradorDeRelatorio.gerarPdfFormularioDeColheitaTroncoEncefalico(lista);
			fileDataSource = new FileDataSource(this.getTempFile(byteArrayRelatorio, formularioDeColheitaTroncoEncefalico.getNumero()));
			listaAnexo.put(formularioDeColheitaTroncoEncefalico.getNumero(), fileDataSource);
		}
		
		return listaAnexo;
	}
	
	public File getTempFile(byte[] byteArray, String numero) throws IOException { 
		File tempFile = File.createTempFile(numero, ".pdf", null);
		tempFile.getName();
		FileOutputStream fos = new FileOutputStream(tempFile);
		fos.write(byteArray);
		
		return tempFile;
    }
	
	private String getMensagemEmail(AchadosLoteEnfermidadeAbatedouroFrigorifico achado) throws MalformedURLException, EmailException {
		VelocityEngine ve = new VelocityEngine();
		ve.setProperty("resource.loader", "file");
		ve.setProperty("runtime.log", FacesContext.getCurrentInstance().getExternalContext().getRealPath(File.separator) + File.separator + "logs" + File.separator + "velocity.log");
		ve.setProperty("file.resource.loader.path", FacesContext.getCurrentInstance().getExternalContext().getRealPath(File.separator) + File.separator + "resources" + File.separator + "templates" + File.separator + "email");
		ve.init();
		
		Template template = ve.getTemplate("emailNotificacaoAchadoAbatedouroFrigorifico.vm");
		HtmlEmail email = new HtmlEmail();
		
		String cidImage1 = email.embed(new File(FacesContext.getCurrentInstance().getExternalContext().getRealPath(File.separator) + File.separator + "resources" + File.separator + "img" + File.separator + "logos" + File.separator + "logo_mt.png").toURI().toURL(), "MT");
		String cidImage2 = email.embed(new File(FacesContext.getCurrentInstance().getExternalContext().getRealPath(File.separator) + File.separator + "resources" + File.separator + "img" + File.separator + "logos" + File.separator + "logo_indea.png").toURI().toURL(), "Indea");
		
		VelocityContext context = new VelocityContext();
		context.put("image1", "cid:" + cidImage1);
		context.put("image2", "cid:" + cidImage2);
		context.put("estadoOrigem", achado.getLoteEnfermidadeAbatedouroFrigorifico().getProprietario().getPropriedade().getMunicipio().getUf().getNome());

		context.put("nomeFrigorifico", achado.getLoteEnfermidadeAbatedouroFrigorifico().getEnfermidadeAbatedouroFrigorifico().getServicoDeInspecao().getAbatedouro().getNome());
		context.put("cnpjFrigorifico", achado.getLoteEnfermidadeAbatedouroFrigorifico().getEnfermidadeAbatedouroFrigorifico().getServicoDeInspecao().getAbatedouro().getCnpj());
		context.put("municipioFrigorifico", achado.getLoteEnfermidadeAbatedouroFrigorifico().getEnfermidadeAbatedouroFrigorifico().getServicoDeInspecao().getAbatedouro().getMunicipio().getNome() + "-" + achado.getLoteEnfermidadeAbatedouroFrigorifico().getEnfermidadeAbatedouroFrigorifico().getServicoDeInspecao().getAbatedouro().getMunicipio().getUf().getUf());
		context.put("tipoInspecaoFrigorifico", achado.getLoteEnfermidadeAbatedouroFrigorifico().getEnfermidadeAbatedouroFrigorifico().getServicoDeInspecao().getAbatedouro().getTipoInspecao());
		
		context.put("achado", achado.getDoencaEnfermidadeAbatedouroFrigorifico().getNome());
		context.put("quantidade", achado.getQuantidadeCasos());
		if (achado.getLoteEnfermidadeAbatedouroFrigorifico().houveColheita())
			context.put("houveColheita", "<br>Houve Colheita: SIM");
		else
			context.put("houveColheita", "");
		context.put("lote", achado.getLoteEnfermidadeAbatedouroFrigorifico().getNumero());
		context.put("gta", achado.getLoteEnfermidadeAbatedouroFrigorifico().getGTAAsString());
		context.put("propriedadeOrigem", achado.getLoteEnfermidadeAbatedouroFrigorifico().getProprietario().getPropriedade().getNome());
		context.put("proprietarioOrigem", achado.getLoteEnfermidadeAbatedouroFrigorifico().getProprietario().getProdutor().getNome());
		context.put("municipioOrigem", achado.getLoteEnfermidadeAbatedouroFrigorifico().getProprietario().getMunicipio().getNome() + " - " + achado.getLoteEnfermidadeAbatedouroFrigorifico().getProprietario().getMunicipio().getUf().getNome());
		
		context.put("achado", achado.getDoencaEnfermidadeAbatedouroFrigorifico().getNome());
		if (achado.getLoteEnfermidadeAbatedouroFrigorifico().houveColheita())
			context.put("anexoFormularioDeColheita", "<p>Segue anexo o Formulário de Colheita</p>");
		else
			context.put("anexoFormularioDeColheita", "");
		
		StringWriter writer = new StringWriter();
		template.merge(context, writer);
		
		return writer.toString();
	}
	
	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	private String getProximoNumeroFormularioDeColheitaTroncoEncefalico(FormularioDeColheitaTroncoEncefalico formularioDeColheitaTroncoEncefalico) throws ApplicationException {
		StringBuilder sb = new StringBuilder();
		
		String numeroSIF = null;
		String siglaUF = null;
		
		numeroSIF = formularioDeColheitaTroncoEncefalico.getAchadosLoteEnfermidadeAbatedouroFrigorifico().getLoteEnfermidadeAbatedouroFrigorifico().getEnfermidadeAbatedouroFrigorifico().getServicoDeInspecao().getNumero();
		siglaUF = formularioDeColheitaTroncoEncefalico.getAchadosLoteEnfermidadeAbatedouroFrigorifico().getLoteEnfermidadeAbatedouroFrigorifico().getProprietario().getMunicipio().getUf().getUf();
		
		int ano = formularioDeColheitaTroncoEncefalico.getDataColeta().get(java.util.Calendar.YEAR);
		
		NumeroFormularioDeColheitaTroncoEncefalico numeroFormularioDeColheitaTroncoEncefalico = this.getNumeroFormularioDeColheitaTroncoEncefalicoByMunicipio(formularioDeColheitaTroncoEncefalico.getAchadosLoteEnfermidadeAbatedouroFrigorifico().getLoteEnfermidadeAbatedouroFrigorifico().getProprietario().getMunicipio().getUf(), formularioDeColheitaTroncoEncefalico.getAchadosLoteEnfermidadeAbatedouroFrigorifico().getLoteEnfermidadeAbatedouroFrigorifico().getEnfermidadeAbatedouroFrigorifico().getServicoDeInspecao(), ano);
		
		String proximoNumero = String.format("%04d", numeroFormularioDeColheitaTroncoEncefalico.getUltimoNumero());
		
		sb.append(proximoNumero)
	      .append("/")
	      .append(numeroSIF)
	      .append("/")
	      .append(siglaUF)
	      .append("/")
	      .append(ano);
		
		return sb.toString();
	}
	
	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	private NumeroFormularioDeColheitaTroncoEncefalico getNumeroFormularioDeColheitaTroncoEncefalicoByMunicipio(UF uf, ServicoDeInspecao servicoDeInspecao, int ano) throws ApplicationException{
		NumeroFormularioDeColheitaTroncoEncefalico numeroFormularioDeColheitaTroncoEncefalico = numeroFormularioDeColheitaTroncoEncefalicoService.getNumeroFormularioDeColheitaTroncoEncefalicoByMunicipio(uf, servicoDeInspecao, ano);
		
		if (numeroFormularioDeColheitaTroncoEncefalico == null){
			numeroFormularioDeColheitaTroncoEncefalico = new NumeroFormularioDeColheitaTroncoEncefalico();
			numeroFormularioDeColheitaTroncoEncefalico.setUf(uf);
			numeroFormularioDeColheitaTroncoEncefalico.setServicoDeInspecao(servicoDeInspecao);
			numeroFormularioDeColheitaTroncoEncefalico.setUltimoNumero(0L);
			numeroFormularioDeColheitaTroncoEncefalico.setAno((long) ano);
			
			numeroFormularioDeColheitaTroncoEncefalicoService.saveOrUpdate(numeroFormularioDeColheitaTroncoEncefalico);
		}
		
		numeroFormularioDeColheitaTroncoEncefalico.incrementNumeroFormularioDeColheitaTroncoEncefalico();
		numeroFormularioDeColheitaTroncoEncefalicoService.merge(numeroFormularioDeColheitaTroncoEncefalico);
		
		return numeroFormularioDeColheitaTroncoEncefalico;
	}

	public void saveOrUpdate(LoteEnfermidadeAbatedouroFrigorifico loteEnfermidadeAbatedouroFrigorifico) {
		EnfermidadeAbatedouroFrigorifico enfermidadeAbatedouroFrigorifico = loteEnfermidadeAbatedouroFrigorifico.getEnfermidadeAbatedouroFrigorifico(); 
		enfermidadeAbatedouroFrigorifico = (EnfermidadeAbatedouroFrigorifico) this.getSession().merge(enfermidadeAbatedouroFrigorifico);
		
		loteEnfermidadeAbatedouroFrigorifico = (LoteEnfermidadeAbatedouroFrigorifico) this.getSession().merge(loteEnfermidadeAbatedouroFrigorifico);
		super.saveOrUpdate(enfermidadeAbatedouroFrigorifico);
	} 
	
	@SuppressWarnings("unchecked")
	public List<LoteEnfermidadeAbatedouroFrigorifico> findAllLoteEnfermidadeAbatedouroFrigorifico(Municipio municipio) {
		List<LoteEnfermidadeAbatedouroFrigorifico> enfermidadeAbatedouroFrigorifico;
		
		StringBuilder sql = new StringBuilder();
		sql.append("select distinct lote")
		   .append("  from LoteEnfermidadeAbatedouroFrigorifico lote ")
		   .append("  join fetch lote.proprietario proprietario")
		   .append("  join fetch lote.listaGtaEnfermidadeAbatedouroFrigorifico listaGtaEnfermidadeAbatedouroFrigorifico")
		   .append("  left join fetch proprietario.municipio municipio");
		
		if (municipio != null)
			sql.append(" where municipio.id = :id ");
		
		Query query = getSession().createQuery(sql.toString());
		
		if (municipio != null)
			query = query.setLong("id", municipio.getId());
		
		enfermidadeAbatedouroFrigorifico = (List<LoteEnfermidadeAbatedouroFrigorifico>) query.list();
		
		if (enfermidadeAbatedouroFrigorifico != null) {
			for (LoteEnfermidadeAbatedouroFrigorifico loteEnfermidadeAbatedouroFrigorifico : enfermidadeAbatedouroFrigorifico) {
				Hibernate.initialize(loteEnfermidadeAbatedouroFrigorifico.getListaAchadosLoteEnfermidadeAbatedouroFrigorifico());
			}
		}
		
		return enfermidadeAbatedouroFrigorifico;
	}
	
	@SuppressWarnings("unchecked")
	public void fetchListaFormularios(LoteEnfermidadeAbatedouroFrigorifico loteEnfermidadeAbatedouroFrigorifico) {
		for (AchadosLoteEnfermidadeAbatedouroFrigorifico achado : loteEnfermidadeAbatedouroFrigorifico.getListaAchadosLoteEnfermidadeAbatedouroFrigorifico()) {
			try {
				Hibernate.initialize(achado.getListaFormularioDeColheitaTroncoEncefalico());
			} catch (LazyInitializationException e) {
				StringBuilder sql = new StringBuilder();
				sql.append("  from FormularioDeColheitaTroncoEncefalico formulario ")
				   .append("  left join formulario.achadosLoteEnfermidadeAbatedouroFrigorifico achado")
				   .append(" where achado.id = :id ");
				
				Query query = getSession().createQuery(sql.toString()).setLong("id", achado.getId());
				achado.setListaFormularioDeColheitaTroncoEncefalico(query.list());
			}
		}
	}
	
	public LoteEnfermidadeAbatedouroFrigorifico findLoteEnfermidadeAbatedouroFrigorificoByIdFetchAll(Long id) {
		LoteEnfermidadeAbatedouroFrigorifico loteEnfermidadeAbatedouroFrigorifico;
		
		StringBuilder sql = new StringBuilder();
		sql.append("select loteEnfermidadeAbatedouroFrigorifico ")
		   .append("  from LoteEnfermidadeAbatedouroFrigorifico loteEnfermidadeAbatedouroFrigorifico ")
		   .append(" where loteEnfermidadeAbatedouroFrigorifico.id = :id ");
		
		Query query = getSession().createQuery(sql.toString()).setLong("id", id);
		loteEnfermidadeAbatedouroFrigorifico = (LoteEnfermidadeAbatedouroFrigorifico) query.uniqueResult();
		
		if (loteEnfermidadeAbatedouroFrigorifico != null){
			
		}
		
		return loteEnfermidadeAbatedouroFrigorifico;
	}

	@Override
	public void validar(EnfermidadeAbatedouroFrigorifico model) {
		
	}

	@Override
	public void validarPersist(EnfermidadeAbatedouroFrigorifico model) {
		
	}

	@Override
	public void validarMerge(EnfermidadeAbatedouroFrigorifico model) {
		
	}

	@Override
	public void validarDelete(EnfermidadeAbatedouroFrigorifico model) {
		
	}

	public FormularioDeColheitaTroncoEncefalico findFormularioDeColheitaTroncoEncefalicoFetchAll(Long id){
		FormularioDeColheitaTroncoEncefalico formularioDeColheitaTroncoEncefalico;
		
		StringBuilder sql = new StringBuilder();
		sql.append("select formularioDeColheitaTroncoEncefalico ")
		   .append("  from FormularioDeColheitaTroncoEncefalico formularioDeColheitaTroncoEncefalico ")
		   .append("  join fetch formularioDeColheitaTroncoEncefalico.achadosLoteEnfermidadeAbatedouroFrigorifico achadosLoteEnfermidadeAbatedouroFrigorifico ")
		   .append("  join fetch achadosLoteEnfermidadeAbatedouroFrigorifico.loteEnfermidadeAbatedouroFrigorifico loteEnfermidadeAbatedouroFrigorifico ")
		   .append("  join fetch loteEnfermidadeAbatedouroFrigorifico.enfermidadeAbatedouroFrigorifico enfermidadeAbatedouroFrigorifico ")
		   .append("  join fetch enfermidadeAbatedouroFrigorifico.servicoDeInspecao servicoDeInspecao ")
		   .append(" where formularioDeColheitaTroncoEncefalico.id = :id ");
		
		Query query = getSession().createQuery(sql.toString()).setLong("id", id);
		formularioDeColheitaTroncoEncefalico = (FormularioDeColheitaTroncoEncefalico) query.uniqueResult();
		
		if (formularioDeColheitaTroncoEncefalico != null){
			
		}
		
		return formularioDeColheitaTroncoEncefalico;
	}

}
