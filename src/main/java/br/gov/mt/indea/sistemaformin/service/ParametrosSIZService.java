package br.gov.mt.indea.sistemaformin.service;

import javax.ejb.Stateful;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.mt.indea.sistemaformin.entity.ParametrosSIZ;
import br.gov.mt.indea.sistemaformin.exception.ApplicationRuntimeException;

@Stateful
@TransactionManagement(TransactionManagementType.CONTAINER)
public class ParametrosSIZService extends BaseEntityService<ParametrosSIZ, Long> {

	private static final Logger log = LoggerFactory.getLogger(ParametrosSIZService.class);
	
	protected ParametrosSIZService() {
		super(ParametrosSIZ.class);
	}
	
	ParametrosSIZ parametrosSIZ;
	
	public ParametrosSIZ getInstance(){
		if (this.parametrosSIZ == null) {
		
			StringBuilder sql = new StringBuilder();
			sql.append("select parametrosSIZ ")
			   .append("  from ParametrosSIZ parametrosSIZ ");
			
			Query query = getSession().createQuery(sql.toString());
			parametrosSIZ = (ParametrosSIZ) query.uniqueResult();
		}
		
		return parametrosSIZ;
	}
	
	@Override
	public void saveOrUpdate(ParametrosSIZ parametrosSIZ) {
		super.saveOrUpdate(parametrosSIZ);
		this.parametrosSIZ = parametrosSIZ;
		
		log.info("Salvando Par�metros SIZ {}", parametrosSIZ.getId());
	}
	
	@Override
	public void delete(ParametrosSIZ parametrosSIZ) {
		log.info("Tentativa de remo��o do ParametroSIZ {}", parametrosSIZ.getId());
		throw new ApplicationRuntimeException("N�o � poss�vel remover os Par�metros do SIZ");
		
	}
	
	@Override
	public void validar(ParametrosSIZ ParametrosSIZ) {

	}

	@Override
	public void validarPersist(ParametrosSIZ ParametrosSIZ) {

	}

	@Override
	public void validarMerge(ParametrosSIZ ParametrosSIZ) {

	}

	@Override
	public void validarDelete(ParametrosSIZ ParametrosSIZ) {

	}

}
