package br.gov.mt.indea.sistemaformin.dao;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.inject.Inject;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import br.gov.mt.indea.sistemaformin.entity.TipoChavePrincipalTermoFiscalizacao;
import br.gov.mt.indea.sistemaformin.entity.TipoChaveSecundariaTermoFiscalizacao;
import br.gov.mt.indea.sistemaformin.enums.Dominio.AtivoInativo;
import br.gov.mt.indea.sistemaformin.exception.ApplicationException;

@Stateless
public class TipoChaveSecundariaTermoFiscalizacaoDAO extends DAO<TipoChaveSecundariaTermoFiscalizacao> {
	
	@Inject
	private EntityManager em;
	
	public TipoChaveSecundariaTermoFiscalizacaoDAO() {
		super(TipoChaveSecundariaTermoFiscalizacao.class);
	}

	@Override
	public EntityManager getEntityManager() {
		return this.em;
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void add(TipoChaveSecundariaTermoFiscalizacao tipoChaveSecundariaTermoFiscalizacao) throws ApplicationException{
		tipoChaveSecundariaTermoFiscalizacao = this.getEntityManager().merge(tipoChaveSecundariaTermoFiscalizacao);
		super.add(tipoChaveSecundariaTermoFiscalizacao);
	}
	
	public List<TipoChaveSecundariaTermoFiscalizacao> findAllByStatus(AtivoInativo status) throws ApplicationException{
		try{
			CriteriaBuilder cb = this.getEntityManager().getCriteriaBuilder();
			CriteriaQuery<TipoChaveSecundariaTermoFiscalizacao> query = cb.createQuery(TipoChaveSecundariaTermoFiscalizacao.class);
			
			Root<TipoChaveSecundariaTermoFiscalizacao> t = query.from(TipoChaveSecundariaTermoFiscalizacao.class);
			
			query.where(cb.equal(t.get("status"), status));
			query.orderBy(cb.asc(t.get("nome")));
			
			return this.getEntityManager().createQuery(query).getResultList();
		}catch(Exception e){
			throw new ApplicationException("N�o foi poss�vel buscar o registro", e);
		}
	}
	
	public List<TipoChaveSecundariaTermoFiscalizacao> findAllByTipoChavePrincipal(TipoChavePrincipalTermoFiscalizacao tipoChavePrincipalTermoFiscalizacao, AtivoInativo status) throws ApplicationException{
		try{
			CriteriaBuilder cb = this.getEntityManager().getCriteriaBuilder();
			CriteriaQuery<TipoChaveSecundariaTermoFiscalizacao> query = cb.createQuery(TipoChaveSecundariaTermoFiscalizacao.class);
			
			Root<TipoChaveSecundariaTermoFiscalizacao> t = query.from(TipoChaveSecundariaTermoFiscalizacao.class);
			
			List<Predicate> predicados = new ArrayList<Predicate>();
			
			if (status != null)
				predicados.add(cb.equal(t.get("status"), status));
			
			predicados.add(cb.equal(t.get("tipoChavePrincipalTermoFiscalizacao"), tipoChavePrincipalTermoFiscalizacao));
			
			query.where(cb.and(predicados.toArray(new Predicate[]{})));
			query.orderBy(cb.asc(t.get("nome")));
			
			return this.getEntityManager().createQuery(query).getResultList();
		}catch(Exception e){
			throw new ApplicationException("N�o foi poss�vel buscar o registro", e);
		}
	}

}
