package br.gov.mt.indea.sistemaformin.entity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

import br.gov.mt.indea.sistemaformin.enums.Dominio.MedidasAdotadasNoEstabelecimento;
import br.gov.mt.indea.sistemaformin.enums.Dominio.MomentoMedidasAdotadasNoEstabelecimento;
import br.gov.mt.indea.sistemaformin.enums.Dominio.ProvavelOrigemFormCOM;
import br.gov.mt.indea.sistemaformin.enums.Dominio.SimNao;
import br.gov.mt.indea.sistemaformin.enums.Dominio.SimNaoSI;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoInvestigacaoFormCOM;
import br.gov.mt.indea.sistemaformin.enums.Dominio.UsoDeMedicacaoFormCOM;

@Audited
@Entity
@Table(name="form_com")
public class FormCOM extends BaseEntity<Long>{

	private static final long serialVersionUID = 8625136333030085639L;
	
	@Id
	@SequenceGenerator(name="form_com_seq", sequenceName="form_com_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="form_com_seq")
	private Long id;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_usuario")
	private Usuario usuario;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_cadastro")
	private Calendar dataCadastro;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_investigacao")
	private Calendar dataInvestigacao;
	
	@Enumerated(EnumType.STRING)
	private SimNao retificador = SimNao.NAO;
	
	@ManyToOne
	@JoinColumn(name="id_formin")
	private FormIN formIN;
	
	@Column(name="numero_investigacao")
	private Long numeroInvestigacao;
	
	@Enumerated(EnumType.STRING)
	@Column(name="tipo_investigacao")
	private TipoInvestigacaoFormCOM tipoInvestigacao;
	
	@OneToMany(mappedBy="formCOM", cascade={CascadeType.ALL}, orphanRemoval=true)
	private List<ResultadoDeTesteDeDiagnostico> listaDeResultadoDeTesteDeDiagnosticos = new ArrayList<ResultadoDeTesteDeDiagnostico>();
	
	@Enumerated(EnumType.STRING)
	@Column(name="ha_diagnostico_conclusivo")
	private SimNao haDiagnosticoConclusivo = SimNao.NAO;
	
	@Column(name="diagnostico_conclusivo", length=255)
	private String diagnosticoConclusivo;
	
	@Enumerated(EnumType.STRING)
	@Column(name="provavel_origem")
	private ProvavelOrigemFormCOM provavelOrigem;
	
	@Column(name="outra_origem", length=255)
	private String outraOrigem;
	
	@Enumerated(EnumType.STRING)
	@Column(name="houve_vacinacoes")
	private SimNaoSI houveVacinacoes;
	
	@Enumerated(EnumType.STRING)
	@Column(name="houve_medicacoes")
	private UsoDeMedicacaoFormCOM houveMedicacoes;
	
	@Enumerated(EnumType.STRING)
	@Column(name="houve_transito")
	private SimNaoSI houveTransito;
	
	@OneToMany(mappedBy="formCOM", cascade={CascadeType.ALL}, orphanRemoval=true)
	private List<VacinacaoFormIN> vacinas = new ArrayList<VacinacaoFormIN>();
	
	@OneToMany(mappedBy="formCOM", cascade={CascadeType.ALL}, orphanRemoval=true)
	private List<Medicacao> medicacoes = new ArrayList<Medicacao>();
	
	@OneToMany(mappedBy="formCOM", cascade={CascadeType.ALL}, orphanRemoval=true)
	private List<TransitoDeAnimais> transitoDeAnimais = new ArrayList<TransitoDeAnimais>();
	
	@Column(name="descricao_achados", length=4000)
	private String descricaoDosAchadosDaOcorrencia;
	
	@OneToMany(mappedBy="formCOM", cascade={CascadeType.ALL}, orphanRemoval=true)
	private List<OcorrenciaObservadaFormCOM> ocorrenciasObservadas = new ArrayList<OcorrenciaObservadaFormCOM>();
	
	@Enumerated(EnumType.STRING)
	@Column(name="aplica_se_medidas_adotadas")
	private SimNao aplica_seMedidasAdotadas;
	
	@ElementCollection(targetClass = MedidasAdotadasNoEstabelecimento.class)
	@JoinTable(name = "formcom_medidas", joinColumns = @JoinColumn(name = "id_form_com", nullable = false))
	@Column(name = "id_formcom_medidas")
	@Enumerated(EnumType.STRING)
	private List<MedidasAdotadasNoEstabelecimento> medidas = new ArrayList<MedidasAdotadasNoEstabelecimento>();
	
	@Enumerated(EnumType.STRING)
	@Column(name="momento_medida_vazio_sanitario")
	private MomentoMedidasAdotadasNoEstabelecimento momentoMedidaVazioSanitario;
	
	@Enumerated(EnumType.STRING)
	@Column(name="momento_medida_animais_sent")
	private MomentoMedidasAdotadasNoEstabelecimento momentoMedidaAnimaisSentinelas;
	
	@Enumerated(EnumType.STRING)
	@Column(name="houve_coleta_de_amostras")
	private SimNao houveColetaDeAmostras = SimNao.NAO;
	
	@OneToMany(mappedBy="formCOM")
	@OrderBy("dataCadastro DESC")
	private List<FolhaAdicional> listaFolhasAdicionais = new ArrayList<FolhaAdicional>();
	
	@OneToMany(mappedBy="formCOM")
	@OrderBy("dataColheita DESC")
	private List<FormLAB> listaFormLAB = new ArrayList<FormLAB>();
	
	@OneToMany(mappedBy="formCOM")
	@OrderBy("dataInvestigacao DESC")
	private List<FormSV> listaFormSV = new ArrayList<FormSV>();
	
	@OneToMany(mappedBy="formCOM")
	@OrderBy("dataInvestigacao DESC")
	private List<FormSH> listaFormSH = new ArrayList<FormSH>();
	
	@OneToMany(mappedBy="formCOM")
	private List<FormSN> listaFormSN = new ArrayList<FormSN>();
	
	@OneToMany(mappedBy="formCOM")
	private List<FormSRN> listaFormSRN = new ArrayList<FormSRN>();
	
	@OneToMany(mappedBy="formCOM")
	private List<FormAIE> listaFormAIE = new ArrayList<FormAIE>();
	
	@OneToMany(mappedBy="formCOM")
	private List<FormMormo> listaFormMormo = new ArrayList<FormMormo>();
	
	@OneToMany(mappedBy="formCOM")
	private List<FormMaleina> listaFormMaleina = new ArrayList<FormMaleina>();
	
	@OneToMany(mappedBy="formCOM")
	private List<FormEQ> listaFormEQ = new ArrayList<FormEQ>();
	
	@OneToOne(cascade={CascadeType.ALL}, orphanRemoval=true)
	@JoinColumn(name="id_info_bovinos")
	private InformacoesBovinos_FormCOM bovinos = new InformacoesBovinos_FormCOM();
	
	@OneToOne(cascade={CascadeType.ALL}, orphanRemoval=true)
	@JoinColumn(name="id_info_bubalinos")
	private InformacoesBubalinos_FormCOM bubalinos = new InformacoesBubalinos_FormCOM();
	
	@OneToOne(cascade={CascadeType.ALL}, orphanRemoval=true)
	@JoinColumn(name="id_info_caprinos")
	private InformacoesCaprinos_FormCOM caprinos = new InformacoesCaprinos_FormCOM();
	
	@OneToOne(cascade={CascadeType.ALL}, orphanRemoval=true)
	@JoinColumn(name="id_info_ovinos")
	private InformacoesOvinos_FormCOM ovinos = new InformacoesOvinos_FormCOM();
	
	@OneToOne(cascade={CascadeType.ALL}, orphanRemoval=true)
	@JoinColumn(name="id_info_suinos")
	private InformacoesSuinos_FormCOM suinos = new InformacoesSuinos_FormCOM();
	
	@OneToOne(cascade={CascadeType.ALL}, orphanRemoval=true)
	@JoinColumn(name="id_info_equinos")
	private InformacoesEquinos_FormCOM equinos = new InformacoesEquinos_FormCOM();
	
	@OneToOne(cascade={CascadeType.ALL}, orphanRemoval=true)
	@JoinColumn(name="id_info_asininos")
	private InformacoesAsininos_FormCOM asininos = new InformacoesAsininos_FormCOM();
	
	@OneToOne(cascade={CascadeType.ALL}, orphanRemoval=true)
	@JoinColumn(name="id_info_muares")
	private InformacoesMuares_FormCOM muares = new InformacoesMuares_FormCOM();
	
	@OneToOne(cascade={CascadeType.ALL}, orphanRemoval=true)
	@JoinColumn(name="id_info_aves")
	private InformacoesAves_FormCOM aves = new InformacoesAves_FormCOM();
	
	@OneToOne(cascade={CascadeType.ALL}, orphanRemoval=true)
	@JoinColumn(name="id_info_abelhas")
	private InformacoesAbelhas_FormCOM abelhas = new InformacoesAbelhas_FormCOM();
	
	@OneToOne(cascade={CascadeType.ALL}, orphanRemoval=true)
	@JoinColumn(name="id_info_lagomorfos")
	private InformacoesLagomorfos_FormCOM lagomorfos = new InformacoesLagomorfos_FormCOM();
	
	@OneToOne(cascade={CascadeType.ALL}, orphanRemoval=true)
	@JoinColumn(name="id_info_outrosAnimais")
	private InformacoesOutrosAnimais_FormCOM outrosAnimais = new InformacoesOutrosAnimais_FormCOM();
	
	@OneToOne(cascade={CascadeType.ALL})
	@JoinColumn(name="id_veterinario")
	private br.gov.mt.indea.sistemaformin.webservice.entity.Veterinario veterinario;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Calendar getDataInvestigacao() {
		return dataInvestigacao;
	}

	public void setDataInvestigacao(Calendar dataInvestigacao) {
		this.dataInvestigacao = dataInvestigacao;
	}

	public SimNao getRetificador() {
		return retificador;
	}

	public void setRetificador(SimNao retificador) {
		this.retificador = retificador;
	}

	public FormIN getFormIN() {
		return formIN;
	}

	public void setFormIN(FormIN formIN) {
		this.formIN = formIN;
	}

	public Long getNumeroInvestigacao() {
		return numeroInvestigacao;
	}

	public void setNumeroInvestigacao(Long numeroInvestigacao) {
		this.numeroInvestigacao = numeroInvestigacao;
	}

	public TipoInvestigacaoFormCOM getTipoInvestigacao() {
		return tipoInvestigacao;
	}

	public void setTipoInvestigacao(TipoInvestigacaoFormCOM tipoInvestigacao) {
		this.tipoInvestigacao = tipoInvestigacao;
	}

	public List<ResultadoDeTesteDeDiagnostico> getListaDeResultadoDeTesteDeDiagnosticos() {
		return listaDeResultadoDeTesteDeDiagnosticos;
	}

	public void setListaDeResultadoDeTesteDeDiagnosticos(
			List<ResultadoDeTesteDeDiagnostico> listaDeResultadoDeTesteDeDiagnosticos) {
		this.listaDeResultadoDeTesteDeDiagnosticos = listaDeResultadoDeTesteDeDiagnosticos;
	}

	public SimNao getHaDiagnosticoConclusivo() {
		return haDiagnosticoConclusivo;
	}

	public void setHaDiagnosticoConclusivo(SimNao haDiagnosticoConclusivo) {
		this.haDiagnosticoConclusivo = haDiagnosticoConclusivo;
	}

	public ProvavelOrigemFormCOM getProvavelOrigem() {
		return provavelOrigem;
	}

	public void setProvavelOrigem(ProvavelOrigemFormCOM provavelOrigem) {
		this.provavelOrigem = provavelOrigem;
	}

	public String getOutraOrigem() {
		return outraOrigem;
	}

	public void setOutraOrigem(String outraOrigem) {
		this.outraOrigem = outraOrigem;
	}

	public List<VacinacaoFormIN> getVacinas() {
		return vacinas;
	}

	public void setVacinas(List<VacinacaoFormIN> vacinas) {
		this.vacinas = vacinas;
	}

	public List<Medicacao> getMedicacoes() {
		return medicacoes;
	}

	public void setMedicacoes(List<Medicacao> medicacoes) {
		this.medicacoes = medicacoes;
	}

	public List<TransitoDeAnimais> getTransitoDeAnimais() {
		return transitoDeAnimais;
	}

	public void setTransitoDeAnimais(List<TransitoDeAnimais> transitoDeAnimais) {
		this.transitoDeAnimais = transitoDeAnimais;
	}

	public String getDescricaoDosAchadosDaOcorrencia() {
		return descricaoDosAchadosDaOcorrencia;
	}

	public void setDescricaoDosAchadosDaOcorrencia(
			String descricaoDosAchadosDaOcorrencia) {
		this.descricaoDosAchadosDaOcorrencia = descricaoDosAchadosDaOcorrencia;
	}

	public List<OcorrenciaObservadaFormCOM> getOcorrenciasObservadas() {
		return ocorrenciasObservadas;
	}

	public void setOcorrenciasObservadas(
			List<OcorrenciaObservadaFormCOM> ocorrenciasObservadas) {
		this.ocorrenciasObservadas = ocorrenciasObservadas;
	}

	public List<MedidasAdotadasNoEstabelecimento> getMedidas() {
		return medidas;
	}

	public void setMedidas(List<MedidasAdotadasNoEstabelecimento> medidas) {
		this.medidas = medidas;
	}

	public MomentoMedidasAdotadasNoEstabelecimento getMomentoMedidaVazioSanitario() {
		return momentoMedidaVazioSanitario;
	}

	public void setMomentoMedidaVazioSanitario(
			MomentoMedidasAdotadasNoEstabelecimento momentoMedidaVazioSanitario) {
		this.momentoMedidaVazioSanitario = momentoMedidaVazioSanitario;
	}

	public MomentoMedidasAdotadasNoEstabelecimento getMomentoMedidaAnimaisSentinelas() {
		return momentoMedidaAnimaisSentinelas;
	}

	public void setMomentoMedidaAnimaisSentinelas(
			MomentoMedidasAdotadasNoEstabelecimento momentoMedidaAnimaisSentinelas) {
		this.momentoMedidaAnimaisSentinelas = momentoMedidaAnimaisSentinelas;
	}

	public SimNao getHouveColetaDeAmostras() {
		return houveColetaDeAmostras;
	}

	public void setHouveColetaDeAmostras(SimNao houveColetaDeAmostras) {
		this.houveColetaDeAmostras = houveColetaDeAmostras;
	}

	public Calendar getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Calendar dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public List<FolhaAdicional> getListaFolhasAdicionais() {
		return listaFolhasAdicionais;
	}

	public void setListaFolhasAdicionais(List<FolhaAdicional> listaFolhasAdicionais) {
		this.listaFolhasAdicionais = listaFolhasAdicionais;
	}

	public List<FormLAB> getListaFormLAB() {
		return listaFormLAB;
	}

	public void setListaFormLAB(List<FormLAB> listaFormLAB) {
		this.listaFormLAB = listaFormLAB;
	}

	public List<FormSV> getListaFormSV() {
		return listaFormSV;
	}

	public void setListaFormSV(List<FormSV> listaFormSV) {
		this.listaFormSV = listaFormSV;
	}

	public List<FormSH> getListaFormSH() {
		return listaFormSH;
	}

	public void setListaFormSH(List<FormSH> listaFormSH) {
		this.listaFormSH = listaFormSH;
	}

	public List<FormSN> getListaFormSN() {
		return listaFormSN;
	}

	public void setListaFormSN(List<FormSN> listaFormSN) {
		this.listaFormSN = listaFormSN;
	}

	public List<FormSRN> getListaFormSRN() {
		return listaFormSRN;
	}

	public void setListaFormSRN(List<FormSRN> listaFormSRN) {
		this.listaFormSRN = listaFormSRN;
	}

	public List<FormAIE> getListaFormAIE() {
		return listaFormAIE;
	}

	public void setListaFormAIE(List<FormAIE> listaFormAIE) {
		this.listaFormAIE = listaFormAIE;
	}

	public List<FormMormo> getListaFormMormo() {
		return listaFormMormo;
	}

	public void setListaFormMormo(List<FormMormo> listaFormMormo) {
		this.listaFormMormo = listaFormMormo;
	}

	public List<FormMaleina> getListaFormMaleina() {
		return listaFormMaleina;
	}

	public void setListaFormMaleina(List<FormMaleina> listaFormMaleina) {
		this.listaFormMaleina = listaFormMaleina;
	}

	public List<FormEQ> getListaFormEQ() {
		return listaFormEQ;
	}

	public void setListaFormEQ(List<FormEQ> listaFormEQ) {
		this.listaFormEQ = listaFormEQ;
	}

	public String getDiagnosticoConclusivo() {
		return diagnosticoConclusivo;
	}

	public void setDiagnosticoConclusivo(String diagnosticoConclusivo) {
		this.diagnosticoConclusivo = diagnosticoConclusivo;
	}

	public SimNao getAplica_seMedidasAdotadas() {
		return aplica_seMedidasAdotadas;
	}

	public void setAplica_seMedidasAdotadas(SimNao aplica_seMedidasAdotadas) {
		this.aplica_seMedidasAdotadas = aplica_seMedidasAdotadas;
	}

	public InformacoesBovinos_FormCOM getBovinos() {
		return bovinos;
	}

	public void setBovinos(InformacoesBovinos_FormCOM bovinos) {
		this.bovinos = bovinos;
	}

	public InformacoesBubalinos_FormCOM getBubalinos() {
		return bubalinos;
	}

	public void setBubalinos(InformacoesBubalinos_FormCOM bubalinos) {
		this.bubalinos = bubalinos;
	}

	public InformacoesCaprinos_FormCOM getCaprinos() {
		return caprinos;
	}

	public void setCaprinos(InformacoesCaprinos_FormCOM caprinos) {
		this.caprinos = caprinos;
	}

	public InformacoesOvinos_FormCOM getOvinos() {
		return ovinos;
	}

	public void setOvinos(InformacoesOvinos_FormCOM ovinos) {
		this.ovinos = ovinos;
	}

	public InformacoesSuinos_FormCOM getSuinos() {
		return suinos;
	}

	public void setSuinos(InformacoesSuinos_FormCOM suinos) {
		this.suinos = suinos;
	}

	public InformacoesEquinos_FormCOM getEquinos() {
		return equinos;
	}

	public void setEquinos(InformacoesEquinos_FormCOM equinos) {
		this.equinos = equinos;
	}

	public InformacoesAsininos_FormCOM getAsininos() {
		return asininos;
	}

	public void setAsininos(InformacoesAsininos_FormCOM asininos) {
		this.asininos = asininos;
	}

	public InformacoesMuares_FormCOM getMuares() {
		return muares;
	}

	public void setMuares(InformacoesMuares_FormCOM muares) {
		this.muares = muares;
	}

	public InformacoesAves_FormCOM getAves() {
		return aves;
	}

	public void setAves(InformacoesAves_FormCOM aves) {
		this.aves = aves;
	}

	public InformacoesAbelhas_FormCOM getAbelhas() {
		return abelhas;
	}

	public void setAbelhas(InformacoesAbelhas_FormCOM abelhas) {
		this.abelhas = abelhas;
	}

	public InformacoesLagomorfos_FormCOM getLagomorfos() {
		return lagomorfos;
	}

	public void setLagomorfos(InformacoesLagomorfos_FormCOM lagomorfos) {
		this.lagomorfos = lagomorfos;
	}

	public InformacoesOutrosAnimais_FormCOM getOutrosAnimais() {
		return outrosAnimais;
	}

	public void setOutrosAnimais(InformacoesOutrosAnimais_FormCOM outrosAnimais) {
		this.outrosAnimais = outrosAnimais;
	}

	public br.gov.mt.indea.sistemaformin.webservice.entity.Veterinario getVeterinario() {
		return veterinario;
	}

	public void setVeterinario(
			br.gov.mt.indea.sistemaformin.webservice.entity.Veterinario veterinario) {
		this.veterinario = veterinario;
	}

	public SimNaoSI getHouveVacinacoes() {
		return houveVacinacoes;
	}

	public void setHouveVacinacoes(SimNaoSI houveVacinacoes) {
		this.houveVacinacoes = houveVacinacoes;
	}

	public UsoDeMedicacaoFormCOM getHouveMedicacoes() {
		return houveMedicacoes;
	}

	public void setHouveMedicacoes(UsoDeMedicacaoFormCOM houveMedicacoes) {
		this.houveMedicacoes = houveMedicacoes;
	}

	public SimNaoSI getHouveTransito() {
		return houveTransito;
	}

	public void setHouveTransito(SimNaoSI houveTransito) {
		this.houveTransito = houveTransito;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FormCOM other = (FormCOM) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}