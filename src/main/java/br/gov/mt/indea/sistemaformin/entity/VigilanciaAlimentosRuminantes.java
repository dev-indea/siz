package br.gov.mt.indea.sistemaformin.entity;

import java.util.Calendar;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

import br.gov.mt.indea.sistemaformin.enums.Dominio.AtivaPassiva;
import br.gov.mt.indea.sistemaformin.enums.Dominio.FaseExploracaoBovinosEBubalinos;
import br.gov.mt.indea.sistemaformin.enums.Dominio.FinalidadeExploracaoBovinosEBubalinos;
import br.gov.mt.indea.sistemaformin.enums.Dominio.SimNao;
import br.gov.mt.indea.sistemaformin.enums.Dominio.SistemaDeCriacaoDeRuminantes;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoExploracaoAvicolaSistemaIndustrial;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoExploracaoOutrosRuminantes;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoExploracaoSuinosSistemaIndustrial;

@Audited
@Entity(name="vigilancia_alimentos_rumin")
public class VigilanciaAlimentosRuminantes extends BaseEntity<Long>{
	
	private static final long serialVersionUID = -361785479572430083L;

	@Id
	@SequenceGenerator(name="vigilancia_alimentos_rumin_seq", sequenceName="vigilancia_alimentos_rumin_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="vigilancia_alimentos_rumin_seq")
	private Long id;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_cadastro")
	private Calendar dataCadastro;
	
	@Enumerated(EnumType.STRING)
	@Column(name="finalidade_exp_ruminantes")
	private FinalidadeExploracaoBovinosEBubalinos finalidadeExploracaoBovinosEBubalinos;
	
	@ElementCollection(targetClass = TipoExploracaoOutrosRuminantes.class, fetch=FetchType.LAZY)
	@JoinTable(name = "vig_tipo_expl_outro_rum", joinColumns = @JoinColumn(name = "id_vigilancia_bov_bub", nullable = false))
	@Column(name = "id_vig_tipo_expl_outro_rum")
	@Enumerated(EnumType.STRING)
	private List<TipoExploracaoOutrosRuminantes> tipoExploracaoOutrosRuminantes;
	
	@Column(name="outro_tipo_exp_ruminantes")
	private String outroTipoExploracaoRuminante;
	
	@Enumerated(EnumType.STRING)
	@Column(name="sistema_criacao_ruminantes")
	private SistemaDeCriacaoDeRuminantes sistemaDeCriacaoDeRuminantes;
	
	@ElementCollection(targetClass = FaseExploracaoBovinosEBubalinos.class, fetch=FetchType.LAZY)
	@JoinTable(name = "vigilancia_fase_expl_bov", joinColumns = @JoinColumn(name = "id_vigilancia_bov_bub", nullable = false))
	@Column(name = "id_vigilancia_fase_expl_bov")
	@Enumerated(EnumType.STRING)
	private List<FaseExploracaoBovinosEBubalinos> sistemaDeCriacaoDeRuminantes2;
	
	@Column(name="outro_sistema_exp_ruminantes")
	private String outroSistemaDeCriacaoDeRuminante;
	
	@Column(name="quantidade_bovinos")
	private Integer quantidadeBovinos;
	
	@Column(name="quantidade_caprinos")
	private Integer quantidadeCaprinos;
	
	@Column(name="quantidade_ovinos")
	private Integer quantidadeOvinos;
	
	@Column(name="outra_especie_ruminante", length=200)
	private String outraEspecieRuminante;
	
	@Column(name="quantidade_outra_esp_rumin")
	private Integer quantidadeOutraEspecieRuminante;
	
	@Column(name="idade_rumin_alim_com_racao", length=200)
	private String idadeDosRuminantesAlimentadosComRacao;
	
	@Column(name="quantidade_bovinos_exp")
	private Integer quantidadeBovinosExpostos;
	
	@Column(name="quantidade_caprinos_exp")
	private Integer quantidadeCaprinosExpostos;
	
	@Column(name="quantidade_ovinos_exp")
	private Integer quantidadeOvinosExpostos;
	
	@Column(name="outra_especie_ruminante_exp", length=200)
	private String outraEspecieRuminanteExposta;
	
	@Column(name="quantidade_outra_esp_rumin_exp")
	private Integer quantidadeOutraEspecieRuminanteExpostos;
	
	@Column(name="tipo_alimentacao_ruminantes", length=200)
	private String tipoAlimentacaoRuminantes;
	
	@Column(name="epoca_ano_suplementacao", length=200)
	private String epocaDoAnoQueAconteceASuplementacao;
	
	@Enumerated(EnumType.STRING)
	@Column(name="tipo_exp_avicola_sist_indust")
	private TipoExploracaoAvicolaSistemaIndustrial tipoExploracaoAvicolaSistemaIndustrial;
	
	@Column(name="outro_tipo_expl_avic_indus", length=200)
	private String outroTipoExploracaoAvicolaSistemaIndustrial;
	
	@Enumerated(EnumType.STRING)
	@Column(name="presenca_cama_aviario")
	private SimNao presencaDeCamaDeAviario;
	
	@Column(name="observacao_cama_aviario", length=200)
	private String observacaoSobreCamaDeAviario;
	
	@Column(name="local_cama_aviario", length=200)
	private String localCamaAviario;
	
	@Enumerated(EnumType.STRING)
	@Column(name="cama_aviario_alimentacao_rum")
	private SimNao utilizacaoDeCamaDeAviarioNaAlimentacaoDeRuminantes;
	
	@Enumerated(EnumType.STRING)
	@Column(name="tipo_exp_suino_sist_indust")
	private TipoExploracaoSuinosSistemaIndustrial tipoExploracaoSuinosSistemaIndustrial;
	
	@Column(name="obs_tipo_exp_suino_sist_ind", length=200)
	private String observacaoTipoExploracaoSuinosSistemaIndustrial;
	
	@Enumerated(EnumType.STRING)
	@Column(name="piscicultura_aliment_base_racao")
	private SimNao pisciculturaComSistemaDeAlimentacaoABaseDeRacao;
	
	@Enumerated(EnumType.STRING)
	@Column(name="contaminacao_racaoPeixe_rumin")
	private SimNao contaminacaoCruzadaDeRacoesDePeixeERuminante;
	
	@Enumerated(EnumType.STRING)
	@Column(name="colheita_amostra_alimento_rum")
	private SimNao colheitaDeAmostraDeAlimentosDeRuminantes;
	
	@Enumerated(EnumType.STRING)
	@Column(name="tipo_fiscalizacao")
	private AtivaPassiva tipoFiscalizacao;
	
	@Column(name="numero_denuncia")
	private Integer numeroDenuncia;
	
	@Column(name="descricao_racoes", length=2000)
	private String descricaoDoSistemaDeArmazenamentoEElaboracaoDeRacoes;
	
	@Column(name="outras_observacoes", length=2000)
	private String outrasObservacoes;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Calendar getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Calendar dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public FinalidadeExploracaoBovinosEBubalinos getFinalidadeExploracaoBovinosEBubalinos() {
		return finalidadeExploracaoBovinosEBubalinos;
	}

	public void setFinalidadeExploracaoBovinosEBubalinos(
			FinalidadeExploracaoBovinosEBubalinos finalidadeExploracaoBovinosEBubalinos) {
		this.finalidadeExploracaoBovinosEBubalinos = finalidadeExploracaoBovinosEBubalinos;
	}

	public SistemaDeCriacaoDeRuminantes getSistemaDeCriacaoDeRuminantes() {
		return sistemaDeCriacaoDeRuminantes;
	}

	public void setSistemaDeCriacaoDeRuminantes(
			SistemaDeCriacaoDeRuminantes sistemaDeCriacaoDeRuminantes) {
		this.sistemaDeCriacaoDeRuminantes = sistemaDeCriacaoDeRuminantes;
	}
	public List<FaseExploracaoBovinosEBubalinos> getSistemaDeCriacaoDeRuminantes2() {
		return sistemaDeCriacaoDeRuminantes2;
	}

	public void setSistemaDeCriacaoDeRuminantes2(
			List<FaseExploracaoBovinosEBubalinos> sistemaDeCriacaoDeRuminantes2) {
		this.sistemaDeCriacaoDeRuminantes2 = sistemaDeCriacaoDeRuminantes2;
	}

	public String getOutroSistemaDeCriacaoDeRuminante() {
		return outroSistemaDeCriacaoDeRuminante;
	}

	public void setOutroSistemaDeCriacaoDeRuminante(
			String outroSistemaDeCriacaoDeRuminante) {
		this.outroSistemaDeCriacaoDeRuminante = outroSistemaDeCriacaoDeRuminante;
	}

	public Integer getQuantidadeBovinos() {
		return quantidadeBovinos;
	}

	public void setQuantidadeBovinos(Integer quantidadeBovinos) {
		this.quantidadeBovinos = quantidadeBovinos;
	}

	public Integer getQuantidadeCaprinos() {
		return quantidadeCaprinos;
	}

	public void setQuantidadeCaprinos(Integer quantidadeCaprinos) {
		this.quantidadeCaprinos = quantidadeCaprinos;
	}

	public Integer getQuantidadeOvinos() {
		return quantidadeOvinos;
	}

	public void setQuantidadeOvinos(Integer quantidadeOvinos) {
		this.quantidadeOvinos = quantidadeOvinos;
	}

	public String getOutraEspecieRuminante() {
		return outraEspecieRuminante;
	}

	public void setOutraEspecieRuminante(String outraEspecieRuminante) {
		this.outraEspecieRuminante = outraEspecieRuminante;
	}

	public Integer getQuantidadeOutraEspecieRuminante() {
		return quantidadeOutraEspecieRuminante;
	}

	public void setQuantidadeOutraEspecieRuminante(
			Integer quantidadeOutraEspecieRuminante) {
		this.quantidadeOutraEspecieRuminante = quantidadeOutraEspecieRuminante;
	}

	public String getIdadeDosRuminantesAlimentadosComRacao() {
		return idadeDosRuminantesAlimentadosComRacao;
	}

	public void setIdadeDosRuminantesAlimentadosComRacao(
			String idadeDosRuminantesAlimentadosComRacao) {
		this.idadeDosRuminantesAlimentadosComRacao = idadeDosRuminantesAlimentadosComRacao;
	}

	public Integer getQuantidadeBovinosExpostos() {
		return quantidadeBovinosExpostos;
	}

	public void setQuantidadeBovinosExpostos(Integer quantidadeBovinosExpostos) {
		this.quantidadeBovinosExpostos = quantidadeBovinosExpostos;
	}

	public Integer getQuantidadeCaprinosExpostos() {
		return quantidadeCaprinosExpostos;
	}

	public void setQuantidadeCaprinosExpostos(Integer quantidadeCaprinosExpostos) {
		this.quantidadeCaprinosExpostos = quantidadeCaprinosExpostos;
	}

	public Integer getQuantidadeOvinosExpostos() {
		return quantidadeOvinosExpostos;
	}

	public void setQuantidadeOvinosExpostos(Integer quantidadeOvinosExpostos) {
		this.quantidadeOvinosExpostos = quantidadeOvinosExpostos;
	}

	public String getOutraEspecieRuminanteExposta() {
		return outraEspecieRuminanteExposta;
	}

	public void setOutraEspecieRuminanteExposta(String outraEspecieRuminanteExposta) {
		this.outraEspecieRuminanteExposta = outraEspecieRuminanteExposta;
	}

	public Integer getQuantidadeOutraEspecieRuminanteExpostos() {
		return quantidadeOutraEspecieRuminanteExpostos;
	}

	public void setQuantidadeOutraEspecieRuminanteExpostos(
			Integer quantidadeOutraEspecieRuminanteExpostos) {
		this.quantidadeOutraEspecieRuminanteExpostos = quantidadeOutraEspecieRuminanteExpostos;
	}

	public String getTipoAlimentacaoRuminantes() {
		return tipoAlimentacaoRuminantes;
	}

	public void setTipoAlimentacaoRuminantes(String tipoAlimentacaoRuminantes) {
		this.tipoAlimentacaoRuminantes = tipoAlimentacaoRuminantes;
	}

	public String getEpocaDoAnoQueAconteceASuplementacao() {
		return epocaDoAnoQueAconteceASuplementacao;
	}

	public void setEpocaDoAnoQueAconteceASuplementacao(
			String epocaDoAnoQueAconteceASuplementacao) {
		this.epocaDoAnoQueAconteceASuplementacao = epocaDoAnoQueAconteceASuplementacao;
	}

	public SimNao getPresencaDeCamaDeAviario() {
		return presencaDeCamaDeAviario;
	}

	public void setPresencaDeCamaDeAviario(SimNao presencaDeCamaDeAviario) {
		this.presencaDeCamaDeAviario = presencaDeCamaDeAviario;
	}

	public String getObservacaoSobreCamaDeAviario() {
		return observacaoSobreCamaDeAviario;
	}

	public void setObservacaoSobreCamaDeAviario(String observacaoSobreCamaDeAviario) {
		this.observacaoSobreCamaDeAviario = observacaoSobreCamaDeAviario;
	}

	public String getLocalCamaAviario() {
		return localCamaAviario;
	}

	public void setLocalCamaAviario(String localCamaAviario) {
		this.localCamaAviario = localCamaAviario;
	}

	public SimNao getUtilizacaoDeCamaDeAviarioNaAlimentacaoDeRuminantes() {
		return utilizacaoDeCamaDeAviarioNaAlimentacaoDeRuminantes;
	}

	public void setUtilizacaoDeCamaDeAviarioNaAlimentacaoDeRuminantes(
			SimNao utilizacaoDeCamaDeAviarioNaAlimentacaoDeRuminantes) {
		this.utilizacaoDeCamaDeAviarioNaAlimentacaoDeRuminantes = utilizacaoDeCamaDeAviarioNaAlimentacaoDeRuminantes;
	}

	public TipoExploracaoSuinosSistemaIndustrial getTipoExploracaoSuinosSistemaIndustrial() {
		return tipoExploracaoSuinosSistemaIndustrial;
	}

	public void setTipoExploracaoSuinosSistemaIndustrial(
			TipoExploracaoSuinosSistemaIndustrial tipoExploracaoSuinosSistemaIndustrial) {
		this.tipoExploracaoSuinosSistemaIndustrial = tipoExploracaoSuinosSistemaIndustrial;
	}

	public String getObservacaoTipoExploracaoSuinosSistemaIndustrial() {
		return observacaoTipoExploracaoSuinosSistemaIndustrial;
	}

	public void setObservacaoTipoExploracaoSuinosSistemaIndustrial(
			String observacaoTipoExploracaoSuinosSistemaIndustrial) {
		this.observacaoTipoExploracaoSuinosSistemaIndustrial = observacaoTipoExploracaoSuinosSistemaIndustrial;
	}

	public SimNao getPisciculturaComSistemaDeAlimentacaoABaseDeRacao() {
		return pisciculturaComSistemaDeAlimentacaoABaseDeRacao;
	}

	public void setPisciculturaComSistemaDeAlimentacaoABaseDeRacao(
			SimNao pisciculturaComSistemaDeAlimentacaoABaseDeRacao) {
		this.pisciculturaComSistemaDeAlimentacaoABaseDeRacao = pisciculturaComSistemaDeAlimentacaoABaseDeRacao;
	}

	public SimNao getContaminacaoCruzadaDeRacoesDePeixeERuminante() {
		return contaminacaoCruzadaDeRacoesDePeixeERuminante;
	}

	public void setContaminacaoCruzadaDeRacoesDePeixeERuminante(
			SimNao contaminacaoCruzadaDeRacoesDePeixeERuminante) {
		this.contaminacaoCruzadaDeRacoesDePeixeERuminante = contaminacaoCruzadaDeRacoesDePeixeERuminante;
	}

	public SimNao getColheitaDeAmostraDeAlimentosDeRuminantes() {
		return colheitaDeAmostraDeAlimentosDeRuminantes;
	}

	public void setColheitaDeAmostraDeAlimentosDeRuminantes(
			SimNao colheitaDeAmostraDeAlimentosDeRuminantes) {
		this.colheitaDeAmostraDeAlimentosDeRuminantes = colheitaDeAmostraDeAlimentosDeRuminantes;
	}

	public AtivaPassiva getTipoFiscalizacao() {
		return tipoFiscalizacao;
	}

	public void setTipoFiscalizacao(AtivaPassiva tipoFiscalizacao) {
		this.tipoFiscalizacao = tipoFiscalizacao;
	}

	public Integer getNumeroDenuncia() {
		return numeroDenuncia;
	}

	public void setNumeroDenuncia(Integer numeroDenuncia) {
		this.numeroDenuncia = numeroDenuncia;
	}

	public String getDescricaoDoSistemaDeArmazenamentoEElaboracaoDeRacoes() {
		return descricaoDoSistemaDeArmazenamentoEElaboracaoDeRacoes;
	}

	public void setDescricaoDoSistemaDeArmazenamentoEElaboracaoDeRacoes(
			String descricaoDoSistemaDeArmazenamentoEElaboracaoDeRacoes) {
		this.descricaoDoSistemaDeArmazenamentoEElaboracaoDeRacoes = descricaoDoSistemaDeArmazenamentoEElaboracaoDeRacoes;
	}

	public List<TipoExploracaoOutrosRuminantes> getTipoExploracaoOutrosRuminantes() {
		return tipoExploracaoOutrosRuminantes;
	}

	public void setTipoExploracaoOutrosRuminantes(
			List<TipoExploracaoOutrosRuminantes> tipoExploracaoOutrosRuminantes) {
		this.tipoExploracaoOutrosRuminantes = tipoExploracaoOutrosRuminantes;
	}

	public String getOutroTipoExploracaoRuminante() {
		return outroTipoExploracaoRuminante;
	}

	public void setOutroTipoExploracaoRuminante(String outroTipoExploracaoRuminante) {
		this.outroTipoExploracaoRuminante = outroTipoExploracaoRuminante;
	}

	public TipoExploracaoAvicolaSistemaIndustrial getTipoExploracaoAvicolaSistemaIndustrial() {
		return tipoExploracaoAvicolaSistemaIndustrial;
	}

	public void setTipoExploracaoAvicolaSistemaIndustrial(
			TipoExploracaoAvicolaSistemaIndustrial tipoExploracaoAvicolaSistemaIndustrial) {
		this.tipoExploracaoAvicolaSistemaIndustrial = tipoExploracaoAvicolaSistemaIndustrial;
	}

	public String getOutroTipoExploracaoAvicolaSistemaIndustrial() {
		return outroTipoExploracaoAvicolaSistemaIndustrial;
	}

	public void setOutroTipoExploracaoAvicolaSistemaIndustrial(
			String outroTipoExploracaoAvicolaSistemaIndustrial) {
		this.outroTipoExploracaoAvicolaSistemaIndustrial = outroTipoExploracaoAvicolaSistemaIndustrial;
	}

	public String getOutrasObservacoes() {
		return outrasObservacoes;
	}

	public void setOutrasObservacoes(String outrasObservacoes) {
		this.outrasObservacoes = outrasObservacoes;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VigilanciaAlimentosRuminantes other = (VigilanciaAlimentosRuminantes) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}