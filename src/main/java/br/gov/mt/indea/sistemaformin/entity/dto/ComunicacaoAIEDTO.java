package br.gov.mt.indea.sistemaformin.entity.dto;

import java.io.Serializable;
import java.util.Date;

import br.gov.mt.indea.sistemaformin.entity.Laboratorio;

public class ComunicacaoAIEDTO implements AbstractDTO, Serializable{
	
	private static final long serialVersionUID = 6205763755360605521L;

	private Laboratorio laboratorio;
	
    private Date dataComunicacao;
	
	public boolean isNull(){
		if (laboratorio == null && dataComunicacao == null)
			return true;
		return false;
	}

	public Laboratorio getLaboratorio() {
		return laboratorio;
	}

	public void setLaboratorio(Laboratorio laboratorio) {
		this.laboratorio = laboratorio;
	}

	public Date getDataComunicacao() {
		return dataComunicacao;
	}

	public void setDataComunicacao(Date dataComunicacao) {
		this.dataComunicacao = dataComunicacao;
	}

}