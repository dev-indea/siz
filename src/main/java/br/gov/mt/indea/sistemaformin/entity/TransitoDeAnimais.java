package br.gov.mt.indea.sistemaformin.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import org.hibernate.envers.Audited;

import br.gov.mt.indea.sistemaformin.webservice.entity.Gta;

@Audited
@Entity(name="transito_de_animais")
public class TransitoDeAnimais extends BaseEntity<Long> implements Comparable<TransitoDeAnimais>, Cloneable {

	private static final long serialVersionUID = -2001105170185778553L;

	@Id
	@SequenceGenerator(name="transito_de_animais_seq", sequenceName="transito_de_animais_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="transito_de_animais_seq")
	private Long id;
	
	@OneToOne(cascade={CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
	@JoinColumn(name="id_gta")
	private Gta gta;
	
	@ManyToOne
	@JoinColumn(name="id_formIN")
	private FormIN formIN;

	@ManyToOne
	@JoinColumn(name="id_formCOM")
	private FormCOM formCOM;
	
	public TransitoDeAnimais(){
		
	}
	
	public TransitoDeAnimais(Gta gta){
		this.gta = gta;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Gta getGta() {
		return gta;
	}

	public void setGta(Gta gta) {
		this.gta = gta;
	}

	public FormIN getFormIN() {
		return formIN;
	}

	public void setFormIN(FormIN formIN) {
		this.formIN = formIN;
	}
	
	public FormCOM getFormCOM() {
		return formCOM;
	}

	public void setFormCOM(FormCOM formCOM) {
		this.formCOM = formCOM;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((formCOM == null) ? 0 : formCOM.hashCode());
		result = prime * result + ((gta == null) ? 0 : gta.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TransitoDeAnimais other = (TransitoDeAnimais) obj;
		if (formCOM == null) {
			if (other.formCOM != null)
				return false;
		} else if (!formCOM.equals(other.formCOM))
			return false;
		if (gta == null) {
			if (other.gta != null)
				return false;
		} else if (!gta.equals(other.gta))
			return false;
		return true;
	}

	@Override
	public int compareTo(TransitoDeAnimais o) {
		if (this.getGta().getNumero() < o.getGta().getNumero())
			return -1;
		else if (this.getGta().getNumero() == o.getGta().getNumero())
			return 0;
		else
			return 1;
	}
	
	protected Object clone(FormIN formIN) throws CloneNotSupportedException {
		TransitoDeAnimais cloned = (TransitoDeAnimais) super.clone();
		
		cloned.setId(null);
		cloned.setFormIN(formIN);
		
		return cloned;
	}
	
}
