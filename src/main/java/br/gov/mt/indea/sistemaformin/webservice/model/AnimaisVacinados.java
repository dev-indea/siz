package br.gov.mt.indea.sistemaformin.webservice.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class AnimaisVacinados implements Serializable {

	private static final long serialVersionUID = -8445456889647366753L;

	private Long id;
    
    private String qntVacinados;
    
    @XmlElement(name = "especie")
    private Especie especie;
    
    @XmlElement(name = "estratificacao")
    private Estratificacao estratificacao;

    public AnimaisVacinados() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getQntVacinados() {
        return qntVacinados;
    }

    public void setQntVacinados(String qntVacinados) {
        this.qntVacinados = qntVacinados;
    }

    public Especie getEspecie() {
        return especie;
    }

    public void setEspecie(Especie especie) {
        this.especie = especie;
    }

    public Estratificacao getEstratificacao() {
        return estratificacao;
    }

    public void setEstratificacao(Estratificacao estratificacao) {
        this.estratificacao = estratificacao;
    }
}
