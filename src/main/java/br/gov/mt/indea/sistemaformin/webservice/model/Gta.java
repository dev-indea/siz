package br.gov.mt.indea.sistemaformin.webservice.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Gta implements Serializable, Comparable<Gta> {

	private static final long serialVersionUID = -5279637163168640688L;

	private Long id;

    @XmlElement
    private Pessoa emitente;

    @XmlElement
    private FinalidadeGta finalidade;
    
    @XmlElement
    private TipoEmitente tipoEmitente;
   
    @XmlElement
    private GrupoEspecie grupoEspecie;
    
    @XmlElement
    private Especie especie;
    
    @XmlElement
    private Uf ufOrigem;
    
    @XmlElement
    private Uf ufDestino;
    
    @XmlElement
    private TipoGta tipoGta;
   
    @XmlElement
    private PessoaDestinoGta pessoaDestino;
    
    @XmlElement
    private PessoaOrigemGta pessoaOrigemGta;
    
    @XmlElement
    private SituacaoGta situacaoGta;
    
    @XmlElement
    private List<EstratificacaoGta> estratificacoes;
    
    private Integer numero;
    
    private String serie;
    
    private Date dataEmissao;
    
    private Date dataValidade;
    
    private Date dataSaida;
    
    private Date dataChegada;
    
    private Date dataDigitacaoEntrada;
    
    private Date dataCancelamento;
    
    private Date dataAtualizacao;
    
    private boolean selecionadoParaInclusao;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Pessoa getEmitente() {
        return emitente;
    }

    public void setEmitente(Pessoa emitente) {
        this.emitente = emitente;
    }

    public FinalidadeGta getFinalidade() {
        return finalidade;
    }

    public void setFinalidade(FinalidadeGta finalidade) {
        this.finalidade = finalidade;
    }

    public GrupoEspecie getGrupoEspecie() {
        return grupoEspecie;
    }

    public void setGrupoEspecie(GrupoEspecie grupoEspecie) {
        this.grupoEspecie = grupoEspecie;
    }

    public Especie getEspecie() {
        return especie;
    }

    public void setEspecie(Especie especie) {
        this.especie = especie;
    }

    public Uf getUfOrigem() {
        return ufOrigem;
    }

    public void setUfOrigem(Uf ufOrigem) {
        this.ufOrigem = ufOrigem;
    }

    public Uf getUfDestino() {
        return ufDestino;
    }

    public void setUfDestino(Uf ufDestino) {
        this.ufDestino = ufDestino;
    }

    public TipoGta getTipoGta() {
        return tipoGta;
    }

    public void setTipoGta(TipoGta tipoGta) {
        this.tipoGta = tipoGta;
    }

    public SituacaoGta getSituacaoGta() {
        return situacaoGta;
    }

    public void setSituacaoGta(SituacaoGta situacaoGta) {
        this.situacaoGta = situacaoGta;
    }

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public TipoEmitente getTipoEmitente() {
		return tipoEmitente;
	}

	public void setTipoEmitente(TipoEmitente tipoEmitente) {
		this.tipoEmitente = tipoEmitente;
	}

	public PessoaDestinoGta getPessoaDestino() {
		return pessoaDestino;
	}

	public void setPessoaDestino(PessoaDestinoGta pessoaDestino) {
		this.pessoaDestino = pessoaDestino;
	}

	public PessoaOrigemGta getPessoaOrigemGta() {
		return pessoaOrigemGta;
	}

	public void setPessoaOrigemGta(PessoaOrigemGta pessoaOrigemGta) {
		this.pessoaOrigemGta = pessoaOrigemGta;
	}

	public Date getDataEmissao() {
		return dataEmissao;
	}

	public void setDataEmissao(Date dataEmissao) {
		this.dataEmissao = dataEmissao;
	}

	public Date getDataValidade() {
		return dataValidade;
	}

	public void setDataValidade(Date dataValidade) {
		this.dataValidade = dataValidade;
	}

	public Date getDataSaida() {
		return dataSaida;
	}

	public void setDataSaida(Date dataSaida) {
		this.dataSaida = dataSaida;
	}

	public Date getDataChegada() {
		return dataChegada;
	}

	public void setDataChegada(Date dataChegada) {
		this.dataChegada = dataChegada;
	}

	public Date getDataDigitacaoEntrada() {
		return dataDigitacaoEntrada;
	}

	public void setDataDigitacaoEntrada(Date dataDigitacaoEntrada) {
		this.dataDigitacaoEntrada = dataDigitacaoEntrada;
	}

	public Date getDataCancelamento() {
		return dataCancelamento;
	}

	public void setDataCancelamento(Date dataCancelamento) {
		this.dataCancelamento = dataCancelamento;
	}

	public Date getDataAtualizacao() {
		return dataAtualizacao;
	}

	public void setDataAtualizacao(Date dataAtualizacao) {
		this.dataAtualizacao = dataAtualizacao;
	}

	public boolean isSelecionadoParaInclusao() {
		return selecionadoParaInclusao;
	}

	public void setSelecionadoParaInclusao(boolean selecionadoParaInclusao) {
		this.selecionadoParaInclusao = selecionadoParaInclusao;
	}

	public List<EstratificacaoGta> getEstratificacoes() {
		return estratificacoes;
	}

	public void setEstratificacoes(List<EstratificacaoGta> estratificacoes) {
		this.estratificacoes = estratificacoes;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((numero == null) ? 0 : numero.hashCode());
		result = prime * result + ((serie == null) ? 0 : serie.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Gta other = (Gta) obj;
		if (numero == null) {
			if (other.numero != null)
				return false;
		} else if (!numero.equals(other.numero))
			return false;
		if (serie == null) {
			if (other.serie != null)
				return false;
		} else if (!serie.equals(other.serie))
			return false;
		return true;
	}
	
	@Override
	public int compareTo(Gta o) {
		if (this.getNumero() < o.getNumero())
			return -1;
		else if (this.getNumero() == o.getNumero())
			return 0;
		else
			return 1;
	}

}
