package br.gov.mt.indea.sistemaformin.util;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.faces.context.FacesContext;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.collections.map.MultiKeyMap;
import org.apache.commons.io.IOUtils;
import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.util.PDFMergerUtility;

import br.gov.mt.indea.sistemaformin.entity.AbstractAmostra;
import br.gov.mt.indea.sistemaformin.entity.AvaliacaoClinicaEQ;
import br.gov.mt.indea.sistemaformin.entity.FolhaAdicional;
import br.gov.mt.indea.sistemaformin.entity.FormAIE;
import br.gov.mt.indea.sistemaformin.entity.FormCOM;
import br.gov.mt.indea.sistemaformin.entity.FormEQ;
import br.gov.mt.indea.sistemaformin.entity.FormIN;
import br.gov.mt.indea.sistemaformin.entity.FormLAB;
import br.gov.mt.indea.sistemaformin.entity.FormMaleina;
import br.gov.mt.indea.sistemaformin.entity.FormMormo;
import br.gov.mt.indea.sistemaformin.entity.FormSH;
import br.gov.mt.indea.sistemaformin.entity.FormSN;
import br.gov.mt.indea.sistemaformin.entity.FormSRN;
import br.gov.mt.indea.sistemaformin.entity.FormSV;
import br.gov.mt.indea.sistemaformin.entity.FormVIN;
import br.gov.mt.indea.sistemaformin.entity.FormularioDeColheitaTroncoEncefalico;
import br.gov.mt.indea.sistemaformin.entity.InformacoesComplementaresEspeciesFormLAB;
import br.gov.mt.indea.sistemaformin.entity.InformacoesComplementaresMedicamentosFormLAB;
import br.gov.mt.indea.sistemaformin.entity.Resenho;
import br.gov.mt.indea.sistemaformin.entity.VacinacaoEQ;
import br.gov.mt.indea.sistemaformin.enums.Dominio.DoencasRespiratorias;
import br.gov.mt.indea.sistemaformin.enums.Dominio.OrigemDoAnimal;
import br.gov.mt.indea.sistemaformin.exception.ApplicationErrorException;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

public class GeradorDeRelatorio {
	
	public static byte[] gerarPdfFolhaAdicional(List<FolhaAdicional> listaDeFolhasAdicionais) throws JRException{
		JasperReport report = JasperCompileManager.compileReport(FacesContext.getCurrentInstance().getExternalContext().getRealPath("/") + "/resources/relatorios/FolhaAdicional.jrxml");
		JasperPrint print = JasperFillManager.fillReport(report, new HashMap<String, Object>(), new JRBeanCollectionDataSource(listaDeFolhasAdicionais));
		return JasperExportManager.exportReportToPdf(print);
	}
	
	public static byte[] gerarPdfFormVIN(List<FormVIN> listaDeFormVIN) throws JRException{
		JasperReport report = JasperCompileManager.compileReport(FacesContext.getCurrentInstance().getExternalContext().getRealPath("/") + "/resources/relatorios/FormVIN.jrxml");
		JasperPrint print = JasperFillManager.fillReport(report, new HashMap<String, Object>(), new JRBeanCollectionDataSource(listaDeFormVIN));
		return JasperExportManager.exportReportToPdf(print);
	}
	
	public static byte[] gerarPdfFormLAB(FormLAB formLAB, OutputStream outputStream) throws ApplicationErrorException{
		List<FormLAB> lista = new ArrayList<FormLAB>();
		lista.add(formLAB);
		
		byte[] pdf = null;
		
		try{
			
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("INFORMACOES_COMPLEMENTARES_ESPECIES", getListaDeInformacoesComplementaresEspeciesFormLAB_FolhaAtual(formLAB, 0));
			map.put("INFORMACOES_COMPLEMENTARES_MEDICAMENTOS", getListaDeInformacoesComplementaresMedicamentosFormLAB_FolhaAtual(formLAB, 0));
			
			pdf = GeradorDeRelatorio.gerarPdfFormLAB_FolhaPrincipal(lista, map);
			
			PDFMergerUtility mergePdf = new PDFMergerUtility();
			mergePdf.addSource(new ByteArrayInputStream(pdf));
			
			if (formLAB.getAmostrasSoroSanguineo().size() > 14 ||
					formLAB.getOutrasAmostras().size() > 5 ){
					
					int totalFolhasAdicionais = getTotalFolhasAdicionais(formLAB);
					
					byte[] pdfAdicional;
					for (int i = 1; i <= totalFolhasAdicionais; i++) {
						HashMap<String, Object> map2 = new HashMap<String, Object>();
						map2.put("TOTAL_FOLHAS_ADICIONAIS", totalFolhasAdicionais);
						map2.put("FOLHAS_ADICIONAIS_ATUAL", i);
						map2.put("INFORMACOES_COMPLEMENTARES_ESPECIES", getListaDeInformacoesComplementaresEspeciesFormLAB_FolhaAtual(formLAB, i));
						map2.put("INFORMACOES_COMPLEMENTARES_MEDICAMENTOS", getListaDeInformacoesComplementaresMedicamentosFormLAB_FolhaAtual(formLAB, i));
						
						pdfAdicional = GeradorDeRelatorio.gerarPdfFormLAB_FolhaAdicional(lista, map2);
						
						mergePdf.addSource(new ByteArrayInputStream(pdfAdicional));
					}
					
				}
			
			mergePdf.setDestinationStream(outputStream);
			mergePdf.mergeDocuments();
			
		} catch (JRException e) {;
			e.printStackTrace();
			throw new ApplicationErrorException("Erro ao abrir o pdf", e);
		} catch (COSVisitorException e) {
			e.printStackTrace();
			throw new ApplicationErrorException("Erro ao abrir o pdf", e);
		} catch (IOException e) {
			e.printStackTrace();
			throw new ApplicationErrorException("Erro ao abrir o pdf", e);
		}
	
		return pdf;
	}
	
	private static int getTotalFolhasAdicionais(FormLAB formLAB){
		int tAmostras = 0;
		int tOutrasAmostras = 0;
		int tEspecies = 0;
		int tMedicacoes = 0;
		
		if (formLAB.getAmostrasSoroSanguineo().size() <= 14)
			tAmostras = 0;
		else{
			int registrosFolhasAdicionais = formLAB.getAmostrasSoroSanguineo().size() - 14;
			tAmostras = registrosFolhasAdicionais/38 +(registrosFolhasAdicionais % 38 == 0 ? 0 : 1);
		}
		
		if (formLAB.getOutrasAmostras().size() <= 5)
			tOutrasAmostras = 0;
		else{
			int registrosFolhasAdicionais = formLAB.getOutrasAmostras().size() - 5;
			tOutrasAmostras = registrosFolhasAdicionais / 12 +(registrosFolhasAdicionais % 12 == 0 ? 0 : 1);
		}
		
		if (tAmostras > tOutrasAmostras && tAmostras > tEspecies && tAmostras > tMedicacoes)
			return tAmostras;
		else if (tOutrasAmostras > tEspecies && tOutrasAmostras > tMedicacoes)
			return tOutrasAmostras;
		else if(tEspecies > tMedicacoes)
			return tEspecies;
		else
			return tMedicacoes;
	}
	
	@SuppressWarnings("unchecked")
	private static List<InformacoesComplementaresEspeciesFormLAB> getListaDeInformacoesComplementaresEspeciesFormLAB_FolhaAtual(FormLAB formLAB, int folhaAtual){
		List<InformacoesComplementaresEspeciesFormLAB> response = new ArrayList<InformacoesComplementaresEspeciesFormLAB>();
		
		List<AbstractAmostra> amostras = new ArrayList<AbstractAmostra>();
		
		if (folhaAtual == 0){
			amostras.addAll(ListUtil.getList(0, 14, formLAB.getAmostrasSoroSanguineo()));
			amostras.addAll(ListUtil.getList(0, 5, formLAB.getOutrasAmostras()));
		}else{
			amostras.addAll(ListUtil.getList(Integer.parseInt(((38*folhaAtual)-14) + ""), 
											 Integer.parseInt(((38*folhaAtual)-14+19) + ""), 
											 formLAB.getAmostrasSoroSanguineo()));
		    
		    amostras.addAll(ListUtil.getList(Integer.parseInt(((12*folhaAtual)-7) + ""), 
								    		 Integer.parseInt(((12*folhaAtual)+5) + ""), 
								    		 formLAB.getOutrasAmostras()));
		}
		
		for (AbstractAmostra abstractAmostra : amostras) {
			response.addAll(abstractAmostra.getInformacoesComplementaresEspeciesFormLAB());
		}
		
		return response;
	}
	
	@SuppressWarnings("unchecked")
	private static List<InformacoesComplementaresMedicamentosFormLAB> getListaDeInformacoesComplementaresMedicamentosFormLAB_FolhaAtual(FormLAB formLAB, int folhaAtual){
		List<InformacoesComplementaresMedicamentosFormLAB> response = new ArrayList<InformacoesComplementaresMedicamentosFormLAB>();
		
		List<AbstractAmostra> amostras = new ArrayList<AbstractAmostra>();
		
		if (folhaAtual == 0){
			amostras.addAll(ListUtil.getList(0, 14, formLAB.getAmostrasSoroSanguineo()));
			amostras.addAll(ListUtil.getList(0, 5, formLAB.getOutrasAmostras()));
		}else{
			amostras.addAll(ListUtil.getList(Integer.parseInt(((38*folhaAtual)-14) + ""), 
											 Integer.parseInt(((38*folhaAtual)-14+19) + ""), 
											 formLAB.getAmostrasSoroSanguineo()));
		    
		    amostras.addAll(ListUtil.getList(Integer.parseInt(((12*folhaAtual)-7) + ""), 
								    		 Integer.parseInt(((12*folhaAtual)+5) + ""), 
								    		 formLAB.getOutrasAmostras()));
		}
		
		for (AbstractAmostra abstractAmostra : amostras) {
			response.addAll(abstractAmostra.getInformacoesComplementaresMedicamentosFormLAB());
		}
		
		return response;
	}
	
	private static byte[] gerarPdfFormLAB_FolhaPrincipal(List<FormLAB> listaDeFormLAB, HashMap<String, Object> map) throws JRException{
		JasperReport report = JasperCompileManager.compileReport(FacesContext.getCurrentInstance().getExternalContext().getRealPath("/") + "/resources/relatorios/FormLAB.jrxml");
		JasperPrint print = JasperFillManager.fillReport(report, map, new JRBeanCollectionDataSource(listaDeFormLAB));
		return JasperExportManager.exportReportToPdf(print);
	}
	
	private static byte[] gerarPdfFormLAB_FolhaAdicional(List<FormLAB> listaDeFormLAB_FolhaAdicional, HashMap<String, Object> map) throws JRException{
		JasperReport report = JasperCompileManager.compileReport(FacesContext.getCurrentInstance().getExternalContext().getRealPath("/") + "/resources/relatorios/FormLAB_FolhaAdicional.jrxml");
		JasperPrint print = JasperFillManager.fillReport(report, map, new JRBeanCollectionDataSource(listaDeFormLAB_FolhaAdicional));
		return JasperExportManager.exportReportToPdf(print);
	}
	
	public static byte[] gerarPdfFormSV(List<FormSV> listaDeFormSV) throws JRException{
		JasperReport report = JasperCompileManager.compileReport(FacesContext.getCurrentInstance().getExternalContext().getRealPath("/") + "/resources/relatorios/FormSV.jrxml");
		JasperPrint print = JasperFillManager.fillReport(report, new HashMap<String, Object>(), new JRBeanCollectionDataSource(listaDeFormSV));
		return JasperExportManager.exportReportToPdf(print);
	}
	
	public static byte[] gerarPdfFormSH(List<FormSH> listaDeFormSH) throws JRException{
		JasperReport report = JasperCompileManager.compileReport(FacesContext.getCurrentInstance().getExternalContext().getRealPath("/") + "/resources/relatorios/FormSH.jrxml");
		JasperPrint print = JasperFillManager.fillReport(report, new HashMap<String, Object>(), new JRBeanCollectionDataSource(listaDeFormSH));
		return JasperExportManager.exportReportToPdf(print);
	}
	
	public static byte[] gerarPdfFormSN(List<FormSN> listaDeFormSN) throws JRException{
		JasperReport report = JasperCompileManager.compileReport(FacesContext.getCurrentInstance().getExternalContext().getRealPath("/") + "/resources/relatorios/FormSN.jrxml");
		JasperPrint print = JasperFillManager.fillReport(report, new HashMap<String, Object>(), new JRBeanCollectionDataSource(listaDeFormSN));
		return JasperExportManager.exportReportToPdf(print);
	}
	
	public static byte[] gerarPdfFormSRN(List<FormSRN> listaDeFormSRN) throws JRException{
		JasperReport report = JasperCompileManager.compileReport(FacesContext.getCurrentInstance().getExternalContext().getRealPath("/") + "/resources/relatorios/FormSRN.jrxml");
		JasperPrint print = JasperFillManager.fillReport(report, new HashMap<String, Object>(), new JRBeanCollectionDataSource(listaDeFormSRN));
		return JasperExportManager.exportReportToPdf(print);
	}
	
	public static byte[] gerarPdfFormEQ(FormEQ formEQ, OutputStream outputStream) throws ApplicationErrorException{
		List<FormEQ> lista = new ArrayList<FormEQ>();
		lista.add(formEQ);
		
		byte[] pdf = null;
		
		try{
		
			PDFMergerUtility mergePdf = new PDFMergerUtility();
			
			int totalFolhasAdicionais = getTotalFolhas(formEQ);
			
			byte[] pdfAdicional;
			
			int folhaAtual = 1;
			
			do {
				HashMap<String, Object> map = new HashMap<String, Object>();
				map.put("TOTAL_FOLHAS_ADICIONAIS", totalFolhasAdicionais);
				map.put("FOLHAS_ADICIONAIS_ATUAL", folhaAtual);
				map.put("IDS_NASCIDOS_NO_ESTABELECIMENTO", getIDsDeAnimaisNascidosNoEstabelecimento(formEQ, folhaAtual));
				map.put("IDS_NASCIDOS_EM_OUTRO_ESTABELECIMENTO_DO_MUNICIPIO", getIDsDeAnimaisNascidosEmOutroEstabelecimentoDoMunicipio(formEQ, folhaAtual));
				map.put("IDS_NASCIDOS_EM_OUTRO_MUNICIPIO_DO_ESTADO", getIDsDeAnimaisNascidosEmOutroMunicipioDoEstado(formEQ, folhaAtual));
				map.put("IDS_NASCIDOS_EM_OUTRO_ESTADO", getIDsDeAnimaisNascidosEmOutroEstado(formEQ, folhaAtual));
				map.put("IDS_HISTORICO_GARROTILHO", getIDsDeAnimaisComHistoricoDeGarrotilho(formEQ, folhaAtual));
				map.put("IDS_HISTORICO_LINFAGITE", getIDsDeAnimaisComHistoricoDeLinfagite(formEQ, folhaAtual));
				map.put("IDS_HISTORICO_INFLUENZA", getIDsDeAnimaisComHistoricoDeInfluenza(formEQ, folhaAtual));
				map.put("IDS_HISTORICO_ESPOROTRICOSE", getIDsDeAnimaisComHistoricoDeEsporotricose(formEQ, folhaAtual));
				map.put("IDS_HISTORICO_BOTRIOMICOSE", getIDsDeAnimaisComHistoricoDeBotriomicose(formEQ, folhaAtual));
				map.put("HISTORICO_VACINACAO", getHistoricoDeVacinacaoPorDataEDoenca(formEQ, folhaAtual));
				
				pdfAdicional = GeradorDeRelatorio.gerarPdfFormEQ_PrimeiraFolha(lista, map);
				//totalLength += pdfAdicional.length;
				
				mergePdf.addSource(new ByteArrayInputStream(pdfAdicional));
				folhaAtual++;
			} while (folhaAtual <= totalFolhasAdicionais);
			
			mergePdf.setDestinationStream(outputStream);
			mergePdf.mergeDocuments();
			
		} catch (JRException e) {;
			e.printStackTrace();
			throw new ApplicationErrorException("Erro ao abrir o pdf", e);
		} catch (COSVisitorException e) {
			e.printStackTrace();
			throw new ApplicationErrorException("Erro ao abrir o pdf", e);
		} catch (IOException e) {
			e.printStackTrace();
			throw new ApplicationErrorException("Erro ao abrir o pdf", e);
		}
	
		return pdf;
	}
	
	private static byte[] gerarPdfFormEQ_PrimeiraFolha(List<FormEQ> listaDeFormEQ, HashMap<String, Object> map) throws JRException{
		JasperReport report = JasperCompileManager.compileReport(FacesContext.getCurrentInstance().getExternalContext().getRealPath("/") + "/resources/relatorios/FormEQ.jrxml");
		JasperPrint print = JasperFillManager.fillReport(report, map, new JRBeanCollectionDataSource(listaDeFormEQ));
		return JasperExportManager.exportReportToPdf(print);
	}
	
	private static int getTotalFolhas(FormEQ formEQ){
		int tAvaliacoes = 0;
		
		tAvaliacoes = formEQ.getAvaliacoesClinicas().size()/10;
		if (formEQ.getAvaliacoesClinicas().size()%10 != 0)
			tAvaliacoes += 1;
		
		return tAvaliacoes;
	}
	
	private static String getIDsDeAnimaisNascidosNoEstabelecimento(FormEQ formEQ, int folhaAtual){
		@SuppressWarnings("unchecked")
		List<AvaliacaoClinicaEQ> lista = ListUtil.getList(Integer.parseInt((10*folhaAtual -10) + ""), Integer.parseInt((10*folhaAtual) + ""), formEQ.getAvaliacoesClinicas());
		
		StringBuilder sb = new StringBuilder();
		if (lista != null && !lista.isEmpty()){
			for (AvaliacaoClinicaEQ avaliacaoClinicaEQ : lista) {
				if (avaliacaoClinicaEQ.getOrigemDoAnimal().equals(OrigemDoAnimal.NASCIDO_NO_ESTABELECIMENTO))
					sb.append(avaliacaoClinicaEQ.getIdAmostra()).append(", ");
			}
		}
		
		if (sb.length() < 2)
			return sb.toString();
		else
			return sb.toString().substring(0, sb.length()-2);
	}
	
	private static String getIDsDeAnimaisNascidosEmOutroEstabelecimentoDoMunicipio(FormEQ formEQ, int folhaAtual){
		@SuppressWarnings("unchecked")
		List<AvaliacaoClinicaEQ> lista = ListUtil.getList(Integer.parseInt((10*folhaAtual -10) + ""), Integer.parseInt((10*folhaAtual) + ""), formEQ.getAvaliacoesClinicas());
		
		StringBuilder sb = new StringBuilder();
		if (lista != null && !lista.isEmpty()){
			for (AvaliacaoClinicaEQ avaliacaoClinicaEQ : lista) {
				if (avaliacaoClinicaEQ.getOrigemDoAnimal().equals(OrigemDoAnimal.ORIUNDO_DE_OUTRO_ESTABELECIMENTO_DO_MUNICIPIO))
					sb.append(avaliacaoClinicaEQ.getIdAmostra()).append(", ");
			}
		}
		
		if (sb.length() < 2)
			return sb.toString();
		else
			return sb.toString().substring(0, sb.length()-2);
	}
	
	private static String getIDsDeAnimaisNascidosEmOutroMunicipioDoEstado(FormEQ formEQ, int folhaAtual){
		@SuppressWarnings("unchecked")
		List<AvaliacaoClinicaEQ> lista = ListUtil.getList(Integer.parseInt((10*folhaAtual -10) + ""), Integer.parseInt((10*folhaAtual) + ""), formEQ.getAvaliacoesClinicas());
		
		StringBuilder sb = new StringBuilder();
		if (lista != null && !lista.isEmpty()){
			for (AvaliacaoClinicaEQ avaliacaoClinicaEQ : lista) {
				if (avaliacaoClinicaEQ.getOrigemDoAnimal().equals(OrigemDoAnimal.ORIUNDO_DE_OUTRO_ESTABELECIMENTO_DO_ESTADO))
					sb.append(avaliacaoClinicaEQ.getIdAmostra()).append(", ");
			}
		}
		
		if (sb.length() < 2)
			return sb.toString();
		else
			return sb.toString().substring(0, sb.length()-2);
	}
	
	private static List<String[]> getIDsDeAnimaisNascidosEmOutroEstado(FormEQ formEQ, int folhaAtual){
		@SuppressWarnings("unchecked")
		List<AvaliacaoClinicaEQ> lista = ListUtil.getList(Integer.parseInt((10*folhaAtual -10) + ""), Integer.parseInt((10*folhaAtual) + ""), formEQ.getAvaliacoesClinicas());
		
		Map<String, ArrayList<Long>> map = new HashMap<String, ArrayList<Long>>();
		
		if (lista != null && !lista.isEmpty()){
			for (AvaliacaoClinicaEQ avaliacaoClinicaEQ : lista) {
				if (avaliacaoClinicaEQ.getOrigemDoAnimal().equals(OrigemDoAnimal.ORIUNDO_DE_OUTRO_ESTADO)){
					if (map.get(avaliacaoClinicaEQ.getUfDeOrigem()) == null)
						map.put(avaliacaoClinicaEQ.getUfDeOrigem(), new ArrayList<Long>());
					
					map.get(avaliacaoClinicaEQ.getUfDeOrigem()).add(avaliacaoClinicaEQ.getIdAmostra());
				}
					
			}
		}
		
		ArrayList<String[]> response = new ArrayList<String[]>();
		String[] s;
		
		Set<Entry<String, ArrayList<Long>>> entrySet = map.entrySet();
		for (Entry<String, ArrayList<Long>> entry : entrySet) {
			s = new String[2];
			s[0] = entry.getKey();
			s[1] = entry.getValue().toString().replace("[", "").replace("]", "").replace(" ", "");
			
			response.add(s);
		}
		
		return response;
		
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static List<String[]> getHistoricoDeVacinacaoPorDataEDoenca(FormEQ formEQ, int folhaAtual){
		List<AvaliacaoClinicaEQ> lista = ListUtil.getList(Integer.parseInt((10*folhaAtual -10) + ""), Integer.parseInt((10*folhaAtual) + ""), formEQ.getAvaliacoesClinicas());
		
		MultiKeyMap map = new MultiKeyMap();
		
		if (lista != null && !lista.isEmpty()){
			for (AvaliacaoClinicaEQ avaliacaoClinicaEQ : lista) {
				if (avaliacaoClinicaEQ.getVacinacoesEQ() != null && !avaliacaoClinicaEQ.getVacinacoesEQ().isEmpty()){
					for (VacinacaoEQ vacinacaoEQ : avaliacaoClinicaEQ.getVacinacoesEQ()){
						
						if (map.get(DataUtil.getCalendar_dd_MM_yyyy(vacinacaoEQ.getData()), vacinacaoEQ.getDoenca()) == null)
							map.put(DataUtil.getCalendar_dd_MM_yyyy(vacinacaoEQ.getData()), vacinacaoEQ.getDoenca(), new ArrayList<Long>());
						
						((ArrayList<Long>) map.get(DataUtil.getCalendar_dd_MM_yyyy(vacinacaoEQ.getData()), vacinacaoEQ.getDoenca())).add(avaliacaoClinicaEQ.getIdAmostra());
					}
					
				}
					
			}
		}
		
		List<String[]> response = new ArrayList<String[]>();
		String[] s;
		
		Set entrySet = map.keySet();
		for (Object entry : entrySet) {
			s = new String[3];
			s[0] = (String) ((MultiKey)entry).getKey(0);
			s[1] = (String) ((MultiKey)entry).getKey(1);
			s[2] = ((ArrayList)map.get(((MultiKey)entry).getKey(0), ((MultiKey)entry).getKey(1))).toString().replace("[", "").replace("]", "").replace(" ", "");
			
			response.add(s);
		}
		
		return response;
		
	}
	
	private static String getIDsDeAnimaisComHistoricoDeGarrotilho(FormEQ formEQ, int folhaAtual){
		@SuppressWarnings("unchecked")
		List<AvaliacaoClinicaEQ> lista = ListUtil.getList(Integer.parseInt((10*folhaAtual -10) + ""), Integer.parseInt((10*folhaAtual) + ""), formEQ.getAvaliacoesClinicas());
		
		StringBuilder sb = new StringBuilder();
		if (lista != null && !lista.isEmpty()){
			for (AvaliacaoClinicaEQ avaliacaoClinicaEQ : lista) {
				if (avaliacaoClinicaEQ.getDoencasRespiratorias().contains(DoencasRespiratorias.GARROTILHO))
					sb.append(avaliacaoClinicaEQ.getIdAmostra()).append(", ");
			}
		}
		
		if (sb.length() < 2)
			return sb.toString();
		else
			return sb.toString().substring(0, sb.length()-2);
	}
	
	private static String getIDsDeAnimaisComHistoricoDeLinfagite(FormEQ formEQ, int folhaAtual){
		@SuppressWarnings("unchecked")
		List<AvaliacaoClinicaEQ> lista = ListUtil.getList(Integer.parseInt((10*folhaAtual -10) + ""), Integer.parseInt((10*folhaAtual) + ""), formEQ.getAvaliacoesClinicas());
		
		StringBuilder sb = new StringBuilder();
		if (lista != null && !lista.isEmpty()){
			for (AvaliacaoClinicaEQ avaliacaoClinicaEQ : lista) {
				if (avaliacaoClinicaEQ.getDoencasRespiratorias().contains(DoencasRespiratorias.LINFANGITE))
					sb.append(avaliacaoClinicaEQ.getIdAmostra()).append(", ");
			}
		}
		
		if (sb.length() < 2)
			return sb.toString();
		else
			return sb.toString().substring(0, sb.length()-2);
	}
	
	private static String getIDsDeAnimaisComHistoricoDeInfluenza(FormEQ formEQ, int folhaAtual){
		@SuppressWarnings("unchecked")
		List<AvaliacaoClinicaEQ> lista = ListUtil.getList(Integer.parseInt((10*folhaAtual -10) + ""), Integer.parseInt((10*folhaAtual) + ""), formEQ.getAvaliacoesClinicas());
		
		StringBuilder sb = new StringBuilder();
		if (lista != null && !lista.isEmpty()){
			for (AvaliacaoClinicaEQ avaliacaoClinicaEQ : lista) {
				if (avaliacaoClinicaEQ.getDoencasRespiratorias().contains(DoencasRespiratorias.INFLUENZA))
					sb.append(avaliacaoClinicaEQ.getIdAmostra()).append(", ");
			}
		}
		
		if (sb.length() < 2)
			return sb.toString();
		else
			return sb.toString().substring(0, sb.length()-2);
	}
	
	private static String getIDsDeAnimaisComHistoricoDeEsporotricose(FormEQ formEQ, int folhaAtual){
		@SuppressWarnings("unchecked")
		List<AvaliacaoClinicaEQ> lista = ListUtil.getList(Integer.parseInt((10*folhaAtual -10) + ""), Integer.parseInt((10*folhaAtual) + ""), formEQ.getAvaliacoesClinicas());
		
		StringBuilder sb = new StringBuilder();
		if (lista != null && !lista.isEmpty()){
			for (AvaliacaoClinicaEQ avaliacaoClinicaEQ : lista) {
				if (avaliacaoClinicaEQ.getDoencasRespiratorias().contains(DoencasRespiratorias.ESPOROTRICOSE))
					sb.append(avaliacaoClinicaEQ.getIdAmostra()).append(", ");
			}
		}
		
		if (sb.length() < 2)
			return sb.toString();
		else
			return sb.toString().substring(0, sb.length()-2);
	}
	
	private static String getIDsDeAnimaisComHistoricoDeBotriomicose(FormEQ formEQ, int folhaAtual){
		@SuppressWarnings("unchecked")
		List<AvaliacaoClinicaEQ> lista = ListUtil.getList(Integer.parseInt((10*folhaAtual -10) + ""), Integer.parseInt((10*folhaAtual) + ""), formEQ.getAvaliacoesClinicas());
		
		StringBuilder sb = new StringBuilder();
		if (lista != null && !lista.isEmpty()){
			for (AvaliacaoClinicaEQ avaliacaoClinicaEQ : lista) {
				if (avaliacaoClinicaEQ.getDoencasRespiratorias().contains(DoencasRespiratorias.BOTRIOMICOSE))
					sb.append(avaliacaoClinicaEQ.getIdAmostra()).append(", ");
			}
		}
		
		if (sb.length() < 2)
			return sb.toString();
		else
			return sb.toString().substring(0, sb.length()-2);
	}
	
	public static byte[] gerarPdfFormMaleina(List<FormMaleina> listaDeFormMaleina) throws JRException{
		JasperReport report = JasperCompileManager.compileReport(FacesContext.getCurrentInstance().getExternalContext().getRealPath("/") + "/resources/relatorios/FormMaleina.jrxml");
		JasperPrint print = JasperFillManager.fillReport(report, new HashMap<String, Object>(), new JRBeanCollectionDataSource(listaDeFormMaleina));
		return JasperExportManager.exportReportToPdf(print);
	}
	
	public static byte[] gerarPdfFormMormo(List<FormMormo> listaDeFormMormo) throws JRException{
		JasperReport report = JasperCompileManager.compileReport(FacesContext.getCurrentInstance().getExternalContext().getRealPath("/") + "/resources/relatorios/FormMormo.jrxml");
		JasperPrint print = JasperFillManager.fillReport(report, new HashMap<String, Object>(), new JRBeanCollectionDataSource(listaDeFormMormo));
		return JasperExportManager.exportReportToPdf(print);
	}
	
	public static byte[] gerarPdfFormAIE(List<FormAIE> listaDeFormAIE) throws JRException{
		JasperReport report = JasperCompileManager.compileReport(FacesContext.getCurrentInstance().getExternalContext().getRealPath("/") + "/resources/relatorios/FormAIE.jrxml");
		JasperPrint print = JasperFillManager.fillReport(report, new HashMap<String, Object>(), new JRBeanCollectionDataSource(listaDeFormAIE));
		return JasperExportManager.exportReportToPdf(print);
	}
	
	public static byte[] gerarPdfFormCOM(FormCOM formCOM, OutputStream outputStream) throws ApplicationErrorException{
		List<FormCOM> lista = new ArrayList<FormCOM>();
		lista.add(formCOM);
		
		byte[] pdf = null;
		
		try{
		
			PDFMergerUtility mergePdf = new PDFMergerUtility();
						
			byte[] pdfAdicional;
			
			pdfAdicional = gerarPdfFormCOM_PrimeiraFolha(lista);
			mergePdf.addSource(new ByteArrayInputStream(pdfAdicional));
			pdfAdicional = gerarPdfFormCOM_SegundaFolha(lista);
			mergePdf.addSource(new ByteArrayInputStream(pdfAdicional));
			
			mergePdf.setDestinationStream(outputStream);
			mergePdf.mergeDocuments();
		} catch (JRException e) {;
			e.printStackTrace();
			throw new ApplicationErrorException("Erro ao abrir o pdf", e);
		} catch (COSVisitorException e) {
			e.printStackTrace();
			throw new ApplicationErrorException("Erro ao abrir o pdf", e);
		} catch (IOException e) {
			e.printStackTrace();
			throw new ApplicationErrorException("Erro ao abrir o pdf", e);
		}
	
		return pdf;
	}

	private static byte[] gerarPdfFormCOM_PrimeiraFolha(List<FormCOM> listaDeFormCOM) throws JRException{
		JasperReport report = JasperCompileManager.compileReport(FacesContext.getCurrentInstance().getExternalContext().getRealPath("/") + "/resources/relatorios/FormCOM_1.jrxml");
		JasperPrint print = JasperFillManager.fillReport(report, new HashMap<String, Object>(), new JRBeanCollectionDataSource(listaDeFormCOM));
		return JasperExportManager.exportReportToPdf(print);
	}
	
	private static byte[] gerarPdfFormCOM_SegundaFolha(List<FormCOM> listaDeFormCOM) throws JRException{
		JasperReport report = JasperCompileManager.compileReport(FacesContext.getCurrentInstance().getExternalContext().getRealPath("/") + "/resources/relatorios/FormCOM_2.jrxml");
		JasperPrint print = JasperFillManager.fillReport(report, new HashMap<String, Object>(), new JRBeanCollectionDataSource(listaDeFormCOM));
		return JasperExportManager.exportReportToPdf(print);
	}
	
	public static byte[] gerarPdfFormIN(FormIN formIN, FormIN formINOriginal, OutputStream outputStream) throws ApplicationErrorException{
		List<FormIN> lista = new ArrayList<FormIN>();
		lista.add(formIN);
		
		byte[] pdf = null;
		
		try {

			PDFMergerUtility mergePdf = new PDFMergerUtility();
						
			byte[] pdfAdicional;

			int contadorFolhaAdicional = 0;
			List<FolhaAdicional> listaFolhaAdicional = formINOriginal.getListaFolhasAdicionais();
			for (FolhaAdicional folhaAdicional : listaFolhaAdicional) {
				if (folhaAdicional.getFormCOM() == null)
					contadorFolhaAdicional++;
			}
			
			int contadorFormAIE = 0;
			List<FormAIE> listaFormAIE = formINOriginal.getListaFormAIE();
			for (FormAIE formAIE : listaFormAIE) {
				if (formAIE.getFormCOM() == null)
					contadorFormAIE++;
			}
			
			int contadorFormEQ = 0;
			List<FormEQ> listaFormEQ = formINOriginal.getListaFormEQ();
			for (FormEQ formEQ : listaFormEQ) {
				if (formEQ.getFormCOM() == null)
					contadorFormEQ++;
			}
			
			int contadorFormLAB = 0;
			List<FormLAB> listaFormLAB = formINOriginal.getListaFormLAB();
			for (FormLAB formLAB : listaFormLAB) {
				if (formLAB.getFormCOM() == null)
					contadorFormLAB++;
			}
			
			int contadorFormMaleina = 0;
			List<FormMaleina> listaFormMaleina = formINOriginal.getListaFormMaleina();
			for (FormMaleina formMaleina : listaFormMaleina) {
				if (formMaleina.getFormCOM() == null)
					contadorFormMaleina++;
			}
			
			int contadorFormMormo = 0;
			List<FormMormo> listaFormMormo = formINOriginal.getListaFormMormo();
			for (FormMormo formMormo : listaFormMormo) {
				if (formMormo.getFormCOM() == null)
					contadorFormMormo++;
			}
			
			int contadorFormSH = 0;
			List<FormSH> listaFormSH = formINOriginal.getListaFormSH();
			for (FormSH formSH : listaFormSH) {
				if (formSH.getFormCOM() == null)
					contadorFormSH++;
			}
			
			int contadorFormSN = 0;
			List<FormSN> listaFormSN = formINOriginal.getListaFormSN();
			for (FormSN formSN : listaFormSN) {
				if (formSN.getFormCOM() == null)
					contadorFormSN++;
			}
			
			int contadorFormSRN = 0;
			List<FormSRN> listaFormSRN = formINOriginal.getListaFormSRN();
			for (FormSRN formSRN : listaFormSRN) {
				if (formSRN.getFormCOM() == null)
					contadorFormSRN++;
			}
			
			int contadorFormSV = 0;
			List<FormSV> listaFormSV = formINOriginal.getListaFormSV();
			for (FormSV formSV : listaFormSV) {
				if (formSV.getFormCOM() == null)
					contadorFormSV++;
			}
			
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("LISTA_FOLHA_ADICIONAL", contadorFolhaAdicional);
			map.put("LISTA_FORM_AIE", contadorFormAIE);
			map.put("LISTA_FORM_COM", new Integer(formINOriginal.getListaFormCOM().size()));
			map.put("LISTA_FORM_EQ", contadorFormEQ);
			map.put("LISTA_FORM_LAB", contadorFormLAB);
			map.put("LISTA_FORM_Maleina", contadorFormMaleina);
			map.put("LISTA_FORM_Mormo", contadorFormMormo);
			map.put("LISTA_FORM_SH", contadorFormSH);
			map.put("LISTA_FORM_SN", contadorFormSN);
			map.put("LISTA_FORM_SRN", contadorFormSRN);
			map.put("LISTA_FORM_SV", contadorFormSV);
			map.put("LISTA_FORM_VIN", new Integer(formINOriginal.getListaFormVIN().size()));
			map.put("LISTA_RESENHO", new Integer(formINOriginal.getListaResenho().size()));
			
			pdfAdicional = gerarPdfFormIN_PrimeiraFolha(lista);
			mergePdf.addSource(new ByteArrayInputStream(pdfAdicional));
			pdfAdicional = gerarPdfFormIN_SegundaFolha(lista);
			mergePdf.addSource(new ByteArrayInputStream(pdfAdicional));
			pdfAdicional = gerarPdfFormIN_TerceiraFolha(lista, map);
			mergePdf.addSource(new ByteArrayInputStream(pdfAdicional));
			
			mergePdf.setDestinationStream(outputStream);
			mergePdf.mergeDocuments();
			
		} catch (JRException e) {;
			e.printStackTrace();
			throw new ApplicationErrorException("Erro ao abrir o pdf", e);
		} catch (COSVisitorException e) {
			e.printStackTrace();
			throw new ApplicationErrorException("Erro ao abrir o pdf", e);
		} catch (IOException e) {
			e.printStackTrace();
			throw new ApplicationErrorException("Erro ao abrir o pdf", e);
		}

		return pdf;
	}
	
	private static byte[] gerarPdfFormIN_PrimeiraFolha(List<FormIN> listaDeFormIN) throws JRException{
		JasperReport report = JasperCompileManager.compileReport(FacesContext.getCurrentInstance().getExternalContext().getRealPath("/") + "/resources/relatorios/FormIN_1.jrxml");
		JasperPrint print = JasperFillManager.fillReport(report, new HashMap<String, Object>(), new JRBeanCollectionDataSource(listaDeFormIN));
		return JasperExportManager.exportReportToPdf(print);
	}
	
	private static byte[] gerarPdfFormIN_SegundaFolha(List<FormIN> listaDeFormIN) throws JRException{
		JasperReport report = JasperCompileManager.compileReport(FacesContext.getCurrentInstance().getExternalContext().getRealPath("/") + "/resources/relatorios/FormIN_2.jrxml");
		JasperPrint print = JasperFillManager.fillReport(report, new HashMap<String, Object>(), new JRBeanCollectionDataSource(listaDeFormIN));
		return JasperExportManager.exportReportToPdf(print);
	}
	
	private static byte[] gerarPdfFormIN_TerceiraFolha(List<FormIN> listaDeFormIN, HashMap<String, Object> map) throws JRException{
		JasperReport report = JasperCompileManager.compileReport(FacesContext.getCurrentInstance().getExternalContext().getRealPath("/") + "/resources/relatorios/FormIN_3.jrxml");
		JasperPrint print = JasperFillManager.fillReport(report, map, new JRBeanCollectionDataSource(listaDeFormIN));
		return JasperExportManager.exportReportToPdf(print);
	}
	
	public static byte[] gerarPdfResenho(List<Resenho> listaDeResenho) throws JRException{
		JasperReport report = JasperCompileManager.compileReport(FacesContext.getCurrentInstance().getExternalContext().getRealPath("/") + "/resources/relatorios/Resenho.jrxml");
		JasperPrint print = JasperFillManager.fillReport(report, new HashMap<String, Object>(), new JRBeanCollectionDataSource(listaDeResenho));
		return JasperExportManager.exportReportToPdf(print);
	}
	
	public static byte[] gerarPdfFormularioDeColheitaTroncoEncefalico(List<FormularioDeColheitaTroncoEncefalico> listaDeFormularioDeColheitaTroncoEncefalico) throws JRException{
		JasperReport report = JasperCompileManager.compileReport(FacesContext.getCurrentInstance().getExternalContext().getRealPath("/") + "/resources/relatorios/FormularioDeColheitaTroncoEncefalico.jrxml");
		JasperPrint print = JasperFillManager.fillReport(report, new HashMap<String, Object>(), new JRBeanCollectionDataSource(listaDeFormularioDeColheitaTroncoEncefalico));
		return JasperExportManager.exportReportToPdf(print);
	}

	public static byte[] gerarPdfManualAbatedouroFrigorifico() throws FileNotFoundException, IOException {
		File f = new File(FacesContext.getCurrentInstance().getExternalContext().getRealPath("/") + "/resources/manuais/manual-enfermidade-abatedouro-frigorifico.pdf");
		
		return IOUtils.toByteArray(new FileInputStream(f));
	}
	
}
