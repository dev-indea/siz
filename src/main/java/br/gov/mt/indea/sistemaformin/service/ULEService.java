package br.gov.mt.indea.sistemaformin.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.hibernate.Query;

import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoUnidade;
import br.gov.mt.indea.sistemaformin.util.StringUtil;
import br.gov.mt.indea.sistemaformin.webservice.entity.ULE;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class ULEService extends PaginableService<ULE, Long> {

	protected ULEService() {
		super(ULE.class);
	}
	
	public ULE findByNome(String nome, TipoUnidade tipoUnidade){
		ULE ule;
		
		StringBuilder sql = new StringBuilder();
		sql.append("  from unidade ule")
		   .append(" where remove_acento(ule.nome) = :nome ")
		   .append("   and ule.tipoUnidade = :tipoUnidade");
		
		Query query = getSession().createQuery(sql.toString()).setString("nome", StringUtil.removeAcentos(nome)).setParameter("tipoUnidade", tipoUnidade);
		ule = (ULE) query.uniqueResult();
		
		return ule;
	}
	
	@SuppressWarnings("unchecked")
	public List<ULE> findAllByTipoUnidade(TipoUnidade tipoUnidade){
		StringBuilder sql = new StringBuilder();
		sql.append("select ule ")
		   .append("  from unidade ule")
		   .append(" where ule.tipoUnidade = :tipoUnidade")
		   .append(" order by ule.nome");
		
		Query query = getSession().createQuery(sql.toString()).setParameter("tipoUnidade", tipoUnidade);
		return query.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<ULE> findAll(){
		StringBuilder sql = new StringBuilder();
		sql.append("select ule ")
		   .append("  from unidade ule")
		   .append("  left join fetch ule.urs")
		   .append(" order by ule.tipoUnidade, ule.nome");
		
		Query query = getSession().createQuery(sql.toString());
		return query.list();
	} 
	
	@SuppressWarnings("unchecked")
	public List<ULE> findAllByURS(ULE urs) {
		StringBuilder sql = new StringBuilder();
		sql.append("select ule ")
		   .append("  from unidade ule")
		   .append(" where ule.tipoUnidade = :tipoUnidade")
		   .append("   and ule.urs = :urs")
		   .append(" order by ule.nome");
		
		Query query = getSession().createQuery(sql.toString()).setParameter("tipoUnidade", TipoUnidade.ULE).setParameter("urs", urs);
		return query.list();
	}
	
	@Override
	public void validar(ULE ULE) {

	}

	@Override
	public void validarPersist(ULE ULE) {

	}

	@Override
	public void validarMerge(ULE ULE) {

	}

	@Override
	public void validarDelete(ULE ULE) {

	}

}
