package br.gov.mt.indea.sistemaformin.dao;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.inject.Inject;


import br.gov.mt.indea.sistemaformin.entity.FormSN;
import br.gov.mt.indea.sistemaformin.exception.ApplicationException;

@Stateless
public class FormSNDAO extends DAO<FormSN> {
	
	@Inject
	private EntityManager em;
	
	public FormSNDAO() {
		super(FormSN.class);
	}

	@Override
	public EntityManager getEntityManager() {
		return this.em;
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void add(FormSN formSN) throws ApplicationException{
		formSN = this.getEntityManager().merge(formSN);
		if (formSN.getFormCOM() != null)
			formSN.setFormCOM(this.getEntityManager().merge(formSN.getFormCOM()));
		super.add(formSN);
	}

}
