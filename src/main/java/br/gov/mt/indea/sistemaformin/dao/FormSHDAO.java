package br.gov.mt.indea.sistemaformin.dao;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.inject.Inject;


import br.gov.mt.indea.sistemaformin.entity.FormSH;
import br.gov.mt.indea.sistemaformin.exception.ApplicationException;

@Stateless
public class FormSHDAO extends DAO<FormSH> {
	
	@Inject
	private EntityManager em;
	
	public FormSHDAO() {
		super(FormSH.class);
	}

	@Override
	public EntityManager getEntityManager() {
		return this.em;
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void add(FormSH formSH) throws ApplicationException{
		formSH = this.getEntityManager().merge(formSH);
		if (formSH.getFormCOM() != null)
			formSH.setFormCOM(this.getEntityManager().merge(formSH.getFormCOM()));
		super.add(formSH);
	}

}
