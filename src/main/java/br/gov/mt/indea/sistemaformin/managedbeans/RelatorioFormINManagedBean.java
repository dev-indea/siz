package br.gov.mt.indea.sistemaformin.managedbeans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;

import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLBeanName;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;

import br.gov.mt.indea.sistemaformin.entity.view.RelatorioFormINGeral;
import br.gov.mt.indea.sistemaformin.service.FormINService;
import br.gov.mt.indea.sistemaformin.util.FacesMessageUtil;

@Named
@ViewScoped
@URLBeanName("relatorioFormINManagedBean")
@URLMappings(mappings = {
		@URLMapping(id = "relatorioFormIN", pattern = "/relatorio/formIN", viewId = "/pages/relatorios/formin.jsf")})
public class RelatorioFormINManagedBean implements Serializable {

	private static final long serialVersionUID = 1760023349204758559L;

	@Inject
	private FormINService formINService;
	
	private List<RelatorioFormINGeral> listaFormIN;
	
	private List<RelatorioFormINGeral> listaFormINFiltered;
	
	private Date dataInicial;
	
	private Date dataFinal;
	
	@PostConstruct
	private void init(){
		dataInicial = new Date();
		dataFinal = new Date();
		
		Calendar c = Calendar.getInstance();
		c.set(Calendar.DAY_OF_MONTH, 1);
		
		dataInicial.setTime(c.getTimeInMillis());
		
		c.set(Calendar.DAY_OF_MONTH, 30);
		
		dataFinal.setTime(c.getTimeInMillis());
	}
	
	@URLAction(mappingId = "relatorioFormIN", onPostback = false)
	public void novo(){
		
	}
	
	public void buscar(){
		long diff = dataFinal.getTime() - dataInicial.getTime();
		
	    if (TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS) > 366){
	    	FacesMessageUtil.addWarnContextFacesMessage("O per�odo de busca n�o deve ser superior a 1 ano", "");
	    	return;
	    }
		
		listaFormIN = (ArrayList<RelatorioFormINGeral>) formINService.relatorioTeste(this.dataInicial, this.dataFinal);
	}
	
	public void postProcessor(Object doc){
		HSSFWorkbook book = (HSSFWorkbook)doc;
	    HSSFSheet sheet = book.getSheetAt(0); 

	    HSSFRow header = sheet.getRow(0);
	    
	    int colCount = header.getPhysicalNumberOfCells();
	    int rowCount = sheet.getPhysicalNumberOfRows();

	    HSSFCellStyle intStyle = book.createCellStyle();
		intStyle.setDataFormat((short)1);

		HSSFCellStyle decStyle = book.createCellStyle();
		decStyle.setDataFormat((short)2);
		
		HSSFCellStyle headerStyle = book.createCellStyle();
		headerStyle.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
		headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		HSSFFont font = book.createFont();
        font.setBold(true);
        headerStyle.setFont(font);

        //Transforma o t�tulo em negrito
        for(int colNum = 0; colNum < header.getLastCellNum(); colNum++){   
	    	HSSFCell cell = header.getCell(colNum);
	    	cell.setCellStyle(headerStyle);
	    	book.getSheetAt(0).autoSizeColumn(colNum);
	    	
	    	if (colNum == 14)
	    		cell.setCellValue("Data de envio do material");
	    	if (colNum == 15)
	    		cell.setCellValue("Data prov�vel do in�cio");
	    	if (colNum == 16)
	    		cell.setCellValue("Data Notifica��o");
	    	if (colNum == 17)
	    		cell.setCellValue("Data Cadastro");
	    	if (colNum == 18)
	    		cell.setCellValue("Data da visita inicial do veterin�rio");
	    	if (colNum == 31)
	    		cell.setCellValue("Data do resultado");
	    	if (colNum == 34)
	    		cell.setCellValue("Data do Form COM de encerramento");
        }
	    
		for(int rowInd = 1; rowInd < rowCount; rowInd++) {
		    HSSFRow row = sheet.getRow(rowInd);
		    for(int cellInd = 1; cellInd < colCount; cellInd++) {
		        HSSFCell cell = row.getCell(cellInd);
		        
		        //Latitude (grau)
		        if (cell.getColumnIndex() == 6){
		        	this.setCellIntegerStyle(cell, intStyle);
		        }
		        
		        //Latitude (Minuto)
		        if (cell.getColumnIndex() == 7){
		        	this.setCellIntegerStyle(cell, intStyle);
		        }
		        
  		        //Latitude (Segundo)
		        if (cell.getColumnIndex() == 8){
		        	this.setCellDoubleStyle(cell, decStyle);
		        }
		        
		        //Longitude (grau)
		        if (cell.getColumnIndex() == 9){
		        	this.setCellIntegerStyle(cell, intStyle);
		        }
		        
		        //Longitude (Minuto)
		        if (cell.getColumnIndex() == 10){
		        	this.setCellIntegerStyle(cell, intStyle);
		        }
		        
  		        //Longitude (Segundo)
		        if (cell.getColumnIndex() == 11){
		        	this.setCellDoubleStyle(cell, decStyle);
		        }
		        
		        //CRMV
		        if (cell.getColumnIndex() == 27){
		        	this.setCellIntegerStyle(cell, intStyle);
		        }
		        
		        //Quantidade Form COM
		        if (cell.getColumnIndex() == 35){
		        	this.setCellIntegerStyle(cell, intStyle);
		        }
		        
		        //Quantidade de equ�deos destru�dos
		        if (cell.getColumnIndex() == 36){
		        	this.setCellIntegerStyle(cell, intStyle);
		        }

		    }
		}
	}
	
	private void setCellIntegerStyle(HSSFCell cell, HSSFCellStyle style){
		String strVal = cell.getStringCellValue();
		cell.setCellStyle(style);
        
		strVal = strVal.replaceAll("[^\\d.]", "");
		if (strVal != null && !strVal.equals("")){
	    	int intVal = Integer.valueOf(strVal);
	    	cell.setCellValue(intVal);
		}
	}
	
	private void setCellDoubleStyle(HSSFCell cell, HSSFCellStyle style){
		String strVal = cell.getStringCellValue();
    	cell.setCellStyle(style);
		
    	if (strVal != null && !strVal.equals("")){
    	    double dblVal = Double.valueOf(strVal);
    	    cell.setCellValue(dblVal);
    	}
	}
	
	public List<String> getListaURSFilter(){
		if (this.listaFormIN == null)
			return null;
		else{
			List<String> lista = new ArrayList<>();
			
			for (RelatorioFormINGeral relatorio : listaFormIN) {
				if (!lista.contains(relatorio.getUrs()))
					lista.add((String) relatorio.getUrs());
			}
			
			List<String> sortedLista = new ArrayList<>();
			sortedLista = lista;
			
			try{
				Collections.sort(lista);
				sortedLista = lista;
			} catch (Exception e) {
			}
			
			return sortedLista;
		}
	}
	
	public List<String> getListaMunicipioFilter(){
		if (this.listaFormIN == null)
			return null;
		else{
			List<String> lista = new ArrayList<>();
			
			for (RelatorioFormINGeral relatorio : listaFormIN) {
				if (!lista.contains(relatorio.getUle()))
					lista.add((String) relatorio.getUle());
			}
			
			List<String> sortedLista = new ArrayList<>();
			sortedLista = lista;
			
			try{
				Collections.sort(lista);
				sortedLista = lista;
			} catch (Exception e) {
			}
			
			return sortedLista;
		}
	}
	
	public List<String> getListaTipoNotificacaoFilter(){
		if (this.listaFormIN == null)
			return null;
		else{
			List<String> lista = new ArrayList<>();
			
			for (RelatorioFormINGeral relatorio : listaFormIN) {
				if (!lista.contains(relatorio.getTipo_notificacao()))
					lista.add((String) relatorio.getTipo_notificacao());
			}
			
			List<String> sortedLista = new ArrayList<>();
			sortedLista = lista;
			
			try{
				Collections.sort(lista);
				sortedLista = lista;
			} catch (Exception e) {
			}
			
			return sortedLista;
		}
	}
	
	public List<String> getListaHouveColheitaFilter(){
		if (this.listaFormIN == null)
			return null;
		else{
			List<String> lista = new ArrayList<>();
			
			for (RelatorioFormINGeral relatorio : listaFormIN) {
				if (!lista.contains(relatorio.getHouve_colheita()))
					lista.add((String) relatorio.getHouve_colheita());
			}
			
			List<String> sortedLista = new ArrayList<>();
			sortedLista = lista;
			
			try{
				Collections.sort(lista);
				sortedLista = lista;
			} catch (Exception e) {
			}
			
			return sortedLista;
		}
	}
	
	public List<String> getListaOrigemNotificacaoFilter(){
		if (this.listaFormIN == null)
			return null;
		else{
			List<String> lista = new ArrayList<>();
			
			for (RelatorioFormINGeral relatorio : listaFormIN) {
				if (!lista.contains(relatorio.getOrigem_notificacao()))
					lista.add((String) relatorio.getOrigem_notificacao());
			}
			
			List<String> sortedLista = new ArrayList<>();
			sortedLista = lista;
			
			try{
				Collections.sort(lista);
				sortedLista = lista;
			} catch (Exception e) {
			}
			
			return sortedLista;
		}
	}
	
	public List<String> getListaEspecieAfetadaFilter(){
		if (this.listaFormIN == null)
			return null;
		else{
			List<String> lista = new ArrayList<>();
			
			for (RelatorioFormINGeral relatorio : listaFormIN) {
				if (!lista.contains(relatorio.getEspecie_afetada()) && !StringUtils.isEmpty(relatorio.getEspecie_afetada()))
					lista.add((String) relatorio.getEspecie_afetada());
				
				if (StringUtils.isEmpty(relatorio.getEspecie_afetada()))
					if (!lista.contains("Nenhuma"))
							lista.add("Nenhuma");
			}
			
			List<String> sortedLista = new ArrayList<>();
			sortedLista = lista;
			
			try{
				Collections.sort(lista);
				sortedLista = lista;
			} catch (Exception e) {
			}
			
			return sortedLista;
		}
	}
	
	public List<String> getListaInvestigacaoEncerradaFilter(){
		if (this.listaFormIN == null)
			return null;
		else{
			List<String> lista = new ArrayList<>();
			
			for (RelatorioFormINGeral relatorio : listaFormIN) {
				if (!lista.contains(relatorio.getInvestiga��o_encerrada()))
					lista.add((String) relatorio.getInvestiga��o_encerrada());
			}
			
			List<String> sortedLista = new ArrayList<>();
			sortedLista = lista;
			
			try{
				Collections.sort(lista);
				sortedLista = lista;
			} catch (Exception e) {
			}
			
			return sortedLista;
		}
	}
	
	public boolean filterByDate(Object value, Object filter, Locale locale){
		if( filter == null ) {
            return true;
        }

        if( value == null ) {
            return false;
        }
        
        if (value instanceof String){
        	String[] date = ((String) value).split("/");

        	Calendar c = Calendar.getInstance();
        	c.set(Calendar.DAY_OF_MONTH, Integer.parseInt(date[0]));
        	c.set(Calendar.MONTH, Integer.parseInt(date[1]) - 1);
        	c.set(Calendar.YEAR, Integer.parseInt(date[2]));
        	
        	return DateUtils.truncatedEquals((Date) filter, c.getTime(), Calendar.DATE);
        } else {
        	return DateUtils.truncatedEquals((Date) filter, (Date) value, Calendar.DATE);
        }
	}
	
	public boolean filterByEspecieAfetada(Object value, Object filter, Locale locale){
		if( filter == null ) {
            return true;
        }

        if (filter.equals("Nenhuma") && value == null)
        	return true;
        
        return filter.equals(value);	
	}

	public List<RelatorioFormINGeral> getListaFormIN() {
		return listaFormIN;
	}

	public void setListaFormIN(List<RelatorioFormINGeral> listaFormIN) {
		this.listaFormIN = listaFormIN;
	}

	public Date getDataInicial() {
		return dataInicial;
	}

	public void setDataInicial(Date dataInicial) {
		this.dataInicial = dataInicial;
	}

	public Date getDataFinal() {
		return dataFinal;
	}

	public void setDataFinal(Date dataFinal) {
		this.dataFinal = dataFinal;
	}

	public List<RelatorioFormINGeral> getListaFormINFiltered() {
		return listaFormINFiltered;
	}

	public void setListaFormINFiltered(List<RelatorioFormINGeral> listaFormINFiltered) {
		this.listaFormINFiltered = listaFormINFiltered;
	}
	
}
