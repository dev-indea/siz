package br.gov.mt.indea.sistemaformin.service;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.mt.indea.sistemaformin.entity.FormVIN;
import br.gov.mt.indea.sistemaformin.entity.NovoEstabelecimentoParaInvestigacao;
import br.gov.mt.indea.sistemaformin.webservice.entity.Exploracao;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class FormVINService extends PaginableService<FormVIN, Long> {
	
	private static final Logger log = LoggerFactory.getLogger(FormVINService.class);

	protected FormVINService() {
		super(FormVIN.class);
	}
	
	public FormVIN findByIdFetchAll(Long id){
		FormVIN formVIN;
		
		StringBuilder sql = new StringBuilder();
		sql.append("  from FormVIN formvin ")
		   .append("  left join fetch formvin.propriedade propriedade")
		   .append("  left join fetch propriedade.municipio")
		   .append("  left join fetch propriedade.endereco ")
		   .append("  left join fetch formvin.formIN formin")
		   .append("  left join fetch formin.proprietario proprietario")
		   .append("  left join fetch proprietario.endereco ")
		   .append("  left join fetch propriedade.coordenadaGeografica ")
		   .append("  left join fetch formvin.representanteEstabelecimento ")
		   .append("  left join fetch formvin.abatedouro abatedouro")
		   .append("  left join fetch abatedouro.coordenadaGeografica")
		   .append("  left join fetch abatedouro.municipio munpaba")
		   .append("  left join fetch abatedouro.endereco endaba")
		   .append("  left join fetch endaba.municipio munendaba")
		   .append("  join fetch formvin.veterinario vet ")
		   .append("  join fetch vet.conselho")
		   .append("  join fetch vet.tipoEmitente")
		   .append("  left join fetch vet.ule uu")
		   .append("  left join fetch uu.urs")
		   .append("  left join fetch vet.endereco")
		   .append("  left join fetch vet.municipioNascimento")
		   .append(" where formvin.id = :id ");
		
		Query query = getSession().createQuery(sql.toString()).setLong("id", id);
		formVIN = (FormVIN) query.uniqueResult();
		
		if (formVIN != null){
		
			if (formVIN.getPropriedade() != null){
				Hibernate.initialize(formVIN.getPropriedade().getProprietarios());
				Hibernate.initialize(formVIN.getPropriedade().getUle());
				Hibernate.initialize(formVIN.getPropriedade().getExploracaos());
				
				for (Exploracao exploracao : formVIN.getPropriedade().getExploracaos()){
					Hibernate.initialize(exploracao.getProdutores());
				}
			}
			
			if (formVIN.getRepresentanteEstabelecimento() != null)
				Hibernate.initialize(formVIN.getRepresentanteEstabelecimento().getFuncoesNoEstabelecimento());
			
			Hibernate.initialize(formVIN.getVinculoEpidemiologico());
			Hibernate.initialize(formVIN.getVistoriasFormVIN());
			Hibernate.initialize(formVIN.getNovoEstabelecimentoParaInvestigacaos());
			
			for (NovoEstabelecimentoParaInvestigacao estabelecimento : formVIN.getNovoEstabelecimentoParaInvestigacaos()) {
				Hibernate.initialize(estabelecimento.getPropriedade());
				Hibernate.initialize(estabelecimento.getPropriedade().getEndereco());
				Hibernate.initialize(estabelecimento.getPropriedade().getMunicipio());
				Hibernate.initialize(estabelecimento.getPropriedade().getUle());
				Hibernate.initialize(estabelecimento.getListaVinculoEpidemiologico());
			}
			
		}
		
		return formVIN;
	}
	
	@Override
	public void saveOrUpdate(FormVIN formVIN) {
		formVIN = (FormVIN) this.getSession().merge(formVIN);
		super.saveOrUpdate(formVIN);
		
		log.info("Salvando Form VIN {}", formVIN.getId());
	}
	
	@Override
	public void delete(FormVIN formVIN) {
		super.delete(formVIN);
		
		log.info("Removendo Form VIN {}", formVIN.getId());
	}
	
	@Override
	public void validar(FormVIN FormVIN) {

	}

	@Override
	public void validarPersist(FormVIN FormVIN) {

	}

	@Override
	public void validarMerge(FormVIN FormVIN) {

	}

	@Override
	public void validarDelete(FormVIN FormVIN) {

	}

}
