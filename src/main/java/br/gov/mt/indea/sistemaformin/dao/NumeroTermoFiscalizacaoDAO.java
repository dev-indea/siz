package br.gov.mt.indea.sistemaformin.dao;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.inject.Inject;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import br.gov.mt.indea.sistemaformin.entity.Municipio;
import br.gov.mt.indea.sistemaformin.entity.NumeroTermoFiscalizacao;

@Stateless
public class NumeroTermoFiscalizacaoDAO extends DAO<NumeroTermoFiscalizacao>{

	@Inject
	private EntityManager em;
	
	public NumeroTermoFiscalizacaoDAO() {
		super(NumeroTermoFiscalizacao.class);
	}

	@Override
	public EntityManager getEntityManager() {
		return this.em;
	}
	
	public NumeroTermoFiscalizacao getNumeroTermoFiscalizacaoByMunicipio(Municipio municipio, int ano){
		NumeroTermoFiscalizacao numeroTermoFiscalizacao;
		
		CriteriaBuilder cb  = em.getCriteriaBuilder();
		CriteriaQuery<NumeroTermoFiscalizacao> criteria = cb.createQuery(NumeroTermoFiscalizacao.class);
		Root<NumeroTermoFiscalizacao> rootNumeroTermoFiscalizacao = criteria.from(NumeroTermoFiscalizacao.class);
		
		List<Predicate> predicados = new ArrayList<Predicate>();
		
		predicados.add(cb.equal(rootNumeroTermoFiscalizacao.get("municipio"), municipio));
		predicados.add(cb.equal(rootNumeroTermoFiscalizacao.get("ano"), ano));
		
		criteria.where(cb.and(predicados.toArray(new Predicate[]{})));
		
		try {
			numeroTermoFiscalizacao = em.createQuery(criteria).getSingleResult();
		} catch (NoResultException e) {
			numeroTermoFiscalizacao = null;
		}

		return numeroTermoFiscalizacao;
	}

}
