package br.gov.mt.indea.sistemaformin.entity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

import br.gov.mt.indea.sistemaformin.enums.Dominio.DireitoEsquerdo;
import br.gov.mt.indea.sistemaformin.enums.Dominio.Especie;
import br.gov.mt.indea.sistemaformin.enums.Dominio.PositivoNegativo;
import br.gov.mt.indea.sistemaformin.enums.Dominio.Sexo;
import br.gov.mt.indea.sistemaformin.enums.Dominio.SinaisClinicosExameMaleina;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoPeriodo;

@Audited
@Entity
@Table(name="form_maleina")
public class FormMaleina extends BaseEntity<Long>{
	
	private static final long serialVersionUID = 1835548096756339091L;

	@Id
	@SequenceGenerator(name="form_maleina_seq", sequenceName="form_maleina_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="form_maleina_seq")
	private Long id;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_usuario")
	private Usuario usuario;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_cadastro")
	private Calendar dataCadastro;
	
	@ManyToOne
	@JoinColumn(name="id_formin")
	private FormIN formIN;
	
	@ManyToOne
	@JoinColumn(name="id_formcom")
	private FormCOM formCOM;
	
	@OneToMany(mappedBy="formMaleina", cascade={CascadeType.ALL}, orphanRemoval=true)
	private List<TesteDeFixacaoDeComplemento> testesDeFixacaoDeComplementos = new ArrayList<TesteDeFixacaoDeComplemento>();
	
	@Column(name="nome_animal")
	private String nomeAnimal;
	
	private Long idade;
	
	@Enumerated(EnumType.STRING)
	@Column(name="tipo_idade")
	private TipoPeriodo tipoPeriodo;
	
	@Column(length=255)
	private String registro;
	
	@Column(name="id_eletronica", length=255)
	private String idEletronica;
	
	@Column(length=255)
	private String raca;
	
	@Enumerated(EnumType.STRING)
	private Especie especie;
	
	@Enumerated(EnumType.STRING)
	private Sexo sexo;
	
	@Column(length=255)
	private String pelagem;
	
	@Column(length=2000)
	private String descricao;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_aplicacao_maleina")
	private Calendar dataDeAplicacaoDaMaleina;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_leitura")
	private Calendar dataDeLeitura;

	@ElementCollection(targetClass = DireitoEsquerdo.class)
	@JoinTable(name = "formmaleina_olhos", joinColumns = @JoinColumn(name = "id_form_maleina", nullable = false))
	@Column(name = "id_formmaleina_olhos")
	@Enumerated(EnumType.STRING)
	private List<DireitoEsquerdo> olhosDaAplicacao = new ArrayList<DireitoEsquerdo>();
	
	@ElementCollection(targetClass = SinaisClinicosExameMaleina.class)
	@JoinTable(name = "formmaleina_sinais", joinColumns = @JoinColumn(name = "id_form_maleina", nullable = false))
	@Column(name = "id_formmaleina_sinais")
	@Enumerated(EnumType.STRING)
	private List<SinaisClinicosExameMaleina> sinaisClinicosExameMaleina = new ArrayList<SinaisClinicosExameMaleina>();
	
	@Column(name="temp_retal_aplicacao")
	private Long temperaturaRetalAntesDaAplicacao;
	
	@Column(name="temp_retal_leitura")
	private Long temperaturaRetalNaLeitura;
	
	@Column(name="fabricante_maleina", length=255)
	private String fabricanteMaleina;
	
	@Column(name="partida_maleina", length=255)
	private String partidaMaleina;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_validade_maleina")
	private Calendar dataValidadeMaleina;
	
	@Enumerated(EnumType.STRING)
	private PositivoNegativo interpretacaoExame;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_validade_exame")
	private Calendar dataValidadeExame;
	
	@OneToOne(cascade={CascadeType.ALL})
	@JoinColumn(name="id_veterinario")
	private br.gov.mt.indea.sistemaformin.webservice.entity.Veterinario veterinario;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Calendar getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Calendar dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public FormIN getFormIN() {
		return formIN;
	}

	public void setFormIN(FormIN formIN) {
		this.formIN = formIN;
	}

	public FormCOM getFormCOM() {
		return formCOM;
	}

	public void setFormCOM(FormCOM formCOM) {
		this.formCOM = formCOM;
	}

	public String getNomeAnimal() {
		return nomeAnimal;
	}

	public void setNomeAnimal(String nomeAnimal) {
		this.nomeAnimal = nomeAnimal;
	}

	public Long getIdade() {
		return idade;
	}

	public void setIdade(Long idade) {
		this.idade = idade;
	}

	public TipoPeriodo getTipoPeriodo() {
		return tipoPeriodo;
	}

	public void setTipoPeriodo(TipoPeriodo tipoPeriodo) {
		this.tipoPeriodo = tipoPeriodo;
	}

	public String getRegistro() {
		return registro;
	}

	public void setRegistro(String registro) {
		this.registro = registro;
	}

	public String getIdEletronica() {
		return idEletronica;
	}

	public void setIdEletronica(String idEletronica) {
		this.idEletronica = idEletronica;
	}

	public String getRaca() {
		return raca;
	}

	public void setRaca(String raca) {
		this.raca = raca;
	}

	public Especie getEspecie() {
		return especie;
	}

	public void setEspecie(Especie especie) {
		this.especie = especie;
	}

	public Sexo getSexo() {
		return sexo;
	}

	public void setSexo(Sexo sexo) {
		this.sexo = sexo;
	}

	public String getPelagem() {
		return pelagem;
	}

	public void setPelagem(String pelagem) {
		this.pelagem = pelagem;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Calendar getDataDeAplicacaoDaMaleina() {
		return dataDeAplicacaoDaMaleina;
	}

	public void setDataDeAplicacaoDaMaleina(Calendar dataDeAplicacaoDaMaleina) {
		this.dataDeAplicacaoDaMaleina = dataDeAplicacaoDaMaleina;
	}

	public Calendar getDataDeLeitura() {
		return dataDeLeitura;
	}

	public void setDataDeLeitura(Calendar dataDeLeitura) {
		this.dataDeLeitura = dataDeLeitura;
	}

	public List<DireitoEsquerdo> getOlhosDaAplicacao() {
		return olhosDaAplicacao;
	}

	public void setOlhosDaAplicacao(List<DireitoEsquerdo> olhosDaAplicacao) {
		this.olhosDaAplicacao = olhosDaAplicacao;
	}

	public List<SinaisClinicosExameMaleina> getSinaisClinicosExameMaleina() {
		return sinaisClinicosExameMaleina;
	}

	public void setSinaisClinicosExameMaleina(
			List<SinaisClinicosExameMaleina> sinaisClinicosExameMaleina) {
		this.sinaisClinicosExameMaleina = sinaisClinicosExameMaleina;
	}

	public Long getTemperaturaRetalAntesDaAplicacao() {
		return temperaturaRetalAntesDaAplicacao;
	}

	public void setTemperaturaRetalAntesDaAplicacao(
			Long temperaturaRetalAntesDaAplicacao) {
		this.temperaturaRetalAntesDaAplicacao = temperaturaRetalAntesDaAplicacao;
	}

	public Long getTemperaturaRetalNaLeitura() {
		return temperaturaRetalNaLeitura;
	}

	public void setTemperaturaRetalNaLeitura(Long temperaturaRetalNaLeitura) {
		this.temperaturaRetalNaLeitura = temperaturaRetalNaLeitura;
	}

	public String getFabricanteMaleina() {
		return fabricanteMaleina;
	}

	public void setFabricanteMaleina(String fabricanteMaleina) {
		this.fabricanteMaleina = fabricanteMaleina;
	}

	public String getPartidaMaleina() {
		return partidaMaleina;
	}

	public void setPartidaMaleina(String partidaMaleina) {
		this.partidaMaleina = partidaMaleina;
	}

	public Calendar getDataValidadeMaleina() {
		return dataValidadeMaleina;
	}

	public void setDataValidadeMaleina(Calendar dataValidadeMaleina) {
		this.dataValidadeMaleina = dataValidadeMaleina;
	}

	public PositivoNegativo getInterpretacaoExame() {
		return interpretacaoExame;
	}

	public void setInterpretacaoExame(PositivoNegativo interpretacaoExame) {
		this.interpretacaoExame = interpretacaoExame;
	}

	public Calendar getDataValidadeExame() {
		return dataValidadeExame;
	}

	public void setDataValidadeExame(Calendar dataValidadeExame) {
		this.dataValidadeExame = dataValidadeExame;
	}

	public List<TesteDeFixacaoDeComplemento> getTestesDeFixacaoDeComplementos() {
		return testesDeFixacaoDeComplementos;
	}

	public void setTestesDeFixacaoDeComplementos(
			List<TesteDeFixacaoDeComplemento> testesDeFixacaoDeComplementos) {
		this.testesDeFixacaoDeComplementos = testesDeFixacaoDeComplementos;
	}

	public br.gov.mt.indea.sistemaformin.webservice.entity.Veterinario getVeterinario() {
		return veterinario;
	}

	public void setVeterinario(
			br.gov.mt.indea.sistemaformin.webservice.entity.Veterinario veterinario) {
		this.veterinario = veterinario;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FormMaleina other = (FormMaleina) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}