package br.gov.mt.indea.sistemaformin.managedbeans;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLBeanName;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;

import br.gov.mt.indea.sistemaformin.entity.AchadosLoteEnfermidadeAbatedouroFrigorifico;
import br.gov.mt.indea.sistemaformin.entity.DoencaEnfermidadeAbatedouroFrigorifico;
import br.gov.mt.indea.sistemaformin.entity.EnfermidadeAbatedouroFrigorifico;
import br.gov.mt.indea.sistemaformin.entity.FormularioDeColheitaTroncoEncefalico;
import br.gov.mt.indea.sistemaformin.entity.GtaEnfermidadeAbatedouroFrigorifico;
import br.gov.mt.indea.sistemaformin.entity.LoteEnfermidadeAbatedouroFrigorifico;
import br.gov.mt.indea.sistemaformin.entity.Municipio;
import br.gov.mt.indea.sistemaformin.entity.Pais;
import br.gov.mt.indea.sistemaformin.entity.Pessoa;
import br.gov.mt.indea.sistemaformin.entity.ServicoDeInspecao;
import br.gov.mt.indea.sistemaformin.entity.TipoExame;
import br.gov.mt.indea.sistemaformin.entity.UF;
import br.gov.mt.indea.sistemaformin.entity.dto.EnfermidadeAbatedouroFrigorificoDTO;
import br.gov.mt.indea.sistemaformin.enums.Dominio.MotivacaoParaAbateDeEmergenciaEmFrigorifico;
import br.gov.mt.indea.sistemaformin.enums.Dominio.SinaisClinicosNervososEncefalopatoaEspongiformeTransmissivel;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoDeMorteEmFrigorifico;
import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoTransitoDeAnimais;
import br.gov.mt.indea.sistemaformin.exception.ApplicationErrorException;
import br.gov.mt.indea.sistemaformin.exception.ApplicationException;
import br.gov.mt.indea.sistemaformin.security.UserSecurity;
import br.gov.mt.indea.sistemaformin.service.DoencaEnfermidadeAbatedouroFrigorificoService;
import br.gov.mt.indea.sistemaformin.service.EnfermidadeAbatedouroFrigorificoService;
import br.gov.mt.indea.sistemaformin.service.LazyObjectDataModel;
import br.gov.mt.indea.sistemaformin.service.LoteEnfermidadeAbatedouroFrigorificoService;
import br.gov.mt.indea.sistemaformin.service.MunicipioService;
import br.gov.mt.indea.sistemaformin.service.PaisService;
import br.gov.mt.indea.sistemaformin.service.ServicoDeInspecaoService;
import br.gov.mt.indea.sistemaformin.service.TipoExameService;
import br.gov.mt.indea.sistemaformin.service.UFService;
import br.gov.mt.indea.sistemaformin.util.DataUtil;
import br.gov.mt.indea.sistemaformin.util.FacesMessageUtil;
import br.gov.mt.indea.sistemaformin.webservice.client.ClientWebServiceGTA;
import br.gov.mt.indea.sistemaformin.webservice.convert.GtaConverter;
import br.gov.mt.indea.sistemaformin.webservice.entity.Especie;
import br.gov.mt.indea.sistemaformin.webservice.entity.Gta;
import br.gov.mt.indea.sistemaformin.webservice.entity.PessoaDestinoGta;
import br.gov.mt.indea.sistemaformin.webservice.entity.PessoaOrigemGta;
import br.gov.mt.indea.sistemaformin.webservice.entity.Produtor;
import br.gov.mt.indea.sistemaformin.webservice.entity.Propriedade;
import br.gov.mt.indea.sistemaformin.webservice.entity.TipoGta;
import br.gov.mt.indea.sistemaformin.webservice.entity.Veterinario;

@Named("enfermidadeAbatedouroFrigorificoManagedBean")
@ViewScoped
@URLBeanName("enfermidadeAbatedouroFrigorificoManagedBean")
@URLMappings(mappings = {
		@URLMapping(id="pesquisarEnfermidadeAbatedouroFrigorifico", pattern="/enfermidadeAbatedouroFrigorifico/pesquisar", viewId="/pages/enfermidadeAbatedouroFrigorifico/lista.jsf"),
		@URLMapping(id="novoEnfermidadeAbatedouroFrigorifico", pattern="/enfermidadeAbatedouroFrigorifico/novo", viewId="/pages/enfermidadeAbatedouroFrigorifico/novo.jsf"),
		@URLMapping(id="alterarEnfermidadeAbatedouroFrigorifico", pattern="/enfermidadeAbatedouroFrigorifico/alterar/#{enfermidadeAbatedouroFrigorificoManagedBean.id}", viewId="/pages/enfermidadeAbatedouroFrigorifico/novo.jsf"),
		@URLMapping(id="impressaoFormularioColheitaBSE", pattern = "/enfermidadeAbatedouroFrigorifico/#{enfermidadeAbatedouroFrigorificoManagedBean.id}/impressao/#{enfermidadeAbatedouroFrigorificoManagedBean.idFormularioColheitaBSE}", viewId = "/pages/impressao.jsf"),
		@URLMapping(id="manualEnfermidadeAbatedouroFrigorifico", pattern = "/manual", viewId = "/pages/impressao.jsf")})
public class EnfermidadeAbatedouroFrigorificoManagedBean implements Serializable{

	private static final long serialVersionUID = 2046061781097536140L;

	@Inject
	private UserSecurity userSecurity;
	
	@Inject
	LoginManagedBean loginManagedBean;

	@Inject
	private ClientWebServiceGTA clientWebServiceGTA;
	
	private LazyObjectDataModel<LoteEnfermidadeAbatedouroFrigorifico> listaLoteEnfermidadeAbatedouroFrigorifico;
	
	private Long id;
	
	private Long idFormularioColheitaBSE;
	
	private EnfermidadeAbatedouroFrigorifico enfermidadeAbatedouroFrigorifico;
	
	@Inject
	private EnfermidadeAbatedouroFrigorificoDTO enfermidadeAbatedouroFrigorificoDTO;
	
	private Date dataAbate;
	
	private Date dataColeta;
	
	private String especie;
	
	private UF uf;
	
	private Pais pais;
	
	private String cpfVeterinario;

	private String crmvVeterinario;

	private List<Veterinario> listaVeterinarios;
	
	private List<TipoExame> listaTipoExame;
	
	private List<br.gov.mt.indea.sistemaformin.webservice.model.Gta> listaGtas;
	
	private List<TipoTransitoDeAnimais> listaTipoTransitoDeAnimais;
	
	private List<DoencaEnfermidadeAbatedouroFrigorifico> listaDoencaEnfermidadeAbatedouroFrigorifico;
	
	private List<String> numeroGta;

	private String serieGta;
	
	private Date dataInicialBuscaGta;
	
	private Date dataFinalBuscaGta;
	
	private LoteEnfermidadeAbatedouroFrigorifico loteEnfermidadeAbatedouroFrigorifico;
	
	private GtaEnfermidadeAbatedouroFrigorifico gtaEnfermidadeAbatedouroFrigorifico;
	
	private Gta gta;
	
	private AchadosLoteEnfermidadeAbatedouroFrigorifico achadosLoteEnfermidadeAbatedouroFrigorifico; 
	
	private FormularioDeColheitaTroncoEncefalico formularioDeColheitaTroncoEncefalico; 
	
	private boolean editandoFormulario = false;
	
	@Inject
	private EnfermidadeAbatedouroFrigorificoService enfermidadeAbatedouroFrigorificoService;
	
	@Inject
	private LoteEnfermidadeAbatedouroFrigorificoService loteEnfermidadeAbatedouroFrigorificoService;
	
	@Inject
	private PaisService paisService;
	
	@Inject
	private UFService ufService;
	
	@Inject
	private MunicipioService municipioService;
	
	@Inject
	private TipoExameService tipoExameService;  
	
	@Inject
	private DoencaEnfermidadeAbatedouroFrigorificoService doencaEnfermidadeAbatedouroFrigorificoService;
	
	@Inject
	private ServicoDeInspecaoService servicoDeInspecaoService;
	
	@Inject
	private GtaConverter gtaConverter;

	private List<ServicoDeInspecao> listaServicoDeInspecao;
	
	@PostConstruct
	public void init(){
		this.listaDoencaEnfermidadeAbatedouroFrigorifico = doencaEnfermidadeAbatedouroFrigorificoService.findAll();
		
	}
	
	private void buscarListaServicoDeInspecao(){
		listaServicoDeInspecao = servicoDeInspecaoService.findAll();
	}
	
	@URLAction(mappingId = "manualEnfermidadeAbatedouroFrigorifico", onPostback = false)
	public void imprimirManual() throws ApplicationErrorException{
		visualizadorImpressaoManagedBean.exibirManualEnfermidadeAbatedouroFrigorifico();
	}
	
	@URLAction(mappingId="pesquisarEnfermidadeAbatedouroFrigorifico", onPostback = false)
	public void busca(){
		limpar();
		
		this.buscarListaServicoDeInspecao();
		
		if (loginManagedBean.getUserSecurity().hasAuthorityAdmin()) {
			
		} else if (loginManagedBean.getUserSecurity().getUsuario().getServicoDeInspecaoEmUso() == null) {
			FacesMessageUtil.addWarnContextFacesMessage("O usu�rio n�o possui Servi�o de Inspe��o vinculado. Procure o administrador do sistema", "");
			return;
		} else
			this.enfermidadeAbatedouroFrigorificoDTO.setServicoDeInspecao(loginManagedBean.getUserSecurity().getUsuario().getServicoDeInspecaoEmUso());
		
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		Long idEnfermidadeAbatedouroFrigorifico = (Long) request.getSession().getAttribute("idEnfermidadeAbatedouroFrigorifico");
		
		if (idEnfermidadeAbatedouroFrigorifico != null){
			this.enfermidadeAbatedouroFrigorificoDTO = new EnfermidadeAbatedouroFrigorificoDTO();
			this.enfermidadeAbatedouroFrigorificoDTO.setId(idEnfermidadeAbatedouroFrigorifico);
			this.buscarEnfermidadeAbatedouroFrigorifico();
			
			if (loginManagedBean.getUserSecurity().hasAuthorityAdmin())
				return;
			else
				this.enfermidadeAbatedouroFrigorificoDTO.setServicoDeInspecao(loginManagedBean.getUserSecurity().getUsuario().getServicoDeInspecaoEmUso());
			
			request.getSession().setAttribute("idEnfermidadeAbatedouroFrigorifico", null);
		}
		
	}
	
	public void buscarEnfermidadeAbatedouroFrigorifico(){
		listaLoteEnfermidadeAbatedouroFrigorifico = new LazyObjectDataModel<LoteEnfermidadeAbatedouroFrigorifico>(this.loteEnfermidadeAbatedouroFrigorificoService, enfermidadeAbatedouroFrigorificoDTO);
	}
	
	public void loadEnfermidadeAbatedouroFrigorifico(){
		if (this.dataAbate == null)
			return;
		
		EnfermidadeAbatedouroFrigorifico enfermidadeAbatedouroFrigorifico = enfermidadeAbatedouroFrigorificoService.findByDataAbateFetchAll(this.dataAbate, this.enfermidadeAbatedouroFrigorifico.getServicoDeInspecao());
		if (enfermidadeAbatedouroFrigorifico != null){
			this.enfermidadeAbatedouroFrigorifico = enfermidadeAbatedouroFrigorifico;
			
			FacesContext context = FacesContext.getCurrentInstance();
			
			context.addMessage("cadastro:tableLotes", new FacesMessage(FacesMessage.SEVERITY_INFO, "Foi encontrada uma Enfermidade Abatedouro-Frigorifico para o dia " + DataUtil.getDate_dd_MM_yyyy(this.dataAbate) + " e ela foi carregada automaticamente", ""));
		}
	}
	
	@URLAction(mappingId="novoEnfermidadeAbatedouroFrigorifico", onPostback = false)
	public String novaEntrada(){
		if (this.userSecurity.getUsuario().getServicoDeInspecaoEmUso().getUltimoNumeroFormularioDeColheitaTroncoEncefalico() == null) {
			FacesMessageUtil.addWarnContextFacesMessage("N�o foi poss�vel concluir a a��o", "O Servi�o de Inspe��o vinculado atual n�o possui numera��o do formul�rio de colheita inicializada. ");
			return "pretty:pesquisarEnfermidadeAbatedouroFrigorifico";
		}
		
		this.enfermidadeAbatedouroFrigorifico = new EnfermidadeAbatedouroFrigorifico();
		this.enfermidadeAbatedouroFrigorifico.setServicoDeInspecao(this.userSecurity.getUsuario().getServicoDeInspecaoEmUso());
		
		this.loadListaTipoExame();
		
		uf = ufService.findByIdFetchAll(UF.MATO_GROSSO.getId());
		
		return "";
	}
	
	public boolean usuarioPossuiServicoDeInspecaoVinculado(){
		if (this.userSecurity.getUsuario().getServicoDeInspecaoEmUso() == null){
			FacesMessageUtil.addWarnContextFacesMessage("N�o foi poss�vel concluir a a��o", "O usu�rio n�o possui Servi�o de Inspe��o vinculado. Procure o administrador do sistema");
			return false;
		}
		
		return true;
	}
	
	public String getNovaEntradaUrl(){
		if (this.usuarioPossuiServicoDeInspecaoVinculado()) {
			if (this.userSecurity.getUsuario().getServicoDeInspecaoEmUso().getUltimoNumeroFormularioDeColheitaTroncoEncefalico() != null)
				return "pretty:novoEnfermidadeAbatedouroFrigorifico";
			else {
				FacesMessageUtil.addWarnContextFacesMessage("N�o foi poss�vel concluir a a��o", "O Servi�o de Inspe��o vinculado atual n�o possui numera��o do formul�rio de colheita inicializada. ");
				return "";
			}
		}else
			return "";
	}
	
	private void limpar(){
		this.enfermidadeAbatedouroFrigorifico = new EnfermidadeAbatedouroFrigorifico();
	}
	
	public String adicionar() throws IOException{
		boolean isEnfermidadeAbatedouroFrigoricoOk = true;
		FacesContext context = FacesContext.getCurrentInstance();
		
		if (this.dataAbate != null){
			if (enfermidadeAbatedouroFrigorifico.getDataAbate() == null)
				this.enfermidadeAbatedouroFrigorifico.setDataAbate(Calendar.getInstance());
			this.enfermidadeAbatedouroFrigorifico.getDataAbate().setTime(this.dataAbate);
		} else
			this.enfermidadeAbatedouroFrigorifico.setDataAbate(null);
		
		
		
		if (this.enfermidadeAbatedouroFrigorifico.getListaLoteEnfermidadeAbatedouroFrigorifico() == null || this.enfermidadeAbatedouroFrigorifico.getListaLoteEnfermidadeAbatedouroFrigorifico().isEmpty()){
			isEnfermidadeAbatedouroFrigoricoOk = false;
			
			context.addMessage("cadastro:tableLotes", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Lotes: deve ser adicionado ao menos um", ""));
		} else {
			for (LoteEnfermidadeAbatedouroFrigorifico loteEnfermidadeAbatedouroFrigorifico : this.enfermidadeAbatedouroFrigorifico.getListaLoteEnfermidadeAbatedouroFrigorifico()) {
				if (loteEnfermidadeAbatedouroFrigorifico.getListaGtaEnfermidadeAbatedouroFrigorifico() == null || loteEnfermidadeAbatedouroFrigorifico.getListaGtaEnfermidadeAbatedouroFrigorifico().isEmpty()){
					isEnfermidadeAbatedouroFrigoricoOk = false;
					
					context.addMessage("cadastro:tableLotes", new FacesMessage(FacesMessage.SEVERITY_ERROR, "O Lote " + loteEnfermidadeAbatedouroFrigorifico.getNumero() + " n�o possui nenhum GTA informado", ""));
				}
				
				if (loteEnfermidadeAbatedouroFrigorifico.getListaAchadosLoteEnfermidadeAbatedouroFrigorifico() == null || loteEnfermidadeAbatedouroFrigorifico.getListaAchadosLoteEnfermidadeAbatedouroFrigorifico().isEmpty()){
					isEnfermidadeAbatedouroFrigoricoOk = false;
					
					context.addMessage("cadastro:tableLotes", new FacesMessage(FacesMessage.SEVERITY_ERROR, "O Lote " + loteEnfermidadeAbatedouroFrigorifico.getNumero() + " n�o possui nenhum Achado informado", ""));
				} else {
					for (AchadosLoteEnfermidadeAbatedouroFrigorifico achado : loteEnfermidadeAbatedouroFrigorifico.getListaAchadosLoteEnfermidadeAbatedouroFrigorifico()) {
						if (achado.isHouveFormularioColheita()){
							if (achado.getListaFormularioDeColheitaTroncoEncefalico() == null || achado.getListaFormularioDeColheitaTroncoEncefalico().isEmpty()){
								isEnfermidadeAbatedouroFrigoricoOk = false;
								
								context.addMessage("cadastro:tableLotes", new FacesMessage(FacesMessage.SEVERITY_ERROR, "O Lote " + loteEnfermidadeAbatedouroFrigorifico.getNumero() + " deve acompanhar o(s) Formul�rio de Colheita para " + achado.getDoencaEnfermidadeAbatedouroFrigorifico().getNome(), ""));
							} else {
								if (achado.getQuantidadeCasos() != achado.getListaFormularioDeColheitaTroncoEncefalico().size()){
									isEnfermidadeAbatedouroFrigoricoOk = false;
									
									context.addMessage("cadastro:tableLotes", new FacesMessage(FacesMessage.SEVERITY_ERROR, "A quantidade de Formul�rios de " + achado.getDoencaEnfermidadeAbatedouroFrigorifico().getNome() + " do lote " + achado.getLoteEnfermidadeAbatedouroFrigorifico().getNumero() + " deve ser igual a quantidade de casos informados", ""));
								}
							}
						}
					}
				}
			}
			
			
		}
		
		if (!isEnfermidadeAbatedouroFrigoricoOk)
			return null;
		
		if (enfermidadeAbatedouroFrigorifico.getId() != null){
			this.enfermidadeAbatedouroFrigorificoService.saveOrUpdate(enfermidadeAbatedouroFrigorifico);
			FacesMessageUtil.addInfoContextFacesMessage("Enfermidade Abatedouro-Frigorifico atualizada", "");
		}else{
			this.enfermidadeAbatedouroFrigorifico.setDataCadastro(Calendar.getInstance());
			this.enfermidadeAbatedouroFrigorificoService.saveOrUpdate(enfermidadeAbatedouroFrigorifico);
			FacesMessageUtil.addInfoContextFacesMessage("Enfermidade Abatedouro-Frigorifico adicionada", "");
		}
		
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		request.getSession().setAttribute("idEnfermidadeAbatedouroFrigorifico", this.enfermidadeAbatedouroFrigorifico.getId());
//		
//		try {
//			for (LoteEnfermidadeAbatedouroFrigorifico lote : enfermidadeAbatedouroFrigorifico.getListaLoteEnfermidadeAbatedouroFrigorifico()) {
//				Email.sendEmailNotificacaoAchadosEnfermidadeAbatedouroFrigorifico("leonardoassis@indea.mt.gov.br", lote);
//			}
//		} catch (EmailException e) {
//			FacesMessageUtil.addErrorContextFacesMessage("N�o foi poss�vel enviar o email para o SFA", "Entre em contato com o administrador");
//			return "";
//		}
		
		limpar();
		
		return "pretty:pesquisarEnfermidadeAbatedouroFrigorifico";
	}
	
	@URLAction(mappingId = "alterarEnfermidadeAbatedouroFrigorifico", onPostback = false)
	public void editar(){
		this.enfermidadeAbatedouroFrigorifico = enfermidadeAbatedouroFrigorificoService.findByIdFetchAll(this.getId());
		
		if (this.enfermidadeAbatedouroFrigorifico.getDataAbate() != null)
			this.dataAbate = this.enfermidadeAbatedouroFrigorifico.getDataAbate().getTime();
		
		this.editandoFormulario = true;
	}
	
	@Inject
	private VisualizadorImpressaoManagedBean visualizadorImpressaoManagedBean;
	
	@URLAction(mappingId = "impressaoFormularioColheitaBSE", onPostback = false)
	public void imprimir() throws ApplicationErrorException{
		
		
		FormularioDeColheitaTroncoEncefalico formularioDeColheitaTroncoEncefalico = enfermidadeAbatedouroFrigorificoService.findFormularioDeColheitaTroncoEncefalicoFetchAll(idFormularioColheitaBSE);
		
		visualizadorImpressaoManagedBean.exibirFormularioDeColheitaTroncoEncefalico(formularioDeColheitaTroncoEncefalico);
	}
	
	public void remover(EnfermidadeAbatedouroFrigorifico enfermidadeAbatedouroFrigorifico){
		this.enfermidadeAbatedouroFrigorificoService.delete(enfermidadeAbatedouroFrigorifico);

		FacesMessageUtil.addInfoContextFacesMessage("Enfermidade Abatedouro-Frigorifico exclu�da com sucesso", "");
	}
	
	public List<Municipio> getListaMunicipios(){
		if (this.uf == null)
			return null;
		
		return municipioService.findAllByUF(this.uf);
	}
	
	public List<Municipio> getListaMunicipiosBusca(){
		return municipioService.findAllByUF(this.enfermidadeAbatedouroFrigorificoDTO.getUf());
	}
	
	private void loadListaTipoExame(){
		this.listaTipoExame = tipoExameService.findAll();
	}
	
	public void novoLoteEnfermidadeAbatedouroFrigorifico(){
		loteEnfermidadeAbatedouroFrigorifico = new LoteEnfermidadeAbatedouroFrigorifico();
	}
	
	public void adicionarLoteEnfermidade(){
		if (this.enfermidadeAbatedouroFrigorifico.getListaLoteEnfermidadeAbatedouroFrigorifico().contains(loteEnfermidadeAbatedouroFrigorifico)){
			FacesMessageUtil.addWarnContextFacesMessage("Lote j� foi inclu�do anteriormente", "");
		} else {
			this.loteEnfermidadeAbatedouroFrigorifico.setEnfermidadeAbatedouroFrigorifico(this.enfermidadeAbatedouroFrigorifico);
			this.enfermidadeAbatedouroFrigorifico.getListaLoteEnfermidadeAbatedouroFrigorifico().add(this.loteEnfermidadeAbatedouroFrigorifico);
			
			FacesMessageUtil.addInfoContextFacesMessage("Lote inclu�do com sucesso", "");
		}
	}
	
	public void removerLoteEnfermidadeAbatedouroFrigorifico(LoteEnfermidadeAbatedouroFrigorifico loteEnfermidadeAbatedouroFrigorifico){
		this.enfermidadeAbatedouroFrigorifico.getListaLoteEnfermidadeAbatedouroFrigorifico().remove(loteEnfermidadeAbatedouroFrigorifico);
	}
	
	public void removerProprietario(LoteEnfermidadeAbatedouroFrigorifico loteEnfermidadeAbatedouroFrigorifico){
		loteEnfermidadeAbatedouroFrigorifico.setListaAchadosLoteEnfermidadeAbatedouroFrigorifico(new ArrayList<>());
		loteEnfermidadeAbatedouroFrigorifico.setListaGtaEnfermidadeAbatedouroFrigorifico(new ArrayList<>());

		loteEnfermidadeAbatedouroFrigorifico.setProprietario(null);
	}
	
	public void abrirTelaGtaLoteEnfermidadeAbatedouroFrigorifico(LoteEnfermidadeAbatedouroFrigorifico loteEnfermidadeAbatedouroFrigorifico){
		this.loteEnfermidadeAbatedouroFrigorifico = loteEnfermidadeAbatedouroFrigorifico;
	}
	
	public void novoGtaEnfermidadeAbatedouroFrigorifico(){
		this.listaGtas = null;
		this.numeroGta = null;
		this.serieGta = null;
		this.listaTipoTransitoDeAnimais = null;
		
		Calendar dataInicial = Calendar.getInstance();
		dataInicial.set(Calendar.MONTH, dataInicial.get(Calendar.MONTH) - 2);
		this.dataInicialBuscaGta = dataInicial.getTime();
		this.dataFinalBuscaGta = Calendar.getInstance().getTime();
	}
	
	public void remover(GtaEnfermidadeAbatedouroFrigorifico gtaEnfermidadeAbatedouroFrigorifico){
		this.loteEnfermidadeAbatedouroFrigorifico.getListaGtaEnfermidadeAbatedouroFrigorifico().remove(gtaEnfermidadeAbatedouroFrigorifico);
		
		if (this.loteEnfermidadeAbatedouroFrigorifico.getListaGtaEnfermidadeAbatedouroFrigorifico().isEmpty())
			this.loteEnfermidadeAbatedouroFrigorifico.setProprietario(null);
	}
	
	public void abrirTelaDeBuscaDeGta(LoteEnfermidadeAbatedouroFrigorifico loteEnfermidadeAbatedouroFrigorifico){
		this.numeroGta = null;
		this.serieGta = null;
		this.listaGtas = null;
		
		if (loteEnfermidadeAbatedouroFrigorifico != null)
			this.loteEnfermidadeAbatedouroFrigorifico = loteEnfermidadeAbatedouroFrigorifico;
	}
	
	public void abrirTelaDeCadastroDeGtaDeForaDoEstado(LoteEnfermidadeAbatedouroFrigorifico loteEnfermidadeAbatedouroFrigorifico) {
		if (loteEnfermidadeAbatedouroFrigorifico != null)
			this.loteEnfermidadeAbatedouroFrigorifico = loteEnfermidadeAbatedouroFrigorifico;
		
		this.gta = new Gta();
		TipoGta tipoGta = new TipoGta();
		tipoGta.setId("S");
		tipoGta.setNome("GTA DE SA�DA");
		gta.setTipoGta(tipoGta);
		gta.setUfDestino(UF.MATO_GROSSO);
		
		gta.setPessoaOrigemGta(new PessoaOrigemGta());
		gta.getPessoaOrigemGta().setPropriedade(new Propriedade());
		gta.getPessoaOrigemGta().setProdutor(new Produtor());
		
		ServicoDeInspecao servicoDeInspecaoEmUso = this.userSecurity.getUsuario().getServicoDeInspecaoEmUso();
		
		if (this.loteEnfermidadeAbatedouroFrigorifico.getProprietario() != null && this.loteEnfermidadeAbatedouroFrigorifico.getProprietario().isPessoaOrigemDeOutroEstado()) {
			this.gta.setPessoaDestino(this.loteEnfermidadeAbatedouroFrigorifico.getListaGtaEnfermidadeAbatedouroFrigorifico().get(0).getGta().getPessoaDestino());
			this.gta.setPessoaOrigemGta(this.loteEnfermidadeAbatedouroFrigorifico.getListaGtaEnfermidadeAbatedouroFrigorifico().get(0).getGta().getPessoaOrigemGta());
			this.uf = this.gta.getPessoaOrigemGta().getPropriedade().getMunicipio().getUf();
			this.gta.setEspecie(this.loteEnfermidadeAbatedouroFrigorifico.getListaGtaEnfermidadeAbatedouroFrigorifico().get(0).getGta().getEspecie());
			this.especie = this.gta.getEspecie().getNome();
		} else {
			gta.setPessoaDestino(new PessoaDestinoGta());
			gta.getPessoaDestino().setCnpjCpf(servicoDeInspecaoEmUso.getAbatedouro().getCnpj());
			gta.getPessoaDestino().setCodigoEstabelecimento(servicoDeInspecaoEmUso.getAbatedouro().getCodigo());
			gta.getPessoaDestino().setEstabelecimento(new Pessoa());
			gta.getPessoaDestino().getEstabelecimento().setApelido(servicoDeInspecaoEmUso.getAbatedouro().getApelido());
			gta.getPessoaDestino().getEstabelecimento().setNome(servicoDeInspecaoEmUso.getAbatedouro().getNome());
			gta.getPessoaDestino().getEstabelecimento().setCpf(servicoDeInspecaoEmUso.getAbatedouro().getCnpj());
			gta.getPessoaDestino().getEstabelecimento().setCodigo(servicoDeInspecaoEmUso.getAbatedouro().getCodigo());
			gta.getPessoaDestino().getEstabelecimento().setEndereco(servicoDeInspecaoEmUso.getAbatedouro().getEndereco());
			gta.getPessoaDestino().getEstabelecimento().setTipoPessoa("J");
			gta.getPessoaDestino().setMunicipio(servicoDeInspecaoEmUso.getAbatedouro().getMunicipio());
			gta.getPessoaDestino().setProdutor(new Pessoa());
			gta.getPessoaDestino().getProdutor().setApelido(servicoDeInspecaoEmUso.getAbatedouro().getApelido());
			gta.getPessoaDestino().getProdutor().setNome(servicoDeInspecaoEmUso.getAbatedouro().getNome());
			gta.getPessoaDestino().getProdutor().setCpf(servicoDeInspecaoEmUso.getAbatedouro().getCnpj());
			gta.getPessoaDestino().getProdutor().setCodigo(servicoDeInspecaoEmUso.getAbatedouro().getCodigo());
			gta.getPessoaDestino().getProdutor().setEndereco(servicoDeInspecaoEmUso.getAbatedouro().getEndereco());
			gta.getPessoaDestino().getProdutor().setTipoPessoa("J");
		}
		
	}
	
	public void buscarGtaPorNumeroESerie(){
		this.listaGtas = new ArrayList<>();
		br.gov.mt.indea.sistemaformin.webservice.model.Gta gta = null;
		
		for (String numero : numeroGta) {
			try {
				gta = clientWebServiceGTA.getModelGTAByNumeroSerie(Integer.parseInt(numero), serieGta);
				if (gta != null){
					this.listaGtas.add(gta);
				}
			} catch (ApplicationException e) {
				FacesMessageUtil.addErrorContextFacesMessage("Erro", e.getLocalizedMessage());
				return;
			}
		}
		
		if (this.listaGtas == null || this.listaGtas.isEmpty())
			FacesMessageUtil.addWarnContextFacesMessage("Nenhum gta foi encontrado", "");
	}
	
	public void buscarGtaPorCnpjEPeriodo(){
		this.listaGtas = new ArrayList<>();
		
		try {
			listaGtas = clientWebServiceGTA.getModelGTAByPeriodoEstabelecimento(this.userSecurity.getUsuario().getServicoDeInspecaoEmUso().getAbatedouro().getCnpj(), dataInicialBuscaGta, dataFinalBuscaGta);
		} catch (ApplicationException e) {
			FacesMessageUtil.addErrorContextFacesMessage("Erro", e.getLocalizedMessage());
			return;
		}
		
		if (this.listaGtas == null || this.listaGtas.isEmpty())
			FacesMessageUtil.addWarnContextFacesMessage("Nenhum gta foi encontrado", "");
	}
	
	public void selecionarGtas(){
		Gta gta = null;
		GtaEnfermidadeAbatedouroFrigorifico gtaEnfermidadeAbatedouroFrigorifico = null;
		boolean haGtasNaoSelecionados = false; 
		boolean haGtasSelecionados = false;
		StringBuilder gtasNaoSelecionados = new StringBuilder();
		StringBuilder gtasSelecionados = new StringBuilder();
		
		for (br.gov.mt.indea.sistemaformin.webservice.model.Gta modelGta : listaGtas) {
			if (modelGta.isSelecionadoParaInclusao()){
				if (!this.loteEnfermidadeAbatedouroFrigorifico.contemGta(modelGta.getSerie(), modelGta.getNumero())){
					try {
						gta = gtaConverter.fromModelToEntity(modelGta);
					} catch (ApplicationException e) {
						FacesMessageUtil.addErrorContextFacesMessage("Ocorreu um erro ao selecionar os GTAs", "Contate o administrador do sistema");
						e.printStackTrace();
					}
				
					gtaEnfermidadeAbatedouroFrigorifico = new GtaEnfermidadeAbatedouroFrigorifico();
					
					gtaEnfermidadeAbatedouroFrigorifico.setGta(gta);
					gtaEnfermidadeAbatedouroFrigorifico.setLoteEnfermidadeAbatedouroFrigorifico(this.loteEnfermidadeAbatedouroFrigorifico);
					
					if (this.loteEnfermidadeAbatedouroFrigorifico.getProprietario() == null){
						if (this.loteEnfermidadeAbatedouroFrigorifico.getListaGtaEnfermidadeAbatedouroFrigorifico().isEmpty())
							this.loteEnfermidadeAbatedouroFrigorifico.setProprietario(gta.getPessoaOrigemGta());
					} 
					
					if (this.loteEnfermidadeAbatedouroFrigorifico.getProprietario().equals(gta.getPessoaOrigemGta())){
						this.loteEnfermidadeAbatedouroFrigorifico.getListaGtaEnfermidadeAbatedouroFrigorifico().add(gtaEnfermidadeAbatedouroFrigorifico);
						
						haGtasSelecionados = true;
						gtasSelecionados.append(gta.getNumero()).append("-")
						  .append(gta.getSerie()).append(", ");
					} else{
						haGtasNaoSelecionados = true;
						gtasNaoSelecionados.append(gta.getNumero()).append("-")
						  .append(gta.getSerie()).append(", ");
					}
				}
			}
		}
		
		if (haGtasSelecionados)
			FacesMessageUtil.addInfoContextFacesMessage("Gtas " + gtasSelecionados.toString() + " selecionados", "");
		if (haGtasNaoSelecionados)
			FacesMessageUtil.addWarnContextFacesMessage("Gtas " + gtasNaoSelecionados.toString() + " n�o foram selecionados por n�o terem a mesma origem do Lote", "");
		
	}
	
	public void adicionarGtaDeForaDoEstado(){
		gta.setEspecie(new Especie());
		gta.getEspecie().setEsp_nome(especie);
		gta.getPessoaOrigemGta().setMunicipio(gta.getPessoaOrigemGta().getPropriedade().getMunicipio());
		
		GtaEnfermidadeAbatedouroFrigorifico gtaEnfermidadeAbatedouroFrigorifico = null;
		boolean haGtasNaoSelecionados = false; 
		boolean haGtasSelecionados = false;
		StringBuilder gtasNaoSelecionados = new StringBuilder();
		StringBuilder gtasSelecionados = new StringBuilder();
		
		gtaEnfermidadeAbatedouroFrigorifico = new GtaEnfermidadeAbatedouroFrigorifico();
		
		gtaEnfermidadeAbatedouroFrigorifico.setGta(gta);
		gtaEnfermidadeAbatedouroFrigorifico.setLoteEnfermidadeAbatedouroFrigorifico(this.loteEnfermidadeAbatedouroFrigorifico);
		
		if (this.loteEnfermidadeAbatedouroFrigorifico.getProprietario() == null){
			if (this.loteEnfermidadeAbatedouroFrigorifico.getListaGtaEnfermidadeAbatedouroFrigorifico().isEmpty())
				this.loteEnfermidadeAbatedouroFrigorifico.setProprietario(gta.getPessoaOrigemGta());
		} 
		
		if (this.loteEnfermidadeAbatedouroFrigorifico.getProprietario().equals(gta.getPessoaOrigemGta())){
			this.loteEnfermidadeAbatedouroFrigorifico.getListaGtaEnfermidadeAbatedouroFrigorifico().add(gtaEnfermidadeAbatedouroFrigorifico);
			
			haGtasSelecionados = true;
			gtasSelecionados.append(gta.getNumero()).append("-")
			  .append(gta.getSerie()).append(", ");
		} else{
			haGtasNaoSelecionados = true;
			gtasNaoSelecionados.append(gta.getNumero()).append("-")
			  .append(gta.getSerie()).append(", ");
		}
		
		if (haGtasSelecionados)
			FacesMessageUtil.addInfoContextFacesMessage("Gtas " + gtasSelecionados.toString() + " selecionados", "");
		if (haGtasNaoSelecionados)
			FacesMessageUtil.addWarnContextFacesMessage("Gtas " + gtasNaoSelecionados.toString() + " n�o foram selecionados por n�o terem a mesma origem do Lote", "");
		
		this.gta = null;
		this.uf = null;
		this.especie = null;
	}
	
	public void cancelarCadastroDeGtaDeForaDoEstado(){
		this.gta = null;
		this.loteEnfermidadeAbatedouroFrigorifico = null;
		this.uf = null;
		this.especie = null;
	}
	
	private void preencherTelaDeSelecaoDeEnfermidadeAbatedouroFrigorificos(LoteEnfermidadeAbatedouroFrigorifico loteEnfermidadeAbatedouroFrigorifico){
		this.loteEnfermidadeAbatedouroFrigorifico = loteEnfermidadeAbatedouroFrigorifico;
		
		if (this.loteEnfermidadeAbatedouroFrigorifico.getListaAchadosLoteEnfermidadeAbatedouroFrigorifico() == null)
			this.loteEnfermidadeAbatedouroFrigorifico.setListaAchadosLoteEnfermidadeAbatedouroFrigorifico(new ArrayList<AchadosLoteEnfermidadeAbatedouroFrigorifico>());
		
		for (DoencaEnfermidadeAbatedouroFrigorifico doencaEnfermidadeAbatedouroFrigorifico : this.listaDoencaEnfermidadeAbatedouroFrigorifico) {
			if (!loteEnfermidadeAbatedouroFrigorifico.contem(doencaEnfermidadeAbatedouroFrigorifico))
				this.loteEnfermidadeAbatedouroFrigorifico.getListaAchadosLoteEnfermidadeAbatedouroFrigorifico().add(new AchadosLoteEnfermidadeAbatedouroFrigorifico(loteEnfermidadeAbatedouroFrigorifico, doencaEnfermidadeAbatedouroFrigorifico, null));
		}
		
		if (loteEnfermidadeAbatedouroFrigorifico.getListaAchadosLoteEnfermidadeAbatedouroFrigorifico().size() > 0) {
			  Collections.sort(loteEnfermidadeAbatedouroFrigorifico.getListaAchadosLoteEnfermidadeAbatedouroFrigorifico(), new Comparator<AchadosLoteEnfermidadeAbatedouroFrigorifico>() {
			      @Override
			      public int compare(final AchadosLoteEnfermidadeAbatedouroFrigorifico object1, final AchadosLoteEnfermidadeAbatedouroFrigorifico object2) {
			          return object1.getDoencaEnfermidadeAbatedouroFrigorifico().getNome().compareTo(object2.getDoencaEnfermidadeAbatedouroFrigorifico().getNome());
			      }
			  });
			}
		
		
	}
	
	public void abrirTelaDeAchadosLoteEnfermidadeAbatedouroFrigorifico(LoteEnfermidadeAbatedouroFrigorifico loteEnfermidadeAbatedouroFrigorifico){
		this.loteEnfermidadeAbatedouroFrigorifico = loteEnfermidadeAbatedouroFrigorifico;
		
		this.preencherTelaDeSelecaoDeEnfermidadeAbatedouroFrigorificos(this.loteEnfermidadeAbatedouroFrigorifico);
	}
	
	public void recalcularAchadosLoteEnfermidadeAbatedouroFrigorifico(LoteEnfermidadeAbatedouroFrigorifico LoteEnfermidadeAbatedouroFrigorifico){
		for (int i = 0; i < loteEnfermidadeAbatedouroFrigorifico.getListaAchadosLoteEnfermidadeAbatedouroFrigorifico().size(); i++) {

			if (loteEnfermidadeAbatedouroFrigorifico.getListaAchadosLoteEnfermidadeAbatedouroFrigorifico().get(i).getQuantidadeCasos() == null || loteEnfermidadeAbatedouroFrigorifico.getListaAchadosLoteEnfermidadeAbatedouroFrigorifico().get(i).getQuantidadeCasos() == 0){
				loteEnfermidadeAbatedouroFrigorifico.getListaAchadosLoteEnfermidadeAbatedouroFrigorifico().remove(i);
				i--;
			}
		}
	}
	
	public void abrirTelaDeListaDeFormularioDeColheitaTroncoEncefalico(AchadosLoteEnfermidadeAbatedouroFrigorifico achadosLoteEnfermidadeAbatedouroFrigorifico){
		this.achadosLoteEnfermidadeAbatedouroFrigorifico = achadosLoteEnfermidadeAbatedouroFrigorifico;
	}
	
	public void abrirTelaDeListaDeFormularioDeColheitaTroncoEncefalico(LoteEnfermidadeAbatedouroFrigorifico loteEnfermidadeAbatedouroFrigorifico){
		this.loteEnfermidadeAbatedouroFrigorifico = loteEnfermidadeAbatedouroFrigorifico;
		enfermidadeAbatedouroFrigorificoService.fetchListaFormularios(this.loteEnfermidadeAbatedouroFrigorifico);
	}
	
	public void abrirTelaDeCadastroDeFormularioDeColheitaTroncoEncefalico(AchadosLoteEnfermidadeAbatedouroFrigorifico achadosLoteEnfermidadeAbatedouroFrigorifico){
		this.achadosLoteEnfermidadeAbatedouroFrigorifico = achadosLoteEnfermidadeAbatedouroFrigorifico;
		this.dataColeta = null;
		this.editandoFormulario = false;
		this.pais = this.paisService.findByIdFetchAll(Pais.BRASIL.getId());
		
		this.formularioDeColheitaTroncoEncefalico = new FormularioDeColheitaTroncoEncefalico();
		this.formularioDeColheitaTroncoEncefalico.setAchadosLoteEnfermidadeAbatedouroFrigorifico(achadosLoteEnfermidadeAbatedouroFrigorifico);
		this.formularioDeColheitaTroncoEncefalico.setGta(this.formularioDeColheitaTroncoEncefalico.getAchadosLoteEnfermidadeAbatedouroFrigorifico().getLoteEnfermidadeAbatedouroFrigorifico().getListaAchadosLoteEnfermidadeAbatedouroFrigorifico().get(0).getLoteEnfermidadeAbatedouroFrigorifico().getListaGtaEnfermidadeAbatedouroFrigorifico().get(0).getGta());
		
		this.formularioDeColheitaTroncoEncefalico.setVeterinario(this.userSecurity.getUsuario().getVeterinarioRemetente());
		
		this.formularioDeColheitaTroncoEncefalico.setPais(this.pais);
		
	}
	
	public void adicionarFormularioDeColheitaTroncoEncefalicoEAbrirTelaDeCadastroDeFormularioDeColheitaTroncoEncefalico(){
		this.adicionarFormularioDeColheitaTroncoEncefalico();
		this.abrirTelaDeCadastroDeFormularioDeColheitaTroncoEncefalico(this.achadosLoteEnfermidadeAbatedouroFrigorifico);
		
		FacesMessageUtil.addInfoContextFacesMessage("Tela permanece aberta para inclus�o do pr�ximo formul�rio", "");
	}
	
	public void adicionarFormularioDeColheitaTroncoEncefalico(){
		if (this.dataColeta != null){
			if (formularioDeColheitaTroncoEncefalico.getDataColeta() == null)
				this.formularioDeColheitaTroncoEncefalico.setDataColeta(Calendar.getInstance());
			this.formularioDeColheitaTroncoEncefalico.getDataColeta().setTime(this.dataColeta);
		} else
			this.formularioDeColheitaTroncoEncefalico.setDataColeta(null);
		
		this.formularioDeColheitaTroncoEncefalico.setAchadosLoteEnfermidadeAbatedouroFrigorifico(this.achadosLoteEnfermidadeAbatedouroFrigorifico);
		
		if (!editandoFormulario)
			this.achadosLoteEnfermidadeAbatedouroFrigorifico.getListaFormularioDeColheitaTroncoEncefalico().add(this.formularioDeColheitaTroncoEncefalico);
		else
			editandoFormulario = false;
		
		FacesMessageUtil.addInfoContextFacesMessage("Formul�rio inclu�do com sucesso", "");
	}

	public void editarFormularioDeColheitaTroncoEncefalico(FormularioDeColheitaTroncoEncefalico formularioDeColheitaTroncoEncefalico){
		this.editandoFormulario = true;
		
		this.formularioDeColheitaTroncoEncefalico = formularioDeColheitaTroncoEncefalico;
		
		if (this.formularioDeColheitaTroncoEncefalico.getDataColeta() != null)
			this.dataColeta = this.formularioDeColheitaTroncoEncefalico.getDataColeta().getTime();
	}
	
	public void remover(FormularioDeColheitaTroncoEncefalico formularioDeColheitaTroncoEncefalico){
		this.achadosLoteEnfermidadeAbatedouroFrigorifico.getListaFormularioDeColheitaTroncoEncefalico().remove(formularioDeColheitaTroncoEncefalico);
	}
	
	public void selecionarTipoDeMorte() {
		if (this.formularioDeColheitaTroncoEncefalico == null)
			return;
		
		if (this.formularioDeColheitaTroncoEncefalico.getTipoDeMorteEmFrigorifico() == null ||
				this.formularioDeColheitaTroncoEncefalico.getTipoDeMorteEmFrigorifico().equals(TipoDeMorteEmFrigorifico.ENCONTRADO_MORTO_NO_DESEMBARQUE) || 
				this.formularioDeColheitaTroncoEncefalico.getTipoDeMorteEmFrigorifico().equals(TipoDeMorteEmFrigorifico.ENCONTRADO_MORTO_NO_MATADOURO)) {

			this.formularioDeColheitaTroncoEncefalico.setListaMotivacaoParaAbateDeEmergenciaEmFrigorifico(null);
			this.formularioDeColheitaTroncoEncefalico.setOutraMotivacao(null);
			this.formularioDeColheitaTroncoEncefalico.setListaSinaisClinicosNervososEncefalopatoaEspongiformeTransmissivel(null);
			this.formularioDeColheitaTroncoEncefalico.setOutroSinal(null);
		}
	}
	
	public boolean isTipoDeMorteEmFrigorificoIgualASubmetidoAAbateEmergencial(){
		boolean resultado = false;
		
		if (this.formularioDeColheitaTroncoEncefalico == null)
			return resultado;
		
		if (this.formularioDeColheitaTroncoEncefalico.getTipoDeMorteEmFrigorifico() == null)
			return resultado;
		else
			resultado = this.formularioDeColheitaTroncoEncefalico.getTipoDeMorteEmFrigorifico().equals(TipoDeMorteEmFrigorifico.SUBMETIDO_A_ABATE_EMERGENCIAL);
		
		return resultado;
	}
	
	public void selecionarMotivacaoParaAbadeDeEmergenciaEmFrigorifico() {
		if (this.formularioDeColheitaTroncoEncefalico == null)
			return;
		
		if (this.formularioDeColheitaTroncoEncefalico.getListaMotivacaoParaAbateDeEmergenciaEmFrigorifico() == null ||
				!this.formularioDeColheitaTroncoEncefalico.getListaMotivacaoParaAbateDeEmergenciaEmFrigorifico().contains(MotivacaoParaAbateDeEmergenciaEmFrigorifico.DISTURBIOS_NERVOSOS)) {

			this.formularioDeColheitaTroncoEncefalico.setListaSinaisClinicosNervososEncefalopatoaEspongiformeTransmissivel(null);
			this.formularioDeColheitaTroncoEncefalico.setOutroSinal(null);
		}
	}

	
	public boolean isMotivacaoParaAbateDeEmergenciaEmFrigorificoIgualADisturbios_Nervosos(){
		boolean resultado = false;
		
		if (this.formularioDeColheitaTroncoEncefalico == null)
			return resultado;
		
		if (this.formularioDeColheitaTroncoEncefalico.getListaMotivacaoParaAbateDeEmergenciaEmFrigorifico() == null)
			return resultado;
		else
			resultado = this.formularioDeColheitaTroncoEncefalico.getListaMotivacaoParaAbateDeEmergenciaEmFrigorifico().contains(MotivacaoParaAbateDeEmergenciaEmFrigorifico.DISTURBIOS_NERVOSOS);
		
		return resultado;
	}
	
	public boolean isMotivacaoParaAbateDeEmergenciaEmFrigorificoIgualAOutroMotivo(){
		boolean resultado = false;
		
		if (this.formularioDeColheitaTroncoEncefalico == null)
			return resultado;
		
		if (this.formularioDeColheitaTroncoEncefalico.getListaMotivacaoParaAbateDeEmergenciaEmFrigorifico() == null)
			return resultado;
		else
			resultado = this.formularioDeColheitaTroncoEncefalico.getListaMotivacaoParaAbateDeEmergenciaEmFrigorifico().contains(MotivacaoParaAbateDeEmergenciaEmFrigorifico.OUTRO);
		
		return resultado;
	}
	
	public boolean isSinaisClinicosNervososEncefalopatoaEspongiformeTransmissivelAOutroSinal(){
		boolean resultado = false;
		
		if (this.formularioDeColheitaTroncoEncefalico == null)
			return resultado;
		
		if (this.formularioDeColheitaTroncoEncefalico.getListaSinaisClinicosNervososEncefalopatoaEspongiformeTransmissivel() == null)
			return resultado;
		else
			resultado = this.formularioDeColheitaTroncoEncefalico.getListaSinaisClinicosNervososEncefalopatoaEspongiformeTransmissivel().contains(SinaisClinicosNervososEncefalopatoaEspongiformeTransmissivel.OUTRO);
		
		return resultado;
	}
	
	public List<String> getListaEspeciesGTA(){
		List<String> especies = new ArrayList<>();
		
		especies.add("BOVINO");
		especies.add("BUBALINO");
		especies.add("CAPRINO");
		especies.add("OVINO");
		
		return especies;
	}
	
	public boolean isUserAdmin(){
		return this.userSecurity.hasAuthorityAdmin();
	}
	
	public void abrirTelaDeTipoDeGta(LoteEnfermidadeAbatedouroFrigorifico loteEnfermidadeAbatedouroFrigorifico) {
		this.loteEnfermidadeAbatedouroFrigorifico = loteEnfermidadeAbatedouroFrigorifico;
	}
	
	public LazyObjectDataModel<LoteEnfermidadeAbatedouroFrigorifico> getListaLoteEnfermidadeAbatedouroFrigorifico() {
		return listaLoteEnfermidadeAbatedouroFrigorifico;
	}

	public EnfermidadeAbatedouroFrigorifico getEnfermidadeAbatedouroFrigorifico() {
		return enfermidadeAbatedouroFrigorifico;
	}

	public void setEnfermidadeAbatedouroFrigorifico(EnfermidadeAbatedouroFrigorifico enfermidadeAbatedouroFrigorifico) {
		this.enfermidadeAbatedouroFrigorifico = enfermidadeAbatedouroFrigorifico;
	}

	public Date getDataAbate() {
		return dataAbate;
	}

	public void setDataAbate(Date dataAbate) {
		this.dataAbate = dataAbate;
	}

	public Date getDataColeta() {
		return dataColeta;
	}

	public void setDataColeta(Date dataColeta) {
		this.dataColeta = dataColeta;
	}

	public UF getUf() {
		return uf;
	}

	public void setUf(UF uf) {
		this.uf = uf;
	}

	public List<TipoExame> getListaTipoExame() {
		return listaTipoExame;
	}

	public void setListaTipoExame(List<TipoExame> listaTipoExame) {
		this.listaTipoExame = listaTipoExame;
	}

	public void setListaEnfermidadeAbatedouroFrigorifico(LazyObjectDataModel<LoteEnfermidadeAbatedouroFrigorifico> listaLoteEnfermidadeAbatedouroFrigorifico) {
		this.listaLoteEnfermidadeAbatedouroFrigorifico = listaLoteEnfermidadeAbatedouroFrigorifico;
	}

	public String getCpfVeterinario() {
		return cpfVeterinario;
	}

	public void setCpfVeterinario(String cpfVeterinario) {
		this.cpfVeterinario = cpfVeterinario;
	}

	public String getCrmvVeterinario() {
		return crmvVeterinario;
	}

	public void setCrmvVeterinario(String crmvVeterinario) {
		this.crmvVeterinario = crmvVeterinario;
	}

	public List<Veterinario> getListaVeterinarios() {
		return listaVeterinarios;
	}

	public void setListaVeterinarios(List<Veterinario> listaVeterinarios) {
		this.listaVeterinarios = listaVeterinarios;
	}

	public EnfermidadeAbatedouroFrigorificoDTO getEnfermidadeAbatedouroFrigorificoDTO() {
		return enfermidadeAbatedouroFrigorificoDTO;
	}

	public void setEnfermidadeAbatedouroFrigorificoDTO(EnfermidadeAbatedouroFrigorificoDTO enfermidadeAbatedouroFrigorificoDTO) {
		this.enfermidadeAbatedouroFrigorificoDTO = enfermidadeAbatedouroFrigorificoDTO;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<br.gov.mt.indea.sistemaformin.webservice.model.Gta> getListaGtas() {
		return listaGtas;
	}

	public void setListaGtas(List<br.gov.mt.indea.sistemaformin.webservice.model.Gta> listaGtas) {
		this.listaGtas = listaGtas;
	}

	public List<String> getNumeroGta() {
		return numeroGta;
	}

	public void setNumeroGta(List<String> numeroGta) {
		this.numeroGta = numeroGta;
	}

	public String getSerieGta() {
		return serieGta;
	}

	public void setSerieGta(String serieGta) {
		this.serieGta = serieGta;
	}

	public Date getDataInicialBuscaGta() {
		return dataInicialBuscaGta;
	}

	public void setDataInicialBuscaGta(Date dataInicialBuscaGta) {
		this.dataInicialBuscaGta = dataInicialBuscaGta;
	}

	public Date getDataFinalBuscaGta() {
		return dataFinalBuscaGta;
	}

	public void setDataFinalBuscaGta(Date dataFinalBuscaGta) {
		this.dataFinalBuscaGta = dataFinalBuscaGta;
	}

	public List<TipoTransitoDeAnimais> getListaTipoTransitoDeAnimais() {
		return listaTipoTransitoDeAnimais;
	}

	public void setListaTipoTransitoDeAnimais(List<TipoTransitoDeAnimais> listaTipoTransitoDeAnimais) {
		this.listaTipoTransitoDeAnimais = listaTipoTransitoDeAnimais;
	}

	public GtaEnfermidadeAbatedouroFrigorifico getGtaEnfermidadeAbatedouroFrigorifico() {
		return gtaEnfermidadeAbatedouroFrigorifico;
	}

	public void setGtaEnfermidadeAbatedouroFrigorifico(GtaEnfermidadeAbatedouroFrigorifico gtaEnfermidadeAbatedouroFrigorifico) {
		this.gtaEnfermidadeAbatedouroFrigorifico = gtaEnfermidadeAbatedouroFrigorifico;
	}

	public LoteEnfermidadeAbatedouroFrigorifico getLoteEnfermidadeAbatedouroFrigorifico() {
		return loteEnfermidadeAbatedouroFrigorifico;
	}

	public void setLoteEnfermidadeAbatedouroFrigorifico(
			LoteEnfermidadeAbatedouroFrigorifico loteEnfermidadeAbatedouroFrigorifico) {
		this.loteEnfermidadeAbatedouroFrigorifico = loteEnfermidadeAbatedouroFrigorifico;
	}

	public List<DoencaEnfermidadeAbatedouroFrigorifico> getListaDoencaEnfermidadeAbatedouroFrigorifico() {
		return listaDoencaEnfermidadeAbatedouroFrigorifico;
	}

	public void setListaDoencaEnfermidadeAbatedouroFrigorifico(
			List<DoencaEnfermidadeAbatedouroFrigorifico> listaDoencaEnfermidadeAbatedouroFrigorifico) {
		this.listaDoencaEnfermidadeAbatedouroFrigorifico = listaDoencaEnfermidadeAbatedouroFrigorifico;
	}

	public AchadosLoteEnfermidadeAbatedouroFrigorifico getAchadosLoteEnfermidadeAbatedouroFrigorifico() {
		return achadosLoteEnfermidadeAbatedouroFrigorifico;
	}

	public void setAchadosLoteEnfermidadeAbatedouroFrigorifico(
			AchadosLoteEnfermidadeAbatedouroFrigorifico achadosLoteEnfermidadeAbatedouroFrigorifico) {
		this.achadosLoteEnfermidadeAbatedouroFrigorifico = achadosLoteEnfermidadeAbatedouroFrigorifico;
	}

	public FormularioDeColheitaTroncoEncefalico getFormularioDeColheitaTroncoEncefalico() {
		return formularioDeColheitaTroncoEncefalico;
	}

	public void setFormularioDeColheitaTroncoEncefalico(
			FormularioDeColheitaTroncoEncefalico formularioDeColheitaTroncoEncefalico) {
		this.formularioDeColheitaTroncoEncefalico = formularioDeColheitaTroncoEncefalico;
	}

	public Long getIdFormularioColheitaBSE() {
		return idFormularioColheitaBSE;
	}

	public void setIdFormularioColheitaBSE(Long idFormularioColheitaBSE) {
		this.idFormularioColheitaBSE = idFormularioColheitaBSE;
	}

	public boolean isEditandoFormulario() {
		return editandoFormulario;
	}

	public void setEditandoFormulario(boolean editandoFormulario) {
		this.editandoFormulario = editandoFormulario;
	}

	public List<ServicoDeInspecao> getListaServicoDeInspecao() {
		return listaServicoDeInspecao;
	}

	public void setListaServicoDeInspecao(List<ServicoDeInspecao> listaServicoDeInspecao) {
		this.listaServicoDeInspecao = listaServicoDeInspecao;
	}

	public Gta getGta() {
		return gta;
	}

	public void setGta(Gta gta) {
		this.gta = gta;
	}

	public String getEspecie() {
		return especie;
	}

	public void setEspecie(String especie) {
		this.especie = especie;
	}

}
