package br.gov.mt.indea.sistemaformin.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;

import org.hibernate.envers.Audited;

import br.gov.mt.indea.sistemaformin.enums.Dominio.TipoDestinoExploracao;

@Audited
@Entity
@DiscriminatorValue("suinos")
public class InformacoesSuinos extends AbstractInformacoesDeAnimais implements Cloneable{

	private static final long serialVersionUID = 5555757893786215468L;

	@OneToMany(mappedBy = "informacoesSuinos", cascade={CascadeType.ALL}, orphanRemoval=true)
	@OrderBy("id")
	private List<DetalhesInformacoesSuinos> detalhes = new ArrayList<DetalhesInformacoesSuinos>();

	public List<DetalhesInformacoesSuinos> getDetalhes() {
		return detalhes;
	}

	public void setDetalhes(List<DetalhesInformacoesSuinos> detalhes) {
		this.detalhes = detalhes;
	}

	@Override
	protected List<AbstractDetalhesInformacoesDeAnimais> getDetalhesInformacoesDeAnimais() {
		List<AbstractDetalhesInformacoesDeAnimais> lista = new ArrayList<AbstractDetalhesInformacoesDeAnimais>();
		lista.addAll(this.detalhes);
		return lista;
	}
	
	protected Object clone(FormIN formIN) throws CloneNotSupportedException{
		InformacoesSuinos cloned = (InformacoesSuinos) super.clone();
		
		cloned.setId(null);
		
		if (this.getDestinos() != null){
			cloned.setDestinos(new ArrayList<TipoDestinoExploracao>());
			for (TipoDestinoExploracao destino : this.getDestinos()) {
				cloned.getDestinos().add(destino);
			}
		}
		
		if (this.detalhes != null){
			cloned.setDetalhes(new ArrayList<DetalhesInformacoesSuinos>());
			for (DetalhesInformacoesSuinos detalhe : this.detalhes) {
				cloned.getDetalhes().add((DetalhesInformacoesSuinos) detalhe.clone(cloned));
			}
		}
		
		cloned.setFormIN(formIN);
		
		return cloned;
	}

}
