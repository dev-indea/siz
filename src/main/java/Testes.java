import java.security.SecureRandom;
import java.util.Random;

import javax.resource.spi.IllegalStateException;

public class Testes {

	public static void main(String[] args) throws IllegalStateException {

		System.out.println(generatePassword(20));
	}

	private static final Random RANDOM = new SecureRandom();
	private static final String ALPHABET = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

	public static String generatePassword(int length) {
		StringBuilder returnValue = new StringBuilder(length);
		for (int i = 0; i < length; i++) {
			returnValue.append(ALPHABET.charAt(RANDOM.nextInt(ALPHABET.length())));
		}
		return new String(returnValue);
	}

	private static String format(String codigo) throws IllegalStateException {
		if (codigo == null)
			return null;

		if (codigo.length() == 11) {
			if (codigo.startsWith("51")) {
				codigo = codigo.replaceFirst("51", "");

				return format(codigo);
			}
		} else if (codigo.length() == 9) {
			if (codigo.startsWith("0")) {
				codigo = codigo.replaceFirst("0", "");

				return format(codigo);
			} else
				return codigo;
		}

		throw new IllegalStateException("O c�digo da propriedade n�o est� formatado corretamente");
	}

}