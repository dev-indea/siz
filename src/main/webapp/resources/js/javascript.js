
function closeMsg() {
	(document.getElementById('messagesPage')).style.display = 'none';
}

function textCounter(campo, exibir, maxlimit) {
	if (document.getElementById(campo).value.length > maxlimit)
		document.getElementById(campo).value = document.getElementById(campo).value
				.substring(0, maxlimit);
	else
		document.getElementById(exibir).value = maxlimit
				- document.getElementById(campo).value.length;
}

/**
 * ABRE UMA NOVA JANELA COM TELA CHEIA
 */
function pageStart(theURL, winName) {

	/** * ABRE UMA NOVA JANELA COM TELA CHEIA ** */

	document.body.scroll = "yes";
	var width = 700;
	var height = 480;
	var jan = window
			.open(
					theURL,
					winName,
					'width='
							+ width
							+ ', height='
							+ height
							+ ', toolbar=no, copyhistory=no, location=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, top=0, left=0');
	width = screen.availWidth;
	height = screen.availHeight;
	jan.window.resizeTo(width, height);
	jan.focus();

}

/**
 * funcao para ler quantidade de erros armazenado
 * 
 * NOTA: deve colocar um inputHidden para armazenar a quantidade de erros vindo
 * do request
 * 
 * <a4j:outputPanel ajaxRendered="true"> <h:form style="display:none"
 * prependId="false"> <h:inputHidden id="maximumSeverity"
 * value="#{facesContext.maximumSeverity.ordinal}"/> </h:form>
 * </a4j:outputPanel>
 * 
 */
function ajaxRequestContainsErrors() {
	return document.getElementById("maximumSeverity").value == "2";
}

/**
 * funcao para gerar mascara
 */
function Mascara(objeto, evt, mask) {
	var LetrasU = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	var LetrasL = 'abcdefghijklmnopqrstuvwxyz';
	var Letras = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz ';
	var Numeros = '0123456789';
	var Moeda = '0123456789,';
	var Fixos = '().-:/ ';
	var Charset = " !\"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_/`abcdefghijklmnopqrstuvwxyz{|}~";
	var NumerosDocs = '0123456789-/';
	var LetrasObs = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.,; ';

	evt = (evt) ? evt : (window.event) ? window.event : "";
	var value = objeto.value;
	if (evt) {
		var ntecla = (evt.which) ? evt.which : evt.keyCode;
		tecla = Charset.substr(ntecla - 32, 1);
		if (ntecla < 32)
			return true;

		var tamanho = value.length;
		if (tamanho >= mask.length)
			return false;

		var pos = mask.substr(tamanho, 1);
		while (Fixos.indexOf(pos) != -1) {
			value += pos;
			tamanho = value.length;
			if (tamanho >= mask.length)
				return false;
			pos = mask.substr(tamanho, 1);
		}

		switch (pos) {
		case '#':
			if (Numeros.indexOf(tecla) == -1)
				return false;
			break;
		case 'A':
			if (LetrasU.indexOf(tecla) == -1)
				return false;
			break;
		case 'a':
			if (LetrasL.indexOf(tecla) == -1)
				return false;
			break;
		case 'Z':
			if (Letras.indexOf(tecla) == -1)
				return false;
			break;
		case 'N':
			if (NumerosDocs.indexOf(tecla) == -1)
				return false;
			break;
		case 'O':
			if (LetrasObs.indexOf(tecla) == -1)
				return false;
			break;
		case 'M':
			if (Moeda.indexOf(tecla) == -1) {
				return false;
				break;
			} else {
				if (objeto.value.indexOf(tecla) != -1 && tecla == ',') {
					return false;
					break;
				}
			}
		case '*':
			objeto.value = value;
			return true;
			break;
		default:
			return false;
			break;
		}
	}
	objeto.value = value;
	return true;
}

function FormataData(objeto, evt) {
	return Mascara(objeto, evt, '##/##/####');
}

function FormataHora(objeto, evt) {
	return Mascara(objeto, evt, '##:##');
}

function FormataTelefone(objeto, evt) {
	return Mascara(objeto, evt, '(##) ####-####');
}

function FormataDataHora(objeto, evt) {
	return Mascara(objeto, evt, '##/##/#### ##:##');
}

function FormataCep(objeto, evt) {
	return Mascara(objeto, evt, '##.###-###');
}

function FormataCpf(objeto, evt) {
	return Mascara(objeto, evt, '###.###.###-##');
}

function FormataCnpj(objeto, evt) {
	return Mascara(objeto, evt, '##.###.###/####-##');
}

function FormataNome(objeto, evt) {
	return Mascara(
			objeto,
			evt,
			'ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ');
}

function FormataNumero(objeto, evt) {
	return Mascara(objeto, evt, 'NNNNNNNNNNNNNNNN');
}

function FormataMoeda(objeto, evt) {
	return Mascara(objeto, evt, 'MMMMMMMMMMMMMMMM');
}

function Limpar(valor, validos) {
	// retira caracteres invalidos da string
	var result = "";
	var aux;
	for ( var i = 0; i < valor.length; i++) {
		aux = validos.indexOf(valor.substring(i, i + 1));
		if (aux >= 0) {
			result += aux;
		}
	}
	return result;
}

function formataValor(campo, tammax, teclapres, decimal) {
	var tecla = teclapres.keyCode;
	vr = Limpar(campo.value, "0123456789");
	tam = vr.length;
	dec = decimal;

	if (tam < tammax && tecla != 8) {
		tam = vr.length + 1;
	}

	if (tecla == 8) {
		tam = tam - 1;
	}

	if (tecla == 8 || tecla >= 48 && tecla <= 57 || tecla >= 96 && tecla <= 105) {
		if (tam <= dec) {
			campo.value = vr;
		}

		if ((tam > dec) && (tam <= 5)) {
			campo.value = vr.substr(0, tam - 2) + ","
					+ vr.substr(tam - dec, tam);
		}

		if ((tam >= 6) && (tam <= 8)) {
			campo.value = vr.substr(0, tam - 5) + "." + vr.substr(tam - 5, 3)
					+ "," + vr.substr(tam - dec, tam);
		}

		if ((tam >= 9) && (tam <= 11)) {
			campo.value = vr.substr(0, tam - 8) + "." + vr.substr(tam - 8, 3)
					+ "." + vr.substr(tam - 5, 3) + ","
					+ vr.substr(tam - dec, tam);
		}

		if ((tam >= 12) && (tam <= 14)) {
			campo.value = vr.substr(0, tam - 11) + "." + vr.substr(tam - 11, 3)
					+ "." + vr.substr(tam - 8, 3) + "." + vr.substr(tam - 5, 3)
					+ "," + vr.substr(tam - dec, tam);
		}

		if ((tam >= 15) && (tam <= 17)) {
			campo.value = vr.substr(0, tam - 14) + "." + vr.substr(tam - 14, 3)
					+ "." + vr.substr(tam - 11, 3) + "."
					+ vr.substr(tam - 8, 3) + "." + vr.substr(tam - 5, 3) + ","
					+ vr.substr(tam - 2, tam);
		}
	}
}